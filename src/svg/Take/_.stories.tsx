import { Story, Meta } from "@storybook/react";

import { Take } from "./index";

export default {
    title: "Svg/Take",
    component: Take,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Take {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
