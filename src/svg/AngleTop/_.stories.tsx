import { Story, Meta } from "@storybook/react";

import { AngleTop } from "./index";

export default {
    title: "Svg/AngleTop",
    component: AngleTop,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <AngleTop {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
