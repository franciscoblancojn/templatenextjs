import { Story, Meta } from "@storybook/react";

import { Xfollow } from "./index";

export default {
    title: "Svg/Xfollow",
    component: Xfollow,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Xfollow {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
