import { Story, Meta } from "@storybook/react";

import { File } from "./index";

export default {
    title: "Svg/File",
    component: File,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <File {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
