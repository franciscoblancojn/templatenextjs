import { Story, Meta } from "@storybook/react";

import { Ban } from "./index";

export default {
    title: "Svg/Ban",
    component: Ban,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Ban {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
