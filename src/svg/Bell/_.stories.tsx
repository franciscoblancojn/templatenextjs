import { Story, Meta } from "@storybook/react";

import { Bell } from "./index";

export default {
    title: "Svg/Bell",
    component: Bell,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Bell {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
