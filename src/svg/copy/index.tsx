export const Copy = ({ size = 16 }: { size?: number }) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width={size / 16 + 'rem'}
        viewBox="0 0 23.469 29.309"
    >
        <g data-name="Group 15618">
            <path
                data-name="Path 11662"
                d="M64.592 85.4H50.967a2.318 2.318 0 0 0-2.317 2.317v19.55a2.318 2.318 0 0 0 2.317 2.317h13.625a2.318 2.318 0 0 0 2.317-2.317v-19.55a2.326 2.326 0 0 0-2.317-2.317zm.69 21.861a.7.7 0 0 1-.7.7H50.961a.7.7 0 0 1-.7-.7V87.717a.7.7 0 0 1 .7-.7h13.625a.7.7 0 0 1 .7.7z"
                transform="translate(-48.65 -80.274)"
                fill="currentColor"
            />
            <path
                data-name="Path 11663"
                d="M151.392 0h-13.625a2.318 2.318 0 0 0-2.317 2.317.81.81 0 0 0 1.621 0 .7.7 0 0 1 .7-.7h13.625a.7.7 0 0 1 .7.7v19.55a.7.7 0 0 1-.7.7.81.81 0 1 0 0 1.621 2.318 2.318 0 0 0 2.317-2.317V2.317A2.318 2.318 0 0 0 151.392 0z"
                transform="translate(-130.24)"
                fill="currentColor"
            />
        </g>
    </svg>
);
export default Copy;
