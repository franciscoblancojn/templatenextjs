import { Story, Meta } from "@storybook/react";

import { Fansoda } from "./index";

export default {
    title: "Svg/Fansoda",
    component: Fansoda,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Fansoda {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
