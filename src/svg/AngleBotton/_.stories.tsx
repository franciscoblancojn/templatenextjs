import { Story, Meta } from "@storybook/react";

import { AngleBottom } from "./index";

export default {
    title: "Svg/AngleBottom",
    component: AngleBottom,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <AngleBottom {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
