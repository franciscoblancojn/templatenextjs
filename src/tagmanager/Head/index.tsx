import * as styles from '@/tagmanager/Head/styles';

import { Theme } from '@/config/theme';
import { HeadClassProps } from '@/tagmanager/Head/Base';

export const HeadStyle = { ...styles } as const;

export type HeadStyles = keyof typeof HeadStyle;

export const Head: () => string = () => {
    const Style: HeadClassProps =
        HeadStyle[(Theme?.styleTemplate ?? '_default') as HeadStyles] ??
        HeadStyle._default;

    return Style.content ?? '';
};
export default Head;
