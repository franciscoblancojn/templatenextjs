# Head

## Dependencies

[Head](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/tagmanager/Head)

```js
import { Head } from '@/tagmanager/Head';
```

## Import

```js
import { Head, HeadStyles } from '@/tagmanager/Head';
```

## Props

```tsx
interface HeadProps {}
```

## Use

```js
<Head>Head</Head>
```
