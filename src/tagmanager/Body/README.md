# Body

## Dependencies

[Body](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/tagmanager/Body)

```js
import { Body } from '@/tagmanager/Body';
```

## Import

```js
import { Body, BodyStyles } from '@/tagmanager/Body';
```

## Props

```tsx
interface BodyProps {}
```

## Use

```js
<Body>Body</Body>
```
