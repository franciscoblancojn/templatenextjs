import { Story, Meta } from "@storybook/react";

import { PerfilTwo } from "./index";

export default {
    title: "Lottie/PerfilTwo",
    component: PerfilTwo,
} as Meta;

const TemplateIndex: Story = (args) => (
    <PerfilTwo {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
