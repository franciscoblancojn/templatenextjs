import lottieSecundario from '@/public/lottie/lottieSecundario.json';
import ImageLottie from '@/components/ImageLottie';

export const PerfilTwo = () => <ImageLottie img={lottieSecundario} />;
export default PerfilTwo;
