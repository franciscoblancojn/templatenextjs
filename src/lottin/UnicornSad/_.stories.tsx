import { Story, Meta } from "@storybook/react";

import { UnicornSad } from "./index";

export default {
    title: "Lottie/UnicornSad",
    component: UnicornSad,
} as Meta;

const TemplateIndex: Story= (args) => (
    <UnicornSad {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
