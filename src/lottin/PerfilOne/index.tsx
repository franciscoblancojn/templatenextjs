import lottiePrincipal from '@/public/lottie/lottiePrincipal.json';
import ImageLottie from '@/components/ImageLottie';

export const PerfilOne = () => <ImageLottie img={lottiePrincipal} />;
export default PerfilOne;
