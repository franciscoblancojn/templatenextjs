import { Story, Meta } from "@storybook/react";

import { PerfilOne } from "./index";

export default {
    title: "Lottie/PerfilOne",
    component: PerfilOne,
} as Meta;

const TemplateIndex: Story = (args) => (
    <PerfilOne {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
