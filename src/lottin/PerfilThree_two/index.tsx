import lottieThree_two from '@/public/lottie/lottieThree_two.json';
import ImageLottie from '@/components/ImageLottie';

export const PerfilThree_two = () => <ImageLottie img={lottieThree_two} />;
export default PerfilThree_two;
