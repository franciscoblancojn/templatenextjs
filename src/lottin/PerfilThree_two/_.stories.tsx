import { Story, Meta } from "@storybook/react";

import { PerfilThree_two } from "./index";

export default {
    title: "Lottie/PerfilThree_two",
    component: PerfilThree_two,
} as Meta;

const TemplateIndex: Story = (args) => (
    <PerfilThree_two {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
