import { Story, Meta } from "@storybook/react";

import { UnicornHappy } from "./index";

export default {
    title: "Lottie/UnicornHappy",
    component: UnicornHappy,
} as Meta;

const TemplateIndex: Story = (args) => (
    <UnicornHappy {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
