import adinerado from '@/public/lottie/adinerado.json';
import ImageLottie from '@/components/ImageLottie';

export const UnicornWealthy = () => <ImageLottie img={adinerado} />;
export default UnicornWealthy;
