import { Story, Meta } from "@storybook/react";

import { PerfilFour } from "./index";

export default {
    title: "Lottie/PerfilFour",
    component: PerfilFour,
} as Meta;

const TemplateIndex: Story = (args) => (
    <PerfilFour {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
