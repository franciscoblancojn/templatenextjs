import lottieFour from '@/public/lottie/lottieFour.json';
import ImageLottie from '@/components/ImageLottie';

export const PerfilFour = () => <ImageLottie img={lottieFour} />;
export default PerfilFour;
