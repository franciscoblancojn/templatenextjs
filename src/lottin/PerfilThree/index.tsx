import lottieThree from '@/public/lottie/lottieThree.json';
import ImageLottie from '@/components/ImageLottie';

export const PerfilThree = () => <ImageLottie img={lottieThree} />;
export default PerfilThree;
