import { Story, Meta } from "@storybook/react";

import { PerfilThree } from "./index";

export default {
    title: "Lottie/PerfilThree",
    component: PerfilThree,
} as Meta;

const TemplateIndex: Story = (args) => (
    <PerfilThree {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
