import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataFormMonetizeSelfieForm } from '@/interfaces/FormMonetizeSelfieForm';

export const FormMonetizeSelfieFormYup = (
    data: DataFormMonetizeSelfieForm,
    inputs: DataFormMonetizeSelfieForm<boolean>
) => {
    const y: DataFormMonetizeSelfieForm<any> = {};

    if (inputs.avatar) {
        y.avatar = ValidateObject({
            fileData: ValidateText({
                errors: DATAERROR.name.ErrorName,
                require: true,
            }),
        });
    }
    return {
        y,
        v: ValidateObject(y),
    };
};
