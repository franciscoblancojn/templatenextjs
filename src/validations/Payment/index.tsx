import { ValidateObject } from '@/validations/object';

import * as Yup from 'yup';

import * as DATAERROR from '@/data/error';

import { DataFormPayment } from '@/interfaces/FormPayment';
import ValidateText from '../text';

export const FormPaymentYup = (
    data: DataFormPayment,
    inputs: DataFormPayment<boolean>
) => {
    const y: DataFormPayment<any> = {};
    const j: DataFormPayment<any> = {};
    //Name
    if (inputs.email) {
        y.email = ValidateText({
            errors: DATAERROR.email.default,
            type: 'email',
            require: true,
        });
        j.email = y.email;
    }
    if (inputs.nameOnTheCard) {
        y.nameOnTheCard = ValidateText({
            errors: DATAERROR.nameCard.ErrorNameOnTheCard,
            require: true,
        });
        j.nameOnTheCard = y.nameOnTheCard;
    }
    //Stripe
    if (inputs.stripe_before) {
        y.stripe_before = ValidateObject({
            complete: Yup.mixed().oneOf([true]),
        });
        j.stripe_before = y.stripe_before;
    }
    //Address
    if (inputs.Street) {
        y.Street = ValidateText({
            errors: DATAERROR.street.ErrorStreet,
            require: true,
        });
    }
    if (inputs.zipPostalCode) {
        y.zipPostalCode = ValidateText({
            errors: DATAERROR.zipPostalCode.ErrorZipPostalCode,
            require: true,
        });
    }
    if (inputs.country) {
        y.country = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }

    if (inputs.state) {
        y.state = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }

    if (inputs.city) {
        y.city = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }
    if (inputs.confirm18) {
        y.confirm18 = Yup.mixed().required().oneOf([true]);
    }
    return {
        y,
        v: ValidateObject(y),
    };
};
