import { ValidateObject } from '@/validations/object';
import ValidateFrom from '@/validations/formRadio';
import * as DATAERROR from '@/data/error';
import { DataFormMonetize } from '@/interfaces/FormMonetize';
import ValidateText from '../text';

export const FromMonetizeYup = (
    data: DataFormMonetize,
    inputs: DataFormMonetize<boolean>
) => {
    const y: DataFormMonetize<any> = {};
    if (inputs.amount) {
        y.amount = ValidateFrom({ require: true });
    }
    if (inputs.description) {
        y.description = ValidateText({
            errors: DATAERROR.description.ErrorDescription,
            require: true,
        });
    }

    return {
        y,
        v: ValidateObject(y),
    };
};
