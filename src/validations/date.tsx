import * as Yup from 'yup';

export interface ValidateDateProps {
    require?: boolean;
    min?: false | Date;
    max?: false | Date;
    errors?: {
        required?: string;
        invalid?: string;
        min?: string;
        max?: string;
    };
}

export const ValidateDate = ({
    require = true,
    min = false,
    max = false,
    errors = {
        required: 'This is Required',
        invalid: 'This Invalid',
        min: 'This is min',
        max: 'This is max',
    },
}: ValidateDateProps) => {
    let yup = Yup.date();

    if (require) {
        yup = yup.required(errors.required);
    }
    if (min !== false) {
        yup = yup.min(min, errors.min);
    }
    if (max !== false) {
        yup = yup.max(max, errors.max);
    }
    return yup;
};
export default ValidateDate;
