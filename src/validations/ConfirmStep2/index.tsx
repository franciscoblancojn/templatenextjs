import { ValidateObject } from '@/validations/object';
import * as DATAERROR from '@/data/error';

import { DataFormConfirmStep2Props } from '@/interfaces/FormConfirmStep2';

import ValidateText from '@/validations/text';
import ValidateTel from '@/validations/tel';

export const ConfirmStep2 = (
    data: DataFormConfirmStep2Props<any>,
    inputs: DataFormConfirmStep2Props<boolean>
) => {
    const y: DataFormConfirmStep2Props<any> = {};
    const j: DataFormConfirmStep2Props<any> = {};

    if (inputs.contactName) {
        y.contactName = ValidateText({
            errors: DATAERROR.contactName.ErrorContactName,
            require: true,
        });
        j.contactName = y.contactName;
    }

    if (inputs.phone) {
        y.phone = ValidateTel({
            errors: DATAERROR.tel.ErrorTel,
            require: true,
        });
        j.phone = y.phone;
    }
    if (inputs.phoneAlternative) {
        y.phoneAlternative = ValidateTel({
            errors: DATAERROR.tel.ErrorTel,
            require: true,
        });
        j.phoneAlternative = y.phoneAlternative;
    }
    if (inputs.search) {
        y.search = ValidateText({
            errors: DATAERROR.address.ErrorAddress,
            require: true,
        });
        j.search = y.search;
    }

    return {
        v: ValidateObject(y),
        y: j,
    };
};
