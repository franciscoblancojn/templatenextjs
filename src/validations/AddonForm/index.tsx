import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';

import { MessageUserSend } from '@/interfaces/message';
import log from '@/functions/log';
import ValidateText from '../text';

export const AddonFormYup = (data: MessageUserSend) => {
    log('UserProps', data);
    const y: any = {};

    if (data.name) {
        y.name = ValidateText({
            errors: DATAERROR.name.ErrorName,
            require: true,
        });
    }

    if (data.email) {
        y.email = ValidateText({
            errors: DATAERROR.email.ErrorEmail,
            type: 'email',
            require: true,
        });
    }

    if (data.phone) {
        y.phone = ValidateText({
            errors: DATAERROR.tel.ErrorTel,
            require: true,
            min: 7,
            max: 10,
            integer: false,
        });
    }
    if (data.message) {
        y.message = ValidateText({
            errors: DATAERROR.message.ErrorMessage,
            min: 4,
            max: 150,
            require: true,
        });
    }

    return ValidateObject(y);
};
