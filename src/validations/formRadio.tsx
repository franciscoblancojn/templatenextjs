import * as Yup from 'yup';

export interface ValidateTextProps {
    require?: boolean;
    integer?: false | number;
    type?: false | 'optional' | 'url';
}

export const ValidateFrom = ({
    require = true,
    type = false,
}: ValidateTextProps) => {
    let yup = Yup.string();

    if (require) {
        yup = yup.required();
    }
    if (type !== false) {
        yup = yup[type]();
    }
    return yup;
};
export default ValidateFrom;
