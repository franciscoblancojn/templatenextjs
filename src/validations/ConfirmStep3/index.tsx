import { ValidateObject } from '@/validations/object';
import * as DATAERROR from '@/data/error';

import { DataFormConfirmStep3Props } from '@/interfaces/FormConfirmStep3';

import ValidateNumber from '../number';

export const ConfirmStep3 = (
    data: DataFormConfirmStep3Props<any>,
    inputs: DataFormConfirmStep3Props<boolean>
) => {
    const y: DataFormConfirmStep3Props<any> = {};
    const j: DataFormConfirmStep3Props<any> = {};

    if (inputs.numberTrucks) {
        y.numberTrucks = ValidateNumber({
            errors: DATAERROR.numberTrucks.ErrorNumberTrucks,
            min: 1,
            require: true,
        });
        j.numberTrucks = y.numberTrucks;
    }

    if (inputs.numberWorkers) {
        y.numberWorkers = ValidateNumber({
            errors: DATAERROR.numberWorkers.ErrorNumberWorkers,
            min: 1,
            require: true,
        });
        j.numberWorkers = y.numberWorkers;
    }

    return {
        v: ValidateObject(y),
        y: j,
    };
};
