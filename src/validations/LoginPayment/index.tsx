import { ValidateObject } from '@/validations/object';
import * as DATAERROR from '@/data/error';

import { DataLoginPayment } from '@/interfaces/LoginPayment';

import ValidateText from '@/validations/text';

export const LoginPaymentYup = (
    data: DataLoginPayment,
    inputs: DataLoginPayment<boolean>
) => {
    const y: DataLoginPayment<any> = {};

    if (inputs.email) {
        y.email = ValidateText({
            errors: DATAERROR.email.default,
            type: 'email',
        });
    }

    if (inputs.password) {
        y.password = ValidateText({
            errors: DATAERROR.password.default,
            min: 6,
            max: 20,
        });
    }

    return ValidateObject(y);
};
