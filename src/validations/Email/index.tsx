import * as DATAERROR from '@/data/error';

import ValidateText from '@/validations/text';

export const email = ValidateText({
    errors: DATAERROR.email.default,
    type: 'email',
});
