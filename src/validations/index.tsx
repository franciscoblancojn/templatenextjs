import * as Yup from 'yup';

import * as DATAERROR from '@/data/error';

import ValidateText from '@/validations/text';
import ValidateNumber from '@/validations/number';

export const V = {
    name: ValidateText({
        errors: DATAERROR.name.ErrorName,
    }),
    email: ValidateText({
        errors: DATAERROR.email.ErrorEmail,
        type: 'email',
    }),
    url: ValidateText({
        errors: DATAERROR.url.ErrorUrl,
        type: 'url',
    }),
    number: ValidateNumber({
        errors: DATAERROR.number.ErrorNumber,
        min: 0,
        max: 10,
    }),
    password: ValidateText({
        errors: DATAERROR.password.ErrorPassword,
        min: 6,
        max: 20,
    }),
    options: Yup.object().shape({
        value: ValidateText({
            errors: DATAERROR.options.ErrorOptions,
        }),
    }),
    date: ValidateText({
        errors: DATAERROR.date.ErrorDate,
    }),
};
export default V;
