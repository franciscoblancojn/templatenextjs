import * as Yup from 'yup';

export const ValidateBoolean = ({
    errors,
    require = false,
}: {
    errors: string;
    require?: boolean;
}) => {
    let validation = Yup.boolean();

    if (require) {
        validation = validation.oneOf([true], errors);
    }

    return validation;
};
