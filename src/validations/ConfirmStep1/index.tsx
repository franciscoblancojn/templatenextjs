import { ValidateObject } from '@/validations/object';
import * as DATAERROR from '@/data/error';

import { DataFormConfirmStep1Props } from '@/interfaces/FormConfirmStep1';

import ValidateText from '@/validations/text';

export const ConfirmStep1 = (
    data: DataFormConfirmStep1Props<any>,
    inputs: DataFormConfirmStep1Props<boolean>
) => {
    const y: DataFormConfirmStep1Props<any> = {};
    const j: DataFormConfirmStep1Props<any> = {};

    if (inputs.companyName) {
        y.companyName = ValidateText({
            errors: DATAERROR.company.ErrorCompany,
            require: true,
        });
        j.companyName = y.companyName;
    }

    if (inputs.legalName) {
        y.legalName = ValidateText({
            errors: DATAERROR.legalName.ErrorLegalName,
            require: true,
        });
        j.legalName = y.legalName;
    }
    if (inputs.yearFounded) {
        y.yearFounded = ValidateText({
            errors: DATAERROR.yearFounded.ErrorYearFounded,
            require: true,
        });
        j.yearFounded = y.yearFounded;
    }
    return {
        v: ValidateObject(y),
        y: j,
    };
};
