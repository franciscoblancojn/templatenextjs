// import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
// import ValidateText from '@/validations/text';

import { UserProps } from '@/interfaces/User';
import log from '@/functions/log';

export const UserYup = (data: UserProps) => {
    log('UserProps', data);
    const y: any = {};

    // if (inputs.name) {
    //     y.name = ValidateText({
    //         errors: DATAERROR.name.ErrorName,
    //     });
    // }

    // if (inputs.email) {
    //     y.email = ValidateText({
    //         errors: DATAERROR.email.default,
    //         type: 'email',
    //     });
    // }

    return ValidateObject(y);
};
