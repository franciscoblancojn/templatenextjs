import { ValidateObject } from '@/validations/object';

import * as Yup from 'yup';

import * as DATAERROR from '@/data/error';

import { DataFormStripe } from '@/interfaces/FormStripe';
import ValidateText from '../text';
import ValidateTel from '../tel';

export const FormStripeYup = (
    data: DataFormStripe,
    inputs: DataFormStripe<boolean>
) => {
    const y: DataFormStripe<any> = {};
    const j: DataFormStripe<any> = {};
    //Name
    if (inputs.firstName) {
        y.firstName = ValidateText({
            errors: DATAERROR.name.ErrorFirstName,
            require: true,
        });
        j.firstName = y.firstName;
    }
    if (inputs.MiddleName) {
        y.MiddleName = ValidateText({
            errors: DATAERROR.name.ErrorMiddleName,
            require: true,
        });
        j.MiddleName = y.MiddleName;
    }
    if (inputs.LastName) {
        y.LastName = ValidateText({
            errors: DATAERROR.name.ErrorLastName,
            require: true,
        });
        j.LastName = y.LastName;
    }
    //Stripe
    if (inputs.stripe_before) {
        y.stripe_before = ValidateObject({
            complete: Yup.mixed().oneOf([true]),
        });
        j.stripe_before = y.stripe_before;
    }
    //Address
    if (inputs.country) {
        y.country = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }

    if (inputs.state) {
        y.state = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }

    if (inputs.city) {
        y.city = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }
    //Contact Details
    if (inputs.phone) {
        y.phone = ValidateTel({
            require: true,
            errors: DATAERROR.tel.ErrorTel,
        });
        j.phone = y.phone;
    }

    return {
        y,
        v: ValidateObject(y),
    };
};
