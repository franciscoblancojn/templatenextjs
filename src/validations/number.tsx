import * as Yup from 'yup';

export interface ValidateNumberProps {
    require?: boolean;
    min?: false | number;
    max?: false | number;
    integer?: false | number;
    errors?: {
        required?: string;
        invalid?: string;
        min?: string;
        max?: string;
        integer?: string;
    };
}

export const ValidateNumber = ({
    require = true,
    min = false,
    max = false,
    integer = false,
    errors = {
        required: 'This is Required',
        invalid: 'This Invalid',
        min: 'This is min',
        max: 'This is max',
        integer: 'This is not integer',
    },
}: ValidateNumberProps) => {
    let yup = Yup.number();

    if (require) {
        yup = yup.required(errors.required);
    }
    if (min !== false) {
        yup = yup.min(min, errors.min);
    }
    if (max !== false) {
        yup = yup.max(max, errors.max);
    }
    if (integer !== false) {
        yup = yup.integer(errors.integer);
    }
    return yup;
};
export default ValidateNumber;
