import { ValidateObject } from '@/validations/object';
import * as Yup from 'yup';

import { DataFormPay } from '@/interfaces/FormPay';

export const FormPayYup = (data: DataFormPay, inputs: DataFormPay<boolean>) => {
    const y: DataFormPay<any> = {};

    if (inputs.paymentMethodId) {
        y.paymentMethodId = Yup.string().required();
    }
    return {
        y,
        v: ValidateObject(y),
    };
};
