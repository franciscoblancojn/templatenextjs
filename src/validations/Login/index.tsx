import { ValidateObject } from '@/validations/object';
import * as DATAERROR from '@/data/error';

import { DataLogin } from '@/interfaces/Login';

import ValidateText from '@/validations/text';

export const LoginYup = (data: DataLogin, inputs: DataLogin<boolean>) => {
    const y: DataLogin<any> = {};

    if (inputs.email) {
        y.email = ValidateText({
            errors: DATAERROR.email.default,
            type: 'email',
        });
    }

    if (inputs.password) {
        y.password = ValidateText({
            errors: DATAERROR.password.default,
            min: 6,
            max: 20,
        });
    }

    return ValidateObject(y);
};
