import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataSearchBook } from '@/interfaces/SearchBook';
import { SelectOptionProps } from '@/components/Input/Select';
import { GoogleAddres } from '@/interfaces/Google/addres';
import { InputFileDataProps } from '@/components/Input/File/Base';

export const SearchBookYup = (
    data: DataSearchBook<
        | string
        | Date
        | SelectOptionProps
        | GoogleAddres
        | boolean
        | number
        | InputFileDataProps[]
    >,
    inputs: DataSearchBook<boolean>
) => {
    const y: DataSearchBook<any> = {};
    const j: DataSearchBook<any> = {};

    if (inputs.date) {
        y.date = ValidateText({
            errors: DATAERROR.date.ErrorDate,
            require: true,
        });
        j.date = y.date;
    }

    if (inputs.moveSize) {
        y.moveSize = ValidateObject({
            value: ValidateText({
                errors: DATAERROR.moveSize.ErrorMoveSize,
                require: true,
            }),
        });
        j.moveSize = ValidateText({
            errors: DATAERROR.moveSize.ErrorMoveSize,
            require: true,
        });
    }
    if (inputs.movingFrom) {
        y.movingFrom = ValidateObject({
            formatedAddress: ValidateText({
                errors: DATAERROR.address.ErrorAddress,
                require: true,
            }),
        });
        j.movingFrom = ValidateText({
            errors: DATAERROR.address.ErrorAddress,
            require: true,
        });
    }
    if (inputs.movingTo) {
        y.movingTo = ValidateObject({
            formatedAddress: ValidateText({
                errors: DATAERROR.address.ErrorAddress,
                require: true,
            }),
        });
        j.movingFrom = ValidateText({
            errors: DATAERROR.address.ErrorAddress,
            require: true,
        });
    }
    if (data.youStairs && inputs.numberStairs) {
        (y.numberStairs = ValidateText({
            errors: DATAERROR.numberStairs.ErrorNumberStairs,
            require: true,
        })),
            (j.numberStairs = y.numberStairs);
    }

    return {
        v: ValidateObject(y),
        y: j,
    };
};
