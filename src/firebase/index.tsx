import firebase from './api/init';

import google from './api/google';
import facebook from './api/facebook';
import {
    UploadFile as uploadFile,
    UploadFileBase64 as uploadFileBase64,
} from './api/uploadFile';

export const LoginFacebook = facebook(firebase);
export const LoginGoogle = google(firebase);
export const UploadFile = uploadFile(firebase);
export const UploadFileBase64 = uploadFileBase64(firebase);
