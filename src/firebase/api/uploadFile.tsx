import { FirebaseApp } from 'firebase/app';
import {
    ref,
    getDownloadURL,
    uploadBytesResumable,
    getStorage,
    uploadString,
} from 'firebase/storage';

export const UploadFile =
    (app: FirebaseApp) =>
    (
        file: any,
        setProgresspercent: (progress: number) => void,
        setImgUrl: (downloadURL: string) => void
    ) => {
        const storage = getStorage(
            app,
            process.env['NEXT_PUBLIC_FIREBASE_STORAGE_URL']
        );

        const storageRef = ref(
            storage,
            `/${process.env['NEXT_PUBLIC_FIREBASE_FOLDER']}/files/${file.name}`
        );
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on(
            'state_changed',
            (snapshot) => {
                const progress = Math.round(
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );
                setProgresspercent(progress);
            },
            (error) => {
                alert(error);
            },
            () => {
                getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                    setImgUrl(downloadURL);
                });
            }
        );
    };

export const UploadFileBase64 =
    (app: FirebaseApp) => async (base64str: string, name: string) => {
        const storage = getStorage(
            app,
            process.env['NEXT_PUBLIC_FIREBASE_STORAGE_URL']
        );

        const storageRef = ref(
            storage,
            `/${process.env['NEXT_PUBLIC_FIREBASE_FOLDER']}/files/${name}`
        );

        const r = await uploadString(
            storageRef,
            base64str.split(',')[1],
            'base64',
            {
                contentType: 'image/png',
            }
        );
        const url = await getDownloadURL(r.ref);
        return { r, url };
    };
export default UploadFile;
