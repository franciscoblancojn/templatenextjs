import { IframeClassProps } from '@/components/Iframe/Base';

export const tolinkme: IframeClassProps = {
    classNameContent: `
        p-10 p-xl-20
        bg-black
        border-radius-20
        pos-r
        width-p-100

    `,
    styleContentDesktop: {
        maxWidth: '1280px',
    },
    styleContentMovil: {
        maxWidth: '375px',
    },
    classNameContentIframe: `
        pos-r
    `,
    styleDesktop: {
        paddingBottom: '62.5%',
    },
    styleMovil: {
        paddingBottom: '177.8%',
    },
    styleIframe: {},
    classNameIframe: `
        pos-a
        inset-0
        m-auto
        width-p-100
        height-p-100
        border-0
        border-radius-5
    `,
    classNameDesktop: `
        
    `,
    classNameMovil: `
        
    `,
    classNameLoader: `
        pos-a
        inset-0
        m-auto
        width-p-100
        height-p-100
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    sizeLoader: 150,
};

export const tolinkmeFull: IframeClassProps = {
    classNameContent: `
        width-p-100
        height-p-100
        pos-r
        bg-black
    `,
    styleContentDesktop: {
        maxWidth: '100%',
    },
    styleContentMovil: {
        maxWidth: '100%',
    },
    classNameContentIframe: `
    `,
    styleDesktop: {},
    styleMovil: {},
    styleIframe: {},
    classNameIframe: `
        pos-a
        inset-0
        m-auto
        width-p-100
        height-p-100
        border-0
        border-radius-5
    `,
    classNameDesktop: `
        
    `,
    classNameMovil: `
        
    `,
    classNameLoader: `
        pos-a
        inset-0
        m-auto
        width-p-100
        height-p-100
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    sizeLoader: 150,
};
