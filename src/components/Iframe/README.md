# Iframe

## Import

```js
import { Iframe, IframeStyles } from '@/components/Iframe';
```

## Props

```ts
interface IframeProps {
    styleTemplate?: IframeStyles;
    url: string;
    type?: 'movil' | 'desktop';
}
```

## Use

```js
<Iframe url="https://fenextjs.vercel.app/" />
```
