import { Story, Meta } from "@storybook/react";

import { IframeProps, Iframe } from "./index";

export default {
    title: "Content/Iframe",
    component: Iframe,
} as Meta;

const IframeIndex: Story<IframeProps> = (args) => (
    <Iframe {...args}>Test Children</Iframe>
);

export const Index = IframeIndex.bind({});
Index.args = {};
