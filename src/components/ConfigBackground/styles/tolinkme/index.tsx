import { ConfigBackgroundClassProps } from '@/components/ConfigBackground/Base';

export const tolinkme: ConfigBackgroundClassProps = {
    classNameContentSw: `
        p-h-15  
        flex
        flex-column
        flex-gap-row-10
    `,

    styleTemplatePopup: 'tolinkme2',
    styleTemplateTitlePopup: 'tolinkme21',
    styleTemplateCollapse: 'tolinkme',
    styleTemplateCheckbox: 'tolinkme2',
    styleTemplateTitleBg: 'tolinkme13',
    classNameReload: `
        cursor-pointer
    `,
};
