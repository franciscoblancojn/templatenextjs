import { Story, Meta } from "@storybook/react";

import { ConfigBackgroundProps, ConfigBackground } from "./index";

export default {
    title: "ConfigBackground/ConfigBackground",
    component: ConfigBackground,
} as Meta;

const ConfigBackgroundIndex: Story<ConfigBackgroundProps> = (args) => (
    <ConfigBackground {...args}>Test Children</ConfigBackground>
);

export const Index = ConfigBackgroundIndex.bind({});
Index.args = {};
