import { useMemo } from 'react';

import * as styles from '@/components/ConfigBackground/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigBackgroundBaseProps,
    ConfigBackgroundBase,
} from '@/components/ConfigBackground/Base';

export const ConfigBackgroundStyle = { ...styles } as const;

export type ConfigBackgroundStyles = keyof typeof ConfigBackgroundStyle;

export interface ConfigBackgroundProps extends ConfigBackgroundBaseProps {
    styleTemplate?: ConfigBackgroundStyles | ThemesType;
}

export const ConfigBackground = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigBackgroundProps) => {
    const Style = useMemo(
        () =>
            ConfigBackgroundStyle[styleTemplate as ConfigBackgroundStyles] ??
            ConfigBackgroundStyle._default,
        [styleTemplate]
    );

    return <ConfigBackgroundBase {...Style} {...props} />;
};
export default ConfigBackground;
