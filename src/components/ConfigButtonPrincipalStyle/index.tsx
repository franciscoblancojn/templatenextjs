import { useMemo } from 'react';

import * as styles from '@/components/ConfigButtonPrincipalStyle/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigButtonPrincipalStyleBaseProps,
    ConfigButtonPrincipalStyleBase,
} from '@/components/ConfigButtonPrincipalStyle/Base';

export const ConfigButtonPrincipalStyleStyle = { ...styles } as const;

export type ConfigButtonPrincipalStyleStyles =
    keyof typeof ConfigButtonPrincipalStyleStyle;

export interface ConfigButtonPrincipalStyleProps
    extends ConfigButtonPrincipalStyleBaseProps {
    styleTemplate?: ConfigButtonPrincipalStyleStyles | ThemesType;
}

export const ConfigButtonPrincipalStyle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigButtonPrincipalStyleProps) => {
    const Style = useMemo(
        () =>
            ConfigButtonPrincipalStyleStyle[
                styleTemplate as ConfigButtonPrincipalStyleStyles
            ] ?? ConfigButtonPrincipalStyleStyle._default,
        [styleTemplate]
    );

    return <ConfigButtonPrincipalStyleBase {...Style} {...props} />;
};
export default ConfigButtonPrincipalStyle;
