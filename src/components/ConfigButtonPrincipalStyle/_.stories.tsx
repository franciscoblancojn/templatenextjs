import { Story, Meta } from "@storybook/react";

import { ConfigButtonPrincipalStyleProps, ConfigButtonPrincipalStyle } from "./index";

export default {
    title: "ConfigButtonPrincipalStyle/ConfigButtonPrincipalStyle",
    component: ConfigButtonPrincipalStyle,
} as Meta;

const ConfigButtonPrincipalStyleIndex: Story<ConfigButtonPrincipalStyleProps> = (args) => (
    <ConfigButtonPrincipalStyle {...args}>Test Children</ConfigButtonPrincipalStyle>
);

export const Index = ConfigButtonPrincipalStyleIndex.bind({});
Index.args = {};
