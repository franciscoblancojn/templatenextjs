import { Text, TextStyles } from '@/components/Text';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

import { useData } from 'fenextjs-hook/cjs/useData';
import { Popup, PopupStyles } from '../Popup';
import { Collapse, CollapseStyles } from '../Collapse';

import Color from '@/svg/Color';
import Font from '@/svg/Font';
import Border from '@/svg/Border';
import {
    RadioBorderRadiusButton,
    RadioBorderRadiusButtonStyles,
} from '../Input/RadioBorderRadiusButton';
import {
    InputSelectBackground,
    InputSelectBackgroundStyles,
} from '../Input/SelectBackground';
import {
    ButtonPrincipalConfig,
    ButtonPrincipalConfigType,
} from '@/interfaces/Button';
import ContentWidth from '../ContentWidth';
import InputRange from '../Input/Range';
import ButtonRsView from '../ButtonRsView';
import { InputColor, InputColorStyles } from '../Input/Color';
import { ConfigBorder, ConfigBorderStyles } from '../Input/ConfigBorder';
import { useEffect, useMemo, useState } from 'react';
import { RS_All } from '@/data/components/RS';
import { Reload } from '@/svg/Reload';
import LoaderPage from '../Loader/LoaderPage';

export interface ConfigButtonPrincipalStyleClassProps {
    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;

    styleTemplateCollapse?: CollapseStyles | ThemesType;

    styleTemplateRadioBorderButton?: RadioBorderRadiusButtonStyles | ThemesType;
    styleTemplateInputSelectBackground?:
        | InputSelectBackgroundStyles
        | ThemesType;

    classNameContentExample?: string;
    classNameContentExampleText?: string;

    styleTemplateInputColor?: InputColorStyles | ThemesType;
    styleTemplateTitleInput?: TextStyles | ThemesType;
    styleTemplateConfigBorder?: ConfigBorderStyles | ThemesType;

    classNameReload?: string;
}

export interface ConfigButtonPrincipalStyleBaseProps {
    defaultValue?: ButtonPrincipalConfig;
    onChange?: (data: ButtonPrincipalConfig) => void;
}

export interface ConfigButtonPrincipalStyleProps
    extends ConfigButtonPrincipalStyleClassProps,
        ConfigButtonPrincipalStyleBaseProps {}

export const ConfigButtonPrincipalStyleBase = ({
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',

    styleTemplateCollapse = Theme?.styleTemplate ?? '_default',

    styleTemplateRadioBorderButton = Theme?.styleTemplate ?? '_default',
    styleTemplateInputSelectBackground = Theme?.styleTemplate ?? '_default',

    styleTemplateInputColor = Theme?.styleTemplate ?? '_default',

    styleTemplateTitleInput = Theme?.styleTemplate ?? '_default',

    styleTemplateConfigBorder = Theme?.styleTemplate ?? '_default',

    classNameContentExample = '',
    classNameContentExampleText = '',

    classNameReload = '',

    defaultValue = {
        color: '#ffffff',
        background: {
            gradient: {
                color1: '#04506c',
                deg: 130,
                color2: '#9d016e',
            },
            type: 'gradient',
        },
        padding: 5,
        size: 15,
        borderRadius: 'rounded',
        border: {
            color: 'white',
            size: 0,
            type: 'solid',
        },
    },
    onChange,
}: ConfigButtonPrincipalStyleProps) => {
    const _t = useLang();

    const [loader, setLoader] = useState(false);
    const [isActive, setIsActive] = useState<ButtonPrincipalConfigType | ''>(
        ''
    );
    const defaultValue_ = useMemo(() => defaultValue, []);
    const { data, onChangeData, onRestart } = useData<ButtonPrincipalConfig>(
        defaultValue_,
        {
            onChangeDataAfter: (data: ButtonPrincipalConfig) => {
                onChange?.(data);
            },
        }
    );
    const [i, setI] = useState(0);
    const rs_list = useMemo(() => RS_All.filter((r) => r.svg), [RS_All]);

    const Icon = useMemo(() => rs_list[i].svg, [i, rs_list]);

    const changeITime = () => {
        setI((pre) => (pre >= rs_list.length - 1 ? 0 : pre + 1));
        setTimeout(() => {
            changeITime();
        }, 2000);
    };

    useEffect(() => {
        changeITime();
    }, []);

    const content = useMemo(() => {
        if (loader) {
            return <LoaderPage />;
        }
        return (
            <>
                <div className={classNameContentExample}>
                    <span className={classNameContentExampleText}>
                        {_t('Vista Previa')}
                    </span>
                    <ButtonRsView config={data} Icon={Icon} />
                </div>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Size')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'size'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'size' : '');
                    }}
                >
                    <div className="p-h-15 p-v-15">
                        <InputRange
                            defaultValue={data.size}
                            value={data.size}
                            useValue={true}
                            min={15}
                            max={40}
                            onChange={onChangeData('size')}
                        />
                    </div>
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Padding')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'padding'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'padding' : '');
                    }}
                >
                    <div className="p-h-15 p-v-15">
                        <InputRange
                            defaultValue={data.padding}
                            value={data.padding}
                            useValue={true}
                            min={0}
                            max={10}
                            onChange={onChangeData('padding')}
                        />
                    </div>
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Border size={15} />
                            {_t('Button Border')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'border'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'border' : '');
                    }}
                >
                    <Text
                        styleTemplate={styleTemplateTitleInput}
                        className="p-h-15"
                    >
                        {_t('Border Round')}
                    </Text>
                    <ContentWidth size={300} className="m-h-auto">
                        <RadioBorderRadiusButton
                            onChange={onChangeData('borderRadius')}
                            styleTemplate={styleTemplateRadioBorderButton}
                            defaultValue={data.borderRadius}
                            value={data.borderRadius}
                            useValue={true}
                        />
                    </ContentWidth>
                    <ConfigBorder
                        defaultValue={data.border}
                        value={data.border}
                        useValue={true}
                        onChange={onChangeData('border')}
                        styleTemplate={styleTemplateConfigBorder}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Color size={15} />
                            {_t('Icon Color')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'icon'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'icon' : '');
                    }}
                >
                    <InputColor
                        defaultValue={data.color}
                        onChange={onChangeData('color')}
                        styleTemplate={styleTemplateInputColor}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Color size={15} />
                            {_t('Button Background')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'bg'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'bg' : '');
                    }}
                >
                    <InputSelectBackground
                        title={null}
                        onChange={onChangeData('background')}
                        styleTemplate={styleTemplateInputSelectBackground}
                        useType={{
                            color: true,
                            gradient: true,
                            img: false,
                            video: false,
                        }}
                        defaultValue={data.background}
                        useOpacity={true}
                    />
                </Collapse>
            </>
        );
    }, [data, isActive, loader]);

    const onRestartData = async () => {
        setLoader(true);
        onRestart();
        await new Promise((r) => setTimeout(r, 500));
        setLoader(false);
    };

    return (
        <>
            <Popup
                btn={_t('Button Principal')}
                styleTemplate={styleTemplatePopup}
                contentBackExtra={
                    <>
                        <div
                            className={classNameReload}
                            onClick={onRestartData}
                        >
                            <Reload size={20} />
                        </div>
                    </>
                }
            >
                {content}
            </Popup>
        </>
    );
};
export default ConfigButtonPrincipalStyleBase;
