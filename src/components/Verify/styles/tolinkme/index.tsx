import { VerifyClassProps } from '@/components/Verify/Base';

export const tolinkme: VerifyClassProps = {
    classNameContent: `
        font-style-italic
        color-pinkishGrey
    `,
    classNameConetentIcon: `
        m-h-5
        flex flex-align-center
    `,
    classNameVerify: `
        flex
        flex-align-center 
        font-w-400
        flex-nowrap
        font-style-italic
    `,
    classNameNotVerify: `
        flex
        flex-align-center 
        font-w-400
        flex-nowrap
        font-style-italic
    `,
};
