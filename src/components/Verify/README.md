# Verify

## Import

```js
import { Verify, VerifyStyles } from '@/components/Verify';
```

## Props

```tsx
interface VerifyProps {
    styleTemplate?: VerifyStyles;
    verify?: boolean;
}
```

## Use

```js
<Verify verify={true} />
```
