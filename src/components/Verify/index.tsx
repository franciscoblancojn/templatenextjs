import { useMemo } from 'react';

import * as styles from '@/components/Verify/styles';

import { Theme, ThemesType } from '@/config/theme';

import { VerifyBaseProps, VerifyBase } from '@/components/Verify/Base';

export const VerifyStyle = { ...styles } as const;

export type VerifyStyles = keyof typeof VerifyStyle;

export interface VerifyProps extends VerifyBaseProps {
    styleTemplate?: VerifyStyles | ThemesType;
}

export const Verify = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: VerifyProps) => {
    const Style = useMemo(
        () =>
            VerifyStyle[styleTemplate as VerifyStyles] ?? VerifyStyle._default,
        [styleTemplate]
    );

    return <VerifyBase {...Style} {...props} />;
};
export default Verify;
