import { Story, Meta } from "@storybook/react";

import { VerifyProps, Verify } from "./index";

export default {
    title: "Verify/Verify",
    component: Verify,
} as Meta;

const VerifyIndex: Story<VerifyProps> = (args) => (
    <Verify {...args}>Test Children</Verify>
);

export const Index = VerifyIndex.bind({});
Index.args = {};
