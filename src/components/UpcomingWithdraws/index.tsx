import { useMemo } from 'react';

import * as styles from '@/components/UpcomingWithdraws/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    UpcomingWithdrawsBaseProps,
    UpcomingWithdrawsBase,
} from '@/components/UpcomingWithdraws/Base';

export const UpcomingWithdrawsStyle = { ...styles } as const;

export type UpcomingWithdrawsStyles = keyof typeof UpcomingWithdrawsStyle;

export interface UpcomingWithdrawsProps extends UpcomingWithdrawsBaseProps {
    styleUpcomingWithdraws?: UpcomingWithdrawsStyles | ThemesType;
}

export const UpcomingWithdraws = ({
    styleUpcomingWithdraws = Theme?.styleTemplate ?? '_default',
    ...props
}: UpcomingWithdrawsProps) => {
    const Style = useMemo(
        () =>
            UpcomingWithdrawsStyle[
                styleUpcomingWithdraws as UpcomingWithdrawsStyles
            ] ?? UpcomingWithdrawsStyle._default,
        [styleUpcomingWithdraws]
    );

    return <UpcomingWithdrawsBase {...Style} {...props} />;
};
export default UpcomingWithdraws;
