import { UpcomingWithdrawsClassProps } from '@/components/UpcomingWithdraws/Base';

export const tolinkme: UpcomingWithdrawsClassProps = {
    classNameSubscriptionsIcon: `
        width-p-60
        m-auto
    `,
    classNameContentSubscriptions: `
        width-p-100
        m-h-5   
        m-v-30
    `,
    styleTemplateTitle: 'tolinkme27',
    classNameContentCardMoney: `
        flex
        flex-align-center
        flex-nowrap
    `,
    classNameContentIconCard: `
        flex
        m-r-10
    `,
    classNameContentTitle: `
        text-center
    `,
};
