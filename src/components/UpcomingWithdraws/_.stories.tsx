import { Story, Meta } from "@storybook/react";

import { UpcomingWithdrawsProps, UpcomingWithdraws } from "./index";

export default {
    title: "UpcomingWithdraws/UpcomingWithdraws",
    component: UpcomingWithdraws,
} as Meta;

const UpcomingWithdrawsIndex: Story<UpcomingWithdrawsProps> = (args) => (
    <UpcomingWithdraws {...args}>Test Children</UpcomingWithdraws>
);

export const Index = UpcomingWithdrawsIndex.bind({});
Index.args = {};
