import { Story, Meta } from "@storybook/react";

import { ToltipProps, Toltip } from "./index";

export default {
    title: "Toltip/Toltip",
    component: Toltip,
} as Meta;

const ToltipIndex: Story<ToltipProps> = (args) => (
    <Toltip {...args}>Test Children</Toltip>
);

export const Index = ToltipIndex.bind({});
Index.args = {
};
