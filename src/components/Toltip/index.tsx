import { useMemo } from 'react';

import * as styles from '@/components/Toltip/styles';

import { Theme, ThemesType } from '@/config/theme';

import { ToltipBaseProps, ToltipBase } from '@/components/Toltip/Base';

export const ToltipStyle = { ...styles } as const;

export type ToltipStyles = keyof typeof ToltipStyle;

export interface ToltipProps extends ToltipBaseProps {
    styleTemplate?: ToltipStyles | ThemesType;
}

export const Toltip = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ToltipProps) => {
    const Style = useMemo(
        () =>
            ToltipStyle[styleTemplate as ToltipStyles] ?? ToltipStyle._default,
        [styleTemplate]
    );

    return <ToltipBase {...Style} {...props} />;
};
export default Toltip;
