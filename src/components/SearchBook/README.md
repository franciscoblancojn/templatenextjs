# SearchBook

## Dependencies

[SearchBook](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/SearchBook)

```js
import { SearchBook } from '@/components/SearchBook';
```

## Import

```js
import { SearchBook, SearchBookStyles } from '@/components/SearchBook';
```

## Props

```tsx
interface SearchBookProps {}
```

## Use

```js
<SearchBook>SearchBook</SearchBook>
```
