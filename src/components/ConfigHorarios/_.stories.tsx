import { Story, Meta } from "@storybook/react";

import { ConfigHorariosProps, ConfigHorarios } from "./index";

export default {
    title: "ConfigHorarios/ConfigHorarios",
    component: ConfigHorarios,
} as Meta;

const ConfigHorariosIndex: Story<ConfigHorariosProps> = (args) => (
    <ConfigHorarios {...args}>Test Children</ConfigHorarios>
);

export const Index = ConfigHorariosIndex.bind({});
Index.args = {};
