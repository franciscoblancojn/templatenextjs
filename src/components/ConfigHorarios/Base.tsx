import Theme, { ThemesType } from '@/config/theme';
import { CompanyHorariosProps } from '@/interfaces/Company';
import { Days } from '@/interfaces/Date';
import { useMemo, useState } from 'react';
import { ConfigHorario, ConfigHorarioStyles } from '../ConfigHorario';

export interface ConfigHorariosClassProps {
    styleTemplateConfigHorario?: ConfigHorarioStyles | ThemesType;

    classNameContentDays?: string;
    classNameContentHorario?: string;
}

export interface ConfigHorariosBaseProps {
    defaultValue?: CompanyHorariosProps[];
    onChange?: (data: CompanyHorariosProps[]) => void;
}

export interface ConfigHorariosProps
    extends ConfigHorariosClassProps,
        ConfigHorariosBaseProps {}

export const ConfigHorariosBase = ({
    styleTemplateConfigHorario = Theme.styleTemplate ?? '_default',

    classNameContentDays = '',
    classNameContentHorario = '',

    defaultValue = [],
    onChange,
}: ConfigHorariosProps) => {
    const [value, setValue] = useState<CompanyHorariosProps[]>(defaultValue);

    const onChangeHorario = (i: number) => (data: CompanyHorariosProps) => {
        setValue((pre) => {
            pre[i] = data;
            onChange?.(pre);
            return pre;
        });
    };

    const CDays = useMemo(() => {
        return (
            <>
                {Days.map((day, i) => {
                    const defaultValue: CompanyHorariosProps = {
                        day,
                        ...(value.find((e) => e.day == day) ?? {}),
                    };

                    return (
                        <div
                            key={i}
                            className={`classNameContentHorario ${classNameContentHorario}`}
                        >
                            <ConfigHorario
                                key={i}
                                defaultValue={defaultValue}
                                onChange={onChangeHorario(i)}
                                styleTemplate={styleTemplateConfigHorario}
                            />
                        </div>
                    );
                })}
            </>
        );
    }, [value]);

    return (
        <>
            <div className={`classNameContentDays ${classNameContentDays}`}>
                {CDays}
            </div>
        </>
    );
};
export default ConfigHorariosBase;
