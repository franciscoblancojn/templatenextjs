import { useMemo } from 'react';

import * as styles from '@/components/Box/styles';

import { Theme, ThemesType } from '@/config/theme';

import { BoxBaseProps, BoxBase } from '@/components/Box/Base';

export const BoxStyle = { ...styles } as const;

export type BoxStyles = keyof typeof BoxStyle;

export interface BoxProps extends BoxBaseProps {
    styleTemplate?: BoxStyles | ThemesType;
}

export const Box = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: BoxProps) => {
    const Style = useMemo(
        () => BoxStyle[styleTemplate as BoxStyles] ?? BoxStyle._default,
        [styleTemplate]
    );

    return <BoxBase {...Style} {...props} />;
};
export default Box;
