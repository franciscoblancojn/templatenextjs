# Box

## Dependencies

[ContentWidth](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ContentWidth)

```js
import { ContentWidth } from '@/components/ContentWidth';
```

## Import

```js
import { Box, BoxStyles } from '@/components/Box';
```

## Props

```tsx
interface BoxProps {}
```

## Use

```js
<Box>Box</Box>
```
