import { BoxClassProps } from '@/components/Box/Base';

export const mooveri: BoxClassProps = {
    classNameContentBox: `
        bg-white
        box-shadow
        box-shadow-x-2 
        box-shadow-b-3
        box-shadow-black-16
        text-center

        m-0
        p-15
        p-v-34
        border-radius-12
        border-whiteSeven
        border-style-solid
        border-1

        width-p-100
    `,
    sizeContentBox: -1,
};

export const mooveriUser: BoxClassProps = {
    classNameContentBox: `
        bg-white
        box-shadow
        box-shadow-x-5
        box-shadow-y-5
        box-shadow-b-10
        box-shadow-black-16
        text-center
        p-h-15
        p-sm-h-22
        p-v-5
        p-sm-v-10
        border-radius-35
    `,
    sizeContentBox: 115,
};
