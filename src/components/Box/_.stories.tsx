import { Story, Meta } from "@storybook/react";

import { BoxProps, Box } from "./index";

export default {
    title: "Box/Box",
    component: Box,
} as Meta;

const BoxIndex: Story<BoxProps> = (args) => (
    <Box {...args}>Test Children</Box>
);

export const Index = BoxIndex.bind({});
Index.args = {};
