import { Story, Meta } from "@storybook/react";

import { StepsProps, Steps } from "./index";

export default {
    title: "Steps/Steps",
    component: Steps,
} as Meta;

const StepsIndex: Story<StepsProps> = (args) => (
    <Steps {...args}>Test Children</Steps>
);

export const Index = StepsIndex.bind({});
Index.args = {};
