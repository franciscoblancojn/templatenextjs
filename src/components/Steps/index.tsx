import { useMemo } from 'react';

import * as styles from '@/components/Steps/styles';

import { Theme, ThemesType } from '@/config/theme';

import { StepsBaseProps, StepsBase } from '@/components/Steps/Base';

export const StepsStyle = { ...styles } as const;

export type StepsStyles = keyof typeof StepsStyle;

export interface StepsProps extends StepsBaseProps {
    styleTemplate?: StepsStyles | ThemesType;
}

export const Steps = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: StepsProps) => {
    const Style = useMemo(
        () => StepsStyle[styleTemplate as StepsStyles] ?? StepsStyle._default,
        [styleTemplate]
    );

    return <StepsBase {...Style} {...props} />;
};
export default Steps;
