import { InfoProfileEditClassProps } from '@/components/InfoProfileEdit/Base';

export const tolinkme: InfoProfileEditClassProps = {
    classNameContent: `
        width-p-100
        flex
        flex-gap-row-15
        flex-sm-gap-50
        flex-sm-gap-column-50
    `,
    sizeContent: -1,
    classNameContentAvatarInfo: `
        flex
        flex-sm-gap-25
        flex-sm-gap-column-25
        flex-gap-15
        flex-gap-column-15
        flex-12 flex-md-6
        flex-justify-between
    `,
    classNameContentMoreInfo: `
        
    flex-12 flex-md-6
    `,

    classNameContentAvatar: `
        flex-4
        flex
        flex-column
        flex-align-center
        flex-gap-row-8
    `,
    classNameContentInfo: `
        flex-8
    `,
    classNameTextAvatar: `
        text-center
        d-none
        d-lg-block
    `,
    styleTemplateAvatar: 'tolinkme',
    styleTemplateTextAvatar: 'tolinkme2',

    classNameContentInputText: `
        m-b-15
    `,
    styleTemplateInputText: 'tolinkme2',

    classNameContentCheckboxImg: `
        pos-r
        pointer
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        border-radius-12
        p-10
        flex
        flex-align-center
        flex-justify-center
    `,
    styleContentCheckboxImg: {
        width: '4.5rem',
        height: '4.5rem',
    },
    classNameMaxLength: `
        text-right
        font-12
        color-warmGrey
    `,
    styleTemplateConfigButtonStyle: 'tolinkme',
    styleTemplateConfigInfoStyle: 'tolinkme',
    styleTemplateConfigButtonAnimationsStyles: 'tolinkme',
};

export const tolinkme2: InfoProfileEditClassProps = {
    classNameContent: `
        flex
        flex-column
        flex-align-center
    `,
    sizeContent: 375,
    classNameTextAvatar: 'd-none',
};
