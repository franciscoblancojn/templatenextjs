export const h1 = `
    font-34 font-nunito font-w-900
`;
export const h11 = `
    font-20 font-sm-25 font-md-32 font-nunito font-w-900
`;
export const h2 = `
    font-22 font-nunito font-w-700
`;
export const h3 = `
    font-21 font-nunito font-w-700
`;
export const h4 = `
    font-20 font-nunito font-w-600
`;
export const h5 = `
    font-19 font-nunito font-w-500
`;
export const h6 = `
    font-18 font-nunito font-w-400
`;

export const h7 = `
    font-18 font-nunito font-w-400
`;
