export const h1 = `
    font-55 
    font-nunito
    font-w-900
    color-sea 
`;
export const h11 = `
    color-black
    font-25 font-montserrat font-w-700
`;
export const h2 = `
    font-22 
    font-nunito 
    font-w-700
    color-sea
`;
export const h3 = `
    font-21 
    font-nunito 
    font-w-700
    color-sea
`;
export const h4 = `
    font-20 
    font-nunito 
    font-w-600
    color-sea
`;
export const h5 = `
    font-19 
    font-nunito 
    font-w-500
    color-sea
`;
export const h6 = `
    font-18 
    font-nunito 
    font-w-400
    color-sea
`;

export const h7 = `
    font-28 
    font-nunito 
    font-w-900
    color-sea
`;
