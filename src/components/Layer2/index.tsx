import { useMemo } from 'react';

import * as styles from '@/components/Layer2/styles';

import { Theme, ThemesType } from '@/config/theme';

import { Layer2BaseProps, Layer2Base } from '@/components/Layer2/Base';

export const Layer2Style = { ...styles } as const;

export type Layer2Styles = keyof typeof Layer2Style;

export interface Layer2Props extends Layer2BaseProps {
    styleTemplate?: Layer2Styles | ThemesType;
}

export const Layer2 = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: Layer2Props) => {
    const Style = useMemo(
        () =>
            Layer2Style[styleTemplate as Layer2Styles] ?? Layer2Style._default,
        [styleTemplate]
    );

    return <Layer2Base {...Style} {...props} />;
};
export default Layer2;
