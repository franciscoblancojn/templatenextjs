import Padlock from '@/svg/padlock';
import Text from '../Text';
import { PropsWithChildren } from 'react';
import moneyStripe from '@/functions/moneyStripe';
import { useLang } from '@/lang/translate';

export interface Layer2ClassProps {
    classNameContent?: string;
    classNameMoney?: string;
    classNameImg?: string;
    classNameLayer2?: string;
}

export interface Layer2BaseProps extends PropsWithChildren {
    price?: any;
    subscribe?: string;
    period?: string;
    title?: any;
}

export interface Layer2Props extends Layer2ClassProps, Layer2BaseProps {}

export const Layer2Base = ({
    classNameContent = '',
    classNameMoney = '',
    classNameImg = '',
    subscribe = 'Subscribe',
    period = 'Month',
    price = 8.5,
    classNameLayer2 = '',
    title = '',
}: Layer2Props) => {
    const _t = useLang();

    return (
        <>
            <div className={`${classNameContent}  `}>
                <div className={classNameLayer2}>
                    <div className={classNameImg}>
                        <Padlock size={15} />
                    </div>
                    <div className="flex flex-align-center">
                        <Text styleTemplate={'tolinkme25'}>
                            <span className="text-capitalize p-r-4">
                                {' '}
                                {_t(title)}
                            </span>
                            {_t(subscribe)}
                        </Text>
                        <div className={classNameMoney}>
                            {moneyStripe(price)}
                        </div>
                        {subscribe == 'Flat_Rate' ? (
                            <Text
                                className="p-l-2"
                                styleTemplate={'tolinkme26'}
                            >
                                /<span className="p-l-1">{_t('Just one')}</span>
                            </Text>
                        ) : (
                            <Text
                                className="p-l-2"
                                styleTemplate={'tolinkme26'}
                            >
                                / <span className="p-l-1">{_t(period)}</span>
                            </Text>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default Layer2Base;
