import { Story, Meta } from "@storybook/react";

import { Categories, CategoriesProps } from "./index";

export default {
    title: "Category/Categories",
    component: Categories,
} as Meta;

const TemplateCategories: Story<CategoriesProps> = (args) => <Categories {...args} />;

export const Index = TemplateCategories.bind({});
Index.args = {
};
