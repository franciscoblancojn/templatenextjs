import { useMemo } from 'react';

import * as styles from '@/components/Categories/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    CategoriesBaseProps,
    CategoriesBase,
} from '@/components/Categories/Base';

export const CategoriesStyle = { ...styles } as const;

export type CategoriesStyles = keyof typeof CategoriesStyle;

export interface CategoriesProps extends CategoriesBaseProps {
    styleTemplate?: CategoriesStyles | ThemesType;
}

export const Categories = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: CategoriesProps) => {
    const Style = useMemo(
        () =>
            CategoriesStyle[styleTemplate as CategoriesStyles] ??
            CategoriesStyle._default,
        [styleTemplate]
    );

    return <CategoriesBase {...Style} {...props} />;
};
export default Categories;
