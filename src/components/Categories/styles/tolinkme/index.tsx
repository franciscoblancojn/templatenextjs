import { CategoriesClassProps } from '@/components/Categories/Base';

export const tolinkme: CategoriesClassProps = {
    classNameContent: `
        flex
        flex-gap-column-7
        flex-gap-row-7
    `,
    sizeContent: 350,
};

export const tolinkme2: CategoriesClassProps = {
    classNameContent: `
        flex
        flex-gap-column-7
        flex-gap-row-7
        m-b-20
    `,
    styleTemplateCheck: 'tolinkme8',
    sizeContent: 350,
};
