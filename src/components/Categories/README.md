# Categories

## Dependencies

[DataCategory](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/interfaces/Category)

```js
import { DataCategory } from '@/interfaces/Category';
```

## Import

```js
import { Categories, CategoriesProps } from '@/components/Categories';
```

## Props

```tsx
interface CategoriesSelectedProps {
    styleTemplate?: CategoriesStyles;
    defaultCategories?: string[];
    onChange?: (data: DataCategory[]) => void;
    disabled?: boolean;
}
```

## Use

```js
<Categories
    defaultCategories={[]}
    onChange={(data: DataCategory[]) => {}}
    disabled={false}
/>
```
