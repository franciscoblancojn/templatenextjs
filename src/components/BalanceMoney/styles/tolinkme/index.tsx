import { BalanceMoneyClassProps } from '@/components/BalanceMoney/Base';

export const tolinkme: BalanceMoneyClassProps = {
    classNameContent: `
        bg-white
        color-black
        box-shadow 
        box-shadow-x-0 
        box-shadow-y-3 
        box-shadow-b-6 
        box-shadow-s-0 
        box-shadow-black-16 
        m-0
        p-h-12
        p-v-16
        border-radius-15
        border-1
        border-style-solid
        border-whiteFive
        width-p-100
        flex
        flex-align-center
        flex-justify-between
        column-gap-5
        flex-nowrap
    `,
    classNameContentCol1: `
        width-p-40
        flex
        flex-column
        column-gap-10
        
    `,
    classNameContentTitleBalance: `
        
    `,
    styleTemplateNumber: 'tolinkme17',
    styleTemplateTitleBalance: 'tolinkme18',
    styleTemplateNumberPending: 'tolinkme17',
    styleTemplateTitlePending: 'tolinkme18',

    classNameContentCol2: `
        width-p-55
        flex
        flex-column
        column-gap-10
        p-l-5
    `,
    classNameContentTitlePendingBalance: `
        flex
        flex-nowrap
        flex-align-center
        column-gap-5
        
    `,
    classNameContentIcon: `
        flex
    `,
    line: `
        width-p-1
        border-style-solid
        border-l-1
        border-0
        height-70
        border-warmGreyFive

    `,
    classNameContentNumberPendingBalance: `
        flex flex-nowrap
    `,
};
