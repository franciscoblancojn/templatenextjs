import { useMemo } from 'react';

import * as styles from '@/components/BalanceMoney/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    BalanceMoneyBaseProps,
    BalanceMoneyBase,
} from '@/components/BalanceMoney/Base';

export const BalanceMoneyStyle = { ...styles } as const;

export type BalanceMoneyStyles = keyof typeof BalanceMoneyStyle;

export interface BalanceMoney extends BalanceMoneyBaseProps {
    styleBalanceMoney?: BalanceMoneyStyles | ThemesType;
}

export const BalanceMoney = ({
    styleBalanceMoney = Theme?.styleTemplate ?? '_default',
    ...props
}: BalanceMoney) => {
    const Style = useMemo(
        () =>
            BalanceMoneyStyle[styleBalanceMoney as BalanceMoneyStyles] ??
            BalanceMoneyStyle._default,
        [styleBalanceMoney]
    );

    return <BalanceMoneyBase {...Style} {...props} />;
};
export default BalanceMoney;
