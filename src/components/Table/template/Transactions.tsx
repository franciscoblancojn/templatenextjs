import { Table, TableProps } from '@/components/Table';
import { TableHeader } from '@/components/Table/Base';
import url from '@/data/routes';
import money from '@/functions/money';
import { TransactionProps } from '@/interfaces/Transaction';

export interface TableItemTransactionsProps extends TransactionProps {}

export const TableHeaderTransactions: TableHeader<TableItemTransactionsProps> =
    [
        {
            id: 'id',
            th: 'ID',
            parse: (data: TableItemTransactionsProps) => {
                return (
                    <a
                        href={`${url.transactions.single}${data.id}`}
                        className={`color-currentColor`}
                    >
                        {data.id}
                    </a>
                );
            },
        },
        {
            id: 'user',
            th: 'User',
            parse: (data: TableItemTransactionsProps) => {
                return (
                    <a
                        href={`${url.users.single}${data.user.id}`}
                        className={`color-currentColor`}
                    >
                        {data.user.name}
                    </a>
                );
            },
        },
        {
            id: 'address',
            th: 'Address',
        },
        {
            id: 'price',
            th: 'Price',
            parse: (data: TableItemTransactionsProps) => {
                return money(data.price);
            },
        },
        {
            id: 'dateCreate',
            th: 'Date',
            parse: (data: TableItemTransactionsProps) => {
                return data.dateCreate.toDateString();
            },
        },
    ];

export interface TableTransactionsProps
    extends Omit<TableProps<TableItemTransactionsProps>, 'header'> {
    header?: null;
}

export const TableTransactions = ({ ...props }: TableTransactionsProps) => {
    return <Table {...props} header={TableHeaderTransactions} />;
};
export default TableTransactions;
