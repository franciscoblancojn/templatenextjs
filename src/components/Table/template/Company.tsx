import { Table, TableProps } from '@/components/Table';
import { TableHeader } from '@/components/Table/Base';
import url from '@/data/routes';
import { numberCount } from '@/functions/numberCount';
import { CompanyProps } from '@/interfaces/Company';

export interface TableItemCompanyProps extends CompanyProps {}

export const TableHeaderCompany: TableHeader<TableItemCompanyProps> = [
    {
        id: 'id',
        th: 'ID',
        parse: (data: TableItemCompanyProps) => {
            return (
                <a
                    href={`${url.companies.single}${data.id}`}
                    className={`color-currentColor`}
                >
                    {data.id}
                </a>
            );
        },
    },
    {
        id: 'name',
        th: 'Name',
        parse: (data: TableItemCompanyProps) => {
            return (
                <a
                    href={`${url.companies.single}${data.id}`}
                    className={`color-currentColor`}
                >
                    {data.name}
                </a>
            );
        },
    },
    {
        id: 'address',
        th: 'Address',
    },
    {
        id: 'moovings',
        th: 'Movings',
        parse: (data: TableItemCompanyProps) => {
            return numberCount(data.moovings);
        },
    },
    {
        id: 'dateCreate',
        th: 'Date',
        parse: (data: TableItemCompanyProps) => {
            return data.dateCreate.toDateString();
        },
    },
];

export interface TableCompanyProps
    extends Omit<TableProps<TableItemCompanyProps>, 'header'> {
    header?: null;
}

export const TableCompany = ({ ...props }: TableCompanyProps) => {
    return <Table {...props} header={TableHeaderCompany} />;
};
export default TableCompany;
