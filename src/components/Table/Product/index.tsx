import { Table, TableProps } from '@/components/Table';
import { TableHeader } from '@/components/Table/Base';

import { money } from '@/functions/money';

export interface TableItemProductProps {
    id: number;
    title: string;
    description: string;
    price: number;
    discountPercentage: number;
    rating: number;
    stock: number;
    brand: string;
    category: string;
    thumbnail: string;
    images: string[];
}

export const TableHeaderProduct: TableHeader<TableItemProductProps> = [
    {
        id: 'id',
        th: 'ID',
        parse: (data: TableItemProductProps) => {
            return <a href={`#${data.id}`}>{data.id}</a>;
        },
    },
    {
        id: 'title',
        th: 'Title',
        parse: (data: TableItemProductProps) => {
            return <a href={`#${data.id}`}>{data.title}</a>;
        },
    },
    {
        id: 'description',
        th: 'Description',
    },
    {
        id: 'price',
        th: 'Price',
        parse: (data: TableItemProductProps) => money(data.price),
    },
    {
        id: 'discountPercentage',
        th: 'Discount Percentage',
        parse: (data: TableItemProductProps) => data.discountPercentage + '%',
    },
    {
        id: 'rating',
        th: 'Rating',
    },
    {
        id: 'stock',
        th: 'Stock',
    },
    {
        id: 'brand',
        th: 'Brand',
    },
    {
        id: 'category',
        th: 'Category',
    },
    {
        id: 'thumbnail',
        th: 'Thumbnail',
        parse: (data: TableItemProductProps) => (
            <img src={data.thumbnail} alt={data.title} className="width-50" />
        ),
    },
    {
        id: 'images',
        th: 'Images',
        parse: (data: TableItemProductProps) =>
            data.images.map((image, i) => (
                <img
                    key={i}
                    src={image}
                    alt={data.title}
                    className="width-50"
                />
            )),
    },
];

export interface TableProductProps
    extends Omit<TableProps<TableItemProductProps>, 'header'> {
    header?: null;
}

export const TableProduct = ({ ...props }: TableProductProps) => {
    return <Table {...props} header={TableHeaderProduct} />;
};
export default TableProduct;
