import { TableClassProps } from '@/components/Table/Base';

export const _default: TableClassProps = {
    classNameContent: `
        
    `,
    classNameContentTable: `
        
    `,
    classNameTable: `
        border-1
        border-style-solid
        border-collapse-collapse
    `,
    classNameTHead: `
    `,
    classNameTh: `
        border-1
        border-style-solid 
        p-v-5
        p-h-15
        font-nunito
    `,
    classNameTd: `
        border-1
        border-style-solid 
        p-v-5
        p-h-15
        font-nunito
    `,
};
