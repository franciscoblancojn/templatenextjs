# Table

## Dependencies

[PaginationProps](https://gitlab.com/franciscoblancojn/fenextjs/-/tree/develop/src/components/Pagination)

```js
import { PaginationProps } from '@/components/Pagination';
```

## Import

```js
import { Table, TableStyles } from '@/components/Table';
```

## Props

```tsx
type TableHeader<T> = {
    id: keyof T;
    th: string;
    parse?: (data: T) => any;
}[];

interface TableProps<T> {
    styleTemplate?: TableStyles;
    items: T[];
    header: TableHeader<T>;
    pagination?: PaginationProps;
    loader?: boolean;
}
```

## Use

```js
<Table
    header={[
        {
            id: 'id',
            th: 'ID',
            parse: (data: TestItemTable) => {
                return <a href={`#${data.id}`}>{data.id}</a>;
            },
        },
        {
            id: 'name',
            th: 'Name',
            parse: (data: TestItemTable) => {
                return <a href={`#${data.id}`}>{data.name}</a>;
            },
        },
        {
            id: 'date',
            th: 'Date',
            parse: (data: TestItemTable) => data.date.toDateString(),
        },
        {
            id: 'dni',
            th: 'Documento',
        },
        {
            id: 'amount',
            th: 'Monto',
            parse: (data: TestItemTable) => money(data.amount),
        },
        {
            id: 'quanty',
            th: 'Cantidad',
        },
    ]}
    items={[
        {
            id: '1',
            name: 'Name',
            date: new Date('1-1-1'),
            dni: '0001',
            amount: 1000,
            quanty: 1,
        },
        {
            id: '1',
            name: 'Name',
            date: new Date('1-1-1'),
            dni: '0001',
            amount: 1000,
            quanty: 1,
        },
        {
            id: '1',
            name: 'Name',
            date: new Date('1-1-1'),
            dni: '0001',
            amount: 1000,
            quanty: 1,
        },
        {
            id: '1',
            name: 'Name',
            date: new Date('1-1-1'),
            dni: '0001',
            amount: 1000,
            quanty: 1,
        },
    ]}
    pagination={{
        nItems: 200,
    }}
    loader={true}
/>
```
