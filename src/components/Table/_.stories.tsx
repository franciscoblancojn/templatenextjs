import { Story, Meta } from "@storybook/react";

import { TableProps, Table } from "./index";

import { money } from "@/functions/money";
import {TableUser,TableUserProps } from "./template/User";

export default {
    title: "Table/Table",
    component: Table,
} as Meta;

interface TestItemTable {
    id: string;
    name: string;
    date: Date;
    dni: string;
    amount: number;
    quanty: number;
}

const TableIndex: Story<TableProps<TestItemTable>> = (args) => (
    <Table {...args}>Test Children</Table>
);

export const Index = TableIndex.bind({});
Index.args = {
    header: [
        {
            id: "id",
            th: "ID",
            parse: (data: TestItemTable) => {
                return <a href={`#${data.id}`}>{data.id}</a>;
            },
        },
        {
            id: "name",
            th: "Name",
            parse: (data: TestItemTable) => {
                return <a href={`#${data.id}`}>{data.name}</a>;
            },
        },
        {
            id: "date",
            th: "Date",
            parse: (data: TestItemTable) => data.date.toDateString(),
        },
        {
            id: "dni",
            th: "Documento",
        },
        {
            id: "amount",
            th: "Monto",
            parse: (data: TestItemTable) => money(data.amount),
        },
        {
            id: "quanty",
            th: "Cantidad",
        },
    ],
    items: [
        {
            id: "1",
            name: "Name",
            date: new Date("1-1-1"),
            dni: "0001",
            amount: 1000,
            quanty: 1,
        },
        {
            id: "1",
            name: "Name",
            date: new Date("1-1-1"),
            dni: "0001",
            amount: 1000,
            quanty: 1,
        },
        {
            id: "1",
            name: "Name",
            date: new Date("1-1-1"),
            dni: "0001",
            amount: 1000,
            quanty: 1,
        },
        {
            id: "1",
            name: "Name",
            date: new Date("1-1-1"),
            dni: "0001",
            amount: 1000,
            quanty: 1,
        },
    ],
    pagination:{
        nItems:200
    },
    loader:true
};


const TableUserIndex: Story<TableUserProps> = (args) => (
    <TableUser {...args}>Test Children</TableUser>
);

export const User = TableUserIndex.bind({});
User.args = {
    items: [
        {
            id: "1",
            name: "Name",
            email:"email@gmail.com",
            phone:"+57111111",
            status:"verify",
            dateCreate: new Date("1-1-1"),
        },
        {
            id: "1",
            name: "Name",
            email:"email@gmail.com",
            phone:"+57111111",
            status:"verify",
            dateCreate: new Date("1-1-1"),
        },
        {
            id: "1",
            name: "Name",
            email:"email@gmail.com",
            phone:"+57111111",
            status:"verify",
            dateCreate: new Date("1-1-1"),
        },
        {
            id: "1",
            name: "Name",
            email:"email@gmail.com",
            phone:"+57111111",
            status:"verify",
            dateCreate: new Date("1-1-1"),
        },
    ],
    pagination:{
        nItems:200
    },
    loader:true
};
