import { ConfigButtonAnimationsClassProps } from '@/components/ConfigButtonAnimations/Base';

export const tolinkme: ConfigButtonAnimationsClassProps = {
    styleTemplatePopup: 'tolinkme2',
    styleTemplateTitlePopup: 'tolinkme21',

    styleTemplateCollapse: 'tolinkme',

    styleTemplateSelectAnimation: 'tolinkmeSimple',

    classNameReload: `
        cursor-pointer
    `,
};
