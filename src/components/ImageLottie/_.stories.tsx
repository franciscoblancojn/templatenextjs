import { Story, Meta } from "@storybook/react";
import adinerado from "@/public/lottie/adinerado.json";

import { ImageLottieProps, ImageLottie } from "./index";

export default {
    title: "ImageLottie/ImageLottie",
    component: ImageLottie,
} as Meta;

const ImageLottieIndex: Story<ImageLottieProps> = (args) => (
    <ImageLottie {...args}>Test Children</ImageLottie>
);

export const Index = ImageLottieIndex.bind({});
Index.args = {
    img: adinerado
};
