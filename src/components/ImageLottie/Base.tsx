import Lottie from 'react-lottie-player';

export interface ImageLottieClassProps {}

export interface ImageLottieBaseProps {
    img: object;
    loop?: boolean;
    play?: boolean;
}

export interface ImageLottieProps
    extends ImageLottieClassProps,
        ImageLottieBaseProps {}

export const ImageLottieBase = ({
    loop = true,
    play = true,
    img,
}: ImageLottieProps) => {
    return (
        <div className="width-p-90 m-auto">
            <Lottie loop={loop} play={play} animationData={img} />
        </div>
    );
};
export default ImageLottieBase;
