import { Story, Meta } from "@storybook/react";

import { TestimonialProps, Testimonial } from "./index";

export default {
    title: "Testimonial/Testimonial",
    component: Testimonial,
} as Meta;

const TestimonialIndex: Story<TestimonialProps> = (args) => (
    <Testimonial {...args}>Test Children</Testimonial>
); 

export const Index = TestimonialIndex.bind({});
Index.args = {
    author: {
        name: "Sheryl Berge",
        position: "CEO at Lynch LLC",
        img: "https://www.aerocivil.gov.co/Style%20Library/CEA/img/01.jpg",
    },
    text: "The best part about TaxPal is every time I pay my employees, my bank balance doesn’t go down like it used to. Looking forward to spending this extra cash when I figure out why my card is being declined.",
};
