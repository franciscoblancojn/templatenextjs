import Theme, { ThemesType } from '@/config/theme';
import Text, { TextStyles } from '../Text';

export interface TestimonialClassProps {
    classNameContent?: string;
    classNameUsername?: string;
    classNameImg?: string;
    classNameHr?: string;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateAuthorName?: TextStyles | ThemesType;
    styleTemplateAuthorPosition?: TextStyles | ThemesType;
}

export interface TestimonialBaseProps {
    text: string;
    author: {
        name: string;
        position: string;
        img: string;
    };
}

export interface TestimonialProps
    extends TestimonialClassProps,
        TestimonialBaseProps {}

export const TestimonialBase = ({
    classNameContent = '',
    classNameUsername = '',
    classNameImg = '',
    classNameHr = '',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateAuthorName = Theme.styleTemplate ?? '_default',
    styleTemplateAuthorPosition = Theme.styleTemplate ?? '_default',

    author,
    text,
}: TestimonialProps) => {
    return (
        <>
            <div className={classNameContent}>
                <Text styleTemplate={styleTemplateText}>{text}</Text>
                <br />

                <hr className={classNameHr} />

                <div className={classNameUsername}>
                    <div>
                        <Text styleTemplate={styleTemplateAuthorName}>
                            {author.name}
                        </Text>
                        <Text styleTemplate={styleTemplateAuthorPosition}>
                            {author.position}
                        </Text>
                    </div>

                    <img
                        className={classNameImg}
                        src={author.img}
                        alt={author.name}
                    />
                </div>
            </div>
        </>
    );
};
export default TestimonialBase;
