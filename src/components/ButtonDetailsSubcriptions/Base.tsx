import { useState } from 'react';
import { useLang } from '@/lang/translate';
import ButtonSuscriptions from '../ButtonSuscriptions';
import { TolinkmeButtonsDataProps } from '../CurrentBalance/Base';
import ListSuscripcionBotton from '../ListSuscripcionBotton';
import ArrowGoBack from '@/svg/arrowGoBack';
import ContentWidth from '../ContentWidth';

export interface ButtonDetailsSubcriptionsClassProps {}

export interface ButtonDetailsSubcriptionsBaseProps {
    btn: TolinkmeButtonsDataProps;
    onSelect: (d: TolinkmeButtonsDataProps | undefined) => void;
    defaultActive?: boolean;
    icoBack?: any;
}

export interface ButtonDetailsSubcriptionsProps
    extends ButtonDetailsSubcriptionsClassProps,
        ButtonDetailsSubcriptionsBaseProps {}

export const ButtonDetailsSubcriptionsBase = ({
    btn,
    onSelect,
    icoBack = <ArrowGoBack size={20} />,
    defaultActive = false,
}: ButtonDetailsSubcriptionsProps) => {
    const [showList, setShowList] = useState(defaultActive);
    const _t = useLang();

    const handleButtonClick = () => {
        setShowList(true);
        onSelect(btn);
    };

    const handleBackButtonClick = () => {
        setShowList(false);
        onSelect(undefined);
    };

    return (
        <>
            <ContentWidth
                size={400}
                className={`${showList ? 'showList' : ''}`}
            >
                {!showList ? (
                    <ButtonSuscriptions
                        IconLogo={btn?.logo}
                        countSubscriptors={
                            btn.SubscriptableTransactions?.length ?? 0
                        }
                        amountButton={btn.ButtonPriceDetails?.basePrice}
                        month={btn.ButtonPriceDetails?.period}
                        titleButton={btn.title}
                        Subscriptors="Subscriptors"
                        onClick={handleButtonClick}
                    />
                ) : (
                    <div>
                        <button
                            onClick={handleBackButtonClick}
                            className="font-16
                            font-w-400
                            color-teal
                            text-decoration-underline
                            bg-transparent
                            border-0
                            font-nunito
                            cursor-pointer"
                        >
                            <span className="p-r-5">{icoBack}</span>
                            {_t('Back')}
                        </button>
                        <ListSuscripcionBotton
                            totalmountpricebalance={
                                btn.SubscriptableTransactions?.length ?? 0
                            }
                            subscriptionValue={
                                btn.ButtonPriceDetails?.basePrice
                            }
                            numSubscribers={
                                btn.SubscriptableTransactions?.length ?? 0
                            }
                            icon={btn.logo}
                            buttonName={btn.title}
                            emailSubscribers={btn.SubscriptableTransactions?.map(
                                (e) => e.user?.email
                            )}
                        />
                    </div>
                )}
            </ContentWidth>
        </>
    );
};

export default ButtonDetailsSubcriptionsBase;
