import { useMemo } from 'react';

import * as styles from '@/components/ButtonDetailsSubcriptions/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ButtonDetailsSubcriptionsBaseProps,
    ButtonDetailsSubcriptionsBase,
} from '@/components/ButtonDetailsSubcriptions/Base';

export const ButtonDetailsSubcriptionsStyle = { ...styles } as const;

export type ButtonDetailsSubcriptionsStyles =
    keyof typeof ButtonDetailsSubcriptionsStyle;

export interface ButtonDetailsSubcriptions
    extends ButtonDetailsSubcriptionsBaseProps {
    styleButtonDetailsSubcriptions?:
        | ButtonDetailsSubcriptionsStyles
        | ThemesType;
}

export const ButtonDetailsSubcriptions = ({
    styleButtonDetailsSubcriptions = Theme?.styleTemplate ?? '_default',
    ...props
}: ButtonDetailsSubcriptions) => {
    const Style = useMemo(
        () =>
            ButtonDetailsSubcriptionsStyle[
                styleButtonDetailsSubcriptions as ButtonDetailsSubcriptionsStyles
            ] ?? ButtonDetailsSubcriptionsStyle._default,
        [styleButtonDetailsSubcriptions]
    );

    return <ButtonDetailsSubcriptionsBase {...Style} {...props} />;
};
export default ButtonDetailsSubcriptions;
