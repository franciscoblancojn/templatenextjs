import { Popup, PopupStyles } from '../Popup';
import {
    InputSelectBackground,
    InputSelectBackgroundStyles,
} from '../Input/SelectBackground';
import { Collapse, CollapseStyles } from '../Collapse';
import { Theme, ThemesType } from '@/config/theme';
import { Text, TextStyles } from '@/components/Text';
import { Space } from '@/components/Space';

import { useLang } from '@/lang/translate';
import { useData } from 'fenextjs-hook/cjs/useData';
import { ConfigImgProfile } from '@/interfaces/ConfigImgProfile';
import InputRange, { InputRangeStyles } from '../Input/Range';
import {
    RadioBorderRadiusButton,
    RadioBorderRadiusButtonStyles,
} from '../Input/RadioBorderRadiusButton';
import { ImageProfile, ImageProfileStyles } from '../ImageProfile';
import {
    InputCheckbox,
    InputCheckboxStyles,
} from '@/components/Input/Checkbox';
import ContentWidth from '../ContentWidth';
import { useMemo, useState } from 'react';
import LoaderPage from '../Loader/LoaderPage';
import Reload from '@/svg/Reload';

export interface ConfigImgProfileClassProps {
    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;

    styleTemplateCollapse?: CollapseStyles | ThemesType;

    classNameContentExample?: string;
    classNameContentExampleText?: string;
    classNameContentExampleImg?: string;

    styleTemplateImageProfile?: ImageProfileStyles | ThemesType;

    styleTemplateRadioBorderRadiusButton?:
        | RadioBorderRadiusButtonStyles
        | ThemesType;

    classNameContentSw?: string;

    styleTemplateTitleBg?: TextStyles | ThemesType;

    styleTemplateCheckbox?: InputCheckboxStyles | ThemesType;

    styleTemplateBg?: InputSelectBackgroundStyles | ThemesType;

    classNameContentSize?: string;
    styleTemplateInputSize?: InputRangeStyles | ThemesType;
    styleTemplateTitleSize?: TextStyles | ThemesType;

    classNameReload?: string;
}

export interface ConfigImgProfileBaseProps {
    defaultValue?: ConfigImgProfile;
    onChange?: (data: ConfigImgProfile) => void;
    img?: string;
}

export interface ConfigImgProfileProps
    extends ConfigImgProfileClassProps,
        ConfigImgProfileBaseProps {}

export const ConfigImgProfileBase = ({
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',

    styleTemplateCollapse = Theme?.styleTemplate ?? '_default',

    classNameContentSw = '',

    styleTemplateTitleBg = Theme?.styleTemplate ?? '_default',

    styleTemplateCheckbox = Theme?.styleTemplate ?? '_default',

    classNameContentExample = '',
    classNameContentExampleText = '',
    classNameContentExampleImg = '',

    styleTemplateImageProfile = Theme.styleTemplate ?? '_default',

    styleTemplateRadioBorderRadiusButton = Theme.styleTemplate ?? '_default',

    styleTemplateBg = Theme?.styleTemplate ?? '_default',

    classNameContentSize = '',
    styleTemplateInputSize = Theme.styleTemplate ?? '_default',
    styleTemplateTitleSize = Theme.styleTemplate ?? '_default',

    classNameReload = '',

    img = '',
    defaultValue = {},
    onChange,
}: ConfigImgProfileProps) => {
    const _t = useLang();
    const [loader, setLoader] = useState(false);
    const defaultValue_ = useMemo(() => defaultValue, []);
    const { data, onChangeData, onRestart } = useData<ConfigImgProfile>(
        defaultValue_,
        {
            onChangeDataAfter: (data: ConfigImgProfile) => {
                onChange?.(data);
            },
        }
    );

    const content = useMemo(() => {
        if (loader) {
            return <LoaderPage />;
        }
        return (
            <>
                <div className={classNameContentExample}>
                    <span className={classNameContentExampleText}>
                        {_t('Vista Previa')}
                    </span>
                    <div
                        className={classNameContentExampleImg}
                        style={{ marginBottom: `-0.9375rem` }}
                    >
                        <ImageProfile
                            data={{
                                ...data,
                                useLogoTolinkme: false,
                            }}
                            img={img}
                            styleTemplate={styleTemplateImageProfile}
                        />
                    </div>
                </div>
                <Collapse
                    header={_t('Profile Image Style')}
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                >
                    <Text styleTemplate={styleTemplateTitleSize}>
                        {_t('Shape Type')}
                    </Text>
                    <ContentWidth size={300} className="m-h-auto">
                        <Space size={25} />
                        <RadioBorderRadiusButton
                            styleTemplate={styleTemplateRadioBorderRadiusButton}
                            onChange={onChangeData('borderType')}
                            defaultValue={data.borderType}
                        />
                    </ContentWidth>
                    <InputSelectBackground
                        title={'Border'}
                        defaultValue={data.bg}
                        onChange={onChangeData('bg')}
                        useType={{
                            color: true,
                            gradient: true,
                        }}
                        styleTemplate={styleTemplateBg}
                    />
                    <Text styleTemplate={styleTemplateTitleSize}>
                        {_t('Border Size')}
                    </Text>
                    <div className={classNameContentSize}>
                        <Text styleTemplate={styleTemplateTitleSize}>
                            {_t('Size')}
                        </Text>
                        <InputRange
                            min={0}
                            max={20}
                            defaultValue={data.borderSize}
                            onChange={onChangeData('borderSize')}
                            styleTemplate={styleTemplateInputSize}
                        />
                    </div>
                    <Space size={20} />
                    <div className={classNameContentSw}>
                        <Text styleTemplate={styleTemplateTitleBg}>
                            {_t('Mostrar Logo Tolinkme')}
                        </Text>
                        <InputCheckbox
                            styleTemplate={styleTemplateCheckbox}
                            defaultValue={data.useLogoTolinkme}
                            onChange={onChangeData('useLogoTolinkme')}
                        />
                    </div>
                    <Space size={20} />
                </Collapse>
            </>
        );
    }, [data, loader, img, _t]);

    const onRestartData = async () => {
        setLoader(true);
        onRestart();
        await new Promise((r) => setTimeout(r, 500));
        setLoader(false);
    };

    return (
        <>
            <Popup
                btn={_t('Profile Image')}
                styleTemplate={styleTemplatePopup}
                contentBackExtra={
                    <>
                        <div
                            className={classNameReload}
                            onClick={onRestartData}
                        >
                            <Reload size={20} />
                        </div>
                    </>
                }
            >
                {content}
            </Popup>
        </>
    );
};
export default ConfigImgProfileBase;
