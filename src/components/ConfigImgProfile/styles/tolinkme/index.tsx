import { ConfigImgProfileClassProps } from '@/components/ConfigImgProfile/Base';

export const tolinkme: ConfigImgProfileClassProps = {
    classNameContentSw: `
        flex
        flex-column
        flex-gap-row-10
    `,

    classNameContentExample: `
        flex
        flex-column
        row-gap-15
        flex-justify-center
        flex-align-center
        color-
        flex-nowrap
        
        font-nunito
        font-20
        font-w-900
        color-warmGrey
        
        pos-sk
        top-25
        bg-white-
        p-b-15
        z-index-5

        border-0
        border-b-1
        border-style-solid
        border-whiteTwo
        
        box-shadow
        box-shadow-c-white
        box-shadow-y--15

        bg-white
    `,
    classNameContentExampleText: `
    `,
    classNameContentExampleImg: `
        width-md-p-25  
        width-50
    `,
    styleTemplatePopup: 'tolinkme2',
    styleTemplateImageProfile: 'tolinkme',

    styleTemplateRadioBorderRadiusButton: 'tolinkmeColumn',
    styleTemplateTitlePopup: 'tolinkme21',
    styleTemplateCollapse: 'tolinkme',
    styleTemplateTitleBg: 'tolinkme13',
    styleTemplateCheckbox: 'tolinkme2',
    styleTemplateBg: 'tolinkmeItemSmall',
    classNameContentSize: `
        p-v-15
        p-h-15
        flex
        flex-column
        flex-gap-row-10
    `,
    styleTemplateInputSize: 'tolinkme',
    styleTemplateTitleSize: 'tolinkme13',
    classNameReload: `
        cursor-pointer
    `,
};
