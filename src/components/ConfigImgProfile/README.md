# ConfigImgProfile

## Dependencies

[ConfigImgProfile](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigImgProfile)

```js
import { ConfigImgProfile } from '@/components/ConfigImgProfile';
```

## Import

```js
import {
    ConfigImgProfile,
    ConfigImgProfileStyles,
} from '@/components/ConfigImgProfile';
```

## Props

```tsx
interface ConfigImgProfileProps {}
```

## Use

```js
<ConfigImgProfile>ConfigImgProfile</ConfigImgProfile>
```
