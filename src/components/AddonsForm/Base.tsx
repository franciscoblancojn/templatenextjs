import { AddonsForm } from '@/interfaces/Addons';
import { useLang } from '@/lang/translate';
import { useMemo } from 'react';
import InputCheckbox from '../Input/Checkbox';
import InputText from '../Input/Text';
import Text from '../Text';
import { useData } from 'fenextjs-hook/cjs/useData';

export interface AddonsFormClassProps {}

export interface AddonsFormBaseProps {
    defaultValue?: AddonsForm;
    onChange?: (data: AddonsForm | undefined) => void;
}

export interface AddonsFormProps
    extends AddonsFormClassProps,
        AddonsFormBaseProps {}

export const AddonsFormBase = ({ defaultValue, onChange }: AddonsFormProps) => {
    const _t = useLang();
    const { data, onChangeData } = useData<AddonsForm>(defaultValue || {}, {
        onChangeDataAfter: (d) => {
            if (onChange) {
                onChange(d);
            }
        },
    });

    const generateForm = useMemo(() => {
        return (
            <>
                <Text styleTemplate="tolinkme22" className="">
                    {_t('Write the name of the fields')}
                </Text>
                <div className="m-t-20">
                    <InputText
                        styleTemplate="tolinkme2"
                        label={_t('Title')}
                        placeholder={
                            _t('Title (Optional)') ?? data?.title_placeholder
                        }
                        onChange={onChangeData('title_placeholder')}
                        defaultValue={data?.title_placeholder}
                    />
                </div>
                <div className="m-t-20">
                    <InputText
                        styleTemplate="tolinkme2"
                        label={_t('Description')}
                        placeholder={
                            _t('Description (Optional)') ??
                            data?.description_placeholder
                        }
                        onChange={onChangeData('description_placeholder')}
                        defaultValue={data?.description_placeholder}
                    />
                </div>
                {data?.name && (
                    <div className="m-t-20">
                        <InputText
                            styleTemplate="tolinkme2"
                            label={_t('Name')}
                            placeholder={_t('Name') ?? data?.name_placeholder}
                            onChange={onChangeData('name_placeholder')}
                            defaultValue={data?.name_placeholder}
                        />
                    </div>
                )}

                {data?.email && (
                    <div className="m-t-20">
                        <InputText
                            styleTemplate="tolinkme2"
                            label={_t('Email')}
                            onChange={onChangeData('email_placeholder')}
                            placeholder={_t('Email') ?? data?.email_placeholder}
                            defaultValue={data?.email_placeholder}
                        />
                    </div>
                )}

                {data?.phone && (
                    <div className="m-t-20">
                        <InputText
                            styleTemplate="tolinkme2"
                            label={_t('Phone') ?? data?.phone_placeholder}
                            placeholder={_t('Phone')}
                            onChange={onChangeData('phone_placeholder')}
                            defaultValue={data?.phone_placeholder}
                        />
                    </div>
                )}

                {data?.message && (
                    <div className="m-t-20">
                        <InputText
                            styleTemplate="tolinkme2"
                            type="textarea"
                            label={_t('Message')}
                            placeholder={
                                _t('Message') ?? data?.message_placeholder
                            }
                            onChange={onChangeData('message_placeholder')}
                            defaultValue={data?.message_placeholder}
                        />
                    </div>
                )}

                <div className="m-t-20">
                    <InputText
                        styleTemplate="tolinkme2"
                        label={_t('Button')}
                        placeholder={_t('Button') ?? data?.button_placeholder}
                        onChange={onChangeData('button_placeholder')}
                        defaultValue={data?.button_placeholder}
                    />
                </div>
            </>
        );
    }, [data, onChange, defaultValue]);

    return (
        <>
            <div className="m-t-20">
                <Text styleTemplate="tolinkme22">
                    {_t('What information do you want to request?')}
                </Text>
                <InputCheckbox
                    label={_t('Email')}
                    styleTemplate={'tolinkme14'}
                    checked={data?.email === undefined || data?.email}
                    onChange={onChangeData('email')}
                    defaultValue={data?.email}
                />
                <InputCheckbox
                    label={_t('Name')}
                    styleTemplate={'tolinkme14'}
                    checked={data?.name === undefined || data?.name}
                    onChange={onChangeData('name')}
                    defaultValue={data?.name}
                />

                <InputCheckbox
                    label={_t('Phone')}
                    styleTemplate={'tolinkme14'}
                    checked={data?.phone === undefined || data?.phone}
                    onChange={onChangeData('phone')}
                    defaultValue={data?.phone}
                />
                <InputCheckbox
                    label={_t('Message')}
                    styleTemplate={'tolinkme14'}
                    checked={data?.message === undefined || data?.message}
                    onChange={onChangeData('message')}
                    defaultValue={data?.message}
                />
            </div>

            {generateForm}
        </>
    );
};
export default AddonsFormBase;
