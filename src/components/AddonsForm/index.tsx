import { useMemo } from 'react';

import * as styles from '@/components/AddonsForm/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    AddonsFormBaseProps,
    AddonsFormBase,
} from '@/components/AddonsForm/Base';

export const AddonsFormStyle = { ...styles } as const;

export type AddonsFormStyles = keyof typeof AddonsFormStyle;

export interface AddonsFormProps extends AddonsFormBaseProps {
    styleTemplate?: AddonsFormStyles | ThemesType;
}

export const AddonsForm = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: AddonsFormProps) => {
    const Style = useMemo(
        () =>
            AddonsFormStyle[styleTemplate as AddonsFormStyles] ??
            AddonsFormStyle._default,
        [styleTemplate]
    );

    return <AddonsFormBase {...Style} {...props} />;
};
export default AddonsForm;
