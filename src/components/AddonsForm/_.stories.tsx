import { Story, Meta } from "@storybook/react";

import { AddonsFormProps, AddonsForm } from "./index";

export default {
    title: "404/404",
    component: AddonsForm,
} as Meta;

const AddonsFormIndex: Story<AddonsFormProps> = (args) => (
    <AddonsForm {...args}>Test Children</AddonsForm>
);

export const Index = AddonsFormIndex.bind({});
Index.args = {};
