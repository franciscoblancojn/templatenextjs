import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';

import { SearchBookYup } from '@/validations/SearchBook';

import { useLang } from '@/lang/translate';

import { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import { GoogleStyles } from '@/components/Input/Google';

import { DataSearchBook } from '@/interfaces/SearchBook';
import { useData } from 'fenextjs-hook/cjs/useData';
import { GoogleAddres } from '@/interfaces/Google/addres';
import { SelectOptionProps } from '@/components/Input/Select';
import InputDateMoveSize from '@/components/Input/Select/Template/InputDateMoveSize';
import Text from '@/components/Text';
import { CheckBoxStairs } from '@/components/Input/Checkbox/Template/CheckBoxStairs';
import { CheckBoxStorage } from '@/components/Input/Checkbox/Template/CheckBoxStorage';
import { CheckBoxPacking } from '@/components/Input/Checkbox/Template/CheckBoxPacking';
import { InputCheckboxStyles } from '@/components/Input/Checkbox';
import InputDateMoveDay from '@/components/Input/Date/Template/InputDateMoveDay';
import InputNumber from '@/components/Input/Number';
import InputGoogleMovingFrom from '@/components/Input/Google/Template/InputGoogleMovingFrom';
import InputGoogleMovingTo from '@/components/Input/Google/Template/InputGoogleMovingTo';
import { ScanFile, ScanFileStyles } from '@/components/ScanFile';
import { InputFileDataProps } from '@/components/Input/File/Base';
import {
    ItemScanProps,
    ScanItemScanProps,
    ScanItemSocketProps,
} from '@/interfaces/Scan/ItemScan';
import InputScanIA from '@/components/Input/ScanIA';
import { Grid, GridProps } from '@/components/Grid';

export type DataTypeFormSearch =
    | string
    | Date
    | SelectOptionProps
    | GoogleAddres
    | boolean
    | number
    | InputFileDataProps[];

export interface FormSearchBookClassProps {
    classNameText?: string;

    styleTemplateCheckBox?: InputCheckboxStyles | ThemesType;
    styleTemplateInput?: DataSearchBook<
        InputTextStyles | GoogleStyles | ThemesType
    >;
    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateButtonScan?: ScanFileStyles | ThemesType;
    styleTemplateGoogle?: GoogleStyles | ThemesType;
    inputs?: DataSearchBook<boolean>;

    GridProps?: GridProps;
}

export type onSubmintSearchBook = (
    data: DataSearchBook<DataTypeFormSearch>
) => Promise<SubmitResult> | SubmitResult;

export interface FormSearchBookBaseProps {
    onSubmit?: onSubmintSearchBook;
    onProssesItemScan?: ScanItemScanProps;
    onSocketSend?: ScanItemSocketProps;
    socketData?: ItemScanProps[];
}

export interface FormSearchBookProps
    extends FormSearchBookClassProps,
        FormSearchBookBaseProps {}

export const FormSearchBookBase = ({
    classNameText = '',

    styleTemplateInput = {
        date: Theme.styleTemplate ?? '_default',
        moveSize: Theme.styleTemplate ?? '_default',
        movingFrom: Theme.styleTemplate ?? '_default',
        movingTo: Theme.styleTemplate ?? '_default',
        scan: Theme.styleTemplate ?? '_default',
        youStairs: Theme.styleTemplate ?? '_default',
        numberStairs: Theme.styleTemplate ?? '_default',
        doStorage: Theme.styleTemplate ?? '_default',
        doPacking: Theme.styleTemplate ?? '_default',
    },
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateButtonScan = Theme.styleTemplate ?? '_default',
    styleTemplateCheckBox = Theme.styleTemplate ?? '_default',

    GridProps = {},

    inputs = {
        date: true,
        moveSize: true,
        movingFrom: true,
        scan: true,
        youStairs: true,
        numberStairs: true,
        doStorage: true,
        doPacking: true,
    },

    ...props
}: FormSearchBookProps) => {
    const _t = useLang();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataSearchBook<DataTypeFormSearch>>(
        {}
    );

    const SearchBookYupV = useMemo(() => SearchBookYup(data, inputs), [data]);

    return (
        <>
            <Form<DataSearchBook<DataTypeFormSearch>>
                data={data}
                onSubmit={props?.onSubmit}
                disabled={disabled}
                onChangeDisable={setDisabled}
                loader={loader}
                onChangeLoader={setLoader}
                yup={SearchBookYupV.v}
            >
                <Grid {...GridProps}>
                    {inputs.date ? (
                        <div className="grid-A">
                            <InputDateMoveDay
                                label={_t('Move Day')}
                                onChange={onChangeData('date')}
                                styleTemplate={
                                    styleTemplateInput.date as InputTextStyles
                                }
                                yup={SearchBookYupV.y.date}
                            />
                        </div>
                    ) : (
                        <></>
                    )}
                    {inputs.moveSize ? (
                        <div className="grid-B">
                            <InputDateMoveSize
                                label={_t('Move Size')}
                                onChange={onChangeData('moveSize')}
                                styleTemplate={
                                    styleTemplateInput.moveSize as InputTextStyles
                                }
                                // yup={SearchBookYupV.y.moveSize}
                            />
                        </div>
                    ) : (
                        <></>
                    )}
                    {inputs.movingFrom ? (
                        <>
                            <div>
                                <InputGoogleMovingFrom
                                    placeholder="Search your address"
                                    label={_t('Moving From')}
                                    styleTemplate={
                                        styleTemplateInput.movingFrom as GoogleStyles
                                    }
                                    onChange={onChangeData('movingFrom')}
                                    yup={SearchBookYupV.y.movingFrom}
                                />
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                    {inputs.movingTo ? (
                        <>
                            <div>
                                <InputGoogleMovingTo
                                    placeholder="Search your address"
                                    label={_t('Moving To')}
                                    styleTemplate={
                                        styleTemplateInput.movingTo as GoogleStyles
                                    }
                                    onChange={onChangeData('movingTo')}
                                    yup={SearchBookYupV.y.movingTo}
                                />
                            </div>
                        </>
                    ) : (
                        <></>
                    )}

                    {inputs.scan ? (
                        <>
                            <div>
                                <div className="d-none d-lg-block">
                                    <ScanFile
                                        onChange={onChangeData('scan')}
                                        onProssesItemScan={
                                            props?.onProssesItemScan
                                        }
                                        styleTemplate={styleTemplateButtonScan}
                                    />
                                </div>
                                <div className="d-lg-none">
                                    <InputScanIA
                                        onChange={onChangeData('scan')}
                                        onSendImg={props?.onProssesItemScan}
                                        onSocketSend={props?.onSocketSend}
                                        socketData={props?.socketData}
                                    />
                                </div>
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                    <div>
                        <Text className={classNameText}>
                            {_t("Powered by Mooveri's Ai")}
                        </Text>
                    </div>
                    <Space size={25} />
                    <div>
                        <CheckBoxStairs
                            styleTemplate={styleTemplateCheckBox}
                            onChange={onChangeData('youStairs')}
                        />
                    </div>
                    {data.youStairs && inputs.numberStairs ? (
                        <>
                            <div>
                                <InputNumber
                                    label={_t('Your place has stairs?')}
                                    onChange={onChangeData('numberStairs')}
                                    styleTemplate={
                                        styleTemplateInput.numberStairs as InputTextStyles
                                    }
                                    yup={SearchBookYupV.y.numberStairs}
                                />
                            </div>
                        </>
                    ) : (
                        <></>
                    )}

                    <div>
                        <CheckBoxStorage
                            onChange={onChangeData('doStorage')}
                            styleTemplate={styleTemplateCheckBox}
                        />
                    </div>

                    <div>
                        <CheckBoxPacking
                            onChange={onChangeData('doPacking')}
                            styleTemplate={styleTemplateCheckBox}
                        />
                    </div>
                    <Space size={35} />
                    <div>
                        <Button
                            styleTemplate={styleTemplateButton}
                            disabled={disabled}
                            loader={loader}
                        >
                            {_t('Search')}
                        </Button>
                    </div>
                    <div>
                        <Text className={classNameText}>
                            {_t('**No charge will be made at this time')}
                        </Text>
                    </div>
                </Grid>
            </Form>
        </>
    );
};
export default FormSearchBookBase;
