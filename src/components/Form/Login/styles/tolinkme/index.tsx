import { FormLoginClassProps } from '@/components/Form/Login/Base';

export const tolinkme: FormLoginClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameContentInputs: `
        m-h-auto
    `,
    classNameLinkTextForgotPassword: `
        font-18
    `,
    classNameLinkRegister: `
    
    `,
    styleTemplateButton: 'tolinkme',
    styleTemplateLinkForgotPassword: 'tolinkme',
    styleTemplateLinkRegister: `tolinkme`,
    sizeContent: 300,
    titleProps: {
        className: `
            text-center
            color-white
        `,
    },
    inputs: {
        password: true,
        email: true,
    },
    rsLogin: null,
    styleTemplateInputs: {
        email: 'tolinkme',
        password: 'tolinkme',
    },
};
