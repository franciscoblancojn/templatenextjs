import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { InputTextStyles } from '@/components/Input/Text';
import { InputEmail } from '@/components/InputYup/Email';
import { InputPassword } from '@/components/InputYup/Password';
import { Title, TitleProps } from '@/components/Title';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { LinkForgotPassword, LinkRegister } from '@/components/Links';
import { ContentWidth } from '@/components/ContentWidth';

import { DataLogin } from '@/interfaces/Login';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { LoginYup } from '@/validations/Login';

import { UserLoginProps, useUser } from '@/hook/useUser';
import ButtonGoogle, { ButtonGoogleStyles } from '@/components/ButtonGoogle';
import ButtonFacebook from '@/components/ButtonFacebook';
import Theme, { ThemesType } from '@/config/theme';
import { LinkStyles } from '@/components/Link';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import { DataRsLogin } from '@/interfaces/RsLogin';
import { DataLinks } from '@/interfaces/Links';

export interface FormLoginClassProps {
    classNameContent?: string;
    classNameContentInputs?: string;
    classNameLinkTextForgotPassword?: string;
    classNameLinkRegister?: string;

    styleTemplateInputs?: DataLogin<InputTextStyles | ThemesType>;
    inputs?: DataLogin<boolean>;

    sizeContent?: number;
    sizeContentInputs?: number;

    rsLogin?: DataRsLogin<boolean> | null;

    stylesRsLogin?: {
        classNameContainer?: string;
        classNameContent?: string;
        sizeContent?: number;
        styleTemplateButtonGogle?: ButtonGoogleStyles | ThemesType;
        styleTemplateButtonFacebook?: ButtonGoogleStyles | ThemesType;
    };

    links?: DataLinks<boolean> | null;

    styleTemplateButton?: ButtonStyles | ThemesType;

    styleTemplateLinkForgotPassword?: LinkStyles | ThemesType;
    styleTemplateLinkRegister?: LinkStyles | ThemesType;

    linkForgotPassword?: string;
    linkRegister?: string;

    titleProps?: TitleProps;
}

export type onSubmintLogin = (
    data: DataLogin
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export interface FormLoginBaseProps {
    onSubmit: onSubmintLogin;
    urlRedirect?: string;
}

export interface FormLoginProps
    extends FormLoginBaseProps,
        FormLoginClassProps {}

export const FormLoginBase = ({
    classNameContent = '',
    classNameContentInputs = '',
    classNameLinkTextForgotPassword = '',
    classNameLinkRegister = '',

    sizeContent = 300,
    sizeContentInputs = 300,

    stylesRsLogin = {
        classNameContainer: '',
        classNameContent: '',
        sizeContent: -1,
        styleTemplateButtonGogle: Theme.styleTemplate ?? '_default',
        styleTemplateButtonFacebook: Theme.styleTemplate ?? '_default',
    },

    styleTemplateInputs = {
        email: Theme.styleTemplate ?? '_default',
        password: Theme.styleTemplate ?? '_default',
    },

    inputs = {
        email: true,
        password: true,
    },

    rsLogin = {
        google: true,
        facebook: true,
    },

    links = {
        ForgotPassword: true,
        Register: true,
    },

    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateLinkForgotPassword = Theme.styleTemplate ?? '_default',
    styleTemplateLinkRegister = Theme.styleTemplate ?? '_default',

    titleProps = {},
    ...props
}: FormLoginProps) => {
    const route = useRouter();
    const _t = useLang();
    const { updateUser } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataLogin>({});

    const LoginYupV = useMemo(() => LoginYup(data, inputs), [data]);

    const _onLogin = (data: SubmitResult<UserLoginProps>) => {
        updateUser(data.data);
        route.push(url.profile);
    };

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <Form<DataLogin, UserLoginProps>
                    id="login"
                    data={data}
                    onSubmit={props.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={LoginYupV}
                    onAfterSubmit={_onLogin}
                >
                    <Title {...titleProps}>{_t('Login')}</Title>
                    {rsLogin ? (
                        <>
                            <div className={stylesRsLogin.classNameContainer}>
                                <ContentWidth
                                    size={stylesRsLogin.sizeContent}
                                    className={stylesRsLogin.classNameContent}
                                >
                                    {rsLogin?.google ? (
                                        <ButtonGoogle
                                            styleTemplate={
                                                stylesRsLogin.styleTemplateButtonGogle
                                            }
                                        >
                                            {_t('Login with Google')}
                                        </ButtonGoogle>
                                    ) : (
                                        <></>
                                    )}
                                    {rsLogin?.facebook ? (
                                        <ButtonFacebook
                                            styleTemplate={
                                                stylesRsLogin.styleTemplateButtonFacebook
                                            }
                                        >
                                            {_t('Login with Facebook')}
                                        </ButtonFacebook>
                                    ) : (
                                        <></>
                                    )}
                                </ContentWidth>
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                    <Space size={12} />
                    <ContentWidth
                        size={sizeContentInputs}
                        className={classNameContentInputs}
                    >
                        {inputs.email ? (
                            <InputEmail
                                defaultValue={data.email}
                                placeholder={_t('Email')}
                                onChange={onChangeData('email')}
                                styleTemplate={styleTemplateInputs.email}
                            />
                        ) : (
                            <></>
                        )}
                        <Space size={6} />
                        {inputs.password ? (
                            <InputPassword
                                defaultValue={data.password}
                                placeholder={_t('Password')}
                                onChange={onChangeData('password')}
                                styleTemplate={styleTemplateInputs.password}
                            />
                        ) : (
                            <></>
                        )}
                        <Space size={12} />
                        <Button
                            styleTemplate={styleTemplateButton}
                            disabled={disabled}
                            loader={loader}
                        >
                            {_t('Login')}
                        </Button>
                        <Space size={15} />
                        {links ? (
                            <>
                                {links.ForgotPassword ? (
                                    <>
                                        <div className="text-center">
                                            <LinkForgotPassword
                                                styleTemplate={
                                                    styleTemplateLinkForgotPassword
                                                }
                                                className={
                                                    classNameLinkTextForgotPassword
                                                }
                                                href={props?.linkForgotPassword}
                                            />
                                        </div>
                                        <Space size={25} />
                                    </>
                                ) : (
                                    <></>
                                )}
                                {links.Register ? (
                                    <>
                                        <div className="text-center">
                                            <LinkRegister
                                                styleTemplate={
                                                    styleTemplateLinkRegister
                                                }
                                                className={
                                                    classNameLinkRegister
                                                }
                                                href={props?.linkRegister}
                                            />
                                        </div>
                                    </>
                                ) : (
                                    <></>
                                )}
                            </>
                        ) : (
                            <></>
                        )}
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormLoginBase;
