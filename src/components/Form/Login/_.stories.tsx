import { Story, Meta } from "@storybook/react";

import { FormLoginProps, FormLogin } from "./index";
import { DataLogin } from "@/interfaces/Login";
import log from "@/functions/log";

export default {
    title: "Form/Login",
    component: FormLogin,
} as Meta;

const Template: Story<FormLoginProps> = (args) => <FormLogin {...args} />;

export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataLogin)=> {
        log("DataLogin",data)
        await new Promise((r) => setTimeout(r, 2000));

        return {
            status: "ok",
            message: "register ok",
            data:{
                id:"1",
                name:"test",
                token:"1234"
            }
        };
    }
};

export const Error_ = Template.bind({});
Error_.args = {
    onSubmit:  async (data: DataLogin)=> {
        log("DataLogin",data)
        await new Promise((r) => setTimeout(r, 2000));
        throw {
            message : "Error"
        }
    }
};
