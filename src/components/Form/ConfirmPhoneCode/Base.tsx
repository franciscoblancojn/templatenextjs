import { useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { InputText, InputTextStyles } from '@/components/Input/Text/';
import { Title, TitleStyles, TitleStylesType } from '@/components/Title';
import { Text, TextStyles } from '@/components/Text';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';
import { Link, LinkStyles } from '@/components/Link';

import { DataCodePhone } from '@/interfaces/CodePhone';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { ValidateTelCode } from '@/validations/tel';

import { url } from '@/data/routes';
import Theme, { ThemesType } from '@/config/theme';
import { useRouter } from 'next/router';
import { UserLoginProps, useUser } from '@/hook/useUser';

export interface FormConfirmPhoneCodeClassProps {
    classNameContent?: string;
    classNameTitle?: string;
    classNameContentText?: string;
    classNameText?: string;
    classNameContentLink?: string;
    classNameLink?: string;

    typeStyleTemplateTitle?: TitleStylesType;
    styleTemplateTitle?: TitleStyles | ThemesType;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateInput?: InputTextStyles | ThemesType;
    styleTemplateLink?: LinkStyles | ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;
}

export type onSubmintConfirmPhoneCode = (
    data: DataCodePhone,
    user: UserLoginProps
) => Promise<SubmitResult> | SubmitResult;

export interface FormConfirmPhoneCodeBaseProps {
    onSubmit: onSubmintConfirmPhoneCode;
    token?: string;
    urlRedirect?: string;
    phone?: string;
}

export interface FormConfirmPhoneCodeProps
    extends FormConfirmPhoneCodeBaseProps,
        FormConfirmPhoneCodeClassProps {}

export const FormConfirmPhoneCodeBase = ({
    classNameContent = '',
    classNameTitle = '',
    classNameContentText = '',
    classNameText = '',
    classNameContentLink = '',
    classNameLink = '',

    typeStyleTemplateTitle = 'h1',

    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateInput = Theme.styleTemplate ?? '_default',
    styleTemplateLink = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',

    phone = '',
    ...props
}: FormConfirmPhoneCodeProps) => {
    const _t = useLang();
    const route = useRouter();
    const { user } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataCodePhone>({
        phone,
        code: '',
    });

    return (
        <>
            <ContentWidth size={330} className={classNameContent}>
                <Form<DataCodePhone>
                    id="confirm-phone-code"
                    data={data}
                    onSubmit={async (data: DataCodePhone) => {
                        return await props.onSubmit(data, user);
                    }}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={ValidateTelCode({
                        require: true,
                    })}
                    onAfterSubmit={() => {
                        route.push(props?.urlRedirect ?? url.index);
                    }}
                >
                    <Title
                        typeStyle={typeStyleTemplateTitle}
                        styleTemplate={styleTemplateTitle}
                        className={classNameTitle}
                    >
                        {_t('Confirm your Phone')}
                    </Title>
                    <Space size={15} />
                    <ContentWidth size={230} className={classNameContentText}>
                        <Text
                            styleTemplate={styleTemplateText}
                            className={classNameText}
                        >
                            {_t('Did you get an activation CODE?')}
                        </Text>
                    </ContentWidth>
                    <Space size={39} />
                    <InputText
                        defaultValue={data.code}
                        placeholder={_t('Your Code')}
                        onChange={onChangeData('code')}
                        styleTemplate={styleTemplateInput}
                    />
                    <Space size={7} />
                    <div className={classNameContentLink}>
                        <Link
                            className={classNameLink}
                            styleTemplate={styleTemplateLink}
                            href={url.confirmPhone.index}
                        >
                            {_t('Resend Again')}
                        </Link>
                    </div>
                    <Space size={39} />
                    <Button
                        styleTemplate={styleTemplateButton}
                        disabled={disabled}
                        loader={loader}
                    >
                        {_t('Confirm Activation Code')}
                    </Button>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormConfirmPhoneCodeBase;
