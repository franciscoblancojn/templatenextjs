import { useMemo } from 'react';

import * as styles from '@/components/Form/ConfirmPhoneCode/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormConfirmPhoneCodeBaseProps,
    FormConfirmPhoneCodeBase,
} from '@/components/Form/ConfirmPhoneCode/Base';

export const FormConfirmPhoneCodeStyle = { ...styles } as const;

export type FormConfirmPhoneCodeStyles = keyof typeof FormConfirmPhoneCodeStyle;

export interface FormConfirmPhoneCodeProps
    extends FormConfirmPhoneCodeBaseProps {
    styleTemplate?: FormConfirmPhoneCodeStyles | ThemesType;
}

export const FormConfirmPhoneCode = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormConfirmPhoneCodeProps) => {
    const Style = useMemo(
        () =>
            FormConfirmPhoneCodeStyle[
                styleTemplate as FormConfirmPhoneCodeStyles
            ] ?? FormConfirmPhoneCodeStyle._default,
        [styleTemplate]
    );

    return <FormConfirmPhoneCodeBase {...Style} {...props} />;
};
export default FormConfirmPhoneCode;
