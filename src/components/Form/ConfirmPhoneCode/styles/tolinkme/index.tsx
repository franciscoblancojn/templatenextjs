import { FormConfirmPhoneCodeClassProps } from '@/components/Form/ConfirmPhoneCode/Base';

export const tolinkme: FormConfirmPhoneCodeClassProps = {
    classNameContent: `
    m-h-auto
  `,
    classNameTitle: `
    text-center color-white
  `,
    styleTemplateTitle: 'tolinkme',
    typeStyleTemplateTitle: 'h1',
    classNameContentText: `
    m-h-auto
  `,
    classNameText: `
    text-center
  `,
    classNameContentLink: `
    text-right
  `,
    styleTemplateInput: 'tolinkme',
    styleTemplateText: 'tolinkme',
    styleTemplateButton: 'tolinkme',
    styleTemplateLink: 'tolinkme',
};
