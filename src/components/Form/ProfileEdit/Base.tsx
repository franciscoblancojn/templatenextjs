import { useMemo, useState } from 'react';

import { Space } from '@/components/Space';
import Form, { SubmitResult } from '@/components/Form/Base';

import { ProfileEditYup } from '@/validations/ProfileEdit';

import Theme, { ThemesType } from '@/config/theme';
import { DataProfile } from '@/interfaces/Profile';
import { useData } from 'fenextjs-hook/cjs/useData';
import { UserLoginProps, useUser } from '@/hook/useUser';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import ButtonEdit from '@/components/Button/Template/ButtonEdit';
import ContentWidth from '@/components/ContentWidth';
import { ButtonStyles } from '@/components/Button';

export interface FormProfileEditClassProps {
    classNameContent?: string;
    classNameContentInputs?: string;
    classNameContentInput?: string;
    classNameContentButton?: string;
    classNameContentButtons?: string;
    classNameBeforeContentButton?: string;
    classNameAfterContentButton?: string;
    classNameForm?: string;
    sizeInputCompanyName?: number;
    sizeInputLegalName?: number;
    styleTemplateInput?: DataProfile<InputTextStyles | ThemesType>;
    styleTemplateButton?: ButtonStyles | ThemesType;
    inputs?: DataProfile<boolean>;
}

export type onSubmitProfileDate = (
    user: UserLoginProps
) => (data: DataProfile) => Promise<SubmitResult> | SubmitResult;

export interface FormProfileEditBaseProps {
    defaultData?: DataProfile;
    onSubmit?: onSubmitProfileDate;
    token?: string;
    BeforeContentButton?: any;
    AfterContentButton?: any;
}

export interface FormProfileEditProps
    extends FormProfileEditClassProps,
        FormProfileEditBaseProps {}

export const FormProfileEditBase = ({
    styleTemplateInput = {
        companyName: Theme.styleTemplate ?? '_default',
        legalName: Theme.styleTemplate ?? '_default',
        description: Theme.styleTemplate ?? '_default',
    },
    classNameContent = '',
    styleTemplateButton = Theme.styleTemplate ?? '_default',

    inputs = {
        companyName: true,
        legalName: true,
        description: true,
    },
    classNameContentButton = '',
    classNameContentButtons = '',
    classNameBeforeContentButton = '',
    classNameAfterContentButton = '',
    classNameContentInputs = '',
    classNameContentInput = '',
    classNameForm = '',
    sizeInputCompanyName = 200,
    sizeInputLegalName = 100,

    defaultData = {},
    BeforeContentButton = <></>,
    AfterContentButton = <></>,
    ...props
}: FormProfileEditProps) => {
    const { user } = useUser();
    const [loader, setLoader] = useState<boolean>(false);
    const [status, setStatus] = useState<'edit' | 'cancel-save'>('edit');
    const [disabled, setDisabled] = useState<boolean>(true);
    const [dataPre, setDataPre] = useState<DataProfile>(defaultData);
    const { data, onChangeData, setData } = useData<DataProfile>(defaultData);

    const ProfileEditYupV = useMemo(() => ProfileEditYup(data, inputs), [data]);

    const onSave = async () => {
        setLoader(true);
        const result: SubmitResult | undefined = await props?.onSubmit?.(
            user
        )?.(data);
        setLoader(false);

        if (result?.status == 'ok') {
            setDataPre(data);
            setStatus('edit');
        }

        return (
            result ?? {
                status: 'ok',
            }
        );
    };

    return (
        <>
            <Form<DataProfile>
                data={data}
                onSubmit={onSave}
                disabled={disabled}
                onChangeDisable={setDisabled}
                loader={loader}
                onChangeLoader={setLoader}
                //yup={validate}
                className={classNameForm}
            >
                <div className={classNameContent}>
                    <div className={classNameContentInputs}>
                        {inputs.companyName ? (
                            <>
                                <ContentWidth
                                    className={classNameContentInput}
                                    size={sizeInputCompanyName}
                                >
                                    <InputText
                                        onChange={onChangeData('companyName')}
                                        placeholder={'Company Name'}
                                        styleTemplate={
                                            styleTemplateInput.companyName
                                        }
                                        yup={ProfileEditYupV.y.companyName}
                                        value={data.companyName ?? ''}
                                        disabled={status == 'edit'}
                                    />
                                    <Space size={5} />
                                </ContentWidth>
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.legalName ? (
                            <>
                                <ContentWidth
                                    className={classNameContentInput}
                                    size={sizeInputLegalName}
                                >
                                    <InputText
                                        placeholder={'Legal company name'}
                                        onChange={onChangeData('legalName')}
                                        styleTemplate={
                                            styleTemplateInput.legalName
                                        }
                                        yup={ProfileEditYupV.y.legalName}
                                        value={data.legalName ?? ''}
                                        disabled={status == 'edit'}
                                    />
                                    <Space size={5} />
                                </ContentWidth>
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.description ? (
                            <>
                                <InputText
                                    type="textarea"
                                    placeholder={'Description'}
                                    onChange={onChangeData('description')}
                                    styleTemplate={
                                        styleTemplateInput.description
                                    }
                                    yup={ProfileEditYupV.y.description}
                                    value={data.description ?? ''}
                                    disabled={status == 'edit'}
                                />
                            </>
                        ) : (
                            <></>
                        )}
                    </div>
                    <div className={classNameContentButtons}>
                        <div className={classNameBeforeContentButton}>
                            {BeforeContentButton}
                        </div>
                        <div className={classNameContentButton}>
                            <ButtonEdit
                                status={status}
                                onEdit={() => {
                                    setStatus('cancel-save');
                                }}
                                onCancel={() => {
                                    setData(dataPre);
                                    setStatus('edit');
                                }}
                                onSave={onSave}
                                loader={loader}
                                disabled={disabled}
                                styleTemplate={styleTemplateButton}
                            />
                        </div>
                        <div className={classNameAfterContentButton}>
                            {AfterContentButton}
                        </div>
                    </div>
                </div>
            </Form>
        </>
    );
};
export default FormProfileEditBase;
