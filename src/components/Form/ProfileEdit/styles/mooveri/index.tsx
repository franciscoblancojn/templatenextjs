import { FormProfileEditClassProps } from '@/components/Form/ProfileEdit/Base';

export const mooveri: FormProfileEditClassProps = {
    classNameContent: `
    flex
    m-h-auto
  `,
    styleTemplateInput: {
        companyName: 'mooveriProfileEdit',
        legalName: 'mooveriProfileNameDescription',
        description: 'mooveriProfileNameDescription',
    },
    inputs: {
        companyName: true,
        legalName: true,
        description: true,
    },
    classNameContentButtons: `
        width-p-40
        flex
        flex-align-start
        flex-nowrap
        flex-justify-right
        
    `,
    classNameContentInputs: `
    width-p-60
    `,
    sizeInputCompanyName: 400,
    sizeInputLegalName: 300,
    styleTemplateButton: 'mooveri',
    classNameForm: `
    width-p-100
    `,
};
