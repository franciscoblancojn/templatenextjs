import log from "@/functions/log";
import { DataProfile } from "@/interfaces/Profile";
import { Story, Meta } from "@storybook/react";

import { FormProfileEditInfProps, FormProfileEditInf } from "./index";

export default {
    title: "Form/ProfileEditInf",
    component: FormProfileEditInf,
} as Meta;

const Template: Story<FormProfileEditInfProps> = (args) => (
    <FormProfileEditInf {...args} />
);


export const Index = Template.bind({});
Index.args = {
    onSubmit: ()=> async (data: DataProfile)=> {
        log("DataProfile",data)
        await new Promise((r) => setTimeout(r, 2000));

        return {
            status: "ok",
            message: "register ok",
        };
    }
};
