import { SubmitResult } from '@/components/Form/Base';

import Theme, { ThemesType } from '@/config/theme';
import { DataProfile } from '@/interfaces/Profile';
import { UserLoginProps } from '@/hook/useUser';
import InputAvatar, { InputAvatarStyles } from '@/components/Input/Avatar';
import FormProfileEdit from '../ProfileEdit';
import ShareProfile from '@/components/ShareProfile';

export interface FormProfileEditInfClassProps {
    classNameContent?: string;

    styleTemplateInputAvatar?: InputAvatarStyles | ThemesType;
}

export type onSubmitProfileDate = (
    user: UserLoginProps
) => (data: DataProfile) => Promise<SubmitResult> | SubmitResult;

export interface FormProfileEditInfBaseProps {
    defaultData?: DataProfile;
    onSubmit?: onSubmitProfileDate;
    token?: string;
    img?: any;
}

export interface FormProfileEditInfProps
    extends FormProfileEditInfClassProps,
        FormProfileEditInfBaseProps {}

export const FormProfileEditInfBase = ({
    classNameContent = '',
    img,
    styleTemplateInputAvatar = Theme.styleTemplate ?? '_default',
    ...props
}: FormProfileEditInfProps) => {
    return (
        <>
            <div className={classNameContent}>
                <InputAvatar
                    defaultValue={img}
                    styleTemplate={styleTemplateInputAvatar}
                />
                <div className="width-p-100  flex p-h-15 p-md-h-30 p-v-30 p-t-50">
                    <FormProfileEdit
                        styleTemplate="mooveri"
                        onSubmit={props?.onSubmit}
                        defaultData={props?.defaultData}
                        BeforeContentButton={
                            <ShareProfile
                                name="Share"
                                styleTemplate="mooveri"
                            />
                        }
                    />
                </div>
            </div>
        </>
    );
};
export default FormProfileEditInfBase;
