import { FormProfileEditInfClassProps } from '@/components/Form/ProfileEditInf/Base';

export const mooveri: FormProfileEditInfClassProps = {
    classNameContent: `
    flex
    m-h-auto
    bg-white
    border-radius-14
    box-shadow 
    box-shadow-black-50 
    box-shadow-b-2

  `,
    styleTemplateInputAvatar: 'mooveriProfile',
};
