import { useMemo } from 'react';

import * as styles from '@/components/Form/ChangePassword/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormChangePasswordBaseProps,
    FormChangePasswordBase,
} from '@/components/Form/ChangePassword/Base';

export const FormChangePasswordStyle = { ...styles } as const;

export type FormChangePasswordStyles = keyof typeof FormChangePasswordStyle;

export interface FormChangePasswordProps extends FormChangePasswordBaseProps {
    styleTemplate?: FormChangePasswordStyles | ThemesType;
}

export const FormChangePassword = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormChangePasswordProps) => {
    const Style = useMemo(
        () =>
            FormChangePasswordStyle[
                styleTemplate as FormChangePasswordStyles
            ] ?? FormChangePasswordStyle._default,
        [styleTemplate]
    );

    return <FormChangePasswordBase {...Style} {...props} />;
};
export default FormChangePassword;
