import { FormChangePasswordClassProps } from '@/components/Form/ChangePassword/Base';

export const tolinkme: FormChangePasswordClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameTitle: `
        text-center color-white
    `,
    styleTemplateButton: 'tolinkme',
    typeStyleTemplateTitle: 'h1',
    styleTemplateTitle: 'tolinkme',
    inputs: {
        password: true,
        repeatPassword: true,
    },
    styleTemplateInputs: {
        password: 'tolinkme',
        repeatPassword: 'tolinkme',
    },
};
