import { FormChangePasswordClassProps } from '@/components/Form/ChangePassword/Base';

export const mooveri: FormChangePasswordClassProps = {
    classNameContent: `
        m-h-auto
        p-h-15
    `,
    classNameTitle: `
        text-center 
        color-sea
        font-28 
        font-w-900
    `,
    styleTemplateButton: 'mooveri',
    typeStyleTemplateTitle: 'h7',
    styleTemplateTitle: 'mooveri',
    inputs: {
        password: true,
        repeatPassword: false,
    },
    styleTemplateInputs: {
        password: 'mooveri',
        repeatPassword: 'mooveri',
    },
};
