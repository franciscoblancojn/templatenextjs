import { useMemo, useState } from 'react';

import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataMonetizeCategory } from '@/interfaces/MonetizeCategory';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { UserLoginProps, useUser } from '@/hook/useUser';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import InputCheckbox, {
    InputCheckboxStyles,
} from '@/components/Input/Checkbox';
import Button, { ButtonStyles } from '@/components/Button';
import { MonetizeCategoryYup } from '@/validations/MonetizeCategory';
import Categories, { CategoriesStyles } from '@/components/Categories';
import Theme, { ThemesType } from '@/config/theme';
import Link from 'next/link';
import { DataCategory } from '@/interfaces/Category';
export interface FormMonetizeCategoryClassProps {
    sizeContent?: number;
    classNameBtn?: string;
    classNameContent?: string;
    classNameContent2?: string;
    sizeContentInputs?: number;
    classNameContentInputs?: string;
    styleTemplateCategories?: CategoriesStyles | ThemesType;
    styleTemplateCheckbox?: InputCheckboxStyles | ThemesType;
    styleTemplateBtn?: ButtonStyles | ThemesType;
    inputs?: DataMonetizeCategory<boolean>;
    sizeContentButton?: number;
    classNamepolicyText2?: string;
    classNameContenCheckbox?: string;
}

export type onSubmintMonetizeCategory = (
    data: DataMonetizeCategory
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormMonetizeCategoryBaseProps {
    Next?: string;
    confirmText1?: string;
    confirmText2?: string;
    policyText1?: string;
    policyText2?: string;
    policyText3?: string;
    policyText4?: string;
    TermsOfService?: string;
    privacyPolicy?: string;
    confirmFollows?: string;
    textBtn?: string;
    onSubmit?: onSubmintMonetizeCategory;
    urlRediret?: string;
    onValidateUsername?: onValidateUsername;
    onClick?: (Next?: string) => void;
    defaultValue?: DataMonetizeCategory;
    onChange?: (data: DataMonetizeCategory) => void;

    extraLoader?: boolean;
}
export interface FormMonetizeCategoryProps
    extends FormMonetizeCategoryBaseProps,
        FormMonetizeCategoryClassProps {}

export const FormMonetizeCategoryBase = ({
    Next,
    confirmText1 = 'Click here to confirm that you agree and confirm, your activity ',
    confirmText2 = 'follows our community standars.',
    policyText1 = 'Click here to agree our ',
    policyText2 = 'Terms of service ',
    policyText3 = 'and our ',
    policyText4 = 'Privacy Policy.',
    classNamepolicyText2 = '',
    textBtn = 'Submit',
    classNameContenCheckbox = '',
    TermsOfService = url.term,
    privacyPolicy = url.servicePolicies,
    confirmFollows = url.follows,
    inputs = {
        categories: true,
        confirm: true,
        privacyPolicy: true,
    },
    classNameBtn = '',
    sizeContentButton = 200,
    classNameContent = '',
    classNameContentInputs = '',
    styleTemplateCategories = Theme?.styleTemplate ?? '_default',
    styleTemplateCheckbox = Theme?.styleTemplate ?? '_default',
    styleTemplateBtn = Theme?.styleTemplate ?? '_default',
    onClick,
    defaultValue = { confirm: false, privacyPolicy: false },
    onChange,

    extraLoader = undefined,
    ...props
}: FormMonetizeCategoryProps) => {
    const _t = useLang();
    const router = useRouter();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataMonetizeCategory>(defaultValue, {
        onChangeDataAfter: onChange,
    });
    const FormMonetizeCategoryYupV = useMemo(
        () => MonetizeCategoryYup(data, inputs),
        [data]
    );
    return (
        <>
            <ContentWidth className={classNameContent}>
                <Form<DataMonetizeCategory>
                    id="MonetizeCategory"
                    data={data}
                    onSubmit={props?.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={FormMonetizeCategoryYupV.v}
                    onAfterSubmit={(data) => {
                        onLogin(data);
                        router.push(props?.urlRediret ?? url.index);
                    }}
                >
                    <ContentWidth className={classNameContentInputs}>
                        <Space size={20} />

                        {inputs.categories ? (
                            <Categories
                                styleTemplate={styleTemplateCategories}
                                defaultCategories={(
                                    data.categories ?? []
                                )?.filter((e: DataCategory) => e.active)}
                                onChange={onChangeData('categories')}
                            />
                        ) : (
                            <></>
                        )}
                        <Space size={15} />

                        {inputs.confirm ? (
                            <InputCheckbox
                                defaultValue={data.confirm}
                                onChange={onChangeData('confirm')}
                                styleTemplate={styleTemplateCheckbox}
                                label={
                                    <>
                                        {_t(confirmText1)}
                                        <Link
                                            target={'_blank'}
                                            href={confirmFollows}
                                        >
                                            <a
                                                target={'_blank'}
                                                className={classNamepolicyText2}
                                            >
                                                {_t(confirmText2)}
                                            </a>
                                        </Link>
                                    </>
                                }
                            />
                        ) : (
                            <></>
                        )}

                        <div className={classNameContenCheckbox}>
                            <Space size={20} />
                            {inputs.privacyPolicy ? (
                                <InputCheckbox
                                    defaultValue={data.privacyPolicy}
                                    onChange={onChangeData('privacyPolicy')}
                                    styleTemplate={styleTemplateCheckbox}
                                    label={
                                        <>
                                            {_t(policyText1)}
                                            <Link
                                                target={'_blank'}
                                                href={TermsOfService}
                                            >
                                                <a
                                                    target={'_blank'}
                                                    className={
                                                        classNamepolicyText2
                                                    }
                                                >
                                                    {_t(policyText2)}
                                                </a>
                                            </Link>
                                            {_t(policyText3)}
                                            <Link
                                                target={'_blank'}
                                                href={privacyPolicy}
                                            >
                                                <a
                                                    target={'_blank'}
                                                    className={
                                                        classNamepolicyText2
                                                    }
                                                >
                                                    {_t(policyText4)}
                                                </a>
                                            </Link>
                                        </>
                                    }
                                />
                            ) : (
                                <></>
                            )}
                        </div>

                        <ContentWidth
                            size={sizeContentButton}
                            className={classNameBtn}
                        >
                            <Button
                                disabled={disabled}
                                loader={extraLoader ?? loader}
                                styleTemplate={styleTemplateBtn}
                                onClick={() => {
                                    onClick?.(Next);
                                }}
                            >
                                {_t(textBtn)}
                            </Button>
                        </ContentWidth>
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormMonetizeCategoryBase;
