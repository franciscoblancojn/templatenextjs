import { FormMonetizeCategoryClassProps } from '@/components/Form/MonetizeCategory/Base';

export const tolinkme: FormMonetizeCategoryClassProps = {
    classNameBtn: `
    pos-f
    bottom-20
    z-index-1
    width-p-100
    p-h-17
    left-0
    right-0
    m-auto
    flex
    flex-justify-center
  `,
    styleTemplateCategories: 'tolinkme2',
    styleTemplateCheckbox: 'tolinkme9',
    styleTemplateBtn: 'tolinkme6',
    classNameContent: `
    flex-align-center
    flex
    `,
    classNameContentInputs: `
    flex
    `,
    sizeContentButton: 400,
    classNamepolicyText2: `
      font-w-900
      text-decoration-underline
      color-black
    `,
    classNameContenCheckbox: `
      p-b-100
    `,
    inputs: {
        confirm: true,
        categories: true,
        privacyPolicy: true,
    },
};
