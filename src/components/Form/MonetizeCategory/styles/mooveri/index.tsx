import { FormMonetizeCategoryClassProps } from '@/components/Form/MonetizeCategory/Base';

export const mooveri: FormMonetizeCategoryClassProps = {
    classNameContent: `
        m-h-auto
    `,

    sizeContentInputs: 330,
    classNameContentInputs: `
        m-h-auto
        p-h-15
    `,
};
