import { Story, Meta } from "@storybook/react";

import { FormPersonalEditProps, FormPersonalEdit } from "./index";

export default {
    title: "Form/PersonalEdit",
    component: FormPersonalEdit,
} as Meta;

const Template: Story<FormPersonalEditProps> = (args) => <FormPersonalEdit {...args} />;

export const Index = Template.bind({});
Index.args = {
};
