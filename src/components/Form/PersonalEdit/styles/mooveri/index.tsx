import { FormPersonalEditClassProps } from '@/components/Form/PersonalEdit/Base';

export const mooveri: FormPersonalEditClassProps = {
    classNameContent: `
        m-h-auto
    `,
    ContentWidthBackTitleTextProps: {
        size: 323,
        className: `
            m-b-17
        `,
    },
    TitleProps: {
        styleTemplate: 'mooveri3',
        className: `
            m-t-8
            m-b-10
        `,
    },
    TextProps: {
        styleTemplate: 'mooveri4',
    },
    classNameContentInputs: `
        flex
        flex-md-gap-50
        flex-md-gap-column-50
        flex-gap-row-20
        flex-justify-between
    `,
    inputs: {
        firstName: {
            use: true,
            props: {
                styleTemplate: 'mooveri',
            },
            classNameContent: 'flex-12 flex-md-6',
        },
        lastName: {
            use: true,
            props: {
                styleTemplate: 'mooveri',
            },
            classNameContent: 'flex-12 flex-md-6',
        },
        phone: {
            use: true,
            props: {
                styleTemplate: 'mooveri',
            },
            classNameContent: 'flex-12 flex-md-6',
        },
        email: {
            use: true,
            props: {
                styleTemplate: 'mooveri',
            },
            classNameContent: 'flex-12 flex-md-6',
        },
    },
};
