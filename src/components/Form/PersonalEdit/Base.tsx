import { useState } from 'react';

import { Text, TextProps } from '@/components/Text';
import { SubmitResult } from '@/components/Form/Base';
import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';

import {
    DataPersonalEdit,
    DataPersonalEditInputs,
} from '@/interfaces/PersonalEdit';

import * as YupName from '@/validations/Name';
import * as YupEmail from '@/validations/Email';

import { useLang } from '@/lang/translate';

import { InputEdit, InputEditProps } from '@/components/Input/Edit';
import Theme from '@/config/theme';
import { UserLoginProps, useUser } from '@/hook/useUser';

export interface FormPersonalEditClassProps {
    classNameContent?: string;

    ContentWidthBackTitleTextProps?: ContentWidthProps;
    TitleProps?: TextProps;
    TextProps?: TextProps;

    classNameContentInputs?: string;

    inputs?: DataPersonalEdit<{
        use?: boolean;
        props?: InputEditProps;
        classNameContent?: string;
    }>;
}

export type onSubmintPersonalEdit = (
    data: DataPersonalEdit
) => Promise<SubmitResult> | SubmitResult;

export interface FormPersonalEditBaseProps {
    onSaveInput?: (
        user?: UserLoginProps
    ) => (
        id: DataPersonalEditInputs
    ) => (value: string) => Promise<SubmitResult> | SubmitResult;
    token?: string;
    defaultData?: DataPersonalEdit<string>;
}
export interface FormPersonalEditProps
    extends FormPersonalEditBaseProps,
        FormPersonalEditClassProps {}

export const FormPersonalEditBase = ({
    classNameContent = '',

    ContentWidthBackTitleTextProps = {},
    TitleProps = {},
    TextProps = {},

    classNameContentInputs = '',

    inputs = {
        firstName: {
            use: true,
            props: {
                styleTemplate: Theme.styleTemplate ?? '_default',
            },
        },
        lastName: {
            use: true,
            props: {
                styleTemplate: Theme.styleTemplate ?? '_default',
            },
        },
        phone: {
            use: true,
            props: {
                styleTemplate: Theme.styleTemplate ?? '_default',
            },
        },
        email: {
            use: true,
            props: {
                styleTemplate: Theme.styleTemplate ?? '_default',
            },
        },
    },

    ...props
}: FormPersonalEditProps) => {
    const _t = useLang();
    const { user } = useUser();
    const [activeEdit, setActiveEdit] = useState<
        DataPersonalEditInputs | 'all'
    >('all');

    return (
        <>
            <div className={classNameContent}>
                <ContentWidth {...ContentWidthBackTitleTextProps}>
                    <Text {...TitleProps}>{_t('Personal Information')}</Text>
                    <Text {...TextProps}>
                        {_t(
                            "Here you can manage your personal information, remember we don't share this information with anyone. It helps us to provide the best customer service experience."
                        )}
                    </Text>
                </ContentWidth>
                <div className={classNameContentInputs}>
                    {inputs?.firstName?.use ? (
                        <>
                            <div
                                className={
                                    inputs?.firstName?.classNameContent ?? ''
                                }
                            >
                                <InputEdit
                                    {...inputs?.firstName?.props}
                                    label={_t('First Name')}
                                    placeholder={_t('First Name')}
                                    defaultValue={props?.defaultData?.firstName}
                                    onSave={props?.onSaveInput?.(user)?.(
                                        'firstName'
                                    )}
                                    yup={YupName.firstName}
                                    activeEdit={
                                        activeEdit == 'all' ||
                                        activeEdit == 'firstName'
                                    }
                                    setStatus={(e) => {
                                        setActiveEdit(
                                            e == 'edit' ? 'all' : 'firstName'
                                        );
                                    }}
                                />
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                    {inputs?.lastName?.use ? (
                        <>
                            <div
                                className={
                                    inputs?.lastName?.classNameContent ?? ''
                                }
                            >
                                <InputEdit
                                    {...inputs?.lastName?.props}
                                    label={_t('Last Name')}
                                    placeholder={_t('Last Name')}
                                    defaultValue={props?.defaultData?.lastName}
                                    onSave={props?.onSaveInput?.(user)?.(
                                        'lastName'
                                    )}
                                    yup={YupName.lastName}
                                    activeEdit={
                                        activeEdit == 'all' ||
                                        activeEdit == 'lastName'
                                    }
                                    setStatus={(e) => {
                                        setActiveEdit(
                                            e == 'edit' ? 'all' : 'lastName'
                                        );
                                    }}
                                />
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                    {inputs?.phone?.use ? (
                        <>
                            <div
                                className={
                                    inputs?.phone?.classNameContent ?? ''
                                }
                            >
                                <InputEdit
                                    {...inputs?.phone?.props}
                                    label={_t('Phone')}
                                    placeholder={_t('Phone')}
                                    defaultValue={props?.defaultData?.phone}
                                    onSave={props?.onSaveInput?.(user)?.(
                                        'phone'
                                    )}
                                    yup={YupEmail.email}
                                    activeEdit={
                                        activeEdit == 'all' ||
                                        activeEdit == 'phone'
                                    }
                                    setStatus={(e) => {
                                        setActiveEdit(
                                            e == 'edit' ? 'all' : 'phone'
                                        );
                                    }}
                                    type="tel"
                                />
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                    {inputs?.email?.use ? (
                        <>
                            <div
                                className={
                                    inputs?.email?.classNameContent ?? ''
                                }
                            >
                                <InputEdit
                                    {...inputs?.email?.props}
                                    label={_t('Email')}
                                    placeholder={_t('Email')}
                                    defaultValue={props?.defaultData?.email}
                                    onSave={props?.onSaveInput?.(user)?.(
                                        'email'
                                    )}
                                    yup={YupEmail.email}
                                    activeEdit={
                                        activeEdit == 'all' ||
                                        activeEdit == 'email'
                                    }
                                    setStatus={(e) => {
                                        setActiveEdit(
                                            e == 'edit' ? 'all' : 'email'
                                        );
                                    }}
                                />
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                </div>
            </div>
        </>
    );
};
export default FormPersonalEditBase;
