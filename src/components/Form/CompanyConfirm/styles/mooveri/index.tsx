import { FormCompanyConfirmClassProps } from '@/components/Form/CompanyConfirm/Base';
import ArrowNext from '@/svg/arrowNext';
import ArrowPre from '@/svg/arrowPre';

export const mooveri: FormCompanyConfirmClassProps = {
    classNameContentButtonPre: `
        width-p-50
    `,
    classNameContentButtonNext: `
        width-p-50
    `,
    styleTemplateButtonPre: 'mooveri',
    styleTemplateButtonNext: 'mooveri',
    classNameContentButtons: `
      flex 
      flex-nowrap
      flex-gap-column-20
      width-p-100
    `,

    styleStep1: {
        classNameContentCol: `
        flex
        flex-gap-40
        flex-gap-column-40
        flex-justify-between
        
        `,
        styleTemplateTexUpload: 'mooveri4',
        styleTemplateTexBusiness: 'mooveri20',
        styleTemplateTextLogo: 'mooveri5',
        classNameContentAvatar: `
        flex-12 
        flex-md-3 
        m-b-20 
        m-md-b-0 
        flex 
        flex-justify-center 
        flex-aling-start 
        flex-align-content-start
        text-center
        `,

        styleTemplateTextHello: 'mooveri9gray',
        classNameTextSub: `
            font-28
            font-w-600 
            font-nunito
            color-warmGreyTwo 
        `,
        classNameText: `
        text-center
        `,
    },
    styleStep2: {
        classNameContentCol: `
        flex
        width-p-100
        
        `,
        styleTemplateTexBusiness: 'mooveri20',
        styleTemplateTextHello: 'mooveri9gray',
        classNameTextSub: `
            font-28
            font-w-600 
            font-nunito
            color-warmGreyTwo 
        `,
        classNameText: `
        text-center
        `,
    },
    styleStep3: {
        classNameContentCol: `
        flex
        width-p-100
        
        `,
        styleTemplateTexBusiness: 'mooveri20',
        styleTemplateTextHello: 'mooveri9gray',
        classNameTextSub: `
            font-28
            font-w-600 
            font-nunito
            color-warmGreyTwo 
        `,
        classNameText: `
        text-center
        `,
    },
    classNameBtn: `
        flex 
        flex-align-center
        flex-justify-center
        flex-nowrap
        flex-gap-column-10
        pointer
    `,
    classNameSkip: `
      flex
      pos-a
      top-10  
      right-10
    `,
    styleTemplateSkips: 'mooveri2',
    iconButtonPre: <ArrowPre size={15} />,
    iconButtonNext: <ArrowNext size={15} />,
};
