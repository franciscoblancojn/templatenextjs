import { SubmitResult } from '@/components/Form/Base';
import InputGoogleConfirm from '@/components/Input/Google/Template/InputGoogleConfirm';
import InputSelect from '@/components/Input/Select';
import InputTel from '@/components/Input/Tel';
import InputText from '@/components/Input/Text';
import Space from '@/components/Space';
import Text, { TextStyles } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';
import { useData } from 'fenextjs-hook/cjs/useData';

import { DataFormConfirmStep2Props } from '@/interfaces/FormConfirmStep2';
import { PhoneProps } from '@/interfaces/Phone';
import { useLang } from '@/lang/translate';
import { ConfirmStep2 } from '@/validations/ConfirmStep2';
import { useMemo } from 'react';

export interface Formstep2ClassProps {
    classNameContent?: string;
    classNameText?: string;
    classNameTextSub?: string;
    classNameContentForm?: string;
    classNameContentCol?: string;
    styleTemplateTextHello?: TextStyles | ThemesType;
    styleTemplateTexBusiness?: TextStyles | ThemesType;

    inputs?: DataFormConfirmStep2Props<boolean>;
}
export type onSubmintstep2 = () =>
    | Promise<SubmitResult<DataFormConfirmStep2Props>>
    | SubmitResult<DataFormConfirmStep2Props>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface Formstep2BaseProps {
    defaultData?: DataFormConfirmStep2Props;
    onSubmit?: onSubmintstep2;
    onChange?: (data: DataFormConfirmStep2Props<string | PhoneProps>) => void;
    onValidateUsername?: onValidateUsername;
    urlRediret?: string;
}
export interface Formstep2Props
    extends Formstep2BaseProps,
        Formstep2ClassProps {}

export const Formstep2Base = ({
    classNameContent = '',
    classNameTextSub = '',
    classNameText = '',
    classNameContentCol = '',

    styleTemplateTextHello = Theme.styleTemplate ?? '_default',
    styleTemplateTexBusiness = Theme.styleTemplate ?? '_default',

    defaultData = {},
    inputs = {
        contactName: true,
        phone: true,
        phoneAlternative: true,
        search: true,
    },
    ...props
}: Formstep2Props) => {
    const _t = useLang();
    const { data, onChangeData } = useData<
        DataFormConfirmStep2Props<string | PhoneProps>
    >(defaultData, {
        onChangeDataAfter: props.onChange,
    });

    const ConfirmStep2YupV = useMemo(() => ConfirmStep2(data, inputs), [data]);
    return (
        <>
            <div className={classNameContent}>
                <Space size={25} />
                <Text
                    styleTemplate={styleTemplateTextHello}
                    className={classNameText}
                >
                    <span className={classNameTextSub}>
                        {_t(
                            'This information is important for contact clients.'
                        )}
                    </span>
                </Text>
                <Space size={25} />
                <div className={classNameContentCol}>
                    <Text styleTemplate={styleTemplateTexBusiness}>
                        {_t('Contact Info')}
                    </Text>
                </div>
                <Space size={25} />
                <div className={classNameContentCol}>
                    <div className="width-p-100">
                        <InputText
                            styleTemplate="mooveri"
                            placeholder="Contact Name"
                            defaultValue={data.contactName as string}
                            onChange={onChangeData('contactName')}
                            yup={ConfirmStep2YupV.y.contactName}
                        />
                        <Space size={25} />
                        <InputTel
                            styleTemplate="mooveri"
                            placeholder="Phone Number"
                            value={data.phone as PhoneProps}
                            onChange={onChangeData('phone')}
                            yup={ConfirmStep2YupV.y.phone}
                        />
                        <Space size={25} />
                        <InputTel
                            styleTemplate="mooveri"
                            placeholder="Alternative Phone Number"
                            value={data.phoneAlternative as PhoneProps}
                            onChange={onChangeData('phoneAlternative')}
                            yup={ConfirmStep2YupV.y.phoneAlternative}
                        />
                        <Space size={25} />

                        <InputSelect
                            styleTemplate="mooveri6"
                            placeholder="State"
                            //defaultValue={data.state}
                            //onChange={onChangeData("state")}
                        />
                        <Space size={25} />
                        <InputSelect
                            styleTemplate="mooveri6"
                            placeholder="City"
                            //defaultValue={data.city}
                            //onChange={onChangeData("city")}
                        />
                        <Space size={25} />
                        <InputGoogleConfirm
                            placeholder="Search your address"
                            //onChange={onChangeData("search")}
                            yup={ConfirmStep2YupV.y.search}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default Formstep2Base;
