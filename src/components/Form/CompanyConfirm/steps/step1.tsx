import { SubmitResult } from '@/components/Form/Base';
import InputAvatar from '@/components/Input/Avatar';
import InputText from '@/components/Input/Text';
import Space from '@/components/Space';
import Text, { TextStyles } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';
import { useData } from 'fenextjs-hook/cjs/useData';

import { DataFormConfirmStep1Props } from '@/interfaces/FormConfirmStep1';

import { ConfirmStep1 } from '@/validations/ConfirmStep1';
import { useLang } from '@/lang/translate';
import { useMemo } from 'react';

export interface Formstep1ClassProps {
    classNameContent?: string;
    classNameText?: string;
    classNameTextSub?: string;
    classNameContentCol?: string;
    classNameContentAvatar?: string;

    styleTemplateTexUpload?: TextStyles | ThemesType;

    styleTemplateTextLogo?: TextStyles | ThemesType;
    styleTemplateTextHello?: TextStyles | ThemesType;
    styleTemplateTexBusiness?: TextStyles | ThemesType;
    inputs?: DataFormConfirmStep1Props<boolean>;
}

export type onSubmintstep1 = () =>
    | Promise<SubmitResult<DataFormConfirmStep1Props>>
    | SubmitResult<DataFormConfirmStep1Props>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface Formstep1BaseProps {
    defaultData?: DataFormConfirmStep1Props;
    onSubmit?: onSubmintstep1;
    onChange?: (data: DataFormConfirmStep1Props) => void;
    onValidateUsername?: onValidateUsername;
    urlRediret?: string;
}
export interface Formstep1Props
    extends Formstep1BaseProps,
        Formstep1ClassProps {}

export const Formstep1Base = ({
    classNameContent = '',
    classNameTextSub = '',
    classNameText = '',
    classNameContentCol = '',
    classNameContentAvatar = '',

    styleTemplateTexUpload = Theme.styleTemplate ?? '_default',
    styleTemplateTextLogo = Theme.styleTemplate ?? '_default',
    styleTemplateTextHello = Theme.styleTemplate ?? '_default',
    styleTemplateTexBusiness = Theme.styleTemplate ?? '_default',
    inputs = {
        companyName: true,
        legalName: true,
        yearFounded: true,
    },
    defaultData = {},
    ...props
}: Formstep1Props) => {
    const _t = useLang();
    const { data, onChangeData } = useData<DataFormConfirmStep1Props>(
        defaultData,
        {
            onChangeDataAfter: props.onChange,
        }
    );

    const ConfirmStep1YupV = useMemo(() => ConfirmStep1(data, inputs), [data]);
    return (
        <>
            <div className={classNameContent}>
                <Space size={25} />
                <Text
                    styleTemplate={styleTemplateTextHello}
                    className={classNameText}
                >
                    {_t('Hello mooveri,')}
                    <span className={classNameTextSub}>
                        {_t('help us to understand your company.')}
                    </span>
                </Text>
                <Space size={25} />
                <div className={classNameContentCol}>
                    <div className={classNameContentAvatar}>
                        <Text styleTemplate={styleTemplateTexBusiness}>
                            {_t('Business Info')}
                        </Text>
                    </div>
                    <div className="flex-12 flex-sm-8"></div>
                </div>
                <Space size={25} />
                <div className={classNameContentCol}>
                    <div className={classNameContentAvatar}>
                        <InputAvatar styleTemplate="mooveri" />
                        <Space size={10} />
                        <Text styleTemplate={styleTemplateTexUpload}>
                            {_t('**(Max Upload File Size 1MB , JPG, PNG).')}{' '}
                        </Text>
                        <Space size={1} />
                        <Text styleTemplate={styleTemplateTexUpload}>
                            {_t('**Recommended Size 400px 400px.')}{' '}
                        </Text>
                        <Space size={1} />
                        <Text styleTemplate={styleTemplateTextLogo}>
                            {_t('Your Logo')}{' '}
                        </Text>
                    </div>
                    <div className="flex-12 flex-sm-8">
                        <InputText
                            styleTemplate="mooveri"
                            placeholder="Company Name"
                            defaultValue={data.companyName}
                            onChange={onChangeData('companyName')}
                            yup={ConfirmStep1YupV.y.companyName}
                        />
                        <Space size={15} />
                        <InputText
                            styleTemplate="mooveri"
                            placeholder="Legal Name"
                            defaultValue={data.legalName}
                            onChange={onChangeData('legalName')}
                            yup={ConfirmStep1YupV.y.legalName}
                        />
                        <Space size={15} />
                        <InputText
                            styleTemplate="mooveri"
                            placeholder="Year Founded"
                            defaultValue={data.yearFounded}
                            onChange={onChangeData('yearFounded')}
                            yup={ConfirmStep1YupV.y.yearFounded}
                        />
                        <Space size={15} />
                        <InputText
                            styleTemplate="mooveri"
                            placeholder="Facebook Page (Optional) "
                            defaultValue={data.facebook}
                            onChange={onChangeData('facebook')}
                        />
                        <Space size={15} />
                        <InputText
                            styleTemplate="mooveri"
                            placeholder="Instagram Profile (Optional)"
                            defaultValue={data.instagram}
                            onChange={onChangeData('instagram')}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default Formstep1Base;
