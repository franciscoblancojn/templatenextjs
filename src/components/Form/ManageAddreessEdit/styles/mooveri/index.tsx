import { FormManageAddreessEditClassProps } from '@/components/Form/ManageAddreessEdit/Base';

export const mooveri: FormManageAddreessEditClassProps = {
    classNameContent: `
    m-h-auto
  `,
    styleTemplateInput: {
        address: 'mooveri',
        apt: 'mooveri',
        city: 'mooveri',
        zipcode: 'mooveri',
    },
    inputs: {
        address: true,
        city: true,
        apt: true,
        zipcode: true,
        location: true,
    },
};
