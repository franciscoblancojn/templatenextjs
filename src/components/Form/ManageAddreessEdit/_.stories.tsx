import { Story, Meta } from "@storybook/react";

import { FormManageAddreessEditProps, FormManageAddreessEdit } from "./index";

export default {
    title: "Form/ManageAddreessEdit",
    component: FormManageAddreessEdit,
} as Meta;

const Template: Story<FormManageAddreessEditProps> = (args) => (
    <FormManageAddreessEdit {...args} />
);


export const Index = Template.bind({});
Index.args = {
};
