import { useMemo } from 'react';

import * as styles from '@/components/Form/ConfirmPhone/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormConfirmPhoneBaseProps,
    FormConfirmPhoneBase,
} from '@/components/Form/ConfirmPhone/Base';

export const FormConfirmPhoneStyle = { ...styles } as const;

export type FormConfirmPhoneStyles = keyof typeof FormConfirmPhoneStyle;

export interface FormConfirmPhoneProps extends FormConfirmPhoneBaseProps {
    styleTemplate?: FormConfirmPhoneStyles | ThemesType;
}

export const FormConfirmPhone = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormConfirmPhoneProps) => {
    const Style = useMemo(
        () =>
            FormConfirmPhoneStyle[styleTemplate as FormConfirmPhoneStyles] ??
            FormConfirmPhoneStyle._default,
        [styleTemplate]
    );

    return <FormConfirmPhoneBase {...Style} {...props} />;
};
export default FormConfirmPhone;
