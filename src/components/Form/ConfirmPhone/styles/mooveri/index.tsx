import { FormConfirmPhoneClassProps } from '@/components/Form/ConfirmPhone/Base';

export const mooveri: FormConfirmPhoneClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameTitle: `
        text-center color-white
    `,
    classNameContentText: `
        m-h-auto
    `,
    classNameText: `
            text-center
    `,
    styleTemplateText: 'mooveri',
    styleTemplateButton: 'mooveri',
    styleTemplateInput: 'mooveri',
    styleTemplateLink: 'mooveri',
    styleTemplateTitle: 'mooveri',
    typeStyleTemplateTitle: 'h7',
};
