import { FormConfirmPhoneClassProps } from '@/components/Form/ConfirmPhone/Base';

export const tolinkme: FormConfirmPhoneClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameTitle: `
        text-center color-white
    `,
    classNameContentText: `
        m-h-auto
    `,
    classNameText: `
            text-center
    `,
    styleTemplateText: 'tolinkme',
    styleTemplateButton: 'tolinkme',
    styleTemplateInput: 'tolinkme',
    styleTemplateLink: 'tolinkme',
    styleTemplateTitle: 'tolinkme',
    typeStyleTemplateTitle: 'h1',
};
