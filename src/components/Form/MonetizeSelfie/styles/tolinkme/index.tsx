import { MonetizeSelfieFormClassProps } from '@/components/Form/MonetizeSelfie/Base';

export const tolinkme: MonetizeSelfieFormClassProps = {
    classNameContenttext: `
  width-p-100
  flex
  flex-justify-center
  p-10
  text-center
  m-auto
  `,
    styleTemplateTitle: 'tolinkme27',
    classNameContentIcon: `
  width-p-50
  m-auto
  `,
    styleTemplateTexts: 'tolinkme12',
    classNameContentBtn: `
      m-auto
      pos-f
      bottom-20
      z-index-1
      width-p-100
      p-h-18
      left-0
      right-0
      m-auto
      flex
      flex-justify-center
    
  `,
    classNameContentBtnCamara: `
    m-auto
    pos-f
    bottom-20
    z-index-2
    width-p-100
    p-h-17
    left-0
    right-0
    m-auto
    flex
    flex-justify-center
  `,
    sizeContentBtnCamara: 400,
    classNameContentAvatar: `
    m-auto
    flex
    flex-justify-center
  `,
    styleTemplateBtn: 'tolinkme6',

    classNameBtn: `
    flex
    
    flex-nowrap
  `,
    classNameContentCamara: `
    flex
  `,
};
