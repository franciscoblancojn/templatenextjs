import { useMemo } from 'react';

import * as styles from '@/components/Form/MonetizeSelfie/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    MonetizeSelfieFormBaseProps,
    MonetizeSelfieFormBase,
} from '@/components/Form/MonetizeSelfie/Base';

export const MonetizeSelfieFormStyle = { ...styles } as const;

export type MonetizeSelfieFormStyles = keyof typeof MonetizeSelfieFormStyle;

export interface MonetizeSelfieFormProps extends MonetizeSelfieFormBaseProps {
    styleTemplate?: MonetizeSelfieFormStyles | ThemesType;
}

export const MonetizeSelfieForm = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: MonetizeSelfieFormProps) => {
    const Style = useMemo(
        () =>
            MonetizeSelfieFormStyle[
                styleTemplate as MonetizeSelfieFormStyles
            ] ?? MonetizeSelfieFormStyle._default,
        [styleTemplate]
    );

    return <MonetizeSelfieFormBase {...Style} {...props} />;
};
export default MonetizeSelfieForm;
