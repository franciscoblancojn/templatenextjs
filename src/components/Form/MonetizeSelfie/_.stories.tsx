import { Story, Meta } from "@storybook/react";

import { MonetizeSelfieFormProps, MonetizeSelfieForm } from "./index";

export default {
    title: "Form/MonetizerForm",
    component: MonetizeSelfieForm,
} as Meta;

const Template: Story<MonetizeSelfieFormProps> = (args) => <MonetizeSelfieForm {...args} />;

export const Index = Template.bind({});
Index.args = {
    
};
