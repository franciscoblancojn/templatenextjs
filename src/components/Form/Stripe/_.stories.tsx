import { Story, Meta } from "@storybook/react";

import { FormStripeProps, FormStripe } from "./index";

export default {
    title: "Form/Stripe",
    component: FormStripe,
} as Meta;

const Template: Story<FormStripeProps> = (args) => <FormStripe {...args} />;

export const Index = Template.bind({});
Index.args = {
    
};
