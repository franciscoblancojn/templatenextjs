import { FormStripeClassProps } from '@/components/Form/Stripe/Base';

export const tolinkme: FormStripeClassProps = {
    classNameContent: `
  width-p-100
  m-t-20
`,

    classNameTitle: `
  text-left
`,
    styleTemplateText: 'tolinkme12',
    styleTemplateTitle: 'tolinkme27',
};
