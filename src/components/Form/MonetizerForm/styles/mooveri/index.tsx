import { FormMonetizerFormClassProps } from '@/components/Form/MonetizerForm/Base';

export const mooveri: FormMonetizerFormClassProps = {
    classNameContent: `
        m-h-auto
    `,
    sizeContentInputs: 330,
    classNameContentInputs: `
        m-h-auto
        p-h-15
    `,
};

export const mooveriCompany: FormMonetizerFormClassProps = {
    ...mooveri,
};
