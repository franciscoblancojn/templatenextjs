import { useMemo, useState } from 'react';

import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataFormMonetizerForm } from '@/interfaces/FormMonetizerForm';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { UserLoginProps, useUser } from '@/hook/useUser';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import InputCheckbox, {
    InputCheckboxStyles,
} from '@/components/Input/Checkbox';
import Button, { ButtonStyles } from '@/components/Button';
import { FormMonetizerFormYup } from '@/validations/MonetizerForm';
import Theme, { ThemesType } from '@/config/theme';
import { InputSelectCSC } from 'fenextjs-component/cjs/Input/SelectCSC';

export interface FormMonetizerFormClassProps {
    sizeContent?: number;
    classNameBtn?: string;
    classNameCol?: string;
    classNamecont?: string;
    classNameContent?: string;
    sizeContentInputs?: number;
    sizeContentBtn?: number;
    classNameContentCol?: string;
    classNameContentLink?: string;
    classNameContentCol2?: string;
    classNameContentInputs?: string;
    inputs?: DataFormMonetizerForm<boolean>;
    styleTemplateText?: InputTextStyles | ThemesType;
    styleTemplateCheckbox?: InputCheckboxStyles | ThemesType;
    styleTemplateBtn?: ButtonStyles | ThemesType;
    classNameContentBtn?: string;
    classNameCheckbox18?: string;
}

export type onSubmintMonetizerForm = (
    data: DataFormMonetizerForm
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormMonetizerFormBaseProps {
    text1?: string;
    text2?: string;
    text18?: string;
    textBtnNext?: string;
    onSubmit?: onSubmintMonetizerForm;
    urlRediret?: string;
    onValidateUsername?: onValidateUsername;
    onClick?: (uuid?: string) => void;
    loader?: boolean;
    Next?: string;
    defaultValue?: DataFormMonetizerForm;
    onChange?: (data: DataFormMonetizerForm) => void;
    extraLoader?: boolean;
}
export interface FormMonetizerFormProps
    extends FormMonetizerFormBaseProps,
        FormMonetizerFormClassProps {}

export const FormMonetizerFormBase = ({
    Next,
    onClick,
    text1 = 'Click here to confirm that you are at least',
    text2 = ' and the age of majority in your place of residence.',
    text18 = '18 years old',
    textBtnNext = '',
    inputs = {
        fullName: true,
        lastName: true,
        country: true,
        state: true,
        city: true,
        zipPostalCode: true,
        confirm18: true,
        Street: true,
    },
    sizeContent = -1,
    sizeContentBtn = 1,
    classNameContent = '',
    sizeContentInputs = -1,
    classNameContentInputs = '',
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    styleTemplateCheckbox = Theme?.styleTemplate ?? '_default',
    styleTemplateBtn = Theme?.styleTemplate ?? '_default',
    classNameContentBtn = '',
    classNameCheckbox18 = '',
    defaultValue = { confirm18: false },
    onChange,
    extraLoader = undefined,
    ...props
}: FormMonetizerFormProps) => {
    const _t = useLang();
    const router = useRouter();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData, setData } = useData<DataFormMonetizerForm>(
        defaultValue,
        {
            onChangeDataAfter: onChange,
        }
    );
    const FormMonetizerFormYupV = useMemo(
        () => FormMonetizerFormYup(data, inputs),
        [data]
    );

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <Form<DataFormMonetizerForm>
                    id="MonetizerForm"
                    data={data}
                    onSubmit={props?.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={FormMonetizerFormYupV.v}
                    onAfterSubmit={(data) => {
                        onLogin(data);
                        router.push(props?.urlRediret ?? url.index);
                    }}
                >
                    <ContentWidth
                        size={sizeContentInputs}
                        className={`formMDPContent ${classNameContentInputs}`}
                    >
                        <div className="formMDPContentDiv1">
                            {inputs.fullName ? (
                                <InputText
                                    yup={FormMonetizerFormYupV.y.fullName}
                                    defaultValue={data.fullName}
                                    onChange={onChangeData('fullName')}
                                    styleTemplate={styleTemplateText}
                                    placeholder={'Full Name'}
                                />
                            ) : (
                                <></>
                            )}
                        </div>
                        <div className="formMDPContentDiv2">
                            {inputs.lastName ? (
                                <InputText
                                    yup={FormMonetizerFormYupV.y.lastName}
                                    defaultValue={data.lastName}
                                    onChange={onChangeData('lastName')}
                                    styleTemplate={styleTemplateText}
                                    placeholder={'Last Name'}
                                />
                            ) : (
                                <></>
                            )}
                        </div>
                        <InputSelectCSC
                            typeSelect="select"
                            autoComplete={false}
                            onChange={(csc) => {
                                setData({
                                    ...data,
                                    ...csc,
                                });
                            }}
                            defaultValue={{
                                country: data.country,
                                state: data.state,
                                city: data.city,
                            }}
                            useContainer={false}
                            _t={_t}
                        />
                        <div className="formMDPContentDiv6">
                            {inputs.Street ? (
                                <>
                                    <InputText
                                        yup={FormMonetizerFormYupV.y.Street}
                                        styleTemplate={styleTemplateText}
                                        defaultValue={data.Street}
                                        placeholder={'Street'}
                                        onChange={onChangeData('Street')}
                                    />
                                    <Space size={6} />
                                </>
                            ) : (
                                <></>
                            )}
                        </div>

                        <div className="formMDPContentDiv7">
                            {inputs.zipPostalCode ? (
                                <InputText
                                    yup={FormMonetizerFormYupV.y.zipPostalCode}
                                    defaultValue={data.zipPostalCode}
                                    onChange={onChangeData('zipPostalCode')}
                                    styleTemplate={styleTemplateText}
                                    placeholder={'Zip/Postal Code'}
                                />
                            ) : (
                                <></>
                            )}
                        </div>

                        <div className="formMDPContentDiv8">
                            {inputs.confirm18 ? (
                                <>
                                    <InputCheckbox
                                        defaultValue={data.confirm18}
                                        onChange={onChangeData('confirm18')}
                                        styleTemplate={styleTemplateCheckbox}
                                        label={
                                            <>
                                                {_t(text1)}
                                                <span
                                                    className={
                                                        classNameCheckbox18
                                                    }
                                                >
                                                    {_t(text18)}
                                                </span>
                                                {_t(text2)}
                                            </>
                                        }
                                    />
                                </>
                            ) : (
                                <></>
                            )}
                        </div>
                        <div className="formMDPContentDiv9">
                            <ContentWidth
                                size={sizeContentBtn}
                                className={classNameContentBtn}
                            >
                                <Button
                                    disabled={disabled}
                                    loader={extraLoader ?? loader}
                                    styleTemplate={styleTemplateBtn}
                                    onClick={() => {
                                        onClick?.(Next);
                                    }}
                                >
                                    {_t(textBtnNext)}
                                </Button>
                            </ContentWidth>
                        </div>
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormMonetizerFormBase;
