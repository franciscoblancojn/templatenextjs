import { useMemo } from 'react';

import * as styles from '@/components/Form/MonetizerForm/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormMonetizerFormBaseProps,
    FormMonetizerFormBase,
} from '@/components/Form/MonetizerForm/Base';

export const FormMonetizerFormStyle = { ...styles } as const;

export type FormMonetizerFormStyles = keyof typeof FormMonetizerFormStyle;

export interface FormMonetizerFormProps extends FormMonetizerFormBaseProps {
    styleTemplate?: FormMonetizerFormStyles | ThemesType;
}

export const FormMonetizerForm = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormMonetizerFormProps) => {
    const Style = useMemo(
        () =>
            FormMonetizerFormStyle[styleTemplate as FormMonetizerFormStyles] ??
            FormMonetizerFormStyle._default,
        [styleTemplate]
    );

    return <FormMonetizerFormBase {...Style} {...props} />;
};
export default FormMonetizerForm;
