import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { UserProps } from '@/interfaces/User';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { UserYup } from '@/validations/User';

import InputText, { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import { Grid, GridProps } from '@/components/Grid';
import { InputAvatar, InputAvatarStyles } from '@/components/Input/Avatar';
import { InputTel, InputTelStyles } from '@/components/Input/Tel';
import {
    InputCheckbox,
    InputCheckboxStyles,
} from '@/components/Input/Checkbox';

export interface FormUserEditClassProps {
    classNameContent?: string;

    styleTemplateInputText?: InputTextStyles | ThemesType;
    styleTemplateInputTel?: InputTelStyles | ThemesType;
    styleTemplateInputCheckbox?: InputCheckboxStyles | ThemesType;
    styleTemplateInputAvatar?: InputAvatarStyles | ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;

    GridProps?: GridProps;
    classNameContentInput?: string;
}

export type onSubmintUserEdit = (
    data: UserProps
) => Promise<SubmitResult> | SubmitResult;

export interface FormUserEditBaseProps {
    defaultData: UserProps;
    onSubmit?: onSubmintUserEdit;
}

export interface FormUserEditProps
    extends FormUserEditClassProps,
        FormUserEditBaseProps {}
export const FormUserEditBase = ({
    classNameContent = '',
    styleTemplateInputText = Theme.styleTemplate ?? '_default',
    styleTemplateInputAvatar = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateInputTel = Theme.styleTemplate ?? '_default',
    styleTemplateInputCheckbox = Theme.styleTemplate ?? '_default',
    GridProps = {},
    classNameContentInput = '',

    defaultData,
    ...props
}: FormUserEditProps) => {
    const _t = useLang();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<UserProps>(defaultData);

    const onSave = async () => {
        const result = await props?.onSubmit?.(data);
        if (result) {
            return result;
        }
        const r: SubmitResult = {
            status: 'ok',
        };
        return r;
    };

    const validate = useMemo(() => UserYup(data), [data]);

    return (
        <>
            <ContentWidth className={classNameContent} size={800}>
                <Form<UserProps>
                    id="user-edit"
                    data={data}
                    onSubmit={onSave}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={validate}
                >
                    <Grid {...GridProps}>
                        <div className={`${classNameContentInput}`}>
                            <InputAvatar
                                defaultValue={{
                                    fileData: (data?.imagen ?? '') as string,
                                }}
                                styleTemplate={styleTemplateInputAvatar}
                                onSubmit={(e) => {
                                    onChangeData('imagen')(e?.fileData);
                                }}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputCheckbox
                                label={'Active'}
                                styleTemplate={styleTemplateInputCheckbox}
                                defaultValue={data.status_confirm ?? false}
                                onChange={onChangeData('status_confirm')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}></div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Nombre"
                                label={'Nombre'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (data?.first_name ?? '') as string
                                }
                                onChange={onChangeData('first_name')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Apellido"
                                label={'Apellido'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={(data?.last_name ?? '') as string}
                                onChange={onChangeData('last_name')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputTel
                                placeholder="Telefono"
                                label={'Telefono'}
                                styleTemplate={styleTemplateInputTel}
                                defaultValue={{
                                    code: (data?.phone ?? '')?.split(
                                        ' '
                                    )[0] as string,
                                    tel: (data?.phone ?? '')?.split?.(
                                        ' '
                                    )?.[1] as string,
                                }}
                                onChange={(e) => {
                                    onChangeData('phone')(`${e.code} ${e.tel}`);
                                }}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Email"
                                label={'Email'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={(data?.email ?? '') as string}
                                onChange={onChangeData('email')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}></div>
                        <div className={`${classNameContentInput}`}></div>
                        <div className={`${classNameContentInput}`}>
                            <Button
                                styleTemplate={styleTemplateButton}
                                loader={loader}
                                disabled={disabled}
                            >
                                {_t('Guardar')}
                            </Button>
                        </div>
                    </Grid>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormUserEditBase;
