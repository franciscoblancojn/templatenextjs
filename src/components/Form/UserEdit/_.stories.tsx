import { Story, Meta } from "@storybook/react";

import { FormUserEditProps, FormUserEdit } from "./index";

export default {
    title: "Form/UserEdit",
    component: FormUserEdit,
} as Meta;

const Template: Story<FormUserEditProps> = (args) => (
    <FormUserEdit {...args} />
);

export const Index = Template.bind({});
Index.args = {
    
};
