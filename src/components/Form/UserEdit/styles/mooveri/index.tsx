import { FormUserEditClassProps } from '@/components/Form/UserEdit/Base';

export const mooveri: FormUserEditClassProps = {
    styleTemplateInputAvatar: 'mooveriBackoffice',
    styleTemplateInputText: 'mooveriBackoffice',
    styleTemplateInputTel: 'mooveriBackoffice',
    styleTemplateButton: 'mooveriBackoffice',
    styleTemplateInputCheckbox: 'mooveriBackoffice',
    GridProps: {
        // areas:["ABC"],
        className: `
            grid-columns-3
            column-gap-10
            row-gap-10
            m-b-20
        `,
    },
    classNameContentInput: `
        grid-
    `,
};
