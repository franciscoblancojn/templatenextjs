import { FormChatMessegesClassProps } from '@/components/Form/ChatMesseges/Base';

export const mooveri: FormChatMessegesClassProps = {
    styleTemplateInput: {
        chat: 'mooveri',
    },
    styleTemplateButton: 'mooveriChat',
    inputs: {
        chat: true,
    },
    classNameContentInputs: `
        flex  
        flex-justify-between   
   `,
    classNameContentInput: `
        width-p-90
   `,
    classNameContentButton: `
        width-p-10
        flex
        flex-align-center
        flex-justify-center
   `,
};
