import { FormMonetizeClassProps } from '@/components/Form/Monetize/Base';

export const tolinkme: FormMonetizeClassProps = {
    inputs: {
        amount: true,
        description: true,
    },
    // sizeContent: 500,
    classNameContent: `
    m-h-auto
    bg-white
    `,
    styleTemplateText: 'tolinkme22',
    classNameInputR: `
        flex
        p-v-10
    `,
    classNameInputT: `
        p-v-10
    `,
    classNameMaxLength: `
    text-right
    font-12
    color-warmGrey
    m-b-10
`,
    styleTemplateInputNumber: 'tolinkme11',
    classNameAmount: `
    m-b-15
`,

    classNameDescription: `
      p-b-10  
    `,
    classNameContentDescription: `
    p-b-100

    `,
};
