import { FormNameCategoryClassProps } from '@/components/Form/NameCategory/Base';

export const tolinkme: FormNameCategoryClassProps = {
    classNameContent: `
    m-h-auto
  `,
    classNameTitle: `
  text-center color-white
  `,
    styleTemplateTitle: 'tolinkme',
    typeStyleTemplateTitle: 'h1',
    classNameContentText: `
  m-h-auto
  `,
    classNameText: `
  text-center
  `,
    styleTemplateInput: 'tolinkme',
    styleTemplateButton: 'tolinkme',
};
