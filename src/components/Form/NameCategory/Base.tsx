import { useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
// import { Categories } from "@/components/Categories";
import { Title, TitleStylesType, TitleStyles } from '@/components/Title';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataNameCategory } from '@/interfaces/NameCategory';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { NameCategoryYup } from '@/validations/NameCategory';

import Text, { TextStyles } from '@/components/Text';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import url from '@/data/routes';
import { useRouter } from 'next/router';

export interface FormNameCategoryClassProps {
    classNameContent?: string;
    classNameTitle?: string;
    classNameContentText?: string;
    classNameText?: string;

    typeStyleTemplateTitle?: TitleStylesType;
    styleTemplateTitle?: TitleStyles | ThemesType;
    styleTemplateInput?: InputTextStyles | ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateText?: TextStyles | ThemesType;
}

export type onSubmintNameCategory = (
    data: DataNameCategory
) => Promise<SubmitResult> | SubmitResult;

export interface FormNameCategoryBaseProps {
    onSubmit: onSubmintNameCategory;
    token?: string;
    urlRedirect?: string;
}

export interface FormNameCategoryProps
    extends FormNameCategoryClassProps,
        FormNameCategoryBaseProps {}

export const FormNameCategoryBase = ({
    classNameContent = '',
    classNameTitle = '',
    classNameContentText = '',
    classNameText = '',

    typeStyleTemplateTitle = 'h1',

    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateInput = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',

    token,

    urlRedirect = url.profile,
    ...props
}: FormNameCategoryProps) => {
    const _t = useLang();
    const route = useRouter();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataNameCategory>({
        name: '',
        categories: [],
        token,
    });

    return (
        <>
            <ContentWidth size={320} className={classNameContent}>
                <Form<DataNameCategory>
                    id="name-category"
                    data={data}
                    onSubmit={props.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={NameCategoryYup}
                    onAfterSubmit={() => {
                        route.push(urlRedirect);
                    }}
                >
                    <Title
                        styleTemplate={styleTemplateTitle}
                        typeStyle={typeStyleTemplateTitle}
                        className={classNameTitle}
                    >
                        {_t('Tell us about you')}
                    </Title>
                    <ContentWidth size={230} className={classNameContentText}>
                        <Text
                            styleTemplate={styleTemplateText}
                            className={classNameText}
                        >
                            {_t('For a better experience')}
                        </Text>
                    </ContentWidth>
                    <Space size={17} />
                    <InputText
                        defaultValue={data.name}
                        placeholder={_t('Your Name')}
                        onChange={onChangeData('name')}
                        styleTemplate={styleTemplateInput}
                    />
                    <Space size={23} />
                    {/* <Categories onChange={onChangeData("categories")} /> */}
                    <Space size={17} />
                    <Button
                        styleTemplate={styleTemplateButton}
                        disabled={disabled}
                        loader={loader}
                    >
                        {_t('Continue')}
                    </Button>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormNameCategoryBase;
