import { useMemo } from 'react';

import * as styles from '@/components/Form/ConfirmEmail/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormConfirmEmailBaseProps,
    FormConfirmEmailBase,
} from '@/components/Form/ConfirmEmail/Base';

export const FormConfirmEmailStyle = { ...styles } as const;

export type FormConfirmEmailStyles = keyof typeof FormConfirmEmailStyle;

export interface FormConfirmEmailProps extends FormConfirmEmailBaseProps {
    styleTemplate?: FormConfirmEmailStyles | ThemesType;
}

export const FormConfirmEmail = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormConfirmEmailProps) => {
    const Style = useMemo(
        () =>
            FormConfirmEmailStyle[styleTemplate as FormConfirmEmailStyles] ??
            FormConfirmEmailStyle._default,
        [styleTemplate]
    );

    return <FormConfirmEmailBase {...Style} {...props} />;
};
export default FormConfirmEmail;
