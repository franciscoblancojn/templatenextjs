import { FormConfirmEmailClassProps } from '@/components/Form/ConfirmEmail/Base';

export const tolinkme: FormConfirmEmailClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameTitle: `
        text-center color-white
    `,
    classNameContentText: `
        m-h-auto
    `,
    classNameText: `
            text-center
    `,
    styleTemplateText: 'tolinkme',
    styleTemplateButton: 'tolinkme',
    styleTemplateInput: 'tolinkme',
    styleTemplateLink: 'tolinkme',
    styleTemplateTitle: 'tolinkme',
    typeStyleTemplateTitle: 'h1',
};
