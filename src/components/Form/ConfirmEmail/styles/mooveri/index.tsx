import { FormConfirmEmailClassProps } from '@/components/Form/ConfirmEmail/Base';

export const mooveri: FormConfirmEmailClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameTitle: `
        text-center color-white
    `,
    classNameContentText: `
        m-h-auto
    `,
    classNameText: `
            text-center
    `,
    styleTemplateText: 'mooveri',
    styleTemplateButton: 'mooveri',
    styleTemplateInput: 'mooveri',
    styleTemplateLink: 'mooveri',
    styleTemplateTitle: 'mooveri',
    typeStyleTemplateTitle: 'h7',
};
