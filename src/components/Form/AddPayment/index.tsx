import { useMemo } from 'react';

import * as styles from '@/components/Form/AddPayment/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormAddPaymentBaseProps,
    FormAddPaymentBase,
} from '@/components/Form/AddPayment/Base';

export const FormAddPaymentStyle = { ...styles } as const;

export type FormAddPaymentStyles = keyof typeof FormAddPaymentStyle;

export interface FormAddPaymentProps extends FormAddPaymentBaseProps {
    styleTemplate?: FormAddPaymentStyles | ThemesType;
}

export const FormAddPayment = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormAddPaymentProps) => {
    const Style = useMemo(
        () =>
            FormAddPaymentStyle[styleTemplate as FormAddPaymentStyles] ??
            FormAddPaymentStyle._default,
        [styleTemplate]
    );

    return <FormAddPaymentBase {...Style} {...props} />;
};
export default FormAddPayment;
