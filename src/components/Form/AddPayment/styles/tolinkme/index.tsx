import { FormPaymentClassProps } from '@/components/Form/Payment/Base';

export const tolinkme: FormPaymentClassProps = {
    sizeContent: 350,
    classNameContent: `
    m-h-auto`,
    classNameContentTitle: `
    text-center color-white
    `,

    styleTemplateText: 'tolinkme50',
    inputs: {
        zipPostalCode: true,
        Street: true,
        city: true,
        country: true,
        nameOnTheCard: true,
        state: true,
        stripe_after: true,
        stripe_before: true,
        confirm18: true,
        email: true,
    },
    rsLogin: null,

    styleTemplateTitle: 'tolinkme',
    classNameContentCol2: `
    flex 
    flex-gap-column-10 
    flex-nowrap
    `,

    classNameContentCheckbox: `
    bg-black-144
    flex
    p-v-10
    p-h-5
    flex-gap-row-10
    border-radius-15
    flex-nowrap
    `,

    styleTemplateInputCheckbox: 'tolinkme12',
    classNameContentSvg: `
        color-white
        flex

    `,

    classNameContentSizeText: `
        m-auto
    `,
    classNameTitle: `
    text-left
    `,
};
