import { useMemo, useState } from 'react';

import Stripe from '@/components/Input/Stripe';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';
import { DataFormAddPayment } from '@/interfaces/FormAddPayment';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { FormAddPaymentYup } from '@/validations/AddPayment';
import { UserLoginProps, useUser } from '@/hook/useUser';
import { ThemesType } from '@/config/theme';
import { InputTextStyles } from '@/components/Input/Text';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import { useNotification } from '@/hook/useNotification';
import { PaymentMethodResult } from '@stripe/stripe-js';
import { POST_STRIPE_ADD_CARD_USER } from '@/api/tolinkme/Payment';

export interface FormAddPaymentClassProps {
    sizeContent?: number;
    classNameContent?: string;
    styleTemplateInputs?: DataFormAddPayment<InputTextStyles | ThemesType>;
    inputs?: DataFormAddPayment<boolean>;
}

export type onSubmintAddPayment = (
    data: DataFormAddPayment
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface CustomSubmitResult<T> extends SubmitResult<T> {
    AddPaymentId?: string;
}

export interface FormAddPaymentBaseProps {
    text?: string;
    onSubmit?: onSubmintAddPayment | undefined;
    onAfterSubmit?: () => void;
    urlRediret?: string;
    onValidateUsername?: onValidateUsername;
    defaultValue?: DataFormAddPayment;
    loader?: boolean;
}
export interface FormAddPaymentProps
    extends FormAddPaymentBaseProps,
        FormAddPaymentClassProps {}

export const FormAddPaymentBase = ({
    sizeContent = -1,
    classNameContent = '',
    inputs = {
        stripe_before: true,
    },
    onAfterSubmit,
    ...props
}: FormAddPaymentProps) => {
    const _t = useLang();
    const { user } = useUser();
    const { pop } = useNotification();
    const router = useRouter();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataFormAddPayment>({
        stripe_before: {
            complete: false,
        },
    });

    const FormAddPaymentYupV = useMemo(
        () => FormAddPaymentYup(data, inputs),
        [data]
    );
    const onSaveCard = async (stripe_after: PaymentMethodResult) => {
        setLoader(true);
        const result: SubmitResult = await POST_STRIPE_ADD_CARD_USER({
            user,
            data: {
                ...data,
                stripe_after,
            },
        });

        pop({
            type: result.status,
            message: _t(result?.message ?? ''),
        });
        setLoader(false);
        if (result.status == 'ok') {
            onAfterSubmit?.();
            window.location.reload();
        }
    };

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <Form<DataFormAddPayment>
                    id="Stripe"
                    data={{ ...data }}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={FormAddPaymentYupV.v}
                    onAfterSubmit={async (
                        data: CustomSubmitResult<UserLoginProps>
                    ) => {
                        onLogin(data);
                        router.push(props?.urlRediret ?? url.pay);
                    }}
                >
                    <Space size={10} />
                    <div className="AddPayment">
                        <Stripe
                            styleTemplate="tolinkme"
                            onSaveCard={onSaveCard}
                            onChange={onChangeData('stripe_before')}
                            textBtn={_t('Add payment method')}
                            disabled={disabled}
                            options={{
                                style: {
                                    base: {
                                        iconColor: '#fff',
                                        color: '#fff',
                                        fontWeight: '500',

                                        fontSize: '15px',

                                        ':-webkit-autofill': {
                                            color: '#fff',
                                        },
                                        '::placeholder': {
                                            color: '#fff',
                                        },
                                    },
                                    invalid: {
                                        iconColor: '#da2424',
                                        color: '#a90202',
                                    },
                                },
                            }}
                        />
                    </div>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormAddPaymentBase;
