import { Button, ButtonStyles } from '@/components/Button';
import { Title, TitleStylesType, TitleStyles } from '@/components/Title';
import { Space } from '@/components/Space';
import { ContentWidth } from '@/components/ContentWidth';
// import { Image } from '@/components/Image';
import { Link, LinkStyles } from '@/components/Link';

import { useLang } from '@/lang/translate';
import url from '@/data/routes';
import Theme, { ThemesType } from '@/config/theme';
import UnicornInLove from '@/lottin/UnicornInLove';

export interface FormSuccesfullActivatedClassProps {
    classNameContent?: string;
    classNameContentTitle?: string;
    classNameContentImg?: string;
    imgFace?: string;
    classNameImg?: string;
    classNameLink?: string;

    styleTemplateTitle?: TitleStyles | ThemesType;
    styleTemplateImgFace?: ThemesType | ThemesType;
    styleTemplateLink?: LinkStyles | ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;

    typeStyleTemplateTitle?: TitleStylesType;
}

export interface FormSuccesfullActivatedBaseProps {}
export interface FormSuccesfullActivatedProps
    extends FormSuccesfullActivatedBaseProps,
        FormSuccesfullActivatedClassProps {}

export const FormSuccesfullActivatedBase = ({
    classNameContent = '',
    classNameContentTitle = '',
    classNameContentImg = '',
    // classNameImg = '',
    classNameLink = '',

    // imgFace = '',

    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    // styleTemplateImgFace = Theme.styleTemplate ?? '_default',
    styleTemplateLink = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    typeStyleTemplateTitle = 'h1',
}: FormSuccesfullActivatedProps) => {
    const _t = useLang();
    return (
        <>
            <ContentWidth size={350} className={classNameContent}>
                <Title
                    typeStyle={typeStyleTemplateTitle}
                    styleTemplate={styleTemplateTitle}
                    className={classNameContentTitle}
                >
                    {_t('Succesfully Activated!')}
                </Title>
                <Space size={25} />
                <ContentWidth size={180} className={classNameContentImg}>
                    {/* <Image
                        className={classNameImg}
                        styleTemplate={styleTemplateImgFace}
                        src={imgFace}
                    /> */}
                    <UnicornInLove />
                </ContentWidth>
                <Space size={25} />
                <Link
                    href={url.index}
                    className={classNameLink}
                    styleTemplate={styleTemplateLink}
                >
                    <Button styleTemplate={styleTemplateButton}>
                        {_t("Let's go")}
                    </Button>
                </Link>
            </ContentWidth>
        </>
    );
};
export default FormSuccesfullActivatedBase;
