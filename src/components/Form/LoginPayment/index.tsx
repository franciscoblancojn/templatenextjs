import { useMemo } from 'react';

import * as styles from '@/components/Form/LoginPayment/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormLoginPaymentBaseProps,
    FormLoginPaymentBase,
} from '@/components/Form/LoginPayment/Base';

export const FormLoginPaymentStyle = { ...styles } as const;

export type FormLoginPaymentStyles = keyof typeof FormLoginPaymentStyle;

export interface FormLoginPaymentProps extends FormLoginPaymentBaseProps {
    styleTemplate?: FormLoginPaymentStyles | ThemesType;
}

export const FormLoginPayment = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormLoginPaymentProps) => {
    const Style = useMemo(
        () =>
            FormLoginPaymentStyle[styleTemplate as FormLoginPaymentStyles] ??
            FormLoginPaymentStyle._default,
        [styleTemplate]
    );

    return <FormLoginPaymentBase {...Style} {...props} />;
};
export default FormLoginPayment;
