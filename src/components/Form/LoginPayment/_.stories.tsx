import { Story, Meta } from "@storybook/react";

import { FormLoginPaymentProps, FormLoginPayment } from "./index";
import { DataLoginPayment } from "@/interfaces/LoginPayment";
import log from "@/functions/log";

export default {
    title: "Form/LoginPayment",
    component: FormLoginPayment,
} as Meta;

const Template: Story<FormLoginPaymentProps> = (args) => <FormLoginPayment {...args} />;

export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataLoginPayment)=> {
        log("DataLoginPayment",data)
        await new Promise((r) => setTimeout(r, 2000));

        return {
            status: "ok",
            message: "register ok",
            data:{
                id:"1",
                name:"test",
                token:"1234"
            }
        };
    }
};

export const Error_ = Template.bind({});
Error_.args = {
    onSubmit:  async (data: DataLoginPayment)=> {
        log("DataLoginPayment",data)
        await new Promise((r) => setTimeout(r, 2000));
        throw {
            message : "Error"
        }
    }
};
