import { useMemo } from 'react';

import * as styles from '@/components/Form/Register/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormRegisterBaseProps,
    FormRegisterBase,
} from '@/components/Form/Register/Base';

export const FormRegisterStyle = { ...styles } as const;

export type FormRegisterStyles = keyof typeof FormRegisterStyle;

export interface FormRegisterProps extends FormRegisterBaseProps {
    styleTemplate?: FormRegisterStyles | ThemesType;
}

export const FormRegister = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormRegisterProps) => {
    const Style = useMemo(
        () =>
            FormRegisterStyle[styleTemplate as FormRegisterStyles] ??
            FormRegisterStyle._default,
        [styleTemplate]
    );

    return <FormRegisterBase {...Style} {...props} />;
};
export default FormRegister;
