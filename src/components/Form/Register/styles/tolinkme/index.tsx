import { FormRegisterClassProps } from '@/components/Form/Register/Base';

export const tolinkme: FormRegisterClassProps = {
    sizeContent: 300,
    classNameContent: `
    m-h-auto
    `,
    classNameContentTitle: `
    text-center color-white
    `,
    classNameContentLinkRegister: `
    text-center
    `,
    styleTemplateInputs: {
        userName: 'tolinkme',
        email: 'tolinkme',
        firstName: 'tolinkme',
        lastName: 'tolinkme',
        phone: 'tolinkme',
        password: 'tolinkme',
        repeatPassword: 'tolinkme',
    },
    styleTemplateButton: 'tolinkme',
    styleTemplateLinkRegister: 'tolinkme',

    inputs: {
        email: true,
        userName: true,
        firstName: false,
        lastName: false,
        phone: false,
        password: true,
        repeatPassword: true,
    },
    rsLogin: null,
};
