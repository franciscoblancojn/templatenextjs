import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { InputMe } from '@/components/Input/Me';
import { InputEmail } from '@/components/InputYup/Email';
import {
    InputPassword,
    InputRepeatPassword,
} from '@/components/InputYup/Password';
import { Title, TitleStylesType, TitleStyles } from '@/components/Title';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { LinkLogin } from '@/components/Links';
import { ContentWidth } from '@/components/ContentWidth';

import { DataRegister } from '@/interfaces/Register';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { RegisterYup } from '@/validations/Register';
import { UserLoginProps, useUser } from '@/hook/useUser';
import Theme, { ThemesType } from '@/config/theme';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import { LinkStyles } from '@/components/Link';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import InputTel, { InputTelStyles } from '@/components/Input/Tel';
import { DataRsLogin } from '@/interfaces/RsLogin';
import ButtonGoogle, { ButtonGoogleStyles } from '@/components/ButtonGoogle';
import ButtonFacebook from '@/components/ButtonFacebook';

export interface FormRegisterClassProps {
    sizeContent?: number;
    classNameContent?: string;
    classNameContentTitle?: string;
    classNameContentLinkRegister?: string;

    styleTemplateTitle?: TitleStyles | ThemesType;
    typeStyleTemplateTitle?: TitleStylesType;

    stylesRsLogin?: {
        classNameContainer?: string;
        classNameContent?: string;
        sizeContent?: number;
        styleTemplateButtonGogle?: ButtonGoogleStyles | ThemesType;
        styleTemplateButtonFacebook?: ButtonGoogleStyles | ThemesType;
    };
    sizeContentInputs?: number;
    classNameContentInputs?: string;

    styleTemplateInputs?: DataRegister<InputTextStyles | ThemesType>;
    inputs?: DataRegister<boolean>;

    rsLogin?: DataRsLogin<boolean> | null;

    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateLinkRegister?: LinkStyles | ThemesType;

    LinkLogin?: string;
}

export type onSubmintRegister = (
    data: DataRegister
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormRegisterBaseProps {
    onSubmit?: onSubmintRegister;
    onValidateUsername?: onValidateUsername;
    urlRediret?: string;
}
export interface FormRegisterProps
    extends FormRegisterBaseProps,
        FormRegisterClassProps {}

export const FormRegisterBase = ({
    sizeContent = -1,
    classNameContent = '',
    classNameContentTitle = '',
    classNameContentLinkRegister = '',

    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    typeStyleTemplateTitle = 'h1',

    stylesRsLogin = {
        classNameContainer: '',
        classNameContent: '',
        sizeContent: -1,
        styleTemplateButtonGogle: Theme.styleTemplate ?? '_default',
        styleTemplateButtonFacebook: Theme.styleTemplate ?? '_default',
    },
    sizeContentInputs = -1,
    classNameContentInputs = '',

    styleTemplateInputs = {
        userName: Theme.styleTemplate ?? '_default',
        email: Theme.styleTemplate ?? '_default',
        firstName: Theme.styleTemplate ?? '_default',
        lastName: Theme.styleTemplate ?? '_default',
        password: Theme.styleTemplate ?? '_default',
        repeatPassword: Theme.styleTemplate ?? '_default',
        phone: Theme.styleTemplate ?? '_default',
    },

    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateLinkRegister = Theme.styleTemplate ?? '_default',

    inputs = {
        email: true,
        userName: true,
        firstName: true,
        lastName: true,
        phone: true,
        password: true,
        repeatPassword: true,
    },

    rsLogin = {
        google: true,
        facebook: true,
    },

    ...props
}: FormRegisterProps) => {
    const _t = useLang();
    const router = useRouter();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataRegister>({});

    const RegisterYupV = useMemo(() => RegisterYup(data, inputs), [data]);

    const validateUserName = async () => {
        setDisabled(true);
        await props?.onValidateUsername?.(data?.userName ?? '');
    };

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <Form<DataRegister, UserLoginProps>
                    id="register"
                    data={data}
                    onSubmit={props?.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={RegisterYupV}
                    validateAfterYup={validateUserName}
                    onAfterSubmit={(data) => {
                        onLogin(data);
                        router.push(props?.urlRediret ?? url.index);
                    }}
                >
                    <Title
                        typeStyle={typeStyleTemplateTitle}
                        styleTemplate={styleTemplateTitle}
                        className={classNameContentTitle}
                    >
                        {_t('Register')}
                    </Title>
                    <Space size={12} />
                    {rsLogin ? (
                        <>
                            <div className={stylesRsLogin.classNameContainer}>
                                <ContentWidth
                                    size={stylesRsLogin.sizeContent}
                                    className={stylesRsLogin.classNameContent}
                                >
                                    {rsLogin?.google ? (
                                        <ButtonGoogle
                                            styleTemplate={
                                                stylesRsLogin.styleTemplateButtonGogle
                                            }
                                        >
                                            {_t('Sign Up with Google')}
                                        </ButtonGoogle>
                                    ) : (
                                        <></>
                                    )}
                                    {rsLogin?.facebook ? (
                                        <ButtonFacebook
                                            styleTemplate={
                                                stylesRsLogin.styleTemplateButtonFacebook
                                            }
                                        >
                                            {_t('Sign Up with Facebook')}
                                        </ButtonFacebook>
                                    ) : (
                                        <></>
                                    )}
                                </ContentWidth>
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                    <ContentWidth
                        size={sizeContentInputs}
                        className={classNameContentInputs}
                    >
                        {inputs.userName ? (
                            <>
                                <InputMe
                                    styleTemplate={styleTemplateInputs.userName}
                                    defaultValue={data.userName}
                                    placeholder={_t('/username')}
                                    onChange={onChangeData('userName')}
                                    onChangeValidateAfterYup={
                                        props?.onValidateUsername
                                    }
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.firstName ? (
                            <>
                                <InputText
                                    styleTemplate={
                                        styleTemplateInputs.firstName
                                    }
                                    defaultValue={data.firstName}
                                    placeholder={_t('First Name')}
                                    onChange={onChangeData('firstName')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.lastName ? (
                            <>
                                <InputText
                                    styleTemplate={styleTemplateInputs.lastName}
                                    defaultValue={data.lastName}
                                    placeholder={_t('Last Name')}
                                    onChange={onChangeData('lastName')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}

                        {inputs.phone ? (
                            <>
                                <InputTel
                                    styleTemplate={
                                        styleTemplateInputs.phone as InputTelStyles
                                    }
                                    placeholder={_t('Phone')}
                                    onChange={(data) => {
                                        onChangeData('phone')(
                                            `${data.code}-${data.tel}`
                                        );
                                    }}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.email ? (
                            <>
                                <InputEmail
                                    styleTemplate={styleTemplateInputs.email}
                                    defaultValue={data.email}
                                    placeholder={_t('Email')}
                                    onChange={onChangeData('email')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.password ? (
                            <>
                                <InputPassword
                                    styleTemplate={styleTemplateInputs.password}
                                    defaultValue={data.password}
                                    placeholder={_t('Password')}
                                    onChange={onChangeData('password')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.repeatPassword ? (
                            <>
                                <InputRepeatPassword
                                    styleTemplate={
                                        styleTemplateInputs.repeatPassword
                                    }
                                    defaultValue={data.repeatPassword}
                                    password={data.password}
                                    placeholder={_t('Repeat Password')}
                                    onChange={onChangeData('repeatPassword')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        <Space size={6} />
                        <Button
                            styleTemplate={styleTemplateButton}
                            disabled={disabled}
                            loader={loader}
                        >
                            {_t('Create Account')}
                        </Button>
                        <Space size={15} />
                        <div className={classNameContentLinkRegister}>
                            <LinkLogin
                                styleTemplate={styleTemplateLinkRegister}
                                href={props?.LinkLogin}
                            />
                        </div>
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormRegisterBase;
