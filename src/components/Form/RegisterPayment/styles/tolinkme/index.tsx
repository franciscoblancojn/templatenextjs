import { FormRegisterPaymentClassProps } from '@/components/Form/RegisterPayment/Base';

export const tolinkme: FormRegisterPaymentClassProps = {
    sizeContent: 300,
    classNameContent: `
    m-h-auto
    `,
    styleTemplateInputs: {
        userName: 'tolinkme',
        email: 'tolinkme',
        firstName: 'tolinkme',
        lastName: 'tolinkme',
        phone: 'tolinkme',
        password: 'tolinkme',
        repeatPassword: 'tolinkme',
    },
    styleTemplateButton: 'tolinkme',

    inputs: {
        email: true,
        userName: true,
        firstName: false,
        lastName: false,
        phone: false,
        password: true,
        repeatPassword: true,
    },
};
