import { Story, Meta } from "@storybook/react";

import { FormRegisterPaymentProps, FormRegisterPayment } from "./index";
import { DataRegisterPayment } from "@/interfaces/RegisterPayment";
import log from "@/functions/log";

export default {
    title: "Form/RegisterPayment",
    component: FormRegisterPayment,
} as Meta;

const Template: Story<FormRegisterPaymentProps> = (args) => <FormRegisterPayment {...args} />;

export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataRegisterPayment)=> {
        log("DataRegisterPayment",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "RegisterPayment ok",
        };
    },
    onValidateUsername:  async (data: string)=> {
        log("onValidateUsername",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }
    },

};
