import { useMemo } from 'react';

import * as styles from '@/components/Form/Payment/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormPaymentBaseProps,
    FormPaymentBase,
} from '@/components/Form/Payment/Base';

export const FormPaymentStyle = { ...styles } as const;

export type FormPaymentStyles = keyof typeof FormPaymentStyle;

export interface FormPaymentProps extends FormPaymentBaseProps {
    styleTemplate?: FormPaymentStyles | ThemesType;
}

export const FormPayment = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormPaymentProps) => {
    const Style = useMemo(
        () =>
            FormPaymentStyle[styleTemplate as FormPaymentStyles] ??
            FormPaymentStyle._default,
        [styleTemplate]
    );

    return <FormPaymentBase {...Style} {...props} />;
};
export default FormPayment;
