import { FormMyAccountClassProps } from '@/components/Form/MyAccount/Base';

export const tolinkme: FormMyAccountClassProps = {
    classNameContent: `
        p-h-17
    `,
    classNameContentTitle: `
        flex 
        flex-align-center
        m-b-20
    `,
    classNameTitle: `
        text-center color-greyishBrown font-25 
        m-r-auto
    `,
    styleTemplateGoBack: 'tolinkme2',
    styleTemplateButtonCancel: 'tolinkmeCancel',
    styleTemplateButtonSave: 'tolinkme2',
    styleTemplateButtonEdit: 'tolinkme3',
    styleTemplateInputText: 'tolinkme2',
    styleTemplateInputTel: 'tolinkme5',
    styleTemplateCategoriesStyles: 'tolinkme',
    styleTemplateInputPassword: 'tolinkme2',
    styleTemplateLinkStyle: 'tolinkme4',
    classNameContentList: `
        flex 
    `,
    classNameContentListColums: `
        flex flex-12 flex-md-6
    `,
    classNameContentEmailLink: `
        flex flex-justify-between flex-nowrap
    `,
    classNameContentEmail: `
       width-p-100 width-md-p-70
    `,
    classNameContentResendEmail: `
        width-p-100 width-md-p-30
        p-l-5
        p-b-5
        flex
        flex-align-end
        flex-justify-right
    `,
    classNameContentPhoneNumberLink: `
        flex flex-justify-between flex-nowrap
    `,
    classNameContentEmailLabelText: `
        flex 
        flex-align-center 
        flex-gap-column-5
        flex-nowrap
        text-white-space-nowrap
    `,
    classNameContentDelete: `
        flex
        flex-justify-right
        m-t-25
        m-b-25
    `,
    styleTemplateButtonDelete: 'tolinkmeDelete',
    classNameContentInputEdit: `
        width-p-100
    `,
};
