import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { CategoriesStyles } from '@/components/Categories';
import { Title } from '@/components/Title';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataMyAccount } from '@/interfaces/MyAccount';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { MyAccountYup } from '@/validations/MyAccount';

import InputText, { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import InputTel, { InputTelStyles } from '@/components/Input/Tel';
import GoBack, { GoBackStyles } from '@/components/GoBack';
import InputPassword from '@/components/InputYup/Password';
import Link from '@/components/Link';
import { LinkStyles } from '@/components/Link';
import Verify from '@/components/Verify';
import url from '@/data/routes';
// import CategoriesSelected from "@/components/CategoriesSelected";
import { InputRepeatPassword } from '@/components/InputYup/Password';

export interface FormMyAccountClassProps {
    classNameContent?: string;
    classNameContentTitle?: string;
    classNameTitle?: string;
    classNameContentList?: string;
    classNameContentListColums?: string;
    classNameContentResendEmail?: string;
    classNameContentEmailLink?: string;
    classNameContentEmail?: string;
    classNameContentPhoneNumberLink?: string;
    classNameContentEmailLabelText?: string;
    classNameContentDelete?: string;
    classNameContentInputEdit?: string;

    styleTemplateGoBack?: GoBackStyles | ThemesType;
    styleTemplateButtonDelete?: ButtonStyles | ThemesType;
    styleTemplateButtonCancel?: ButtonStyles | ThemesType;
    styleTemplateButtonSave?: ButtonStyles | ThemesType;
    styleTemplateButtonEdit?: ButtonStyles | ThemesType;
    styleTemplateInputText?: InputTextStyles | ThemesType;
    styleTemplateInputTel?: InputTelStyles | ThemesType;
    styleTemplateCategoriesStyles?: CategoriesStyles | ThemesType;
    styleTemplateInputPassword?: InputTextStyles | ThemesType;
    styleTemplateLinkStyle?: LinkStyles | ThemesType;
}

export type onSubmintMyAccount = (
    data: DataMyAccount
) => Promise<SubmitResult> | SubmitResult;

export interface FormMyAccountBaseProps {
    defaultData: DataMyAccount;
    onSubmit?: onSubmintMyAccount;
    onSendEmail?: () => void;
    onDelete?: () => Promise<SubmitResult>;
}

export interface FormMyAccountProps
    extends FormMyAccountClassProps,
        FormMyAccountBaseProps {}
export const FormMyAccountBase = ({
    classNameContent = '',
    classNameContentTitle = '',
    classNameTitle = '',
    classNameContentList = '',
    classNameContentListColums = '',
    classNameContentResendEmail = '',
    classNameContentEmailLink = '',
    classNameContentEmail = '',
    classNameContentPhoneNumberLink = '',
    classNameContentEmailLabelText = '',
    classNameContentDelete = '',
    classNameContentInputEdit = '',

    styleTemplateGoBack = Theme.styleTemplate ?? '_default',
    styleTemplateButtonDelete = Theme.styleTemplate ?? '_default',
    styleTemplateButtonCancel = Theme.styleTemplate ?? '_default',
    styleTemplateButtonSave = Theme.styleTemplate ?? '_default',
    styleTemplateButtonEdit = Theme.styleTemplate ?? '_default',
    styleTemplateInputText = Theme.styleTemplate ?? '_default',
    styleTemplateInputTel = Theme.styleTemplate ?? '_default',
    // styleTemplateCategoriesStyles = Theme.styleTemplate ?? "_default",
    styleTemplateInputPassword = Theme.styleTemplate ?? '_default',
    styleTemplateLinkStyle = Theme.styleTemplate ?? '_default',

    defaultData,

    ...props
}: FormMyAccountProps) => {
    const _t = useLang();
    const [edit, setEdit] = useState<boolean>(false);
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData, setData } = useData<DataMyAccount>(defaultData);

    const onEdit = () => {
        setEdit(true);
    };
    const onSave = async () => {
        const result = await props?.onSubmit?.(data);
        setEdit(false);
        if (result) {
            return result;
        }
        const r: SubmitResult = {
            status: 'ok',
        };
        return r;
    };
    const onCancel = () => {
        setData(defaultData);
        setEdit(false);
    };
    const onDelete = async () => {
        const result = await props?.onDelete?.();
        setEdit(false);
        return result;
    };

    const validate = useMemo(() => MyAccountYup(data), [data]);

    return (
        <>
            <ContentWidth className={classNameContent} size={800}>
                <Form<DataMyAccount>
                    id="my-account"
                    data={data}
                    onSubmit={onSave}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={validate}
                >
                    <Space size={16} />
                    <GoBack
                        styleTemplate={styleTemplateGoBack}
                        className={`${edit ? 'opacity-0' : ''}`}
                    />
                    <Space size={16} />
                    <div className={classNameContentTitle}>
                        <Title className={classNameTitle}>
                            {_t('My Account')}
                        </Title>
                        {edit ? (
                            <>
                                {!loader ? (
                                    <Button
                                        styleTemplate={
                                            styleTemplateButtonCancel
                                        }
                                        onClick={onCancel}
                                        isBtn={false}
                                    >
                                        {_t('Cancel')}
                                    </Button>
                                ) : (
                                    <></>
                                )}
                                <Button
                                    styleTemplate={styleTemplateButtonSave}
                                    loader={loader}
                                    disabled={disabled}
                                >
                                    {_t('Save')}
                                </Button>
                            </>
                        ) : (
                            <Button
                                styleTemplate={styleTemplateButtonEdit}
                                onClick={onEdit}
                                loader={loader}
                                isBtn={false}
                            >
                                {_t('Edit')}
                            </Button>
                        )}
                    </div>
                    <div className={classNameContentList}>
                        <div className={classNameContentListColums}>
                            <ContentWidth size={400}>
                                <InputText
                                    label="Your Name"
                                    value={data.name}
                                    onChange={onChangeData('name')}
                                    disabled={!edit}
                                    styleTemplate={styleTemplateInputText}
                                />
                                <Space size={13} />
                                <div className={classNameContentEmailLink}>
                                    <div
                                        className={`
                                        ${
                                            edit
                                                ? classNameContentInputEdit
                                                : classNameContentEmail
                                        }
                                        `}
                                    >
                                        <InputText
                                            label={
                                                <>
                                                    <div
                                                        className={
                                                            classNameContentEmailLabelText
                                                        }
                                                    >
                                                        {_t('Your Email')}
                                                        {!edit ? (
                                                            <Verify
                                                                verify={
                                                                    data.emailVerify
                                                                }
                                                            />
                                                        ) : (
                                                            <></>
                                                        )}
                                                    </div>
                                                </>
                                            }
                                            value={data.email}
                                            onChange={onChangeData('email')}
                                            disabled={!edit}
                                            styleTemplate={
                                                styleTemplateInputText
                                            }
                                        />
                                    </div>
                                    {!edit ? (
                                        <div
                                            className={
                                                classNameContentResendEmail
                                            }
                                        >
                                            {!data.emailVerify ? (
                                                <>
                                                    {/* TODO: pendiente activar */}
                                                    {/* <Link
                                                    onClick={props.onSendEmail}
                                                    styleTemplate={
                                                        styleTemplateLinkStyle
                                                    }
                                                    href={url.confirmEmail.index}
                                                >
                                                    {_t("Verify")}
                                                </Link> */}
                                                </>
                                            ) : (
                                                <></>
                                            )}
                                        </div>
                                    ) : (
                                        <></>
                                    )}
                                </div>
                                <Space size={13} />
                                <div
                                    className={classNameContentPhoneNumberLink}
                                >
                                    <div
                                        className={`
                                    ${
                                        edit
                                            ? classNameContentInputEdit
                                            : classNameContentEmail
                                    }
                                    `}
                                    >
                                        <InputTel
                                            label={
                                                <>
                                                    <div
                                                        className={
                                                            classNameContentEmailLabelText
                                                        }
                                                    >
                                                        {_t('Phone Number')}
                                                        {!edit ? (
                                                            <Verify
                                                                verify={
                                                                    data.telVerify
                                                                }
                                                            />
                                                        ) : (
                                                            <></>
                                                        )}
                                                    </div>
                                                </>
                                            }
                                            value={data.tel}
                                            onChange={onChangeData('tel')}
                                            disabled={!edit}
                                            styleTemplate={
                                                styleTemplateInputTel
                                            }
                                        />
                                    </div>
                                    {!edit ? (
                                        <div
                                            className={
                                                classNameContentResendEmail
                                            }
                                        >
                                            {!data.telVerify ? (
                                                <Link
                                                    styleTemplate={
                                                        styleTemplateLinkStyle
                                                    }
                                                    href={
                                                        url.confirmPhone.index
                                                    }
                                                >
                                                    {_t('Verify')}
                                                </Link>
                                            ) : (
                                                <></>
                                            )}
                                        </div>
                                    ) : (
                                        <></>
                                    )}
                                </div>
                            </ContentWidth>
                        </div>
                        <Space size={25} />
                        <div className={classNameContentListColums}>
                            {/* <CategoriesSelected
                                value={data.categories}
                                onChange={onChangeData("categories")}
                                styleTemplate={styleTemplateCategoriesStyles}
                                disabled={!edit}
                            /> */}
                        </div>
                        <Space size={25} />
                        <div className={classNameContentListColums}>
                            <ContentWidth size={400}>
                                <InputPassword
                                    label="Password"
                                    value={data.password}
                                    onChange={onChangeData('password')}
                                    disabled={!edit}
                                    placeholder={
                                        edit
                                            ? _t('New Password')
                                            : '**************'
                                    }
                                    styleTemplate={styleTemplateInputPassword}
                                    showIcon={edit}
                                />
                                {edit ? (
                                    <>
                                        <Space size={16} />
                                        <InputRepeatPassword
                                            label="Repeat Password"
                                            password={data.password}
                                            value={data.repeatPassword}
                                            onChange={onChangeData(
                                                'repeatPassword'
                                            )}
                                            disabled={!edit}
                                            placeholder="Repeat New Password"
                                            styleTemplate={
                                                styleTemplateInputPassword
                                            }
                                        />
                                    </>
                                ) : (
                                    <></>
                                )}
                            </ContentWidth>
                        </div>
                    </div>
                    <div className={classNameContentDelete}>
                        {!edit ? (
                            <Button
                                styleTemplate={styleTemplateButtonDelete}
                                onClick={onDelete}
                            >
                                {_t('Delete Account')}
                            </Button>
                        ) : (
                            <></>
                        )}
                    </div>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormMyAccountBase;
