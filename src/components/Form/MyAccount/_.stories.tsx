import { Story, Meta } from "@storybook/react";

import { FormMyAccountProps, FormMyAccount } from "./index";
import { DataMyAccount } from "@/interfaces/MyAccount";
import log from "@/functions/log";

export default {
    title: "Form/MyAccount",
    component: FormMyAccount,
} as Meta;

const Template: Story<FormMyAccountProps> = (args) => (
    <FormMyAccount {...args} />
);

export const Index = Template.bind({});
Index.args = {
    onSubmit: async (data: DataMyAccount) => {
        log("DataMyAccount",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error = false;
        if (error) {
            throw {
                message: "Error",
            };
        }

        return {
            status: "ok",
            message: "Edit ok",
        };
    },
    defaultData: {
        name: "",
        email: "",
        emailVerify: false,
        tel: {
            code: "",
            tel: "",
        },
        telVerify: true,
        password: "",
        categories: ["adult-content", "beauty", "business"],
    },
};
