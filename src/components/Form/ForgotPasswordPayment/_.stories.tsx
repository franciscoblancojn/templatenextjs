import { Story, Meta } from "@storybook/react";

import { FormForgotPasswordPaymentProps, FormForgotPasswordPayment } from "./index";
import { DataForgotPasswordPayment } from "@/interfaces/ForgotPasswordPayment";
import log from "@/functions/log";

export default {
    title: "Form/ForgotPasswordPayment",
    component: FormForgotPasswordPayment,
} as Meta;

const Template: Story<FormForgotPasswordPaymentProps> = (args) => <FormForgotPasswordPayment {...args} />;

export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataForgotPasswordPayment)=> {
        log("DataForgotPasswordPayment",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "register ok",
        };
    }
};
