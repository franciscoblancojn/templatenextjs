import { FormForgotPasswordPaymentClassProps } from '@/components/Form/ForgotPasswordPayment/Base';

export const tolinkme: FormForgotPasswordPaymentClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameContentText: `
        m-h-auto
    `,
    classNameText: `
        text-center
    `,
    TitleProps: {
        className: `
            text-center color-white
        `,
    },
    sizeContentWidth: 300,
    classNameContentWidth: `
        m-h-auto
    `,
    sizeContentWidthImg: 100,
    classNameContentWidthImg: `
        m-h-auto
    `,
    img: 'sendEmail.png',
    styleTemplateImg: 'tolinkme',
    styleTemplateButton: 'tolinkme',
};
