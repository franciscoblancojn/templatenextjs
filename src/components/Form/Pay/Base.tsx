import { useEffect, useMemo, useState } from 'react';

import { Space } from '@/components/Space';
import { Form, FormProps, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataFormPay } from '@/interfaces/FormPay';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { UserLoginProps, useUser } from '@/hook/useUser';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import Button, { ButtonStyles } from '@/components/Button';
import { FormPayYup } from '@/validations/Pay';
import Theme, { ThemesType } from '@/config/theme';
import Text from '@/components/Text';
import { CheckBoxPaymentMethod } from '@/components/Input/Checkbox/Template/CheckBoxPaymentMethod';
import Payment from '@/components/Payment';
import { InputCheckboxStyles } from '@/components/Input/Checkbox';
import FormPayment from '../Payment';
import { POST_STRIPE_AVAILABLE_CARD_USER } from '@/api/tolinkme/Payment';
import ArrowGoBack from '@/svg/arrowGoBack';
import FormAddPayment from '../AddPayment';
import { useNotification } from '@/hook/useNotification';
import Link from '@/components/Link';
import UnicornWealthy from '@/lottin/UnicornWealthy';

export interface MethodCardDataProps {
    id?: string;
    card?: {
        brand?: string;
        exp_month?: any;
        exp_year?: any;
        funding?: string;
        last4?: any;
    };
}

export interface PaymentMethodCardDataProps {
    PaymentMethodCard?: MethodCardDataProps[];
}

export interface FormPayClassProps {
    text2?: string;
    UserImg?: string;
    textBtn?: string;
    textLink?: string;
    classNameContent?: string;
    classNameContentText?: string;
    classNameContentLink?: string;
    styleTemplateButtonAddPaymnet?: ButtonStyles | ThemesType;
    styleTemplateCheckBoxPay?: InputCheckboxStyles | ThemesType;
}

export type onSubmintPay = (
    data: DataFormPay
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormPayBaseProps extends PaymentMethodCardDataProps {
    text?: string;
    Content?: AuthenticatorSelectionCriteria;
    onSubmit?: onSubmintPay;
    urlRediret?: string;
    onValidateUsername?: onValidateUsername;
    onClick?: (uuid?: string) => void;
    Next?: string;
    inputs?: DataFormPay<boolean>;
    btn_uuid: string;
}
export interface FormPayProps
    extends FormPayBaseProps,
        FormPayClassProps,
        PaymentMethodCardDataProps {}

export const FormPayBase = ({
    text2 = 'We are fully compliant with Payment Card Industry Data Security Standards.',
    inputs = {
        paymentMethodId: true,
    },
    textBtn = 'Pay Now',
    textLink = 'Add another Payment Method',
    classNameContent = '',
    classNameContentText = '',
    classNameContentLink = '',
    styleTemplateButtonAddPaymnet = Theme?.styleTemplate ?? '_default',
    styleTemplateCheckBoxPay = Theme?.styleTemplate ?? '_default',
    btn_uuid,
    PaymentMethodCard,
}: //...props
FormPayProps) => {
    const _t = useLang();
    const router = useRouter();
    const { pop } = useNotification();
    const { user } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataFormPay>({
        paymentMethodId: !!(PaymentMethodCard && PaymentMethodCard.length > 0),
    });
    const [showDeleteButton, setShowDeleteButton] = useState(true);

    const [showSecondForm, setShowSecondForm] = useState<boolean>(false);
    const [showAddPaymentMethod, setShowAddPaymentMethod] =
        useState<boolean>(false);

    const handleButtonClick = () => {
        if (PaymentMethodCard && PaymentMethodCard.length > 0) {
            setShowAddPaymentMethod(true);
        } else {
            setShowSecondForm(true);
        }
    };

    const handleBackButtonClick = () => {
        setShowSecondForm(false);
        setShowAddPaymentMethod(false);
    };

    const FormPayYupV = useMemo(() => FormPayYup(data, inputs), [data, inputs]);

    const onBuyButton: FormProps<DataFormPay>['onSubmit'] = async () => {
        const result: SubmitResult = await POST_STRIPE_AVAILABLE_CARD_USER({
            user,
            btn_uuid,
            data: {
                paymentMethodId: data.paymentMethodId,
            },
        });
        pop({
            type: result.status,
            message: _t(result?.message ?? ''),
        });
        if (result.status === 'ok') {
            setShowForm(false);
            setShowSuccessMessage(true);
        }
        return result;
    };

    const hasPaymentMethods = PaymentMethodCard && PaymentMethodCard.length > 0;
    const [showForm, setShowForm] = useState(true);
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);

    useEffect(() => {
        if (inputs.paymentMethodId && PaymentMethodCard) {
            if (PaymentMethodCard.length === 1) {
                setShowDeleteButton(false);
            }
        }
    }, [PaymentMethodCard, inputs]);

    return (
        <>
            <ContentWidth size={350} className={classNameContent}>
                <ContentWidth
                    size={330}
                    className="m-auto p-t-10 pos-a top-70 text-center"
                >
                    <Text className="font-20" styleTemplate="tolinkme27">
                        {_t('Payment Method')}
                    </Text>
                </ContentWidth>
                {hasPaymentMethods ? (
                    showSecondForm ? (
                        <>
                            <Button
                                classNameBtn="pos-a top-50 color-teal"
                                styleTemplate="tolinkme2_2"
                                onClick={handleBackButtonClick}
                            >
                                <span className="flex m-r-4 color-teal">
                                    <ArrowGoBack size={20} />
                                </span>
                                {_t('Close')}
                            </Button>
                            <FormPayment btn_uuid={btn_uuid} />
                        </>
                    ) : showAddPaymentMethod ? (
                        <>
                            <Button
                                classNameBtn="color-white  color-brightPink-hover"
                                styleTemplate="tolinkme2_5"
                                onClick={handleBackButtonClick}
                            >
                                <span className="flex m-r-4 ">
                                    <ArrowGoBack size={20} />
                                </span>
                                {_t('Close')}
                            </Button>
                            <Space size={20} />
                            <Text className="flex">
                                {_t('Add payment method')}
                            </Text>

                            <FormAddPayment
                                onAfterSubmit={() => {
                                    handleBackButtonClick();
                                }}
                            />
                        </>
                    ) : (
                        <>
                            {showForm && (
                                <Form<DataFormPay>
                                    id="Pay"
                                    data={data}
                                    onSubmit={onBuyButton}
                                    disabled={disabled}
                                    onChangeDisable={setDisabled}
                                    loader={loader}
                                    onChangeLoader={setLoader}
                                    yup={FormPayYupV.v}
                                    onAfterSubmit={() => {
                                        router.push(url.mySubscriptions);
                                    }}
                                >
                                    <div className={classNameContent}>
                                        <ContentWidth
                                            size={300}
                                            className={classNameContentText}
                                        >
                                            <Text>{_t(text2)}</Text>
                                        </ContentWidth>
                                        <ContentWidth
                                            className={classNameContentText}
                                            size={500}
                                        >
                                            {inputs.paymentMethodId &&
                                                PaymentMethodCard?.map(
                                                    (pay, i) => (
                                                        <CheckBoxPaymentMethod
                                                            key={i}
                                                            styleTemplate={
                                                                styleTemplateCheckBoxPay
                                                            }
                                                            useValue={true}
                                                            value={
                                                                data.paymentMethodId ===
                                                                    pay.id ||
                                                                (i === 0 &&
                                                                    !data.paymentMethodId)
                                                            }
                                                            card={
                                                                pay?.card?.brand
                                                            }
                                                            id={pay?.id ?? ''}
                                                            number={
                                                                pay?.card?.last4
                                                            }
                                                            onChange={(e) => {
                                                                if (e) {
                                                                    onChangeData(
                                                                        'paymentMethodId'
                                                                    )(pay.id);
                                                                } else {
                                                                    onChangeData(
                                                                        'paymentMethodId'
                                                                    )('');
                                                                }
                                                            }}
                                                            showDeleteButton={
                                                                showDeleteButton
                                                            }
                                                        />
                                                    )
                                                )}
                                            <div
                                                className={classNameContentLink}
                                            >
                                                <Button
                                                    styleTemplate={
                                                        styleTemplateButtonAddPaymnet
                                                    }
                                                    onClick={handleButtonClick}
                                                >
                                                    {_t(textLink)}
                                                </Button>
                                            </div>
                                            <Space size={60} />
                                            <div className="m-b-50 width-p-100">
                                                <Payment />
                                            </div>
                                            <Space size={20} />
                                            <Button
                                                loader={loader}
                                                disabled={disabled}
                                            >
                                                {_t(textBtn)}
                                            </Button>
                                        </ContentWidth>
                                        <Space size={50} />
                                    </div>
                                </Form>
                            )}

                            {showSuccessMessage && (
                                <ContentWidth className="m-auto" size={350}>
                                    <Space size={20} />
                                    <Text
                                        styleTemplate="tolinkme55"
                                        className="text-center color-white"
                                    >
                                        {_t('Successful purchase')}
                                    </Text>
                                    <Space size={20} />
                                    <ContentWidth className="m-auto" size={200}>
                                        <UnicornWealthy />
                                    </ContentWidth>
                                    <Space size={20} />
                                    <Link href={url.mySubscriptions}>
                                        <Button
                                            disabled={false}
                                            classNameBtn="bg-black color-white bg-black-hover"
                                        >
                                            {_t('Go to my')} {_t('Purchase')}
                                        </Button>
                                    </Link>
                                </ContentWidth>
                            )}
                        </>
                    )
                ) : (
                    <div>
                        <FormPayment btn_uuid={btn_uuid} />
                    </div>
                )}
            </ContentWidth>
        </>
    );
};

export default FormPayBase;
