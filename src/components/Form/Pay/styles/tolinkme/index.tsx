import { FormPayClassProps } from '@/components/Form/Pay/Base';

export const tolinkme: FormPayClassProps = {
    classNameContent: `
  width-p-100
  text-center
  m-auto
  p-10
  
`,

    classNameContentLink: `
  m-l-auto
  p-t-10
  
  
`,
    classNameContentText: `
  flex
  p-b-10
  p-t-10
  m-auto
`,

    styleTemplateButtonAddPaymnet: 'tolinkme2_4',
    styleTemplateCheckBoxPay: 'tolinkme3',
};
