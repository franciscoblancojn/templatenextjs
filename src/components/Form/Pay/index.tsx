import { useMemo } from 'react';

import * as styles from '@/components/Form/Pay/styles';

import { Theme, ThemesType } from '@/config/theme';

import { FormPayBaseProps, FormPayBase } from '@/components/Form/Pay/Base';

export const FormPayStyle = { ...styles } as const;

export type FormPayStyles = keyof typeof FormPayStyle;

export interface FormPayProps extends FormPayBaseProps {
    styleTemplate?: FormPayStyles | ThemesType;
}

export const FormPay = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormPayProps) => {
    const Style = useMemo(
        () =>
            FormPayStyle[styleTemplate as FormPayStyles] ??
            FormPayStyle._default,
        [styleTemplate]
    );

    return <FormPayBase {...Style} {...props} />;
};
export default FormPay;
