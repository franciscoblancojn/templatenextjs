import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';

import { BussinessInformationYup } from '@/validations/BussinessInformation';

import { useLang } from '@/lang/translate';

import InputText, { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import url from '@/data/routes';
import { useRouter } from 'next/router';

import { DataBussinessInformation } from '@/interfaces/BussinessInformation';
import { useData } from 'fenextjs-hook/cjs/useData';
import Text, { TextStyles } from '@/components/Text';
import InputAvatar from '@/components/Input/Avatar';
import InputDate from '@/components/Input/Date';
import InputTel, { InputTelStyles } from '@/components/Input/Tel';
import { InputTelValue } from '@/components/Input/Tel/Base';
import { UserLoginProps, useUser } from '@/hook/useUser';
import { InputFileDataProps } from '@/components/Input/File/Base';

export type DataTypeFormBussiness = string | Date | InputTelValue;

export interface FormBussinessInformationClassProps {
    classNameContent?: string;
    classNameContentAvatar?: string;
    classNameContentForm?: string;

    styleTemplateTextLogo?: TextStyles | ThemesType;
    styleTemplateInput?: DataBussinessInformation<
        InputTextStyles | ThemesType | InputTelStyles
    >;
    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateTexUpload?: TextStyles | ThemesType;
    inputs?: DataBussinessInformation<boolean>;
}

export type onSubmintBussinessInformation = (
    user: UserLoginProps
) => (
    data: DataBussinessInformation<DataTypeFormBussiness>
) => Promise<SubmitResult> | SubmitResult;

export interface FormBussinessInformationBaseProps {
    defaultData?: DataBussinessInformation<DataTypeFormBussiness>;
    onSubmit?: onSubmintBussinessInformation;
    urlRedirect?: string;
}

export interface FormBussinessInformationProps
    extends FormBussinessInformationClassProps,
        FormBussinessInformationBaseProps {}

export const FormBussinessInformationBase = ({
    classNameContent = '',
    classNameContentAvatar = '',
    classNameContentForm = '',

    styleTemplateInput = {
        company: Theme.styleTemplate ?? '_default',
        legalName: Theme.styleTemplate ?? '_default',
        yearFounded: Theme.styleTemplate ?? '_default',
        contactName: Theme.styleTemplate ?? '_default',
        tel: Theme.styleTemplate ?? '_default',
        alternativeTel: Theme.styleTemplate ?? '_default',
    },
    styleTemplateTextLogo = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateTexUpload = Theme.styleTemplate ?? '_default',
    inputs = {
        company: true,
        legalName: true,
        yearFounded: true,
        contactName: true,
        tel: true,
        alternativeTel: true,
    },

    urlRedirect = url.profile,

    defaultData = {},
    ...props
}: FormBussinessInformationProps) => {
    const _t = useLang();
    const { user } = useUser();
    const route = useRouter();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } =
        useData<DataBussinessInformation<DataTypeFormBussiness>>(defaultData);

    const BussinessInformationYupV = useMemo(
        () => BussinessInformationYup(data, inputs),
        [data]
    );

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentAvatar}>
                    <InputAvatar
                        styleTemplate="mooveri"
                        defaultValue={{
                            fileData: data.img,
                            text: data.contactName as string,
                        }}
                        onSubmit={(data: InputFileDataProps | undefined) => {
                            if (data) {
                                onChangeData('img')(data.fileData);
                            }
                            return {
                                status: 'ok',
                            };
                        }}
                    />
                    <Space size={10} />
                    <Text styleTemplate={styleTemplateTexUpload}>
                        {_t('**(Max Upload File Size 1MB , JPG, PNG).')}{' '}
                    </Text>
                    <Space size={1} />
                    <Text styleTemplate={styleTemplateTexUpload}>
                        {_t('**Recommended Size 400px 400px.')}{' '}
                    </Text>
                    <Space size={1} />
                    <Text styleTemplate={styleTemplateTextLogo}>
                        {_t('Your Logo')}{' '}
                    </Text>
                </div>
                <div className={classNameContentForm}>
                    <Form<DataBussinessInformation<DataTypeFormBussiness>>
                        data={data}
                        onSubmit={props?.onSubmit?.(user)}
                        disabled={disabled}
                        onChangeDisable={setDisabled}
                        loader={loader}
                        onChangeLoader={setLoader}
                        yup={BussinessInformationYupV.v}
                        onAfterSubmit={() => {
                            route.push(urlRedirect);
                        }}
                    >
                        {inputs.company ? (
                            <>
                                <InputText
                                    label={_t('Company Name')}
                                    placeholder="My Company"
                                    onChange={onChangeData('company')}
                                    defaultValue={data.company as string}
                                    styleTemplate={
                                        styleTemplateInput.company as InputTextStyles
                                    }
                                    yup={BussinessInformationYupV.y.company}
                                />
                                <Space size={10} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.legalName ? (
                            <>
                                <InputText
                                    label={_t('Legal Name')}
                                    placeholder={_t('Company LLC')}
                                    onChange={onChangeData('legalName')}
                                    defaultValue={data.legalName as string}
                                    styleTemplate={
                                        styleTemplateInput.legalName as InputTextStyles
                                    }
                                    yup={BussinessInformationYupV.y.legalName}
                                />
                                <Space size={10} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.yearFounded ? (
                            <>
                                <InputDate
                                    type="month"
                                    placeholder="1998"
                                    label={_t('Year Founded')}
                                    styleTemplate={
                                        styleTemplateInput.yearFounded as InputTextStyles
                                    }
                                    onChange={onChangeData('yearFounded')}
                                    defaultValue={data.yearFounded as Date}
                                    yup={BussinessInformationYupV.y.yearFounded}
                                />
                                <Space size={10} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.contactName ? (
                            <>
                                <InputText
                                    placeholder="My Contact Name"
                                    label={_t('Contact Name')}
                                    styleTemplate={
                                        styleTemplateInput.contactName as InputTextStyles
                                    }
                                    onChange={onChangeData('contactName')}
                                    defaultValue={data.contactName as string}
                                    yup={BussinessInformationYupV.y.contactName}
                                />

                                <Space size={10} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.tel ? (
                            <>
                                <InputTel
                                    placeholder="1956865358"
                                    label={_t('Phone')}
                                    styleTemplate={
                                        styleTemplateInput.tel as InputTelStyles
                                    }
                                    onChange={onChangeData('tel')}
                                    defaultValue={data.tel as InputTelValue}
                                    yup={BussinessInformationYupV.y.tel}
                                />

                                <Space size={10} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.alternativeTel ? (
                            <>
                                <InputTel
                                    placeholder="1956865358"
                                    label={_t('Alternative Phone')}
                                    styleTemplate={
                                        styleTemplateInput.alternativeTel as InputTelStyles
                                    }
                                    onChange={onChangeData('alternativeTel')}
                                    defaultValue={
                                        data.alternativeTel as InputTelValue
                                    }
                                    yup={
                                        BussinessInformationYupV.y
                                            .alternativeTel
                                    }
                                />
                            </>
                        ) : (
                            <></>
                        )}
                        <Space size={22} />
                        <Button
                            styleTemplate={styleTemplateButton}
                            disabled={disabled}
                            loader={loader}
                        >
                            {_t('Save')}
                        </Button>
                    </Form>
                </div>
            </div>
        </>
    );
};
export default FormBussinessInformationBase;
