import { useMemo, useState } from 'react';

import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { UserLoginProps, useUser } from '@/hook/useUser';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import Button, { ButtonStyles } from '@/components/Button';
import InputFile, { InputFileStyles } from '@/components/Input/File';
import Text, { TextStyles } from '@/components/Text';
import FrontIdentification from '@/svg/FrontIdentification';
import LaterIdentification from '@/svg/LaterIdentification';
import { DataVerifyIdentity } from '@/interfaces/VerifyIdentity';
import { VerifyIdentityYup } from '@/validations/VerifyIdentity';
import Theme, { ThemesType } from '@/config/theme';
import { InputFileDataProps } from '@/components/Input/File/Base';
export interface FormVerifyIdentityClassProps {
    text?: string;
    text2?: string;
    text3?: string;
    textBtn?: string;
    sizeContent?: number;
    classNameContent?: string;
    sizeContentInputs?: number;
    classNameContentInputs?: string;
    styleTemplateInpFile?: InputFileStyles | ThemesType;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateText2?: TextStyles | ThemesType;
    inputs?: DataVerifyIdentity<boolean>;
    styleTemplateBtn: ButtonStyles | ThemesType;
    classNamecontent3?: string;
    classNameContent2?: string;
    classNameContenttext?: string;
    sizeContentButton?: number;
    classNameContentBtn?: string;
    classNameInputDni?: string;
}

export type onSubmintVerifyIdentity = (
    data: DataVerifyIdentity
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormVerifyIdentityBaseProps {
    icon?: any;
    icon2?: any;
    onSubmit?: onSubmintVerifyIdentity;
    urlRediret?: string;
    onValidateUsername?: onValidateUsername;
    Next?: string;
    onClick?: (Next?: string) => void;
    defaultValue?: DataVerifyIdentity;
    onChange?: (data: DataVerifyIdentity) => void;
    textBtn?: string;
    extraLoader?: boolean;
}
export interface FormVerifyIdentityProps
    extends FormVerifyIdentityBaseProps,
        FormVerifyIdentityClassProps {}

export const FormVerifyIdentityBase = ({
    Next,
    onClick,
    icon = <FrontIdentification size={230} />,
    icon2 = <LaterIdentification size={230} />,
    text = 'Upload you Front ID',
    text2 = 'Drag Here',
    text3 = 'Upload you Back ID',
    textBtn = 'Next',
    inputs = {
        FrontIdentification: true,
        LaterIdentification: true,
    },
    sizeContent = -1,
    classNameContent = '',
    sizeContentInputs = -1,
    classNameContentInputs = '',
    classNamecontent3 = '',
    classNameContent2 = '',
    classNameContentBtn = '',
    classNameInputDni = '',
    styleTemplateInpFile = Theme?.styleTemplate ?? '_default',
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    styleTemplateText2 = Theme?.styleTemplate ?? '_default',
    classNameContenttext = '',
    sizeContentButton = 100,
    styleTemplateBtn = Theme?.styleTemplate ?? '_default',
    defaultValue = {},
    onChange,

    extraLoader = undefined,

    ...props
}: FormVerifyIdentityProps) => {
    const _t = useLang();
    const router = useRouter();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);

    const { data, onChangeData } = useData<
        DataVerifyIdentity<InputFileDataProps>
    >(defaultValue, {
        onChangeDataAfter: onChange,
    });
    const FormVerifyIdentityYupV = useMemo(
        () => VerifyIdentityYup(data, inputs),
        [data]
    );
    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <Form<DataVerifyIdentity>
                    id="VerifyIdentity"
                    data={data}
                    onSubmit={props?.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={FormVerifyIdentityYupV}
                    onAfterSubmit={(data) => {
                        onLogin(data);
                        router.push(props?.urlRediret ?? url.index);
                    }}
                >
                    <ContentWidth
                        size={sizeContentInputs}
                        className={classNameContentInputs}
                    >
                        <Space size={20} />

                        {inputs.FrontIdentification ? (
                            <InputFile
                                defaultValue={
                                    data.FrontIdentification?.fileData
                                }
                                onChange={(fileData) => {
                                    onChangeData('FrontIdentification')(
                                        fileData
                                    );
                                }}
                                styleTemplate={styleTemplateInpFile}
                            >
                                <div className={classNameContent2}>
                                    <div className={classNameContenttext}>
                                        <Text styleTemplate={styleTemplateText}>
                                            {_t(text)}
                                        </Text>
                                    </div>

                                    <div>{icon}</div>

                                    <div className={classNameContenttext}>
                                        <Text
                                            styleTemplate={styleTemplateText2}
                                        >
                                            {_t(text2)}
                                        </Text>
                                    </div>
                                </div>
                            </InputFile>
                        ) : (
                            <></>
                        )}

                        <Space size={15} />
                        <div className={classNameInputDni}>
                            {inputs.LaterIdentification ? (
                                <InputFile
                                    defaultValue={
                                        data.LaterIdentification?.fileData
                                    }
                                    onChange={(fileData) => {
                                        onChangeData('LaterIdentification')(
                                            fileData
                                        );
                                    }}
                                    styleTemplate={styleTemplateInpFile}
                                >
                                    <div className={classNameContent2}>
                                        <div className={classNameContenttext}>
                                            <Text
                                                styleTemplate={
                                                    styleTemplateText
                                                }
                                            >
                                                {_t(text3)}
                                            </Text>
                                        </div>

                                        <div>{icon2}</div>
                                        <div className={classNameContenttext}>
                                            <Text
                                                styleTemplate={
                                                    styleTemplateText2
                                                }
                                            >
                                                {_t(text2)}
                                            </Text>
                                        </div>
                                    </div>
                                </InputFile>
                            ) : (
                                <></>
                            )}
                        </div>
                        <div className={classNamecontent3}>
                            <ContentWidth
                                className={classNameContentBtn}
                                size={sizeContentButton}
                            >
                                <Button
                                    disabled={disabled}
                                    loader={extraLoader ?? loader}
                                    styleTemplate={styleTemplateBtn}
                                    onClick={() => {
                                        onClick?.(Next);
                                    }}
                                >
                                    {_t(textBtn)}
                                </Button>
                            </ContentWidth>
                        </div>
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormVerifyIdentityBase;
