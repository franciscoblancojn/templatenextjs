import { useMemo } from 'react';

import * as styles from '@/components/Form/VerifyIdentity/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormVerifyIdentityBaseProps,
    FormVerifyIdentityBase,
} from '@/components/Form/VerifyIdentity/Base';

export const FormVerifyIdentityStyle = { ...styles } as const;

export type FormVerifyIdentityStyles = keyof typeof FormVerifyIdentityStyle;

export interface FormVerifyIdentityProps extends FormVerifyIdentityBaseProps {
    styleTemplate?: FormVerifyIdentityStyles | ThemesType;
}

export const FormVerifyIdentity = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormVerifyIdentityProps) => {
    const Style = useMemo(
        () =>
            FormVerifyIdentityStyle[
                styleTemplate as FormVerifyIdentityStyles
            ] ?? FormVerifyIdentityStyle._default,
        [styleTemplate]
    );

    return <FormVerifyIdentityBase {...Style} {...props} />;
};
export default FormVerifyIdentity;
