import { useMemo } from 'react';

import * as styles from '@/components/Form/ForgotPassword/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormForgotPasswordBaseProps,
    FormForgotPasswordBase,
} from '@/components/Form/ForgotPassword/Base';

export const FormForgotPasswordStyle = { ...styles } as const;

export type FormForgotPasswordStyles = keyof typeof FormForgotPasswordStyle;

export interface FormForgotPasswordProps extends FormForgotPasswordBaseProps {
    styleTemplate?: FormForgotPasswordStyles | ThemesType;
}

export const FormForgotPassword = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormForgotPasswordProps) => {
    const Style = useMemo(
        () =>
            FormForgotPasswordStyle[
                styleTemplate as FormForgotPasswordStyles
            ] ?? FormForgotPasswordStyle._default,
        [styleTemplate]
    );

    return <FormForgotPasswordBase {...Style} {...props} />;
};
export default FormForgotPassword;
