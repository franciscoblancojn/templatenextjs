import { FormForgotPasswordClassProps } from '@/components/Form/ForgotPassword/Base';

export const mooveri: FormForgotPasswordClassProps = {
    classNameContent: `
        m-h-auto
        p-h-15
    `,
    sizeContent: 330,
    classNameContentText: `
        m-h-auto
    `,
    classNameText: `
        color-warmGreyThree
        font-13 
        text-center
    `,
    styleTemplateEmail: 'mooveri',
    styleTemplateButton: 'mooveri',
    TitleProps: {
        className: `
            color-sea 
            m-h-auto 
            text-center 
            font-nunito
            font-w-900
            font-28
        `,
        styleTemplate: 'mooveri',
        typeStyle: 'h7',
    },
};
