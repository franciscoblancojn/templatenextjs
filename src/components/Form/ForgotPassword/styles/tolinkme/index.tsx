import { FormForgotPasswordClassProps } from '@/components/Form/ForgotPassword/Base';

export const tolinkme: FormForgotPasswordClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameContentText: `
        m-h-auto
    `,
    classNameText: `
        text-center
    `,
    TitleProps: {
        className: `
            text-center color-white
        `,
    },
};
