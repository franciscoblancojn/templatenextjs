import { useMemo } from 'react';

import * as styles from '@/components/ListItemScan/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ListItemScanBaseProps,
    ListItemScanBase,
} from '@/components/ListItemScan/Base';

export const ListItemScanStyle = { ...styles } as const;

export type ListItemScanStyles = keyof typeof ListItemScanStyle;

export interface ListItemScanProps extends ListItemScanBaseProps {
    styleTemplate?: ListItemScanStyles | ThemesType;
}

export const ListItemScan = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ListItemScanProps) => {
    const Style = useMemo(
        () =>
            ListItemScanStyle[styleTemplate as ListItemScanStyles] ??
            ListItemScanStyle._default,
        [styleTemplate]
    );

    return <ListItemScanBase {...Style} {...props} />;
};
export default ListItemScan;
