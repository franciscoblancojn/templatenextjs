import { Story, Meta } from "@storybook/react";

import { ListItemScanProps, ListItemScan } from "./index";

export default {
    title: "ListItemScan/ListItemScan",
    component: ListItemScan,
} as Meta;

const ListItemScanIndex: Story<ListItemScanProps> = (args) => (
    <ListItemScan {...args}>Test Children</ListItemScan>
);

export const Index = ListItemScanIndex.bind({});
Index.args = {};
