import Theme, { ThemesType } from '@/config/theme';
import Button, { ButtonStyles } from '../Button';
import Balance, { BalanceStyles } from '../Balance';
import Subscriptors from '../Subscriptors';
import Tiktok from '@/svg/tiktok';
import money from '@/functions/money';
import Text, { TextStyles } from '../Text';
import Link, { LinkStyles } from '../Link';
import ContentWidth from '../ContentWidth';

export interface RequestClassProps {
    text?: string;
    text2?: string;
    price?: number;
    price2?: number;
    price3?: number;
    numSub?: number;
    textSub?: string;
    textBtn?: string;
    textLink?: string;
    textSub2?: string;
    textSub3?: string;
    textSub4?: string;
    textSub5?: string;
    textPrice?: string;
    priceMoney?: number;
    priceMoney2?: number;
    textBtnTiktok?: string;
    priceMoneySubs?: number;
    styleTemplateBtn?: ButtonStyles | ThemesType;
    styleTemplateBtn2?: ButtonStyles | ThemesType;
    styleTemplateLink?: LinkStyles | ThemesType;
    styleTemplateTextSub?: TextStyles | ThemesType;
    styleTemplateBalance?: BalanceStyles | ThemesType;
    styleTemplateTextPrice?: TextStyles | ThemesType;
    classNameLink?: string;
    classNameMoney?: string;
    classNameConten?: string;
    classNameConten2?: string;
    classNameContentBtn?: string;
    classNameContentMoney?: string;
    classNameContentRequest?: string;
    classNameContentBtnMoney?: string;
    classNameContentMoneySub?: string;
}

export interface RequestBaseProps {}

export interface RequestProps extends RequestClassProps, RequestBaseProps {}

export const RequestBase = ({
    text = 'Current Balance',
    text2 = 'Pending Balance',
    price = 8.5,
    price2 = 18.5,
    price3 = 250.5,
    numSub = 100,
    textSub = '3 Buttons',
    textBtn = 'Request Withdraw',
    textLink = 'Upcoming WIthdraws',
    textSub2 = 'Subscriptors',
    textSub3 = '70 Subscriptors',
    textSub4 = '20 Subscriptors',
    textSub5 = '10 Subscriptors',
    textPrice = '/Month',
    priceMoney = 104.0,
    priceMoney2 = 200.0,
    textBtnTiktok = 'TikTok Subscribed',
    priceMoneySubs = 3465.0,
    styleTemplateBtn = Theme.styleTemplate ?? '_default',
    styleTemplateBtn2 = Theme.styleTemplate ?? '_default',
    styleTemplateLink = Theme.styleTemplate ?? '_default',
    styleTemplateTextSub = Theme.styleTemplate ?? '_default',
    styleTemplateBalance = Theme.styleTemplate ?? '_default',
    styleTemplateTextPrice = Theme.styleTemplate ?? '_default',
    classNameLink = '',
    classNameMoney = '',
    classNameConten = '',
    classNameConten2 = '',
    classNameContentBtn = '',
    classNameContentMoney = '',
    classNameContentRequest = '',
    classNameContentBtnMoney = '',
    classNameContentMoneySub = '',
}: RequestProps) => {
    return (
        <>
            <ContentWidth className={classNameContentRequest}>
                <div className={classNameConten}>
                    <Button styleTemplate={styleTemplateBtn}>{textBtn}</Button>
                    <div className={classNameLink}>
                        <Link styleTemplate={styleTemplateLink}>
                            {textLink}
                        </Link>
                    </div>
                </div>
                <div className={classNameConten}>
                    <Balance
                        price={priceMoney}
                        price2={priceMoney2}
                        text={text}
                        text2={text2}
                        styleTemplate={styleTemplateBalance}
                    />
                </div>
                <div className={classNameConten}>
                    <Subscriptors
                        price={priceMoneySubs}
                        text2={textSub}
                        Numbertext={numSub}
                        text3={textSub2}
                    ></Subscriptors>
                </div>
                <div className={classNameConten2}>
                    <div className={classNameContentBtnMoney}>
                        <div className={classNameContentBtn}>
                            <Button styleTemplate={styleTemplateBtn2}>
                                <Tiktok size={20} />
                                {textBtnTiktok}
                            </Button>
                        </div>
                        <div className={classNameContentMoneySub}>
                            <div className={classNameContentMoney}>
                                <div className={classNameMoney}>
                                    {money(price)}
                                </div>
                                <Text styleTemplate={styleTemplateTextPrice}>
                                    {textPrice}
                                </Text>
                            </div>
                            <Text styleTemplate={styleTemplateTextSub}>
                                {textSub3}
                            </Text>
                        </div>
                    </div>

                    <div className={classNameContentBtnMoney}>
                        <div className={classNameContentBtn}>
                            <Button styleTemplate={styleTemplateBtn2}>
                                <Tiktok size={20} />
                                {textBtnTiktok}
                            </Button>
                        </div>
                        <div className={classNameContentMoneySub}>
                            <div className={classNameContentMoney}>
                                <div className={classNameMoney}>
                                    {money(price2)}
                                </div>
                                <Text styleTemplate={styleTemplateTextPrice}>
                                    {textPrice}
                                </Text>
                            </div>
                            <Text styleTemplate={styleTemplateTextSub}>
                                {textSub4}
                            </Text>
                        </div>
                    </div>

                    <div className={classNameContentBtnMoney}>
                        <div className={classNameContentBtn}>
                            <Button styleTemplate={styleTemplateBtn2}>
                                <div>
                                    <Tiktok size={20} />
                                </div>{' '}
                                <div>{textBtnTiktok}</div>
                            </Button>
                        </div>
                        <div className={classNameContentMoneySub}>
                            <div className={classNameContentMoney}>
                                <div className={classNameMoney}>
                                    {money(price3)}
                                </div>
                                <Text styleTemplate={styleTemplateTextPrice}>
                                    {textPrice}
                                </Text>
                            </div>
                            <Text styleTemplate={styleTemplateTextSub}>
                                {textSub5}
                            </Text>
                        </div>
                    </div>
                </div>
            </ContentWidth>
        </>
    );
};
export default RequestBase;
