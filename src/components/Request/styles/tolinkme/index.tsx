import { RequestClassProps } from '@/components/Request/Base';

export const tolinkme: RequestClassProps = {
    classNameLink: `
    text-center
    p-t-14
    `,
    classNameMoney: `
    font-14 font-nunito font-w-900
    color-black
    `,
    classNameConten: `
    p-b-17
    `,
    classNameConten2: `
    p-v-30
    `,
    classNameContentBtn: `
    width-p-60
    `,
    classNameContentMoney: `
    flex
    text-right
    flex-justify-right
    p-b-5
    `,
    classNameContentRequest: `
    bg-white
    p-h-5
    p-t-29
    `,
    classNameContentBtnMoney: `
    flex
    p-b-20

    `,
    classNameContentMoneySub: `
    width-p-40
    text-right
    `,
    styleTemplateBtn: 'tolinkme',
    styleTemplateBtn2: 'tolinkme10',
    styleTemplateLink: 'tolinkme10',
    styleTemplateTextSub: 'tolinkme40',
    styleTemplateBalance: 'tolinkme',
    styleTemplateTextPrice: 'tolinkme39',
};
