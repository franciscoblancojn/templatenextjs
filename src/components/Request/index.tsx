import { useMemo } from 'react';

import * as styles from '@/components/Request/styles';

import { Theme, ThemesType } from '@/config/theme';

import { RequestBaseProps, RequestBase } from '@/components/Request/Base';

export const RequestStyle = { ...styles } as const;

export type RequestStyles = keyof typeof RequestStyle;

export interface RequestProps extends RequestBaseProps {
    styleTemplate?: RequestStyles | ThemesType;
}

export const Request = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: RequestProps) => {
    const Style = useMemo(
        () =>
            RequestStyle[styleTemplate as RequestStyles] ??
            RequestStyle._default,
        [styleTemplate]
    );

    return <RequestBase {...Style} {...props} />;
};
export default Request;
