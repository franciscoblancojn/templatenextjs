import { useMemo } from 'react';

import * as styles from '@/components/Grid/styles';

import { Theme, ThemesType } from '@/config/theme';

import { GridBaseProps, GridBase } from '@/components/Grid/Base';

export const GridStyle = { ...styles } as const;

export type GridStyles = keyof typeof GridStyle;

export interface GridProps extends GridBaseProps {
    styleTemplate?: GridStyles | ThemesType;
}

export const Grid = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: GridProps) => {
    const Style = useMemo(
        () => GridStyle[styleTemplate as GridStyles] ?? GridStyle._default,
        [styleTemplate]
    );

    return <GridBase {...Style} {...props} />;
};
export default Grid;
