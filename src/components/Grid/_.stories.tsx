import { Story, Meta } from "@storybook/react";

import { GridProps, Grid } from "./index";

export default {
    title: "Grid/Grid",
    component: Grid,
} as Meta;

const GridIndex: Story<GridProps> = (args) => <Grid {...args} />;

export const Index = GridIndex.bind({});
Index.args = {
    areas: ["A", "CB", "D", "FEE"],
    areas_md: ["A", "FEE", "CB", "D", ],
    className:"column-gap-15 row-gap-25",
    children: (
        <>
            <div className="grid-A bg-red">A</div>
            <div className="grid-B bg-blue">B</div>
            <div className="grid-C bg-green">C</div>
            <div className="grid-D bg-yellow p-25">
                D
                <div className="grid grid-columns-3 grid-rows-4 gap-10">
                    <div className=" bg-red">A</div>
                    <div className=" bg-blue">B</div>
                    <div className=" bg-green">C</div>
                    <div className=" bg-green">C</div>
                    <div className=" bg-blue">B</div>
                    <div className=" bg-red">A</div>
                    <div className=" bg-blue">B</div>
                    <div className=" bg-red">A</div>
                    <div className=" bg-green">C</div>
                </div>
            </div>
            <div className="grid-E bg-indigo">E</div>
            <div className="grid-F bg-pink">F</div>
        </>
    ),
};
