import { Story, Meta } from "@storybook/react";

import { CardNumberProps, CardNumber } from "./index";

export default {
    title: "CardNumber/CardNumber",
    component: CardNumber,
} as Meta;

const CardNumberIndex: Story<CardNumberProps> = (args) => (
    <CardNumber {...args}>Test Children</CardNumber>
);

export const Index = CardNumberIndex.bind({});
Index.args = {};
