import { Story, Meta } from "@storybook/react";

import { MonetizeProps, Monetize } from "./index";
import Unicorn from "@/svg/Unicorn";

export default {
    title: "Monetize/Monetize",
    component: Monetize,
} as Meta;

const MonetizeIndex: Story<MonetizeProps> = (args) => (
    <Monetize {...args}>Test Children</Monetize>
);

export const Index = MonetizeIndex.bind({});
Index.args = {
    text:"Start our onboarding Process, on 4 easy steps",
    title: "Monetize your Buttons",
    icon:<Unicorn size={90} />,
    textBtn2: 'Next'
};
