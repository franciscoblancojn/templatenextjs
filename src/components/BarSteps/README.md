# BarSteps

## Dependencies

[BarSteps](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/BarSteps)

```js
import { BarSteps } from '@/components/BarSteps';
```

## Import

```js
import { BarSteps, BarStepsStyles } from '@/components/BarSteps';
```

## Props

```tsx
interface BarStepsProps {}
```

## Use

```js
<BarSteps>BarSteps</BarSteps>
```
