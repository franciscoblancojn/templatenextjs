import Theme, { ThemesType } from '@/config/theme';
import { CompanyHorariosProps } from '@/interfaces/Company';
import { useData } from 'fenextjs-hook/cjs/useData';
import { ConfigSchedules, ConfigSchedulesStyles } from '../ConfigSchedules';
import InputCheckbox from '../Input/Checkbox';

export interface ConfigHorarioClassProps {
    classNameContentHorario?: string;
    classNameContentTitle?: string;

    styleTemplateConfigSchedules?: ConfigSchedulesStyles | ThemesType;
}

export interface ConfigHorarioBaseProps {
    defaultValue?: CompanyHorariosProps;
    onChange?: (data: CompanyHorariosProps) => void;
}

export interface ConfigHorarioProps
    extends ConfigHorarioClassProps,
        ConfigHorarioBaseProps {}

export const ConfigHorarioBase = ({
    classNameContentHorario = '',
    classNameContentTitle = '',

    styleTemplateConfigSchedules = Theme.styleTemplate ?? '_default',

    defaultValue = { day: 'Monday' },
    onChange,
}: ConfigHorarioProps) => {
    const { data, onChangeData } = useData<CompanyHorariosProps>(defaultValue, {
        onChangeDataAfter: onChange,
    });

    return (
        <>
            <div
                className={`classNameContentHorario ${classNameContentHorario}`}
            >
                <div
                    className={`classNameContentTitle ${classNameContentTitle}`}
                >
                    <InputCheckbox
                        defaultValue={data.active ?? false}
                        onChange={onChangeData('active')}
                        label={data.day}
                    />
                </div>
                <ConfigSchedules
                    defaultValue={data.schedules}
                    onChange={onChangeData('schedules')}
                    styleTemplate={styleTemplateConfigSchedules}
                />
            </div>
        </>
    );
};
export default ConfigHorarioBase;
