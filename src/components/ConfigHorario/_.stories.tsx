import { Story, Meta } from "@storybook/react";

import { ConfigHorarioProps, ConfigHorario } from "./index";

export default {
    title: "ConfigHorario/ConfigHorario",
    component: ConfigHorario,
} as Meta;

const ConfigHorarioIndex: Story<ConfigHorarioProps> = (args) => (
    <ConfigHorario {...args}>Test Children</ConfigHorario>
);

export const Index = ConfigHorarioIndex.bind({});
Index.args = {};
