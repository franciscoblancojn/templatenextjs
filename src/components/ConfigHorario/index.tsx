import { useMemo } from 'react';

import * as styles from '@/components/ConfigHorario/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigHorarioBaseProps,
    ConfigHorarioBase,
} from '@/components/ConfigHorario/Base';

export const ConfigHorarioStyle = { ...styles } as const;

export type ConfigHorarioStyles = keyof typeof ConfigHorarioStyle;

export interface ConfigHorarioProps extends ConfigHorarioBaseProps {
    styleTemplate?: ConfigHorarioStyles | ThemesType;
}

export const ConfigHorario = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigHorarioProps) => {
    const Style = useMemo(
        () =>
            ConfigHorarioStyle[styleTemplate as ConfigHorarioStyles] ??
            ConfigHorarioStyle._default,
        [styleTemplate]
    );

    return <ConfigHorarioBase {...Style} {...props} />;
};
export default ConfigHorario;
