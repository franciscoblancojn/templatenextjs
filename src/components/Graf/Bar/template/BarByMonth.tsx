import { Bar, BarProps } from '@/components/Graf/Bar';
import { Months } from '@/data/components/Month';

export const BarByMonth = (props: BarProps) => {
    return (
        <>
            <Bar labels={Months} {...props} />
        </>
    );
};
