import { Bar } from 'react-chartjs-2';
import type { ChartData } from 'chart.js';

import {
    Chart as ChartJS,
    CategoryScale,
    BarElement,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Properties } from 'csstype';

import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';

ChartJS.register(
    CategoryScale,
    BarElement,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export interface BarClassProps {
    ContentWidthProps?: ContentWidthProps;
    styleLine?: Properties;
}

export interface BarBaseProps extends ChartData<'bar', number[], string> {
    height?: number;
}

export interface BarProps extends BarClassProps, BarBaseProps {}

export const BarBase = ({
    ContentWidthProps = {},
    styleLine = {},
    height = 500,
    ...props
}: BarProps) => {
    return (
        <>
            <ContentWidth {...ContentWidthProps}>
                <Bar
                    data={props}
                    style={styleLine}
                    height={height}
                    options={{
                        maintainAspectRatio: false,
                        scales: {
                            y: {
                                grid: {
                                    color: styleLine.color,
                                },
                                ticks: {
                                    color: styleLine.color,
                                },
                            },
                            x: {
                                grid: {
                                    color: styleLine.color,
                                },
                                ticks: {
                                    color: styleLine.color,
                                },
                            },
                        },
                    }}
                />
            </ContentWidth>
        </>
    );
};
export default BarBase;
