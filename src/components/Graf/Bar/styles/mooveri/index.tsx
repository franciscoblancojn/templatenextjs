import { BarClassProps } from '@/components/Graf/Bar/Base';

export const mooveri: BarClassProps = {
    ContentWidthProps: {
        size: -1,
        className: `
            color-white
        `,
    },
    styleLine: {
        color: 'white',
    },
};
