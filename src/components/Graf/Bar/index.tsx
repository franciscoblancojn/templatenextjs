import { useMemo } from 'react';

import * as styles from '@/components/Graf/Bar/styles';

import { Theme, ThemesType } from '@/config/theme';

import { BarBaseProps, BarBase } from '@/components/Graf/Bar/Base';

export const BarStyle = { ...styles } as const;

export type BarStyles = keyof typeof BarStyle;

export interface BarProps extends BarBaseProps {
    styleTemplate?: BarStyles | ThemesType;
}

export const Bar = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: BarProps) => {
    const Style = useMemo(
        () => BarStyle[styleTemplate as BarStyles] ?? BarStyle._default,
        [styleTemplate]
    );

    return <BarBase {...Style} {...props} />;
};
export default Bar;
