# Template

## Dependencies

[Template](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Template)

```js
import { Template } from '@/components/Template';
```

## Import

```js
import { Template, TemplateStyles } from '@/components/Template';
```

## Props

```tsx
interface TemplateProps {}
```

## Use

```js
<Template>Template</Template>
```
