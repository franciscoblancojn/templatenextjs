import { useMemo } from 'react';

import * as styles from '@/components/Graf/Template/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    TemplateBaseProps,
    TemplateBase,
} from '@/components/Graf/Template/Base';

export const TemplateStyle = { ...styles } as const;

export type TemplateStyles = keyof typeof TemplateStyle;

export interface TemplateProps extends TemplateBaseProps {
    styleTemplate?: TemplateStyles | ThemesType;
}

export const Template = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: TemplateProps) => {
    const Style = useMemo(
        () =>
            TemplateStyle[styleTemplate as TemplateStyles] ??
            TemplateStyle._default,
        [styleTemplate]
    );

    return <TemplateBase {...Style} {...props} />;
};
export default Template;
