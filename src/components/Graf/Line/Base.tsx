import { Line } from 'react-chartjs-2';
import type { ChartData } from 'chart.js';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Properties } from 'csstype';

import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export interface LineClassProps {
    ContentWidthProps?: ContentWidthProps;
    styleLine?: Properties;
}

export interface LineBaseProps extends ChartData<'line', number[], string> {
    height?: number;
}

export interface LineProps extends LineClassProps, LineBaseProps {}

export const LineBase = ({
    ContentWidthProps = {},
    styleLine = {},
    height = 500,
    ...props
}: LineProps) => {
    return (
        <>
            <ContentWidth {...ContentWidthProps}>
                <Line
                    data={props}
                    style={styleLine}
                    height={height}
                    options={{
                        maintainAspectRatio: false,
                        scales: {
                            y: {
                                grid: {
                                    color: styleLine.color,
                                },
                                ticks: {
                                    color: styleLine.color,
                                },
                            },
                            x: {
                                grid: {
                                    color: styleLine.color,
                                },
                                ticks: {
                                    color: styleLine.color,
                                },
                            },
                        },
                    }}
                />
            </ContentWidth>
        </>
    );
};
export default LineBase;
