# Content

## Dependencies

[Header](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Header)
[Footer](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Footer)

```js
import Header from '@/components/Header';
import Footer from '@/components/Footer';
```

## Import

```js
import { Content, ContentStyles } from '@/components/Content';
```

## Props

```tsx
interface ContentProps {
    styleTemplate?: ContentStyles;
    header?: boolean;
    footer?: boolean;
    className?: string;
    children?: any;
}
```

## Use

```js
<Content>Content</Content>
```
