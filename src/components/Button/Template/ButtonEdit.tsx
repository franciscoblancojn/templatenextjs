import Button, { ButtonProps } from '@/components/Button';
import { useLang } from '@/lang/translate';
import Cancel from '@/svg/cancel';
import CheckSearch from '@/svg/checkSearch';
import Edit from '@/svg/edit';
import { useMemo } from 'react';

export interface ButtonEditProps extends ButtonProps {
    status?: 'edit' | 'cancel-save';

    onEdit?: () => void;
    onSave?: () => void;
    onCancel?: () => void;
}

export const ButtonEdit = ({ status = 'edit', ...props }: ButtonEditProps) => {
    const _t = useLang();
    const CEdit = useMemo(() => {
        if (status == 'edit') {
            return (
                <>
                    <div className="m-h-20 width-50  height-50">
                        <Button
                            {...props}
                            styleTemplate="mooveriProfile"
                            onClick={props.onEdit}
                            disabled={false}
                            isBtn={false}
                        >
                            <Edit size={16} />
                        </Button>
                    </div>
                </>
            );
        }
        if (status == 'cancel-save') {
            return (
                <>
                    <div className="height-50 m-auto flex flex-align-center m-h-10">
                        <div className="width-p-50">
                            <Button
                                {...props}
                                styleTemplate="mooveriCancel"
                                onClick={props.onCancel}
                                disabled={false}
                                isBtn={false}
                            >
                                <span className="m-r-5">
                                    <Cancel size={8} />
                                </span>
                                {_t('Cancel')}
                            </Button>
                        </div>
                        <div className="width-p-50">
                            <Button
                                {...props}
                                styleTemplate="mooveriSave"
                                onClick={props.onSave}
                            >
                                <span className="m-r-5">
                                    <CheckSearch size={8} />
                                </span>
                                {_t('Save')}
                            </Button>
                        </div>
                    </div>
                </>
            );
        }
        return <></>;
    }, [status]);
    return <>{CEdit}</>;
};
export default ButtonEdit;
