import {
    ButtonFile,
    ButtonFileProps,
} from '@/components/Button/Template/ButtonFile';
import { InputFileDataProps } from '@/components/Input/File/Base';
import { useLang } from '@/lang/translate';
import Camera from '@/svg/camera';

export interface ButtonScanProps extends ButtonFileProps {
    onScan?: (data: InputFileDataProps) => void;
    returnUrl?: boolean;
}

export const ButtonScan = ({ returnUrl = true, ...props }: ButtonScanProps) => {
    const _t = useLang();
    return (
        <>
            <ButtonFile
                file={{
                    accept: ['png', 'jpg'],
                    onChange: props?.onScan,
                    clearAfterUpload: true,
                    returnUrl: returnUrl,
                }}
                {...props}
            >
                <span className="m-r-10 flex flex-align-center">
                    <Camera size={16} />
                </span>

                {_t('Scan your place and refine your quote')}
                <span className="font-em-8 font-w-400 color-graySea  m-0 m-l-10 pos-a right-2  top-0">
                    ({_t('Optional')})
                </span>
            </ButtonFile>
        </>
    );
};
export default ButtonScan;
