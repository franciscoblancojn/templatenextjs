import { ButtonClassProps } from '@/components/Button/Base';

export const mooveri: ButtonClassProps = {
    className: `
        border-radius-10
        border-0
        font-16
        font-w-700
        font-nunito
        color-white
        width-p-100
        height-min-48
    `,
    classNameDisabled: `
        bg-whiteThree
        color-grayDisabled
        border-radius-10
        border-0
        width-p-100
        height-min-48
    `,
    classNameNotDisabled: `
        bg-gradient-greenishCyan-metallicBlue
        bg-gradient-greenishCyan2-metallicBlue2-hover
    `,
    classNameLoader: `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
        text-center
        m-h-auto
    `,
};

export const mooveriGoogle: ButtonClassProps = {
    className: `
        p-h-25
        p-v-10
        border-radius-10
        border-0
        font-14
        font-w-700
        font-nunito
        color-greyBrowd
        width-p-100
        height-min-48
        box-shadow
        flex
        flex-justify-between
        flex-align-center
        hover-scale-11
        box-shadow-y-2
        box-shadow-b-3
        box-shadow-c-black-16
        bg-white
        
    `,
    classNameLoader: `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
        text-center
    `,
    classNameDisabled: `
        filter-brightness-8
        opacity-5
        not-allowed
    `,
};
export const mooveriFacebook: ButtonClassProps = {
    className: `
        p-h-25
        p-v-10
        border-radius-10
        border-0
        font-14
        font-w-700
        font-nunito
        color-white
        width-p-100
        height-min-48
        box-shadow
        flex
        flex-justify-between
        flex-align-center
        hover-scale-11
        box-shadow-y-2
        box-shadow-b-3
        box-shadow-c-black-16
        bg-FrenchBlue
    `,
    classNameLoader: `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
    `,
    classNameDisabled: `
        filter-brightness-8
        opacity-5
        not-allowed
    `,
};

export const mooveriAddPayment: ButtonClassProps = {
    className: `
        p-h-25
        p-v-10
        border-radius-8
        font-13
        font-w-900
        font-nunito
        color-greyishBrown
        color-white-hover
        width-p-100
        height-min-48
        flex
        flex-justify-center
        flex-align-center
        p-11
        bg-white
        box-shadow
        box-shadow-inset
        box-shadow-s-1
        box-shadow-c-greyishBrown
        box-shadow-c-transparent-hover
        border-0
        
        m-auto
        pointer
        bg-gradient-greenishCyan-metallicBlue-hover
        
    `,
    classNameLoader: `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
    `,
    classNameDisabled: `
        filter-brightness-8
        opacity-5
        not-allowed
    `,
};

export const mooveriSearch: ButtonClassProps = {
    ...mooveri,
    className: `
        border-radius-50 
        p-v-10 p-h-36
        border-0
        font-18
        font-w-900
        font-nunito
        color-white
        width-p-100
        height-min-48
    `,
};

export const mooveriSearchScan: ButtonClassProps = {
    className: `
        border-radius-8 
        p-v-10 p-h-10
        border-0
        font-11
        font-w-900
        font-nunito
        color-white
        width-p-100
        height-min-48
        bg-black
        bg-gradient-greenishCyan2-metallicBlue2-hover
        flex
        flex-align-center
        flex-justify-center
    `,
};

export const mooveriChat: ButtonClassProps = {
    className: `
        bg-transparent 
        border-0 
        flex 
        flex-align-center 
        flex-justify-center 
        pointer 
        color-warm-grey-two 
        color-sea-hover
    `,
};
export const mooveriProfile: ButtonClassProps = {
    className: `
        bg-graySix
        bg-warmGreyTwo-hover
        border-0
        border-radius-100
        flex
        flex-align-center
        flex-justify-center
        pointer
        color-warmGreyThree
        color-white-hover
        p-15
    `,
};

export const mooveriSave: ButtonClassProps = {
    className: `
        bg-transparent
        flex
        border-0
        flex-align-center
        flex-justify-center
        pointer
        color-sea
        color-seaDark-hover
        font-14
        font-w-900
        font-nunito
        flex-nowrap
    `,
};

export const mooveriCancel: ButtonClassProps = {
    className: `
        bg-transparent
        flex
        flex-align-center
        flex-justify-center
        pointer
        color-peachyPink
        color-warmGreyThree-hover
        font-14
        border-0
        font-w-900
        font-nunito
        flex-nowrap
    `,
};

export const mooveriBackoffice: ButtonClassProps = {
    className: `
        border-radius-10
        border-0
        font-16
        font-w-700
        font-nunito
        color-white
        width-p-100
        height-min-48
    `,
    classNameDisabled: `
        bg-whiteThree
        color-grayDisabled
        border-radius-10
        border-0
        width-p-100
        height-min-48
    `,
    classNameNotDisabled: `
        bg-gradient-greenishCyan-metallicBlue
        bg-gradient-greenishCyan2-metallicBlue2-hover
    `,
    classNameLoader: `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
        text-center
        m-h-auto
    `,
};
