import { ButtonClassProps, BaseStyle } from '@/components/Button/Base';

export const tolinkme: ButtonClassProps = {
    className: `
        bg-black
        font-nunito
        font-18 font-w-900
        font-sm-19
        color-white
        p-v-15
        p-h-24
        width-p-100
        border-0
        border-radius-21
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};
export const tolinkmeButtonForm: ButtonClassProps = {
    className: `
        
        width-p-100
       
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};
export const tolinkmeHome: ButtonClassProps = {
    className: `
        bg-brightPink2
        font-nunito
        border-4
        border-brightPink3
        font-14 font-w-900
        font-sm-15
        color-white
        bg-borderBotton-hover
        color-brightPink3-hover
        border-warmGrey-hover
        border-style-solid
        p-v-10
        p-h-15
        width-p-40
        border-0
        z-index-10
        border-radius-50
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkmeHome2: ButtonClassProps = {
    className: `
        bg-brightPink3
        font-nunito
        
        bg-darkAqua-hover
        font-14 font-w-900
        font-sm-15
        color-white
        
        color-white-hover
        border-warmGrey-hover
        border-style-solid
        p-v-10
        p-h-15
        width-p-100
        border-0
        z-index-10
        border-radius-50
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkme2: ButtonClassProps = {
    className: `
        bg-darkAqua
        font-nunito
        font-14 font-w-800
        color-white
        p-v-10
        p-h-24
        color-white
        border-0
        border-radius-10
        flex flex-justify-center flex-align-center
        text-decoration-none
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkme_register: ButtonClassProps = {
    className: `
        bg-darkAqua
        bg-brightPink3-hover
        font-nunito
        font-14 font-w-800
        color-white
        p-v-10
        p-h-24
        color-white
        border-0
        border-radius-30
        flex flex-justify-center flex-align-center
        text-decoration-none
        cursor-pointer
        hover-scale-11
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkme2_2: ButtonClassProps = {
    ...tolinkme2,
    className: `
        color-teal
        font-nunito
        font-14 font-w-700
        bg-transparent
        p-v-10
        p-h-24
        border-0
        border-radius-10
        flex flex-justify-center flex-align-center
        text-decoration-underline
        cursor-pointer
    `,
};
export const tolinkme2_3: ButtonClassProps = {
    className: `
    color-darkAqua
    font-nunito
    font-12 font-w-800
    bg-transparent
    m-l-auto
    color-white
    border-0
    border-radius-10
    flex flex-justify-center flex-align-center
    text-decoration-underline
    cursor-pointer
`,
};
export const tolinkme2_4: ButtonClassProps = {
    className: `
    color-white
    font-nunito
    font-12 font-w-800
    color-brightPink-hover
    bg-transparent
    m-l-auto
    color-white
    border-0
    border-radius-10
    flex flex-justify-center flex-align-center
    text-decoration-underline
    cursor-pointer
`,
};
export const tolinkme_login: ButtonClassProps = {
    className: `
    color-white
    font-nunito
    font-14 font-w-800
    color-darkAqua-hover
    bg-transparent
    m-l-auto
    border-0
    border-radius-10
    flex flex-justify-center flex-align-center
    hover-scale-11
    cursor-pointer
`,
};
export const tolinkme2_5: ButtonClassProps = {
    ...tolinkme2,
    className: `
        color-teal
        font-nunito
        font-14 font-w-700
        bg-transparent
        border-0
        border-radius-10
        flex flex-justify-center flex-align-center
        text-decoration-underline
        cursor-pointer
    `,
};

export const tolinkme3: ButtonClassProps = {
    className: `
        bg-black
        font-nunito
        font-14 font-w-800
        color-white
        p-v-10
        p-h-24
        color-white
        border-0
        border-radius-10
        flex flex-justify-center flex-align-center
        text-decoration-none
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkme4: ButtonClassProps = {
    className: `
        bg-gradient-darkAqua-brightPink
        width-p-100
        font-nunito
        font-20
        font-w-900
        color-white
        p-v-15
        p-h-24
        color-white
        border-0
        border-radius-21
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
    `,
    classNameLoader: `
        width-27 height-27 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkmeCancel: ButtonClassProps = {
    className: `
        bg-transparent
        font-nunito
        font-14 font-w-900
        color-red
        p-0
        p-h-10
        border-0
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkmeCancel2: ButtonClassProps = {
    className: `
        bg-transparent
        font-nunito
        font-12 font-w-900
        color-red
        p-0
        p-h-10
        border-0
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkmeDelete: ButtonClassProps = {
    className: `
        bg-transparent
        font-nunito
        font-12 font-w-900
        color-red
        text-decoration-underline
        p-0
        p-h-5
        border-0
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};
export const tolinkmeDelete_2: ButtonClassProps = {
    className: `
        bg-transparent
        font-nunito
        font-12 font-w-900
        color-red
        p-0
        
        border-0
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};
export const tolinkmeSave: ButtonClassProps = {
    className: `
        bg-transparent
        font-nunito
        font-12 font-w-900
        color-green
       
        p-0
        
        border-0
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkme5: ButtonClassProps = {
    ...tolinkme,
    className: `
        btn-tolinkme5
        bg-black
        font-nunito
        font-15 font-w-900
        color-white
        p-v-25 p-sm-v-15
        p-h-5 p-sm-h-24
        width-p-100
        border-0
        border-radius-21
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
    `,
};

export const tolinkme6: ButtonClassProps = {
    className: `
        m-auto
        bg-gradient-darkAqua-brightPink
        width-p-100
        font-nunito
        font-20
        font-w-900
        color-white
        p-v-14
        p-h-24
        color-white
        border-0
        border-radius-21
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
    `,
    classNameLoader: `
        width-27 height-27 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkme9: ButtonClassProps = {
    className: `
        bg-transparent
        font-nunito
        font-18 font-w-900
        font-sm-19
        color-white
        p-v-12
        p-h-24
        width-p-100
        border-4
        border-style-solid
        border-white
        border-radius-15
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
        bg-black-hover
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};
export const tolinkme10: ButtonClassProps = {
    className: `
        bg-white
        font-nunito
        font-13 font-w-900
        color-black
        p-v-10
        width-p-100
        border-4
        border-style-solid 
        border-black
        border-radius-15
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};
export const tolinkme11: ButtonClassProps = {
    className: `
        bg-pinkishGreyFour
        font-nunito
        font-13 font-w-900
        color-black
        p-v-10
        width-p-100
        border-4
        border-style-solid 
        border-transparent
        border-radius-15
        flex flex-justify-center flex-align-center
        text-decoration-none
        flex-gap-column-15
        cursor-pointer
    `,
    classNameLoader: `
        ${BaseStyle.classNameLoader}
    `,
    classNameDisabled: `
        bg-gray-6
        color-white-3
        not-allowed
    `,
};

export const tolinkmeForgotPassword: ButtonClassProps = {
    className: `
        bg-transparent
        width-p-100
        border-0
        font-18 
        font-nunito 
        font-w-600
        color-white 
        color-brightPink-hover
        text-decoration-underline
    `,
};
export const tolinkmeDontAccount: ButtonClassProps = {
    className: `
        bg-transparent
        width-p-100
        border-0
        font-14 
        font-nunito 
        font-w-600
        color-white 
        color-brightPink-hover
        text-decoration-underline
    `,
};
