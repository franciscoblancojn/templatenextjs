import { PropsWithChildren } from 'react';
import ButtonBase, { ButtonBaseProps } from '@/components/Button/Base';
import * as styles from '@/components/Button/styles';

import { Theme, ThemesType } from '@/config/theme';

export const ButtonStyle = { ...styles } as const;

export type ButtonStyles = keyof typeof ButtonStyle;

export interface ButtonProps extends ButtonBaseProps {
    styleTemplate?: ButtonStyles | ThemesType;
}

export const Button = ({
    styleTemplate = Theme.styleTemplate ?? '_default',
    ...props
}: PropsWithChildren<ButtonProps>) => {
    const config = {
        ...(ButtonStyle[styleTemplate as ButtonStyles] ?? ButtonStyle._default),
        ...props,
    };

    return <ButtonBase {...config} />;
};
export default Button;
