export const mooveri = `
    font-11 font-nunito font-w-400
    color-warmGreyThree color-sea-hover
    text-decoration-underline
`;
export const mooveri2 = `
    font-14 font-nunito font-w-600
    color-seaDark color-warmGreyThree-hover
    text-decoration-underline
`;

export const mooveri3 = `
    font-13
    font-sm-15
    font-w-900
    color-sea
    color-metallic-blue-dark-hover
    text-decoration-underline
`;
export const mooveri3_2 = `
    ${mooveri3}
    text-white-space-nowrap
`;

export const mooveri4 = `
    font-15
    font-w-800
    color-greyishBrown
    font-nunito
`;
export const mooveri5 = `
    font-11
    font-w-900
    color-sea
    color-greyishBrown-hover
    font-nunito
`;
