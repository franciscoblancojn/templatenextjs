export const tolinkme = `
    font-14 font-nunito font-w-600
    color-white color-brightPink-hover
    text-decoration-underline
`;

export const tolinkme2 = `
    font-10 font-nunito font-w-600
    color-white color-brightPink-hover
    text-decoration-underline 
`;

export const tolinkme3 = `
    font-16 font-nunito font-w-600
    color-white color-brightPink-hover
    text-decoration-none
    opacity-7
`;

export const tolinkme4 = `
    font-14 font-nunito font-w-700
    color-darkAqua color-brightPink-hover
    text-decoration-underline
`;
export const tolinkm5 = `
    font-14 font-nunito font-w-600
    color-white color-brightPink-hover
    text-decoration-none
`;
export const tolinkme6 = `
    font-13 font-nunito font-w-900
    color-darkAqua
    text-decoration-underline
`;
export const tolinkme7 = `
    font-14 font-nunito font-w-900
    color-darkAqua
    text-decoration-underline
    
`;
export const tolinkme8 = `
    font-17 font-nunito font-w-900
    color-white
    color-brightPink-hover
    text-decoration-none

`;
export const tolinkme9 = `
    font-17 font-nunito font-w-900
    color-white
    color-brightPink-hover
    text-decoration-none
`;
export const tolinkme10 = `
    font-14 font-nunito font-w-900
    color-darkAqua
    text-decoration-underline
`;
export const tolinkme11 = `
    font-14 font-nunito font-w-900
    color-sonicSilver
    text-decoration-none
    p-l-10
`;
export const tolinkme12 = `
    font-12 font-nunito font-w-900
    color-white 
    text-decoration-underline
`;
export const tolinkme13 = `
    font-15 font-nunito font-w-900
    color-blackOlive 
    text-decoration-underline
`;
export const tolinkme14 = `
    font-14 font-nunito font-w-700
    color-teal
    text-decoration-underline
`;
export const tolinkm15 = `
    text-decoration-none
`;
