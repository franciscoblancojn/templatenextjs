import { PropsWithChildren } from 'react';
import Link_ from 'next/link';

import * as styles from '@/components/Link/styles';

import { Theme, ThemesType } from '@/config/theme';

export const LinkStyle = { ...styles } as const;

export type LinkStyles = keyof typeof LinkStyle;

export interface LinkProps extends PropsWithChildren {
    styleTemplate?: LinkStyles | ThemesType;
    href?: string;
    className?: string;
    onClick?: () => void;
    targetBlank?: boolean;
}

export const Link = ({
    children,
    href = '#',
    styleTemplate = Theme?.styleTemplate ?? '_default',
    className = '',
    onClick = () => {},
    targetBlank = false,
}: PropsWithChildren<LinkProps>) => {
    return (
        <Link_ href={href ?? '#'} target={targetBlank ? '_blank' : '_self'}>
            <a
                target={targetBlank ? '_blank' : '_self'}
                onClick={onClick}
                className={`${
                    LinkStyle[styleTemplate as LinkStyles] ??
                    LinkStyle._default ??
                    ''
                } ${className}`}
            >
                {children}
            </a>
        </Link_>
    );
};
export default Link;
