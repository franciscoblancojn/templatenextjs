# Chat

## Dependencies

[Chat](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Chat)

```js
import { Chat } from '@/components/Chat';
```

## Import

```js
import { Chat, ChatStyles } from '@/components/Chat';
```

## Props

```tsx
interface ChatProps {}
```

## Use

```js
<Chat>Chat</Chat>
```
