import { ChatClassProps } from '@/components/Chat/Base';

export const mooveri: ChatClassProps = {
    classNameContent: `
        flex
        flex-nowrap
    `,
    styleContent: {
        height: 'calc(100vh - var(--sizeHeader,0px))',
    },
    classNameContentListChat: `
        flex-3
        overflow-auto
        height-p-100
        bg-whiteChat
        box-shadow
        box-shadow-x-2
        box-shadow-b-3
        box-shadow-c-black-16
        z-index-1
    `,
    classNameContentItemChat: `
        border-0
        border-b-1
        border-style-solid
        border-black-16
    `,
    ChatUserProps: {
        styleTemplate: 'mooveri',
    },
    classNameContentChat: `
        flex-6
        height-p-100
        pos-r
        flex
        flex-column
        p-b-50
    `,
    classNameContentMessage: `
        overflow-auto
        height-p-100
        p-v-10
        p-h-0
        scroll-width-5
        scroll-color-transparent
        scroll-scrollbar-track-width-0
        scroll-scrollbar-track-color-transparent
        scroll-scrollbar-thumb-width-0
        scroll-scrollbar-thumb-color-sea
        scroll-scrollbar-thumb-border-radius-10
    `,
    classNameContentChatNoItems: `
        pos-a
        inset-0
        m-auto
        flex
        flex-align-center
        flex-justify-center
    `,
    NotItemProps: {
        styleTemplate: 'mooveri2',
    },
    MessegesUserProps: {
        styleTemplate: 'mooveri',
    },
    classNameContentFormChat: `
        pos-a
        left-0
        bottom-0
        width-p-100
        p-h-15
        p-v-10
        bg-white
    `,
    FormChatMessegesProps: {
        styleTemplate: 'mooveri',
    },
    classNameContentInfoChat: `
        flex-3
        overflow-auto
        height-p-100
        bg-whiteChat
        box-shadow
        box-shadow-x--2
        box-shadow-b-3
        box-shadow-c-black-16
        pos-r
    `,
    classNameContentInfoChatNoItems: `
        pos-a
        inset-0
        m-auto
        flex
        flex-align-center
        flex-justify-center
    `,
    NotItemDetailsProps: {
        styleTemplate: 'mooveri',
    },
};
