import CSS from 'csstype';
import { ChatUserBaseProps } from '@/components/ChatUser/Base';
import { useMemo, useState } from 'react';
import { ChatUser, ChatUserProps } from '@/components/ChatUser';
import FormChatMesseges, {
    FormChatMessegesProps,
} from '@/components/Form/ChatMesseges';
import { MessegesUser, MessegesUserProps } from '@/components/MessegesUser';
import { useUser } from '@/hook/useUser';
import { onSubmintChatMesseges } from '@/components/Form/ChatMesseges/Base';
import NotItemMessage from '@/components/NotItem/Template/Message';
import { NotItemProps } from '@/components/NotItem';
import NotItemDetails from '../NotItem/Template/Details';

export interface ChatClassProps {
    classNameContent?: string;
    styleContent?: CSS.Properties;
    classNameContentListChat?: string;
    classNameContentItemChat?: string;
    ChatUserProps?: ChatUserProps;
    classNameContentChat?: string;
    classNameContentChatNoItems?: string;

    classNameContentMessage?: string;
    NotItemProps?: NotItemProps;
    MessegesUserProps?: MessegesUserProps;

    classNameContentFormChat?: string;
    FormChatMessegesProps?: FormChatMessegesProps;

    classNameContentInfoChat?: string;
    classNameContentInfoChatNoItems?: string;
    NotItemDetailsProps?: NotItemProps;
}
export interface ChatDetalle {}
export interface ChatMessages {
    id?: string;
    dateSend?: Date;
    dateView?: Date;
    content?: any;
    my?: boolean;
}

export interface ChatItemProps extends ChatUserBaseProps {
    id: string;
    messages?: ChatMessages[];
    detalle?: ChatDetalle | null;
}

export interface ChatBaseProps {
    listChat?: ChatItemProps[];
    onClickChat?: (id: string) => void;
    sendMessage?: onSubmintChatMesseges;
}

export interface ChatProps extends ChatClassProps, ChatBaseProps {}

export const ChatBase = ({
    classNameContent = '',
    styleContent = {},
    classNameContentListChat = '',
    classNameContentItemChat = '',
    ChatUserProps = {},
    classNameContentChat = '',
    classNameContentChatNoItems = '',
    classNameContentMessage = '',
    NotItemProps = {},
    MessegesUserProps = {},
    classNameContentFormChat = '',
    FormChatMessegesProps = {},
    classNameContentInfoChat = '',
    classNameContentInfoChatNoItems = '',
    NotItemDetailsProps = {},

    listChat = [],
    onClickChat = () => 1,

    ...props
}: ChatProps) => {
    const { user } = useUser();
    const [chatActive, setChatActive] = useState(0);

    const ListChatComponent = useMemo(
        () =>
            listChat.map((chat, i) => (
                <div
                    key={chat.id}
                    onClick={() => {
                        onClickChat(chat.id);
                        setChatActive(i);
                    }}
                    className={classNameContentItemChat}
                >
                    <ChatUser
                        {...ChatUserProps}
                        {...chat}
                        active={chatActive == i}
                    />
                </div>
            )),
        [listChat, chatActive]
    );

    return (
        <>
            <div className={classNameContent} style={styleContent}>
                <div className={classNameContentListChat}>
                    {ListChatComponent}
                </div>
                <div className={classNameContentChat}>
                    <div className={classNameContentMessage}>
                        {listChat[chatActive]?.messages?.length == 0 ? (
                            <div className={classNameContentChatNoItems}>
                                <NotItemMessage {...NotItemProps} />
                            </div>
                        ) : (
                            <>
                                {listChat[chatActive]?.messages?.map(
                                    (message, i) => (
                                        <MessegesUser
                                            {...MessegesUserProps}
                                            key={i}
                                            date={message.dateSend}
                                            user={
                                                message.my
                                                    ? user?.name
                                                    : listChat[chatActive]?.user
                                            }
                                            img={
                                                message.my
                                                    ? user?.img
                                                    : listChat[chatActive]?.img
                                            }
                                            msj={message.content}
                                            my={message.my}
                                        />
                                    )
                                )}
                            </>
                        )}
                    </div>
                    <div className={classNameContentFormChat}>
                        <FormChatMesseges
                            {...FormChatMessegesProps}
                            onSubmit={props?.sendMessage}
                        />
                    </div>
                </div>
                <div className={classNameContentInfoChat}>
                    {!listChat[chatActive]?.detalle ? (
                        <div className={classNameContentInfoChatNoItems}>
                            <NotItemDetails {...NotItemDetailsProps} />
                        </div>
                    ) : (
                        <></>
                    )}
                </div>
            </div>
        </>
    );
};
export default ChatBase;
