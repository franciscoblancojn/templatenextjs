import { CountersClassProps } from '@/components/Counters/Base';

export const mooveri: CountersClassProps = {
    classNameContent: `
        flex
        flex-nowrap
        flex-align-center
        flex-justify-between
        p-h-15
        p-v-10
        bg-gradient-greenishCyan2-metallicBlue2
        border-radius-15
        color-white
    `,
    classNameContentCounter: `
        flex
        flex-column
        flex-nowrap
        flex-align-center
        color-currentColor
        p-h-15
    `,
    classNameContentTitle: `
        font-16
        font-w-700
        color-currentColor
        font-nunito
    `,
    classNameContentCount: `
        font-25
        font-w-900
        color-currentColor
        font-nunito
    `,
    classNameLink: `
        color-white
        color-metallicBlueDark-hover
    `,
};

export const mooveriBackoffice: CountersClassProps = mooveri;
