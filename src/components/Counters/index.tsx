import { useMemo } from 'react';

import * as styles from '@/components/Counters/styles';

import { Theme, ThemesType } from '@/config/theme';

import { CountersBaseProps, CountersBase } from '@/components/Counters/Base';

export const CountersStyle = { ...styles } as const;

export type CountersStyles = keyof typeof CountersStyle;

export interface CountersProps extends CountersBaseProps {
    styleTemplate?: CountersStyles | ThemesType;
}

export const Counters = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: CountersProps) => {
    const Style = useMemo(
        () =>
            CountersStyle[styleTemplate as CountersStyles] ??
            CountersStyle._default,
        [styleTemplate]
    );

    return <CountersBase {...Style} {...props} />;
};
export default Counters;
