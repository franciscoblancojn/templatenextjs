# ListSuscripcionBotton

## Dependencies

[ListSuscripcionBotton](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ListSuscripcionBotton)

```js
import { ListSuscripcionBotton } from '@/components/ListSuscripcionBotton';
```

## Import

```js
import {
    ListSuscripcionBotton,
    ListSuscripcionBottonStyles,
} from '@/components/ListSuscripcionBotton';
```

## Props

```tsx
interface ListSuscripcionBottonProps {}
```

## Use

```js
<ListSuscripcionBotton>ListSuscripcionBotton</ListSuscripcionBotton>
```
