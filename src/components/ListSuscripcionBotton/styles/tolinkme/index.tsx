import { ListSuscripcionBottonClassProps } from '@/components/ListSuscripcionBotton/Base';

export const tolinkme: ListSuscripcionBottonClassProps = {
    classNameContentButtonSubscription: `
        flex
        flex-nowrap
        m-v-20
    `,
    classNameContentButton: `
        width-p-65
    `,
    classNameContentStatistics: `
        width-p-35
        text-right
        cursor-pointer
    `,
    classNameContentEmail: `
        width-p-50
    `,
    classNameContentCancel: `
        width-p-50
        flex
        
    `,
};
