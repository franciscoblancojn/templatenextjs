import { IconTextClassProps } from '@/components/IconText/Base';

export const mooveri: IconTextClassProps = {
    classNameContent: `
        flex
        flex-nowrap
        flex-align-center
    `,
    classNameContentIcon: `
        m-r-10
        flex 
        flex-align-center
    `,
    classNameContentText: `
    
    `,
    classNameText: `
    
    `,
    styleTemplateText: 'mooveri2',
};
