import { Facebook } from "@/svg/Facebook";
import { Story, Meta } from "@storybook/react";

import { IconTextProps, IconText } from "./index";

export default {
    title: "IconText/IconText",
    component: IconText,
} as Meta;

const IconTextIndex: Story<IconTextProps> = (args) => (
    <IconText {...args}>Test Children</IconText>
);

export const Index = IconTextIndex.bind({});
Index.args = {
    icon: <Facebook/>,
    text:"sadd"
};
