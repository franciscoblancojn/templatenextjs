import { useMemo } from 'react';

import * as styles from '@/components/IconText/styles';

import { Theme, ThemesType } from '@/config/theme';

import { IconTextBaseProps, IconTextBase } from '@/components/IconText/Base';

export const IconTextStyle = { ...styles } as const;

export type IconTextStyles = keyof typeof IconTextStyle;

export interface IconTextProps extends IconTextBaseProps {
    styleTemplate?: IconTextStyles | ThemesType;
}

export const IconText = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: IconTextProps) => {
    const Style = useMemo(
        () =>
            IconTextStyle[styleTemplate as IconTextStyles] ??
            IconTextStyle._default,
        [styleTemplate]
    );

    return <IconTextBase {...Style} {...props} />;
};
export default IconText;
