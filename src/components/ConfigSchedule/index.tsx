import { useMemo } from 'react';

import * as styles from '@/components/ConfigSchedule/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigScheduleBaseProps,
    ConfigScheduleBase,
} from '@/components/ConfigSchedule/Base';

export const ConfigScheduleStyle = { ...styles } as const;

export type ConfigScheduleStyles = keyof typeof ConfigScheduleStyle;

export interface ConfigScheduleProps extends ConfigScheduleBaseProps {
    styleTemplate?: ConfigScheduleStyles | ThemesType;
}

export const ConfigSchedule = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigScheduleProps) => {
    const Style = useMemo(
        () =>
            ConfigScheduleStyle[styleTemplate as ConfigScheduleStyles] ??
            ConfigScheduleStyle._default,
        [styleTemplate]
    );

    return <ConfigScheduleBase {...Style} {...props} />;
};
export default ConfigSchedule;
