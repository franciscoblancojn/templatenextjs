import {
    CompanySchedulesHours,
    CompanySchedulesHoursDate,
} from '@/interfaces/Company';
import { useData } from 'fenextjs-hook/cjs/useData';
import { InputDate } from '../Input/Date';

import { parseTextToDate, parseDateToText } from '@/functions/parseDate';
import { InputTextStyles } from '../Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import { Trash } from '@/svg/trash';

export interface ConfigScheduleClassProps {
    classNameContentStartEnd?: string;
    classNameContentDelete?: string;

    styleTemplateInputDate?: InputTextStyles | ThemesType;
}

export interface ConfigScheduleBaseProps {
    defaultValue?: CompanySchedulesHours;
    onChange?: (data: CompanySchedulesHours) => void;
    onDelete?: () => void;
}

export interface ConfigScheduleProps
    extends ConfigScheduleClassProps,
        ConfigScheduleBaseProps {}

export const ConfigScheduleBase = ({
    classNameContentStartEnd = '',
    classNameContentDelete = '',

    styleTemplateInputDate = Theme?.styleTemplate ?? '_default',

    defaultValue = {},
    onChange,
    ...props
}: ConfigScheduleProps) => {
    const { onChangeData, dataMemo } = useData<
        CompanySchedulesHours,
        CompanySchedulesHoursDate
    >(defaultValue, {
        onChangeDataAfter: onChange,
        onMemo: (data: CompanySchedulesHours) => {
            const { start, end } = data;

            const DateStart = parseTextToDate({
                text: start ?? '0:0',
                type: 'time',
            });
            const DateEnd = parseTextToDate({
                text: end ?? '0:0',
                type: 'time',
            });

            const r: CompanySchedulesHoursDate = {
                ...data,
                start: DateStart,
                end: DateEnd,
            };
            return r;
        },
    });

    const onDelete = () => {
        props?.onDelete?.();
        onChangeData('delete')(true);
    };

    if (dataMemo?.delete) {
        return null;
    }

    return (
        <>
            <div
                className={`classNameContentStartEnd ${classNameContentStartEnd}`}
            >
                <InputDate
                    type="time"
                    label={'Start'}
                    defaultValue={dataMemo?.start}
                    onChange={(date) => {
                        const time = parseDateToText({
                            date,
                            type: 'time',
                        });
                        onChangeData('start')(time);
                    }}
                    styleTemplate={styleTemplateInputDate}
                    // max={dataMemo?.end}
                />
                <InputDate
                    type="time"
                    label={'End'}
                    defaultValue={dataMemo?.end}
                    min={dataMemo?.start}
                    onChange={(date) => {
                        const time = parseDateToText({
                            date,
                            type: 'time',
                        });
                        onChangeData('end')(time);
                    }}
                    styleTemplate={styleTemplateInputDate}
                />
                <div
                    className={`classNameContentDelete ${classNameContentDelete}`}
                    onClick={onDelete}
                >
                    <Trash size={12} />
                </div>
            </div>
        </>
    );
};
export default ConfigScheduleBase;
