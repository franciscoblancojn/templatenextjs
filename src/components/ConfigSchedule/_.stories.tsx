import { Story, Meta } from "@storybook/react";

import { ConfigScheduleProps, ConfigSchedule } from "./index";

export default {
    title: "ConfigSchedule/ConfigSchedule",
    component: ConfigSchedule,
} as Meta;

const ConfigScheduleIndex: Story<ConfigScheduleProps> = (args) => (
    <ConfigSchedule {...args}>Test Children</ConfigSchedule>
);

export const Index = ConfigScheduleIndex.bind({});
Index.args = {};
