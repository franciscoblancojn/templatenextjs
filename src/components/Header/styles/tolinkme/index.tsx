import { HeaderClassProps } from '@/components/Header/Base';
import Link from '@/components/Link';
import Menu from '@/components/Menu';
import ContentWidth from '@/components/ContentWidth';
import Img from '@/components/Img';
import { LogOut } from '@/components/User/LogOut';
import { DATA } from '@/data/components/Header';
import url from '@/data/routes';
import { SelectLang } from '@/components/SelectLang';
import Button from '@/components/Button';
import MenuHome from '@/components/MenuHome';

export const tolinkme: HeaderClassProps = {
    classNameHeader: `
        header 
        pos-f top-0 left-0 
        width-p-100 
        flex flex-align-center
        p-v-5 
        z-index-lg-10
        z-index-8
        bg-gradient-darkAqua-brightPink
        box-shadow box-shadow-black box-shadow-b-1
    `,
    classNameContent: `
        container
        p-h-15
        flex flex-align-center
        flex-nowrap
    `,
    content: (
        <>
            <div className="flex-6 flex-sm-9  flex-gap-column-15 flex flex-justify-left flex flex-nowrap flex-align-center">
                <div>
                    <Menu items={DATA.nav2} styleTemplate={'tolinkme'} />
                </div>
                <Link href={url.index} className="d-black">
                    <ContentWidth
                        // size={180}
                        className={`
                            height-45
                            p-v-5
                            flex
                            flex-align-center
                        `}
                    >
                        <Img
                            src="logo.png"
                            className="height-p-100"
                            classNameImg="height-p-100 object-fit-contain"
                        />
                    </ContentWidth>
                </Link>
            </div>
            <div className="flex-6 flex-sm-3  flex flex-justify-right flex-align-center ">
                <div>
                    <SelectLang />
                </div>
                <div className="d-none d-sm-flex">
                    <LogOut />
                </div>
            </div>
        </>
    ),
};

export const tolinkme2: HeaderClassProps = {
    ...tolinkme,
    content: (
        <>
            <div className="flex-3 flex-sm-6">
                <Link href={url.index}>
                    <ContentWidth
                        size={180}
                        className={`
                    height-50
                `}
                    >
                        <Img src="logo.png" />
                    </ContentWidth>
                </Link>
            </div>
            <div className="flex-9 flex-sm-9 flex flex-justify-right flex flex-nowrap flex-align-center">
                <div>
                    <SelectLang />
                </div>
            </div>
        </>
    ),
};

export const tolinkm3: HeaderClassProps = {
    classNameHeader: `
        header 
        header-home
        width-p-100 
        flex flex-align-center
        flex-nowrap
        z-index-lg-10
        z-index-8
        p-h-20
       
    `,
    classNameContent: `
    container
        width-p-100
        p-h-15
        flex flex-align-center
        flex-nowrap
        bg-Bg-white-trans
        border-radius-30
        p-v-4
    `,
    content: (
        <>
            <div className="width-p-100 flex flex-nowrap ">
                <div className="col-home-menu-1 flex-3 flex-sm-3 col-home-menu-1  flex-gap-column-15 flex flex-justify-left flex flex-nowrap flex-align-center">
                    <div>
                        <Menu items={DATA.nav2} styleTemplate={'tolinkme4'} />
                    </div>
                    <Link href={url.index} className="d-black">
                        <ContentWidth
                            // size={180}
                            className={`
                            height-45
                            p-v-5
                            flex
                            flex-align-center
                        `}
                        >
                            <Img
                                src="logo.png"
                                className="height-p-100"
                                classNameImg="height-p-100 object-fit-contain"
                            />
                        </ContentWidth>
                    </Link>
                </div>
                <div className="flex flex-align-center flex-4 flex-align-center flex-justify-around gap-15 menu-home">
                    <MenuHome />
                </div>

                <div className="flex-3 flex-sm-9 col-idioma flex-nowrap flex flex-justify-right flex-align-center ">
                    <div>
                        <SelectLang />
                    </div>
                    <div className="">
                        <LogOut />
                    </div>
                </div>
            </div>
        </>
    ),
};

export const tolinkm4: HeaderClassProps = {
    classNameHeader: `
        header 
        header-home
        width-p-100 
        flex flex-align-center
        z-index-lg-10
        z-index-8
       p-h-20
       flex-nowrap
       
    `,
    classNameContent: `
    container
        width-p-100
        p-h-15
        flex flex-align-center
        flex-nowrap
        bg-Bg-white-trans
        border-radius-30
        p-v-4
    `,

    content: (
        <>
            <div className="width-p-100 flex flex-nowrap ">
                <div className="col-home-menu-1 flex-3 flex-sm-3  flex-gap-column-10 flex flex-justify-left flex flex-nowrap flex-align-center">
                    <Link href={url.index} className="d-black">
                        <ContentWidth
                            // size={180}
                            className={`
                            height-45
                            p-v-5
                            flex
                            flex-align-center
                        `}
                        >
                            <Img
                                src="logo.png"
                                className="height-p-100"
                                classNameImg="height-p-100 object-fit-contain"
                            />
                        </ContentWidth>
                    </Link>
                </div>
                <div className="flex flex-4 flex-align-center flex-justify-around  menu-home">
                    <MenuHome />
                </div>

                <div className="flex-3 flex-sm-9 flex-nowrap  flex flex-justify-right flex-align-center col-idioma">
                    <SelectLang />
                    <div className="flex">
                        <Link
                            className="flex"
                            styleTemplate="tolinkme8"
                            href={url.login}
                        >
                            <Button styleTemplate="tolinkme_login">
                                Login
                            </Button>
                        </Link>
                        <Link styleTemplate="tolinkme8" href={url.register}>
                            <Button
                                classNameBtn="m-l-15"
                                styleTemplate="tolinkme_register"
                            >
                                Sing up
                            </Button>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    ),
};
