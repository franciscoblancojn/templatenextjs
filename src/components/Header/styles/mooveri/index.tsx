import { HeaderClassProps } from '@/components/Header/Base';
import Link from '@/components/Link';
import Menu from '@/components/Menu';
import ContentWidth from '@/components/ContentWidth';

import url from '@/data/routes';
import { ImgProfile } from '@/components/User/ImgProfile';
import { LinkBecomeMover, LinkMyMoves } from '@/components/Links';
import Box from '@/components/Box';
import Img from '@/components/Img';
import Image from '@/components/Image';

export const mooveri: HeaderClassProps = {
    classNameHeader: `
        header 
        pos-f top-0 left-0 
        width-p-100 
        flex flex-align-center
        p-v-10 
        z-index-9
        bg-white
        border-style-solid
        border-black-16
        border-0
        border-b-1
    `,
    classNameContent: `
        container
        p-h-15
        flex flex-align-center
        flex-nowrap
    `,
    content: (
        <>
            <div className="flex-4">
                <LinkBecomeMover styleTemplate="mooveri3_2" />
            </div>
            <div className="flex-4 flex flex-justify-center">
                <Link href={url.index} className="flex flex-justify-center">
                    <ContentWidth
                        size={120}
                        className={`
                            height-45
                            flex
                        `}
                    >
                        <Img
                            src="logo.png"
                            styleTemplate="mooveri"
                            classNameImg="height-45 object-fit-contain"
                        />
                    </ContentWidth>
                </Link>
            </div>
            <div className="flex-4 flex flex-justify-right flex-align-center">
                <div className="d-none d-md-block">
                    <LinkMyMoves styleTemplate="mooveri3" />
                </div>
                <div className="p-l-15"></div>
                <Box styleTemplate="mooveriUser" className="pos-r">
                    <div className="flex flex-nowrap flex-align-center flex-gap-column-10 flex-justify-between">
                        <Menu
                            styleTemplate="mooveri"
                            items={[
                                {
                                    link: url.bookNow,
                                    text: 'Book Now',
                                },
                                {
                                    link: url.chat,
                                    text: 'Messages',
                                },
                                {
                                    link: url.movings,
                                    text: 'Moving',
                                },
                                {
                                    link: url.myAccount.index,
                                    text: 'My Account',
                                    separator: true,
                                },
                                {
                                    link: url.help,
                                    text: 'Help',
                                },
                                {
                                    link: url.logout,
                                    text: 'Log Out',
                                },
                            ]}
                        />
                        <ImgProfile styleTemplate="mooveri" />
                    </div>
                </Box>
            </div>
        </>
    ),
};

export const mooveriBackoffice: HeaderClassProps = {
    classNameHeader: `
        header 
        pos-f top-0 left-0 
        width-p-100 
        flex flex-align-center
        p-v-10 
        z-index-9
        bg-dark-jungle-green
        border-style-solid
        border-black-16
        border-0
        border-b-1
    `,
    styleHeader: {
        height: '106px',
    },
    classNameContent: `
        width-p-100
        container-
        p-h-15
        flex flex-align-center
        flex-nowrap
    `,
    content: (
        <>
            <div className="flex-6">
                <Link href={url.index} className="flex">
                    <ContentWidth
                        size={120}
                        className={`
                            height-45
                            flex
                        `}
                    >
                        <Image
                            src="logo-white.svg"
                            styleTemplate="mooveri"
                            className="height-45 object-fit-contain"
                        />
                    </ContentWidth>
                </Link>
            </div>
            <div className="flex-6 flex flex-justify-right flex-align-center">
                <div className="p-l-15"></div>
                <Box styleTemplate="mooveriUser" className="pos-r">
                    <div className="flex flex-nowrap flex-align-center flex-gap-column-10 flex-justify-between">
                        <Menu
                            styleTemplate="mooveriBackoffice"
                            items={[
                                {
                                    link: url.index,
                                    text: 'Home',
                                },
                                {
                                    link: url.transactions.index,
                                    text: 'Transactions',
                                },
                                {
                                    link: url.companies.index,
                                    text: 'Companies',
                                },
                                {
                                    link: url.users.index,
                                    text: 'Users',
                                    separator: true,
                                },
                                {
                                    link: url.logout,
                                    text: 'Log Out',
                                },
                            ]}
                        />
                        <ImgProfile styleTemplate="mooveri" />
                    </div>
                </Box>
            </div>
        </>
    ),
};
