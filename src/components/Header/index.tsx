import { useMemo } from 'react';

import * as styles from '@/components/Header/styles';

import { Theme, ThemesType } from '@/config/theme';

import { HeaderBaseProps, HeaderBase } from '@/components/Header/Base';

export const HeaderStyle = { ...styles } as const;

export type HeaderStyles = keyof typeof HeaderStyle;

export interface HeaderProps extends HeaderBaseProps {
    styleTemplate?: HeaderStyles | ThemesType;
}

export const Header = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: HeaderProps) => {
    const Style = useMemo(
        () =>
            HeaderStyle[styleTemplate as HeaderStyles] ?? HeaderStyle._default,
        [styleTemplate]
    );

    return <HeaderBase {...Style} {...props} />;
};
export default Header;
