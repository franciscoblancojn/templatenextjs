# Header

## Dependencies

[MenuStyles](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Menu)

```js
import { MenuStyles } from '@/components/Menu';
```

## Import

```js
import { Header, HeaderStyles } from '@/components/Header';
```

## Props

```tsx
interface HeaderProps {
    styleTemplate?: HeaderStyles;
    classNameHeader?: string;
    classNameContent?: string;
    distribution?: [string, string, string];
    classNameContentLogo?: string;
    sizeContentLogo?: number;
    menuStyle?: MenuStyles;
}
```

## Use

```js
<Header />
```
