import { LogOutClassProps } from '@/components/User/LogOut/Base';

export const _default: LogOutClassProps = {
    classNameContent: `
        font-18 font-nunito
        color-white color-red-hover
        pointer
        flex
        flex-align-center
        flex-gap-column-15
    `,
    classNameContentExit: `
        flex
        flex-align-center
        flex-gap-column-15
    `,
    ImgProfileProps: {
        styleTemplate: '_default',
    },
};
