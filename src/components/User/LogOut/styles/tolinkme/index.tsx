import { LogOutClassProps } from '@/components/User/LogOut/Base';

export const tolinkme: LogOutClassProps = {
    classNameContent: `
        font-18 font-nunito
        color-white color-red-hover
        pointer
        flex
        flex-align-center
        flex-gap-column-15
    `,
    classNameContentExit: `
        flex
        flex-align-center
        flex-gap-column-5
    `,
    ImgProfileProps: {
        styleTemplate: 'tolinkme',
    },
};
