import { ImgProfileClassProps } from '@/components/User/ImgProfile/Base';

export const tolinkme: ImgProfileClassProps = {
    classNameImg: `
        width-30
        height-30
        border-radius-100
    `,
    styleTemplateImg: 'tolinkme',
    LinkProps: {
        className: `
            flex
            flex-align-center
        `,
    },
};
