import { ImgProfileClassProps } from '@/components/User/ImgProfile/Base';
import { UserAccount2 } from '@/svg/userAccount';

export const mooveri: ImgProfileClassProps = {
    classNameImg: `
        width-28
        height-28
        border-radius-100
    `,
    styleTemplateImg: 'mooveri',
    iconUserNotImg: (
        <span
            className={`
                color-greenish-cyan
                color-metallicBlueDark-hover    
            `}
        >
            <UserAccount2 size={23} />
        </span>
    ),
};
