import { useMemo } from 'react';

import * as styles from '@/components/User/ImgProfile/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ImgProfileBase,
    ImgProfileBaseProps,
} from '@/components/User/ImgProfile/Base';

export const ImgProfileStyle = { ...styles } as const;

export type ImgProfileStyles = keyof typeof ImgProfileStyle;

export interface ImgProfileProps extends ImgProfileBaseProps {
    styleTemplate?: ImgProfileStyles | ThemesType;
}

export const ImgProfile = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ImgProfileProps) => {
    const Style = useMemo(
        () =>
            ImgProfileStyle[styleTemplate as ImgProfileStyles] ??
            ImgProfileStyle._default,
        [styleTemplate]
    );

    return <ImgProfileBase {...props} {...Style} />;
};
