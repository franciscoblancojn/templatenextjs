import { Story, Meta } from "@storybook/react";

import { ImgProfile,ImgProfileProps } from "./index";

export default {
    title: "User/ImgProfile",
    component: ImgProfile,
} as Meta;

const Template: Story<ImgProfileProps> = (args) => (
    <ImgProfile {...args}>Test Children</ImgProfile>
);

export const Index = Template.bind({});
Index.args = {};
