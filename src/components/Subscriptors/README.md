# Subscriptors

## Dependencies

[ContentWidth](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ContentWidth)

```js
import { ContentWidth } from '@/components/ContentWidth';
```

## Import

```js
import { Subscriptors, SubscriptorsStyles } from '@/components/Subscriptors';
```

## Props

```tsx
interface SubscriptorsProps {}
```

## Use

```js
<Subscriptors>Subscriptors</Subscriptors>
```
