import { SubscriptorsClassProps } from '@/components/Subscriptors/Base';

export const tolinkme: SubscriptorsClassProps = {
    classNamePrice: `
    font-22 
    font-nunito 
    font-w-900
    color-greyishBrown
    `,
    classNameText: `
    `,
    classNamecontent: `
    p-h-20
    p-v-13
    width-p-50
    `,
    classNamecontent2: `
    p-l-10
    `,
    classNamecontentPrice: `
    flex
    flex-justify-left
    `,
    classNameContentSubscriptors: `
    flex
    bg-white
    box-shadow 
    box-shadow-x-0 
    box-shadow-y-3 
    box-shadow-b-6 
    box-shadow-s-0 
    box-shadow-black16 
    border-radius-20
    border-whiteSeven
    border-style-solid
    border-1
    `,
    styleTemplateText: 'tolinkme37',
    styleTemplateText2: 'tolinkme36',
    styleTemplateText3: 'tolinkme38',
};
