import { useMemo } from 'react';

import * as styles from '@/components/Subscriptors/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    SubscriptorsBaseProps,
    SubscriptorsBase,
} from '@/components/Subscriptors/Base';

export const SubscriptorsStyle = { ...styles } as const;

export type SubscriptorsStyles = keyof typeof SubscriptorsStyle;

export interface SubscriptorsProps extends SubscriptorsBaseProps {
    styleTemplate?: SubscriptorsStyles | ThemesType;
}

export const Subscriptors = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: SubscriptorsProps) => {
    const Style = useMemo(
        () =>
            SubscriptorsStyle[styleTemplate as SubscriptorsStyles] ??
            SubscriptorsStyle._default,
        [styleTemplate]
    );

    return <SubscriptorsBase {...Style} {...props} />;
};
export default Subscriptors;
