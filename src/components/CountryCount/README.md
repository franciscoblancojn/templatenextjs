# CountryCount

## Import

```js
import { CountryCount, CountryCountStyles } from '@/components/CountryCount';
```

## Props

```ts
interface CountryCountProps {
    styleTemplate?: CountryCountStyles;
    country: string;
    count: number;
}
```

## Use

```js
<CountryCount country={'Colombia'} count={50} />
```
