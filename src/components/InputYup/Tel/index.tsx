import { InputTel as InputTel_, InputTelProps } from '@/components/Input/Tel';

import { ValidateTel } from '@/validations/tel';

export const InputTel = (props: InputTelProps) => (
    <InputTel_
        {...props}
        yup={ValidateTel({
            require: true,
        })}
    />
);

export default InputTel;
