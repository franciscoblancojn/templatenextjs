import { InputText, InputTextProps } from '@/components/Input/Text';

import { ValidateText } from '@/validations/text';
import { ErrorName } from '@/data/error/name';

export const InputName = (props: InputTextProps) => (
    <InputText
        {...props}
        yup={ValidateText({
            require: true,
            errors: ErrorName,
        })}
    />
);

export default InputName;
