import { useMemo } from 'react';

import * as styles from '@/components/ContentCol/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ContentColBaseProps,
    ContentColBase,
} from '@/components/ContentCol/Base';

export const ContentColStyle = { ...styles } as const;

export type ContentColStyles = keyof typeof ContentColStyle;

export interface ContentColProps extends ContentColBaseProps {
    styleTemplate?: ContentColStyles | ThemesType;
}

export const ContentCol = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ContentColProps) => {
    const Style = useMemo(
        () =>
            ContentColStyle[styleTemplate as ContentColStyles] ??
            ContentColStyle._default,
        [styleTemplate]
    );

    return <ContentColBase {...Style} {...props} />;
};
export default ContentCol;
