# ContentCol

## Import

```js
import { ContentCol, ContentColStyles } from '@/components/ContentCol';
```

## Props

```tsx
interface ContentColProps {
    styleTemplate?: ContentColStyles;
    size?: number;
    className?: string;
    columnGap?: number;
    rowGap?: number;
}
```

## Use

```js
<ContentCol size={200} columnGap={20} rowGap={10}>
    <div className="bg-red">col1</div>
    <div className="bg-blue">col2</div>
    <div className="bg-green">col3</div>
    <div className="bg-yellow">col4</div>
</ContentCol>
```
