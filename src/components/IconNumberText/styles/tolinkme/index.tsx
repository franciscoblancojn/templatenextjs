import { IconNumberTextClassProps } from '@/components/IconNumberText/Base';

export const tolinkme: IconNumberTextClassProps = {
    classNameContent: `
        flex m-b-15 flex-nowrap 
    `,
    classNameContentNumberText: `
        m-l-14
    `,
    classNameContentNumber: `
        text-capitalize
        text-white-space-nowrap
        flex flex-4
        flex-justify-left
    `,
    styleTemplateNumber: 'tolinkme17',
    classNameContentText: `

    `,
    styleTemplateText: 'tolinkme18',
    classNameContentIcon: `

    `,
    textSize: 150,
};

export const tolinkme2: IconNumberTextClassProps = {
    ...tolinkme,
    classNameContentNumberText: `
       m-l-10  m-md-l-22
    `,
    classNameContentNumber: `
        text-capitalize
        text-white-space-nowrap
        flex flex-4
        flex-justify-right-
    `,
    styleTemplateNumber: 'tolinkme14',
    classNameContentText: `
        text-right
    `,
};
