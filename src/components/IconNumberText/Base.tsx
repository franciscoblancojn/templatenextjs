import { TextStyles, Text } from '@/components/Text';
import { ContentWidth } from '@/components/ContentWidth';
import Theme, { ThemesType } from '@/config/theme';
import { numberCount } from '@/functions/numberCount';
import { useLang } from '@/lang/translate';

export interface IconNumberTextClassProps {
    classNameContent?: string;
    classNameContentIcon?: string;
    classNameContentNumberText?: string;
    classNameContentNumber?: string;
    classNameContentText?: string;

    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateNumber?: TextStyles | ThemesType;

    textSize?: number;
}

export interface IconNumberTextBaseProps {
    icon?: any;
    number: number;
    caraterNumber?: string;
    text: string;
}

export interface IconNumberTextProps
    extends IconNumberTextClassProps,
        IconNumberTextBaseProps {}

export const IconNumberTextBase = ({
    classNameContent = '',
    classNameContentIcon = '',
    classNameContentNumberText = '',
    classNameContentNumber = '',
    classNameContentText = '',

    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateNumber = Theme.styleTemplate ?? '_default',

    textSize = 100,

    icon,
    number,
    caraterNumber = '',
    text,
}: IconNumberTextProps) => {
    const _t = useLang();
    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentIcon}>{icon}</div>
                <div className={classNameContentNumberText}>
                    <Text
                        className={classNameContentNumber}
                        styleTemplate={styleTemplateNumber}
                    >
                        {numberCount(number)}
                        {caraterNumber}
                    </Text>
                    <ContentWidth size={textSize}>
                        <Text
                            className={classNameContentText}
                            styleTemplate={styleTemplateText}
                        >
                            {_t(text)}
                        </Text>
                    </ContentWidth>
                </div>
            </div>
        </>
    );
};
export default IconNumberTextBase;
