import { Story, Meta } from "@storybook/react";

import { IconNumberTextProps, IconNumberText } from "./index";

export default {
    title: "IconNumberText/IconNumberText",
    component: IconNumberText,
} as Meta;

const IconNumberTextIndex: Story<IconNumberTextProps> = (args) => (
    <IconNumberText {...args}>Test Children</IconNumberText>
);

export const Index = IconNumberTextIndex.bind({});
Index.args = {};
