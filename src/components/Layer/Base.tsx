import Padlock from '@/svg/padlock';
import Text from '../Text';
import { PropsWithChildren } from 'react';
import money from '@/functions/money';
import { useLang } from '@/lang/translate';

export interface LayerClassProps {
    classNameContent?: string;
    classNameMoney?: string;
    classNameImg?: string;
    classNameLayer?: string;
}

export interface LayerBaseProps extends PropsWithChildren {
    price?: any;
    subscribe?: string;
    period?: string;
    title?: any;
}

export interface LayerProps extends LayerClassProps, LayerBaseProps {}

export const LayerBase = ({
    classNameContent = '',
    classNameMoney = '',
    classNameImg = '',
    subscribe = 'Subscribe',
    period = 'Month',
    price = 8.5,
    classNameLayer = '',
    title = '',
}: LayerProps) => {
    const _t = useLang();

    return (
        <>
            <div className={`${classNameContent}  `}>
                <div className={classNameLayer}>
                    <div className={classNameImg}>
                        <Padlock size={15} />
                    </div>
                    <div className="flex flex-align-center">
                        <Text styleTemplate={'tolinkme25'}>
                            <span className="text-capitalize p-r-4">
                                {' '}
                                {_t(title)}
                            </span>
                            {_t(subscribe)}
                        </Text>
                        <div className={classNameMoney}>{money(price)}</div>
                        {subscribe == 'Flat_Rate' ? (
                            <Text
                                className="p-l-2"
                                styleTemplate={'tolinkme26'}
                            >
                                /<span className="p-l-1">{_t('Just one')}</span>
                            </Text>
                        ) : (
                            <Text
                                className="p-l-2"
                                styleTemplate={'tolinkme26'}
                            >
                                / <span className="p-l-1">{_t(period)}</span>
                            </Text>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default LayerBase;
