import { useMemo } from 'react';

import * as styles from '@/components/Layer/styles';

import { Theme, ThemesType } from '@/config/theme';

import { LayerBaseProps, LayerBase } from '@/components/Layer/Base';

export const LayerStyle = { ...styles } as const;

export type LayerStyles = keyof typeof LayerStyle;

export interface LayerProps extends LayerBaseProps {
    styleTemplate?: LayerStyles | ThemesType;
}

export const Layer = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: LayerProps) => {
    const Style = useMemo(
        () => LayerStyle[styleTemplate as LayerStyles] ?? LayerStyle._default,
        [styleTemplate]
    );

    return <LayerBase {...Style} {...props} />;
};
export default Layer;
