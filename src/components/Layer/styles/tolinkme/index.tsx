import { LayerClassProps } from '@/components/Layer/Base';

export const tolinkme: LayerClassProps = {
    classNameContent: `
        bg-americanSilver
        
        flex
        flex-align-center
        flex-justify-center
        border-radius-10
        width-p-100
        height-p-100
        animation-iteration-count-1 
        animation-duration-10
       
    `,
    classNameLayer: `
    width-p-100
    flex
    flex-justify-center
    flex-align-center
    `,
    classNameMoney: `
        p-l-5
        font-14 
        font-nunito 
        font-w-900
        color-black
    `,
    classNameImg: `
        color-black
        flex
    `,
};
