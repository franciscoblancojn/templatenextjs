import { CollapseClassProps } from '@/components/Collapse/Base';
import { PaginationNext } from '@/svg/pagination';

export const tolinkme: CollapseClassProps = {
    className: `
        
    `,
    classNameActive: ``,
    classNameNotActive: `overflow-hidden`,
    classNameHeader: `
        pointer
        p-v-20
        p-h-5
        cursor-pointer
        font-18
        font-nunito
        font-w-900
        color-Charcoal
        border-0
        border-b-1
        border-style-solid
        border-whiteTwo
        flex
        flex-align-center
        flex-justify-between
        flex-gap-column-10
        pos-r
        z-index-1
        bg-white
    `,
    classNameContentHeader: `
        flex
        flex-align-center
        flex-justify-between
        flex-gap-column-10
    `,
    classNameHeaderActive: `
        color-bunker
    `,
    classNameIcon: `
        transition-5
    `,
    classNameIconActive: `
        rotate-180
    `,
    classNameBody: `
        font-12 font-montserrat
        color-gray
        transition-5
        transform
    `,
    classNameBodyActive: `
        p-t-10
        m-t-10
        transform-translate-Y-0
    `,
    classNameBodyNotActive: `
        height-max-0
        transform-translate-Y-p--100
        overflow-hidden
    `,
    classNameHeaderNotActive: ``,
    classNameIconNotActive: ``,
    icon: (
        <span className="transform transform-rotate-Z-90 d-inline-block">
            <PaginationNext size={10} />
        </span>
    ),
};
