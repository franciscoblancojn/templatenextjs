import { CollapseClassProps } from '@/components/Collapse/Base';

export const _default: CollapseClassProps = {
    className: `
    border-radius-10
    box-shadow box-shadow-b-3 box-shadow-c-gray
    p-v-10 p-h-15
`,
    classNameHeader: `
    flex flex-align-center flex-justify-between
    font-montserrat font-18
    color-gray
    pointer
`,
    classNameHeaderActive: `
    color-bunker
`,
    classNameIcon: `
    transition-5
`,
    classNameIconActive: `
    rotate-180
`,
    classNameBody: `
    font-12 font-montserrat
    color-gray
    overflow-hidden
    transition-5
`,
    classNameBodyActive: `
    border-0 border-t-1 border-style-solid
    p-t-10
    m-t-10
    height-vh-max-100
`,
    classNameBodyNotActive: `
    height-max-0
`,
    classNameActive: ``,
    classNameHeaderNotActive: ``,
    classNameIconNotActive: ``,
    classNameNotActive: ``,
};
