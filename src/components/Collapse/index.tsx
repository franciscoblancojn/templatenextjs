import { CollapseBase, CollapseBaseProps } from '@/components/Collapse/Base';
import * as styles from '@/components/Collapse/styles';
import { ThemesType } from '@/config/theme';

export const CollapseStyle = { ...styles } as const;

export type CollapseStyles = keyof typeof CollapseStyle;

export interface CollapseProps extends CollapseBaseProps {
    styleTemplate?: CollapseStyles | ThemesType;
}
export const Collapse = ({
    styleTemplate = '_default',
    ...props
}: CollapseProps) => {
    return (
        <CollapseBase
            {...props}
            {...(CollapseStyle[styleTemplate as CollapseStyles] ??
                CollapseStyle._default)}
        />
    );
};
export default Collapse;
