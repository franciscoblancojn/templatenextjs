# ConfigButtonIconStyle

## Dependencies

[ConfigButtonIconStyle](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigButtonIconStyle)

```js
import { ConfigButtonIconStyle } from '@/components/ConfigButtonIconStyle';
```

## Import

```js
import {
    ConfigButtonIconStyle,
    ConfigButtonIconStyleStyles,
} from '@/components/ConfigButtonIconStyle';
```

## Props

```tsx
interface ConfigButtonIconStyleProps {}
```

## Use

```js
<ConfigButtonIconStyle>ConfigButtonIconStyle</ConfigButtonIconStyle>
```
