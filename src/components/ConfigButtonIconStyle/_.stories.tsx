import { Story, Meta } from "@storybook/react";

import { ConfigButtonIconStyleProps, ConfigButtonIconStyle } from "./index";

export default {
    title: "ConfigButtonIconStyle/ConfigButtonIconStyle",
    component: ConfigButtonIconStyle,
} as Meta;

const ConfigButtonIconStyleIndex: Story<ConfigButtonIconStyleProps> = (args) => (
    <ConfigButtonIconStyle {...args}>Test Children</ConfigButtonIconStyle>
);

export const Index = ConfigButtonIconStyleIndex.bind({});
Index.args = {};
