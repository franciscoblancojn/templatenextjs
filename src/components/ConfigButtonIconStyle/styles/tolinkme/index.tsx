import { ConfigButtonIconStyleClassProps } from '@/components/ConfigButtonIconStyle/Base';

export const tolinkme: ConfigButtonIconStyleClassProps = {
    styleTemplatePopup: 'tolinkme2',
    styleTemplateTitlePopup: 'tolinkme21',

    styleTemplateCollapse: 'tolinkme',

    styleTemplateRadioBorderButton: 'tolinkmeColumn',
    styleTemplateInputSelectBackground: 'tolinkme',
    styleTemplateInputColor: 'tolinkme2',
    styleTemplateTitleInput: 'tolinkme13',
    styleTemplateConfigBorder: 'tolinkme',

    classNameContentExample: `
        flex
        flex-align-center
        flex-gap-column-25
        color-

        font-nunito
        font-20
        font-w-900
        color-warmGrey

        pos-sk
        top-25
        bg-white
        p-v-15
        z-index-5

        border-0
        border-b-1
        border-style-solid
        border-whiteTwo
        
        box-shadow
        box-shadow-c-white
        box-shadow-y--15
    `,
};
