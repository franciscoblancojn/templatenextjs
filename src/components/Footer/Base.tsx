import { useEffect, useRef } from 'react';
import Link from '@/components/Link';
import url from '@/data/routes';
import Text from '../Text';
import { useLang } from '@/lang/translate';
import Img from '../Img';
import SelectLang from '../SelectLang';
import Instagram from '@/svg/instagram';

export interface FooterClassProps {
    classNameContent?: string;
}

export interface FooterBaseProps {}

export interface FooterProps extends FooterClassProps, FooterBaseProps {}

export const FooterBase = ({ classNameContent = '' }: FooterProps) => {
    const footer = useRef<any>(null);

    const _t = useLang();
    const loadHeightTopPaddingBody = () => {
        const heightFooter = footer.current.offsetHeight;
        document.body.style.setProperty('--sizeFooter', `${heightFooter}px`);
    };
    const loadFooter = () => {
        loadHeightTopPaddingBody();
    };
    useEffect(() => {
        if (footer) {
            loadFooter();
        }
    }, [footer]);
    useEffect(() => {
        return () => {
            document.body.style.setProperty('--sizeFooter', `0px`);
        };
    }, []);
    return (
        <>
            <footer className={`${classNameContent}`} ref={footer}>
                <div className=" bg-gradient-darkAqua-brightPink p-v-20  ">
                    <div className="container flex flex-justify-between flex-align-end p-h-20 footer-home">
                        <div className="width-p-20 footer-home-logo">
                            <Img
                                src="../../../image/tolinkme/me.png"
                                className="width-p-20 img-footer-movil"
                                classNameImg="width-p-20 img-footer-movil"
                            />
                            <div className=" flex flex-align-center  col-dis-no ">
                                <div className="width-p-60 z-index-2">
                                    <div className="m-b-10">
                                        <SelectLang styleTemplate="tolinkme2" />
                                    </div>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Home')}
                                        </Text>
                                    </Link>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Gallery')}
                                        </Text>
                                    </Link>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Blog')}
                                        </Text>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="width-p-30 footer-home-col-1  col-dis-no">
                            <div className=" flex flex-align-center ">
                                <div className="width-p-60 z-index-2">
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Terms of service')}
                                        </Text>
                                    </Link>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Privacy policy')}
                                        </Text>
                                    </Link>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Our community rules')}
                                        </Text>
                                    </Link>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Cookies')}
                                        </Text>
                                    </Link>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Report Violation')}
                                        </Text>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="col-dis-yes footer-home-col-1 col-dis-no">
                            <div className=" flex flex-align-center col-dis-no  ">
                                <div className="width-p-60 z-index-2">
                                    <div className="m-b-10">
                                        <SelectLang styleTemplate="tolinkme2" />
                                    </div>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Home')}
                                        </Text>
                                    </Link>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Gallery')}
                                        </Text>
                                    </Link>
                                    <Link
                                        styleTemplate="tolinkme8"
                                        href={url.index}
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                            {_t('Blog')}
                                        </Text>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="flex col-dis-yes">
                            <div className="width-p-50 text-start   col-dis-yes">
                                <div className=" flex flex-align-center flex-justify-center">
                                    <div className="width-p-60 z-index-2 ">
                                        <Link
                                            styleTemplate="tolinkme8"
                                            href={url.index}
                                        >
                                            <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                                {_t('Terms of service')}
                                            </Text>
                                        </Link>
                                        <Link
                                            styleTemplate="tolinkme8"
                                            href={url.index}
                                        >
                                            <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                                {_t('Privacy policy')}
                                            </Text>
                                        </Link>
                                        <Link
                                            styleTemplate="tolinkme8"
                                            href={url.index}
                                        >
                                            <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                                {_t('Our community rules')}
                                            </Text>
                                        </Link>
                                        <Link
                                            styleTemplate="tolinkme8"
                                            href={url.index}
                                        >
                                            <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                                {_t('Cookies')}
                                            </Text>
                                        </Link>
                                        <Link
                                            styleTemplate="tolinkme8"
                                            href={url.index}
                                        >
                                            <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                                {_t('Report Violation')}
                                            </Text>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="width-p-50 text-start col-dis-yes">
                                <div className=" flex flex-align-center flex-justify-center col-dis-yes  ">
                                    <div className="width-p-60 z-index-2">
                                        <div className="m-b-10 idioma">
                                            <SelectLang styleTemplate="tolinkme2" />
                                        </div>
                                        <Link
                                            styleTemplate="tolinkme8"
                                            href={url.index}
                                        >
                                            <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                                {_t('Home')}
                                            </Text>
                                        </Link>
                                        <Link
                                            styleTemplate="tolinkme8"
                                            href={url.index}
                                        >
                                            <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                                {_t('Gallery')}
                                            </Text>
                                        </Link>
                                        <Link
                                            styleTemplate="tolinkme8"
                                            href={url.index}
                                        >
                                            <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10 ">
                                                {_t('Blog')}
                                            </Text>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="width-p-50 text-right col-inf-footer">
                            <div className=" flex flex-align-center  flex-justify-right  col-inf-footer-col">
                                <div className="width-p-60 z-index-2 col-inf-footer">
                                    <Text className="color-white font-14  font-w-400 m-b-10 col-text-center ">
                                        {_t(
                                            'Use Tolinkme to hold all of your links, social accounts, contact information, and more'
                                        )}
                                    </Text>
                                    <Link
                                        href={`${'mailto:info@tolinkme.com'}`}
                                        styleTemplate="tolinkme8"
                                    >
                                        <Text className="color-white color-brightPink-hover link-movil font-14 font-w-900 m-b-10  col-text-center">
                                            {_t('info@tolinkme.com')}
                                        </Text>
                                    </Link>
                                    <Link
                                        href={`${'https://www.instagram.com/tolinkme/'}`}
                                        className="col-text-center"
                                        styleTemplate="tolinkme8"
                                    >
                                        <Text className="m-l-auto border-radius-30 p-5 m-ins-auto col-text-center bg-black width-p-8 height-p-10 flex flex-align-center text-center color-white color-brightPink-hover font-14 font-w-900 m-b-10 flex-justify-center">
                                            <Instagram size={20} />
                                        </Text>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="flex bg-black flex-justify-center p-10  ">
                    <Text className="font-8 link-movil">
                        {_t('© 2021-2023 Tolinkme – All Rigths Reserved.')}
                    </Text>
                </div>
            </footer>
        </>
    );
};
export default FooterBase;
