import { FooterClassProps } from '@/components/Footer/Base';

export const _default: FooterClassProps = {
    classNameContent: `
        p-v-20
        m-t-auto
    `,
};
