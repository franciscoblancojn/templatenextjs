# Footer

## Import

```js
import { Footer, FooterStyles } from '@/components/Footer';
```

## Props

```tsx
interface FooterProps {}
```

## Use

```js
<Footer>Footer</Footer>
```
