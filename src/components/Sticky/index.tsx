import { useEffect, useState, PropsWithChildren } from 'react';

export interface StickyProps {
    className?: string;
}

export const Sticky = ({
    children,
    className = '',
}: PropsWithChildren<StickyProps>) => {
    const [top, setTop] = useState(0);
    const loadScroll = () => {
        window.addEventListener('scroll', () => {
            setTop(window.scrollY);
        });
    };
    useEffect(() => {
        loadScroll();
    }, []);
    return (
        <>
            <div className={`pos-sk ${className}`} style={{ top: `${top}px` }}>
                {children}
            </div>
        </>
    );
};
export default Sticky;
