import Text, { TextStyles } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';
import Image from '@/components/Image';
import Button, { ButtonStyles } from '@/components/Button';
import Link, { LinkStyles } from '@/components/Link';
import ContentWidth from '../ContentWidth';
import Space from '../Space';

export interface NotItemClassProps {
    classNameContent?: string;
    classNameContentIcon?: string;
    classNameContentText?: string;
    classNameContentImg?: string;
    classNameContentButton?: string;
    classNameContentLink?: string;
    classNameLink?: string;
    sizeButton?: number;
    sizeText?: number;
    sizeImg?: number;
    classNameText?: string;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateLink?: LinkStyles | ThemesType;
    styleTemplateImg?: ThemesType;

    sizeTitle?: number;
    classNameContentTitle?: string;
    classNameTitle?: string;
    styleTemplateTitle?: TextStyles | ThemesType;
}

export interface NotItemBaseProps {
    srcImg?: string;
    title?: any;
    text?: any;
    textBtn?: string;
    linkBtn?: string;
}
export interface NotItemProps extends NotItemClassProps, NotItemBaseProps {}

export const NotItemBase = ({
    classNameContent = '',
    classNameContentText = '',
    classNameContentImg = '',
    classNameContentLink = '',
    classNameLink = '',
    classNameContentButton = '',
    classNameText = '',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateLink = Theme.styleTemplate ?? '_default',
    styleTemplateImg = Theme.styleTemplate ?? '_default',

    sizeTitle = -1,
    classNameContentTitle = '',
    classNameTitle = '',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',

    srcImg = 'Payment.png',

    title = undefined,

    text,
    linkBtn,
    textBtn = undefined,
    sizeButton = -1,
    sizeImg = -1,
    sizeText = -1,
}: NotItemProps) => {
    return (
        <>
            <div className={classNameContent}>
                {title ? (
                    <>
                        <ContentWidth
                            size={sizeTitle}
                            className={classNameContentTitle}
                        >
                            <Text
                                className={classNameTitle}
                                styleTemplate={styleTemplateTitle}
                            >
                                {title}
                            </Text>
                        </ContentWidth>
                    </>
                ) : (
                    <></>
                )}
                <ContentWidth size={sizeImg} className={classNameContentImg}>
                    <Image
                        styleTemplate={styleTemplateImg}
                        src={srcImg}
                    ></Image>
                </ContentWidth>
                <Space size={27} />
                <ContentWidth size={sizeText} className={classNameContentText}>
                    <Text
                        className={classNameText}
                        styleTemplate={styleTemplateText}
                    >
                        {text}
                    </Text>
                </ContentWidth>
                {textBtn ? (
                    <>
                        <div className={classNameContentLink}>
                            <ContentWidth
                                size={sizeButton}
                                className={classNameContentButton}
                            >
                                <Link
                                    href={linkBtn}
                                    styleTemplate={styleTemplateLink}
                                    className={classNameLink}
                                >
                                    <Button styleTemplate={styleTemplateButton}>
                                        {textBtn}
                                    </Button>
                                </Link>
                            </ContentWidth>
                        </div>
                    </>
                ) : (
                    <></>
                )}
            </div>
        </>
    );
};
export default NotItemBase;
