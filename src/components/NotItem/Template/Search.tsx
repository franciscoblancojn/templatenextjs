import { NotItem, NotItemProps } from '@/components/NotItem';

export const NotItemSearch = ({ ...props }: NotItemProps) => {
    return (
        <NotItem
            {...props}
            title="You haven't search nothing yet!"
            text="Sorry we didn't find any result."
            srcImg="emptyMovings.png"
        />
    );
};
export default NotItemSearch;
