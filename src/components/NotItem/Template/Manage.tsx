import { NotItem, NotItemProps } from '@/components/NotItem';
import url from '@/data/routes';
export const NotItemManage = ({ ...props }: NotItemProps) => {
    return (
        <NotItem
            {...props}
            text="There is no any address associated to your account"
            linkBtn={url.myAccount.addresses.add}
            textBtn="Add Addresses"
            srcImg="addresses.png"
        />
    );
};
export default NotItem;
