import { NotItem, NotItemProps } from '@/components/NotItem';
import url from '@/data/routes';
export const NotItemPayment = ({ ...props }: NotItemProps) => {
    return (
        <NotItem
            {...props}
            text="There is no any address associated to your account"
            linkBtn={url.myAccount.payments.add}
            textBtn="Add Payment"
            srcImg="Payment.png"
        />
    );
};
export default NotItem;
