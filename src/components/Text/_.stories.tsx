import { Story, Meta } from "@storybook/react";

import { TextProps, Text } from "./index";

export default {
    title: "Text/Text",
    component: Text,
} as Meta;

const Template: Story<TextProps> = (args) => (
    <Text {...args}>Test Children</Text>
);

export const Index = Template.bind({});
Index.args = {};
