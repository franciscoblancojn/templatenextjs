# Text

## Import

```js
import { Text, TextStyles } from '@/components/Text';
```

## Props

```tsx
interface TextProps {
    styleTemplate?: TextStyles;
    className?: string;
    style?: CSS.Properties;
}
```

## Use

```js
<Text>Text</Text>
```
