export const mooveriVoid = `
    font-nunito
`;

export const mooveri = `
    font-13
    font-nunito
    color-warmGreyTwo
`;

export const mooveri2 = `
    font-16 
    font-w-800
    color-greyishBrown
    font-nunito
`;
export const mooveri3 = `
    font-28 
    font-nunito 
    font-w-900
    color-black
`;
export const mooveri4 = `
    font-12 
    font-nunito 
    font-w-400
    color-warmGreyTwo
`;
export const mooveri5 = `
    m-auto
    font-15
    font-w-700 
    color-warmGreyTwo 
`;

export const mooveri6 = `
    font-13
    font-w-900 
    font-circular
    color-greyishBrownFive 
`;

export const mooveri7 = `
    font-13
    font-w-500 
    font-circular
    color-warmGreyTwo 
`;

export const mooveri8 = `
    font-22
    font-w-500 
    font-circular
    color-white 
`;

export const mooveri9 = `
    font-28
    font-w-900 
    font-nunito
    color-sea 
`;
export const mooveri9gray = `
    font-28
    font-w-900 
    font-nunito
    color-warmGreyTwo 
`;

export const mooveri10 = `
    font-14 
    font-w-800 
    font-nunito
    color-greenish-cyan 
`;

export const mooveri11 = `
    font-14 
    font-w-800 
    font-nunito
    color-warmGreyThree
`;

export const mooveri12 = `
    font-28 
    font-w-600 
    font-circular
    color-warmGreyTwo
`;
export const mooveri13 = `
    font-9 
    font-w-400 
    font-nunito
    color-warmGreyTwo
`;
export const mooveri14 = `
    font-16 
    font-w-700 
    font-nunito
    color-black
`;

export const mooveri15 = `
    font-14 
    font-w-400 
    font-nunito
    color-grayBook
`;

export const mooveri16 = `
    font-12 
    font-nunito 
    font-w-400
    color-grayBook 
`;
export const mooveri17 = `
    font-16 
    font-nunito 
    font-w-700
    color-grayBook 
`;
export const mooveri18 = `
    font-18 
    font-circular 
    font-w-300
    color-white 
`;
export const mooveri19 = `
    font-14 
    font-nunito 
    font-w-700
    color-warmGreyTwo 
`;
export const mooveri20 = `
    font-18
    font-w-700 
    font-nunito
    color-sea 
`;
