export const tolinkme = `
    text-tolinkme
    font-13 font-nunito
    color-white
`;
export const tolinkme2 = `
    text-tolinkme2
    font-8 font-nunito
    color-warmGrey
`;
export const tolinkme3 = `
    text-tolinkme3
    font-19 font-sm-18 font-nunito font-w-900
    color-greyishBrown
`;
export const tolinkme4 = `
    text-tolinkme4   
    font-12 font-sm-16 font-nunito font-w-300
    color-greyishBrown
`;
export const tolinkme5 = `
    text-tolinkme5
    font-11 font-sm-16 font-nunito font-w-400
    color-pinkishGreyTwo
`;
export const tolinkme6 = `
    text-tolinkme6
    font-11 font-sm-17 font-nunito font-w-700
    color-blackTwo
`;
export const tolinkme7 = `
    text-tolinkme7
    font-20 font-nunito font-w-900
    color-white
`;
export const tolinkme8 = `
    text-tolinkme8
    font-17 font-nunito font-w-900
    color-warmGreyFour
`;
export const tolinkme9 = `
    text-tolinkme9
    font-16 font-nunito font-w-900
    color-black
`;
export const tolinkme10 = `
    text-tolinkme10
    font-14 font-nunito font-w-900
    color-blackTwo
`;
export const tolinkme11 = `
    text-tolinkme11
    font-18 font-nunito font-w-900
    color-blackTwo
`;
export const tolinkme12 = `
    text-tolinkme12
    font-19 font-nunito font-w-400
    color-warmGreyFive
`;
export const tolinkme13 = `
    text-tolinkme13
    font-14 font-nunito font-w-900
    color-greyishBrown
`;

export const tolinkme14 = `
    text-tolinkme14
    font-25 font-md-29 
    font-nunito font-w-900
    color-black
`;

export const tolinkme15 = `
    text-tolinkme15
    font-18 font-nunito font-w-900
    color-warmGrey
`;
export const tolinkme16 = `
    text-tolinkme16
    font-13 font-md-16 font-nunito font-w-900
    color-warmGrey
`;
export const tolinkme17 = `
    text-tolinkme17
    font-20 font-md-22  font-nunito font-w-900
    color-black
`;
export const tolinkme18 = `
    text-tolinkme18
    font-14 font-w-900 font-nunito
    color-pinkishGrey 
`;
export const tolinkme19 = `
    text-tolinkme19
    font-14 font-md-16 font-nunito font-w-900
    color-warmGrey
`;
export const tolinkme20 = `
    text-tolinkme20
    font-18 font-w-900 font-nunito
    color-black 
`;

export const tolinkme21 = `
    text-tolinkme21
    font-21 font-nunito font-w-900
    color-greyishBrown
`;

export const tolinkme22 = `
    text-tolinkme21
    font-16 font-nunito font-w-900
    color-greyishBrown
`;
export const tolinkme23 = `
    text-tolinkme21
    font-14 font-nunito font-w-900
    color-greyishBrown
`;
export const tolinkme24 = `
    text-tolinkme21
    font-14 font-nunito font-w-600
    color-black
    p-l-10
    p-r-15
`;
export const tolinkme25 = `
    font-14 font-nunito font-w-900
    color-black
    p-l-10
`;
export const tolinkme26 = `
    font-10 font-nunito font-w-900
    color-outerSpace
`;
export const tolinkme27 = `
    font-22 font-nunito font-w-900
    color-black
    p-b-15
`;
export const tolinkme28 = `
    font-18 font-nunito font-w-500
    color-warmGreyFive
    p-l-25
    p-r-25
    p-t-15
    text-center
`;
export const tolinkme29 = `
    font-18 font-nunito font-w-900
    color-spanishGray
    flex
    flex-justify-left
`;
export const tolinkme30 = `
    font-12 font-nunito font-w-900
    color-spanishGray
    p-l-5
`;
export const tolinkme31 = `
    font-30 font-nunito font-w-900
    color-brightPink
    
`;
export const tolinkme32 = `
    font-14 font-nunito font-w-900
    color-black
`;
export const tolinkme33 = `
    font-14 font-nunito font-w-300
    color-philippineGray
`;
export const tolinkme34 = `
    font-nunito
    font-18
    font-w-900
    color-sonicSilver
    p-b-8
   
`;
export const tolinkme35 = `
    p-t-8
    font-nunito
    font-20
    font-w-800
    color-spanishGray2
    text-style-oblique
`;
export const tolinkme36 = `
    font-14 font-nunito font-w-700
    color-spanishGray
    
`;
export const tolinkme37 = `
    font-10 font-nunito font-w-900
    color-greyishBrown
    p-t-14
`;
export const tolinkme38 = `
font-24 font-nunito font-w-900
color-greyishBrown

`;
export const tolinkme39 = `
    font-14 font-nunito font-w-900
    color-black
`;
export const tolinkme40 = `
    font-12 font-nunito font-w-400
    color-darkCharcoal
`;
export const tolinkme41 = `
    font-14 font-nunito font-w-500
    color-black text-center
`;
export const tolinkme42 = `
    text-tolinkme
    font-9 font-nunito
    color-white
`;
export const tolinkme43 = `
    text-tolinkme
    font-12 font-nunito
    color-brownishGrey
`;
export const tolinkme44 = `
    text-tolinkme
    font-24 font-nunito
    color-blackOlive
    font-w-900
`;
export const tolinkme45 = `
    text-tolinkme
    font-15 font-nunito font-w-900
    color-blackOlive 
`;
export const tolinkme46 = `
    text-tolinkme
    font-15 font-nunito 
    font-w-700
    color-warmGreyFive 
`;
export const tolinkme47 = `
    text-tolinkme
    font-24 font-nunito 
    font-w-900
    color-greyishBrown
`;
export const tolinkme48 = `
    text-tolinkme
    font-18 font-nunito 
    font-w-900
    color-warmGreyFive 
`;
export const tolinkme49 = `
    text-tolinkme
    font-12 font-nunito 
    font-w-400
    color-warmGreyFive
`;
export const tolinkme50 = `
    text-tolinkme7
    font-16 font-nunito font-w-900
    text-decoration-underline
    color-white
`;
export const tolinkme51 = `
    text-tolinkme
    font-12 font-nunito
    color-white
`;
export const tolinkme52 = `
    text-tolinkme21
    font-12 font-nunito font-w-600
    color-black

`;
export const tolinkme53 = `
    text-tolinkme18
    font-14 font-w-900 font-nunito
    color-pinkishGrey 
    flex
    flex-justify-right
`;
export const tolinkme54 = `
    text-tolinkme10
    font-14 font-nunito font-w-900
    color-blackTwo
    flex
    flex-justify-right
`;
export const tolinkme55 = `
    text-tolinkme10
    font-22 font-nunito font-w-900
    color-white

`;

export const tolinkme56 = `
    text-tolinkme10
    font-30 font-nunito font-w-900
    color-white

`;
export const tolinkme57 = `
    font-14 font-nunito font-w-900
    color-black
    
`;
