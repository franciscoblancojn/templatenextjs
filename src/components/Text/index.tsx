import CSS from 'csstype';
import { PropsWithChildren } from 'react';

import * as styles from '@/components/Text/styles';

import { Theme, ThemesType } from '@/config/theme';

export const TextStyle = { ...styles } as const;

export type TextStyles = keyof typeof TextStyle;

export interface TextProps {
    styleTemplate?: TextStyles | ThemesType;
    className?: string;
    style?: CSS.Properties;
}

export const Text = ({
    children,
    styleTemplate = Theme?.styleTemplate ?? '_default',
    className = '',
    style = {},
}: PropsWithChildren<TextProps>) => {
    return (
        <p
            className={`text ${className} ${
                TextStyle[styleTemplate as TextStyles] ??
                TextStyle._default ??
                ''
            }`}
            style={style}
        >
            {children}
        </p>
    );
};
export default Text;
