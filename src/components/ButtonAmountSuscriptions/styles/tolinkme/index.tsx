import { ButtonAmountSuscriptionsClassProps } from '@/components/ButtonAmountSuscriptions/Base';

export const tolinkme: ButtonAmountSuscriptionsClassProps = {
    classNameContentButton: `
        cursor-pointer
        flex
        flex-justify-center
        flex-align-center
        width-p-65
        border-style-solid 
        border-3 
        border-black 
        height-45 
        border-radius-10
    `,
    classNameContentIconLogo: `
        border-radius-100 
        width-30 
        height-30 
        text-center
        m-r-5
    `,
    classNameContent: `
        bg-white
        color-black
        box-shadow 
        box-shadow-x-0 
        box-shadow-y-3 
        box-shadow-b-6 
        box-shadow-s-0 
        box-shadow-black-16 
        m-0
        p-h-12
        p-v-16
        border-radius-15
        border-1
        border-style-solid
        border-whiteFive
        width-p-100
        flex
        flex-align-center
        flex-justify-between
        column-gap-5
        flex-nowrap
    `,
    classNameContentCol1: `
        width-p-49
        flex
        flex-column
        column-gap-10
        
    `,
    classNameContentTitleBalance: `
        p-t-2
    `,
    styleTemplateNumber: 'tolinkme17',
    styleTemplateTitleBalance: 'tolinkme18',

    classNameContentCol2: `
        width-p-49
        flex
        flex-column
        column-gap-10
        p-l-20
    `,

    line: `
        width-p-1
        border-style-solid
        border-l-1
        border-0
        height-70
        border-warmGreyFive

    `,

    classNameMonth: `
        font-12
    `,
};
