import { useMemo } from 'react';

import * as styles from '@/components/ButtonAmountSuscriptions/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ButtonAmountSuscriptionsBaseProps,
    ButtonAmountSuscriptionsBase,
} from '@/components/ButtonAmountSuscriptions/Base';

export const ButtonAmountSuscriptionsStyle = { ...styles } as const;

export type ButtonAmountSuscriptionsStyles =
    keyof typeof ButtonAmountSuscriptionsStyle;

export interface ButtonAmountSuscriptions
    extends ButtonAmountSuscriptionsBaseProps {
    styleButtonAmountSuscriptions?: ButtonAmountSuscriptionsStyles | ThemesType;
}

export const ButtonAmountSuscriptions = ({
    styleButtonAmountSuscriptions = Theme?.styleTemplate ?? '_default',
    ...props
}: ButtonAmountSuscriptions) => {
    const Style = useMemo(
        () =>
            ButtonAmountSuscriptionsStyle[
                styleButtonAmountSuscriptions as ButtonAmountSuscriptionsStyles
            ] ?? ButtonAmountSuscriptionsStyle._default,
        [styleButtonAmountSuscriptions]
    );

    return <ButtonAmountSuscriptionsBase {...Style} {...props} />;
};
export default ButtonAmountSuscriptions;
