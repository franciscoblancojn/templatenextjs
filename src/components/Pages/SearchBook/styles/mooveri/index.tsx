import { PageSearchBookClassProps } from '@/components/Pages/SearchBook/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageSearchBookClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'SearchBook',
        LayoutProps: {
            styleTemplate: 'mooveri',
            style: {
                padding: 0,
            },
            className: `
                flex
            `,
        },
        validateLogin: false,
    },
};
export const mooveri2 = mooveri;
export const mooveri3 = mooveri;

export const mooveriBackoffice: PageSearchBookClassProps = {
    render: {
        Layout: Layout,
        title: 'SearchBook',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
