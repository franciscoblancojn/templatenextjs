import { PageSearchBookClassProps } from '@/components/Pages/SearchBook/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageSearchBookClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'SearchBook',
        validateLogin: true,
    },
};
