import { useMemo } from 'react';

import * as styles from '@/components/Pages/SearchBook/styles';
import * as connect from '@/components/Pages/SearchBook/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageSearchBookBaseProps,
    PageSearchBookBase,
} from '@/components/Pages/SearchBook/Base';

export const PageSearchBookStyle = { ...styles } as const;
export const PageSearchBookConnect = { ...connect } as const;

export type PageSearchBookStyles = keyof typeof PageSearchBookStyle;
export type PageSearchBookConnects = keyof typeof PageSearchBookConnect;

export interface PageSearchBookProps extends PageSearchBookBaseProps {
    styleTemplate?: PageSearchBookStyles | ThemesType;
    company?: boolean;
}

export const PageSearchBook = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageSearchBookProps) => {
    const Style = useMemo(
        () =>
            PageSearchBookStyle[styleTemplate as PageSearchBookStyles] ??
            PageSearchBookStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageSearchBookConnect[styleTemplate as PageSearchBookConnects] ??
            PageSearchBookConnect._default,
        [styleTemplate]
    );

    return <PageSearchBookBase<any> {...Style} {...Connect} {...props} />;
};
export default PageSearchBook;
