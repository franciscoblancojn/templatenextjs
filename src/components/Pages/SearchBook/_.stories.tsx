import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageSearchBookProps, PageSearchBook } from "./index";

export default {
    title: "Page/PageSearchBook",
    component: PageSearchBook,
} as Meta;

const SearchBook: Story<PropsWithChildren<PageSearchBookProps>> = (args) => (
    <PageSearchBook {...args}>Test Children</PageSearchBook>
);

export const Index = SearchBook.bind({});
Index.args = {
   
};
