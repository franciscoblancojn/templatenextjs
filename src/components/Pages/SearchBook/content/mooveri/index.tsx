import {
    onSubmintSearchBook,
    DataTypeFormSearch,
} from '@/components/Form/SearchBook/Base';
import { SelectOptionProps } from '@/components/Input/Select';
import { SearchBook, SearchBookStyles } from '@/components/SearchBook';
import Theme, { ThemesType } from '@/config/theme';
import { calcDistance } from '@/functions/calcDistance';
import log from '@/functions/log';
import { GoogleAddres } from '@/interfaces/Google/addres';
import { DataSearchBook } from '@/interfaces/SearchBook';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Cotizar } from '@/api/mooveri/cotizar';
import { MoveSize } from '@/data/components/MoveSize';
import { Inference } from '@/api/mooveri/inference';
import { Socket } from '@/api/mooveri/socket';
import { DataCotizar } from '@/interfaces/Cotizar';
import { ScanItemSocketProps, ItemScanProps } from '@/interfaces/Scan/ItemScan';
// import { useNotification } from "@/hook/useNotification";

export interface MooveriProps {
    styleTemplateSearchBook?: SearchBookStyles | ThemesType;
    ip?: string;
}

export const Mooveri = ({
    styleTemplateSearchBook = Theme.styleTemplate ?? '_default',
    ip = '1225',
}: MooveriProps) => {
    // const { pop } = useNotification();
    const [box, setBox] = useState<ItemScanProps[]>([]);
    const [pre, setPre] = useState(true);
    const [loader, setLoader] = useState(false);
    const [data, setData] = useState<DataSearchBook<DataTypeFormSearch>>({});
    const [load_retrive_stream, set_load_retrive_stream] = useState(false);

    const onCotizar: onSubmintSearchBook = async (
        data: DataSearchBook<DataTypeFormSearch>
    ) => {
        setPre(false);
        setLoader(true);

        const qqq = await calcDistance(
            [(data?.movingFrom as GoogleAddres)?.formatedAddress],
            [(data?.movingTo as GoogleAddres)?.formatedAddress]
        );
        const distance = qqq.rows[0].elements[0].distance?.value;
        const duration = qqq.rows[0].elements[0].duration?.value;

        if (distance == undefined || duration == undefined) {
            log('error', 'from or to invalid', 'red');
            throw {
                message: 'Address invalid',
            };
        }

        const dataSend: DataCotizar = {
            distance_time: duration / 3600,

            size_details:
                MoveSize[(data?.moveSize as SelectOptionProps)?.value],

            stairs: data.youStairs ? data.numberStairs : 0,
            miles: distance * 0.000621371,
            // equipment: data.items,
        };

        const cotizaciones = await Cotizar(dataSend);
        log('cotizaciones', cotizaciones);
        setData(data);

        setLoader(false);
        return {
            status: 'ok',
        };
    };

    const onEmitImg: ScanItemSocketProps = (data: ItemScanProps) => {
        // pop({
        //     type: "ok",
        //     message: "enviando img",
        //     styleTemplate: "tolinkme",
        // });
        Socket.emit('image', {
            image: data.fileData.split(';base64,')[1],
            id: ip,
        });
    };

    const onLoadSocketRead = () => {
        if (!load_retrive_stream) {
            set_load_retrive_stream(true);
            Socket.on(`retrive_stream_${ip}`, (imageData) => {
                try {
                    const data: any = JSON.parse(imageData);
                    if (data.length > 0) {
                        setBox(
                            data.map((r: any) => ({
                                text: r.label,
                                bbox: r.bbox,
                                fileData: `data:image/png;base64,${r.image}`,
                                name: r.label,
                            }))
                        );
                        // pop({
                        //     type: "warning",
                        //     message: "reciviendo img " + imageData,
                        //     styleTemplate: "tolinkme",
                        // });
                    }
                } catch (error) {
                    log('Error Socker', error, 'red');
                }
            });
        }
    };
    useEffect(() => {
        onLoadSocketRead();
    }, []);
    log('cotizaciones', { onEmitImg, box });

    return (
        <>
            <SearchBook
                styleTemplate={styleTemplateSearchBook}
                pre={pre}
                loader={loader}
                onCotizar={onCotizar}
                data={data}
                onProssesItemScan={Inference}
                onSocketSend={onEmitImg}
                socketData={box}
            />
        </>
    );
};

export interface MooveriBackofficeProps {}

export const MooveriBackoffice = ({}: MooveriBackofficeProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};
