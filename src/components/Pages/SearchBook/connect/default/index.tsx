import { PageSearchBookConnectProps } from '@/components/Pages/SearchBook/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/SearchBook/content/default';

export const _default: PageSearchBookConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
