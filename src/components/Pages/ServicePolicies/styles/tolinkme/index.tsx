import { PageServicePoliciesClassProps } from '@/components/Pages/ServicePolicies/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageServicePoliciesClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Terms Conditions',
        LayoutProps: {
            goBack: true,
        },
    },
    ContentWidthProps: {
        size: 500,
        className: `
            text-center
            m-h-auto
        `,
    },
    TextProps: {
        styleTemplate: 'tolinkme',
    },
};
