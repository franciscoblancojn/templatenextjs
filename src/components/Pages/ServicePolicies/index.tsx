import { useMemo } from 'react';

import * as styles from '@/components/Pages/ServicePolicies/styles';
import * as connect from '@/components/Pages/ServicePolicies/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageServicePoliciesBaseProps,
    PageServicePoliciesBase,
} from '@/components/Pages/ServicePolicies/Base';

export const PageServicePoliciesStyle = { ...styles } as const;
export const PageServicePoliciesConnect = { ...connect } as const;

export type PageServicePoliciesStyles = keyof typeof PageServicePoliciesStyle;
export type PageServicePoliciesConnects =
    keyof typeof PageServicePoliciesConnect;

export interface PageServicePoliciesProps extends PageServicePoliciesBaseProps {
    styleTemplate?: PageServicePoliciesStyles | ThemesType;
    company?: boolean;
}

export const PageServicePolicies = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageServicePoliciesProps) => {
    const Style = useMemo(
        () =>
            PageServicePoliciesStyle[
                styleTemplate as PageServicePoliciesStyles
            ] ?? PageServicePoliciesStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageServicePoliciesConnect[
                styleTemplate as PageServicePoliciesConnects
            ] ?? PageServicePoliciesConnect._default,
        [styleTemplate]
    );

    return <PageServicePoliciesBase {...Style} {...Connect} {...props} />;
};
export default PageServicePolicies;
