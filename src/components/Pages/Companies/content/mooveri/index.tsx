import { useRouter } from 'next/router';
import { LineByMonth } from '@/components/Graf/Line/template/LineByMonth';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import { MonthsConst, MonthsType } from '@/data/components/Month';
import Bar from '@/components/Graf/Bar';
import {
    TableCompany,
    TableItemCompanyProps,
} from '@/components/Table/template/Company';
import Link from '@/components/Link';
import url from '@/data/routes';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    companiesByMonths: {
        [id in MonthsType]: number;
    };
    companiesMoovings: {
        [id: string]: number;
    };
    companies: TableItemCompanyProps[];
}

export const MooveriBackoffice = (props: MooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <div className="flex flex- m-b-20">
                <Text styleTemplate="mooveri8">{_t('Companies')}</Text>

                <Link
                    styleTemplate="mooveri3"
                    href={url.companies.create}
                    className="m-l-auto m-r-25"
                >
                    {_t('Create Company')}
                </Link>

                <Link styleTemplate="mooveri3" href={url.companies.import}>
                    {_t('Import Companies')}
                </Link>
            </div>

            <div className="m-b-50 flex flex-nowrap flex-gap-40 flex-justify-between width-p-100">
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">
                        {_t('Monthly Register Companies')}
                    </Text>
                    <LineByMonth
                        datasets={[
                            {
                                label: 'Monthly Register Companies',
                                data: MonthsConst.map((m) => {
                                    return props?.companiesByMonths?.[m] ?? 0;
                                }),
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">
                        {_t('Moving Companies')}
                    </Text>
                    <Bar
                        labels={Object.keys(props.companiesMoovings)}
                        datasets={[
                            {
                                label: 'Companies Moovings',
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                                data: Object.keys(props.companiesMoovings).map(
                                    (code) =>
                                        props.companiesMoovings?.[code] ?? 0
                                ),
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
            </div>
            <div className="m-b-50">
                <TableCompany
                    items={props.companies}
                    pagination={{
                        nItems: 200,
                    }}
                    loader={false}
                    styleTemplate="mooveri"
                />
            </div>
        </>
    );
};
