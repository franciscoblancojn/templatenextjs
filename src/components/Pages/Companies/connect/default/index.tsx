import { PageCompaniesConnectProps } from '@/components/Pages/Companies/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/Companies/content/default';

export const _default: PageCompaniesConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
