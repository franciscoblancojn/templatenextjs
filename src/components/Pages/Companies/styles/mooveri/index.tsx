import { PageCompaniesClassProps } from '@/components/Pages/Companies/Base';
import { Layout } from '@/layout/Backoffice';

export const mooveri: PageCompaniesClassProps = {};

export const mooveriBackoffice: PageCompaniesClassProps = {
    render: {
        Layout: Layout,
        title: 'Home',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
