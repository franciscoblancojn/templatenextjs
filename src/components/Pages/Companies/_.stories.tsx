import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageCompaniesProps, PageCompanies } from "./index";

export default {
    title: "Page/PageCompanies",
    component: PageCompanies,
} as Meta;

const Companies: Story<PropsWithChildren<PageCompaniesProps>> = (args) => (
    <PageCompanies {...args}>Test Children</PageCompanies>
);

export const Index = Companies.bind({});
Index.args = {
   
};
