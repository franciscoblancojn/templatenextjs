import { LayoutBase as LayoutCompanies } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageCompaniesConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageCompaniesClassProps {
    render?: RenderLoaderProps;
}

export interface PageCompaniesBaseProps {}

export interface PageCompaniesProps<P = any>
    extends PageCompaniesBaseProps,
        PageCompaniesClassProps,
        PageCompaniesConnectProps<P> {}

export const PageCompaniesBase = <P,>({
    render = {
        Layout: LayoutCompanies,
        title: 'Companies',
    },

    useLoadData = true,

    ...props
}: PageCompaniesProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageCompaniesBase;
