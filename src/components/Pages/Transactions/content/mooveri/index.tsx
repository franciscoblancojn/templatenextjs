import { useRouter } from 'next/router';
import { LineByMonth } from '@/components/Graf/Line/template/LineByMonth';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import { MonthsConst, MonthsType } from '@/data/components/Month';
import {
    TableTransactions,
    TableItemTransactionsProps,
} from '@/components/Table/template/Transactions';
import { BarByMonth } from '@/components/Graf/Bar/template/BarByMonth';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    transactionsByMonths: {
        [id in MonthsType]: {
            count: number;
            total: number;
        };
    };
    transactions: TableItemTransactionsProps[];
}

export const MooveriBackoffice = (props: MooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <div className="m-b-50 flex flex-nowrap flex-gap-40 flex-justify-between width-p-100">
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">
                        {_t('Monthly Register Transactions')}
                    </Text>
                    <LineByMonth
                        datasets={[
                            {
                                label: 'Monthly Register Transactions',
                                data: MonthsConst.map((m) => {
                                    return (
                                        props?.transactionsByMonths?.[m]
                                            ?.count ?? 0
                                    );
                                }),
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">
                        {_t('Total Transactions')}
                    </Text>
                    <BarByMonth
                        datasets={[
                            {
                                label: 'Total Transactions',
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                                data: MonthsConst.map((m) => {
                                    return (
                                        props?.transactionsByMonths?.[m]
                                            ?.total ?? 0
                                    );
                                }),
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
            </div>
            <div className="m-b-50">
                <TableTransactions
                    items={props.transactions}
                    pagination={{
                        nItems: 200,
                    }}
                    loader={false}
                    styleTemplate="mooveri"
                />
            </div>
        </>
    );
};
