import { PageTransactionsClassProps } from '@/components/Pages/Transactions/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageTransactionsClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Transactions',
        validateLogin: true,
    },
};
