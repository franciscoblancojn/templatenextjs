import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageTransactionsProps, PageTransactions } from "./index";

export default {
    title: "Page/PageTransactions",
    component: PageTransactions,
} as Meta;

const Transactions: Story<PropsWithChildren<PageTransactionsProps>> = (args) => (
    <PageTransactions {...args}>Test Children</PageTransactions>
);

export const Index = Transactions.bind({});
Index.args = {
   
};
