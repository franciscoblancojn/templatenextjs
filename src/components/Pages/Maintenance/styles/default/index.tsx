import { PageMaintenanceClassProps } from '@/components/Pages/Maintenance/Base';
import LayoutLogin from '@/layout/Login';

export const _default: PageMaintenanceClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Maintenance',
    },
};
