import { PageMaintenanceClassProps } from '@/components/Pages/Maintenance/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageMaintenanceClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Maintenance',
    },
    styleContentWidth: {
        size: 500,
        className: `
            m-h-auto
        `,
    },
    styleContentWidthImg: {
        size: 200,
        className: `
            m-h-auto
        `,
    },
    styleImg: {
        src: 'maintenance.png',
        styleTemplate: 'tolinkme',
    },
    styleTitle: {
        className: 'text-center color-white',
        styleTemplate: 'tolinkme',
    },
};
