import { useMemo } from 'react';

import * as styles from '@/components/Pages/Maintenance/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageMaintenanceBaseProps,
    PageMaintenanceBase,
} from '@/components/Pages/Maintenance/Base';

export const PageMaintenanceStyle = { ...styles } as const;

export type PageMaintenanceStyles = keyof typeof PageMaintenanceStyle;

export interface PageMaintenanceProps extends PageMaintenanceBaseProps {
    styleTemplate?: PageMaintenanceStyles | ThemesType;
}

export const PageMaintenance = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageMaintenanceProps) => {
    const Style = useMemo(
        () =>
            PageMaintenanceStyle[styleTemplate as PageMaintenanceStyles] ??
            PageMaintenanceStyle._default,
        [styleTemplate]
    );
    return <PageMaintenanceBase {...Style} {...props} />;
};
export default PageMaintenance;
