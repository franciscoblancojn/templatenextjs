import { PageProfile2ClassProps } from '@/components/Pages/Profile2/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageProfile2ClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Profile2',
        validateLogin: true,
    },
};
