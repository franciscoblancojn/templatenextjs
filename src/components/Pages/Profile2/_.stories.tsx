import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageProfile2Props, PageProfile2 } from "./index";

export default {
    title: "Page/PageProfile2",
    component: PageProfile2,
} as Meta;

const Profile2: Story<PropsWithChildren<PageProfile2Props>> = (args) => (
    <PageProfile2 {...args}>Test Children</PageProfile2>
);

export const Index = Profile2.bind({});
Index.args = {
   
};
