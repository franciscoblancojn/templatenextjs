import Profile from '@/components/Profile';

import * as ProfileApi from '@/api/tolinkme/profile';
import { UserLoginProps } from '@/hook/useUser';
//import { DateTypeValue } from '@/data/components/Filters';
//import log from '@/functions/log';
import { ProfileDataProps } from '@/components/Profile/Base';
//import { useEffect, useState } from 'react';
//import { AnalitycsData } from '@/interfaces/AnalityscsData';

export interface TolinkmeProps {
    data: ProfileDataProps;
    user: UserLoginProps;
}
export interface ttt {}

export const Tolinkme = ({ data, user }: TolinkmeProps) => {
    const onSubmit = async (data: ProfileDataProps) => {
        const result = await ProfileApi.PUT(data, user);
        return result;
    };
    /*
    const [analitycs, setAnalitycs] = useState<AnalitycsData>({})
    const onLoadAnalitycs = async () => {
        try {
            const result = await ProfileApi.GETANALITYCS({
                profile_id: data.profile_uuid ?? '',
                user_id: data.user_uuid ?? '',
                start: `${0}`,
                end: `${new Date().getTime()}`,
            });
            setAnalitycs(result)
        } catch (error) {
            log('onChangeDate', error);

        }
    }
    useEffect(() => {
        onLoadAnalitycs()
    }, [])
*/
    return (
        <>
            <Profile {...data} onSubmit={onSubmit} name={user?.name ?? ''} />
        </>
    );
};
