import { useMemo } from 'react';

import * as styles from '@/components/Pages/Profile2/styles';
import * as connect from '@/components/Pages/Profile2/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageProfile2BaseProps,
    PageProfile2Base,
} from '@/components/Pages/Profile2/Base';

export const PageProfile2Style = { ...styles } as const;
export const PageProfile2Connect = { ...connect } as const;

export type PageProfile2Styles = keyof typeof PageProfile2Style;
export type PageProfile2Connects = keyof typeof PageProfile2Connect;

export interface PageProfile2Props extends PageProfile2BaseProps {
    styleTemplate?: PageProfile2Styles | ThemesType;
    company?: boolean;
}

export const PageProfile2 = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageProfile2Props) => {
    const Style = useMemo(
        () =>
            PageProfile2Style[styleTemplate as PageProfile2Styles] ??
            PageProfile2Style._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageProfile2Connect[styleTemplate as PageProfile2Connects] ??
            PageProfile2Connect._default,
        [styleTemplate]
    );

    return <PageProfile2Base<any> {...Style} {...Connect} {...props} />;
};
export default PageProfile2;
