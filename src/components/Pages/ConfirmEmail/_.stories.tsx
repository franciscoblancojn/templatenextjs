import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageConfirmEmailProps, PageConfirmEmail } from "./index";

export default {
    title: "Page/PageConfirmEmail",
    component: PageConfirmEmail,
} as Meta;

const ConfirmEmail: Story<PropsWithChildren<PageConfirmEmailProps>> = (args) => (
    <PageConfirmEmail {...args}>Test Children</PageConfirmEmail>
);

export const Index = ConfirmEmail.bind({});
Index.args = {
   
};
