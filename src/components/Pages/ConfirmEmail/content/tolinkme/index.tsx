import FormConfirmEmail from '@/components/Form/ConfirmEmail';
import { onSubmintConfirmEmail } from '@/components/Form/ConfirmEmail/Base';

export interface TolinkmeProps {
    onSubmintConfirmEmail: onSubmintConfirmEmail;
}

export const Tolinkme = ({ onSubmintConfirmEmail }: TolinkmeProps) => {
    return (
        <>
            <FormConfirmEmail
                styleTemplate="tolinkme"
                onSubmit={onSubmintConfirmEmail}
            />
        </>
    );
};
