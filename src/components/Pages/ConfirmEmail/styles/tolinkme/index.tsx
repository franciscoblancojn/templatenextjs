import { PageConfirmEmailClassProps } from '@/components/Pages/ConfirmEmail/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageConfirmEmailClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'ConfirmEmail',
        validateLogin: false,
    },
};
