import { PageConfirmEmailClassProps } from '@/components/Pages/ConfirmEmail/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageConfirmEmailClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'ConfirmEmail',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageConfirmEmailClassProps = {
    render: {
        Layout: Layout,
        title: 'ConfirmEmail',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
