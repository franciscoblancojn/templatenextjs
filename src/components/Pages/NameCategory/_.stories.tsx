import { Story, Meta } from "@storybook/react";


import { PageNameCategoryProps, PageNameCategory } from "./index";

export default {
    title: "Page/PageNameCategory",
    component: PageNameCategory,
} as Meta;

const Template: Story<PageNameCategoryProps> = (args) => (
    <PageNameCategory {...args}>Test Children</PageNameCategory>
);

export const Index = Template.bind({});
Index.args = {
};
