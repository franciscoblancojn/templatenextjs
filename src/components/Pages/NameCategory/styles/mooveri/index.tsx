import { PageNameCategoryClassProps } from '@/components/Pages/NameCategory/Base';
import TwoColumns from '@/layout/TwoColumns';

export const mooveri: PageNameCategoryClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Name Category',
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCustomer: PageNameCategoryClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Name Category',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCompany: PageNameCategoryClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Name Category',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};
