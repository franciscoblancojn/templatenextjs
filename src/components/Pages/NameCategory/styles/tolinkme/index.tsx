import { PageNameCategoryClassProps } from '@/components/Pages/NameCategory/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageNameCategoryClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Name Category',
        LayoutProps: {
            skip: true,
            showLangs: false,
        },
    },
    form: {
        styleTemplate: 'tolinkme',
    },
};
