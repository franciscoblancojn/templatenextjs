import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageCompaniesImportProps, PageCompaniesImport } from "./index";

export default {
    title: "Page/PageCompaniesImport",
    component: PageCompaniesImport,
} as Meta;

const CompaniesImport: Story<PropsWithChildren<PageCompaniesImportProps>> = (args) => (
    <PageCompaniesImport {...args}>Test Children</PageCompaniesImport>
);

export const Index = CompaniesImport.bind({});
Index.args = {
   
};
