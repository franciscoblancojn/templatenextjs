import { PageCompaniesImportConnectProps } from '@/components/Pages/CompaniesImport/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/CompaniesImport/content/default';

export const _default: PageCompaniesImportConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
