import { PageCompaniesImportConnectProps } from '@/components/Pages/CompaniesImport/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/CompaniesImport/content/tolinkme';

export const tolinkme: PageCompaniesImportConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
