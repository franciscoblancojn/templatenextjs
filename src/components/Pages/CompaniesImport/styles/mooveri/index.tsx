import { PageCompaniesImportClassProps } from '@/components/Pages/CompaniesImport/Base';
import { Layout } from '@/layout/Backoffice';

export const mooveri: PageCompaniesImportClassProps = {};

export const mooveriBackoffice: PageCompaniesImportClassProps = {
    render: {
        Layout: Layout,
        title: 'Home',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
