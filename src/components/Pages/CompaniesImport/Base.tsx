import { LayoutBase as LayoutCompaniesImport } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageCompaniesImportConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageCompaniesImportClassProps {
    render?: RenderLoaderProps;
}

export interface PageCompaniesImportBaseProps {}

export interface PageCompaniesImportProps<P = any>
    extends PageCompaniesImportBaseProps,
        PageCompaniesImportClassProps,
        PageCompaniesImportConnectProps<P> {}

export const PageCompaniesImportBase = <P,>({
    render = {
        Layout: LayoutCompaniesImport,
        title: 'CompaniesImport',
    },

    useLoadData = true,

    ...props
}: PageCompaniesImportProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageCompaniesImportBase;
