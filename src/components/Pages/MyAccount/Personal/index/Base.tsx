import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PagePersonalConnectProps {
    onLoad?: (data: RenderDataLoadPage) => Promise<any>;
    onLoadContent?: (data: any) => any;
}
export interface PagePersonalClassProps {
    render?: RenderLoaderProps;
    content?: any;
}

export interface PagePersonalBaseProps {}

export interface PagePersonalProps
    extends PagePersonalBaseProps,
        PagePersonalClassProps,
        PagePersonalConnectProps {}

export const PagePersonalBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Forgot Password',
    },
    content = <div />,
    ...props
}: PagePersonalProps) => {
    const [c, setC] = useState(<></>);
    const loadPage = async (data: RenderDataLoadPage) => {
        if (!data.user && !(process?.env?.['NEXT_PUBLIC_STORYBOK'] == 'TRUE')) {
            return 401;
        }
        if (props?.onLoad) {
            const d = await props?.onLoad?.(data);
            if (props?.onLoadContent) {
                const c = props?.onLoadContent(d);
                setC(c);
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };

    return (
        <>
            <RenderLoader {...render} loadPage={loadPage} validateLogin={true}>
                {content}
                {c}
            </RenderLoader>
        </>
    );
};
export default PagePersonalBase;
