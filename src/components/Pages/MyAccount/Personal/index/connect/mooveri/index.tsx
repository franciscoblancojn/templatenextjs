import { SubmitResult } from '@/components/Form/Base';
import {
    DataPersonalEditInputs,
    DataPersonalEdit,
} from '@/interfaces/PersonalEdit';
import { onSaveInput, onLoadData } from '@/api/mooveri/myAccount/personal';
import { PagePersonalConnectProps } from '@/components/Pages/MyAccount/Personal/index/Base';
import { Cmooveri as C } from '@/components/Pages/MyAccount/Personal/index/content/mooveri';
import { RenderDataLoadPage } from '@/components/Render';
import { UserLoginProps } from '@/hook/useUser';

export type onSaveInputProps = (
    user?: UserLoginProps
) => (
    id: DataPersonalEditInputs
) => (value: string) => Promise<SubmitResult> | SubmitResult;

export interface PagePersonalConnectMooveriProps
    extends PagePersonalConnectProps {
    onSaveInput?: onSaveInputProps;
    onLoadData?: (
        user?: UserLoginProps
    ) => Promise<DataPersonalEdit<string>> | DataPersonalEdit<string>;
}

export const movery: PagePersonalConnectMooveriProps = {
    onSaveInput:
        (user?: UserLoginProps) =>
        (id: DataPersonalEditInputs) =>
        async (value: string) => {
            const r: SubmitResult = await onSaveInput({
                id,
                value,
                user,
            });
            return r;
        },
    onLoad: async (data: RenderDataLoadPage) => {
        return await onLoadData(data?.user);
    },
    onLoadContent: (data) => <C {...data} />,
};

export const mooveriCustomer: PagePersonalConnectMooveriProps = movery;

export const mooveriCompany: PagePersonalConnectMooveriProps = movery;
