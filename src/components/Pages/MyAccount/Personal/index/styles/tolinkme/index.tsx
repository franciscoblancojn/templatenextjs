import { PagePersonalClassProps } from '@/components/Pages/MyAccount/Personal/index/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PagePersonalClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'My Account',
        validateLogin: true,
    },
};
