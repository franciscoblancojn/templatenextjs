import { FormMyAccount } from '@/components/Form/MyAccount';
import { onSubmintMyAccount } from '@/components/Form/MyAccount/Base';
import { UserLoginProps, useUser } from '@/hook/useUser';
import { DataMyAccount } from '@/interfaces/MyAccount';
import { SubmitResult } from '@/components/Form/Base';

export interface TolinkmeProps {
    onSubmit?: (user: UserLoginProps) => onSubmintMyAccount;
    onSendEmail?: (user: UserLoginProps) => () => void;
    onDelete?: (user: UserLoginProps) => () => Promise<SubmitResult>;
    data: DataMyAccount;
}

export const Tolinkme = ({ ...props }: TolinkmeProps) => {
    const { user, onLogOut } = useUser();
    return (
        <>
            <FormMyAccount
                styleTemplate="tolinkme"
                defaultData={props.data}
                onDelete={async () => {
                    const r = await props?.onDelete?.(user)?.();
                    if (r) {
                        onLogOut();
                        return r;
                    }
                    return {
                        status: 'ok',
                    };
                }}
                onSubmit={props?.onSubmit?.(user)}
                onSendEmail={props?.onSendEmail?.(user)}
            />
        </>
    );
};
