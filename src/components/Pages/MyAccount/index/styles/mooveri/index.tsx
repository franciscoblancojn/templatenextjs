import { PageMyAccountClassProps } from '@/components/Pages/MyAccount/index/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageMyAccountClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'MyAccount',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageMyAccountClassProps = {
    render: {
        Layout: Layout,
        title: 'MyAccount',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
