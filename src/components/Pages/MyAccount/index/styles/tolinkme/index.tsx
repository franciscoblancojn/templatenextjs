import { PageMyAccountClassProps } from '@/components/Pages/MyAccount/index/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageMyAccountClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'My Account',
        validateLogin: true,
    },
};
