import { useMemo } from 'react';

import * as styles from '@/components/Pages/MyAccount/index/styles';
import * as connect from '@/components/Pages/MyAccount/index/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageMyAccountBaseProps,
    PageMyAccountBase,
} from '@/components/Pages/MyAccount/index/Base';

export const PageMyAccountStyle = { ...styles } as const;
export const PageMyAccountConnect = { ...connect } as const;

export type PageMyAccountStyles = keyof typeof PageMyAccountStyle;
export type PageMyAccountConnects = keyof typeof PageMyAccountConnect;

export interface PageMyAccountProps extends PageMyAccountBaseProps {
    styleTemplate?: PageMyAccountStyles | ThemesType;
    company?: boolean;
}

export const PageMyAccount = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageMyAccountProps) => {
    const Style = useMemo(
        () =>
            PageMyAccountStyle[styleTemplate as PageMyAccountStyles] ??
            PageMyAccountStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageMyAccountConnect[styleTemplate as PageMyAccountConnects] ??
            PageMyAccountConnect._default,
        [styleTemplate]
    );

    return <PageMyAccountBase<any> {...Style} {...Connect} {...props} />;
};
export default PageMyAccount;
