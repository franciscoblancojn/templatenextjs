import { PageMyAccountConnectProps } from '@/components/Pages/MyAccount/index/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/MyAccount/index/content/tolinkme';
import * as MyAccountApi from '@/api/tolinkme/myAccount';
import { RenderDataLoadPage } from '@/components/Render';
import { DataMyAccount } from '@/interfaces/MyAccount';
import { onSubmintMyAccount } from '@/components/Form/MyAccount/Base';
import { UserLoginProps } from '@/hook/useUser';
import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';

const onSubmit: (user: UserLoginProps) => onSubmintMyAccount =
    (user: UserLoginProps) => async (data: DataMyAccount) => {
        const result: SubmitResult = await MyAccountApi.PUT({
            data,
            user,
        });
        return result;
    };

const onSendEmail = (user: UserLoginProps) => () => {
    log('UserLoginProps', user);
};

const onDelete = (user: UserLoginProps) => async () => {
    const result: SubmitResult = await MyAccountApi.DELETE({
        user,
    });
    return result;
};

export const tolinkme: PageMyAccountConnectProps<TolinkmeProps> = {
    onLoadData: async ({ user }: RenderDataLoadPage) => {
        const data: DataMyAccount = await MyAccountApi.GET({
            user,
        });
        return {
            data,
        };
    },
    onLoadContent: (props) => {
        return (
            <Tolinkme
                {...props}
                onDelete={onDelete}
                onSendEmail={onSendEmail}
                onSubmit={onSubmit}
            />
        );
    },
};
