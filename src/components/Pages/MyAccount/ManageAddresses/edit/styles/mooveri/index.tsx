import { PageManageAddressesEditClassProps } from '@/components/Pages/MyAccount/ManageAddresses/edit/Base';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageManageAddressesEditClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Manage Addresses',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};
