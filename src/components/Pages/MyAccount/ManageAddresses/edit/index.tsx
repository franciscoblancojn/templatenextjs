import { useMemo } from 'react';

import * as styles from '@/components/Pages/MyAccount/ManageAddresses/edit/styles';
import * as connect from '@/components/Pages/MyAccount/ManageAddresses/edit/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageManageAddressesEditBaseProps,
    PageManageAddressesEditBase,
} from '@/components/Pages/MyAccount/ManageAddresses/edit/Base';

export const PageManageAddressesEditStyle = { ...styles } as const;
export const PageManageAddressesEditConnect = { ...connect } as const;

export type PageManageAddressesEditStyles =
    keyof typeof PageManageAddressesEditStyle;
export type PageManageAddressesEditConnects =
    keyof typeof PageManageAddressesEditConnect;

export interface PageManageAddressesEditProps
    extends PageManageAddressesEditBaseProps {
    styleTemplate?: PageManageAddressesEditStyles | ThemesType;
    company?: boolean;
}

export const PageManageAddressesEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageManageAddressesEditProps) => {
    const Style = useMemo(
        () =>
            PageManageAddressesEditStyle[
                styleTemplate as PageManageAddressesEditStyles
            ] ?? PageManageAddressesEditStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageManageAddressesEditConnect[
                styleTemplate as PageManageAddressesEditConnects
            ] ?? PageManageAddressesEditConnect._default,
        [styleTemplate]
    );

    return <PageManageAddressesEditBase {...Style} {...Connect} {...props} />;
};
export default PageManageAddressesEdit;
