import {
    onUpdateAddress,
    onLoadAddressById,
} from '@/api/mooveri/myAccount/address';
import { PageManageAddressesEditConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/edit/Base';
import {
    Mooveri as C,
    MooveriProps,
} from '@/components/Pages/MyAccount/ManageAddresses/edit/content/mooveri';

import { RenderDataLoadPage } from '@/components/Render';
import log from '@/functions/log';

export interface mooveriDataProps extends MooveriProps {}

export const mooveri: PageManageAddressesEditConnectProps<mooveriDataProps> = {
    onLoadData: async (props: RenderDataLoadPage) => {
        log('RenderDataLoadPage', props);
        const defaultData = await onLoadAddressById({
            user: props.user,
            id: props?.query?.id,
        });
        return {
            defaultData,
        };
    },
    onLoadContent: (props) => {
        return <C {...props} onSubmit={onUpdateAddress} />;
    },
};
