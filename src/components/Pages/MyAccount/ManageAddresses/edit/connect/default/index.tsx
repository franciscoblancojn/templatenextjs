import { PageManageAddressesEditConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/edit/Base';
import { _default as C } from '@/components/Pages/MyAccount/ManageAddresses/edit/content/default';

export interface _defaultDataProps {}

export const _default: PageManageAddressesEditConnectProps<_defaultDataProps> =
    {
        onLoadData: async () => ({}),
        onLoadContent: (props) => {
            return <C {...props} />;
        },
    };
