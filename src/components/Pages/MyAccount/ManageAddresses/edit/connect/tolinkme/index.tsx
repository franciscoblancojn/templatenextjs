import { PageManageAddressesEditConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/edit/Base';
import { tolinkme as C } from '@/components/Pages/MyAccount/ManageAddresses/edit/content/tolinkme';

export interface tolinkmeDataProps {}

export const tolinkme: PageManageAddressesEditConnectProps<tolinkmeDataProps> =
    {
        onLoadData: async () => ({}),
        onLoadContent: (props) => {
            return <C {...props} />;
        },
    };
