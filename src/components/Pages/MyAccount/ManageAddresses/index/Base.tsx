import { LayoutBase as LayoutManageAddresses } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageManageAddressesConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageManageAddressesClassProps {
    render?: RenderLoaderProps;
}

export interface PageManageAddressesBaseProps {}

export interface PageManageAddressesProps<P = any>
    extends PageManageAddressesBaseProps,
        PageManageAddressesClassProps,
        PageManageAddressesConnectProps<P> {}

export const PageManageAddressesBase = <P,>({
    render = {
        Layout: LayoutManageAddresses,
        title: 'ManageAddresses',
    },

    useLoadData = true,

    ...props
}: PageManageAddressesProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (!p.user) {
            return 401;
        }
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageManageAddressesBase;
