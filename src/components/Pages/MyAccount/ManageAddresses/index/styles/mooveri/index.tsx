import { PageManageAddressesClassProps } from '@/components/Pages/MyAccount/ManageAddresses/index/Base';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageManageAddressesClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'ManageAddresses',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};
