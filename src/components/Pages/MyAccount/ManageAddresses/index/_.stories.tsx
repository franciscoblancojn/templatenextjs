import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageManageAddressesProps, PageManageAddresses } from "./index";

export default {
    title: "Page/MyAccount/ManageAddresses",
    component: PageManageAddresses,
} as Meta;

const ManageAddresses: Story<PropsWithChildren<PageManageAddressesProps>> = (args) => (
    <PageManageAddresses {...args}>Test Children</PageManageAddresses>
);

export const Index = ManageAddresses.bind({});
Index.args = {
   
};
