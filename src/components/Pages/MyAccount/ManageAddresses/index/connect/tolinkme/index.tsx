import { PageManageAddressesConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/index/Base';
import { tolinkme as C } from '@/components/Pages/MyAccount/ManageAddresses/index/content/tolinkme';

export interface tolinkmeDataProps {}

export const tolinkme: PageManageAddressesConnectProps<tolinkmeDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
