import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageManageAddressesAddProps, PageManageAddressesAdd } from "./index";

export default {
    title: "Page/MyAccount/ManageAddresses/Add",
    component: PageManageAddressesAdd,
} as Meta;

const ManageAddressesAdd: Story<PropsWithChildren<PageManageAddressesAddProps>> = (args) => (
    <PageManageAddressesAdd {...args}>Test Children</PageManageAddressesAdd>
);

export const Index = ManageAddressesAdd.bind({});
Index.args = {
   
};
