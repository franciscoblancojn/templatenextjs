import ContentWidth from '@/components/ContentWidth';
import GoBack from '@/components/GoBack';
import Space from '@/components/Space';
import Text from '@/components/Text';
import { FormManageAddreess } from '@/components/Form/ManageAddreess';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';
import { onSubmintManageAddress } from '@/components/Form/ManageAddreess/Base';

export interface MooveriProps {
    onSubmit?: onSubmintManageAddress;
}

export const Mooveri = (props: MooveriProps) => {
    const _t = useLang();
    return (
        <div className="p-h-15 p-sm-h-0">
            <GoBack
                text={_t('Back / My Account / Addresses')}
                href={url.myAccount.addresses.index}
                styleTemplate="mooveri2"
            />
            <Space size={10} />
            <Text styleTemplate="mooveri3">{_t('Manage Addresses')}</Text>
            <Space size={10} />
            <ContentWidth size={323}>
                <Text styleTemplate="mooveri4">
                    {_t(
                        'Here you can manage different addresses, your home, your family or friend place and so on, customize it according to your needs.'
                    )}
                </Text>
            </ContentWidth>
            <Space size={26} />
            <ContentWidth size={300}>
                <FormManageAddreess
                    styleTemplate="mooveri"
                    onSubmit={props?.onSubmit}
                    urlRedirect={url.myAccount.addresses.index}
                />
            </ContentWidth>
            <Space size={26} />
        </div>
    );
};
