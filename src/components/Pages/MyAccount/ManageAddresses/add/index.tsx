import { useMemo } from 'react';

import * as styles from '@/components/Pages/MyAccount/ManageAddresses/add/styles';
import * as connect from '@/components/Pages/MyAccount/ManageAddresses/add/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageManageAddressesAddBaseProps,
    PageManageAddressesAddBase,
} from '@/components/Pages/MyAccount/ManageAddresses/add/Base';

export const PageManageAddressesAddStyle = { ...styles } as const;
export const PageManageAddressesAddConnect = { ...connect } as const;

export type PageManageAddressesAddStyles =
    keyof typeof PageManageAddressesAddStyle;
export type PageManageAddressesAddConnects =
    keyof typeof PageManageAddressesAddConnect;

export interface PageManageAddressesAddProps
    extends PageManageAddressesAddBaseProps {
    styleTemplate?: PageManageAddressesAddStyles | ThemesType;
    company?: boolean;
}

export const PageManageAddressesAdd = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageManageAddressesAddProps) => {
    const Style = useMemo(
        () =>
            PageManageAddressesAddStyle[
                styleTemplate as PageManageAddressesAddStyles
            ] ?? PageManageAddressesAddStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageManageAddressesAddConnect[
                styleTemplate as PageManageAddressesAddConnects
            ] ?? PageManageAddressesAddConnect._default,
        [styleTemplate]
    );

    return <PageManageAddressesAddBase {...Style} {...Connect} {...props} />;
};
export default PageManageAddressesAdd;
