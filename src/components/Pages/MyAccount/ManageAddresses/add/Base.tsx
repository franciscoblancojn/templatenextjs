import { LayoutBase as LayoutManageAddressesAdd } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageManageAddressesAddConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageManageAddressesAddClassProps {
    render?: RenderLoaderProps;
}

export interface PageManageAddressesAddBaseProps {}

export interface PageManageAddressesAddProps<P = any>
    extends PageManageAddressesAddBaseProps,
        PageManageAddressesAddClassProps,
        PageManageAddressesAddConnectProps<P> {}

export const PageManageAddressesAddBase = <P,>({
    render = {
        Layout: LayoutManageAddressesAdd,
        title: 'ManageAddressesAdd',
    },

    useLoadData = true,

    ...props
}: PageManageAddressesAddProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (!p.user) {
            return 401;
        }
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageManageAddressesAddBase;
