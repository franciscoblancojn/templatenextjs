import { LayoutBase as LayoutPayments } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PagePaymentsConnectProps<P = any> extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PagePaymentsClassProps {
    render?: RenderLoaderProps;
}

export interface PagePaymentsBaseProps {}

export interface PagePaymentsProps<P = any>
    extends PagePaymentsBaseProps,
        PagePaymentsClassProps,
        PagePaymentsConnectProps<P> {}

export const PagePaymentsBase = <P,>({
    render = {
        Layout: LayoutPayments,
        title: 'Payments',
    },

    useLoadData = true,

    ...props
}: PagePaymentsProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (!p.user && !(process?.env?.['NEXT_PUBLIC_STORYBOK'] == 'TRUE')) {
            return 401;
        }
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PagePaymentsBase;
