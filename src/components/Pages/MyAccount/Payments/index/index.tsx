import { useMemo } from 'react';

import * as styles from '@/components/Pages/MyAccount/Payments/index/styles';
import * as connect from '@/components/Pages/MyAccount/Payments/index/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PagePaymentsBaseProps,
    PagePaymentsBase,
} from '@/components/Pages/MyAccount/Payments/index/Base';

export const PagePaymentsStyle = { ...styles } as const;
export const PagePaymentsConnect = { ...connect } as const;

export type PagePaymentsStyles = keyof typeof PagePaymentsStyle;
export type PagePaymentsConnects = keyof typeof PagePaymentsConnect;

export interface PagePaymentsProps extends PagePaymentsBaseProps {
    styleTemplate?: PagePaymentsStyles | ThemesType;
    company?: boolean;
}

export const PagePayments = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PagePaymentsProps) => {
    const Style = useMemo(
        () =>
            PagePaymentsStyle[styleTemplate as PagePaymentsStyles] ??
            PagePaymentsStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PagePaymentsConnect[styleTemplate as PagePaymentsConnects] ??
            PagePaymentsConnect._default,
        [styleTemplate]
    );
    return <PagePaymentsBase {...Style} {...Connect} {...props} />;
};
export default PagePayments;
