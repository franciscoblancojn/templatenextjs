import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PagePaymentsProps, PagePayments } from "./index";

export default {
    title: "Page/MyAccount/Payments",
    component: PagePayments,
} as Meta;

const Payments: Story<PropsWithChildren<PagePaymentsProps>> = (args) => (
    <PagePayments {...args}>Test Children</PagePayments>
);

export const Index = Payments.bind({});
Index.args = {
   
};
