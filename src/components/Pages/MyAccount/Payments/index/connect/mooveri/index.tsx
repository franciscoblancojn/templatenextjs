import { PagePaymentsConnectProps } from '@/components/Pages/MyAccount/Payments/index/Base';
import {
    Mooveri,
    MooveriProps,
} from '@/components/Pages/MyAccount/Payments/index/content/mooveri';

import {
    onLoadPayments,
    onDeletePayment,
} from '@/api/mooveri/myAccount/payment';
import { RenderDataLoadPage } from '@/components/Render';

export interface mooveriDataProps extends MooveriProps {}

export const mooveri: PagePaymentsConnectProps<mooveriDataProps> = {
    onLoadData: async (props: RenderDataLoadPage) => {
        const payments = await onLoadPayments(props.user);
        return {
            payments,
        };
    },
    onLoadContent: (props) => {
        return <Mooveri {...props} onDelete={onDeletePayment} />;
    },
};
