import { PagePaymentsClassProps } from '@/components/Pages/MyAccount/Payments/index/Base';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PagePaymentsClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Payments',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};
