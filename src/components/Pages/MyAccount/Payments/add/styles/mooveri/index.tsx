import { PagePaymentsAddClassProps } from '@/components/Pages/MyAccount/Payments/add/Base';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PagePaymentsAddClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'PaymentsAdd',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};
