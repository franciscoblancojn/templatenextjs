import { PagePaymentsAddConnectProps } from '@/components/Pages/MyAccount/Payments/add/Base';
import { _default as C } from '@/components/Pages/MyAccount/Payments/add/content/default';

export interface _defaultDataProps {}

export const _default: PagePaymentsAddConnectProps<_defaultDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
