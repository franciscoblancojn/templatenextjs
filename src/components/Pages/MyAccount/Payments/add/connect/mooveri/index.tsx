import { PagePaymentsAddConnectProps } from '@/components/Pages/MyAccount/Payments/add/Base';
import {
    Mooveri as C,
    MooveriProps,
} from '@/components/Pages/MyAccount/Payments/add/content/mooveri';

import { RenderDataLoadPage } from '@/components/Render';
import { onSaveCard } from '@/api/mooveri/myAccount/payment';
import log from '@/functions/log';

export interface mooveriDataProps extends MooveriProps {}

export const mooveri: PagePaymentsAddConnectProps<mooveriDataProps> = {
    onLoadData: async (props: RenderDataLoadPage) => {
        log('RenderDataLoadPage', props);
        return {};
    },
    onLoadContent: (props) => {
        return <C {...props} onSaveCardType={onSaveCard} />;
    },
};
