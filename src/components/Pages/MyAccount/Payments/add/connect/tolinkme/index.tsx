import { PagePaymentsAddConnectProps } from '@/components/Pages/MyAccount/Payments/add/Base';
import { tolinkme as C } from '@/components/Pages/MyAccount/Payments/add/content/tolinkme';

export interface tolinkmeDataProps {}

export const tolinkme: PagePaymentsAddConnectProps<tolinkmeDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
