import GoBack from '@/components/GoBack';
import Space from '@/components/Space';
import Text from '@/components/Text';
import { CardStripe } from '@/components/Input/CardStripe';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';
import ContentWidth from '@/components/ContentWidth';
import { onSaveCardType } from '@/interfaces/Card';

export interface MooveriProps {
    onSaveCardType?: onSaveCardType;
}

export const Mooveri = (props: MooveriProps) => {
    const _t = useLang();
    return (
        <div className="p-h-15 p-sm-h-0">
            <GoBack
                text={_t('Back / My Account / Payments')}
                href={url.myAccount.payments.index}
                styleTemplate="mooveri2"
            />
            <Space size={10} />
            <Text styleTemplate="mooveri3">{_t('Payment Information')}</Text>
            <Space size={10} />
            <Text styleTemplate="mooveri4">
                {_t('Manage different payment methods.')}
            </Text>
            <Space size={26} />
            <ContentWidth size={223}>
                <CardStripe
                    styleTemplate="mooveri"
                    textBtn="Add Payment"
                    onSaveCard={props.onSaveCardType}
                    urlRedirect={url.myAccount.payments.index}
                />
            </ContentWidth>
            <Space size={26} />
        </div>
    );
};
