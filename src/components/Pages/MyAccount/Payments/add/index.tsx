import { useMemo } from 'react';

import * as styles from '@/components/Pages/MyAccount/Payments/add/styles';
import * as connect from '@/components/Pages/MyAccount/Payments/add/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PagePaymentsAddBaseProps,
    PagePaymentsAddBase,
} from '@/components/Pages/MyAccount/Payments/add/Base';

export const PagePaymentsAddStyle = { ...styles } as const;
export const PagePaymentsAddConnect = { ...connect } as const;

export type PagePaymentsAddStyles = keyof typeof PagePaymentsAddStyle;
export type PagePaymentsAddConnects = keyof typeof PagePaymentsAddConnect;

export interface PagePaymentsAddProps extends PagePaymentsAddBaseProps {
    styleTemplate?: PagePaymentsAddStyles | ThemesType;
    company?: boolean;
}

export const PagePaymentsAdd = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PagePaymentsAddProps) => {
    const Style = useMemo(
        () =>
            PagePaymentsAddStyle[styleTemplate as PagePaymentsAddStyles] ??
            PagePaymentsAddStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PagePaymentsAddConnect[styleTemplate as PagePaymentsAddConnects] ??
            PagePaymentsAddConnect._default,
        [styleTemplate]
    );

    return <PagePaymentsAddBase {...Style} {...Connect} {...props} />;
};
export default PagePaymentsAdd;
