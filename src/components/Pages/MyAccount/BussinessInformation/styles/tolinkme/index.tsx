import { PageBussinessInformationClassProps } from '@/components/Pages/MyAccount/BussinessInformation/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageBussinessInformationClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'BussinessInformation',
        validateLogin: true,
    },
};
