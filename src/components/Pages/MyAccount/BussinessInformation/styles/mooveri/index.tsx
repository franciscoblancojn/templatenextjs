import { PageBussinessInformationClassProps } from '@/components/Pages/MyAccount/BussinessInformation/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageBussinessInformationClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'BussinessInformation',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageBussinessInformationClassProps = {
    render: {
        Layout: Layout,
        title: 'BussinessInformation',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
