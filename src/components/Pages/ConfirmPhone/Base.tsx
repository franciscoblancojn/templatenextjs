import {
    FormConfirmPhone,
    FormConfirmPhoneStyles,
} from '@/components/Form/ConfirmPhone';
import { LayoutLogin } from '@/layout/Login';
import RenderLoader, { RenderLoaderProps } from '@/components/Render';
import { FormConfirmPhoneBaseProps } from '@/components/Form/ConfirmPhone/Base';
import Theme, { ThemesType } from '@/config/theme';

export interface PageConfirmPhoneConnectProps
    extends FormConfirmPhoneBaseProps {}
export interface PageConfirmPhoneClassProps {
    render?: RenderLoaderProps;
    form?: {
        styleTemplate?: FormConfirmPhoneStyles | ThemesType;
    };
}

export interface PageConfirmPhoneBaseProps {}

export interface PageConfirmPhoneProps
    extends PageConfirmPhoneBaseProps,
        PageConfirmPhoneClassProps,
        PageConfirmPhoneConnectProps {}

export const PageConfirmPhoneBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Confirm Phone',
    },
    form = {
        styleTemplate: Theme?.styleTemplate ?? '_default',
    },
    ...props
}: PageConfirmPhoneProps) => {
    return (
        <>
            <RenderLoader {...render}>
                <FormConfirmPhone {...props} {...form} />
            </RenderLoader>
        </>
    );
};
export default PageConfirmPhoneBase;
