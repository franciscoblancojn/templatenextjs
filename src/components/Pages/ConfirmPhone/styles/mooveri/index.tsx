import { PageConfirmPhoneClassProps } from '@/components/Pages/ConfirmPhone/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageConfirmPhoneClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Confirm Phone',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
    },
};

export const mooveriCustomer: PageConfirmPhoneClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Confirm Phone',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCompany: PageConfirmPhoneClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Confirm Phone',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};
