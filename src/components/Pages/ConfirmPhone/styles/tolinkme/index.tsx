import { PageConfirmPhoneClassProps } from '@/components/Pages/ConfirmPhone/Base';
import { LayoutLogin } from '@/layout/Login';

export const tolinkme: PageConfirmPhoneClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Confirm Phone',
        LayoutProps: {
            skip: true,
            showLangs: false,
        },
    },
};
