import { useMemo } from 'react';

import * as styles from '@/components/Pages/ConfirmPhone/styles';
import * as connect from '@/components/Pages/ConfirmPhone/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageConfirmPhoneBaseProps,
    PageConfirmPhoneBase,
} from '@/components/Pages/ConfirmPhone/Base';

export const PageConfirmPhoneStyle = { ...styles } as const;
export const PageConfirmPhoneConnect = { ...connect } as const;

export type PageConfirmPhoneStyles = keyof typeof PageConfirmPhoneStyle;
export type PageConfirmPhoneConnects = keyof typeof PageConfirmPhoneConnect;

export interface PageConfirmPhoneProps extends PageConfirmPhoneBaseProps {
    styleTemplate?: PageConfirmPhoneStyles | ThemesType;
    company?: boolean;
}

export const PageConfirmPhone = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageConfirmPhoneProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageConfirmPhoneStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);

    const Style = useMemo(
        () =>
            PageConfirmPhoneStyle[
                styleTemplateSelected as PageConfirmPhoneStyles
            ] ?? PageConfirmPhoneStyle._default,
        [styleTemplateSelected]
    );
    const Connect = useMemo(
        () =>
            PageConfirmPhoneConnect[
                styleTemplateSelected as PageConfirmPhoneConnects
            ] ?? PageConfirmPhoneConnect._default,
        [styleTemplateSelected]
    );

    return <PageConfirmPhoneBase {...Style} {...Connect} {...props} />;
};
export default PageConfirmPhone;
