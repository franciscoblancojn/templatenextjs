import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageTransactionDetalleProps, PageTransactionDetalle } from "./index";

export default {
    title: "Page/PageTransactionDetalle",
    component: PageTransactionDetalle,
} as Meta;

const TransactionDetalle: Story<PropsWithChildren<PageTransactionDetalleProps>> = (args) => (
    <PageTransactionDetalle {...args}>Test Children</PageTransactionDetalle>
);

export const Index = TransactionDetalle.bind({});
Index.args = {
   
};
