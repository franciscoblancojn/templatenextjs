import { LayoutBase as LayoutTransactionDetalle } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageTransactionDetalleConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageTransactionDetalleClassProps {
    render?: RenderLoaderProps;
}

export interface PageTransactionDetalleBaseProps {}

export interface PageTransactionDetalleProps<P = any>
    extends PageTransactionDetalleBaseProps,
        PageTransactionDetalleClassProps,
        PageTransactionDetalleConnectProps<P> {}

export const PageTransactionDetalleBase = <P,>({
    render = {
        Layout: LayoutTransactionDetalle,
        title: 'TransactionDetalle',
    },

    useLoadData = true,

    ...props
}: PageTransactionDetalleProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageTransactionDetalleBase;
