import { useMemo } from 'react';

import * as styles from '@/components/Pages/TransactionDetalle/styles';
import * as connect from '@/components/Pages/TransactionDetalle/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageTransactionDetalleBaseProps,
    PageTransactionDetalleBase,
} from '@/components/Pages/TransactionDetalle/Base';

export const PageTransactionDetalleStyle = { ...styles } as const;
export const PageTransactionDetalleConnect = { ...connect } as const;

export type PageTransactionDetalleStyles =
    keyof typeof PageTransactionDetalleStyle;
export type PageTransactionDetalleConnects =
    keyof typeof PageTransactionDetalleConnect;

export interface PageTransactionDetalleProps
    extends PageTransactionDetalleBaseProps {
    styleTemplate?: PageTransactionDetalleStyles | ThemesType;
    company?: boolean;
}

export const PageTransactionDetalle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageTransactionDetalleProps) => {
    const Style = useMemo(
        () =>
            PageTransactionDetalleStyle[
                styleTemplate as PageTransactionDetalleStyles
            ] ?? PageTransactionDetalleStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageTransactionDetalleConnect[
                styleTemplate as PageTransactionDetalleConnects
            ] ?? PageTransactionDetalleConnect._default,
        [styleTemplate]
    );

    return (
        <PageTransactionDetalleBase<any> {...Style} {...Connect} {...props} />
    );
};
export default PageTransactionDetalle;
