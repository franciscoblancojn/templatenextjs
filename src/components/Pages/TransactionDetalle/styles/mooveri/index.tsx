import { PageTransactionDetalleClassProps } from '@/components/Pages/TransactionDetalle/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageTransactionDetalleClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'TransactionDetalle',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageTransactionDetalleClassProps = {
    render: {
        Layout: Layout,
        title: 'TransactionDetalle',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
