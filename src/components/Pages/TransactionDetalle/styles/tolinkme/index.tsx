import { PageTransactionDetalleClassProps } from '@/components/Pages/TransactionDetalle/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageTransactionDetalleClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'TransactionDetalle',
        validateLogin: true,
    },
};
