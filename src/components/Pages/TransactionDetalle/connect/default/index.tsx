import { PageTransactionDetalleConnectProps } from '@/components/Pages/TransactionDetalle/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/TransactionDetalle/content/default';

export const _default: PageTransactionDetalleConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
