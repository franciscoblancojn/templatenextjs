import { PageTransactionDetalleConnectProps } from '@/components/Pages/TransactionDetalle/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/TransactionDetalle/content/tolinkme';

export const tolinkme: PageTransactionDetalleConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
