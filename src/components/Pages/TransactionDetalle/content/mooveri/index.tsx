import { UserProps } from '@/interfaces/User';
import { useRouter } from 'next/router';
import { TableUser } from '@/components/Table/template/User';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import { TableTransactions } from '@/components/Table/template/Transactions';
import { TransactionProps } from '@/interfaces/Transaction';
import { CompanyProps } from '@/interfaces/Company';
import { TableCompany } from '@/components/Table/template/Company';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    user: UserProps;
    transacions: TransactionProps;
    companies: CompanyProps;
}

export const MooveriBackoffice = (props: MooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <div className="m-b-50">
                <Text styleTemplate="mooveri8">{_t('Transaction')}</Text>
                <br />
                <TableTransactions
                    items={[props?.transacions]}
                    styleTemplate="mooveri"
                />
            </div>
            <div className="m-b-50">
                <Text styleTemplate="mooveri8">{_t('User')}</Text>
                <br />
                <TableUser items={[props.user]} styleTemplate="mooveri" />
            </div>
            <div className="m-b-50">
                <Text styleTemplate="mooveri8">{_t('Company')}</Text>
                <br />
                <TableCompany
                    items={[props.companies]}
                    styleTemplate="mooveri"
                />
            </div>
        </>
    );
};
