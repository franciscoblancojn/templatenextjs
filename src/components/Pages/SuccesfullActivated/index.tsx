import { useMemo } from 'react';

import * as styles from '@/components/Pages/SuccesfullActivated/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageSuccesfullActivatedBaseProps,
    PageSuccesfullActivatedBase,
} from '@/components/Pages/SuccesfullActivated/Base';

export const PageSuccesfullActivatedStyle = { ...styles } as const;

export type PageSuccesfullActivatedStyles =
    keyof typeof PageSuccesfullActivatedStyle;

export interface PageSuccesfullActivatedProps
    extends PageSuccesfullActivatedBaseProps {
    styleTemplate?: PageSuccesfullActivatedStyles | ThemesType;
    company?: boolean;
}

export const PageSuccesfullActivated = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageSuccesfullActivatedProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageSuccesfullActivatedStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);

    const Style = useMemo(
        () =>
            PageSuccesfullActivatedStyle[
                styleTemplateSelected as PageSuccesfullActivatedStyles
            ] ?? PageSuccesfullActivatedStyle._default,
        [styleTemplateSelected]
    );
    return <PageSuccesfullActivatedBase {...Style} {...props} />;
};
export default PageSuccesfullActivated;
