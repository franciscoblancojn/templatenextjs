import { PageSuccesfullActivatedClassProps } from '@/components/Pages/SuccesfullActivated/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageSuccesfullActivatedClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Succesfull Activated',
    },
    styleTemplateFormSuccesfullActivated: 'tolinkme',
};
