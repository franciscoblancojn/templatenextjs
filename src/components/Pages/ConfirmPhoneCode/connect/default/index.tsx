import { onSubmintConfirmPhoneCode } from '@/components/Form/ConfirmPhoneCode/Base';
import { PageConfirmPhoneCodeConnectProps } from '@/components/Pages/ConfirmPhoneCode/Base';
import { DataCodePhone } from '@/interfaces/CodePhone';

import log from '@/functions/log';

const onSubmit: onSubmintConfirmPhoneCode = async (data: DataCodePhone) => {
    log('DataConfirmPhoneCode', data);
    return {
        status: 'ok',
        message: 'default',
    };
};

export const _default: PageConfirmPhoneCodeConnectProps = {
    onSubmit,
};
