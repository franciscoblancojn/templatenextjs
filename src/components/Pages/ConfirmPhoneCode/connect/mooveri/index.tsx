import { onSubmintConfirmPhoneCode } from '@/components/Form/ConfirmPhoneCode/Base';
import { PageConfirmPhoneCodeConnectProps } from '@/components/Pages/ConfirmPhoneCode/Base';

import { ConfirmPhoneCode as ConfirmPhoneCodeApi } from '@/api/mooveri/confirmPhone';
import url from '@/data/routes';
import { DataCodePhone } from '@/interfaces/CodePhone';

const onSubmit: onSubmintConfirmPhoneCode = async (data: DataCodePhone) => {
    const result = await ConfirmPhoneCodeApi(data);
    return result;
};

export const mooveri: PageConfirmPhoneCodeConnectProps = {
    onSubmit,
    urlRedirect: url?.index,
};
