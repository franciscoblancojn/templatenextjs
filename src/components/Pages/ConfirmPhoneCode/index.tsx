import { useMemo } from 'react';

import * as styles from '@/components/Pages/ConfirmPhoneCode/styles';
import * as connect from '@/components/Pages/ConfirmPhoneCode/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageConfirmPhoneCodeBaseProps,
    PageConfirmPhoneCodeBase,
} from '@/components/Pages/ConfirmPhoneCode/Base';

export const PageConfirmPhoneCodeStyle = { ...styles } as const;
export const PageConfirmPhoneCodeConnect = { ...connect } as const;

export type PageConfirmPhoneCodeStyles = keyof typeof PageConfirmPhoneCodeStyle;
export type PageConfirmPhoneCodeConnects =
    keyof typeof PageConfirmPhoneCodeConnect;

export interface PageConfirmPhoneCodeProps
    extends PageConfirmPhoneCodeBaseProps {
    styleTemplate?: PageConfirmPhoneCodeStyles | ThemesType;
    company?: boolean;
}

export const PageConfirmPhoneCode = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageConfirmPhoneCodeProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageConfirmPhoneCodeStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);

    const Style = useMemo(
        () =>
            PageConfirmPhoneCodeStyle[
                styleTemplateSelected as PageConfirmPhoneCodeStyles
            ] ?? PageConfirmPhoneCodeStyle._default,
        [styleTemplateSelected]
    );
    const Connect = useMemo(
        () =>
            PageConfirmPhoneCodeConnect[
                styleTemplateSelected as PageConfirmPhoneCodeConnects
            ] ?? PageConfirmPhoneCodeConnect._default,
        [styleTemplateSelected]
    );

    return <PageConfirmPhoneCodeBase {...Style} {...Connect} {...props} />;
};
export default PageConfirmPhoneCode;
