import { PageConfirmPhoneCodeClassProps } from '@/components/Pages/ConfirmPhoneCode/Base';
import { LayoutLogin } from '@/layout/Login';

export const tolinkme: PageConfirmPhoneCodeClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Confirm Phone Code',
        LayoutProps: {
            skip: true,
            showLangs: false,
        },
    },
    form: {
        styleTemplate: 'tolinkme',
    },
};
