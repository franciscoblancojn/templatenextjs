import { PageConfirmPhoneCodeClassProps } from '@/components/Pages/ConfirmPhoneCode/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageConfirmPhoneCodeClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Confirm Phone Code',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
    },
};

export const mooveriCustomer: PageConfirmPhoneCodeClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Confirm Phone Code',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCompany: PageConfirmPhoneCodeClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Confirm Phone Code',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};
