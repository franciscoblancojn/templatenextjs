import { Story, Meta } from "@storybook/react";


import { PageConfirmPhoneCodeProps, PageConfirmPhoneCode } from "./index";

export default {
    title: "Page/PageConfirmPhoneCode",
    component: PageConfirmPhoneCode,
} as Meta;

const Template: Story<PageConfirmPhoneCodeProps> = (args) => (
    <PageConfirmPhoneCode {...args}>Test Children</PageConfirmPhoneCode>
);

export const Index = Template.bind({});
Index.args = {
};
