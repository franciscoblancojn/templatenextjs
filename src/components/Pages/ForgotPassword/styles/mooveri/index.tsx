import { PageForgotPasswordClassProps } from '@/components/Pages/ForgotPassword/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageForgotPasswordClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Forgo tPassword',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
    },
};

export const mooveriCustomer: PageForgotPasswordClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Forgot Password',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
            showGoBack: true,
            showLogo: false,
            showLink: false,
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCompany: PageForgotPasswordClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Forgot Password',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
            showGoBack: true,
            showLogo: false,
            showLink: false,
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};
