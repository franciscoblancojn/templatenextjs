import { PageForgotPasswordClassProps } from '@/components/Pages/ForgotPassword/Base';
import { LayoutLogin } from '@/layout/Login';

export const tolinkme: PageForgotPasswordClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'ForgotPassword',
        LayoutProps: {
            goBack: true,
        },
        validateNotLogin: true,
    },
};
