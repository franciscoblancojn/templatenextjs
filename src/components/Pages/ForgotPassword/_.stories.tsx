import { Story, Meta } from "@storybook/react";


import { PageForgotPasswordProps, PageForgotPassword } from "./index";

export default {
    title: "Page/PageForgotPassword",
    component: PageForgotPassword,
} as Meta;

const Template: Story<PageForgotPasswordProps> = (args) => (
    <PageForgotPassword {...args}>Test Children</PageForgotPassword>
);

export const Index = Template.bind({});
Index.args = {
    
};
