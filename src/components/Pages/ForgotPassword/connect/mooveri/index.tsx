import { onSubmintForgotPassword } from '@/components/Form/ForgotPassword/Base';
import { PageForgotPasswordConnectProps } from '@/components/Pages/ForgotPassword/Base';
import { DataForgotPassword } from '@/interfaces/ForgotPassword';

import { ForgotPassword as ForgotPasswordApi } from '@/api/mooveri/forgotPassword';
import { UserRoles } from '@/hook/useUser';
import url from '@/data/routes';

const onSubmit: (type: UserRoles) => onSubmintForgotPassword =
    (type: UserRoles) => async (data: DataForgotPassword) => {
        const result = await ForgotPasswordApi({
            type,
            ...data,
        });
        return result;
    };

export const mooveri: PageForgotPasswordConnectProps = {
    onSubmit: onSubmit('client'),
    urlRedirect: url.forgotPasswordOk,
};

export const mooveriCustomer: PageForgotPasswordConnectProps = {
    onSubmit: onSubmit('client'),
    urlRedirect: url.forgotPasswordOk,
};

export const mooveriCompany: PageForgotPasswordConnectProps = {
    onSubmit: onSubmit('company'),
    urlRedirect: url.forgotPasswordOk,
};

export const mooveriBackoffice: PageForgotPasswordConnectProps = {
    onSubmit: onSubmit('backoffice'),
    urlRedirect: url.forgotPasswordOk,
};
