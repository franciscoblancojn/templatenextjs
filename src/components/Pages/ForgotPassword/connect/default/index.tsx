import { onSubmintForgotPassword } from '@/components/Form/ForgotPassword/Base';
import { PageForgotPasswordConnectProps } from '@/components/Pages/ForgotPassword/Base';
import { DataForgotPassword } from '@/interfaces/ForgotPassword';

import { ForgotPassword as ForgotPasswordApi } from '@/api/tolinkme/forgotPassword';

const onSubmit: onSubmintForgotPassword = async (data: DataForgotPassword) => {
    const result = await ForgotPasswordApi(data);
    return result;
};

export const _default: PageForgotPasswordConnectProps = {
    onSubmit,
};
