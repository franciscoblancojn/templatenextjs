import {
    FormForgotPassword,
    FormForgotPasswordStyles,
} from '@/components/Form/ForgotPassword';
import { LayoutLogin } from '@/layout/Login';
import RenderLoader, { RenderLoaderProps } from '@/components/Render';
import { FormForgotPasswordBaseProps } from '@/components/Form/ForgotPassword/Base';
import Theme, { ThemesType } from '@/config/theme';

export interface PageForgotPasswordConnectProps
    extends FormForgotPasswordBaseProps {}
export interface PageForgotPasswordClassProps {
    render?: RenderLoaderProps;
    form?: {
        styleTemplate?: FormForgotPasswordStyles | ThemesType;
    };
}

export interface PageForgotPasswordBaseProps {}

export interface PageForgotPasswordProps
    extends PageForgotPasswordBaseProps,
        PageForgotPasswordClassProps,
        PageForgotPasswordConnectProps {}

export const PageForgotPasswordBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Forgot Password',
    },
    form = {
        styleTemplate: Theme?.styleTemplate ?? '_default',
    },
    ...props
}: PageForgotPasswordProps) => {
    return (
        <>
            <RenderLoader {...render}>
                <FormForgotPassword {...props} {...form} />
            </RenderLoader>
        </>
    );
};
export default PageForgotPasswordBase;
