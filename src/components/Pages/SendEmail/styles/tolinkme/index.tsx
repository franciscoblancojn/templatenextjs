import { PageSendEmailClassProps } from '@/components/Pages/SendEmail/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageSendEmailClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Succesfully',
    },
    sizeContentWidth: 300,
    classNameContentWidth: `
        m-h-auto
    `,
    TitleProps: {
        className: `
            text-center
            color-white
        `,
        styleTemplate: 'tolinkme',
    },
    classNameText: `
        text-center
        color-white
    `,
    sizeContentWidthImg: 100,
    classNameContentWidthImg: `
        m-h-auto
    `,
    img: 'sendEmail.png',
    styleTemplateImg: 'tolinkme',
    classNameLink: `
        text-decoration-none-link
    `,
    styleTemplateButton: 'tolinkme',
};
