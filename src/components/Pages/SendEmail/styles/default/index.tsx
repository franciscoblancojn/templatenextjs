import { PageSendEmailClassProps } from '@/components/Pages/SendEmail/Base';
import LayoutLogin from '@/layout/Login';

export const _default: PageSendEmailClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Change Password',
    },
    sizeContentWidth: 300,
    classNameContentWidth: `
        m-h-auto
    `,
};
