import { Story, Meta } from "@storybook/react";


import { PageSendEmailProps, PageSendEmail } from "./index";

export default {
    title: "Page/PageSendEmail",
    component: PageSendEmail,
} as Meta;

const Template: Story<PageSendEmailProps> = (args) => (
    <PageSendEmail {...args}>Test Children</PageSendEmail>
);

export const Index = Template.bind({});
Index.args = {
};
