import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageCompanyCreateProps, PageCompanyCreate } from "./index";

export default {
    title: "Page/PageCompanyCreate",
    component: PageCompanyCreate,
} as Meta;

const CompanyCreate: Story<PropsWithChildren<PageCompanyCreateProps>> = (args) => (
    <PageCompanyCreate {...args}>Test Children</PageCompanyCreate>
);

export const Index = CompanyCreate.bind({});
Index.args = {
   
};
