import { PageCompanyCreateConnectProps } from '@/components/Pages/CompanyCreate/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/CompanyCreate/content/default';

export const _default: PageCompanyCreateConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
