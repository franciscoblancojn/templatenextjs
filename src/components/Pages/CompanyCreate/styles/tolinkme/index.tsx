import { PageCompanyCreateClassProps } from '@/components/Pages/CompanyCreate/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageCompanyCreateClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'CompanyCreate',
        validateLogin: true,
    },
};
