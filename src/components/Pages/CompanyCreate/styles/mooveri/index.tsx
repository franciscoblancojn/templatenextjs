import { PageCompanyCreateClassProps } from '@/components/Pages/CompanyCreate/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageCompanyCreateClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'CompanyCreate',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageCompanyCreateClassProps = {
    render: {
        Layout: Layout,
        title: 'CompanyCreate',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
