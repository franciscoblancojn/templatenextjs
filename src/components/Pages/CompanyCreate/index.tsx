import { useMemo } from 'react';

import * as styles from '@/components/Pages/CompanyCreate/styles';
import * as connect from '@/components/Pages/CompanyCreate/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageCompanyCreateBaseProps,
    PageCompanyCreateBase,
} from '@/components/Pages/CompanyCreate/Base';

export const PageCompanyCreateStyle = { ...styles } as const;
export const PageCompanyCreateConnect = { ...connect } as const;

export type PageCompanyCreateStyles = keyof typeof PageCompanyCreateStyle;
export type PageCompanyCreateConnects = keyof typeof PageCompanyCreateConnect;

export interface PageCompanyCreateProps extends PageCompanyCreateBaseProps {
    styleTemplate?: PageCompanyCreateStyles | ThemesType;
    company?: boolean;
}

export const PageCompanyCreate = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageCompanyCreateProps) => {
    const Style = useMemo(
        () =>
            PageCompanyCreateStyle[styleTemplate as PageCompanyCreateStyles] ??
            PageCompanyCreateStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageCompanyCreateConnect[
                styleTemplate as PageCompanyCreateConnects
            ] ?? PageCompanyCreateConnect._default,
        [styleTemplate]
    );

    return <PageCompanyCreateBase<any> {...Style} {...Connect} {...props} />;
};
export default PageCompanyCreate;
