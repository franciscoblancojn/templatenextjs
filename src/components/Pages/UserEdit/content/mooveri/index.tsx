import { UserProps } from '@/interfaces/User';
import { useRouter } from 'next/router';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import FormUserEdit from '@/components/Form/UserEdit';
import { onUpdateUser } from '@/api/mooveri/backoffice/users';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    user: UserProps;
    customer: UserProps;
}

export const MooveriBackoffice = (props: MooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <div className="m-b-50">
                <div className="flex flex-justify-between">
                    <Text styleTemplate="mooveri8">
                        {_t('User')}: {props.customer.name}
                    </Text>
                </div>
                <br />
                <FormUserEdit
                    defaultData={props.customer}
                    styleTemplate={'mooveri'}
                    onSubmit={(e) => {
                        return onUpdateUser(props?.user, e);
                    }}
                />
            </div>
        </>
    );
};
