import { PageUserEditClassProps } from '@/components/Pages/UserEdit/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageUserEditClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'UserEdit',
        validateLogin: true,
    },
};
