import { onLoadUserById } from '@/api/mooveri/backoffice/users';
import { PageUserEditConnectProps } from '@/components/Pages/UserEdit/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/UserEdit/content/mooveri';
import { RenderDataLoadPage } from '@/components/Render';
import { UserProps } from '@/interfaces/User';

export const mooveri: PageUserEditConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageUserEditConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const user = await onLoadUserById(props?.user, props?.query?.id);

            const propsComponent: MooveriBackofficeProps = {
                user: props?.user as UserProps,
                customer: user,
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
