import { PageUserEditConnectProps } from '@/components/Pages/UserEdit/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/UserEdit/content/tolinkme';

export const tolinkme: PageUserEditConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
