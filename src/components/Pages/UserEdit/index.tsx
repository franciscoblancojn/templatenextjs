import { useMemo } from 'react';

import * as styles from '@/components/Pages/UserEdit/styles';
import * as connect from '@/components/Pages/UserEdit/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageUserEditBaseProps,
    PageUserEditBase,
} from '@/components/Pages/UserEdit/Base';

export const PageUserEditStyle = { ...styles } as const;
export const PageUserEditConnect = { ...connect } as const;

export type PageUserEditStyles = keyof typeof PageUserEditStyle;
export type PageUserEditConnects = keyof typeof PageUserEditConnect;

export interface PageUserEditProps extends PageUserEditBaseProps {
    styleTemplate?: PageUserEditStyles | ThemesType;
    company?: boolean;
}

export const PageUserEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageUserEditProps) => {
    const Style = useMemo(
        () =>
            PageUserEditStyle[styleTemplate as PageUserEditStyles] ??
            PageUserEditStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageUserEditConnect[styleTemplate as PageUserEditConnects] ??
            PageUserEditConnect._default,
        [styleTemplate]
    );

    return <PageUserEditBase<any> {...Style} {...Connect} {...props} />;
};
export default PageUserEdit;
