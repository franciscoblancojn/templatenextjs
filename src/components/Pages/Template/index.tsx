import { useMemo } from 'react';

import * as styles from '@/components/Pages/Template/styles';
import * as connect from '@/components/Pages/Template/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageTemplateBaseProps,
    PageTemplateBase,
} from '@/components/Pages/Template/Base';

export const PageTemplateStyle = { ...styles } as const;
export const PageTemplateConnect = { ...connect } as const;

export type PageTemplateStyles = keyof typeof PageTemplateStyle;
export type PageTemplateConnects = keyof typeof PageTemplateConnect;

export interface PageTemplateProps extends PageTemplateBaseProps {
    styleTemplate?: PageTemplateStyles | ThemesType;
    company?: boolean;
}

export const PageTemplate = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageTemplateProps) => {
    const Style = useMemo(
        () =>
            PageTemplateStyle[styleTemplate as PageTemplateStyles] ??
            PageTemplateStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageTemplateConnect[styleTemplate as PageTemplateConnects] ??
            PageTemplateConnect._default,
        [styleTemplate]
    );

    return <PageTemplateBase<any> {...Style} {...Connect} {...props} />;
};
export default PageTemplate;
