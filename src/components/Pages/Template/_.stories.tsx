import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageTemplateProps, PageTemplate } from "./index";

export default {
    title: "Page/PageTemplate",
    component: PageTemplate,
} as Meta;

const Template: Story<PropsWithChildren<PageTemplateProps>> = (args) => (
    <PageTemplate {...args}>Test Children</PageTemplate>
);

export const Index = Template.bind({});
Index.args = {
   
};
