import { PageTemplateClassProps } from '@/components/Pages/Template/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageTemplateClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Template',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageTemplateClassProps = {
    render: {
        Layout: Layout,
        title: 'Template',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
