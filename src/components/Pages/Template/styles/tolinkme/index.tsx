import { PageTemplateClassProps } from '@/components/Pages/Template/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageTemplateClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Template',
        validateLogin: true,
    },
};
