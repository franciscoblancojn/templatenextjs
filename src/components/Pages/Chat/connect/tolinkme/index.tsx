import { PageChatConnectProps } from '@/components/Pages/Chat/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/Chat/content/tolinkme';

export const tolinkme: PageChatConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
