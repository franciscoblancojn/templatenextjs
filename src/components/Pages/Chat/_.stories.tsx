import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageChatProps, PageChat } from "./index";

export default {
    title: "Page/PageChat",
    component: PageChat,
} as Meta;

const Chat: Story<PropsWithChildren<PageChatProps>> = (args) => (
    <PageChat {...args}>Test Children</PageChat>
);

export const Index = Chat.bind({});
Index.args = {
   
};
