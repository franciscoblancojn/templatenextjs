import { PageChatClassProps } from '@/components/Pages/Chat/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageChatClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Chat',
        validateLogin: true,
    },
};
