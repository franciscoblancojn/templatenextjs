import Button from '@/components/Button';
import Footer from '@/components/Footer';
import Link from '@/components/Link';
import Text from '@/components/Text';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';
import PerfilFour from '@/lottin/PerfilFour';
import { PerfilOne } from '@/lottin/PerfilOne';
import PerfilThree from '@/lottin/PerfilThree';
import PerfilThree_two from '@/lottin/PerfilThree_two';
import PerfilTwo from '@/lottin/PerfilTwo';
import AngleBottom from '@/svg/AngleBotton';

export const Tolinkme = () => {
    const _t = useLang();
    return (
        <>
            <div className="repet overlay"></div>
            <div className="pos-r banner-princ ">
                <div className=" bg-gradient-darkAqua-brightPink banner-home p-v-90  ">
                    <div className="container p-h-20 flex flex-align-center banner-cols ">
                        <div className="width-p-60 z-index-2 banner-cols-1">
                            <Text className="color-white font-40 font-w-800 title-one-banner all-title">
                                {_t('All your content with a')}
                            </Text>
                            <Text className="color-brightPink font-w-900 font-69 title-two-banner all-title">
                                {_t('SINGLE CLICK')}
                            </Text>
                            <Text className="font-14 color-white font-w-400 description-banner">
                                {_t('Connect audiences to all of your content')}
                                <br />
                                {_t('with just one link.')}
                            </Text>
                            <div className="z-index-2  ">
                                <Link
                                    className="banner-button"
                                    styleTemplate="tolinkme8"
                                    href={url.register}
                                >
                                    <Button
                                        classNameBtn="m-t-10 img-perfil-2"
                                        styleTemplate="tolinkmeHome"
                                    >
                                        {_t('Get Started Now For Free')}
                                    </Button>
                                </Link>
                            </div>
                        </div>
                        <div className="width-p-40 z-index-2 banner-cols-2 perfil-principal">
                            <PerfilOne />
                        </div>
                        <div className="elementor-background-overlay"></div>
                        <div className="shpe-div color-white z-index-1 banner-princ-angle">
                            <AngleBottom size={10} />
                        </div>
                    </div>
                </div>
            </div>
            <div className="pos-r bg-white z-index-1 p-v-40 p-h-20">
                <div className="flex  flex-align-center flex-justify-center home-secction-2 flex-nowrap">
                    <img
                        className="home-secction-2-img"
                        src="../image/tolinkme/emojiHappy.png"
                    />
                    <div className="text-center ">
                        <Text className=" font-30 font-w-900 m-h-20 color-greyishTwo title-one-banner">
                            {_t('Personalize your profile')}
                        </Text>

                        <Text className=" font-40 font-w-900 m-h-20 color-brightPink title-two-banner">
                            {_t('QUICKLY AND EASILY')}
                        </Text>
                    </div>

                    <img
                        className="home-secction-2-img"
                        src="../image/tolinkme/emojiHappy2.png"
                    />
                </div>

                <div className="width-p-30 img-perfil-2  m-auto">
                    <PerfilTwo />
                </div>
                <div className="m-auto width-p-35 img-perfil-2 text-center p-v-20">
                    <Text styleTemplate="tolinkme33">
                        {_t(
                            'You can change, the background add an image, video, GIF or a gradient color, also you can play with the font color to make you feel right with your personal brand.'
                        )}
                    </Text>
                </div>
                <div className="width-p-10 m-v-20 m-auto img-perfil-2">
                    <Link styleTemplate="tolinkme8" href={url.register}>
                        <Button styleTemplate="tolinkmeHome2">
                            {_t('Join Now')}
                        </Button>
                    </Link>
                </div>
            </div>
            <div className="section-perfilThree pos-r bg-gradient-darkAqua-brightPink p-v-50 p-h-20">
                <div className="container">
                    <PerfilThree />{' '}
                </div>
                <div className="repet overlay"></div>
            </div>
            <div className="section-perfilThree-movil pos-r bg-gradient-darkAqua-brightPink p-v-50 p-h-20">
                <div className="container">
                    <PerfilThree_two />{' '}
                </div>
                <div className="repet overlay"></div>
            </div>
            <div className="pos-r  z-index-1 bg-white p-v-90 p-h-20 ">
                <div className="flex  flex-align-center  banner-cols container  bg-white">
                    <div className="width-p-50 banner-cols-1">
                        <PerfilFour />
                    </div>
                    <div className="width-p-50 banner-cols-1">
                        <Text className=" font-30 font-w-900  color-greyishTwo title-two-banner">
                            {_t('The power of')}
                        </Text>

                        <Text className=" font-40 font-w-900  color-brightPink title-one-banner">
                            {_t('ANALITYCS')}
                        </Text>
                        <Text
                            className="description-banner"
                            styleTemplate="tolinkme33"
                        >
                            {_t(
                                'Get analytics from your users completely free, have an average of clicks as well as the location of your audiences.'
                            )}
                        </Text>
                        <div className="width-p-30 m-v-20 img-perfil-2">
                            <Link styleTemplate="tolinkme8" href={url.register}>
                                <Button
                                    classNameBtn=" "
                                    styleTemplate="tolinkmeHome2"
                                >
                                    {_t('Sign Up Free')}
                                </Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
};
