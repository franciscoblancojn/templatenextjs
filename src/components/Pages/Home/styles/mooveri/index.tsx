import { PageHomeClassProps } from '@/components/Pages/Home/Base';
import { Layout } from '@/layout/Backoffice';

export const mooveri: PageHomeClassProps = {};

export const mooveriBackoffice: PageHomeClassProps = {
    render: {
        Layout: Layout,
        title: 'Home',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
