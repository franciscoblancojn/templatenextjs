import { PageHomeClassProps } from '@/components/Pages/Home/Base';
import { LayoutHome } from '@/layout/Home';

export const tolinkme: PageHomeClassProps = {
    render: {
        Layout: LayoutHome,
        title: 'Tolinkme Everything you are, in one single link',
        LayoutProps: {
            skip: true,
            showLangs: false,
        },
        description:
            'Tolinkme join various links on a single page to facilitate traffic between the different websites of your brand or content.',
    },
};
