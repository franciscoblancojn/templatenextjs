import { PageHomeConnectProps } from '@/components/Pages/Home/Base';

import { Tolinkme } from '@/components/Pages/Home/content/tolinkme';

export interface tolinkmeDataProps {}

export const tolinkme: PageHomeConnectProps<tolinkmeDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: () => <Tolinkme />,
    useLoadData: false,
};
