import { PageVerificationCodeClassProps } from '@/components/Pages/VerificationCode/Base';
import { LayoutPay } from '@/layout/Pay';

export const tolinkme: PageVerificationCodeClassProps = {
    render: {
        Layout: LayoutPay,
        title: 'VerificationCode',
        validateLogin: true,
    },
};
