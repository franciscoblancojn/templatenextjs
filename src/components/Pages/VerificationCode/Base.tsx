import { LayoutBase as LayoutVerificationCode } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageVerificationCodeConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageVerificationCodeClassProps {
    render?: RenderLoaderProps;
}

export interface PageVerificationCodeBaseProps {}

export interface PageVerificationCodeProps<P = any>
    extends PageVerificationCodeBaseProps,
        PageVerificationCodeClassProps,
        PageVerificationCodeConnectProps<P> {}

export const PageVerificationCodeBase = <P,>({
    render = {
        Layout: LayoutVerificationCode,
        title: 'VerificationCode',
    },

    useLoadData = true,

    ...props
}: PageVerificationCodeProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageVerificationCodeBase;
