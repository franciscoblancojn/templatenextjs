import { PageEsConnectProps } from '@/components/Pages/es/Base';
import { _default as C } from '@/components/Pages/es/content/default';

export interface _defaultDataProps {}

export const _default: PageEsConnectProps<_defaultDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
