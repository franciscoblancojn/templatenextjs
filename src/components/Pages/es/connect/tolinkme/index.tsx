import { PageEsConnectProps } from '@/components/Pages/es/Base';

import { Tolinkme } from '@/components/Pages/es/content/tolinkme';

export interface tolinkmeDataProps {}

export const tolinkme: PageEsConnectProps<tolinkmeDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: () => <Tolinkme />,
    useLoadData: false,
};
