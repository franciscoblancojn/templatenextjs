import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';
import LayoutHome from '@/layout/Home';

export interface PageEsConnectProps<P = any> extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageEsClassProps {
    render?: RenderLoaderProps;
}

export interface PageEsBaseProps {}

export interface PageEsProps<P = any>
    extends PageEsBaseProps,
        PageEsClassProps,
        PageEsConnectProps<P> {}

export const PageEsBase = <P,>({
    render = {
        Layout: LayoutHome,
        title: 'Es',
    },
    useLoadData = true,
    ...props
}: PageEsProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageEsBase;
