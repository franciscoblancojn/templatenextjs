import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageEsProps, PageEs } from "./index";

export default {
    title: "Page/PageEs",
    component: PageEs,
} as Meta;

const Es: Story<PropsWithChildren<PageEsProps>> = (args) => (
    <PageEs {...args}>Test Children</PageEs>
);

export const Index = Es.bind({});
Index.args = {
   
};
