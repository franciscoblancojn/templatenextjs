import { useMemo } from 'react';

import * as styles from '@/components/Pages/es/styles';
import * as connect from '@/components/Pages/es/connect';

import { Theme, ThemesType } from '@/config/theme';

import { PageEsBaseProps, PageEsBase } from '@/components/Pages/es/Base';

export const PageEsStyle = { ...styles } as const;
export const PageEsConnect = { ...connect } as const;

export type PageEsStyles = keyof typeof PageEsStyle;
export type PageEsConnects = keyof typeof PageEsConnect;

export interface PageEsProps extends PageEsBaseProps {
    styleTemplate?: PageEsStyles | ThemesType;
    company?: boolean;
}

export const PageEs = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageEsProps) => {
    const Style = useMemo(
        () =>
            PageEsStyle[styleTemplate as PageEsStyles] ?? PageEsStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageEsConnect[styleTemplate as PageEsConnects] ??
            PageEsConnect._default,
        [styleTemplate]
    );

    return <PageEsBase<any> {...Style} {...Connect} {...props} />;
};
export default PageEs;
