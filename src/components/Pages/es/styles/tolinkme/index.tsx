import { PageEsClassProps } from '@/components/Pages/es/Base';
import { LayoutHome } from '@/layout/Home';

export const tolinkme: PageEsClassProps = {
    render: {
        Layout: LayoutHome,
        title: 'Tolinkme Everything you are, in one single link',
        LayoutProps: {
            skip: true,
            showLangs: false,
        },
        description:
            'Tolinkme join various links on a single page to facilitate traffic between the different websites of your brand or content.',
    },
};
