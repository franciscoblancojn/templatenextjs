import { PageMySubscriptionsClassProps } from '@/components/Pages/MySubscriptions/index/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageMySubscriptionsClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'My Subscriptions',
        validateLogin: true,
    },
};
