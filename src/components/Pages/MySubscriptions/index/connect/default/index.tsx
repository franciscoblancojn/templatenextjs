import { PageMyAccountConnectProps } from '@/components/Pages/MyAccount/index/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/MyAccount/index/content/default';

export const _default: PageMyAccountConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
