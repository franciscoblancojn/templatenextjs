import { PageLoginConnectProps } from '@/components/Pages/Login/Base';

import { onSubmintLogin } from '@/components/Form/Login/Base';
import { DataLogin } from '@/interfaces/Login';

import { Login as LoginApi } from '@/api/mooveri/login';
import url from '@/data/routes';
import { UserRoles } from '@/hook/useUser';

const onSubmit: (type: UserRoles) => onSubmintLogin =
    (type: UserRoles) => async (data: DataLogin) => {
        const result = await LoginApi({
            type,
            ...data,
        });
        return result;
    };

export const mooveri: PageLoginConnectProps = {
    onSubmit: onSubmit('client'),
    urlRedirect: url.myAccount.index,
};

export const mooveriCustomer: PageLoginConnectProps = {
    onSubmit: onSubmit('client'),
    urlRedirect: url.myAccount.index,
};

export const mooveriCompany: PageLoginConnectProps = {
    onSubmit: onSubmit('company'),
    urlRedirect: url.myAccount.index,
};

export const mooveriBackoffice: PageLoginConnectProps = {
    onSubmit: onSubmit('backoffice'),
    urlRedirect: url.index,
};
