import { PageLoginClassProps } from '@/components/Pages/Login/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageLoginClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Login',
        validateNotLogin: true,
    },
};
