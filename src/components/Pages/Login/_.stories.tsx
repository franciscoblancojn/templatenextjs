import { Story, Meta } from "@storybook/react";


import { PageLoginProps, PageLogin } from "./index";

export default {
    title: "Page/PageLogin",
    component: PageLogin,
} as Meta;

const Template: Story<PageLoginProps> = (args) => (
    <PageLogin {...args}>Test Children</PageLogin>
);

export const Index = Template.bind({});
Index.args = {
};
