import { useMemo } from 'react';

import * as styles from '@/components/Pages/Login/styles';
import * as connect from '@/components/Pages/Login/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageLoginBaseProps,
    PageLoginBase,
} from '@/components/Pages/Login/Base';

export const PageLoginStyle = { ...styles } as const;
export const PageLoginConnect = { ...connect } as const;

export type PageLoginStyles = keyof typeof PageLoginStyle;
export type PageLoginConnects = keyof typeof PageLoginConnect;

export interface PageLoginProps extends PageLoginBaseProps {
    styleTemplate?: PageLoginStyles | ThemesType;
    company?: boolean;
}

export const PageLogin = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageLoginProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageLoginStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);

    const Style = useMemo(
        () =>
            PageLoginStyle[styleTemplateSelected as PageLoginStyles] ??
            PageLoginStyle._default,
        [styleTemplateSelected]
    );
    const Connect = useMemo(
        () =>
            PageLoginConnect[styleTemplateSelected as PageLoginConnects] ??
            PageLoginConnect._default,
        [styleTemplateSelected]
    );

    return <PageLoginBase {...Style} {...Connect} {...props} />;
};
export default PageLogin;
