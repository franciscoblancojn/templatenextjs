import { PageCompanyDetalleConnectProps } from '@/components/Pages/CompanyDetalle/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/CompanyDetalle/content/tolinkme';

export const tolinkme: PageCompanyDetalleConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
