import { PageCompanyDetalleConnectProps } from '@/components/Pages/CompanyDetalle/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/CompanyDetalle/content/default';

export const _default: PageCompanyDetalleConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
