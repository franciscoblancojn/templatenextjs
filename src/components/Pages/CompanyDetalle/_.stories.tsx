import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageCompanyDetalleProps, PageCompanyDetalle } from "./index";

export default {
    title: "Page/PageCompanyDetalle",
    component: PageCompanyDetalle,
} as Meta;

const CompanyDetalle: Story<PropsWithChildren<PageCompanyDetalleProps>> = (args) => (
    <PageCompanyDetalle {...args}>Test Children</PageCompanyDetalle>
);

export const Index = CompanyDetalle.bind({});
Index.args = {
   
};
