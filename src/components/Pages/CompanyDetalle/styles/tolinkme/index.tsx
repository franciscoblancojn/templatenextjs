import { PageCompanyDetalleClassProps } from '@/components/Pages/CompanyDetalle/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageCompanyDetalleClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'CompanyDetalle',
        validateLogin: true,
    },
};
