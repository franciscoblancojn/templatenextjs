import { PageCompanyDetalleClassProps } from '@/components/Pages/CompanyDetalle/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageCompanyDetalleClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'CompanyDetalle',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageCompanyDetalleClassProps = {
    render: {
        Layout: Layout,
        title: 'CompanyDetalle',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
