import { PageRegisterClassProps } from '@/components/Pages/Register/Base';
import { LayoutLogin } from '@/layout/Login';

export const tolinkme: PageRegisterClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Register',
        LayoutProps: {
            goBack: true,
        },
        // validateNotLogin: true,
    },
};
