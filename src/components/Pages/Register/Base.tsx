import { FormRegister, FormRegisterStyles } from '@/components/Form/Register';
import { LayoutLogin } from '@/layout/Login';
import RenderLoader, { RenderLoaderProps } from '@/components/Render';
import { FormRegisterBaseProps } from '@/components/Form/Register/Base';
import Theme, { ThemesType } from '@/config/theme';

export interface PageRegisterConnectProps extends FormRegisterBaseProps {}
export interface PageRegisterClassProps {
    render?: RenderLoaderProps;
    form?: {
        styleTemplate?: FormRegisterStyles | ThemesType;
    };
}

export interface PageRegisterBaseProps {}

export interface PageRegisterProps
    extends PageRegisterBaseProps,
        PageRegisterClassProps,
        PageRegisterConnectProps {}

export const PageRegisterBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Forgot Password',
    },
    form = {
        styleTemplate: Theme?.styleTemplate ?? '_default',
    },
    ...props
}: PageRegisterProps) => {
    return (
        <>
            <RenderLoader {...render}>
                <FormRegister {...props} {...form} />
            </RenderLoader>
        </>
    );
};
export default PageRegisterBase;
