import { useMemo } from 'react';

import * as styles from '@/components/Pages/User/styles';
import * as connect from '@/components/Pages/User/connect';

import { Theme, ThemesType } from '@/config/theme';

import { PageUserBaseProps, PageUserBase } from '@/components/Pages/User/Base';

export const PageUserStyle = { ...styles } as const;
export const PageUserConnect = { ...connect } as const;

export type PageUserStyles = keyof typeof PageUserStyle;
export type PageUserConnects = keyof typeof PageUserConnect;

export interface PageUserProps extends PageUserBaseProps {
    styleTemplate?: PageUserStyles | ThemesType;
    company?: boolean;
}

export const PageUser = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageUserProps) => {
    const Style = useMemo(
        () =>
            PageUserStyle[styleTemplate as PageUserStyles] ??
            PageUserStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageUserConnect[styleTemplate as PageUserConnects] ??
            PageUserConnect._default,
        [styleTemplate]
    );

    return <PageUserBase<any> {...Style} {...Connect} {...props} />;
};
export default PageUser;
