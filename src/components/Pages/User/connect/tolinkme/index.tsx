import { PageUserConnectProps } from '@/components/Pages/User/Base';
import { tolinkme as C } from '@/components/Pages/User/content/tolinkme';

export interface tolinkmeDataProps {}

export const tolinkme: PageUserConnectProps<tolinkmeDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
