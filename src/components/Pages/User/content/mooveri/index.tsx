import { LineByMonth } from '@/components/Graf/Line/template/LineByMonth';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import { MonthsConst, MonthsType } from '@/data/components/Month';
import Bar from '@/components/Graf/Bar';
import {
    TableUser,
    TableItemUserProps,
} from '@/components/Table/template/User';

export interface mooveriProps {}

export const Mooveri = () => <>Mooveri</>;

export interface mooveriBackofficeProps {
    usersByMonth: {
        [id in MonthsType]: number;
    };
    userByStatesEEUU: {
        [id: string]: number;
    };
    users: TableItemUserProps[];
}

export const MooveriBackoffice = (props: mooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <div className="m-b-50 flex flex-nowrap flex-gap-40 flex-justify-between width-p-100">
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">
                        {_t('Monthly Users Register')}
                    </Text>
                    <LineByMonth
                        datasets={[
                            {
                                label: 'Usuarios',
                                data: MonthsConst.map((m) => {
                                    return props?.usersByMonth?.[m] ?? 0;
                                }),
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">{_t('User per state')}</Text>
                    <Bar
                        labels={Object.keys(props.userByStatesEEUU).map(
                            (code) => code
                        )}
                        datasets={[
                            {
                                label: 'User',
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                                data: Object.keys(props.userByStatesEEUU).map(
                                    (code) =>
                                        props.userByStatesEEUU?.[code] ?? 0
                                ),
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
            </div>
            <div className="m-b-50">
                <TableUser
                    items={props.users}
                    pagination={{
                        nItems: 200,
                    }}
                    loader={false}
                    styleTemplate="mooveri"
                />
            </div>
        </>
    );
};
