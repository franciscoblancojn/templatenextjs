import { PageUserClassProps } from '@/components/Pages/User/Base';

import { Layout } from '@/layout/Backoffice';

export const mooveri: PageUserClassProps = {};

export const mooveriBackoffice: PageUserClassProps = {
    render: {
        Layout: Layout,
        title: 'User',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
