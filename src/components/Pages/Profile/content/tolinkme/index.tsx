import { UserLoginProps } from '@/hook/useUser';
import { ProfileDataProps } from '@/components/Profile/Base';
import Profile from '@/components/Profile';
import * as ProfileApi from '@/api/tolinkme/profile';

export interface TolinkmeProps {
    data: ProfileDataProps;
    user: UserLoginProps;
}

export const Tolinkme = ({ user, data }: TolinkmeProps) => {
    const onSubmit = async (data: ProfileDataProps) => {
        const result = await ProfileApi.PUT(data, user);
        return result;
    };
    return (
        <>
            <Profile
                {...data}
                onSubmit={onSubmit}
                name={user?.name ?? ''}
                addons={data?.addons}
            />
        </>
    );
};
