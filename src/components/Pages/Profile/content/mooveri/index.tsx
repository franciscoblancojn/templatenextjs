import { useRouter } from 'next/router';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {}

export const MooveriBackoffice = ({}: MooveriBackofficeProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};
