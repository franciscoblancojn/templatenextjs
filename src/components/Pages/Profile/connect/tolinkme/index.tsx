import { PageProfileConnectProps } from '@/components/Pages/Profile/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/Profile/content/tolinkme';
import { RenderDataLoadPage } from '@/components/Render';

import * as ProfileApi from '@/api/tolinkme/profile';
import * as MessagesApi from '@/api/tolinkme/messages';
import * as MonetizeApi from '@/api/tolinkme/monetize';
import * as ButtonMonetizeApi from '@/api/tolinkme/buttonMonetize';

export const tolinkme: PageProfileConnectProps<TolinkmeProps> = {
    onLoadData: async ({ user }: RenderDataLoadPage) => {
        if (!user) {
            throw {
                code: 401,
            };
        }
        //Get Profile
        const profile = await ProfileApi.GET({
            username: user?.name,
        });

        const messages = await MessagesApi.GET_MESSAGES({
            user,
        });

        let monetize = await MonetizeApi.GET_MONETIZE({
            user,
        });

        if (monetize == 401 || monetize == undefined) {
            monetize = {};
            throw 'error';
        }
        if (!monetize.balance) {
            monetize.balance = {};
        }
        if (!monetize.balance.btns) {
            monetize.balance.btns = [];
        }
        const btnsMonetize =
            await ButtonMonetizeApi.GET_BUTTONS_SUBCRIPTION_USER_ID({
                user,
            });

        if (btnsMonetize == 401 || btnsMonetize == undefined) {
            throw {
                code: 401,
            };
        }

        monetize.balance.btns = btnsMonetize.data?.btns ?? [];
        monetize.balance.getCurrentBalance =
            btnsMonetize.data?.getCurrentBalance ?? {};
        monetize.balance.currentMounthAmounthBalance =
            btnsMonetize.data?.currentMounthAmounthBalance ?? '';
        if (profile == 401) {
            throw {
                code: 401,
            };
        }

        if (messages == 401) {
            throw {
                code: 401,
            };
        }
        return {
            data: {
                ...profile,
                messages,
                monetize,
            },
            user,
        };
    },
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
