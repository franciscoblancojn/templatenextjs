import { PageProfileConnectProps } from '@/components/Pages/Profile/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/Profile/content/default';

export const _default: PageProfileConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
