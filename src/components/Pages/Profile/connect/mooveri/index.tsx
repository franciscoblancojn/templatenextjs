import { PageProfileConnectProps } from '@/components/Pages/Profile/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/Profile/content/mooveri';

export const mooveri: PageProfileConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageProfileConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async () => ({}),
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
