import { PageProfileClassProps } from '@/components/Pages/Profile/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageProfileClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Profile',
        validateLogin: true,
    },
};
