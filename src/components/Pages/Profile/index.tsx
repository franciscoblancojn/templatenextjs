import { useMemo } from 'react';

import * as styles from '@/components/Pages/Profile/styles';
import * as connect from '@/components/Pages/Profile/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageProfileBaseProps,
    PageProfileBase,
} from '@/components/Pages/Profile/Base';

export const PageProfileStyle = { ...styles } as const;
export const PageProfileConnect = { ...connect } as const;

export type PageProfileStyles = keyof typeof PageProfileStyle;
export type PageProfileConnects = keyof typeof PageProfileConnect;

export interface PageProfileProps extends PageProfileBaseProps {
    styleTemplate?: PageProfileStyles | ThemesType;
    company?: boolean;
}

export const PageProfile = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageProfileProps) => {
    const Style = useMemo(
        () =>
            PageProfileStyle[styleTemplate as PageProfileStyles] ??
            PageProfileStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageProfileConnect[styleTemplate as PageProfileConnects] ??
            PageProfileConnect._default,
        [styleTemplate]
    );

    return <PageProfileBase<any> {...Style} {...Connect} {...props} />;
};
export default PageProfile;
