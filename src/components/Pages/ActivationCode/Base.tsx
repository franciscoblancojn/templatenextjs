import { LayoutBase as LayoutActivationCode } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageActivationCodeConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageActivationCodeClassProps {
    render?: RenderLoaderProps;
}

export interface PageActivationCodeBaseProps {}

export interface PageActivationCodeProps<P = any>
    extends PageActivationCodeBaseProps,
        PageActivationCodeClassProps,
        PageActivationCodeConnectProps<P> {}

export const PageActivationCodeBase = <P,>({
    render = {
        Layout: LayoutActivationCode,
        title: 'ActivationCode',
    },

    useLoadData = true,

    ...props
}: PageActivationCodeProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageActivationCodeBase;
