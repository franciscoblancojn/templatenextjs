import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageActivationCodeProps, PageActivationCode } from "./index";

export default {
    title: "Page/PageActivationCode",
    component: PageActivationCode,
} as Meta;

const ActivationCode: Story<PropsWithChildren<PageActivationCodeProps>> = (args) => (
    <PageActivationCode {...args}>Test Children</PageActivationCode>
);

export const Index = ActivationCode.bind({});
Index.args = {
   
};
