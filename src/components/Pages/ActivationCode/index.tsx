import { useMemo } from 'react';

import * as styles from '@/components/Pages/ActivationCode/styles';
import * as connect from '@/components/Pages/ActivationCode/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageActivationCodeBaseProps,
    PageActivationCodeBase,
} from '@/components/Pages/ActivationCode/Base';

export const PageActivationCodeStyle = { ...styles } as const;
export const PageActivationCodeConnect = { ...connect } as const;

export type PageActivationCodeStyles = keyof typeof PageActivationCodeStyle;
export type PageActivationCodeConnects = keyof typeof PageActivationCodeConnect;

export interface PageActivationCodeProps extends PageActivationCodeBaseProps {
    styleTemplate?: PageActivationCodeStyles | ThemesType;
    company?: boolean;
}

export const PageActivationCode = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageActivationCodeProps) => {
    const Style = useMemo(
        () =>
            PageActivationCodeStyle[
                styleTemplate as PageActivationCodeStyles
            ] ?? PageActivationCodeStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageActivationCodeConnect[
                styleTemplate as PageActivationCodeConnects
            ] ?? PageActivationCodeConnect._default,
        [styleTemplate]
    );

    return <PageActivationCodeBase<any> {...Style} {...Connect} {...props} />;
};
export default PageActivationCode;
