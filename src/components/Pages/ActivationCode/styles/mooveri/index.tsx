import { PageActivationCodeClassProps } from '@/components/Pages/ActivationCode/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageActivationCodeClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'ActivationCode',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageActivationCodeClassProps = {
    render: {
        Layout: Layout,
        title: 'ActivationCode',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
