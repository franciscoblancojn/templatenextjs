import { PageActivationCodeClassProps } from '@/components/Pages/ActivationCode/Base';

import { LayoutPay } from '@/layout/Pay';

export const tolinkme: PageActivationCodeClassProps = {
    render: {
        Layout: LayoutPay,
        title: 'ActivationCode',
        validateLogin: true,
    },
};
