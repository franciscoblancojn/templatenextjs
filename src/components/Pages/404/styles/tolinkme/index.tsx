import { Page404ClassProps } from '@/components/Pages/404/Base';
import { LayoutLogin } from '@/layout/Login';

export const tolinkme: Page404ClassProps = {
    render: {
        Layout: LayoutLogin,
        title: '404',
        LayoutProps: {
            styleTemplate: 'tolinkme',
            goBack: true,
        },
        validateLogin: false,
    },
};
