import { Page404ConnectProps } from '@/components/Pages/404/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/404/content/mooveri';

export const mooveri: Page404ConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: Page404ConnectProps<MooveriBackofficeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <MooveriBackoffice {...props} />;
    },
};
