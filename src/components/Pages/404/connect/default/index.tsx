import { Page404ConnectProps } from '@/components/Pages/404/Base';
import { Defualt, DefualtProps } from '@/components/Pages/404/content/default';

export const _default: Page404ConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
