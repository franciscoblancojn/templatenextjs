import { PageIDClassProps } from '@/components/Pages/ID/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageIDClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Profile',
        LayoutProps: {
            showTerm: false,
            showLangs: false,
        },

        titleLoader: false,
        // loadHeadBeforeRender: true,
    },
};
