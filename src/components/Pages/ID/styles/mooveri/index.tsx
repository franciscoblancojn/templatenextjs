import { PageIDClassProps } from '@/components/Pages/ID/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageIDClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'ID',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: false,
    },
};

export const mooveriBackoffice: PageIDClassProps = {
    render: {
        Layout: Layout,
        title: 'ID',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: false,
    },
};
