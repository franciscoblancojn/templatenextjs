import { LayoutBase as LayoutID } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';
import { useRouter } from 'next/router';

export interface PageIDConnectProps<P = any> extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P | '404'> | P | '404';
    onLoadContent?: (props: P) => any;
}

export interface PageIDClassProps {
    render?: RenderLoaderProps;
}

export interface PageIDBaseProps {
    title?: string;
    description?: string;
    favicon?: string;
    uuidID?: string;
}

export interface PageIDProps<P = any>
    extends PageIDBaseProps,
        PageIDClassProps,
        PageIDConnectProps<P> {}

export const PageIDBase = <P,>({
    render = {
        Layout: LayoutID,
        title: 'ID',
    },

    useLoadData = true,
    title,
    description,
    favicon,
    uuidID,
    ...props
}: PageIDProps<P>) => {
    const route = useRouter();
    const [content, setContent] = useState<any>(<></>);
    const [renderData, setRenderData] = useState<RenderLoaderProps>({});
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent: any = await props?.onLoadData(p);
                if (propsComponent == '404') {
                    route.push('/404');
                }
                setRenderData(propsComponent?.renderData);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader
                {...render}
                {...renderData}
                loadPage={loadPage}
                title={title}
                description={description}
                favicon={favicon}
                uuidID={uuidID}
            >
                {content}
            </RenderLoader>
        </>
    );
};
export default PageIDBase;
