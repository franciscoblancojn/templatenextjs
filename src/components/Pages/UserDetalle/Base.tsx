import { LayoutBase as LayoutUserDetalle } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageUserDetalleConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageUserDetalleClassProps {
    render?: RenderLoaderProps;
}

export interface PageUserDetalleBaseProps {}

export interface PageUserDetalleProps<P = any>
    extends PageUserDetalleBaseProps,
        PageUserDetalleClassProps,
        PageUserDetalleConnectProps<P> {}

export const PageUserDetalleBase = <P,>({
    render = {
        Layout: LayoutUserDetalle,
        title: 'UserDetalle',
    },

    useLoadData = true,

    ...props
}: PageUserDetalleProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageUserDetalleBase;
