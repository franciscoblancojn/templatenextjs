import { useMemo } from 'react';

import * as styles from '@/components/Pages/UserDetalle/styles';
import * as connect from '@/components/Pages/UserDetalle/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageUserDetalleBaseProps,
    PageUserDetalleBase,
} from '@/components/Pages/UserDetalle/Base';

export const PageUserDetalleStyle = { ...styles } as const;
export const PageUserDetalleConnect = { ...connect } as const;

export type PageUserDetalleStyles = keyof typeof PageUserDetalleStyle;
export type PageUserDetalleConnects = keyof typeof PageUserDetalleConnect;

export interface PageUserDetalleProps extends PageUserDetalleBaseProps {
    styleTemplate?: PageUserDetalleStyles | ThemesType;
    company?: boolean;
}

export const PageUserDetalle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageUserDetalleProps) => {
    const Style = useMemo(
        () =>
            PageUserDetalleStyle[styleTemplate as PageUserDetalleStyles] ??
            PageUserDetalleStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageUserDetalleConnect[styleTemplate as PageUserDetalleConnects] ??
            PageUserDetalleConnect._default,
        [styleTemplate]
    );

    return <PageUserDetalleBase<any> {...Style} {...Connect} {...props} />;
};
export default PageUserDetalle;
