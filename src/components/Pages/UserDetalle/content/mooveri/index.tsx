import { UserProps } from '@/interfaces/User';
import { useRouter } from 'next/router';
import { TableUser } from '@/components/Table/template/User';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import { TableTransactions } from '@/components/Table/template/Transactions';
import { TransactionProps } from '@/interfaces/Transaction';
import { LineByMonth } from '@/components/Graf/Line/template/LineByMonth';
import { BarByMonth } from '@/components/Graf/Bar/template/BarByMonth';
import { MonthsConst, MonthsType } from '@/data/components/Month';
import { CompanyProps } from '@/interfaces/Company';
import { TableCompany } from '@/components/Table/template/Company';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    user: UserProps;
    transactions: TransactionProps[];
    transactionsByMonths: {
        [id in MonthsType]: {
            count: number;
            total: number;
        };
    };
    companies: CompanyProps[];
}

export const MooveriBackoffice = (props: MooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <div className="m-b-50">
                <div className="flex flex-justify-between">
                    <Text styleTemplate="mooveri8">{_t('User')}</Text>
                    {/* <Link
                        styleTemplate="mooveri3"
                        href={url.users.edit + props?.user?.id}
                    >
                        {_t('Editar')}
                    </Link> */}
                </div>
                <br />
                <TableUser items={[props.user]} styleTemplate="mooveri" />
            </div>
            <div className="m-b-50">
                <Text styleTemplate="mooveri8">{_t('Transactions')}</Text>
                <br />
                <TableTransactions
                    items={props?.transactions}
                    styleTemplate="mooveri"
                    pagination={{
                        nItems: 200,
                    }}
                    loader={false}
                />
            </div>
            <div className="m-b-50 flex flex-nowrap flex-gap-40 flex-justify-between width-p-100">
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">
                        {_t('Monthly Register Transactions')}
                    </Text>
                    <LineByMonth
                        datasets={[
                            {
                                label: 'Monthly Register Transactions',
                                data: MonthsConst.map((m) => {
                                    return (
                                        props?.transactionsByMonths?.[m]
                                            ?.count ?? 0
                                    );
                                }),
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">
                        {_t('Total Monthly Transactions')}
                    </Text>
                    <BarByMonth
                        datasets={[
                            {
                                label: 'Total Monthly Transactions',
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                                data: MonthsConst.map((m) => {
                                    return (
                                        props?.transactionsByMonths?.[m]
                                            ?.total ?? 0
                                    );
                                }),
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
            </div>
            <div className="m-b-50">
                <Text styleTemplate="mooveri8">{_t('Moving Companies')}</Text>
                <br />
                <TableCompany
                    items={props.companies}
                    pagination={{
                        nItems: 200,
                    }}
                    loader={false}
                    styleTemplate="mooveri"
                />
            </div>
        </>
    );
};
