import { PageUserDetalleConnectProps } from '@/components/Pages/UserDetalle/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/UserDetalle/content/default';

export const _default: PageUserDetalleConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
