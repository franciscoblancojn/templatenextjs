import { onLoadUserById } from '@/api/mooveri/backoffice/users';
import { PageUserDetalleConnectProps } from '@/components/Pages/UserDetalle/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/UserDetalle/content/mooveri';
import { RenderDataLoadPage } from '@/components/Render';
import { Months, MonthsType } from '@/data/components/Month';

export const mooveri: PageUserDetalleConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageUserDetalleConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const user = await onLoadUserById(props?.user, props?.query?.id);

            const transactions = user.transactions ?? [];

            const transactionsByMonths: any = {};

            Months.forEach((month: MonthsType, i) => {
                transactionsByMonths[month] =
                    transactions?.filter(
                        (transaction) => transaction.dateCreate.getMonth() == i
                    ).length ?? 0;
            });

            const propsComponent: MooveriBackofficeProps = {
                user,
                transactions,

                transactionsByMonths,

                companies: user?.companies ?? [],
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
