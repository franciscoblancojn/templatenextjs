import { PageUserDetalleConnectProps } from '@/components/Pages/UserDetalle/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/UserDetalle/content/tolinkme';

export const tolinkme: PageUserDetalleConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
