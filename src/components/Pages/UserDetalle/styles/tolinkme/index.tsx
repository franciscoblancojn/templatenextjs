import { PageUserDetalleClassProps } from '@/components/Pages/UserDetalle/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageUserDetalleClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'UserDetalle',
        validateLogin: true,
    },
};
