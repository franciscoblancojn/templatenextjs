import { onSubmintChangePassword } from '@/components/Form/ChangePassword/Base';
import { PageChangePasswordConnectProps } from '@/components/Pages/ChangePassword/Base';
import { DataChangePassword } from '@/interfaces/ChangePassword';
import { ChangePassword as ChangePasswordApi } from '@/api/mooveri/changePassword';
import url from '@/data/routes';
import { UserRoles } from '@/hook/useUser';

const onSubmit: (type: UserRoles) => onSubmintChangePassword =
    (type: UserRoles) => async (data: DataChangePassword) => {
        const result = await ChangePasswordApi({
            ...data,
            type,
        });
        return result;
    };

export const mooveri: PageChangePasswordConnectProps = {
    onSubmit: onSubmit('client'),
    urlRedirect: url.login,
};

export const mooveriCustomer: PageChangePasswordConnectProps = {
    onSubmit: onSubmit('client'),
    urlRedirect: url.login,
};

export const mooveriCompany: PageChangePasswordConnectProps = {
    onSubmit: onSubmit('company'),
    urlRedirect: url.login,
};

export const mooveriBackoffice: PageChangePasswordConnectProps = {
    onSubmit: onSubmit('backoffice'),
    urlRedirect: url.login,
};
