import { useMemo } from 'react';

import * as styles from '@/components/Pages/ChangePassword/styles';
import * as connect from '@/components/Pages/ChangePassword/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageChangePasswordBaseProps,
    PageChangePasswordBase,
} from '@/components/Pages/ChangePassword/Base';

export const PageChangePasswordStyle = { ...styles } as const;
export const PageChangePasswordConnect = { ...connect } as const;

export type PageChangePasswordStyles = keyof typeof PageChangePasswordStyle;
export type PageChangePasswordConnects = keyof typeof PageChangePasswordConnect;

export interface PageChangePasswordProps extends PageChangePasswordBaseProps {
    styleTemplate?: PageChangePasswordStyles | ThemesType;
    company?: boolean;
}

export const PageChangePassword = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageChangePasswordProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageChangePasswordStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);
    const Style = useMemo(
        () =>
            PageChangePasswordStyle[
                styleTemplateSelected as PageChangePasswordStyles
            ] ?? PageChangePasswordStyle._default,
        [styleTemplateSelected]
    );
    const Connect = useMemo(
        () =>
            PageChangePasswordConnect[
                styleTemplateSelected as PageChangePasswordConnects
            ] ?? PageChangePasswordConnect._default,
        [styleTemplateSelected]
    );

    return <PageChangePasswordBase {...Style} {...Connect} {...props} />;
};
export default PageChangePassword;
