import { Story, Meta } from "@storybook/react";


import { PageChangePasswordProps, PageChangePassword } from "./index";

export default {
    title: "Page/PageChangePassword",
    component: PageChangePassword,
} as Meta;

const Template: Story<PageChangePasswordProps> = (args) => (
    <PageChangePassword {...args}>Test Children</PageChangePassword>
);

export const Index = Template.bind({});
Index.args = {
    
};
