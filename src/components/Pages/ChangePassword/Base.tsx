import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import FormChangePassword, {
    FormChangePasswordStyles,
} from '@/components/Form/ChangePassword';
import { FormChangePasswordBaseProps } from '@/components/Form/ChangePassword/Base';
import Theme, { ThemesType } from '@/config/theme';
import { useState } from 'react';

export interface PageChangePasswordConnectProps
    extends RenderLoadPageProps,
        FormChangePasswordBaseProps {}

export interface PageChangePasswordClassProps {
    render?: RenderLoaderProps;
    form?: {
        styleTemplate: FormChangePasswordStyles | ThemesType;
    };
}

export interface PageChangePasswordBaseProps {}

export interface PageChangePasswordProps
    extends PageChangePasswordBaseProps,
        PageChangePasswordClassProps,
        PageChangePasswordConnectProps {}

export const PageChangePasswordBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Change Password',
    },
    form = {
        styleTemplate: Theme?.styleTemplate ?? '_default',
    },
    ...props
}: PageChangePasswordProps) => {
    const [token, setToken] = useState('');
    const onLoadPage = async ({ query }: RenderDataLoadPage) => {
        const token = query?.id;

        setToken(token);

        const r: LoadPageFunctionRespond = 'ok';
        return r;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={onLoadPage}>
                <FormChangePassword {...props} {...form} token={token} />
            </RenderLoader>
        </>
    );
};
export default PageChangePasswordBase;
