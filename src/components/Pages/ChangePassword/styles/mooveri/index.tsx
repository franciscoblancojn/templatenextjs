import { PageChangePasswordClassProps } from '@/components/Pages/ChangePassword/Base';
import TwoColumns from '@/layout/TwoColumns';

export const mooveri: PageChangePasswordClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Change Password',
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCustomer: PageChangePasswordClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Login',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCompany: PageChangePasswordClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Login',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};
