import { PageChangePasswordClassProps } from '@/components/Pages/ChangePassword/Base';
import LayoutLogin from '@/layout/Login';

export const tolinkme: PageChangePasswordClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Change Password',
    },
    form: {
        styleTemplate: 'tolinkme',
    },
};
