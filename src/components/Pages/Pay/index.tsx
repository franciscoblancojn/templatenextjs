import { useMemo } from 'react';

import * as styles from '@/components/Pages/Pay/styles';
import * as connect from '@/components/Pages/Pay/connect';

import { Theme, ThemesType } from '@/config/theme';

import { PagePayBaseProps, PagePayBase } from '@/components/Pages/Pay/Base';

export const PagePayStyle = { ...styles } as const;
export const PagePayConnect = { ...connect } as const;

export type PagePayStyles = keyof typeof PagePayStyle;
export type PagePayConnects = keyof typeof PagePayConnect;

export interface PagePayProps extends PagePayBaseProps {
    styleTemplate?: PagePayStyles | ThemesType;
    company?: boolean;
}

export const PagePay = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PagePayProps) => {
    const Style = useMemo(
        () =>
            PagePayStyle[styleTemplate as PagePayStyles] ??
            PagePayStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PagePayConnect[styleTemplate as PagePayConnects] ??
            PagePayConnect._default,
        [styleTemplate]
    );

    return <PagePayBase<any> {...Style} {...Connect} {...props} />;
};
export default PagePay;
