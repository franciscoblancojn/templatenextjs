import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PagePayProps, PagePay } from "./index";

export default {
    title: "Page/PagePay",
    component: PagePay,
} as Meta;

const Pay: Story<PropsWithChildren<PagePayProps>> = (args) => (
    <PagePay {...args}>Test Children</PagePay>
);

export const Index = Pay.bind({});
Index.args = {
   
};
