import { PagePayClassProps } from '@/components/Pages/Pay/Base';
import { LayoutPay } from '@/layout/Pay';

export const tolinkme: PagePayClassProps = {
    render: {
        Layout: LayoutPay,
        title: 'Pay',
        validateLogin: false,
    },
};
