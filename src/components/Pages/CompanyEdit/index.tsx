import { useMemo } from 'react';

import * as styles from '@/components/Pages/CompanyEdit/styles';
import * as connect from '@/components/Pages/CompanyEdit/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageCompanyEditBaseProps,
    PageCompanyEditBase,
} from '@/components/Pages/CompanyEdit/Base';

export const PageCompanyEditStyle = { ...styles } as const;
export const PageCompanyEditConnect = { ...connect } as const;

export type PageCompanyEditStyles = keyof typeof PageCompanyEditStyle;
export type PageCompanyEditConnects = keyof typeof PageCompanyEditConnect;

export interface PageCompanyEditProps extends PageCompanyEditBaseProps {
    styleTemplate?: PageCompanyEditStyles | ThemesType;
    company?: boolean;
}

export const PageCompanyEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageCompanyEditProps) => {
    const Style = useMemo(
        () =>
            PageCompanyEditStyle[styleTemplate as PageCompanyEditStyles] ??
            PageCompanyEditStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageCompanyEditConnect[styleTemplate as PageCompanyEditConnects] ??
            PageCompanyEditConnect._default,
        [styleTemplate]
    );

    return <PageCompanyEditBase<any> {...Style} {...Connect} {...props} />;
};
export default PageCompanyEdit;
