import { onLoadCompanyById } from '@/api/mooveri/backoffice/companies';
import { PageCompanyEditConnectProps } from '@/components/Pages/CompanyEdit/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/CompanyEdit/content/mooveri';
import { RenderDataLoadPage } from '@/components/Render';

export const mooveri: PageCompanyEditConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageCompanyEditConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const companies = await onLoadCompanyById(
                props?.user,
                props?.query?.id
            );

            const propsComponent: MooveriBackofficeProps = {
                user: props?.user ?? {},
                companies,
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
