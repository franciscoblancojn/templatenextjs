import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageCompanyEditProps, PageCompanyEdit } from "./index";

export default {
    title: "Page/PageCompanyEdit",
    component: PageCompanyEdit,
} as Meta;

const CompanyEdit: Story<PropsWithChildren<PageCompanyEditProps>> = (args) => (
    <PageCompanyEdit {...args}>Test Children</PageCompanyEdit>
);

export const Index = CompanyEdit.bind({});
Index.args = {
   
};
