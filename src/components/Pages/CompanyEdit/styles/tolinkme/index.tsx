import { PageCompanyEditClassProps } from '@/components/Pages/CompanyEdit/Base';
import { LayoutBase } from '@/layout/Base';

export const tolinkme: PageCompanyEditClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'CompanyEdit',
        validateLogin: true,
    },
};
