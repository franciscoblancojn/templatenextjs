import { TypeRS } from '@/data/components/RS';

import { LayoutLogin } from '@/layout/Login';
import RenderLoader, { RenderLoadPageProps } from '@/components/Render';
import { InfoProfile } from '@/components/InfoProfile';
import { InfoProfileBaseProps } from '@/components/InfoProfile/Base';
import RSLink from '@/components/RS/Link';
import { ContentWidth } from '@/components/ContentWidth';
import Space from '@/components/Space';
import { useEffect, useMemo } from 'react';
import { useUser } from '@/hook/useUser';

export interface PageProfilePublicTypeRSProps {
    uuid?: string;
    id: TypeRS;
    href: string;
    blocked?: boolean;
    active?: boolean;
}

export interface PageProfilePublicProps extends RenderLoadPageProps {
    uuid?: string;
    user_uuid?: string;
    RS: PageProfilePublicTypeRSProps[];
    onClickRS?: (uuid?: string) => void;
    info: InfoProfileBaseProps;
    name: string;
}

export const PageProfilePublic = async ({
    name,
    RS,
    info,
    onClickRS,
    ...props
}: PageProfilePublicProps) => {
    const { user, load } = useUser();
    const fondo = useMemo(() => {
        const classImg = 'pos-f inset-0 width-p-100 height-p-100';
        return (
            <picture className={classImg}>
                {info.bgDesktop && info.bgDesktop != '' ? (
                    <source
                        srcSet={info.bgDesktop}
                        media="(min-width: 575px)"
                    />
                ) : (
                    <></>
                )}
                {info.bgMovil && info.bgMovil != '' ? (
                    <img
                        src={info.bgMovil}
                        alt={info.name}
                        className={classImg}
                    />
                ) : (
                    <></>
                )}
                {info.capaColor && (
                    <div
                        className={`capa pos-f inset-0 width-p-100 height-p-100 
                            ${info.bgMovil == '' ? 'opacity-10' : 'opacity-5'}
                            ${
                                info.bgDesktop == ''
                                    ? 'opacity-sm-10'
                                    : 'opacity-sm-5'
                            }
                        `}
                        style={{
                            background: info.capaColor,
                        }}
                    ></div>
                )}
            </picture>
        );
    }, [info]);

    useEffect(() => {}, [user, load]);

    return (
        <>
            <RenderLoader
                loadPage={props.loadPage}
                title={info?.name != '' ? info?.name : name}
                Layout={LayoutLogin}
                LayoutProps={{
                    fondo,
                    showLogo: info.tolinkmeLogo,
                    showTerm: false,
                }}
            >
                <ContentWidth size={375} className="m-h-auto r">
                    <div className="flex flex-column flex-align-center">
                        <InfoProfile {...info} styleTemplate="tolinkme2" />
                        <Space size={30} />
                        {RS.filter(
                            (rs: PageProfilePublicTypeRSProps) => rs.active
                        ).map((rs: PageProfilePublicTypeRSProps, i) => (
                            <div key={i} className="width-p-100">
                                <RSLink
                                    buttonRound={info.buttonRound}
                                    btnColor={info.btnColor}
                                    btnBgColor={info.btnBgColor}
                                    showIconButton={info.showIconButton}
                                    btnBorderColor={info.btnBorderColor}
                                    buttonHeight={info.buttonHeight}
                                    {...rs}
                                    name={rs.id}
                                    img={rs.id ?? ''}
                                    onClick={onClickRS}
                                />
                                <Space size={9} />
                            </div>
                        ))}
                    </div>
                </ContentWidth>
            </RenderLoader>
        </>
    );
};
export default PageProfilePublic;
