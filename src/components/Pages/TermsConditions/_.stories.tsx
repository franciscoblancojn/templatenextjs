import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageTermsConditionsProps, PageTermsConditions } from "./index";

export default {
    title: "Page/PageTermsConditions",
    component: PageTermsConditions,
} as Meta;

const Template: Story<PropsWithChildren<PageTermsConditionsProps>> = (args) => (
    <PageTermsConditions {...args}>Test Children</PageTermsConditions>
);

export const Index = Template.bind({});
Index.args = {
   
};
