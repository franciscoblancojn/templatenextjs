import FormPayment from '@/components/Form/Payment';
import ProfileAndSubscriptions from '@/components/ProfileAndSubscriptions';

export interface TolinkmeProps {}

export const Tolinkme = ({}: TolinkmeProps) => {
    return (
        <>
            <div>
                <ProfileAndSubscriptions
                    text="Billing Details"
                    text2="Register now and get these subscription:"
                    img="https://i.blogs.es/e91376/650_1000_we-love-your-genes-img-models-instagram_-3-/450_1000.jpg"
                    btn={{
                        monetize: true,
                        active: true,
                    }}
                    style={{
                        description: '',
                        name: '',
                        web: '',
                    }}
                />
                <div className="p-h-10">
                    <FormPayment btn_uuid="" />
                </div>
            </div>
        </>
    );
};
