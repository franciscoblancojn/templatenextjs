import { PagePaymentMethodClassProps } from '@/components/Pages/PaymentMethod/Base';

import { LayoutPay } from '@/layout/Pay';

export const tolinkme: PagePaymentMethodClassProps = {
    render: {
        Layout: LayoutPay,
        title: 'PaymentMethod',
        validateLogin: true,
    },
};
