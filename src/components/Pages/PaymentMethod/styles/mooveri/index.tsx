import { PagePaymentMethodClassProps } from '@/components/Pages/PaymentMethod/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PagePaymentMethodClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'PaymentMethod',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PagePaymentMethodClassProps = {
    render: {
        Layout: Layout,
        title: 'PaymentMethod',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
