import { LayoutBase as LayoutPaymentMethod } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PagePaymentMethodConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PagePaymentMethodClassProps {
    render?: RenderLoaderProps;
}

export interface PagePaymentMethodBaseProps {}

export interface PagePaymentMethodProps<P = any>
    extends PagePaymentMethodBaseProps,
        PagePaymentMethodClassProps,
        PagePaymentMethodConnectProps<P> {}

export const PagePaymentMethodBase = <P,>({
    render = {
        Layout: LayoutPaymentMethod,
        title: 'PaymentMethod',
    },

    useLoadData = true,

    ...props
}: PagePaymentMethodProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PagePaymentMethodBase;
