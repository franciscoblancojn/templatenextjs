import { PagePaymentMethodConnectProps } from '@/components/Pages/PaymentMethod/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/PaymentMethod/content/default';

export const _default: PagePaymentMethodConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
