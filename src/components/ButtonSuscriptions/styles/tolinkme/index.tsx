import { ButtonSuscriptionsClassProps } from '@/components/ButtonSuscriptions/Base';

export const tolinkme: ButtonSuscriptionsClassProps = {
    classNameContent: `
        flex 
        flex-align-center 
        flex-nowrap
        flex-justify-between
        m-v-10
    `,
    classNameContentButton: `
        cursor-pointer
        
        flex
        flex-justify-center
        flex-align-center
        width-p-90
        border-style-solid 
        border-3 
        border-black 
        height-45 
        border-radius-10
    `,
    classNameContentIconLogo: `
        border-radius-100 
        text-center
        m-r-5
        flex
    `,
    classNameTitleButton: `
       text-capitalize
       m-r-3
       
        
    `,
};
