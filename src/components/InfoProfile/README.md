# InfoProfile

## Import

```js
import { InfoProfile, InfoProfileStyles } from '@/components/InfoProfile';
```

## Props

```ts
interface InfoProfileProps {
    styleTemplate?: InfoProfileStyles;
    avatar?: string;
    name: string;
    web: string;
    description: string;
    bgDesktop?: string;
    bgMovil?: string;
    capaColor?: string;
    fontColor?: string;
    tolinkmeLogo?: boolean;
}
```

## Use

```js
<InfoProfile name="Name" web="Web" description="Description" />
```
