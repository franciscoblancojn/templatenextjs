import { InfoProfileClassProps } from '@/components/InfoProfile/Base';

export const tolinkme: InfoProfileClassProps = {
    classNameContent: `
        bg-whiteTwo
        border-sm-radius-35
        p-15 p-sm-30
        flex
        flex-column
        flex-align-center
    `,
    sizeContent: 1000,
    classNameContentEditView: `
        flex
        flex-align-center
        flex-justify-between
        m-b-20
    `,
    classNameContentView: `
        flex
        flex-align-center
        flex-gap-column-10
        pointer
        font-14 font-w-900 font-nunito
        color-brightPink
    `,
    styleTemplateAvatar: 'tolinkme',
    styleTemplateDataName: 'tolinkme3',
    styleTemplateDataWeb: 'tolinkme4',
    classNameDataDesciption: `
        text-center
        word-break-all
    `,
    styleTemplateDataDesciption: 'tolinkme5',
};

export const tolinkme2: InfoProfileClassProps = {
    classNameContent: `
        flex
        flex-column
        flex-align-center
        p-h-5
    `,
    sizeContent: 375,
    classNameContentEditView: `
        d-none
    `,
    styleTemplateDataName: 'tolinkme7',
    styleTemplateAvatar: 'tolinkme3',
    classNameDataDesciption: `
        text-center
        word-break-all
    `,
};
