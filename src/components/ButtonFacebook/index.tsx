import { useMemo } from 'react';

import * as styles from '@/components/ButtonFacebook/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ButtonFacebookBaseProps,
    ButtonFacebookBase,
} from '@/components/ButtonFacebook/Base';

export const ButtonFacebookStyle = { ...styles } as const;

export type ButtonFacebookStyles = keyof typeof ButtonFacebookStyle;

export interface ButtonFacebookProps extends ButtonFacebookBaseProps {
    styleTemplate?: ButtonFacebookStyles | ThemesType;
}

export const ButtonFacebook = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    children,
    ...props
}: ButtonFacebookProps) => {
    const Style = useMemo(
        () =>
            ButtonFacebookStyle[styleTemplate as ButtonFacebookStyles] ??
            ButtonFacebookStyle._default,
        [styleTemplate]
    );

    return (
        <ButtonFacebookBase {...Style} {...props}>
            {children}
            <div></div>
        </ButtonFacebookBase>
    );
};
export default ButtonFacebook;
