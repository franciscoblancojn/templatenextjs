import { ButtonFacebookClassProps } from '@/components/ButtonFacebook/Base';
import { Facebook } from '@/svg/Facebook';

export const mooveri: ButtonFacebookClassProps = {
    icon: <Facebook size={25} />,
    styleTemplateBtn: 'mooveriFacebook',
};
