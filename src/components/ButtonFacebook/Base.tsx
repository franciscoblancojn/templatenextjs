import Button, { ButtonStyles } from '@/components/Button';
import { ButtonBaseProps } from '@/components/Button/Base';
import Theme, { ThemesType } from '@/config/theme';
import { Facebook } from '@/svg/Facebook';
import { LoginGoogle } from '@/firebase';

export interface ButtonFacebookClassProps {
    styleTemplateBtn?: ButtonStyles | ThemesType;
    icon?: any;
    position?: 'left' | 'right';
}

export interface ButtonFacebookBaseProps extends ButtonBaseProps {}

export interface ButtonFacebookProps
    extends ButtonFacebookClassProps,
        ButtonFacebookBaseProps {}

export const ButtonFacebookBase = ({
    styleTemplateBtn = Theme.styleTemplate ?? '_default',
    icon = <Facebook size={25} />,
    position = 'left',
    ...props
}: ButtonFacebookProps) => {
    return (
        <>
            <Button
                {...props}
                styleTemplate={styleTemplateBtn}
                iconPosition={position}
                icon={icon}
                onClick={LoginGoogle}
            />
        </>
    );
};
export default ButtonFacebookBase;
