# ButtonFacebook

## Dependencies

[Button](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Button)

```js
import { ButtonFacebook } from '@/components/ButtonFacebook';
```

## Import

```js
import {
    ButtonFacebook,
    ButtonFacebookStyles,
} from '@/components/ButtonFacebook';
```

## Props

```tsx
interface ButtonFacebookProps {}
```

## Use

```js
<ButtonFacebook>ButtonFacebook</ButtonFacebook>
```
