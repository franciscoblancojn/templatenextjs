import { Story, Meta } from "@storybook/react";

import { ButtonRsViewLinkProps, ButtonRsViewLink } from "./index";

export default {
    title: "ButtonRsViewLink/ButtonRsViewLink",
    component: ButtonRsViewLink,
} as Meta;

const ButtonRsViewLinkIndex: Story<ButtonRsViewLinkProps> = (args) => (
    <ButtonRsViewLink {...args}>Test Children</ButtonRsViewLink>
);

export const Index = ButtonRsViewLinkIndex.bind({});
Index.args = {};
