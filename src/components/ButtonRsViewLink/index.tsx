import { useMemo } from 'react';

import * as styles from '@/components/ButtonRsViewLink/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ButtonRsViewLinkBaseProps,
    ButtonRsViewLinkBase,
} from '@/components/ButtonRsViewLink/Base';

export const ButtonRsViewLinkStyle = { ...styles } as const;

export type ButtonRsViewLinkStyles = keyof typeof ButtonRsViewLinkStyle;

export interface ButtonRsViewLinkProps extends ButtonRsViewLinkBaseProps {
    styleTemplate?: ButtonRsViewLinkStyles | ThemesType;
}

export const ButtonRsViewLink = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ButtonRsViewLinkProps) => {
    const Style = useMemo(
        () =>
            ButtonRsViewLinkStyle[styleTemplate as ButtonRsViewLinkStyles] ??
            ButtonRsViewLinkStyle._default,
        [styleTemplate]
    );

    return <ButtonRsViewLinkBase {...Style} {...props} />;
};
export default ButtonRsViewLink;
