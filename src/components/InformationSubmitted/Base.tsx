import { useMemo } from 'react';
import ContentWidth from '@/components/ContentWidth';
import Text, { TextStyles } from '../Text';
import Theme, { ThemesType } from '@/config/theme';
import EarringWatch from '@/svg/EarringWatch';
import NoConfirm from '@/svg/NoConfirm';
import Link from '../Link';
import Verified from '@/svg/Verified';
import { useLang } from '@/lang/translate';
import { InformationSubmittedStatusType } from '@/interfaces/InformationSubmittedStatusType';

export interface InformationSubmittedClassProps {
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateText2?: TextStyles | ThemesType;
    styleTemplateNumber?: TextStyles | ThemesType;
    classNameContent?: string;
    classNameContent2?: string;
    classNameTextNumber?: string;
    classNameText?: string;
    classNameContentInformationSubmitted?: string;
    classNameContentText?: string;
    classNameContentNumber?: string;
    classNameStatus?: string;
    classNameTextStatus?: string;
}

export interface InformationSubmittedBaseProps {
    number?: number;
    text?: string;
    Next?: string;
    onClick?: (Next?: string) => void;
    status?: InformationSubmittedStatusType;
}

export interface InformationSubmittedProps
    extends InformationSubmittedClassProps,
        InformationSubmittedBaseProps {}

export const InformationSubmittedBase = ({
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateText2 = Theme.styleTemplate ?? '_default',
    styleTemplateNumber = Theme.styleTemplate ?? '_default',
    classNameContentInformationSubmitted = '',
    classNameText = '',
    classNameTextNumber = '',
    classNameContent = '',
    classNameStatus = '',
    status = 'Appeal',
    classNameTextStatus = '',
    number,
    text = '',
    Next,
    onClick,
}: InformationSubmittedProps) => {
    const _t = useLang();
    const statusCountent = useMemo(() => {
        const swStatus: {
            [id in InformationSubmittedStatusType]: any;
        } = {
            Verified: (
                <>
                    <div className={classNameStatus}>
                        <Verified size={15} />
                        <Text
                            className={classNameTextStatus}
                            styleTemplate={styleTemplateText2}
                        >
                            {_t('Verified')}
                        </Text>
                    </div>
                </>
            ),
            Appeal: (
                <>
                    <div className={classNameStatus}>
                        <NoConfirm size={15} />
                        <Link
                            href=""
                            styleTemplate={'tolinkme7'}
                            className={classNameTextStatus}
                            onClick={() => {
                                onClick?.(Next);
                            }}
                        >
                            {_t('Appeal')}
                        </Link>
                    </div>
                </>
            ),
            In_Review: (
                <>
                    <div className={classNameStatus}>
                        <EarringWatch size={20} />
                        <Text
                            className={classNameTextStatus}
                            styleTemplate={styleTemplateText2}
                        >
                            {_t('In Review')}
                        </Text>
                    </div>
                </>
            ),
            Without_Sending: (
                <>
                    <div className={classNameStatus}>
                        <EarringWatch size={20} />
                        <Text
                            className={classNameTextStatus}
                            styleTemplate={styleTemplateText2}
                        >
                            {_t('Without Sending')}
                        </Text>
                    </div>
                </>
            ),
        };
        return swStatus[status] ?? swStatus['Without_Sending'];
    }, [status]);

    return (
        <div className={`${classNameContentInformationSubmitted}`}>
            {status === 'Appeal' && (
                <Link
                    href=""
                    className="fullWidthLink"
                    onClick={() => {
                        onClick?.(Next);
                    }}
                >
                    <ContentWidth className={classNameContent}>
                        <Text
                            styleTemplate={styleTemplateNumber}
                            className={classNameTextNumber}
                        >
                            {number}
                        </Text>
                        <Text
                            styleTemplate={styleTemplateText}
                            className={classNameText}
                        >
                            {_t(text)}
                        </Text>
                    </ContentWidth>
                </Link>
            )}

            {status !== 'Appeal' && (
                <ContentWidth className={classNameContent}>
                    <Text
                        styleTemplate={styleTemplateNumber}
                        className={classNameTextNumber}
                    >
                        {number}
                    </Text>
                    <Text
                        styleTemplate={styleTemplateText}
                        className={classNameText}
                    >
                        {_t(text)}
                    </Text>
                </ContentWidth>
            )}
            {statusCountent}
        </div>
    );
};
export default InformationSubmittedBase;
