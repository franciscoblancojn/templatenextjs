# InformationSubmitted

## Dependencies

[ContentWidth](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ContentWidth)

```js
import { ContentWidth } from '@/components/ContentWidth';
```

## Import

```js
import {
    InformationSubmitted,
    InformationSubmittedStyles,
} from '@/components/InformationSubmitted';
```

## Props

```tsx
interface InformationSubmittedProps {}
```

## Use

```js
<InformationSubmitted>InformationSubmitted</InformationSubmitted>
```
