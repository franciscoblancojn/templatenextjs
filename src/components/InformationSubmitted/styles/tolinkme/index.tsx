import { InformationSubmittedClassProps } from '@/components/InformationSubmitted/Base';

export const tolinkme: InformationSubmittedClassProps = {
    classNameContentInformationSubmitted: `
        bg-white
        color-black
        box-shadow 
        box-shadow-x-0 
        box-shadow-y-3 
        box-shadow-b-6 
        box-shadow-s-0 
        box-shadow-black-16 
        m-0
        p-h-12
        p-v-16
        border-radius-15
        width-p-100
        flex
        flex-nowrap
        flex-align-center
        flex-justify-between
        hover-scale-11
        cursor-pointer
        `,

    classNameContent: `
    flex
    flex-align-center
    flex-6
    p-r-5
    flex-justify-between
`,
    classNameContent2: `
    flex
    flex-align-center
    flex-justify-right
    p-r-15
    flex-6
`,
    classNameText: `
        flex-10
     `,
    classNameTextNumber: `
        flex-2 
     `,
    styleTemplateText: 'tolinkme32',
    styleTemplateText2: 'tolinkme33',
    styleTemplateNumber: 'tolinkme31',
    classNameStatus: `
        flex
        flex-nowrap
    `,
    classNameTextStatus: `
        p-l-8
    `,
};
