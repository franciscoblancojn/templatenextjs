import { useMemo } from 'react';

import * as styles from '@/components/InformationSubmitted/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    InformationSubmittedBaseProps,
    InformationSubmittedBase,
} from '@/components/InformationSubmitted/Base';

export const InformationSubmittedStyle = { ...styles } as const;

export type InformationSubmittedStyles = keyof typeof InformationSubmittedStyle;

export interface InformationSubmittedProps
    extends InformationSubmittedBaseProps {
    styleTemplate?: InformationSubmittedStyles | ThemesType;
}

export const InformationSubmitted = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InformationSubmittedProps) => {
    const Style = useMemo(
        () =>
            InformationSubmittedStyle[
                styleTemplate as InformationSubmittedStyles
            ] ?? InformationSubmittedStyle._default,
        [styleTemplate]
    );

    return <InformationSubmittedBase {...Style} {...props} />;
};
export default InformationSubmitted;
