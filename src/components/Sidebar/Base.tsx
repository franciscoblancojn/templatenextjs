import Sticky from '@/components/Sticky';
import { Link, LinkStyles } from '@/components/Link';

export interface SidebarLink {
    href: string;
    text: string;
    styleTemplateLink?: LinkStyles;
}
export interface SidebarBaseProps {
    links: SidebarLink[];
}
export interface SidebarClassProps {
    classNameSticky: string;
    classNameUl: string;
    classNameLi: string;
    styleTemplateLink: LinkStyles;
}
export interface SidebarProps extends SidebarBaseProps, SidebarClassProps {}
export const SidebarBase = ({
    links = [],
    classNameSticky = '',
    classNameUl = '',
    classNameLi = '',
    styleTemplateLink = '_default',
}: SidebarProps) => {
    return (
        <Sticky className={classNameSticky}>
            <ul className={classNameUl}>
                {links.map((link: SidebarLink, i) => {
                    return (
                        <li className={classNameLi} key={i}>
                            <Link
                                href={link.href}
                                styleTemplate={
                                    link?.styleTemplateLink ?? styleTemplateLink
                                }
                            >
                                {link.text}
                            </Link>
                        </li>
                    );
                })}
            </ul>
        </Sticky>
    );
};
export default SidebarBase;
