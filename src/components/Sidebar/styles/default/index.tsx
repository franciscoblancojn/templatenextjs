import { SidebarClassProps } from '@/components/Sidebar/Base';

export const _default: SidebarClassProps = {
    classNameSticky: `p-v-10`,
    classNameUl: `m-0`,
    classNameLi: `m-b-10`,
    styleTemplateLink: `_default`,
};
