import { SidebarClassProps } from '@/components/Sidebar/Base';

export const sidebar2: SidebarClassProps = {
    classNameSticky: `p-v-10`,
    classNameUl: `m-0`,
    classNameLi: `m-b-10`,
    styleTemplateLink: `_default`,
};
