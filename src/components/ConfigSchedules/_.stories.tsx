import { Story, Meta } from "@storybook/react";

import { ConfigSchedulesProps, ConfigSchedules } from "./index";

export default {
    title: "ConfigSchedules/ConfigSchedules",
    component: ConfigSchedules,
} as Meta;

const ConfigSchedulesIndex: Story<ConfigSchedulesProps> = (args) => (
    <ConfigSchedules {...args}>Test Children</ConfigSchedules>
);

export const Index = ConfigSchedulesIndex.bind({});
Index.args = {};
