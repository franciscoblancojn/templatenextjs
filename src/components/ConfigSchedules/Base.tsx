import Theme, { ThemesType } from '@/config/theme';
import { CompanySchedulesHours } from '@/interfaces/Company';
import { useLang } from '@/lang/translate';
import { useMemo, useState } from 'react';
import { Button, ButtonStyles } from '../Button';
import ConfigSchedule from '../ConfigSchedule';

export interface ConfigSchedulesClassProps {
    classNameContent?: string;
    classNameContentShedules?: string;
    classNameContentShedule?: string;
    classNameAddShedules?: string;
    styleTemplateBtnAddShedules?: ButtonStyles | ThemesType;
}

export interface ConfigSchedulesBaseProps {
    defaultValue?: CompanySchedulesHours[];
    onChange?: (data: CompanySchedulesHours[]) => void;
}

export interface ConfigSchedulesProps
    extends ConfigSchedulesClassProps,
        ConfigSchedulesBaseProps {}

export const ConfigSchedulesBase = ({
    classNameContent = '',
    classNameContentShedules = '',
    classNameContentShedule = '',
    classNameAddShedules = '',
    styleTemplateBtnAddShedules = Theme.styleTemplate ?? '_default',

    defaultValue = [],
    onChange,
}: ConfigSchedulesProps) => {
    const _t = useLang();
    const [value, setValue] = useState<CompanySchedulesHours[]>(defaultValue);

    const onChangeSchedule = (i: number) => (data: CompanySchedulesHours) => {
        setValue((pre) => {
            pre[i] = data;
            onChange?.(pre);
            return pre;
        });
    };
    const addSchedule = () => {
        setValue((pre) => {
            onChange?.([...pre, {}]);
            return [...pre, {}];
        });
    };
    const deleteSchedule = (i: number) => () => {
        setValue((pre) => {
            const np = pre.map((e, j) =>
                j === i ? { ...e, delete: true } : e
            );
            onChange?.(np);
            return np;
        });
    };

    const CSchedule = useMemo(() => {
        return (
            <>
                {(value ?? [])?.map((e, i) => {
                    return (
                        <div
                            key={i + JSON.stringify(e)}
                            className={`classNameContentShedule ${classNameContentShedule}`}
                        >
                            <ConfigSchedule
                                key={i + JSON.stringify(e)}
                                defaultValue={e}
                                onChange={onChangeSchedule(i)}
                                onDelete={deleteSchedule(i)}
                            />
                        </div>
                    );
                })}
            </>
        );
    }, [value]);

    return (
        <>
            <div className={`classNameContent ${classNameContent}`}>
                <div className={`classNameAddShedules ${classNameAddShedules}`}>
                    <Button
                        styleTemplate={styleTemplateBtnAddShedules}
                        onClick={addSchedule}
                        isBtn={false}
                    >
                        {_t('Add')}
                    </Button>
                </div>
                <div
                    className={`classNameContentShedules ${classNameContentShedules}`}
                >
                    {CSchedule}
                </div>
            </div>
        </>
    );
};
export default ConfigSchedulesBase;
