import { useMemo } from 'react';

import * as styles from '@/components/ConfigSchedules/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigSchedulesBaseProps,
    ConfigSchedulesBase,
} from '@/components/ConfigSchedules/Base';

export const ConfigSchedulesStyle = { ...styles } as const;

export type ConfigSchedulesStyles = keyof typeof ConfigSchedulesStyle;

export interface ConfigSchedulesProps extends ConfigSchedulesBaseProps {
    styleTemplate?: ConfigSchedulesStyles | ThemesType;
}

export const ConfigSchedules = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigSchedulesProps) => {
    const Style = useMemo(
        () =>
            ConfigSchedulesStyle[styleTemplate as ConfigSchedulesStyles] ??
            ConfigSchedulesStyle._default,
        [styleTemplate]
    );

    return <ConfigSchedulesBase {...Style} {...props} />;
};
export default ConfigSchedules;
