import { ConfigSchedulesClassProps } from '@/components/ConfigSchedules/Base';

export const mooveri: ConfigSchedulesClassProps = {
    classNameContentShedules: `
        
    `,
    classNameContentShedule: `
        
    `,
};

export const mooveriBackoffice: ConfigSchedulesClassProps = {
    classNameContent: `
    `,
    classNameContentShedules: `
        flex
        row-gap-15
        p-b-15
    `,
    classNameContentShedule: `
        
    `,
    classNameAddShedules: `
        m-t-auto-
        p-t-15
        flex
        flex-justify-right
        width-p-100
    `,
    styleTemplateBtnAddShedules: 'mooveriSave',
};
