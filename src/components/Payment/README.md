# Payment

## Dependencies

[Payment](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Payment)

```js
import { Payment } from '@/components/Payment';
```

## Import

```js
import { Payment, PaymentStyles } from '@/components/Payment';
```

## Props

```tsx
interface PaymentProps {}
```

## Use

```js
<Payment>Payment</Payment>
```
