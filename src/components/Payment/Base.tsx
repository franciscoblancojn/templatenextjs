import Visa from '@/svg/visa';
import MasterCard from '@/svg/masterCard';
import DinersClub from '@/svg/dinersClub';
import Discover from '@/svg/discover';
import Jcb from '@/svg/jcb';
export interface PaymentClassProps {
    classNameJcb?: string;
    classNameVisa?: string;
    classNameDiscover?: string;
    classNameMasterCard?: string;
    classNameDinersClub?: string;
    classNameContentPayment?: string;
}

export interface PaymentBaseProps {}

export interface PaymentProps extends PaymentClassProps, PaymentBaseProps {}

export const PaymentBase = ({
    classNameJcb = '',
    classNameVisa = '',
    classNameDiscover = '',
    classNameMasterCard = '',
    classNameDinersClub = '',
    classNameContentPayment = '',
}: PaymentProps) => {
    return (
        <>
            <div className={classNameContentPayment}>
                <div className={classNameVisa}>
                    <Visa size={30} />
                </div>
                <div className={classNameMasterCard}>
                    <MasterCard size={40} />
                </div>
                <div className={classNameDinersClub}>
                    <DinersClub size={30} />
                </div>
                <div className={classNameDiscover}>
                    <Discover size={80} />
                </div>
                <div className={classNameJcb}>
                    <Jcb size={40} />
                </div>
            </div>
        </>
    );
};
export default PaymentBase;
