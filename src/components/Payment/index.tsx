import { useMemo } from 'react';

import * as styles from '@/components/Payment/styles';

import { Theme, ThemesType } from '@/config/theme';

import { PaymentBaseProps, PaymentBase } from '@/components/Payment/Base';

export const PaymentStyle = { ...styles } as const;

export type PaymentStyles = keyof typeof PaymentStyle;

export interface PaymentProps extends PaymentBaseProps {
    styleTemplate?: PaymentStyles | ThemesType;
}

export const Payment = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PaymentProps) => {
    const Style = useMemo(
        () =>
            PaymentStyle[styleTemplate as PaymentStyles] ??
            PaymentStyle._default,
        [styleTemplate]
    );

    return <PaymentBase {...Style} {...props} />;
};
export default Payment;
