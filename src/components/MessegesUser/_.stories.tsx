import { Story, Meta } from "@storybook/react";

import { MessegesUserProps, MessegesUser } from "./index";
import { MessegesUserChat } from "./Template/MessegesUserChat";

export default {
    title: "MessegesUser/MessegesUser",
    component: MessegesUser,
} as Meta;

const MessegesUserIndex: Story<MessegesUserProps> = (args) => (
    <MessegesUser {...args}>Test Children</MessegesUser>
);

export const Index = MessegesUserIndex.bind({});
Index.args = {};


const UserMessegesIndex: Story<MessegesUserProps> = (args) => (
    <MessegesUserChat {...args}>Test Children</MessegesUserChat>
);

export const Messeges_User = UserMessegesIndex.bind({});
Messeges_User.args = {
};