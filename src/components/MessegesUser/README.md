# MessegesUser

## Dependencies

[Text](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Text)

```js
import { Text } from '@/components/Text';
```

## Import

```js
import { MessegesUser, MessegesUserStyles } from '@/components/MessegesUser';
```

## Props

```tsx
interface MessegesUserProps {
    user?: string;
    msj?: string;
    date?: Date;
    bookedText?: string;
    img?: string;
    DateOfLastMsj?: Date;
}
```

## Use

```js
<MessegesUser>MessegesUser</MessegesUser>
```
