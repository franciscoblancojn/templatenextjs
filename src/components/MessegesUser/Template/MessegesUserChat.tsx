import { MessegesUser, MessegesUserProps } from '@/components/MessegesUser';

export const MessegesUserChat = ({ ...props }: MessegesUserProps) => {
    return (
        <MessegesUser
            {...props}
            user="Test"
            img="/image/mooveri/mooveri.png"
            date={new Date()}
            msj="Hello !!!"
        />
    );
};
export default MessegesUser;
