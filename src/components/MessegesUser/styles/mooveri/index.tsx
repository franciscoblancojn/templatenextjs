import { MessegesUserClassProps } from '@/components/MessegesUser/Base';

export const mooveri: MessegesUserClassProps = {
    styleNormal: {
        classNameContent: `
            flex 
            flex-align-center 
            
            p-16
        `,
        classNameImg: `
            width-50
            height-50
            border-radius-100
        `,
        styleTemplateTextUser: 'mooveri14',
        classNameContentUserText: `
            m-r-auto
        `,
        classNameContentImg: `
            m-r-20
        `,
        styleTemplateTextDate: 'mooveri16',
        styleTemplateTextMsj: 'mooveri17',
        classNameContentUserDate: `
            flex
            flex-align-center
        `,
        classNameContentUser: `
            m-r-5
        `,
    },
    styleMe: {
        classNameContent: `
            flex 
            flex-align-center 
            flex-row-reverse

            p-16
            bg-whiteTwo
        `,
        classNameImg: `
            width-50
            height-50
            border-radius-100
        `,
        styleTemplateTextUser: 'mooveri14',
        classNameContentUserText: `
            m-l-auto
            text-right
        `,
        classNameContentImg: `
            m-l-20
        `,
        styleTemplateTextDate: 'mooveri16',
        styleTemplateTextMsj: 'mooveri17',
        classNameContentUserDate: `
            flex
            flex-align-center
            flex-row-reverse
        `,
        classNameContentUser: `
            m-l-5
        `,
    },
};
