import { StatusClassProps } from '@/components/Status/Base';
import NoConfirm from '@/svg/NoConfirm';

export const mooveri: StatusClassProps = {
    classNameContent: `
        font-nunito
        font-8
        font-w-300
        flex
        flex-align-center
        flex-gap-column-5
    `,
    classNameStatus: {
        normal: `
            color-jungle-green
        `,
        warning: `
            color-orange
        `,
        error: `
            color-red
        `,
        ok: `
            color-green
        `,
    },
    iconStatus: {
        normal: '',
        warning: '',
        error: <NoConfirm size={8} />,
        ok: '',
    },
};
