import { ButtonGoogleClassProps } from '@/components/ButtonGoogle/Base';
import { Google } from '@/svg/Google';

export const mooveri: ButtonGoogleClassProps = {
    icon: <Google size={25} />,
    styleTemplateBtn: 'mooveriGoogle',
};
