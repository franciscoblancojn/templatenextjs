# GoBack

## Import

```js
import { GoBack } from '@/components/GoBack';
```

## Props

```ts
interface GoBackProps {
    href?: string;
    className?: string;
    onClick?: () => void;
    text?: string;
}
```

## Use

```js
<GoBack />
```
