import { useMemo } from 'react';

import * as styles from '@/components/GoBack/styles';

import { Theme, ThemesType } from '@/config/theme';

import { GoBackBaseProps, GoBackBase } from '@/components/GoBack/Base';

export const GoBackStyle = { ...styles } as const;

export type GoBackStyles = keyof typeof GoBackStyle;

export interface GoBackProps extends GoBackBaseProps {
    styleTemplate?: GoBackStyles | ThemesType;
}

export const GoBack = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: GoBackProps) => {
    const Style = useMemo(
        () =>
            GoBackStyle[styleTemplate as GoBackStyles] ?? GoBackStyle._default,
        [styleTemplate]
    );

    return <GoBackBase {...Style} {...props} />;
};
export default GoBack;
