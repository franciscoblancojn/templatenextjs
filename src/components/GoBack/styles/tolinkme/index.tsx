export const tolinkme = {
    classNameA: `
        font-16 font-nunito
        color-white color-greenblue-hover
        text-decoration-underline
    `,
    classNameArrow: `
        color-white
        m-r-10
    `,
    sizeArrow: 25,
};
export const tolinkme2 = {
    classNameA: `
        font-16 font-nunito
        color-greenblue 
        text-decoration-underline
    `,
    classNameArrow: `
        color-greenblue
        m-r-10
    `,
    sizeArrow: 25,
};
