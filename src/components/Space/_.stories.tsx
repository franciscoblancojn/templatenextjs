import { Story, Meta } from "@storybook/react";

import { SpaceProps, Space } from "./index";

export default {
    title: "Space/Space",
    component: Space,
} as Meta;

const Template: Story<SpaceProps> = (args) => (
    <Space {...args}>Test Children</Space>
);

export const Index = Template.bind({});
Index.args = {};
