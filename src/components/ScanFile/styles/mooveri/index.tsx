import { ScanFileClassProps } from '@/components/ScanFile/Base';

export const mooveri: ScanFileClassProps = {
    styleTemplateBtn: 'mooveriSearchScan',
    styleTemplateListItemScan: 'mooveri',
};
