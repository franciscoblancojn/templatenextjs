import { useMemo } from 'react';

import * as styles from '@/components/ScanFile/styles';

import { Theme, ThemesType } from '@/config/theme';

import { ScanFileBaseProps, ScanFileBase } from '@/components/ScanFile/Base';

export const ScanFileStyle = { ...styles } as const;

export type ScanFileStyles = keyof typeof ScanFileStyle;

export interface ScanFileProps extends ScanFileBaseProps {
    styleTemplate?: ScanFileStyles | ThemesType;
}

export const ScanFile = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ScanFileProps) => {
    const Style = useMemo(
        () =>
            ScanFileStyle[styleTemplate as ScanFileStyles] ??
            ScanFileStyle._default,
        [styleTemplate]
    );

    return <ScanFileBase {...Style} {...props} />;
};
export default ScanFile;
