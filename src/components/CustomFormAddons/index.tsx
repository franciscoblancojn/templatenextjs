import { useMemo } from 'react';

import * as styles from '@/components/CustomFormAddons/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    CustomFormAddonsBaseProps,
    CustomFormAddonsBase,
} from '@/components/CustomFormAddons/Base';

export const CustomFormAddonsStyle = { ...styles } as const;

export type CustomFormAddonsStyles = keyof typeof CustomFormAddonsStyle;

export interface CustomFormAddonsProps extends CustomFormAddonsBaseProps {
    styleTemplate?: CustomFormAddonsStyles | ThemesType;
}

export const CustomFormAddons = ({
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    ...props
}: CustomFormAddonsProps) => {
    const Style = useMemo(
        () =>
            CustomFormAddonsStyle[
                styleTemplateText as CustomFormAddonsStyles
            ] ?? CustomFormAddonsStyle._default,
        [styleTemplateText]
    );

    return <CustomFormAddonsBase {...Style} {...props} />;
};
export default CustomFormAddons;
