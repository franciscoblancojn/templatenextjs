import { Story, Meta } from "@storybook/react";

import { CustomFormAddonsProps, CustomFormAddons } from "./index";

export default {
    title: "CustomFormAddons/CustomFormAddons",
    component: CustomFormAddons,
} as Meta;

const CustomFormAddonsIndex: Story<CustomFormAddonsProps> = (args) => (
    <CustomFormAddons {...args}>Test Children</CustomFormAddons>
);

export const Index = CustomFormAddonsIndex.bind({});
Index.args = {};
