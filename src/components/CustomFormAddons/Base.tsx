import Theme, { ThemesType } from '@/config/theme';
import { Addons } from '@/interfaces/Addons';
import { useLang } from '@/lang/translate';
import Move from '@/svg/move';
import Trash from '@/svg/trash';
import { useData } from 'fenextjs-hook/cjs/useData';
import React, { useMemo } from 'react';
import PopupAddons from '../Input/PopupAddons';
import { InputTextStyles } from '../Input/Text';
import Text, { TextStyles } from '../Text';

export interface FormConfigDataProps {
    delete?: boolean;
}

export interface CustomFormAddonsBaseProps {
    styleTemplateSelectRS?: InputTextStyles | ThemesType;
    delete?: boolean;
    onDelete?: (addon?: Addons) => void;
    addons?: Addons;
    useAddons?: boolean;
    move?: boolean;
    classNameContent?: string;
    //styleContent?: CSS.Properties;
    classNameContentTitleRs?: string;
    classNameImgSelected?: string;
    styleTemplateText?: TextStyles | ThemesType;
    classNameContentSelectRS?: string;
    classNameContentUrl?: string;
    classNameMove?: string;
    sizeMove?: number;
    classNameActive?: string;
    sizeDelete?: number;
    onChange?: (data: Addons) => void;
    classNameDelete?: string;
}

export const CustomFormAddonsBase: React.FC<CustomFormAddonsBaseProps> = ({
    styleTemplateSelectRS = Theme?.styleTemplate ?? '_default',
    useAddons = false,
    addons,
    classNameContent = '',
    move = true,
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    classNameContentSelectRS = '',
    classNameMove = '',
    sizeMove = 20,
    classNameActive = '',
    delete: isDelete = false,
    sizeDelete = 20,
    onDelete = () => {},
    classNameDelete = '',
    onChange,
}) => {
    const _t = useLang();
    const { data, setData } = useData<Addons>(addons ?? {}, {
        onChangeDataAfter: (d) => {
            if (onChange) {
                onChange(d);
            }
        },
    });

    const ComponentSelectCustomAddons = useMemo(() => {
        return (
            <>
                <div className={styleTemplateSelectRS}>
                    <PopupAddons defaultValue={addons} onChange={setData} />
                </div>
            </>
        );
    }, [styleTemplateSelectRS, data, useAddons]);

    return (
        <>
            {isDelete ? (
                <></>
            ) : (
                <>
                    <div className={classNameContent}>
                        {move && (
                            <div className={classNameMove}>
                                <Move size={sizeMove} />
                            </div>
                        )}
                        <div className={classNameActive}>
                            <Text styleTemplate={styleTemplateText}>
                                {addons?.fromActive
                                    ? _t('Form')
                                    : _t('Choose an addon')}
                            </Text>
                        </div>
                        <div className={classNameContentSelectRS}>
                            {ComponentSelectCustomAddons}
                        </div>
                        <span
                            className={classNameDelete}
                            onClick={() => {
                                onDelete && onDelete(data);
                            }}
                        >
                            <Trash size={sizeDelete} />
                        </span>
                    </div>
                </>
            )}
        </>
    );
};

export default CustomFormAddonsBase;
