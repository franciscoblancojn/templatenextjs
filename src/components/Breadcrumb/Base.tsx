import { Theme, ThemesType } from '@/config/theme';

import { Link, LinkProps, LinkStyles } from '@/components/Link';

export interface BreadcrumbClassProps {
    classNameContent?: string;
    classNameContentLink?: string;
    styleTemplateLink?: LinkStyles | ThemesType;

    classNameSeparator?: string;
}

export interface BreadcrumbBaseProps {
    links?: LinkProps[];
}

export interface BreadcrumbProps
    extends BreadcrumbClassProps,
        BreadcrumbBaseProps {}

export const BreadcrumbBase = ({
    classNameContent = '',
    classNameContentLink = '',
    styleTemplateLink = Theme?.styleTemplate ?? '_default',

    classNameSeparator = '',

    links = [],
}: BreadcrumbProps) => {
    return (
        <div className={classNameContent}>
            {links.map((link, i) => (
                <div key={i} className={classNameContentLink}>
                    <Link {...link} styleTemplate={styleTemplateLink} />
                    <span className={classNameSeparator}>
                        {i < links.length - 1 && '/'}
                    </span>
                </div>
            ))}
        </div>
    );
};
export default BreadcrumbBase;
