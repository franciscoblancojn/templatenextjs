import { Story, Meta } from "@storybook/react";

import { BreadcrumbProps, Breadcrumb } from "./index";

export default {
    title: "Breadcrumb/Breadcrumb",
    component: Breadcrumb,
} as Meta;

const BreadcrumbIndex: Story<BreadcrumbProps> = (args) => (
    <Breadcrumb {...args}>Test Children</Breadcrumb>
);

export const Index = BreadcrumbIndex.bind({});
Index.args = {
    links:[
        {
            href:"#",
            children:"Home"
        },
        {
            href:"#",
            children:"Products"
        },
        {
            href:"#",
            children:"Card"
        },
    ]
};
