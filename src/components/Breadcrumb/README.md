# Breadcrumb

## Dependencies

[LinkProps](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Link/index.tsx)

```js
import { LinkProps } from '@/components/Link';
```

## Import

```js
import { Breadcrumb, BreadcrumbStyles } from '@/components/Breadcrumb';
```

## Props

```tsx
interface BreadcrumbProps {
    links?: LinkProps[];
    styleTemplate?: BreadcrumbStyles;
}
```

## Use

```js
<Breadcrumb
    links={[
        {
            href: '#',
            children: 'Home',
        },
        {
            href: '#',
            children: 'Products',
        },
        {
            href: '#',
            children: 'Card',
        },
    ]}
/>
```
