import { BreadcrumbClassProps } from '@/components/Breadcrumb/Base';

export const tolinkme: BreadcrumbClassProps = {
    classNameContent: `
        flex
        flex-gap-row-5
        flex-align-center
    `,
    classNameContentLink: `
        
    `,
    styleTemplateLink: 'tolinkme',
    classNameSeparator: `
      font-nunito
      color-white
      m-h-10
    `,
};
