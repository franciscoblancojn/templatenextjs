import { ChatUser, ChatUserProps } from '@/components/ChatUser';

export const ChatUserMesseges = ({ ...props }: ChatUserProps) => {
    return (
        <ChatUser
            {...props}
            user="Mooveri"
            img="/image/mooveri/mooveri.png"
            msj="Hello!"
            DateOfLastMsj={new Date()}
        />
    );
};
export default ChatUserMesseges;
