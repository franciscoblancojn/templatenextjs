import { useMemo } from 'react';

import * as styles from '@/components/ChatUser/styles';

import { Theme, ThemesType } from '@/config/theme';

import { ChatUserBaseProps, ChatUserBase } from '@/components/ChatUser/Base';

export const ChatUserStyle = { ...styles } as const;

export type ChatUserStyles = keyof typeof ChatUserStyle;

export interface ChatUserProps extends ChatUserBaseProps {
    styleTemplate?: ChatUserStyles | ThemesType;
}

export const ChatUser = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ChatUserProps) => {
    const Style = useMemo(
        () =>
            ChatUserStyle[styleTemplate as ChatUserStyles] ??
            ChatUserStyle._default,
        [styleTemplate]
    );

    return <ChatUserBase {...Style} {...props} />;
};
export default ChatUser;
