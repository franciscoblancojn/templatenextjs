import { Story, Meta } from "@storybook/react";

import { ChatUserProps, ChatUser } from "./index";
import { ChatUserMesseges } from "./Template/ChatUserMesseges";

export default {
    title: "ChatUser/ChatUser",
    component: ChatUser,
} as Meta;

const ChatUserIndex: Story<ChatUserProps> = (args) => (
    <ChatUser {...args}>Test Children</ChatUser>
);

export const Index = ChatUserIndex.bind({});
Index.args = {};


const ChatUserMessegesIndex: Story<ChatUserProps> = (args) => (
    <ChatUserMesseges {...args}>Test Children</ChatUserMesseges>
);

export const Manage = ChatUserMessegesIndex.bind({});
Manage.args = {
};