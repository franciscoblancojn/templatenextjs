# ChatUser

## Dependencies

[SvgDate](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/svg/date)
[Text](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Text)

```js
import { Text } from '@/components/Text';
import { Date as SvgDate } from '@/svg/date';
```

## Import

```js
import { ChatUser, ChatUserStyles } from '@/components/ChatUser';
```

## Props

```tsx
interface ChatUserProps {
    user?: string;
    msj?: string;
    date?: Date;
    bookedText?: string;
    img?: string;
    DateOfLastMsj?: Date;
}
```

## Use

```js
<ChatUser>ChatUser</ChatUser>
```
