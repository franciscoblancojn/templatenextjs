import { Story, Meta } from "@storybook/react";

import { SelectLangProps, SelectLang } from "./index";

export default {
    title: "SelectLang/SelectLang",
    component: SelectLang,
} as Meta;

const SelectLangIndex: Story<SelectLangProps> = (args) => (
    <SelectLang {...args}>Test Children</SelectLang>
);

export const Index = SelectLangIndex.bind({});
Index.args = {};
