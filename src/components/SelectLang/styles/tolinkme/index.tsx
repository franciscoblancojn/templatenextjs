import { SelectLangClassProps } from '@/components/SelectLang/Base';

export const tolinkme: SelectLangClassProps = {
    styleTemplateSelect: 'tolinkmeLang',
    classNameImg: `
        width-20
        bg-white-hover
        border-radius-40
    `,
    classNameCurrentLang: `
        pos-a
        inset-0
        m-auto
        flex
        flex-align-center
        flex-justify-center
        z-index--1 
        cursor-pointer
    `,
};

export const tolinkme2: SelectLangClassProps = {
    styleTemplateSelect: 'tolinkmeLang2',
    classNameImg: `
    
        width-20
    `,
    classNameCurrentLang: `
        pos-a
        inset-0
        m-auto
        flex
        flex-align-center
        flex-justify-start
        z-index--1
        cursor-pointer
        lang-idioma
    `,
};
