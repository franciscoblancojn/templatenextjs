import { useStateLang, langsKeys, langsType, useLang } from '@/lang/translate';

import { InputTextStyles } from '@/components/Input/Text';
import { InputSelect } from '@/components/Input/Select';
import Image from '../Image';
import Theme, { ThemesType } from '@/config/theme';

export interface SelectLangClassProps {
    styleTemplateSelect?: InputTextStyles | ThemesType;
    classNameImg?: string;
    classNameCurrentLang?: string;
}

export interface SelectLangBaseProps {}

export interface SelectLangProps
    extends SelectLangClassProps,
        SelectLangBaseProps {}

export const SelectLangBase = ({
    styleTemplateSelect = Theme.styleTemplate ?? '_default',
    classNameImg = '',
    classNameCurrentLang = '',
}: SelectLangProps) => {
    const { lang, setLang } = useStateLang();
    const _t = useLang();
    return (
        <>
            <InputSelect
                extraInContentInput={
                    <div className={classNameCurrentLang}>
                        <Image
                            src={`/langs/${lang}.svg`}
                            className={classNameImg}
                        />
                    </div>
                }
                styleTemplate={styleTemplateSelect}
                defaultValue={{
                    text: _t(`lang-${lang}`),
                    value: lang,
                }}
                options={langsKeys.map((l) => ({
                    text: _t(`lang-${l}`),
                    value: l,
                    html: (
                        <Image
                            src={`/langs/${l}.svg`}
                            className={classNameImg}
                        />
                    ),
                }))}
                onChange={(o) => {
                    if (o) {
                        setLang(o?.value as langsType);
                    }
                }}
            />
        </>
    );
};
export default SelectLangBase;
