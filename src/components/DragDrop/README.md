# DragDrop

## Import

```js
import { DragDrop, DragDropStyles } from '@/components/DragDrop';
```

## Props

```ts
interface DragDropItemProps<T> {
    id: number;
    data: T | any;
}

interface DragDropProps<T> {
    styleTemplate?: DragDropStyles;
    id: string;
    component: any;
    items: DragDropItemProps<T>[];

    onChangeState?: (data: T[]) => void;

    classNameContent?: string;
    classNameItem?: string;
    classNameHandle?: string;
}
```

## Use

```js
<DragDrop<{
    content:any
}>
    id="content"
    component={({content}:{content:any})=>(
        <div>
            {content}
        </div>
    )}
    items={[
        {
            id:1,
            data:{
                content:"test 1"
            }
        },
        {
            id:2,
            data:{
                content:"test 2"
            }
        },
    ]}
/>
```
