import { useMemo } from 'react';

import * as styles from '@/components/DragDrop/styles';

import { Theme, ThemesType } from '@/config/theme';

import { DragDropBaseProps, DragDropBase } from '@/components/DragDrop/Base';

export const DragDropStyle = { ...styles } as const;

export type DragDropStyles = keyof typeof DragDropStyle;

export interface DragDropProps<T> extends DragDropBaseProps<T> {
    styleTemplate?: DragDropStyles | ThemesType;
}

export const DragDrop = <T,>({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: DragDropProps<T>) => {
    const Style = useMemo(
        () =>
            DragDropStyle[styleTemplate as DragDropStyles] ??
            DragDropStyle._default,
        [styleTemplate]
    );

    return <DragDropBase {...Style} {...props} />;
};
export default DragDrop;
