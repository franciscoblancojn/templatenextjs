import { Story, Meta } from "@storybook/react";

import { ShareProfileProps, ShareProfile } from "./index";
import { Share } from "./Template/ShareProfile";

export default {
    title: "ShareProfile/ShareProfile",
    component: ShareProfile,
} as Meta;

const ShareProfileIndex: Story<ShareProfileProps> = (args) => (
    <ShareProfile {...args}>Test Children</ShareProfile>
);

export const Index = ShareProfileIndex.bind({});
Index.args = {
    name:"username"
};


const ShareIndex: Story<ShareProfileProps> = (args) => (
    <Share {...args}>Test Children</Share>
);

export const Share_ = ShareIndex.bind({});
Share_.args = {
};