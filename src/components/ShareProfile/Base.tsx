import { TextStyles, Text } from '@/components/Text';
import { LinkStyles, Link } from '@/components/Link';
import { Popup, PopupStyles } from '@/components/Popup';
import { RSShare, RSShareStyles } from '@/components/RS/Share';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

export interface ShareProfileClassProps {
    classNameContent?: string;
    classNameContentTextLink?: string;
    classNameShare?: string;
    classNameContentIcon?: string;
    styleTemplateText?: TextStyles | ThemesType;
    classNameText?: string;
    classNameLink?: string;
    styleTemplateLink?: LinkStyles | ThemesType;
    classNameContentShareBtn?: string;
    classNameContentShare?: string;
    styleTemplateShare?: TextStyles | ThemesType;
    styleTemplateRsShare?: RSShareStyles | ThemesType;
    iconShare?: any;
    iconClose?: any;
    styleTemplatePopup?: PopupStyles | ThemesType;
}

export interface ShareProfileBaseProps {
    name: string;
}

export interface ShareProfileProps
    extends ShareProfileClassProps,
        ShareProfileBaseProps {}

export const ShareProfileBase = ({
    classNameContent = '',
    classNameContentTextLink = '',
    classNameShare = '',
    classNameContentIcon = '',
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    classNameText = '',
    classNameLink = '',
    styleTemplateLink = Theme?.styleTemplate ?? '_default',
    classNameContentShareBtn = '',
    classNameContentShare = '',
    styleTemplateShare = Theme?.styleTemplate ?? '_default',
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',
    styleTemplateRsShare = Theme?.styleTemplate ?? '_default',
    iconShare = <></>,
    iconClose = <></>,

    name,
}: ShareProfileProps) => {
    const _t = useLang();

    const url = window.location.origin + '/' + name;

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentTextLink}>
                    <Text
                        styleTemplate={styleTemplateText}
                        className={classNameText}
                    >
                        {_t('My Profile')}
                        {':'}
                        <span className="p-l-0 p-md-l-5">
                            <Link
                                styleTemplate={styleTemplateLink}
                                href={`/${name}`}
                                className={classNameLink}
                            >
                                {url}
                            </Link>
                        </span>
                    </Text>
                </div>
                <Popup
                    btn={
                        <div className={classNameContentShare}>
                            <Text
                                className={classNameShare}
                                styleTemplate={styleTemplateShare}
                            >
                                <span className={classNameContentIcon}>
                                    {iconShare}
                                </span>

                                {_t('Share')}
                            </Text>
                        </div>
                    }
                    btnClose={
                        <div className={classNameContentShare}>
                            <Text
                                className={classNameShare}
                                styleTemplate={styleTemplateShare}
                            >
                                <span className={classNameContentIcon}>
                                    {iconClose}
                                </span>
                                {_t('Close')}
                            </Text>
                        </div>
                    }
                    className={classNameContentShareBtn}
                    styleTemplate={styleTemplatePopup}
                >
                    <RSShare styleTemplate={styleTemplateRsShare} url={url} />
                </Popup>
            </div>
        </>
    );
};
export default ShareProfileBase;
