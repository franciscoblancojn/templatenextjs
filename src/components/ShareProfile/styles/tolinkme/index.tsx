import { ShareProfileClassProps } from '@/components/ShareProfile/Base';

import { Share } from '@/svg/share';
import { Close } from '@/svg/close';

export const tolinkme: ShareProfileClassProps = {
    classNameContent: `
        width-p-100
        flex
        flex-align-center
        flex-justify-between
        flex-nowrap
        bg-white
        p-h-20 p-sm-h-0
        p-v-16
    `,
    classNameContentTextLink: `
        width-p-max-50
        pos-r
    `,
    styleTemplateText: 'tolinkme10',
    classNameText: `
          flex
          
    `,
    classNameLink: `
        d-block
        text-overflow-ellipsis
        overflow-hidden
        text-white-space-nowrap
    `,
    styleTemplateLink: 'tolinkme4',
    classNameContentShareBtn: `
        width-p-50
        flex
        flex-align-center
        flex-justify-right
    `,
    classNameContentShare: `
        flex
        flex-align-center
        flex-gap-column-8
        flex-nowrap
        pointer
    `,
    classNameShare: `
        color-brightPink-hover
        flex 
        flex-align-center
        flex-gap-column-5
    `,
    styleTemplateShare: 'tolinkme10',
    styleTemplateRsShare: 'tolinkme',
    iconShare: <Share size={20} />,
    iconClose: <Close size={15} />,
};
