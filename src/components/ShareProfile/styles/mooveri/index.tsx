import { ShareProfileClassProps } from '@/components/ShareProfile/Base';

import ShareArrow from '@/svg/shareArrow';

export const mooveri: ShareProfileClassProps = {
    classNameContent: `
        width-p-100
        flex
        flex-align-center
        flex-justify-center
        flex-nowrap
        p-h-12 p-sm-h-0
        height-50
        
    `,
    classNameContentTextLink: `
        d-none
    `,
    styleTemplateText: 'tolinkme10',
    classNameText: `
          flex
          flex-align-center
    `,
    classNameLink: `
        d-block
        text-overflow-ellipsis
        overflow-hidden
        text-white-space-nowrap
    `,
    styleTemplateLink: 'tolinkme4',
    classNameContentShareBtn: `
        width-p-100
        flex
        flex-align-center
    `,
    classNameContentShare: `
        flex
        flex-justify-between
        flex-align-center
        flex-gap-column-8
        flex-nowrap
        pointer
        
    `,
    classNameShare: `
        flex 
        flex-align-center
        bg-gradient-greenishCyan-metallicBlue
        bg-graySix-hover
        border-radius-100
        height-50  
        p-h-13   
        color-warmGreyThree-hover
    `,
    classNameContentIcon: `
    m-r-5
    `,
    styleTemplatePopup: 'mooveri',
    styleTemplateShare: 'mooveri18',
    styleTemplateRsShare: 'mooveri',
    iconShare: <ShareArrow size={16} />,
    iconClose: <ShareArrow size={16} />,
};
