# InformationSent

## Dependencies

[InformationSent](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/InformationSent)

```js
import { InformationSent } from '@/components/InformationSent';
```

## Import

```js
import {
    InformationSent,
    InformationSentStyles,
} from '@/components/InformationSent';
```

## Props

```tsx
interface InformationSentProps {}
```

## Use

```js
<InformationSent>InformationSent</InformationSent>
```
