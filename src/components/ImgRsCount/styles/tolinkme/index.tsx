import { ImgRsCountClassProps } from '@/components/ImgRsCount/Base';

export const tolinkme: ImgRsCountClassProps = {
    classNameContent: `
        flex 
        flex-nowrap 
        flex-align-center 
        flex-gap-10 
        flex-justify-between
        m-b-20 
    `,
    classNameContentImg: `
        flex-4
    `,
    classNameImg: `
        object-fit-contain
        height-30
    `,
    classNameContentText: `
        flex flex-4
        flex-justify-left
    `,
    classNameText: `
        text-capitalize
        text-white-space-nowrap
    `,
    styleTemplateText: 'tolinkme16',
    classNameContentCount: `
        flex
        flex-4
        flex-justify-right
    `,
    classNameCount: `
        text-capitalize
        text-white-space-nowrap
        flex flex-4
        flex-justify-right
    `,
    styleTemplateCount: 'tolinkme20',
};
