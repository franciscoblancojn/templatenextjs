import { Story, Meta } from "@storybook/react";

import { ImgRsCountProps, ImgRsCount } from "./index";

export default {
    title: "ImgRsCount/ImgRsCount",
    component: ImgRsCount,
} as Meta;

const ImgRsCountIndex: Story<ImgRsCountProps> = (args) => (
    <ImgRsCount {...args}>Test Children</ImgRsCount>
);

export const Index = ImgRsCountIndex.bind({});
Index.args = {};
