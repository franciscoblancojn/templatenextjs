import Text, { TextStyles } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';
import Image from '@/components/Image';
import { TypeRS } from '@/data/components/RS';
import { numberCount } from '@/functions/numberCount';

export interface ImgRsCountClassProps {
    classNameContent?: string;
    classNameContentImg?: string;
    classNameImg?: string;
    classNameContentText?: string;
    classNameText?: string;
    styleTemplateText?: TextStyles | ThemesType;
    classNameContentCount?: string;
    classNameCount?: string;
    styleTemplateCount?: TextStyles | ThemesType;
}

export interface ImgRsCountBaseProps {
    id: TypeRS;
    count: number;
    customImg?: string;
    customTitle?: string;
}

export interface ImgRsCountProps
    extends ImgRsCountClassProps,
        ImgRsCountBaseProps {}

export const ImgRsCountBase = ({
    classNameContentText = '',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    classNameContent = '',
    classNameContentCount = '',
    classNameContentImg = '',
    classNameCount = '',
    classNameImg = '',
    classNameText = '',
    styleTemplateCount = Theme.styleTemplate ?? '_default',

    id,
    count,
    customImg,
    customTitle,
}: ImgRsCountProps) => {
    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentImg}>
                    {id == 'custom' ? (
                        <img src={customImg} className={classNameImg} />
                    ) : (
                        <Image
                            src={`/rs/${id?.toLocaleLowerCase()}.png`}
                            className={classNameImg}
                        />
                    )}
                </div>
                <div className={classNameContentText}>
                    <Text
                        className={classNameText}
                        styleTemplate={styleTemplateText}
                    >
                        {id == 'custom' ? customTitle : id}
                    </Text>
                </div>

                <div className={classNameContentCount}>
                    <Text
                        className={classNameCount}
                        styleTemplate={styleTemplateCount}
                    >
                        {numberCount(count)}
                    </Text>
                </div>
            </div>
        </>
    );
};
export default ImgRsCountBase;
