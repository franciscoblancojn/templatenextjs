import { Button, ButtonProps } from '@/components/Button';
import { Text, TextProps } from '@/components/Text';
import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';
import { Image, ImageProps } from '@/components/Image';
import { Link } from '@/components/Link';

import { useLang } from '@/lang/translate';
import url from '@/data/routes';

export interface C404ClassProps {
    ContentWidthProps?: ContentWidthProps;
    TitleProps?: TextProps;
    ContentWidthImgProps?: ContentWidthProps;
    ImageProps?: ImageProps;
    ContentWidthTextProps?: ContentWidthProps;
    TextProps?: TextProps;
    ContentWidthLinkProps?: ContentWidthProps;
    ButtonProps?: ButtonProps;
}

export interface C404BaseProps {
    title?: string;
    text?: string;
    btn?: string;
    link?: string;
}

export interface C404Props extends C404ClassProps, C404BaseProps {}

export const C404Base = ({
    ContentWidthProps = {},
    TitleProps = {},
    ContentWidthImgProps = {},
    ImageProps = {},
    ContentWidthTextProps = {},
    TextProps = {},
    ContentWidthLinkProps = {},
    ButtonProps = {},

    title = '404',
    text = "Sorry we couldn't find what you were looking for",
    btn = 'Back To Home',
    link = url.index,
}: C404Props) => {
    const _t = useLang();
    return (
        <>
            <ContentWidth {...ContentWidthProps}>
                <Text {...TitleProps}>{_t(title)}</Text>
                <ContentWidth {...ContentWidthImgProps}>
                    <Image {...ImageProps} />
                </ContentWidth>
                <ContentWidth {...ContentWidthTextProps}>
                    <Text {...TextProps}>{_t(text)}</Text>
                </ContentWidth>
                <ContentWidth {...ContentWidthLinkProps}>
                    <Link href={link} className="text-decoration-none-link">
                        <Button {...ButtonProps}>{_t(btn)}</Button>
                    </Link>
                </ContentWidth>
            </ContentWidth>
            <style jsx global>
                {`
                    .title {
                        font-size: 8rem;
                        line-height: 1;
                    }
                    .text-decoration-none-link {
                        text-decoration: none;
                    }
                `}
            </style>
        </>
    );
};
export default C404Base;
