import { C404ClassProps } from '@/components/404/Base';

export const tolinkme: C404ClassProps = {
    ContentWidthProps: {
        size: 500,
        className: `
            m-h-auto
        `,
    },
    TitleProps: {
        styleTemplate: 'tolinkme',
        className: `
            text-center
            color-white
            title
            font-w-900
            m-b-25
        `,
    },
    ContentWidthImgProps: {
        size: 100,
        className: `
            m-h-auto
            m-b-25
        `,
    },
    ImageProps: {
        src: 'face2.png',
        styleTemplate: 'tolinkme',
    },
    ContentWidthTextProps: {
        size: 200,
        className: `
            m-h-auto
            m-b-25
        `,
    },
    TextProps: {
        styleTemplate: 'tolinkme',
        className: `
            m-h-auto
            text-center
        `,
    },
    ContentWidthLinkProps: {
        size: 200,
        className: `
            m-h-auto
            m-b-25
        `,
    },
    ButtonProps: {
        styleTemplate: 'tolinkme',
    },
};
