import { C404ClassProps } from '@/components/404/Base';

export const mooveri: C404ClassProps = {
    ContentWidthProps: {
        size: 500,
        className: `
            m-h-auto
        `,
    },
    TitleProps: {
        styleTemplate: 'mooveriVoid',
        className: `
            text-center
            color-sea
            title
            font-w-900
            m-b-25
        `,
    },
    ContentWidthImgProps: {
        size: 100,
        className: `
            m-h-auto
            m-b-25
            d-none
        `,
    },
    ImageProps: {
        src: 'face2.png',
        styleTemplate: 'tolinkme',
    },
    ContentWidthTextProps: {
        size: 200,
        className: `
            m-h-auto
            m-b-25
        `,
    },
    TextProps: {
        styleTemplate: 'mooveriVoid',
        className: `
            m-h-auto
            text-center
        `,
    },
    ContentWidthLinkProps: {
        size: 200,
        className: `
            m-h-auto
            m-b-25
        `,
    },
    ButtonProps: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriBackoffice: C404ClassProps = {
    ...mooveri,
    TitleProps: {
        styleTemplate: 'mooveriVoid',
        className: `
            text-center
            color-white
            title
            font-w-900
            m-b-25
        `,
    },
    TextProps: {
        styleTemplate: 'mooveriVoid',
        className: `
            m-h-auto
            text-center
            color-white
        `,
    },
};
