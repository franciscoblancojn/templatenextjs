# C404

## Dependencies

[C404](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/C404)

```js
import { C404 } from '@/components/C404';
```

## Import

```js
import { C404, C404Styles } from '@/components/C404';
```

## Props

```tsx
interface C404Props {}
```

## Use

```js
<C404>C404</C404>
```
