import { BalanceClassProps } from '@/components/Balance/Base';

export const tolinkme: BalanceClassProps = {
    classNameContentBalance: `
        bg-white
        box-shadow 
        box-shadow-x-0 
        box-shadow-y-3 
        box-shadow-b-6 
        box-shadow-s-0 
        box-shadow-black16 
        m-0
        p-v-30
        p-h-20
        border-radius-20
        border-whiteSeven
        border-style-solid
        border-1
        width-p-100
        flex
        flex-justify-between
        flex-align-center
        flex-justify-left
    `,
    classNameContentSvg: `
        flex
        flex-align-center
        color-spanishGray
        flex-justify-left
    `,
    classNameContentMoney: `
        flex
        flex-justify-left
        color-greyishBrown
        font-25 font-nunito font-w-900
    `,
    classNameContentMoney2: `
        flex
        flex-align-start
        font-18 font-nunito font-w-900
        color-greyishBrown
    `,
    classNameContent: `
        border-r-1
        border-b-0
        border-t-0
        border-l-0
        width-p-50
        border-style-solid
        border-argent
        p-r-28
    `,
    classNameContent2: `
        width-p-50
        p-l-28
    `,
    classNameText: `
        flex-8
    `,
    classNameIcon: `
        flex-1
    `,
    styleTemplateText: 'tolinkme29',
    styleTemplateText2: 'tolinkme30',
};
