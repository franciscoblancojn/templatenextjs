import { PropsWithChildren } from 'react';
import ContentWidth from '@/components/ContentWidth';
import money from '@/functions/money';
import Exclamation from '@/svg/exclamation';
import Text, { TextStyles } from '../Text';
import Theme, { ThemesType } from '@/config/theme';

export interface BalanceClassProps {
    classNameIcon?: string;
    classNameText?: string;
    classNameContent?: string;
    classNameContent2?: string;
    classNameContentSvg?: string;
    classNameContentMoney?: string;
    classNameContentMoney2?: string;
    classNameContentBalance?: string;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateText2?: TextStyles | ThemesType;
}

export interface BalanceBaseProps extends PropsWithChildren {
    text?: string;
    text2?: string;
    price?: number;
    price2?: number;
    className?: string;
}

export interface BalanceProps extends BalanceClassProps, BalanceBaseProps {}

export const BalanceBase = ({
    text = '',
    text2 = '',
    price = 0.0,
    price2 = 0.0,
    classNameIcon = '',
    classNameText = '',
    classNameContent = '',
    classNameContent2 = '',
    classNameContentSvg = '',
    classNameContentMoney = '',
    classNameContentMoney2 = '',
    classNameContentBalance = '',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateText2 = Theme.styleTemplate ?? '_default',
}: BalanceProps) => {
    return (
        <>
            <ContentWidth className={`${classNameContentBalance}`}>
                <div className={classNameContent}>
                    <div className={classNameContentMoney}>{money(price)}</div>
                    <Text styleTemplate={styleTemplateText}>{text}</Text>
                </div>

                <div className={classNameContent2}>
                    <div className={classNameContentMoney2}>
                        {money(price2)}
                    </div>
                    <div className={classNameContentSvg}>
                        <div className={classNameIcon}>
                            <Exclamation size={17} />
                        </div>
                        <Text
                            className={classNameText}
                            styleTemplate={styleTemplateText2}
                        >
                            {text2}
                        </Text>
                    </div>
                </div>
            </ContentWidth>
        </>
    );
};
export default BalanceBase;
