import { InputDate, InputDateProps } from '@/components/Input/Date';
import Date from '@/svg/date';

export const InputDateMoveDay = ({ ...props }: InputDateProps) => {
    return (
        <InputDate
            {...props}
            placeholder="30/11/2022"
            label="Move Day"
            icon={<Date size={10} />}
        />
    );
};
export default InputDateMoveDay;
