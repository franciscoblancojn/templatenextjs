import { useData } from 'fenextjs-hook/cjs/useData';
import { InputColor, InputColorStyles } from '@/components/Input/Color';
import Theme, { ThemesType } from '@/config/theme';
import { InputRange, InputRangeStyles } from '@/components/Input/Range';
import { useEffect } from 'react';

export interface InputGradientClassProps {
    classNameContent?: string;
    styleTemplateInputColor?: InputColorStyles | ThemesType;
    styleTemplateInputRange?: InputRangeStyles | ThemesType;

    classNameContentColor1?: string;
    classNameContentDeg?: string;
    classNameContentColor2?: string;
}

export interface InputGradientDataProps {
    color1: string;
    color2: string;
    deg: number;
}

export interface InputGradientBaseProps {
    defaultValue?: InputGradientDataProps;
    onChange?: (data: InputGradientDataProps) => void;
}

export interface InputGradientProps
    extends InputGradientClassProps,
        InputGradientBaseProps {}

export const InputGradientBase = ({
    classNameContent = '',
    styleTemplateInputColor = Theme.styleTemplate ?? '_default',
    styleTemplateInputRange = Theme.styleTemplate ?? '_default',

    classNameContentColor1 = '',
    classNameContentDeg = '',
    classNameContentColor2 = '',

    defaultValue = {
        color1: '',
        color2: '',
        deg: 0,
    },
    ...props
}: InputGradientProps) => {
    const { data, onChangeData } =
        useData<InputGradientDataProps>(defaultValue);

    useEffect(() => {
        props?.onChange?.(data);
    }, [data]);

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentColor1}>
                    <InputColor
                        defaultValue={data.color1}
                        onChange={onChangeData('color1')}
                        styleTemplate={styleTemplateInputColor}
                    />
                </div>
                <div className={classNameContentDeg}>
                    <InputRange
                        defaultValue={data.deg}
                        min={0}
                        max={360}
                        onChange={onChangeData('deg')}
                        styleTemplate={styleTemplateInputRange}
                    />
                </div>
                <div className={classNameContentColor2}>
                    <InputColor
                        defaultValue={data.color2}
                        onChange={onChangeData('color2')}
                        styleTemplate={styleTemplateInputColor}
                    />
                </div>
            </div>
        </>
    );
};
export default InputGradientBase;
