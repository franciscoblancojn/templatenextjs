import { InputGradientClassProps } from '@/components/Input/Gradient/Base';

export const tolinkme: InputGradientClassProps = {
    classNameContent: `
        flex
        flex-nowrap
        flex-gap-15
        flex-gap-column-15
        flex-align-center
        width-p-100
    `,
    classNameContentColor1: `
        flex-3
        flex
        flex-justify-right
    `,
    classNameContentDeg: `
        flex-6
        flex
        flex-justify-center
    `,
    classNameContentColor2: `
        flex-3
        flex
        flex-justify-left
    `,
    styleTemplateInputColor: 'tolinkme2',
    styleTemplateInputRange: 'tolinkme',
};
