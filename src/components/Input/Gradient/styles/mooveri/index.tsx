import { InputGradientClassProps } from '@/components/Input/Gradient/Base';

export const mooveri: InputGradientClassProps = {
    classNameContent: `
        flex
        flex-nowrap
        flex-gap-column-15
    `,
};
