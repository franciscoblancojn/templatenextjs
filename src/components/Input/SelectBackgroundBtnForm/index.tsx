import { useMemo } from 'react';

import * as styles from '@/components/Input/SelectBackgroundBtnForm/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    SelectBackgroundBtnFormBaseProps,
    SelectBackgroundBtnFormBase,
} from '@/components/Input/SelectBackgroundBtnForm/Base';

export const SelectBackgroundBtnFormStyle = { ...styles } as const;

export type SelectBackgroundBtnFormStyles =
    keyof typeof SelectBackgroundBtnFormStyle;

export interface SelectBackgroundBtnFormProps
    extends SelectBackgroundBtnFormBaseProps {
    styleTemplate?: SelectBackgroundBtnFormStyles | ThemesType;
}

export const SelectBackgroundBtnForm = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: SelectBackgroundBtnFormProps) => {
    const Style = useMemo(
        () =>
            SelectBackgroundBtnFormStyle[
                styleTemplate as SelectBackgroundBtnFormStyles
            ] ?? SelectBackgroundBtnFormStyle._default,
        [styleTemplate]
    );

    return <SelectBackgroundBtnFormBase {...Style} {...props} />;
};
export default SelectBackgroundBtnForm;
