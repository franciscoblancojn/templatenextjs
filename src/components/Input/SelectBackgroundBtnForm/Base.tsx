import { useData } from 'fenextjs-hook/cjs/useData';
import { useMemo } from 'react';
import { InputColor, InputColorStyles } from '@/components/Input/Color';
import {
    InputGradient,
    InputGradientStyles,
} from '@/components/Input/Gradient';
import Theme, { ThemesType } from '@/config/theme';
import { useLang } from '@/lang/translate';
import { Text, TextStyles } from '@/components/Text';
import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';
import Color from '@/svg/Color';
import Brush from '@/svg/Brush';
import Img from '@/svg/Img';
import Video from '@/svg/Video';
import { InputUpload, InputUploadStyles } from '../Upload';
import {
    BackgroundDataObjectTypeProps,
    BackgroundType,
    BackgroundTypes,
} from '@/interfaces/Background';

export interface SelectBackgroundBtnFormClassProps {
    classNameContentTitle?: string;
    classNameTitle?: string;
    styleTemplateTitle?: TextStyles | ThemesType;

    classNameContentSelectType?: string;
    classNameContentSelectTypeItem?: string;
    classNameContentSelectTypeItemActive?: string;
    classNameContentSelectTypeItemInactive?: string;
    classNameContentSelectTypeItemContentValue?: string;
    classNameContentSelectTypeItemContentValueType?: {
        [id in BackgroundType]?: string;
    };

    classNameContentSelectTypeItemColor?: string;
    classNameContentSelectTypeItemGradient?: string;
    classNameContentSelectTypeItemImg?: string;
    classNameContentSelectTypeItemVideo?: string;

    placeholderContentWidthProps?: ContentWidthProps;
    sizeIconPlaceholder?: number;

    classNameContentInput?: string;
    styleTemplateTitleInput?: TextStyles | ThemesType;

    styleTemplateInputColor?: InputColorStyles | ThemesType;
    styleTemplateInputGradient?: InputGradientStyles | ThemesType;
    styleTemplateInputImg?: InputUploadStyles | ThemesType;
    styleTemplateInputVideo?: InputUploadStyles | ThemesType;
}

export interface SelectBackgroundBtnFormBaseProps {
    title?: any;
    defaultValue?: BackgroundDataObjectTypeProps;
    onChange?: (data: BackgroundDataObjectTypeProps) => void;
    useType?: {
        [id in BackgroundType]?: boolean;
    };
}

export interface SelectBackgroundBtnFormProps
    extends SelectBackgroundBtnFormClassProps,
        SelectBackgroundBtnFormBaseProps {}

export const SelectBackgroundBtnFormBase = ({
    classNameContentTitle = '',
    classNameTitle = '',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',

    classNameContentSelectType = '',
    classNameContentSelectTypeItem = '',
    classNameContentSelectTypeItemActive = '',
    classNameContentSelectTypeItemInactive = '',
    classNameContentSelectTypeItemContentValue = '',
    classNameContentSelectTypeItemContentValueType = {},

    classNameContentSelectTypeItemColor = '',
    classNameContentSelectTypeItemGradient = '',
    classNameContentSelectTypeItemImg = '',
    classNameContentSelectTypeItemVideo = '',

    classNameContentInput = '',
    styleTemplateTitleInput = Theme.styleTemplate ?? '_default',

    styleTemplateInputColor = Theme.styleTemplate ?? '_default',
    styleTemplateInputGradient = Theme.styleTemplate ?? '_default',
    styleTemplateInputImg = Theme.styleTemplate ?? '_default',
    styleTemplateInputVideo = Theme.styleTemplate ?? '_default',

    placeholderContentWidthProps = {},
    sizeIconPlaceholder = 20,

    title = 'Background',
    defaultValue = {
        type: 'color',
        opacity: 100,
        color: '#68e7e7',
        gradient: {
            color1: '#04506c',
            color2: '#9d016e',
            deg: 130,
        },
        img: {
            fileData: '',
            text: '',
        },
        video: {
            fileData: '',
            text: '',
        },
    },
    useType = {
        color: true,
        gradient: true,
        img: true,
        video: true,
    },

    ...props
}: SelectBackgroundBtnFormProps) => {
    const _t = useLang();

    const { data, onChangeData } = useData<BackgroundDataObjectTypeProps>(
        defaultValue,
        {
            onChangeDataAfter: props?.onChange,
        }
    );

    const ContentInput = useMemo(() => {
        if (data.type == 'color') {
            return (
                <>
                    <Text styleTemplate={styleTemplateTitleInput}>
                        {_t('Color')}
                    </Text>
                    <InputColor
                        defaultValue={data.color}
                        onChange={onChangeData('color')}
                        styleTemplate={styleTemplateInputColor}
                    />
                </>
            );
        }
        if (data.type == 'gradient') {
            return (
                <>
                    <Text styleTemplate={styleTemplateTitleInput}>
                        {_t('Gradient Color')}
                    </Text>
                    <InputGradient
                        defaultValue={data.gradient}
                        onChange={onChangeData('gradient')}
                        styleTemplate={styleTemplateInputGradient}
                    />
                </>
            );
        }
        if (data.type == 'img') {
            return (
                <>
                    <Text styleTemplate={styleTemplateTitleInput}>
                        {_t('Image')}
                    </Text>
                    <InputUpload
                        key={'image'}
                        accept={[
                            'png',
                            'PNG',
                            'jpg',
                            'jpeg',
                            'gif',
                            'webp',
                            'JPEG',
                            'JPG',
                            'WEBP',
                            'JPEG',
                            'GIF',
                        ]}
                        textUpload="Upload Image"
                        defaultValue={data.img}
                        onSubmit={onChangeData('img')}
                        styleTemplate={styleTemplateInputImg}
                    />
                </>
            );
        }
        if (data.type == 'video') {
            return (
                <>
                    <Text styleTemplate={styleTemplateTitleInput}>
                        {_t('Video')}
                    </Text>
                    <InputUpload
                        key={'video'}
                        accept={['mp4']}
                        textUpload="Upload Video"
                        defaultValue={data.video}
                        onSubmit={onChangeData('video')}
                        styleTemplate={styleTemplateInputVideo}
                    />
                </>
            );
        }
        return <></>;
    }, [data]);

    const ContentItem: {
        [id in BackgroundType]?: any;
    } = useMemo(
        () => ({
            color: (
                <>
                    <div
                        className={classNameContentSelectTypeItemColor}
                        style={{
                            background: data.color,
                            ...(data.type == 'color'
                                ? {
                                      opacity: (data?.opacity ?? 100) / 100,
                                  }
                                : {}),
                        }}
                    ></div>
                    <ContentWidth {...placeholderContentWidthProps}>
                        <Color size={sizeIconPlaceholder} />
                    </ContentWidth>
                </>
            ),
            gradient: (
                <>
                    <div
                        className={classNameContentSelectTypeItemGradient}
                        style={{
                            background: `linear-gradient(${data?.gradient?.deg}deg, ${data?.gradient?.color1}, ${data?.gradient?.color2})`,
                            ...(data.type == 'gradient'
                                ? {
                                      opacity: (data?.opacity ?? 100) / 100,
                                  }
                                : {}),
                        }}
                    ></div>
                    <ContentWidth {...placeholderContentWidthProps}>
                        <Brush size={sizeIconPlaceholder} />
                    </ContentWidth>
                </>
            ),
            img: (
                <>
                    {data.img?.fileData ? (
                        <>
                            <img
                                src={data.img?.fileData}
                                alt={data.img?.text}
                                className={classNameContentSelectTypeItemImg}
                                style={{
                                    ...(data.type == 'img'
                                        ? {
                                              opacity:
                                                  (data?.opacity ?? 100) / 100,
                                          }
                                        : {}),
                                }}
                            />
                        </>
                    ) : (
                        <></>
                    )}
                    <ContentWidth {...placeholderContentWidthProps}>
                        <Img size={sizeIconPlaceholder} />
                    </ContentWidth>
                </>
            ),
            video: (
                <>
                    {data.video?.fileData ? (
                        <>
                            <video
                                src={data.video?.fileData}
                                autoPlay={true}
                                controls={false}
                                loop={true}
                                muted={true}
                                className={classNameContentSelectTypeItemVideo}
                                style={{
                                    ...(data.type == 'video'
                                        ? {
                                              opacity:
                                                  (data?.opacity ?? 100) / 100,
                                          }
                                        : {}),
                                }}
                            />
                        </>
                    ) : (
                        <></>
                    )}
                    <ContentWidth {...placeholderContentWidthProps}>
                        <Video size={sizeIconPlaceholder} />
                    </ContentWidth>
                </>
            ),
        }),
        [data]
    );

    return (
        <>
            {title ? (
                <>
                    <div className={classNameContentTitle}>
                        <Text
                            styleTemplate={styleTemplateTitle}
                            className={classNameTitle}
                        >
                            {typeof title == 'string' ? '' : <>{title} </>}
                            {_t(
                                `${
                                    typeof title == 'string' ? `${title} ` : ''
                                }${data.type == 'img' ? 'image' : data.type}`
                            )}
                        </Text>
                    </div>
                </>
            ) : (
                <></>
            )}
            <div className={classNameContentSelectType}>
                {BackgroundTypes.filter((e) => useType[e]).map((v, i) => (
                    <div
                        key={i}
                        className={`${classNameContentSelectTypeItem}
                            ${
                                data.type == v
                                    ? classNameContentSelectTypeItemActive
                                    : classNameContentSelectTypeItemInactive
                            }
                        `}
                        onClick={() => {
                            onChangeData('type')(v);
                        }}
                    >
                        <div
                            className={`
                                    ${classNameContentSelectTypeItemContentValue}
                                    ${classNameContentSelectTypeItemContentValueType[v]}    
                                `}
                        >
                            {ContentItem[v]}
                        </div>
                    </div>
                ))}
            </div>
            <div className={classNameContentInput}>{ContentInput}</div>
        </>
    );
};
export default SelectBackgroundBtnFormBase;
