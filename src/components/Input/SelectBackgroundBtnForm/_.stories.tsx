import { Story, Meta } from "@storybook/react";

import { SelectBackgroundBtnFormProps, SelectBackgroundBtnForm } from "./index";

export default {
    title: "Input/SelectBackgroundBtnForm",
    component: SelectBackgroundBtnForm,
} as Meta;

const SelectBackgroundBtnFormIndex: Story<SelectBackgroundBtnFormProps> = (args) => (
    <SelectBackgroundBtnForm {...args}>Test Children</SelectBackgroundBtnForm>
);

export const Index = SelectBackgroundBtnFormIndex.bind({});
Index.args = {};
