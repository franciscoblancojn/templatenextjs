import { SelectBackgroundBtnFormClassProps } from '@/components/Input/SelectBackgroundBtnForm/Base';

export const tolinkme: SelectBackgroundBtnFormClassProps = {
    classNameContentTitle: `
        m-b-25
        text-capitalize
    `,
    classNameTitle: `
        flex
        flex-align-center
        flex-gap-column-10
    `,
    styleTemplateTitle: 'tolinkme23',
    classNameContentSelectType: `
        flex
        flex-gap-10
        flex-gap-row-10
        flex-gap-column-10
        flex-justify-between
    `,
    classNameContentSelectTypeItem: `
        flex-6
        
        box-shadow
        box-shadow-inset
        box-shadow-s-1
        
        border-radius-15
        p-5
        bg-white
        pos-r
        cursor-pointer
    `,
    classNameContentSelectTypeItemActive: `
        bg-gradient-darkAqua-brightPink
        box-shadow-c-transparent
    `,
    classNameContentSelectTypeItemInactive: `
        box-shadow-c-pinkishGreyThree
        filter-grayscale-2
    `,

    classNameContentSelectTypeItemColor: `
        pos-a
        inset-0
        width-p-100
        height-p-100
    `,

    classNameContentSelectTypeItemGradient: `
        pos-a
        inset-0
        width-p-100
        height-p-100
    `,

    classNameContentSelectTypeItemImg: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        object-fit-cover
    `,
    classNameContentSelectTypeItemVideo: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        object-fit-cover
    `,

    classNameContentSelectTypeItemContentValue: `
        border-radius-15
        bg-graySea
        p-p-b-115
        p-sm-p-b-50
        pos-r
        overflow-hidden
        bg-white
    `,
    classNameContentInput: `
        flex
        flex-column
        flex-gap-row-10
        p-v-15
        text-left
        p-h-15
    `,
    styleTemplateTitleInput: 'tolinkme13',

    styleTemplateInputColor: 'tolinkme2',
    styleTemplateInputGradient: 'tolinkme',
    styleTemplateInputImg: 'tolinkme',
    styleTemplateInputVideo: 'tolinkme',

    placeholderContentWidthProps: {
        className: `
            pos-a
            inset-0
            m-auto
            width-50
            height-50
            bg-white
            flex
            flex-align-center
            flex-justify-center
            color-warmGrey
            border-radius-100
        `,
    },
    sizeIconPlaceholder: 20,
    classNameContentSelectTypeItemContentValueType: {
        color: `
        
        `,
        gradient: `
        
        `,
        img: `
            bg-line-whiteFive-white
        `,
        video: `
            bg-line-whiteFive-white
            animation
            animation-moveBg
            animation-duration-20
            animation-timing-function-linear
        `,
    },
};

export const tolinkmeItemSmall: SelectBackgroundBtnFormClassProps = {
    ...tolinkme,
    classNameContentSelectTypeItemContentValue: `
        border-radius-15
        bg-graySea
        p-t-30
        p-b-30
        pos-r
        overflow-hidden
        bg-white
    `,
};
