import { InputPopupFileClassProps } from '@/components/Input/InputPopupFile/Base';

export const tolinkme: InputPopupFileClassProps = {
    styleTemplateInputFile: 'tolinkme',
    styleTemplatePopup: 'tolinkme3',

    classNameSelect: `
        flex
        flex-align
        font-16
        font-w-900
        font-nunito
        color-greyish
        cursor-pointer
        p-v-5

    
    `,
    classNameSelectNotSelected: `
        p-v-5  
    `,

    classNameAFile: `
        d-flex
        p-15
        height-vh-40
        flex-justify-center
        flex-align-center
        color-black
        border-5
        border-style-dashed
        border-black
        border-radius-15
        cursor-pointer
        m-b-15
        pos-r
    `,
    classNameFileItem: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        object-fit-contain
        p-15
    `,
    classNameAFileAudio: `
        height-max-70
    `,
    ContentBtnProps: {
        size: 200,
        className: `
            m-h-auto
            pos-sk
            top-p-80
            left-0
            right-0
            z-index-1
            height-0
        `,
    },
    classNameContentBtn: `
        transform
        transition-5
    `,
    classNameContentBtnActive: `
        transform-translate-X-p-0
    `,
    classNameContentBtnInactive: `
        transform-translate-X-vw-100   
    `,
    styleTemplateButton: 'tolinkme',
    styleTemplateTitlePopup: 'tolinkme11',
};
