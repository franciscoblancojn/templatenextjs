import { Popup, PopupStyles } from '@/components/Popup';
import { Theme, ThemesType } from '@/config/theme';
import { Text, TextStyles } from '@/components/Text';
import { File } from '@/svg/File';

import { useLang } from '@/lang/translate';
import { useData } from 'fenextjs-hook/cjs/useData';

import { ReactNode, useMemo, useState } from 'react';
import Reload from '@/svg/Reload';
import { InputFileDataProps } from '../File/Base';
import {
    InputUpload,
    InputUploadProps,
    InputUploadStyles,
} from '@/components/Input/Upload';
import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';
import Button, { ButtonStyles } from '@/components/Button';
import Space from '@/components/Space';
import {
    ExtensionesAudio,
    ExtensionesDis2D,
    ExtensionesDis3D,
    ExtensionesImagenes,
    ExtensionesMusicProduction,
    ExtensionesText,
    ExtensionesVideos,
} from '@/interfaces/File';
import Png from '@/svg/Png';
import Video from '@/svg/Video';
import Audio from '@/svg/Audio';
import Desing from '@/svg/Desing';
import Image from '@/components/Image';

export interface InputPopupFileClassProps {
    classNameSelect?: string;
    classNameSelectNotSelected?: string;

    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;

    ContentBtnProps?: ContentWidthProps;
    classNameContentBtn?: string;
    classNameContentBtnActive?: string;
    classNameContentBtnInactive?: string;
    styleTemplateButton?: ButtonStyles | ThemesType;

    classNameReload?: string;

    classNameAFile?: string;
    classNameAFileImg?: string;
    classNameAFileVideo?: string;
    classNameAFileAudio?: string;
    classNameAFileOthers?: string;
    classNameFileItem?: string;

    styleTemplateInputFile?: InputUploadStyles | ThemesType;
}

export interface InputPopupFileBaseProps
    extends Omit<InputUploadProps, 'styleTemplate'> {}

export interface InputPopupFileProps
    extends InputPopupFileClassProps,
        InputPopupFileBaseProps {}

export const InputPopupFileBase = ({
    classNameSelect = '',
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',
    styleTemplateInputFile = Theme?.styleTemplate ?? '_default',
    styleTemplateTitlePopup = Theme?.styleTemplate ?? '_default',
    defaultValue,
    classNameReload = '',
    classNameAFile = '',
    classNameAFileImg = '',
    classNameAFileVideo = '',
    classNameAFileAudio = '',
    classNameAFileOthers = '',
    classNameFileItem = '',
    onChange,
    ContentBtnProps = {},
    classNameContentBtn = '',
    classNameContentBtnActive = '',
    classNameContentBtnInactive = '',
    styleTemplateButton = Theme.styleTemplate ?? '_default',
}: InputPopupFileProps) => {
    const _t = useLang();
    const [activePopup, setActivePopup] = useState(false);
    const defaultValue_ = useMemo<InputFileDataProps>(
        () =>
            defaultValue ?? {
                fileData: '',
            },
        []
    );
    const { data, setData, onRestart, dataMemo, isChange, setIsChange } =
        useData<InputFileDataProps, ReactNode>(defaultValue_, {
            // onChangeDataAfter: (data: InputFileDataProps) => {
            //     onChange?.(data);
            // },
            onMemo: (data: InputFileDataProps) => {
                if (!data?.fileData || data?.fileData == '') {
                    return <></>;
                }
                const extencion = data?.fileData
                    ?.split?.('?')?.[0]
                    ?.split?.('.')
                    ?.pop?.();

                const getFileView = () => {
                    if (ExtensionesImagenes.includes(extencion)) {
                        return (
                            <>
                                <div
                                    className={`${classNameAFile} ${classNameAFileImg}`}
                                >
                                    <img
                                        src={data.fileData}
                                        className={classNameFileItem}
                                    />
                                </div>
                            </>
                        );
                    }
                    if (ExtensionesVideos.includes(extencion)) {
                        return (
                            <>
                                <div
                                    className={`${classNameAFile} ${classNameAFileVideo}`}
                                >
                                    <video
                                        src={data.fileData}
                                        controls
                                        className={classNameFileItem}
                                    />
                                </div>
                            </>
                        );
                    }
                    if (ExtensionesAudio.includes(extencion)) {
                        return (
                            <>
                                <div
                                    className={`${classNameAFile} ${classNameAFileAudio}`}
                                >
                                    <video
                                        src={data.fileData}
                                        controls
                                        className={classNameFileItem}
                                    />
                                </div>
                            </>
                        );
                    }
                    return (
                        <>
                            <div
                                className={`${classNameAFile} ${classNameAFileOthers}`}
                            >
                                <File size={50} />
                            </div>
                        </>
                    );
                };

                return (
                    <>
                        <a
                            href={data?.fileData}
                            target="_blank"
                            rel="noreferrer"
                        >
                            {getFileView()}
                        </a>
                    </>
                );
            },
        });

    const onRestartData = async () => {
        onRestart();
    };

    const onSave = () => {
        onChange?.(data);
        setActivePopup(false);
        setIsChange(false);
    };

    const onCancel = () => {
        setActivePopup(false);
        setIsChange(false);
    };

    const file = useMemo(() => {
        const url = decodeURI(data?.fileData);
        const urlFixed = url?.split('%2F').join('/');
        const extencion = urlFixed?.split?.('?')?.[0]?.split?.('.')?.pop?.();
        const name = urlFixed
            ?.split?.('?')?.[0]
            ?.split?.('/')
            ?.pop?.()
            ?.split('.')[0];
        return {
            extencion,
            name,
            url,
        };
    }, [data]);

    return (
        <>
            <Popup
                btnSwActive={activePopup}
                setBtnSwActive={setActivePopup}
                onClose={onCancel}
                styleTemplate={styleTemplatePopup}
                contentBackExtra={
                    <>
                        <div
                            className={classNameReload}
                            onClick={onRestartData}
                        >
                            <Reload size={20} />
                        </div>
                    </>
                }
                btn={
                    <>
                        {data?.fileData && data?.fileData != '' ? (
                            <>
                                <div
                                    className={`${classNameSelect}`}
                                    style={{ textTransform: 'none' }}
                                >
                                    {file.name?.slice?.(0, 20)}
                                    {(file?.name ?? '')?.length > 20
                                        ? '...'
                                        : ''}
                                    .{file.extencion}
                                </div>
                            </>
                        ) : (
                            <>
                                <div className={`${classNameSelect}`}>
                                    {_t('File')}
                                </div>
                            </>
                        )}
                    </>
                }
            >
                <Text styleTemplate={styleTemplateTitlePopup}>
                    {_t('Select your Custom File')}
                </Text>
                <Space size={25} />
                {dataMemo}
                <InputUpload
                    styleTemplate={styleTemplateInputFile}
                    accept={'ALL'}
                    onSubmit={async (e: InputFileDataProps | undefined) => {
                        if (e) {
                            setData(e);
                        }
                        return {
                            status: 'ok',
                        };
                    }}
                />
                <div className="filesRecomend m-t-20">
                    <Text styleTemplate={styleTemplateTitlePopup}>
                        {_t('Archivos Recomendados')}
                    </Text>
                    <div className="m-t-30">
                        <Text
                            styleTemplate={styleTemplateTitlePopup}
                            className="flex flex-gap-column-10"
                        >
                            <Png size={15} />
                            {_t('Imagenes')}
                        </Text>
                        <div className="flex flex-justify-between- flex-gap-column-15 flex-gap-row-15 text-lowercase">
                            {ExtensionesImagenes.map((e, i) => (
                                <div
                                    key={i}
                                    className="flex flex-align-center flex-gap-column-5"
                                >
                                    <Image
                                        src={`extencions/${e}.png`}
                                        className="width-20"
                                    />
                                    .{e}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="m-t-30">
                        <Text
                            styleTemplate={styleTemplateTitlePopup}
                            className="flex flex-gap-column-10"
                        >
                            <Video size={15} />
                            {_t('Videos')}
                        </Text>
                        <div className="flex flex-justify-between- flex-gap-column-15 flex-gap-row-15 text-lowercase">
                            {ExtensionesVideos.map((e, i) => (
                                <div
                                    key={i}
                                    className="flex flex-align-center flex-gap-column-5"
                                >
                                    <Image
                                        src={`extencions/${e}.png`}
                                        className="width-20"
                                    />
                                    .{e}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="m-t-30">
                        <Text
                            styleTemplate={styleTemplateTitlePopup}
                            className="flex flex-gap-column-10"
                        >
                            <Audio size={15} />
                            {_t('Audios')}
                        </Text>
                        <div className="flex flex-justify-between- flex-gap-column-15 flex-gap-row-15 text-lowercase">
                            {ExtensionesAudio.map((e, i) => (
                                <div
                                    key={i}
                                    className="flex flex-align-center flex-gap-column-5"
                                >
                                    <Image
                                        src={`extencions/${e}.png`}
                                        className="width-20"
                                    />
                                    .{e}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="m-t-30">
                        <Text
                            styleTemplate={styleTemplateTitlePopup}
                            className="flex flex-gap-column-10"
                        >
                            <Desing size={15} />
                            {_t('Diseño 2D')}
                        </Text>
                        <div className="flex flex-justify-between- flex-gap-column-15 flex-gap-row-15 text-lowercase">
                            {ExtensionesDis2D.map((e, i) => (
                                <div
                                    key={i}
                                    className="flex flex-align-center flex-gap-column-5"
                                >
                                    <Image
                                        src={`extencions/${e}.png`}
                                        className="width-20"
                                    />
                                    .{e}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="m-t-30">
                        <Text
                            styleTemplate={styleTemplateTitlePopup}
                            className="flex flex-gap-column-10"
                        >
                            <Desing size={15} />
                            {_t('Diseño 3D')}
                        </Text>
                        <div className="flex flex-justify-between- flex-gap-column-15 flex-gap-row-15 text-lowercase">
                            {ExtensionesDis3D.map((e, i) => (
                                <div
                                    key={i}
                                    className="flex flex-align-center flex-gap-column-5"
                                >
                                    <Image
                                        src={`extencions/${e}.png`}
                                        className="width-20"
                                    />
                                    .{e}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="m-t-30">
                        <Text
                            styleTemplate={styleTemplateTitlePopup}
                            className="flex flex-gap-column-10"
                        >
                            <Audio size={15} />
                            {_t('Music Production')}
                        </Text>
                        <div className="flex flex-justify-between- flex-gap-column-15 flex-gap-row-15 text-lowercase">
                            {ExtensionesMusicProduction.map((e, i) => (
                                <div
                                    key={i}
                                    className="flex flex-align-center flex-gap-column-5"
                                >
                                    <Image
                                        src={`extencions/${e}.png`}
                                        className="width-20"
                                    />
                                    .{e}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="m-t-30">
                        <Text
                            styleTemplate={styleTemplateTitlePopup}
                            className="flex flex-gap-column-10"
                        >
                            <Png size={15} />
                            {_t('Text')}
                        </Text>
                        <div className="flex flex-justify-between- flex-gap-column-15 flex-gap-row-15 text-lowercase">
                            {ExtensionesText.map((e, i) => (
                                <div
                                    key={i}
                                    className="flex flex-align-center flex-gap-column-5"
                                >
                                    <Image
                                        src={`extencions/${e}.png`}
                                        className="width-20"
                                    />
                                    .{e}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>

                <ContentWidth {...ContentBtnProps}>
                    <div
                        className={`${classNameContentBtn} ${
                            isChange
                                ? classNameContentBtnActive
                                : classNameContentBtnInactive
                        }`}
                    >
                        <Button
                            styleTemplate={styleTemplateButton}
                            onClick={onSave}
                        >
                            {_t('Aplicar')}
                        </Button>
                    </div>
                </ContentWidth>
            </Popup>
        </>
    );
};
export default InputPopupFileBase;
