# InputPopupFile

## Dependencies

[InputPopupFile](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/InputPopupFile)

```js
import { InputPopupFile } from '@/components/InputPopupFile';
```

## Import

```js
import {
    InputPopupFile,
    InputPopupFileStyles,
} from '@/components/InputPopupFile';
```

## Props

```tsx
interface InputPopupFileProps {}
```

## Use

```js
<InputPopupFile>InputPopupFile</InputPopupFile>
```
