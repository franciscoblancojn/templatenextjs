import { RadioBorderRadiusButtonClassProps } from '@/components/Input/RadioBorderRadiusButton/Base';
import { BorderRadiusButtonValues } from '@/interfaces/Button';

export const tolinkme: RadioBorderRadiusButtonClassProps = {
    ContentWidthProps: {
        size: 500,
        className: `
            p-h-15
            m-h-auto
        `,
    },
    styleTemplateRadius: 'tolinkme',
    classNameBtn: `
        width-p-100
        text-center
        color-white
        text-capitalize
        font-nunito
        font-w-700

        font-20
        p-10
    `,
    classNameBtnActive: ` 
        bg-gradient-darkAqua-brightPink
    `,
    classNameBtnInactive: `
        bg-greyish
    `,
    classNameBtnTypes: BorderRadiusButtonValues,
};

export const tolinkmeColumn: RadioBorderRadiusButtonClassProps = {
    ...tolinkme,
    ContentWidthProps: {
        size: -1,
        className: `
            m-h-auto
            flex 
            flex-nowrap
            flex-gap-column-15
        `,
    },
    styleTemplateRadius: 'tolinkmeColumn',
    classNameBtn: `
        width-p-100
        p-p-b-100
        font-0
    `,
};
