import { Story, Meta } from "@storybook/react";

import { InputSelectBackgroundProps, InputSelectBackground } from "./index";

export default {
    title: "Input/InputSelectBackground",
    component: InputSelectBackground,
} as Meta;

const InputSelectBackgroundIndex: Story<InputSelectBackgroundProps> = (args) => (
    <InputSelectBackground {...args}>Test Children</InputSelectBackground>
);

export const Index = InputSelectBackgroundIndex.bind({});
Index.args = {};
