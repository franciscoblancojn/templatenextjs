import React, { CSSProperties, useState } from 'react';
import * as Yup from 'yup';

import log from '@/functions/log';
import { useRef } from 'react';
import { useLang } from '@/lang/translate';

function useRunAfterUpdate() {
    const afterPaintRef: any = React.useRef(null);
    React.useLayoutEffect(() => {
        if (afterPaintRef.current) {
            afterPaintRef.current();
            afterPaintRef.current = null;
        }
    });
    const runAfterUpdate = (fn: any) => (afterPaintRef.current = fn);
    return runAfterUpdate;
}

export const BaseStyle = {
    classNameInput: `
        p-h-10 p-v-5 
        font-20 font-montserrat
        width-p-100 
        border-0 
        border-radius-0
        box-shadow box-shadow-inset box-shadow-s-1
        outline-none 
        color-white
    `,
    classNameLabel: `
        pos-r 
        d-block 
        font-20 font-montserrat
        color-white
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-error

    `,
    //for Select
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        color-white
        border-style-solid border-1 border-black
        font-15
        z-index-1
        overflow-auto
    `,
    classNameOption: `
        p-h-15
        pointer
        list-none
        color-white
    `,
    classNameOptionDisabled: `
        bg-gray
        color-white
    `,
};
export interface InputTextClassProps {
    classNameLabel?: string;
    classNameLabelError?: string;
    classNameLabelOk?: string;
    classNameContentInput?: string;
    classNameInput?: string;
    classNameInputEnabled?: string;
    classNameInputDisabled?: string;
    classNameIcon?: string;
    classNameError?: string;
    classNameOptions?: string;
    classNameOption?: string;
    classNameOptionDisabled?: string;
    classNameLoaderValidate?: string;
    iconLoaderValidate?: any;
}
export interface InputTextBaseProps {
    style?: CSSProperties;
    styleInput?: CSSProperties;
    styleErrorYup?: CSSProperties;
    yup?: any;
    label?: any;
    contentFlexAddon?: string;
    placeholder?: string;
    defaultValue?: string | null;
    value?: string | null;
    type?:
        | 'text'
        | 'search'
        | 'tel'
        | 'url'
        | 'password'
        | 'number'
        | 'textarea';
    onChange?: (v: string) => void;
    onBlur?: (v: string) => void;
    onEnter?: () => void;
    onChangeValidate?: (e: string) => Promise<any> | any;
    onChangeValidateBeforeYup?: (e: string) => Promise<void> | void;
    onChangeValidateAfterYup?: (e: string) => Promise<void> | void;
    props?: any;
    icon?: any;
    extraInContentInput?: any;
    extraInLabel?: any;
    disabled?: boolean;
    useLoader?: boolean;
    showIcon?: boolean;
}
export interface InputTextProps
    extends InputTextBaseProps,
        InputTextClassProps {}

export const InputTextBase = ({
    yup = Yup.string(),
    style,
    styleInput,
    styleErrorYup,
    label = '',
    placeholder = '',
    defaultValue = null,
    value = null,
    type = 'text',
    classNameLabel = '',
    classNameLabelError = '',
    classNameLabelOk = '',
    classNameContentInput = '',
    contentFlexAddon = '',
    classNameInput = '',
    classNameInputEnabled = '',
    classNameInputDisabled = '',
    classNameIcon = '',
    classNameError = '',
    classNameLoaderValidate = '',
    iconLoaderValidate = <></>,
    onChange = () => {},
    onBlur = () => {},
    onEnter = () => {},
    onChangeValidate = async (e) => e,
    onChangeValidateBeforeYup = async (e) => {
        log('onChangeValidateBeforeYup', e);
    },
    onChangeValidateAfterYup = async (e) => {
        log('onChangeValidateAfterYup', e);
    },
    props = {},
    icon = <></>,
    extraInContentInput = <></>,
    extraInLabel = <></>,
    disabled = false,
    useLoader = true,
    showIcon = true,
}: InputTextProps) => {
    const _t = useLang();

    const [loaderValidate, setLoaderValidate] = useState<boolean>(false);
    const [statusInput, setStateInput] = useState('');
    const [error, setError] = useState('');
    const [valueInput, setValueInput] = useState(defaultValue ?? '');
    const ref = useRef(null);
    const runAfterUpdate = useRunAfterUpdate();

    const validateValue = async (v: any) => {
        setLoaderValidate(true);
        try {
            v = await onChangeValidate(v);
            await onChangeValidateBeforeYup(v);
        } catch (error: any) {
            log('error', error, 'red');
            setStateInput('error');
            setError(error.message);
            setLoaderValidate(false);
            return v;
        }
        if (yup != null) {
            yup.validate(v)
                .then(async function (valid: boolean) {
                    if (valid) {
                        setStateInput('ok');
                        setError('');
                        try {
                            setLoaderValidate(true);
                            await onChangeValidateAfterYup(v);
                            setLoaderValidate(false);
                        } catch (error: any) {
                            log('error', error, 'red');
                            setStateInput('error');
                            setError(error.message);
                            setLoaderValidate(false);
                            return;
                        }
                    }
                })
                .catch(function (error: any) {
                    log('error', error, 'red');
                    setStateInput('error');
                    setError(error.message);
                    setLoaderValidate(false);
                    return;
                });
            setLoaderValidate(false);
            return v;
        } else {
            try {
                setLoaderValidate(true);
                await onChangeValidateAfterYup(v);
                setLoaderValidate(false);
                return v;
            } catch (error: any) {
                log('error', error, 'red');
                setStateInput('error');
                setError(error.message);
                setLoaderValidate(false);
                return;
            }
        }
    };

    const changeInput = async (e: any) => {
        const input = e.target;
        const text = input.value;
        const cursor = input.selectionStart;

        const v = await validateValue(text);
        setValueInput(v);
        onChange(v);
        runAfterUpdate(() => {
            try {
                input.selectionStart = cursor;
                input.selectionEnd = cursor;
            } catch (error) {
                log('runAfterUpdate error', error);
            }
        });
    };
    const blurInput = () => {
        validateValue(valueInput);
        onBlur(valueInput);
    };

    const TagInput = type == 'textarea' ? 'textarea' : 'input';

    return (
        <>
            <label
                style={style}
                className={`${classNameLabel}
                ${error != '' ? classNameLabelError : classNameLabelOk}
            `}
            >
                <div>{label}</div>
                <div className={`${classNameContentInput} ${contentFlexAddon}`}>
                    <TagInput
                        type={type}
                        style={styleInput}
                        ref={ref}
                        className={`input ${classNameInput} ${statusInput} ${
                            disabled
                                ? classNameInputDisabled
                                : classNameInputEnabled
                        }`}
                        placeholder={_t(placeholder)}
                        value={value ?? valueInput}
                        onChange={changeInput}
                        onBlur={blurInput}
                        disabled={disabled}
                        name="medad"
                        aria-autocomplete="none"
                        autoComplete={'off'}
                        onKeyUp={(event) => {
                            if (event.keyCode === 13) {
                                onEnter();
                            }
                        }}
                        {...props}
                    />
                    {showIcon ? (
                        <span className={`icon ${classNameIcon}`}>{icon}</span>
                    ) : (
                        <></>
                    )}
                    {loaderValidate && useLoader && (
                        <span
                            className={`loaderValidate ${classNameLoaderValidate}`}
                        >
                            {iconLoaderValidate}
                        </span>
                    )}
                    {extraInContentInput}
                </div>
                {error != '' && (
                    <div style={styleErrorYup} className={classNameError}>
                        {_t(error)}
                    </div>
                )}
                {extraInLabel}
            </label>
        </>
    );
};
export default InputTextBase;
