import { InputTextClassProps } from '@/components/Input/Text/Base';

export const mooveri: InputTextClassProps = {
    classNameInput: `
        p-h-19 p-v-12 
        font-16 font-nunito
        width-p-100 
        border-0 
        border-radius-9
        box-shadow box-shadow-inset box-shadow-s-2
        outline-none 
        color-warmGreyTwo
        bg-whiteThree
    `,
    classNameLabel: `
        pos-r 
        d-block 
        font-0 font-nunito
    `,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
    `,
    classNameIcon: `
        color-warmGreyTwo  
    `,
    //for Select
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-min-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-15
        z-index-1
        overflow-auto
    `,
    classNameOption: `
        p-h-15
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-white-space-nowrap
        flex 
        flex-nowrap
    `,
    classNameOptionDisabled: `
        bg-gray
        color-white
    `,
};

export const mooveriSaveCancel: InputTextClassProps = {
    classNameInput: `
        bg-transparent
        font-circular 
        p-h-0
        p-v-3 
        font-13 font-nunito
        font-w-500
        width-p-100 
        border-0 
        outline-none 
        color-warmGreyTwo
        box-shadow box-shadow-inset box-shadow-y--2 box-shadow-x-0
        box-shadow-c-peachyPink
    `,
    classNameLabel: `
        pos-r 
        nameField 
        font-13  
        font-w-900 
        color-greyishBrown
        m-r-auto  
        font-circular
    `,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
    `,
    classNameIcon: `
        color-warmGreyTwo  
    `,
    //for Select
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-15
        z-index-1
        overflow-auto
    `,
    classNameOption: `
        p-h-15
        pointer
        list-none
        bg-gray-hover
        color-white-hover
    
    `,
    classNameOptionDisabled: `
        bg-gray
        color-white
    `,
};

export const mooveriEdit: InputTextClassProps = {
    ...mooveriSaveCancel,
    classNameInput: `
        bg-transparent
        w-175     
        font-circular 
        p-v-3 
        font-13 font-nunito
        font-w-500
        width-p-100 
        border-0 
        outline-none 
        color-warmGreyTwo
        box-shadow box-shadow-inset box-shadow-y-0 box-shadow-x-0
        box-shadow-c-peachyPink
    `,
};

export const mooveriManage: InputTextClassProps = {
    ...mooveri,
    classNameInput: `
        bg-transparent
        w-175     
        font-circular 
        p-v-3 
        font-13 font-nunito
        font-w-500
        width-p-100 
        border-0 
        outline-none 
        color-warmGreyTwo
        box-shadow box-shadow-inset box-shadow-y--1 box-shadow-x-0
        box-shadow-c-warmGreyTwo
    `,
    classNameLabel: `
        pos-r 
        nameField 
        font-13  
        font-w-900 
        color-greyishBrown
        m-r-auto  
        font-circular
    `,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        bg-whiteThree-hover
        color-greyishBrown
        font-w-400
        font-14
        text-white-space-nowrap
        flex 
        flex-nowrap
    `,
    classNameOptionDisabled: `
    bg-whiteThree
    color-white
    `,
};

export const mooveri2: InputTextClassProps = {
    classNameInput: `
        font-nunito 
        font-12 
        font-nunito
        color-warmGreyTwo
        font-w-400
        width-p-100 
        border-0 
        outline-none 
        bg-graySea
        height-min-26
        
    `,
    classNameContentInput: `
    flex
    flex-nowrap
    `,
    classNameLabel: `
        pos-r 
        nameField 
        font-13  
        font-w-900 
        color-greyishBrown
        m-r-auto  
        font-nunito
        bg-graySea
        flex
        d-inline-block
        p-v-5
        p-h-10
        border-radius-9
        width-p-100
    `,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
    `,
    classNameIcon: `
        color-warmGreyTwo  
    `,
    //for Select
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-min-100
        m-0
        p-v-0 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-15
        z-index-1
        overflow-auto
        border-b-l-radius-9
        border-b-r-radius-9
    `,
    classNameOption: `
       
        pointer
        list-none
        bg-pinkishGreyFour-hover
        font-nunito 
        font-12 
        font-nunito
        color-warmGreyTwo
        font-w-400
        p-5
        text-white-space-nowrap
        flex 
        flex-nowrap
    `,
    classNameOptionDisabled: `
        
        color-white
    `,
};

export const mooveri2InputIconReverse: InputTextClassProps = {
    ...mooveri2,
    classNameContentInput: `
        flex
        flex-nowrap
        flex-row-reverse
`,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
        pos-a
        right-0
        top-p-100
        z-index-5
    `,
    classNameLabelError: `
        m-b-10
    `,
};

export const mooveri3: InputTextClassProps = {
    ...mooveri2,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
        pos-a
        right-0
        top-p-100
        z-index-5
    `,
    classNameLabelError: `
        m-b-10
    `,
};
export const mooveriTelCompany: InputTextClassProps = {
    ...mooveri,
};

export const mooveriProfileEdit: InputTextClassProps = {
    classNameInput: `
    bg-transparent
    width-p-100    
    font-nunito 
    p-v-3 
    font-32 
    font-nunito
    font-w-900
    width-p-100 
    border-0 
    outline-none 
    color-warmGreyTwo
    box-shadow-c-warmGreyTwo
`,
    classNameInputEnabled: `
    font-32 
    font-nunito
    font-w-900
    width-p-100 
    border-0 
    outline-none 
    color-warmGreyTwo
    border-style-solid
    border-b-2
    border-black16
`,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
       
    `,
};

export const mooveriProfileNameDescription: InputTextClassProps = {
    classNameInput: `
    bg-transparent
    width-p-100    
    font-nunito 
    p-v-3 
    font-16 
    font-nunito
    font-w-700
    width-p-100 
    border-0 
    outline-none 
    color-warmGreyThree
    box-shadow-c-warmGreyTwo
    
`,
    classNameInputEnabled: `
    font-16 
    font-nunito
    font-w-700
    width-p-100 
    border-0 
    outline-none 
    color-warmGreyThree
    border-style-solid
    border-b-2
    border-black16
`,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
        
    `,
};

export const mooveri6: InputTextClassProps = {
    ...mooveri3,

    classNameLabelError: `
        m-b-10
    `,

    classNameInput: `
        p-h-10 p-v-8 
        font-16 font-nunito
        width-p-100 
        border-0 
        border-radius-9
        box-shadow box-shadow-inset box-shadow-s-2
        outline-none 
        color-warmGreyTwo
        bg-whiteThree
    `,

    classNameIcon: `
        color-warmGreyTwo  
        flex
        flex-align-center
        pointer
    `,
    //for Select

    classNameOption: `
        p-h-15
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-white-space-nowrap
        flex 
        flex-nowrap
    `,
    classNameOptionDisabled: `
        bg-gray
        color-white
    `,
};

export const mooveriInputNumber: InputTextClassProps = {
    ...mooveri,
};

export const mooveriBackoffice: InputTextClassProps = {
    classNameInput: `
        p-h-19 p-v-12 
        font-16 font-nunito
        width-p-100 
        border-0 
        border-radius-9
        box-shadow box-shadow-inset box-shadow-s-2
        outline-none 
        color-warmGreyTwo
        bg-whiteThree
        pos-r
    `,
    classNameLabel: `
        pos-r 
        d-block 
        font-16 font-nunito
        color-white
        font-nunito 
        font-w-800
        width-p-100
    `,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameIcon: `
        color-warmGreyTwo  
        pos-a
        top-0
        right-15
        bottom-0
        m-auto
    `,
    //for Select
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-min-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-15
        z-index-1
        overflow-auto
        color-gray
    `,
    classNameOption: `
        p-h-15
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-white-space-nowrap
        flex 
        flex-nowrap
    `,
    classNameOptionDisabled: `
        bg-gray
        color-white
    `,
};

export const mooveriBackofficeDate: InputTextClassProps = {
    classNameInput: `
        p-5
        p-h-10
        font-14 font-nunito
        width-p-100 
        border-0 
        border-radius-9
        box-shadow box-shadow-inset box-shadow-s-2
        outline-none 
        color-warmGreyTwo
        bg-whiteThree
        pos-r
    `,
    classNameLabel: `
        pos-r 
        d-block 
        font-12 font-nunito
        color-white
        font-nunito 
        font-w-800
        width-p-100
    `,
    classNameError: `
        font-8
        font-w-900
        font-nunito
        text-right
        p-v-3
        color-error
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameIcon: `
        color-warmGreyTwo  
        pos-a
        top-0
        right-15
        bottom-0
        m-auto
    `,
    //for Select
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-min-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-15
        z-index-1
        overflow-auto
        color-gray
    `,
    classNameOption: `
        p-h-15
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-white-space-nowrap
        flex 
        flex-nowrap
    `,
    classNameOptionDisabled: `
        bg-gray
        color-white
    `,
};
