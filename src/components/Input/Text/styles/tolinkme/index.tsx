import { BaseStyle, InputTextClassProps } from '@/components/Input/Text/Base';

import { Loader } from '@/svg/loader';

export const tolinkme: InputTextClassProps = {
    classNameInput: `
        box-shadow-black 
        bg-black-144
        box-shadow box-shadow-inset box-shadow-s-2
        box-shadow-transparent box-shadow-c-transparent
        border-radius-11
        border-0
        width-p-100
        p-v-20
        p-h-18
        font-14
        color-white
        color-white-placeholder
        font-nunito
        outline-none 
    `,
    classNameLabel: `
        pos-r
        ${BaseStyle.classNameLabel}
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-white
    `,
    classNameIcon: `
        color-white-4 color-white-hover
        flex
        flex-align-center
        
    `,
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-14
        z-index-5
        overflow-auto
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-capitalize
    `,
    classNameOptionDisabled: `
        ${BaseStyle.classNameOptionDisabled}
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        pos-a
        top-0
        right-10
        bottom-0
        m-auto
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoaderValidate: (
        <>
            <Loader size={40} />
        </>
    ),
};
export const tolinkmeAddon: InputTextClassProps = {
    classNameInput: `
        box-shadow-black 
        bg-black-144
        box-shadow box-shadow-inset box-shadow-s-2
        box-shadow-transparent box-shadow-c-transparent
        border-radius-11
        border-0
        width-p-100
        p-v-10
        p-h-15
        font-18
        color-white
        color-gray-placeholder
        font-nunito
        outline-none 
    `,
    classNameLabel: `
        pos-r
        ${BaseStyle.classNameLabel}
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-white
    `,
    classNameIcon: `
        color-white-4 color-white-hover
        flex
        flex-align-center
        
    `,
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-14
        z-index-5
        overflow-auto
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-capitalize
    `,
    classNameOptionDisabled: `
        ${BaseStyle.classNameOptionDisabled}
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        pos-a
        top-0
        right-10
        bottom-0
        m-auto
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoaderValidate: (
        <>
            <Loader size={40} />
        </>
    ),
};

export const tolinkmeAddons: InputTextClassProps = {
    classNameInput: `
       
        bg-transparent
        border-0
        width-p-100
        outline-none 
    `,
    classNameLabel: `
        pos-r
        ${BaseStyle.classNameLabel}
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-white
    `,

    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        pos-a
        top-0
        right-10
        bottom-0
        m-auto
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoaderValidate: (
        <>
            <Loader size={40} />
        </>
    ),
};
export const tolinkmeAddEdit: InputTextClassProps = {
    ...tolinkme,
    classNameInput: `
        box-shadow-azure 
        box-shadow box-shadow-inset box-shadow-y--2
        border-radius-0
        border-0
        width-p-100
        p-v-4
        p-h-0
        font-14 font-sm-21
        bg-transparent
        color-greyish
        color-greyish-placeholder
        font-nunito
        outline-none 
    `,
    classNameLabel: `
        pos-r
        ${tolinkme.classNameLabel}
        width-p-100
    `,
};
export const tolinkmeSimple: InputTextClassProps = {
    classNameContentInput: `
        pos-r
    `,
    classNameLabel: `
        pos-r
        d-block
    `,
    classNameInput: `
        width-p-max-100
        border-0
        font-18
        font-w-900
        font-nunito
        color-greyish
        outline-none
        bg-transparent
        text-capitalize
    `,
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        font-14
        font-w-900
        font-nunito
        z-index-5
        overflow-auto
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        flex
        flex-align-center
        text-capitalize
    `,
    classNameOptionDisabled: `
        ${BaseStyle.classNameOptionDisabled}
    `,
    classNameIcon: `
        pointer
        color-black
        pos-a
        top-0
        right-10
        butttom-0
        m-auto
    `,
};
export const tolinkmeSimple2: InputTextClassProps = {
    ...tolinkmeSimple,
    classNameInput: `
        width-p-max-100
        width-p-100
        border-0
        font-13
        font-w-700
        font-nunito
        color-greyish
        outline-none
        border-b-1
        p-b-5
        border-style-solid
        border-greyish
        border-darkAqua-hover
    `,
};
export const tolinkmeSimple2Bold: InputTextClassProps = {
    ...tolinkmeSimple2,
    classNameInput: `
        width-p-max-100
        width-p-100
        border-0
        font-19
        font-sm-18
        font-w-900
        font-nunito
        color-greyishBrown
        outline-none
        border-b-1
        p-0
        border-style-solid
        border-greyish
        border-darkAqua-hover
    `,
};

export const tolinkme2: InputTextClassProps = {
    ...tolinkme,
    classNameInput: `
        width-p-100
        classNameInputDisabled
        classNameInputEnabled
        bg-transparent


        p-h-0
        p-v-5

        font-14 font-w-400 font-nunito
        color-black

        outline-none 
        border-greyish
        border-darkAqua-hover
        text-decoration-none

    `,
    classNameLabel: `
        font-14 font-w-900 font-nunito
        color-greyishBrown 
        pos-r
    `,
    classNameInputDisabled: `
        border-0
    `,
    classNameInputEnabled: `
        border-0
        border-style-solid
        border-b-1
        border-pinkishGreyThree
    `,
    classNameIcon: `
        color-pinkishGrey
        pos-a
        top-0
        right-0
        bottom-0
        m-auto
        flex
        flex-align-center
    `,
    classNameError: `
        color-red
        font-nunito
        font-11
        text-right
        p-t-5
    `,
};
export const tolinkme8: InputTextClassProps = {
    classNameInput: `
        width-p-100
        bg-transparent
        font-17 font-w-900 font-nunito
        outline-none 
        border-greyish
        text-decoration-none
        m-b-14
        color-greyish
        
    `,
    classNameLabel: `
        font-17 font-w-900 font-nunito
        color-greyish
        
    `,
    classNameInputDisabled: `
        border-0
        color-greyish
    `,
    classNameInputEnabled: `
        border-0
        border-style-solid
        border-b-1
        border-pinkishGreyThree
        color-greyish
    `,

    classNameError: `
        color-red
        font-nunito
        font-11
        text-right
        
    `,
};
export const tolinkme9: InputTextClassProps = {
    classNameInput: `
        width-p-100
        font-12 font-w-900 font-nunito
        outline-none 
        border-greyish
        text-decoration-none
        color-greyish
        
    `,
    classNameLabel: `
        
        font-12 font-w-900 font-nunito
        color-greyish
    `,
    classNameInputDisabled: `
        border-0
    `,
    classNameInputEnabled: `
        border-0
        border-style-solid
        border-b-1
        border-pinkishGreyThree
        
    `,

    classNameError: `
        color-red
        font-nunito
        font-11
        text-right
        
    `,
};

export const tolinkmeSearch: InputTextClassProps = {
    ...tolinkme2,

    classNameInput: `
        width-p-100
        classNameInputDisabled
        classNameInputEnabled
        bg-transparent


        p-h-0
        p-v-11

        font-18 font-w-400 font-nunito
        color-greyish

        outline-none 
        border-greyish
        border-darkAqua-hover
        text-decoration-none

        p-l-35

    `,

    classNameIcon: `
        color-pinkishGrey
        pos-a
        top-0
        left-0
        bottom-0
        m-auto
        flex
        flex-align-center
    `,
};

export const tolinkme3: InputTextClassProps = {
    classNameInput: `
        box-shadow-black 
        bg-grayAcd
        box-shadow box-shadow-inset box-shadow-s-2
        box-shadow-transparent box-shadow-c-transparent
        border-radius-10
        border-0
        width-p-100
        p-v-5
        p-h-15
        font-15
        font-w-800
        color-greyishBrown
        color-greyishBrown-placeholder
        font-nunito
        outline-none 
    `,
    classNameLabel: `
        font-14 font-w-900 font-nunito
        color-pinkishGrey 
        pos-r
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-white
        font-nunito
    `,
    classNameIcon: `
        color-black color-white-hover
        pos-a
        top-0
        right-15
        bottom-0
        m-auto
    `,
    classNameOptions: `
        font-nunito
        pos-a 
        border-radius-16
        top-p-100 left-0 
        width-p-min-100
        m-0
        p-v-10
        p-h-0
        bg-white
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16

        font-14
        z-index-1
        overflow-auto
    `,
    classNameOption: `
        p-h-29
        p-v-10
        pointer
        list-none
        color-black
        font-size-16 font-size-md-14
        
        font-w-700
        font-nunito
        text-white-space-nowrap
        bg-grayAcd-hover
    `,
    classNameOptionDisabled: `
        d-none
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        pos-a
        top-0
        right-10
        bottom-0
        m-auto
        color-white
        flex
        flex-align-center
        flex-justify-center
        font-nunito
    `,
    iconLoaderValidate: (
        <>
            <Loader size={40} />
        </>
    ),
};
export const tolinkme4: InputTextClassProps = {
    ...tolinkme2,

    classNameLabel: `
        font-16
        font-w-800
        color-greyishBrown
        font-nunito
        width-p-100
        pos-r
    `,
};
export const tolinkme5: InputTextClassProps = {
    ...tolinkme4,
};
export const tolinkme6: InputTextClassProps = {
    ...tolinkme2,
    classNameInput: `
        width-p-100
        bg-transparent
        border-pinkishGreyThree
        p-h-5  p-md-h-24
        p-v-5
        font-14 font-w-400 font-nunito
        color-greyish
        outline-none 
    `,
};
export const tolinkme7: InputTextClassProps = {
    ...tolinkme,
    classNameInput: `
        bg-transparent
        border-0
        width-p-100
        p-0
        font-14
        font-w-700
        color-darkAqua-placeholder
        color-brightPink-hover-placeholder
        font-nunito
        outline-none 
        text-right
        text-decoration-underline
    `,
    classNameIcon: `
        d-none
    `,
};

export const tolinkmeLang: InputTextClassProps = {
    classNameInput: `
        bg-transparent
        border-0
        width-p-100
        p-h-18
        font-14
        color-white
        color-white-placeholder
        font-nunito
        outline-none 
        opacity-0
        cursor-pointer
    `,
    classNameLabel: `
        pos-r
        width-49
        d-block
    `,
    classNameError: `
        d-none
    `,
    classNameIcon: `
        d-none
    `,
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        border-0
        border-radius-10
        font-14
        z-index-9
        overflow-auto
        box-shadow 
        box-shadow-x-0 
        box-shadow-y-3 
        box-shadow-b-6 
        box-shadow-s-0 
        box-shadow-black-16 
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        color-white-hover
        bg-whiteTwo-hover
        text-center
    `,
    classNameOptionDisabled: `
        d-none
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        d-none
    `,
};

export const tolinkmeLang2: InputTextClassProps = {
    classNameInput: `
        bg-transparent
        border-0
        flex 
        width-p-100
        font-14
        color-white
        color-white-placeholder
        font-nunito
        outline-none 
        opacity-0
        cursor-pointer
    `,
    classNameLabel: `
        pos-r
        width-49
        d-block
    `,
    classNameError: `
        d-none
    `,
    classNameIcon: `
        d-none
    `,
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        border-0
        border-radius-10
        font-14
        z-index-9
        overflow-auto
        box-shadow-black 
        box-shadow box-shadow-inset box-shadow-s-2
        box-shadow-transparent box-shadow-c-transparent
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        color-white-hover
        bg-whiteTwo-hover
        text-center
    `,
    classNameOptionDisabled: `
        d-none
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        d-none
    `,
};

export const tolinkme10: InputTextClassProps = {
    classNameInput: `
        box-shadow-black 
        bg-black-144
        box-shadow box-shadow-inset box-shadow-s-2
        box-shadow-transparent box-shadow-c-transparent
        border-radius-11
        border-0
        width-p-100
        p-v-20
        p-h-18
        font-14
        color-black
        font-nunito
        outline-none 
    `,
    classNameLabel: `
        pos-r
        ${BaseStyle.classNameLabel}
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-black
    `,
    classNameIcon: `
        color-greyBrowd color-black
        flex
        flex-align-center
        
    `,
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-14
        z-index-5
        overflow-auto
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-capitalize
    `,
    classNameOptionDisabled: `
        ${BaseStyle.classNameOptionDisabled}
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        pos-a
        top-0
        right-10
        bottom-0
        m-auto
        color-black
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoaderValidate: (
        <>
            <Loader size={40} />
        </>
    ),
};
export const tolinkme11: InputTextClassProps = {
    classNameInput: `
        width-p-100
        bg-transparent
        font-17 
        font-w-700 
        font-nunito
        outline-none 
        border-greyish
        color-greyish
        outline-none 
        
    `,
    classNameLabel: `
        font-17 font-w-900 font-nunito
        color-transparent
    `,
    classNameInputDisabled: `
    border-0
    `,
    classNameInputEnabled: `
        border-0
        border-style-solid
        border-b-1
        border-pinkishGreyThree
        color-transparent
        
        
    `,

    classNameError: `
        color-red
        font-nunito
        font-11
        text-right
        
    `,
};
export const tolinkme12: InputTextClassProps = {
    classNameInput: `
        box-shadow-black 
        bg-black-144
        box-shadow box-shadow-inset box-shadow-s-2
        box-shadow-transparent box-shadow-c-transparent
        border-radius-11
        border-0
        width-p-100
        p-v-20
        p-h-18
        font-14
        color-black
        font-nunito
        outline-none 
        m-b-10
    `,
    classNameLabel: `
        pos-r
        ${BaseStyle.classNameLabel}
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-black
    `,
    classNameIcon: `
        color-greyBrowd 
    pos-a
    top-0
    right-15
    bottom-0
    m-auto
        
    `,
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-14
        z-index-5
        overflow-auto
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-capitalize
    `,
    classNameOptionDisabled: `
        ${BaseStyle.classNameOptionDisabled}
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        pos-a
        top-0
        right-10
        bottom-0
        m-auto
        color-black
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoaderValidate: (
        <>
            <Loader size={40} />
        </>
    ),
};
export const tolinkme13: InputTextClassProps = {
    classNameInput: `
        box-shadow-black 
        bg-black-144
        box-shadow box-shadow-inset box-shadow-s-2
        box-shadow-transparent box-shadow-c-transparent
        border-radius-11
        border-0
        width-p-100
        p-v-20
        p-h-18
        font-14
        color-white
        font-nunito
        outline-none 
        m-b-10
        font-w-600
    `,
    classNameLabel: `
        pos-r
        ${BaseStyle.classNameLabel}
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-white
        font-w-600
    `,
    classNameIcon: `
        color-white
    pos-a
    top-0
    right-15
    bottom-0
    m-auto
        
    `,
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-14
        z-index-5
        overflow-auto
    `,
    classNameOption: `
        p-h-5
        p-v-10
        pointer
        list-none
        bg-gray-hover
        color-white-hover
        text-capitalize
    `,
    classNameOptionDisabled: `
        ${BaseStyle.classNameOptionDisabled}
    `,
    classNameContentInput: `
        pos-r
    `,
    classNameLoaderValidate: `
        pos-a
        top-0
        right-10
        bottom-0
        m-auto
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoaderValidate: (
        <>
            <Loader size={40} />
        </>
    ),
};

export const tolinkme14: InputTextClassProps = {
    classNameInput: `
        width-p-100
        font-12 font-w-900 font-nunito
        outline-none 
        border-greyish
        text-decoration-none
        color-grayDisabled  
        color-grayDisabled-placeholder
    `,

    classNameLabel: `
        font-12 font-w-900 font-nunito
        color-grayAcd
    `,

    classNameInputDisabled: `
        border-0
    `,
    classNameInputEnabled: `
        border-0
        border-style-solid
        border-b-1
        border-pinkishGreyThree
        
    `,

    classNameError: `
        color-red
        font-nunito
        font-11
        text-right
        
    `,
};
