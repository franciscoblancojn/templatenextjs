import { Story, Meta } from "@storybook/react";

import { InputTextProps, InputText } from "./index";

export default {
    title: "Input/InputText",
    component: InputText,
} as Meta;

const Template: Story<InputTextProps> = (args) => (
    <InputText {...args}>Test Children</InputText>
);

export const Index = Template.bind({});
Index.args = {
    label: "Label Test",
    onChangeValidate(e) {
        if(e == "error"){
            throw {
                message:"Error"
            }
        }
    },
};
