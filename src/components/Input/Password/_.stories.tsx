import { Story, Meta } from "@storybook/react";

import { InputPasswordProps, InputPassword } from "./index";

export default {
    title: "Input/InputPassword",
    component: InputPassword,
} as Meta;

const Template: Story<InputPasswordProps> = (args) => (
    <InputPassword {...args}>Test Children</InputPassword>
);

export const Index = Template.bind({});
Index.args = {
    label: "Label Test",
};
