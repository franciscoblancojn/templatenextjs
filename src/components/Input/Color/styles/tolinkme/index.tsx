import { InputColorClassProps } from '@/components/Input/Color/Base';
import Color from '@/svg/Color';

export const tolinkme: InputColorClassProps = {
    classNameContent: `
        pos-r
        pointer
        flex
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        border-radius-100
    `,
    styleContent: {
        width: 'min(100%,4.5rem)',
    },
    classNameInput: `
        pos-a    
        inset-0
        width-p-100
        height-p-100
        pointer
        opacity-0
    `,
    classNameColorShow: `
        pos-a    
        inset-0
        width-p-100
        height-p-100
        pointer
        border-radius-100
    `,
};

export const tolinkme2: InputColorClassProps = {
    classNameContent: `
        pos-r
        pointer
        flex
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        border-radius-7
        width-p-100
        height-64
        overflow-hidden
    `,
    classNameInput: `
        pos-a    
        inset-0
        width-p-100
        height-p-100
        pointer
        opacity-0
    `,
    classNameColorShow: `
        pos-a    
        inset-0
        width-p-100
        height-p-100
        pointer
    `,
    icon: (
        <>
            <div
                className={`
            pos-a
            z-index-1
            inset-0
            m-auto
            width-40
            height-40
            color-white
            flex
            flex-align-center
            flex-justify-center
            bg-black-16
            border-radius-100
        `}
            >
                <Color size={20} />
            </div>
        </>
    ),
};
