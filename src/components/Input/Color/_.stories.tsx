import { Story, Meta } from "@storybook/react";

import { InputColor, InputColorProps } from "./index";

export default {
    title: "Input/InputColor",
    component: InputColor,
} as Meta;

const Template: Story<InputColorProps> = (args) => (
    <InputColor {...args}>Test Children</InputColor>
);

export const Index = Template.bind({});
Index.args = {
    
};
