import { InputUploadClassProps } from '@/components/Input/Upload/Base';
import { Trash } from '@/svg/trash';
import { Loader } from '@/svg/loader';
import Upload from '@/svg/Upload';

export const mooveri: InputUploadClassProps = {
    classNameContent: `
        pos-r
        pointer
        flex
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        border-radius-7
        width-p-100
        height-64
        overflow-hidden
        border-2
        border-style-dashed
        border-black16
    `,
    classNameContentImg: `
        pos-r
        pointer
        flex
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        border-radius-7
        width-p-100
        height-64
    `,
    classNameContentIcon: ` 
        pos-a
        z-index-1
        inset-0
        m-auto
        flex
        flex-align-center
        flex-justify-center
        flex-gap-column-15
        cursor-pointer
        color-independence
        font-nunito
        font-w-700
    `,
    icon: (
        <>
            <div
                className={`
                    width-40
                    height-40
                    color-white
                    flex
                    flex-align-center
                    flex-justify-center
                    bg-black-16
                    border-radius-100
                `}
            >
                <Upload size={20} />
            </div>
        </>
    ),
    classNameRemove: `
        pos-a
        inset-0
        m-auto
        bg-black-50
        opacity-0
        opacity-10-hover
        color-white
        flex
        flex-align-center
        flex-justify-center
        flex-column
        flex-nowrap
        overflow-hidden
        pointer
    `,
    classNameNameFile: `
        color-independence
        font-nunito
        font-w-700
        flex
        flex-align-center
        flex-justify-center
        text-center
        width-p-100
        p-5
    `,
    classNameRemoveMovil: `
        d-sm-none  
    `,
    classNameRemoveDesktop: `
        d-none d-sm-block  
    `,
    iconDelete: (
        <>
            <Trash size={20} />
        </>
    ),
    iconChange: (
        <>
            <span className="font-nunito color-white font-40 line-h-10 cursor-pointer">
                +
            </span>
        </>
    ),
    classNameRemoveTopActive: `
        transform-translate-X-p-0
    `,
    classNameRemoveTopNotActive: `
        transform-translate-X-p--100
    `,
    classNameRemoveTop: `
        bg-brightSkyBlue
        pos-a
        top-0
        left-0
        width-p-50
        height-p-100
        flex
        flex-align-center
        flex-justify-center
        transform
        transition-5
    `,
    classNameRemoveDownActive: `
        transform-translate-X-p-0
    `,
    classNameRemoveDownNotActive: `
        transform-translate-X-p-100
    `,
    classNameRemoveDown: `
        bg-red
        pos-a
        top-0
        left-p-50
        width-p-50
        height-p-100
        flex
        flex-align-center
        flex-justify-center
        transform
        transition-5
    `,
    classNameLoader: `
        pos-a
        inset-0
        m-auto
        bg-black
        opacity-5
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoader: (
        <>
            <Loader size={200} />
        </>
    ),
};

export const mooveriBackoffice: InputUploadClassProps = {
    classNameContent: `
        pos-r
        pointer
        flex
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        border-radius-7
        width-p-100
        height-64
        overflow-hidden
        border-2
        border-style-dashed
        border-black16
    `,
    classNameContentImg: `
        pos-r
        pointer
        flex
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        border-radius-7
        width-p-100
        height-64
    `,
    classNameContentIcon: ` 
        pos-a
        z-index-1
        inset-0
        m-auto
        flex
        flex-align-center
        flex-justify-center
        flex-gap-column-15
        cursor-pointer
        color-white
        font-nunito
        font-w-700
    `,
    icon: (
        <>
            <div
                className={`
                    width-40
                    height-40
                    color-white
                    flex
                    flex-align-center
                    flex-justify-center
                    bg-black-16
                    border-radius-100
                `}
            >
                <Upload size={20} />
            </div>
        </>
    ),
    classNameRemove: `
        pos-a
        inset-0
        m-auto
        bg-black-50
        opacity-0
        opacity-10-hover
        color-white
        flex
        flex-align-center
        flex-justify-center
        flex-column
        flex-nowrap
        overflow-hidden
        pointer
    `,
    classNameNameFile: `
        color-white
        font-nunito
        font-w-700
        flex
        flex-align-center
        flex-justify-center
        text-center
        width-p-100
        p-5
    `,
    classNameRemoveMovil: `
        d-sm-none  
    `,
    classNameRemoveDesktop: `
        d-none d-sm-block  
    `,
    iconDelete: (
        <>
            <Trash size={20} />
        </>
    ),
    iconChange: (
        <>
            <span className="font-nunito color-white font-40 line-h-10 cursor-pointer">
                +
            </span>
        </>
    ),
    classNameRemoveTopActive: `
        transform-translate-X-p-0
    `,
    classNameRemoveTopNotActive: `
        transform-translate-X-p--100
    `,
    classNameRemoveTop: `
        bg-brightSkyBlue
        pos-a
        top-0
        left-0
        width-p-50
        height-p-100
        flex
        flex-align-center
        flex-justify-center
        transform
        transition-5
    `,
    classNameRemoveDownActive: `
        transform-translate-X-p-0
    `,
    classNameRemoveDownNotActive: `
        transform-translate-X-p-100
    `,
    classNameRemoveDown: `
        bg-red
        pos-a
        top-0
        left-p-50
        width-p-50
        height-p-100
        flex
        flex-align-center
        flex-justify-center
        transform
        transition-5
    `,
    classNameLoader: `
        pos-a
        inset-0
        m-auto
        bg-black
        opacity-5
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoader: (
        <>
            <Loader size={200} />
        </>
    ),
};
