import { PopupAddonsClassProps } from '@/components/Input/PopupAddons/Base';

export const tolinkme: PopupAddonsClassProps = {
    classNameContent: `
    `,

    classNameSelect: `
        flex
        flex-align
        font-16
        font-w-900
        font-nunito
        color-greyish
        cursor-pointer
        
    `,

    PopupProps: {
        styleTemplate: 'tolinkme3',
    },

    classNameGoBack: `
    `,
};
