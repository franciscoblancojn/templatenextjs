import { Popup, PopupProps } from '@/components/Popup';
import { useMemo, useState } from 'react';
import { useData } from 'fenextjs-hook/cjs/useData';
import ContentWidth from '@/components/ContentWidth';
import { useLang } from '@/lang/translate';
import Text from '@/components/Text';
import InputCheckbox from '../Checkbox';
import { Addons } from '@/interfaces/Addons';
import AddonsForm from '@/components/AddonsForm';

export interface PopupAddonsClassProps {
    classNameContent?: string;
    classNameSelect?: string;
    PopupProps?: PopupProps;
    PopupConfigProps?: PopupProps;
    classNameGoBack?: string;
}

export interface PopupAddonsBaseProps {
    defaultValue?: Addons;
    onChange?: (data: Addons) => void;
}

export interface PopupAddonsProps
    extends PopupAddonsClassProps,
        PopupAddonsBaseProps {}

export const PopupAddonsBase = ({
    classNameContent = '',
    classNameSelect = '',
    PopupProps = {},
    PopupConfigProps = {},
    classNameGoBack = '',
    defaultValue,
    onChange,
}: PopupAddonsProps) => {
    const _t = useLang();

    const [activePopup, setActivePopup_] = useState(false);
    const { data, onChangeData } = useData<Addons>(defaultValue || {}, {
        onChangeDataAfter: (d) => {
            if (onChange) {
                onChange(d);
            }
        },
    });
    const setActivePopup = (a: boolean) => {
        if (!a) {
            if (data === undefined) {
                return;
            }
        }
        setActivePopup_(a);
    };

    const textBtn = useMemo(() => {
        if (data?.fromActive) {
            return _t('Form settings');
        }
        return _t('Select Addons');
    }, [data?.fromActive, _t]);

    const textBtnConfig = useMemo(() => {
        if (data?.fromActive) {
            return _t('Form settings');
        }
        return _t('Message form');
    }, [data?.fromActive, _t]);

    return (
        <>
            <div className={classNameContent}>
                <Popup
                    {...PopupProps}
                    btnSwActive={activePopup}
                    setBtnSwActive={setActivePopup}
                    btn={<div className={`${classNameSelect} `}>{textBtn}</div>}
                    goBackProps={{
                        text: `${_t('Back')}`,
                        className: classNameGoBack,
                    }}
                >
                    <ContentWidth size={900}>
                        <Text styleTemplate="tolinkme27">
                            {_t('Type Addons')}
                        </Text>

                        <div className="flex flex-justify-between">
                            <div className={`${classNameSelect} width-p-40`}>
                                <InputCheckbox
                                    label={textBtnConfig}
                                    styleTemplate="tolinkme14"
                                    checked={data?.fromActive}
                                    onChange={onChangeData('fromActive')}
                                    defaultValue={data?.fromActive}
                                />
                            </div>
                            <div className="width-p-60">
                                {data?.fromActive && (
                                    <Popup
                                        {...PopupConfigProps}
                                        btnSwActive={true}
                                        btn={<></>}
                                        goBackProps={{
                                            text: `${_t('Back')}`,
                                            className: classNameGoBack,
                                        }}
                                        styleTemplate="tolinkmeAddons"
                                    >
                                        <div className="d-block">
                                            <Text styleTemplate="tolinkme27">
                                                {_t('Message form')}
                                            </Text>
                                            <AddonsForm
                                                defaultValue={data?.form}
                                                onChange={onChangeData('form')}
                                            />
                                        </div>
                                    </Popup>
                                )}
                            </div>
                        </div>
                    </ContentWidth>
                </Popup>
            </div>
        </>
    );
};

export default PopupAddonsBase;
