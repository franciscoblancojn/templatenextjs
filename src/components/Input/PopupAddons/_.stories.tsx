import { Story, Meta } from "@storybook/react";

import { PopupAddonsProps, PopupAddons } from "./index";

export default {
    title: "Input/PopupAddons",
    component: PopupAddons,
} as Meta;

const PopupAddonsIndex: Story<PopupAddonsProps> = (args) => (
    <PopupAddons {...args}>Test Children</PopupAddons>
);

export const Index = PopupAddonsIndex.bind({});
Index.args = {};
