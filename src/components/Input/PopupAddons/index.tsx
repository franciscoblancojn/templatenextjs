import { useMemo } from 'react';

import * as styles from '@/components/Input/PopupAddons/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    PopupAddonsBaseProps,
    PopupAddonsBase,
} from '@/components/Input/PopupAddons/Base';

export const PopupAddonsStyle = { ...styles } as const;

export type PopupAddonsStyles = keyof typeof PopupAddonsStyle;

export interface PopupAddonsProps extends PopupAddonsBaseProps {
    styleTemplate?: PopupAddonsStyles | ThemesType;
}

export const PopupAddons = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PopupAddonsProps) => {
    const Style = useMemo(
        () =>
            PopupAddonsStyle[styleTemplate as PopupAddonsStyles] ??
            PopupAddonsStyle._default,
        [styleTemplate]
    );

    return <PopupAddonsBase {...Style} {...props} />;
};
export default PopupAddons;
