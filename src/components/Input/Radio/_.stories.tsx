import { Story, Meta } from "@storybook/react";

import { InputRadioProps, InputRadio } from "./index";

export default {
    title: "Input/InputRadio",
    component: InputRadio,
} as Meta;

const Template: Story<InputRadioProps> = (args) => (
    <InputRadio {...args}>Test Children</InputRadio>
);

export const Index = Template.bind({});
Index.args = {
    options: [
        {
            label: "Label Test",
        },
        {
            label: "Label Test",
        },
        {
            label: "Label Test",
        },
        {
            label: "Label Test",
        },
    ],
};
