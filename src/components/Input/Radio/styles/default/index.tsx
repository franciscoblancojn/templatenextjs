import { InputRadioClassProps } from '@/components/Input/Radio/Base';

export const _default: InputRadioClassProps = {
    classNameLabel: `
        flex
        flex-align-center
        pointer
    `,
    classNameText: `
        font-20 font-montserrat
    `,
    classNameRadio: `
        flex
        width-10
        height-10
        m-h-5
        color-white
        p-1
        border-style-solid
        border-1
        border-radius-100
        border-black
    `,
    classNameRadioActive: `
        bg-black
    `,
};
