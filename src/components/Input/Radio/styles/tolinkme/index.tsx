import { InputRadioClassProps } from '@/components/Input/Radio/Base';
import SVGCheck from '@/svg/check';

export const tolinkme: InputRadioClassProps = {
    classNameLabel: `
        flex
        flex-align-center
        pointer
        width-p-100
        m-b-15
        flex-nowrap
        flex-gap-column-25
    `,
    classNameText: `
        font-20
        font-nunito
        width-p-90
    `,
    classNameRadio: `
        flex
        width-20
        height-20
        color-white
        p-5
        box-shadow
        box-shadow-black-50
        box-shadow-b-3

        border-radius-100
    `,
    classNameRadioActive: `
        bg-gradient-darkAqua-brightPink
    `,
    iconCheck: <SVGCheck size={20} />,
    classNameTitleOption: `
        font-20
        font-nunito
        font-w-900
        m-b-20
    `,
    classNameFirstTitleOption: `
        
    `,
    classNameAnyTitleOption: `
        m-t-35
    `,
};

export const tolinkme2: InputRadioClassProps = {
    ...tolinkme,
    classNameLabel: `
        flex
        flex-align-center
        pointer
        width-p-100
        m-b-15
        flex-nowrap
        flex-gap-column-25

        flex-row-reverse
        flex-justify-right
    `,
    classNameRadio: `
        flex
        width-20
        height-20
        color-white
        p-5
        box-shadow
        box-shadow-black-50
        box-shadow-b-3
        border-radius-100
    `,
    classNameRadioActive: `
        bg-gradient-darkAqua-brightPink
    `,
    classNameRadioInactive: `
        d-none
    `,
};

export const tolinkme3: InputRadioClassProps = {
    ...tolinkme,
    classNameLabel: `
        flex
        flex-align-center
        pointer
        width-p-50
        m-b-15
        flex-nowrap
        flex-gap-column-25
        font-14
        font-nunito
        color-reddishGrey
        p-b-11
        p-t-12
    `,
    classNameText: `
        font-14
        font-nunito
        width-p-100
        font-w-900
    `,
    classNameRadio: `
        flex
        width-15
        height-15
        color-white
        border-radius-3
    `,
    classNameRadioActive: `
        border-brightPink
        border-style-solid
        border-3
        font-14
        font-w-900
        font-nunito
        color-brightPink
        aspect-ratio-1-1
    `,
    classNameRadioInactive: `
    border-warmGreyThree
    border-style-solid
    border-3
    font-14
    font-nunito
    p-5
    aspect-ratio-1-1
    `,
};
export const tolinkme4: InputRadioClassProps = {
    ...tolinkme3,
    classNameLabel: `
        flex
        flex-align-center
        pointer
        m-b-15
        flex-nowrap
        flex-gap-column-5
        font-14
        font-nunito
        color-greyish
        p-b-11
        p-t-12
    `,
};

export const tolinkmeColumn: InputRadioClassProps = {
    classNameLabel: `
        flex
        flex-column-reverse
        flex-align-center
        pointer
        width-p-100
        m-b-15
        flex-gap-row-15
    `,
    classNameText: `
        font-10
        font-nunito
        width-p-90
    `,
    classNameRadio: `
        flex
        width-20
        height-20
        color-white
        p-5
        box-shadow
        box-shadow-black-50
        box-shadow-b-3

        border-radius-100
    `,
    classNameRadioActive: `
        bg-gradient-darkAqua-brightPink
    `,
    iconCheck: <SVGCheck size={20} />,
};
export const tolinkme5: InputRadioClassProps = {
    ...tolinkme,
    classNameText: `
    font-14
    font-nunito
    width-p-100
    font-w-700
    `,
};
