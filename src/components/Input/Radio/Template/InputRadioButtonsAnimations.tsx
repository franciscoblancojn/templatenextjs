import { useLang } from '@/lang/translate';
import { InputRadio, InputRadioProps } from '..';
import {
    AnimationsList,
    AnimationsListType,
    AnimationsListConst,
} from '@/interfaces/Animations';
import Text from '@/components/Text';
import { useMemo } from 'react';

export interface InputRadioButtonsAnimationsProps
    extends Omit<InputRadioProps, 'options' | 'onChange'> {
    onChange?: (value: AnimationsListType) => void;
    defaultValue?: AnimationsListType;
}

export const InputRadioButtonsAnimations = ({
    defaultValue = 'none',
    ...props
}: InputRadioButtonsAnimationsProps) => {
    const _t = useLang();
    const options = useMemo(
        () =>
            AnimationsList.map((value, i) => ({
                label: (
                    <div className="text-capitalize" key={i}>
                        {_t(value.split('-').join(' '))}
                    </div>
                ),
            })),
        [_t]
    );

    return (
        <>
            <Text styleTemplate="tolinkme3">{_t('Animation')}</Text>
            <div className="grid grid-force grid-columns-3 gap-10">
                <InputRadio
                    {...props}
                    options={options}
                    onChange={(e) => {
                        props?.onChange?.(AnimationsListConst?.[e]);
                    }}
                    styleTemplate="tolinkme4"
                    selectedRadioDefault={AnimationsList.findIndex(
                        (e) => e == _t(defaultValue)
                    )}
                    checked={true}
                />
            </div>
        </>
    );
};
