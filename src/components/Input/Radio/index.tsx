import { useEffect, useMemo, useState } from 'react';

import {
    InputRadioBase,
    InputRadioBaseProps,
} from '@/components/Input/Radio/Base';

import * as styles from '@/components/Input/Radio/styles';
import log from '@/functions/log';
import Theme, { ThemesType } from '@/config/theme';
import { useLang } from '@/lang/translate';

export const InputRadioStyle = { ...styles } as const;
export type InputRadioStyles = keyof typeof InputRadioStyle;

export interface InputRadioOptions {
    label: any;
    title?: string;
}
export interface InputRadioProps extends InputRadioBaseProps {
    styleTemplate?: InputRadioStyles | ThemesType;
    selectedRadioDefault?: number;
    options: InputRadioOptions[];
    onChange: (e: number) => void;
}

export const InputRadio = ({
    styleTemplate = Theme.styleTemplate ?? '_default',
    selectedRadioDefault = 0,
    options = [],
    onChange = (e) => {
        log('onChange', e);
    },
    keyChecked = -1,
    disabled = false,
    ...props
}: InputRadioProps) => {
    const _t = useLang();
    const [selectedRadio, setSelectedRadio] = useState(
        selectedRadioDefault ?? 0
    );

    const selectRadio = (key: number) => {
        if (disabled) {
            return;
        }
        onChange(key);
        setSelectedRadio(key);
    };

    const config = {
        ...(InputRadioStyle[styleTemplate as InputRadioStyles] ??
            InputRadioStyle._default),
        ...props,
    };

    useEffect(() => {
        if (keyChecked > -1 && keyChecked != selectedRadio) {
            setSelectedRadio(keyChecked);
        }
    }, [keyChecked, _t]);

    const C = useMemo(
        () => (
            <>
                {options.map((option, i) => (
                    <>
                        {option.title ? (
                            <>
                                <div
                                    className={`${
                                        config.classNameTitleOption ?? ''
                                    } ${
                                        i == 0
                                            ? config.classNameFirstTitleOption ??
                                              ''
                                            : config.classNameAnyTitleOption ??
                                              ''
                                    }`}
                                    key={i}
                                >
                                    {_t(option.title ?? '')}
                                </div>
                            </>
                        ) : (
                            <InputRadioBase
                                {...config}
                                {...option}
                                key={i}
                                i={i}
                                onChange={selectRadio}
                                checked={selectedRadio == i}
                            />
                        )}
                    </>
                ))}
            </>
        ),
        [options, selectedRadio, _t]
    );

    return C;
};
export default InputRadio;
