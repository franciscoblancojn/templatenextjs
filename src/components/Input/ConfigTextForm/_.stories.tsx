import { Story, Meta } from "@storybook/react";

import { ConfigTextFormProps, ConfigTextForm } from "./index";

export default {
    title: "Input/ConfigTextForm",
    component: ConfigTextForm,
} as Meta;

const ConfigTextFormIndex: Story<ConfigTextFormProps> = (args) => (
    <ConfigTextForm {...args}>Test Children</ConfigTextForm>
);

export const Index = ConfigTextFormIndex.bind({});
Index.args = {};
