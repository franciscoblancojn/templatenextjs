import { useMemo } from 'react';

import * as styles from '@/components/Input/ConfigTextForm/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigTextFormBaseProps,
    ConfigTextFormBase,
} from '@/components/Input/ConfigTextForm/Base';

export const ConfigTextFormStyle = { ...styles } as const;

export type ConfigTextFormStyles = keyof typeof ConfigTextFormStyle;

export interface ConfigTextFormProps extends ConfigTextFormBaseProps {
    styleTemplate?: ConfigTextFormStyles | ThemesType;
}

export const ConfigTextForm = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigTextFormProps) => {
    const Style = useMemo(
        () =>
            ConfigTextFormStyle[styleTemplate as ConfigTextFormStyles] ??
            ConfigTextFormStyle._default,
        [styleTemplate]
    );

    return <ConfigTextFormBase {...Style} {...props} />;
};
export default ConfigTextForm;
