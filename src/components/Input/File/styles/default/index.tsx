import { InputFileClassProps } from '../../Base';

export const _default: InputFileClassProps = {
    classNameLabel: `
    
    `,
    classNameLabelUpload: `
      bg-black  
    `,
    classNameInput: `
        
    `,
    classNameError: `
        
    `,
};

export const _defaultBtnFile: InputFileClassProps = {
    classNameLabel: `
        pos-a
        top-0
        left-0
        width-p-100
        height-p-100
        flex
        cursor-pointer
        opacity-0
    `,
    classNameInput: `
        pos-a
        top-0
        left-0
        width-p-100
        height-p-100
        flex
        cursor-pointer
    `,
    classNameError: `
        
    `,
};
