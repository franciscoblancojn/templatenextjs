import { InputFileClassProps } from '../../Base';

export const tolinkme: InputFileClassProps = {
    classNameLabel: `
        flex-justify-center
        flex
        text-center
        cursor-pointer
        border-radius-20
        border-3
        border-style-dashed
        border-spanishGray3
        width-p-100
        p-v-20
    `,
    classNameInput: `
        
    `,
    classNameError: `
        color-red
        font-w-800
        m-auto
        flex
        flex-justify-center
        p-t-10
    `,
    classNameLabelUpload: `
        
        input-file-svg-upload
    `,
    classNameLabelNotUpload: `
        bg-pinkishGreyThree
    `,
    classNameLabelError: `
        input-file-error-upload
    `,
    classNameContentProgress: `
    m-t-10
    `,
};

export const tolinkme2: InputFileClassProps = {
    classNameLabel: `
    `,
    classNameInput: `

    `,
    classNameError: `
        
    `,
};

export const tolinkme3: InputFileClassProps = {
    classNameLabel: `
    pos-r
    width-p-100
    `,
    classNameInput: `
    pos-a
    inset-0
    width-p-100
    height-p-100
    z-index-2
        d-block
        opacity-0
        cursor-pointer
    `,
    classNameError: `
        
    `,
    classNameContentProgress: `
        m-t-10
    `,
};
