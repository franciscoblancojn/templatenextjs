import { Story, Meta } from "@storybook/react";

import { PopupMonetizeProps, PopupMonetize } from "./index";

export default {
    title: "Input/PopupMonetize",
    component: PopupMonetize,
} as Meta;

const PopupMonetizeIndex: Story<PopupMonetizeProps> = (args) => (
    <PopupMonetize {...args}>Test Children</PopupMonetize>
);

export const Index = PopupMonetizeIndex.bind({});
Index.args = {};
