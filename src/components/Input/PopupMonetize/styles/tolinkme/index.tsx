import { PopupMonetizeClassProps } from '@/components/Input/PopupMonetize/Base';

export const tolinkme: PopupMonetizeClassProps = {
    classNameContent: `
    `,

    classNameSelect: `
        flex
        flex-align
        font-16
        font-w-900
        font-nunito
        color-greyish
        cursor-pointer
        
    `,

    PopupProps: {
        styleTemplate: 'tolinkme3',
    },
    classNameItemRs: `
        flex
        flex-align
        font-20
        font-w-900
        font-nunito
        cursor-pointer
    `,
    classNameItemRsActive: `
        bg-gradient-darkAqua-brightPink-
        text-bg-
        color-brightPink2
    `,
    classNameItemRsNoActive: `
        color-greyish
    `,
    styleTemplateInputRadio: 'tolinkme2',

    classNameGoBack: `
    `,

    classNameContentSearch: `
        pos-sk
        top-20
        bg-white  
        box-shadow
        box-shadow-c-white
        box-shadow-y-10
    `,
    SearchProps: {
        styleTemplate: 'tolinkme',
    },

    ContentBtnProps: {
        size: 200,
        className: `
            m-h-auto
            pos-sk
            top-p-80
            left-0
            right-0
            z-index-1
            height-0
        `,
    },
    classNameContentBtn: `
        transform
        transition-5
    `,
    classNameContentBtnActive: `
        transform-translate-X-p-0
    `,
    classNameContentBtnInactive: `
        transform-translate-X-vw-100-
    `,
    styleTemplateButton: 'tolinkme',
};
