# PopupMonetize

## Dependencies

[PopupMonetize](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/PopupMonetize)

```js
import { PopupMonetize } from '@/components/Input/PopupMonetize';
```

## Import

```js
import {
    PopupMonetize,
    PopupMonetizeStyles,
} from '@/components/Input/PopupMonetize';
```

## Props

```tsx
interface PopupMonetizeProps {}
```

## Use

```js
<PopupMonetize>PopupMonetize</Input/PopupMonetize>
```
