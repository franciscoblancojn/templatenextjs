import { BaseStyle, InputRangeClassProps } from '@/components/Input/Range/Base';

export const _default: InputRangeClassProps = {
    classNameInput: `
    ${BaseStyle.classNameInput}
    bg-gray
`,
    classNameInputBox: `
    ${BaseStyle.classNameInputBox}
    bg-white
`,
    classNameLabel: `
    ${BaseStyle.classNameLabel}
`,
    classNameMinMax: `
    ${BaseStyle.classNameMinMax}
`,
    classNameCurrentValue: `
    ${BaseStyle.classNameCurrentValue}
`,
    classNameContentInput: `
    ${BaseStyle.classNameContentInput}
`,
};
