import { InputRangeClassProps } from '@/components/Input/Range/Base';

export const tolinkme: InputRangeClassProps = {
    classNameInput: `
        width-p-100
        height-10
        pointer
        border-radius-5
        bg-gradient-darkAqua-brightPink
    `,
    classNameInputBox: `
        border-radius-100
        width-25
        height-25
        bg-white
        box-shadow
        box-shadow-black-50
        box-shadow-s-1
        box-shadow-b-5
    `,
    classNameLabel: `
        flex
        width-p-100
        flex-justify-center
    `,
    classNameMinMax: `
        font-montserrat
        font-16
        width-p-10
        text-center
    `,
    classNameCurrentValue: `
        font-nunito
        font-16
        font-w-700
        d-none
    `,
    classNameContentInput: `
        flex
        flex-column
        flex-align-center
        width-p-100
    `,
};
