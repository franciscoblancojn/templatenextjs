import log from '@/functions/log';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useMemo } from 'react';

export const BaseStyle = {
    classNameInput: `
        width-p-100
        height-8
        pointer
        border-radius-10
    `,
    classNameInputBox: `
        border-radius-100
        width-12
        height-12
        border-style-solid
        border-2
    `,
    classNameLabel: `
        flex
    `,
    classNameMinMax: `
        font-montserrat
        font-16
        width-p-10
        text-center
    `,
    classNameCurrentValue: `
        font-montserrat
        font-16
    `,
    classNameContentInput: `
        flex
        flex-column
        flex-align-center
        width-p-80
    `,
};

export interface InputRangeClassProps {
    classNameInput?: string;
    classNameInputBox?: string;
    classNameLabel?: string;
    classNameMinMax?: string;
    classNameCurrentValue?: string;
    classNameContentInput?: string;
}
export interface InputRangeBaseProps {
    min?: number;
    max?: number;
    showMinMax?: boolean;
    value?: number | null;
    defaultValue?: number | null;
    useValue?: boolean;
    onChange?: (value: number) => void;
}
export interface InputRangeProps
    extends InputRangeBaseProps,
        InputRangeClassProps {}

export const InputRangeBase = ({
    classNameInput = '',
    classNameInputBox = '',
    classNameLabel = '',
    classNameMinMax = '',
    classNameCurrentValue = '',
    classNameContentInput = '',
    min = -100,
    max = 100,
    showMinMax = false,
    value = null,
    defaultValue = null,
    useValue = false,
    onChange = (value) => {
        log('onChange', value);
    },
}: InputRangeProps) => {
    const { dataMemo: currentValue = min, onChangeData } = useData<
        {
            currentValue: number;
        },
        number
    >(
        {
            currentValue: defaultValue ?? min,
        },
        {
            onMemo: ({ currentValue }) => {
                return (useValue ? value : currentValue) ?? min;
            },
        }
    );
    const onChangeValue = (e: any) => {
        const newValue = e.target.value;
        onChangeData('currentValue')(newValue);
        onChange(newValue);
    };

    const box = useMemo(
        () => (
            <span
                className={`pos-a flex ${classNameInputBox}`}
                style={{
                    left: ((currentValue - min) / (max - min)) * 100 + '%',
                    transform: 'translate(-50%,-50%)',
                    top: '50%',
                }}
            />
        ),
        [currentValue, min, max, classNameInputBox]
    );

    return (
        <label className={classNameLabel}>
            {showMinMax && <span className={classNameMinMax}>{min}</span>}
            <div className={classNameContentInput}>
                <span className={classNameCurrentValue}>{currentValue}</span>
                <div className={`pos-r ${classNameInput}`}>
                    {box}
                    <input
                        type="range"
                        min={min}
                        max={max}
                        className="pos-a top-0 left-0 width-p-100 height-p-100 m-0 pointer opacity-0"
                        onChange={onChangeValue}
                        value={`${useValue ? value : currentValue}`}
                    />
                </div>
            </div>
            {showMinMax && <span className={classNameMinMax}>{max}</span>}
        </label>
    );
};
export default InputRangeBase;
