import { Story, Meta } from "@storybook/react";

import { InputRangeProps, InputRange } from "./index";

export default {
    title: "Input/InputRange",
    component: InputRange,
} as Meta;

const Template: Story<InputRangeProps> = (args) => (
    <InputRange {...args}>Test Children</InputRange>
);

export const Index = Template.bind({});
Index.args = {};
