import { Story, Meta } from "@storybook/react";

import { InputNumberProps, InputNumber } from "./index";

export default {
    title: "Input/InputNumber",
    component: InputNumber,
} as Meta;

const Template: Story<InputNumberProps> = (args) => (
    <InputNumber {...args}>Test Children</InputNumber>
);

export const Index = Template.bind({});
Index.args = {
    label: "Label Test",
};
