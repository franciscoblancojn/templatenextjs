# ConfigBorder

## Dependencies

[ConfigBorder](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/ConfigBorder)

```js
import { ConfigBorder } from '@/components/Input/ConfigBorder';
```

## Import

```js
import {
    ConfigBorder,
    ConfigBorderStyles,
} from '@/components/Input/ConfigBorder';
```

## Props

```tsx
interface ConfigBorderProps {}
```

## Use

```js
<ConfigBorder>ConfigBorder</Input/ConfigBorder>
```
