import { useMemo } from 'react';

import * as styles from '@/components/Input/ConfigBorder/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigBorderBaseProps,
    ConfigBorderBase,
} from '@/components/Input/ConfigBorder/Base';

export const ConfigBorderStyle = { ...styles } as const;

export type ConfigBorderStyles = keyof typeof ConfigBorderStyle;

export interface ConfigBorderProps extends ConfigBorderBaseProps {
    styleTemplate?: ConfigBorderStyles | ThemesType;
}

export const ConfigBorder = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigBorderProps) => {
    const Style = useMemo(
        () =>
            ConfigBorderStyle[styleTemplate as ConfigBorderStyles] ??
            ConfigBorderStyle._default,
        [styleTemplate]
    );

    return <ConfigBorderBase {...Style} {...props} />;
};
export default ConfigBorder;
