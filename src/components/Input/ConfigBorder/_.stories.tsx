import { Story, Meta } from "@storybook/react";

import { ConfigBorderProps, ConfigBorder } from "./index";

export default {
    title: "Input/ConfigBorder",
    component: ConfigBorder,
} as Meta;

const ConfigBorderIndex: Story<ConfigBorderProps> = (args) => (
    <ConfigBorder {...args}>Test Children</ConfigBorder>
);

export const Index = ConfigBorderIndex.bind({});
Index.args = {};
