import Theme, { ThemesType } from '@/config/theme';
import SVGSearch from '@/svg/search';
import { InputText, InputTextProps, InputTextStyles } from '../Text';

export interface InputSearchClassProps {
    styleTemplateInputText?: InputTextStyles | ThemesType;
    icon?: any;
}

export interface InputSearchBaseProps extends InputTextProps {}

export interface InputSearchProps
    extends InputSearchClassProps,
        InputSearchBaseProps {}

export const InputSearchBase = ({
    styleTemplateInputText = Theme.styleTemplate ?? '_default',

    icon = <SVGSearch size={26} />,

    placeholder = 'InputSearch',
    ...props
}: InputSearchProps) => {
    return (
        <>
            <InputText
                {...props}
                placeholder={placeholder}
                styleTemplate={styleTemplateInputText}
                icon={icon}
            />
        </>
    );
};
export default InputSearchBase;
