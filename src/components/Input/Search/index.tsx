import { useMemo } from 'react';

import * as styles from '@/components/Input/Search/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    InputSearchBaseProps,
    InputSearchBase,
} from '@/components/Input/Search/Base';
import { InputTextStyles } from '../Text';

export const InputSearchStyle = { ...styles } as const;

export type InputSearchStyles = keyof typeof InputSearchStyle;

export interface InputSearchProps extends InputSearchBaseProps {
    styleTemplate?: InputSearchStyles | ThemesType | InputTextStyles;
}

export const InputSearch = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputSearchProps) => {
    const Style = useMemo(
        () =>
            InputSearchStyle[styleTemplate as InputSearchStyles] ??
            InputSearchStyle._default,
        [styleTemplate]
    );

    return <InputSearchBase {...Style} {...props} />;
};
export default InputSearch;
