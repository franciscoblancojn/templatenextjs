import { Story, Meta } from "@storybook/react";

import { InputSearchProps, InputSearch } from "./index";

export default {
    title: "Input/InputSearch",
    component: InputSearch,
} as Meta;

const InputSearchIndex: Story<InputSearchProps> = (args) => (
    <InputSearch {...args}>Test Children</InputSearch>
);

export const Index = InputSearchIndex.bind({});
Index.args = {};
