# InputSearch

## Dependencies

[InputSearch](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/InputSearch)

```js
import { InputSearch } from '@/components/Input/InputSearch';
```

## Import

```js
import { InputSearch, InputSearchStyles } from '@/components/Input/InputSearch';
```

## Props

```tsx
interface InputSearchProps {}
```

## Use

```js
<InputSearch>InputSearch</Input/InputSearch>
```
