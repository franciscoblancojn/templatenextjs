import { useState, useEffect } from 'react';
import { phoneProps } from 'world-phones';

import {
    InputText,
    InputTextProps,
    InputTextStyles,
} from '@/components/Input/Text';
import { InputSelect } from '@/components/Input/Select';

import Theme, { ThemesType } from '@/config/theme';
import log from '@/functions/log';

import { PhoneProps } from '@/interfaces/Phone';

export interface InputTelClassProps {
    classNameContent?: string;
    styleTemplateSelect?: InputTextStyles | ThemesType;
    classNameContentLabel?: string;
    classNameContentSelect?: string;
    classNameContentInput?: string;
    styleTemplateInput?: InputTextStyles | ThemesType;
    classNameContentSelectImg?: string;
    classNameInputDisabled?: string;
    classNameInputEnabled?: string;
}

export type InputTelValue = PhoneProps;

export interface InputTelBaseProps
    extends Omit<InputTextProps, 'onChange' | 'defaultValue' | 'value'> {
    value?: PhoneProps;
    defaultValue?: InputTelValue;
    onChange?: (data: InputTelValue) => void;
}

export interface InputTelProps extends InputTelBaseProps, InputTelClassProps {}

export const InputTelBase = ({
    classNameContent = '',
    classNameContentLabel = '',
    classNameContentSelect = '',
    classNameContentInput = '',
    classNameContentSelectImg = '',

    styleTemplateSelect = Theme.styleTemplate ?? '_default',
    styleTemplateInput = Theme.styleTemplate ?? '_default',
    label,
    defaultValue,
    value,
    onChange = (data: InputTelValue) => {
        log('InputTelValue', data);
    },
    ...props
}: InputTelProps) => {
    const [codeSelect, setCodeSelect] = useState(
        value?.code ?? defaultValue?.code ?? ''
    );
    const [codeImgSelect, setCodeImgSelect] = useState(
        <div className="flex flex-align-center">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Colombia.svg/20px-Flag_of_Colombia.svg.png" />
            +57
        </div>
    );
    const [tel, setTel] = useState(value?.tel ?? defaultValue?.tel ?? '');
    const [tels, setTels] = useState<phoneProps[]>([
        {
            code: '+57',
            img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Colombia.svg/20px-Flag_of_Colombia.svg.png',
        },
    ]);
    const config = {
        ...props,
        label: '',
        defaultValue: defaultValue?.tel,
        onChangeValidate: (value: string) => {
            value = `${value}`.replaceAll(/[^\d+-]/g, '');
            return props?.onChangeValidate?.(value) ?? value;
        },
        onChange: (value: string) => {
            value = `${value}`.replaceAll(/[^\d+-]/g, '');
            setTel(value);
        },
    };
    const loadTels = async () => {
        const { phones } = await import('world-phones');
        setTels(phones);
    };
    useEffect(() => {
        onChange({
            code: codeSelect,
            tel,
        });
    }, [codeSelect, tel]);
    useEffect(() => {
        loadTels();
    }, []);
    return (
        <div className={classNameContent}>
            <div className={classNameContentLabel}>{label}</div>
            <div className={classNameContentSelect}>
                <InputSelect
                    disabled={props.disabled}
                    // value={value?.code ?? codeSelect ?? null}
                    icon={codeImgSelect}
                    placeholder="+57"
                    defaultValue={{
                        text: '+57',
                        value: '+57',
                        html: (
                            <div className={classNameContentSelectImg}>
                                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Colombia.svg/20px-Flag_of_Colombia.svg.png" />
                                +57
                            </div>
                        ),
                    }}
                    yup={null}
                    options={tels.map((e) => {
                        return {
                            text: e.code,
                            value: e.code,
                            html: (
                                <div className={classNameContentSelectImg}>
                                    <img src={e.img} />
                                    {e.code}
                                </div>
                            ),
                        };
                    })}
                    onChange={(e) => {
                        setCodeSelect(e.value);
                        setCodeImgSelect(e.html);
                    }}
                    styleTemplate={styleTemplateSelect}
                />
            </div>
            <div className={classNameContentInput}>
                <InputText
                    styleTemplate={styleTemplateInput}
                    {...config}
                    type="tel"
                    value={value?.tel ?? null}
                    yup={null}
                />
            </div>
            <style jsx global>
                {`
                    .contentSelectCode .icon {
                        position: absolute;
                        top: 0;
                        right: 0;
                        bottom: 0;
                        margin: auto;
                        padding: 0 0.6rem;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        cursor: pointer;
                    }
                    .contentSelectCode .icon span {
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }
                    .contentSelectCode li img {
                        margin-right: 1rem;
                    }
                `}
            </style>
        </div>
    );
};
export default InputTelBase;
