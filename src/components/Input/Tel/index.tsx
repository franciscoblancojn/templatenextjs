import { InputTelBase, InputTelBaseProps } from '@/components/Input/Tel/Base';
import * as styles from '@/components/Input/Tel/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InputTelStyle = { ...styles } as const;
export type InputTelStyles = keyof typeof InputTelStyle;

export interface InputTelProps extends InputTelBaseProps {
    styleTemplate?: InputTelStyles | ThemesType;
}

export const InputTel = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputTelProps) => {
    return (
        <>
            <InputTelBase
                {...props}
                {...(InputTelStyle[styleTemplate as InputTelStyles] ??
                    InputTelStyle._default)}
            />
        </>
    );
};
export default InputTel;
