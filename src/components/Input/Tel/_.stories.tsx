import { Story, Meta } from "@storybook/react";

import { InputTelProps, InputTel } from "./index";

export default {
    title: "Input/InputTel",
    component: InputTel,
} as Meta;

const Template: Story<InputTelProps> = (args) => (
    <InputTel {...args}>Test Children</InputTel>
);

export const Index = Template.bind({});
Index.args = {
    label: "Label Test",
    
};
