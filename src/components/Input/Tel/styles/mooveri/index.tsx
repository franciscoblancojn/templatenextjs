import { InputTelClassProps } from '@/components/Input/Tel/Base';

export const mooveri: InputTelClassProps = {
    classNameContent: `
        flex flex-gap-column-10 flex-nowrap
        border-0 

    `,
    styleTemplateSelect: 'mooveri',
    classNameContentLabel: `
        d-none
        font-nunito 
        font-w-800
        color-greyishBrown 
        font-16
    `,
    classNameContentSelect: `
        width-p-30
        contentSelectCode 
    
    `,
    classNameContentInput: `
        width-p-70 
        p-l-5"
    `,
    styleTemplateInput: `mooveri`,

    classNameContentSelectImg: `
        flex flex-align-center
        text-white-space-nowrap
        flex 
        flex-nowrap
    `,
};

export const mooveriEdit: InputTelClassProps = {
    ...mooveri,
    classNameContent: `
        flex
        flex-gap-10 
        flex-gap-column-10 
        border-0 
    `,
    classNameContentLabel: `
        font-13  
        font-w-900 
        color-greyishBrown
        m-r-auto  
        font-circular
        display-block
        width-p-100
    `,
    classNameContentSelect: `
        flex-4
        contentSelectCode 
    
    `,
    classNameContentInput: `
        flex-8
        p-l-5"
    `,
    styleTemplateSelect: 'mooveriEdit',
    styleTemplateInput: `mooveriEdit`,
};

export const mooveriSaveCancel: InputTelClassProps = {
    ...mooveriEdit,
    styleTemplateSelect: 'mooveriSaveCancel',
    styleTemplateInput: `mooveriSaveCancel`,
};

export const mooveriTelCompany: InputTelClassProps = {
    ...mooveri,
    classNameContent: `
        flex
        border-0 
    `,
    classNameContentLabel: `
        font-13  
        font-w-900 
        color-greyishBrown
        m-r-auto  
        font-circular
        display-block
        width-p-100
    `,
    classNameContentSelect: `
        flex-2
        contentSelectCode 
    
    `,
    classNameContentInput: `
        flex-10
    `,
    styleTemplateSelect: 'mooveriManage',
    styleTemplateInput: 'mooveriManage',
};

export const mooveriBackoffice: InputTelClassProps = {
    classNameContent: `
        flex flex-gap-column-10 flex-nowrap-
        border-0 
        flex-justify-between

    `,
    styleTemplateSelect: 'mooveri',
    classNameContentLabel: `
        font-nunito 
        font-w-800
        font-16
        font-16 font-nunito
        color-white
        width-p-100
    `,
    classNameContentSelect: `
        width-p-30
        contentSelectCode 
    
    `,
    classNameContentInput: `
        width-p-65 
        p-l-5"
    `,
    styleTemplateInput: `mooveri`,

    classNameContentSelectImg: `
        flex flex-align-center
        text-white-space-nowrap
        flex 
        flex-nowrap
    `,
};
