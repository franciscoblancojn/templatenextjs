import { InputTelClassProps } from '@/components/Input/Tel/Base';

export const tolinkme: InputTelClassProps = {
    classNameContent: `
        flex flex-gap-column-10 flex-nowrap
        border-0 

    `,
    styleTemplateSelect: 'tolinkme',
    classNameContentLabel: `
        d-none
        font-nunito 
        font-w-800
        color-greyishBrown 
        font-16
    `,
    classNameContentSelect: `
    width-p-30
    contentSelectCode 
    
    `,
    classNameContentInput: `
    width-p-70 
    p-l-5"
    `,
    styleTemplateInput: `tolinkme`,

    classNameContentSelectImg: `
    flex flex-align-center
    text-white-space-nowrap
    flex 
    flex-nowrap
    `,
};
export const tolinkme2: InputTelClassProps = {
    classNameContent: `
        flex border-0
    `,
    styleTemplateSelect: 'tolinkme',
    classNameContentLabel: `
        width-p-100
        font-nunito 
        font-w-800
        color-greyishBrown 
        font-16
    `,
};

export const tolinkme3: InputTelClassProps = {
    classNameContent: `
        flex border-0
    `,
    styleTemplateSelect: 'tolinkme6',
    classNameContentLabel: `
        width-p-100
        font-nunito 
        font-w-800
        color-greyishBrown 
        font-16
    `,
    classNameContentSelect: `
    width-p-8 
    
    contentSelectCode 
    
    `,
    classNameContentInput: `
    "width-p-20 
    p-l-5"
    `,
    styleTemplateInput: `tolinkme2`,
    classNameContentSelectImg: `
    flex flex-align-center
    `,
};

export const tolinkme4: InputTelClassProps = {
    classNameContent: `
        flex border-0
        flex-justify-between
    `,
    styleTemplateSelect: 'tolinkme6',
    classNameContentLabel: `
        width-p-100
        font-nunito 
        font-w-800
        color-greyishBrown 
        font-16
    `,
    classNameContentSelect: `
    width-p-30
    contentSelectCode 
    
    `,
    classNameContentInput: `
    width-p-70 
    p-l-5"
    `,
    styleTemplateInput: `tolinkme2`,

    classNameContentSelectImg: `
    flex flex-align-center
    `,
    classNameInputDisabled: `
    border-0
    `,
    classNameInputEnabled: `
    border-0
    border-style-solid
    border-b-1
    border-pinkishGreyThree
`,
};

export const tolinkme5: InputTelClassProps = {
    classNameContent: `
        flex border-0
        flex-justify-between
    `,
    styleTemplateSelect: 'tolinkme6',
    classNameContentLabel: `
        width-p-100
        font-14 font-w-900 font-nunito
        color-greyishBrown 
        pos-r

    `,
    classNameContentSelect: `
    width-p-30
    contentSelectCode 
    
    `,
    classNameContentInput: `
    width-p-70 
    p-l-5"
    `,
    styleTemplateInput: `tolinkme2`,

    classNameContentSelectImg: `
    flex flex-align-center
    `,
    classNameInputDisabled: `
    border-0
    `,
    classNameInputEnabled: `
    border-0
    border-style-solid
    border-b-1
    border-pinkishGreyThree
`,
};
export const tolinkme6: InputTelClassProps = {
    classNameContent: `
        flex flex-gap-column-10 flex-nowrap
        border-0 

    `,
    styleTemplateSelect: 'tolinkme10',
    classNameContentLabel: `
        d-none
        font-nunito 
        font-w-800
        color-greyishBrown 
        font-16
    `,
    classNameContentSelect: `
    width-p-30
    contentSelectCode 
    
    `,
    classNameContentInput: `
    width-p-70 
    p-l-5"
    `,
    styleTemplateInput: `tolinkme10`,

    classNameContentSelectImg: `
    flex flex-align-center
    text-white-space-nowrap
    flex 
    flex-nowrap
    `,
};
