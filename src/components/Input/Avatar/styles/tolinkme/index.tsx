import { InputAvatarClassProps } from '@/components/Input/Avatar/Base';
import { Trash } from '@/svg/trash';
import { Loader } from '@/svg/loader';

export const tolinkme: InputAvatarClassProps = {
    classNameContent: `
        pos-r
        pointer
    `,
    styleContent: {
        width: 'min(100%,8rem)',
    },
    classNameContentImg: `
        pos-r
        border-radius-100
        overflow-hidden
        box-shadow
        box-shadow-inset
        box-shadow-s-3
        box-shadow-c-whiteThree
        bg-whiteThree
        pointer
        aspect-ratio-1-1
        width-p-100
    `,
    styleContentImg: {},
    classNameImg: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        object-fit-cover
        pointer
    `,
    img: 'uploadAvatar.png',
    styleTemplateImg: 'tolinkme',
    classNameRemove: `
        pos-a
        inset-0
        m-auto
        bg-black-50
        opacity-0
        opacity-10-hover
        color-white
        flex
        flex-align-center
        flex-justify-center
        flex-column
        flex-nowrap
        overflow-hidden
        pointer
    `,
    classNameRemoveMovil: `
        d-sm-none  
    `,
    classNameRemoveDesktop: `
        d-none d-sm-block  
    `,
    iconDelete: (
        <>
            <Trash size={20} />
        </>
    ),
    iconChange: (
        <>
            <span className="font-nunito color-white font-40 line-h-10 cursor-pointer">
                +
            </span>
        </>
    ),
    classNameRemoveTopActive: `
        transform-translate-Y-p-0
    `,
    classNameRemoveTopNotActive: `
        transform-translate-Y-p--100
    `,
    classNameRemoveTop: `
        bg-brightSkyBlue
        pos-a
        top-0
        left-0
        width-p-100
        height-p-50
        flex
        flex-align-center
        flex-justify-center
        transform
        transition-5
    `,
    classNameRemoveDownActive: `
        transform-translate-Y-p-0
    `,
    classNameRemoveDownNotActive: `
        transform-translate-Y-p-100
    `,
    classNameRemoveDown: `
        bg-red
        pos-a
        top-p-50
        left-0
        width-p-100
        height-p-50
        flex
        flex-align-center
        flex-justify-center
        transform
        transition-5
    `,
    classNameLoader: `
        pos-a
        inset-0
        m-auto
        bg-black
        opacity-5
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoader: (
        <>
            <Loader size={200} />
        </>
    ),
    styleTempleteInputFile: 'tolinkme2',
};

export const tolinkme2: InputAvatarClassProps = {
    ...tolinkme,
    classNameContent: `
        pos-r
        pointer
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
        border-radius-12
    `,
    styleContent: {
        width: '4.5rem',
    },
    classNameContentImg: `
        pos-r
        border-radius-12
        bg-white
        pointer
        overflow-hidden
    `,
    classNameImg: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        pointer
    `,
    img: 'uploadBg.png',
    iconDelete: (
        <>
            <Trash size={20} />
        </>
    ),
    iconLoader: (
        <>
            <Loader size={50} />
        </>
    ),
};

export const tolinkme3: InputAvatarClassProps = {
    ...tolinkme,
    classNameContent: `
        pos-r
    `,
    classNameContentImg: `
        pos-r
        border-radius-100
        overflow-hidden
        bg-gradient-darkAqua-brightPink 
    `,
    classNameImg: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        p-5
        border-radius-100
    `,
    classNameRemove: `
        d-none
    `,
};

export const tolinkmeSmall: InputAvatarClassProps = {
    ...tolinkme,
    styleContent: {
        width: 'min(100%,2.5rem)',
    },
    iconDelete: (
        <>
            <Trash size={10} />
        </>
    ),
    iconChange: (
        <>
            <span className="font-nunito color-white font-20 line-h-10 cursor-pointer">
                +
            </span>
        </>
    ),
    iconLoader: <></>,
};
export const tolinkme4: InputAvatarClassProps = {
    ...tolinkme,
    styleContent: {
        width: 'min(100%,5rem)',
    },
    iconDelete: (
        <>
            <Trash size={10} />
        </>
    ),
    iconChange: (
        <>
            <span className="font-nunito color-white font-20 line-h-10 cursor-pointer">
                +
            </span>
        </>
    ),
    iconLoader: <></>,
};
