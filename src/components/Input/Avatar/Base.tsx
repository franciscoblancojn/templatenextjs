import { useMemo, useState } from 'react';

import { InputFile, InputFileStyles } from '@/components/Input/File';
import { InputFileDataProps } from '@/components/Input/File/Base';
import { Image } from '@/components/Image';
import { SubmitResult } from '@/components/Form/Base';

import { useNotification } from '@/hook/useNotification';
import CSS from 'csstype';

import { NotificationStyles } from '@/components/Notification';

import { useLang } from '@/lang/translate';
import Theme, { ThemesType } from '@/config/theme';

export type InputAvatarOnSubmit = (
    avatar: InputFileDataProps | undefined
) => Promise<SubmitResult> | SubmitResult | void;

export interface InputAvatarClassProps {
    classNameContent?: string;
    styleContent?: CSS.Properties;
    classNameContentImg?: string;
    classNameTextPhoto?: string;
    styleContentImg?: CSS.Properties;
    classNameImg?: string;
    classNameText?: string;
    img?: string;
    inf?: any;
    styleTemplateImg?: ThemesType;
    styleTempleteInputFile?: InputFileStyles | ThemesType;
    classNameRemove?: string;
    classNameRemoveMovil?: string;
    classNameRemoveDesktop?: string;
    classNameRemoveTop?: string;
    classNameRemoveDown?: string;
    classNameRemoveTopActive?: string;
    classNameRemoveDownActive?: string;
    classNameRemoveTopNotActive?: string;
    classNameRemoveDownNotActive?: string;
    iconDelete?: any;
    iconChange?: any;
    classNameLoader?: string;
    iconLoader?: any;
    styleTemplateNotification?: NotificationStyles;
}

export interface InputAvatarBaseProps {
    defaultValue?: InputFileDataProps;
    value?: InputFileDataProps;
    useValue?: boolean;
    onSubmit?: InputAvatarOnSubmit;
    accept?: string[];
}

export interface InputAvatarProps
    extends InputAvatarBaseProps,
        InputAvatarClassProps {}

export const InputAvatarBase = ({
    defaultValue,
    value,
    useValue = false,
    classNameContent = '',
    classNameText = '',
    classNameTextPhoto = '',
    styleContent = {},
    classNameContentImg = '',
    styleContentImg = {},
    classNameImg = '',
    img = '',
    styleTemplateImg = Theme.styleTemplate ?? '_default',
    styleTempleteInputFile = Theme.styleTemplate ?? '_default',
    classNameRemove = '',
    classNameRemoveTop = '',
    classNameRemoveDown = '',
    classNameRemoveTopActive = '',
    classNameRemoveDownActive = '',
    classNameRemoveTopNotActive = '',
    classNameRemoveDownNotActive = '',
    iconChange = <></>,
    iconDelete = <></>,
    classNameLoader = '',
    iconLoader = <></>,
    inf = false,
    accept = ['png', 'jpg', 'jpeg', 'webp', 'gift'],
    ...props
}: InputAvatarProps) => {
    const { pop } = useNotification();
    const _t = useLang();
    const [showChangeDelete, setShowChangeDelete] = useState(false);
    const [loader, setLoader] = useState(false);
    const [avatar_, setAvatar] = useState<InputFileDataProps | undefined>(
        defaultValue
    );
    const avatar = useMemo(
        () => (useValue ? value : avatar_),
        [useValue, value, avatar_]
    );

    const onChangeAvatar = async (avatar: InputFileDataProps | undefined) => {
        setLoader?.(true);
        try {
            const result = await props?.onSubmit?.(avatar);
            pop({
                type: 'ok',
                message: _t(result?.message ?? ''),
            });
        } catch (error: any) {
            pop({
                type: 'error',
                message: _t(error?.message ?? error ?? ''),
            });
        }
        setLoader?.(false);
    };
    const Content = useMemo(() => {
        if (loader) {
            return (
                <>
                    <div className={classNameLoader}>{iconLoader}</div>
                </>
            );
        }
        if (avatar == undefined || avatar.fileData == '') {
            return (
                <>
                    <InputFile
                        onChange={(e) => {
                            setAvatar(e);
                            onChangeAvatar(e);
                            setShowChangeDelete(false);
                        }}
                        accept={accept}
                        styleTemplate={styleTempleteInputFile}
                    >
                        <Image
                            src={img}
                            className={classNameImg}
                            styleTemplate={styleTemplateImg}
                        />
                        {inf && !avatar?.fileData ? (
                            <>
                                <span className={classNameText}>
                                    **Recommended Size 400px 400px.
                                    <br />
                                    **(Max Upload File Size 1MB , JPG, PNG).
                                </span>
                                <span className={classNameTextPhoto}>
                                    Cover Photo
                                </span>
                            </>
                        ) : (
                            <></>
                        )}
                    </InputFile>
                </>
            );
        }
        return (
            <>
                {avatar.fileData && avatar.fileData != '' ? (
                    <img
                        src={avatar.fileData}
                        alt={avatar.text}
                        className={classNameImg}
                    />
                ) : (
                    <></>
                )}

                <div
                    className={`${classNameRemove} `}
                    onMouseLeave={() => {
                        setShowChangeDelete(false);
                    }}
                    onMouseEnter={() => {
                        setShowChangeDelete(true);
                    }}
                    onClick={() => {
                        setShowChangeDelete(true);
                    }}
                >
                    <div
                        className={`${classNameRemoveTop} ${
                            showChangeDelete
                                ? classNameRemoveTopActive
                                : classNameRemoveTopNotActive
                        }`}
                        onClick={() => {
                            setShowChangeDelete(false);
                        }}
                    >
                        <InputFile
                            onChange={(e) => {
                                if (e.fileData != '') {
                                    setAvatar(e);
                                    onChangeAvatar(e);
                                }
                                // setShowChangeDelete(false);
                            }}
                            accept={['png', 'jpg', 'webp']}
                            styleTemplate={styleTempleteInputFile}
                        >
                            {iconChange}
                        </InputFile>
                    </div>
                    <div
                        className={`${classNameRemoveDown} ${
                            showChangeDelete
                                ? classNameRemoveDownActive
                                : classNameRemoveDownNotActive
                        }`}
                        onClick={() => {
                            setAvatar(undefined);
                            onChangeAvatar({
                                fileData: '',
                                text: '',
                            });
                            setShowChangeDelete(false);
                        }}
                    >
                        {iconDelete}
                    </div>
                </div>
            </>
        );
    }, [avatar, loader, showChangeDelete]);

    return (
        <>
            <div className={classNameContent} style={styleContent}>
                <div className={classNameContentImg} style={styleContentImg}>
                    {Content}
                </div>
            </div>
        </>
    );
};
export default InputAvatarBase;
