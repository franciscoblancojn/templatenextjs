import { InputSelect, SelectProps } from '@/components/Input/Select';
import { AnimationsList } from '@/interfaces/Animations';
// import Date from '@/svg/date';

export const InputSelectAnimation = ({ ...props }: SelectProps) => {
    return (
        <InputSelect
            {...props}
            placeholder="Select Animation"
            label="Animation"
            // icon={<Date size={15} />}
            options={AnimationsList.map((e) => {
                return {
                    value: e,
                    text: e.split('-').join(' '),
                };
            })}
        />
    );
};
export default InputSelectAnimation;
