import { InputSelect, SelectProps } from '@/components/Input/Select';
import Date from '@/svg/date';

export const InputDateSearch = ({ ...props }: SelectProps) => {
    return (
        <InputSelect
            {...props}
            placeholder="1 Bedroom Large"
            label="Move Size"
            icon={<Date size={15} />}
            options={[
                {
                    text: 'Studio',
                    value: 'Studio',
                },
                {
                    text: 'Studio Alcove',
                    value: 'Studio Alcove',
                },
                {
                    text: '1 Bedroom Small',
                    value: '1 Bedroom Small',
                },
                {
                    text: '1 Bedroom Large',
                    value: '1 Bedroom Large',
                },
                {
                    text: '2 Bedrooms',
                    value: '2 Bedrooms',
                },
                {
                    text: '3 Bedrooms',
                    value: '3 Bedrooms',
                },
                {
                    text: '4+ Bedrooms',
                    value: '4+ Bedrooms',
                },
            ]}
        />
    );
};
export default InputDateSearch;
