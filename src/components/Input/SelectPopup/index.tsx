import { useMemo } from 'react';

import * as styles from '@/components/Input/SelectPopup/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    SelectPopupBaseProps,
    SelectPopupBase,
} from '@/components/Input/SelectPopup/Base';

export const SelectPopupStyle = { ...styles } as const;

export type SelectPopupStyles = keyof typeof SelectPopupStyle;

export interface SelectPopupProps extends SelectPopupBaseProps {
    styleTemplate?: SelectPopupStyles | ThemesType;
}

export const SelectPopup = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: SelectPopupProps) => {
    const Style = useMemo(
        () =>
            SelectPopupStyle[styleTemplate as SelectPopupStyles] ??
            SelectPopupStyle._default,
        [styleTemplate]
    );

    return <SelectPopupBase {...Style} {...props} />;
};
export default SelectPopup;
