import { Story, Meta } from "@storybook/react";

import { SelectPopupProps, SelectPopup } from "./index";

export default {
    title: "Input/SelectPopup",
    component: SelectPopup,
} as Meta;

const SelectPopupIndex: Story<SelectPopupProps> = (args) => (
    <SelectPopup {...args}>Test Children</SelectPopup>
);

export const Index = SelectPopupIndex.bind({});
Index.args = {};
