import { useMemo, useState } from 'react';

import { SubmitResult } from '@/components/Form/Base';
import { InputText, InputTextStyles } from '@/components/Input/Text';

import { useNotification } from '@/hook/useNotification';

import { useLang } from '@/lang/translate';

import { Trash } from '@/svg/trash';
import { Theme, ThemesType } from '@/config/theme';

export type StatusTypeAddEdit = 'add' | 'edit' | 'cancel-save';

export interface InputAddEditClassProps {
    classNameContent?: string;
    classNameContentIcon?: string;
    classNameContentInput?: string;
    classNameName?: string;
    classNameContentAddEdit?: string;
    classNameAdd?: string;
    classNameEdit?: string;
    classNameCancel?: string;
    classNameSave?: string;
    classNameDelete?: string;
    styleTemplateInput?: InputTextStyles | ThemesType;
}
export interface InputAddEditBaseProps {
    icon?: any;
    defaultValue?: string;
    placeholder?: string;
    name?: string;
    onSave?: (value: string) => Promise<SubmitResult>;
    onDelete?: () => Promise<SubmitResult>;
}

export interface InputAddEditProps
    extends InputAddEditBaseProps,
        InputAddEditClassProps {}

export const InputAddEditBase = ({
    classNameContent = '',
    classNameContentIcon = '',
    classNameContentInput = '',
    classNameName = '',
    classNameContentAddEdit = '',
    classNameAdd = '',
    classNameEdit = '',
    classNameCancel = '',
    classNameSave = '',
    classNameDelete = '',

    icon = <></>,
    defaultValue = '',
    placeholder = '',
    name = '',
    styleTemplateInput = Theme?.styleTemplate ?? '_default',
    ...props
}: InputAddEditProps) => {
    const { pop } = useNotification();
    const _t = useLang();
    const [status, setStatus] = useState<StatusTypeAddEdit>(
        defaultValue == '' ? 'add' : 'edit'
    );
    const [value, setValue] = useState(defaultValue);
    const [valueEdit, setValueEdit] = useState(defaultValue);

    const onSave = async () => {
        try {
            const result = await props?.onSave?.(valueEdit);

            pop({
                type: 'ok',
                message: _t(result?.message ?? ''),
            });

            if (result?.status == 'ok') {
                setValue(valueEdit);
                setStatus('edit');
            }
        } catch (error: any) {
            pop({
                type: 'error',
                message: _t(error?.message ?? error ?? ''),
            });
        }
    };
    const onCancel = () => {
        setValueEdit(value);
        setStatus(value == '' ? 'add' : 'edit');
    };
    const onDelete = async () => {
        setValueEdit('');
        setStatus('add');
        await props?.onDelete?.();
    };

    const CAddEdit = useMemo(() => {
        if (status == 'add') {
            return (
                <>
                    <div
                        className={classNameAdd}
                        onClick={() => {
                            setStatus('cancel-save');
                        }}
                    >
                        {_t('Add')}
                    </div>
                </>
            );
        }
        if (status == 'edit') {
            return (
                <>
                    <div
                        className={classNameEdit}
                        onClick={() => {
                            setStatus('cancel-save');
                        }}
                    >
                        {_t('Edit')}
                    </div>
                </>
            );
        }
        if (status == 'cancel-save') {
            return (
                <>
                    <div className={classNameCancel} onClick={onCancel}>
                        {_t('Cancel')}
                    </div>
                    <div className={classNameSave} onClick={onSave}>
                        {_t('Save')}
                    </div>
                    <div className={classNameDelete} onClick={onDelete}>
                        <Trash />
                    </div>
                </>
            );
        }

        return <></>;
    }, [status, value, valueEdit]);

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentIcon}>{icon}</div>
                <div className={classNameContentInput}>
                    <InputText
                        value={valueEdit}
                        onChange={setValueEdit}
                        disabled={status != 'cancel-save'}
                        styleTemplate={styleTemplateInput}
                        placeholder={placeholder}
                    />
                </div>
                <div className={classNameName}>{name}</div>
                <div className={classNameContentAddEdit}>{CAddEdit}</div>
            </div>
        </>
    );
};
export default InputAddEditBase;
