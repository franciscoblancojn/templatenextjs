import { InputAddEditClassProps } from '@/components/Input/AddEdit/Base';

export const tolinkme: InputAddEditClassProps = {
    classNameContent: `
        flex
        flex-align-center
        flex-gap-10 flex-sm-gap-25
        flex-gap-row-25
        flex-justify-between
        flex-nowrap flex-sm-wrap
    `,
    classNameContentIcon: `
        flex-2
        flex
        flex-align-center
    `,
    classNameContentInput: `
        flex-6 flex-sm-10 flex-lg-4 
        flex
        flex-align-center
    `,
    classNameName: `
        flex-6 flex-lg-3
        color-greyish
        font-nunito
        font-15
        font-w-900
        text-capitalize
        d-none d-sm-block
    `,
    classNameContentAddEdit: `
        flex-4 flex-sm-6 flex-lg-3
        flex
        flex-align-center
        flex-gap-column-5 flex-sm-gap-column-15
        flex-nowrap
        flex-justify-right
    `,
    classNameAdd: `
        color-greyishBrownTwo
        font-nunito
        font-11
        pointer
    `,
    classNameEdit: `
        color-greyishBrownTwo
        font-nunito
        font-10 font-sm-12
        font-w-600
        pointer
    `,
    classNameCancel: `
        color-pinkishGreyThree
        font-nunito
        font-10 font-sm-14
        font-w-900
        pointer
    `,
    classNameSave: `
        color-brightSkyBlue
        font-nunito
        font-10 font-sm-14
        font-w-900
        pointer
    `,
    classNameDelete: `
        color-greyishBrownTwo
        pointer
    `,
    styleTemplateInput: 'tolinkmeAddEdit',
};
