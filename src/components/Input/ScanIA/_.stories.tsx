import { Story, Meta } from "@storybook/react";

import { InputScanIAProps, InputScanIA } from "./index";

export default {
    title: "Input/InputScanIA",
    component: InputScanIA,
} as Meta;

const InputScanIAIndex: Story<InputScanIAProps> = (args) => (
    <InputScanIA {...args}>Test Children</InputScanIA>
);

export const Index = InputScanIAIndex.bind({});
Index.args = {};
