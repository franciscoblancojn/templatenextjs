import { Button, ButtonStyles } from '@/components/Button';
import Grid from '@/components/Grid';
import ListItemScan, { ListItemScanStyles } from '@/components/ListItemScan';
import Theme, { ThemesType } from '@/config/theme';
import { useNotification } from '@/hook/useNotification';
import {
    ItemScanProps,
    ScanItemScanProps,
    ScanItemSocketProps,
} from '@/interfaces/Scan/ItemScan';
import { useLang } from '@/lang/translate';
import Add from '@/svg/Add';
import Camera from '@/svg/camera';
import Close from '@/svg/close';
import Loader from '@/svg/loader';
import { useState, useRef, useEffect, useMemo } from 'react';

export interface InputScanIAClassProps {
    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateListItemScan?: ListItemScanStyles | ThemesType;
    styleTemplateListItemScan2?: ListItemScanStyles | ThemesType;

    classNameContentScanerVideo?: string;
    classNameContentScaner?: string;
    classNameContentVideo?: string;
    classNameScanerVideo?: string;
    classNameContentListItemScanAfterVideo?: string;
    classNameContentItems?: string;
    classNameContentItemsTitle?: string;
    classNameScanerCanvas?: string;

    classNameContentCloseCamera?: string;
    classNameBtnClose?: string;
    classNameBtnDone?: string;
    classNameBtnDoneActive?: string;
    classNameBtnDoneInactive?: string;
    classNameContentLoader?: string;

    classNameError?: string;

    classNameContenBox?: string;
    classNameBox?: string;
    classNameAdd?: string;
}

export interface InputScanIABaseProps {
    delay?: number;
    onSendImg?: ScanItemScanProps;

    onSocketSend?: ScanItemSocketProps;
    socketData?: ItemScanProps[];
    onChange?: (data: ItemScanProps[]) => void;
}

export interface InputScanIAProps
    extends InputScanIAClassProps,
        InputScanIABaseProps {}

export const InputScanIABase = ({
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateListItemScan = Theme.styleTemplate ?? '_default',
    styleTemplateListItemScan2 = Theme.styleTemplate ?? '_default',

    classNameContentScaner = '',
    classNameContentScanerVideo = '',
    classNameContentVideo = '',
    classNameScanerVideo = '',
    classNameContentListItemScanAfterVideo = '',
    classNameContentItems = '',
    classNameContentItemsTitle = '',
    classNameScanerCanvas = '',

    classNameContentCloseCamera = '',
    classNameBtnClose = '',
    classNameBtnDone = '',
    classNameBtnDoneActive = '',
    classNameBtnDoneInactive = '',
    classNameContentLoader = '',

    classNameError = '',

    classNameContenBox = '',
    classNameBox = '',
    classNameAdd = '',

    delay = 2000,
    onSendImg,
    onSocketSend,
    socketData,
    onChange,
}: InputScanIAProps) => {
    const _t = useLang();
    const v = useRef<any>(null);
    const c = useRef<any>(null);
    const { pop } = useNotification();
    const [loader, setLoader] = useState(false);
    const [imgBase64, setImgBase64] = useState('');
    const [dimencionsVideo, setDimencionsVideo] = useState({
        width: 0,
        height: 0,
    });
    const [showScaner, setShowScaner] = useState(false);
    const [error, setError] = useState('');
    const [bbox, setBbox] = useState<ItemScanProps[]>([]);

    const [files, setFiles] = useState<ItemScanProps[]>([]);

    const addFile = async (file: ItemScanProps) => {
        setFiles((pre) => [...pre, file]);
        pop({
            type: 'ok',
            message: '✅ Item Added Successfully',
        });
    };

    const removeFile = (i: number) => {
        setFiles((pre) => pre.filter((p, j) => j != i && p));
    };

    useEffect(() => {
        onChange?.(files);
    }, [files]);

    function handleSuccess(stream: any) {
        const w: any = window;
        w.stream = stream;
        v.current.srcObject = stream;
    }
    const initScaner = async () => {
        try {
            const constraints = {
                audio: false,
                video: {
                    with: window.outerWidth,
                    height: window.outerHeight,
                    facingMode: 'environment',
                },
            };
            const stream = await navigator.mediaDevices.getUserMedia(
                constraints
            );

            if (stream.active != true) {
                throw 'Error';
            }
            handleSuccess(stream);
            setError('');
            loopTakePicture();
        } catch (e: any) {
            alert('Error: ' + e);
            setError(`navigator.getUserMedia error:${e.toString()}`);
        }
    };
    const toggleScaner = () => {
        setShowScaner((pre) => !pre);
    };
    const onLoadCameras = () => {
        navigator.mediaDevices
            .enumerateDevices()
            .then(() => {
                setLoader(false);
            })
            .catch(function (error) {
                setError(`Error al obtener la lista de cámaras: ${error}`);
            });
    };
    useEffect(() => {
        if (showScaner) {
            setLoader(true);
            const n: any = navigator;
            n.permissions.query({ name: 'camera' }).then(function () {
                onLoadCameras();
            });
        }
    }, [showScaner]);
    useEffect(() => {
        if (showScaner) {
            initScaner();
        }
    }, [showScaner]);

    const takePictureWithVideo = () => {
        c.current.width = v.current.videoWidth;
        c.current.height = v.current.videoHeight;
        const context = c.current.getContext('2d');
        context.drawImage(v.current, 0, 0);
        const imageData = c.current.toDataURL('image/jpeg');
        if (imageData != 'data:,') {
            setImgBase64(imageData);
            return imageData;
        }
        return undefined;
    };
    const loopTakePicture = async () => {
        if (showScaner) {
            const data = takePictureWithVideo();
            setTimeout(loopTakePicture, delay);
            if (data) {
                if (onSocketSend) {
                    onSocketSend?.({
                        fileData: data,
                    });
                } else if (onSendImg) {
                    const r = await onSendImg?.({
                        fileData: data,
                    });
                    if (r.length > 0) {
                        setBbox(r);
                    }
                }
            }
        }
    };

    const CBox = useMemo(() => {
        let itemsScan = [];
        if (socketData) {
            itemsScan = socketData;
        } else {
            itemsScan = bbox;
        }
        if (itemsScan.length == 0) {
            return <></>;
        }
        return (
            <div className={classNameContenBox}>
                {itemsScan.map((itemScan, i) => {
                    const box = itemScan.bbox ?? [0, 0, 0, 0];
                    const l = `${
                        (box[2] * 100) / (dimencionsVideo.width + 1)
                    }%`;
                    const t = `${
                        (box[0] * 100) / (dimencionsVideo.height + 1)
                    }%`;
                    const w = `${
                        ((box[3] - box[2]) * 100) / (dimencionsVideo.width + 1)
                    }%`;
                    const h = `${
                        ((box[1] - box[0]) * 100) / (dimencionsVideo.height + 1)
                    }%`;
                    // pop({
                    //     message: `${l} ${t} ${w} ${h}`,
                    //     type: "error",
                    // });
                    return (
                        <>
                            <div
                                key={`${i}_${t}_${l}_${w}_${h}`}
                                style={{
                                    top: t,
                                    left: l,
                                    width: w,
                                    height: h,
                                }}
                                className={classNameBox}
                            >
                                <div
                                    className={classNameAdd}
                                    onClick={() => {
                                        addFile?.(itemScan);
                                    }}
                                >
                                    <Add size={20} />
                                    <span>Add Item</span>
                                </div>
                            </div>
                        </>
                    );
                })}
            </div>
        );
    }, [bbox, socketData]);

    const onLoadDimencions = async () => {
        const i = new Image();
        i.onload = () => {
            setDimencionsVideo({
                width: i.width,
                height: i.height,
            });
        };
        i.src = imgBase64;
    };

    useEffect(() => {
        onLoadDimencions();
    }, [imgBase64]);

    return (
        <>
            <Button styleTemplate={styleTemplateButton} onClick={toggleScaner}>
                <span className="m-r-10 flex flex-align-center">
                    <Camera size={16} />
                </span>
                {_t('Scan your place and refine your quote')}
                <span className="font-em-8 font-w-400 color-graySea  m-0 m-l-10 pos-a right-2  top-0">
                    ({_t('Optional')})
                </span>
            </Button>
            <ListItemScan
                files={files}
                removeFile={removeFile}
                styleTemplate={styleTemplateListItemScan}
            />
            <div
                className={`${classNameContentScaner} ${
                    showScaner ? '' : 'd-none'
                }`}
            >
                <canvas ref={c} className={classNameScanerCanvas}></canvas>
                <Grid
                    className={classNameContentScanerVideo}
                    areas={['A', 'B', 'B', 'B', 'B', 'B', 'C', 'C', 'C', 'C']}
                >
                    <div className={classNameContentCloseCamera}>
                        <div
                            className={classNameBtnClose}
                            onClick={toggleScaner}
                        >
                            <Close size={10} />
                            {_t('Close')}
                        </div>
                        <div
                            className={`${classNameBtnDone} ${
                                files.length > 0
                                    ? classNameBtnDoneActive
                                    : classNameBtnDoneInactive
                            }`}
                            onClick={toggleScaner}
                        >
                            Done
                        </div>
                    </div>
                    <div className={classNameContentVideo}>
                        <video
                            ref={v}
                            className={classNameScanerVideo}
                            playsInline
                            autoPlay
                            muted
                        ></video>
                        {CBox}
                    </div>
                    <div className={classNameContentListItemScanAfterVideo}>
                        <div className={classNameContentItems}>
                            <div className={classNameContentItemsTitle}>
                                {files.length > 0
                                    ? `Items results: ${files.length}`
                                    : ''}
                            </div>
                            <ListItemScan
                                files={files}
                                removeFile={removeFile}
                                styleTemplate={styleTemplateListItemScan2}
                            />
                        </div>
                    </div>
                </Grid>
                {error != '' ? (
                    <>
                        <div className={classNameError}>{error}</div>
                    </>
                ) : (
                    <></>
                )}
                {showScaner && loader ? (
                    <div className={classNameContentLoader}>
                        <Loader size={100} />
                    </div>
                ) : (
                    <></>
                )}
            </div>
        </>
    );
};
export default InputScanIABase;
