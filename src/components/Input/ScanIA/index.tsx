import { useMemo } from 'react';

import * as styles from '@/components/Input/ScanIA/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    InputScanIABaseProps,
    InputScanIABase,
} from '@/components/Input/ScanIA/Base';

export const InputScanIAStyle = { ...styles } as const;

export type InputScanIAStyles = keyof typeof InputScanIAStyle;

export interface InputScanIAProps extends InputScanIABaseProps {
    styleTemplate?: InputScanIAStyles | ThemesType;
}

export const InputScanIA = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputScanIAProps) => {
    const Style = useMemo(
        () =>
            InputScanIAStyle[styleTemplate as InputScanIAStyles] ??
            InputScanIAStyle._default,
        [styleTemplate]
    );

    return <InputScanIABase {...Style} {...props} />;
};
export default InputScanIA;
