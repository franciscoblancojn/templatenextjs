# InputScanIA

## Dependencies

[InputScanIA](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/InputScanIA)

```js
import { InputScanIA } from '@/components/Input/InputScanIA';
```

## Import

```js
import { InputScanIA, InputScanIAStyles } from '@/components/Input/InputScanIA';
```

## Props

```tsx
interface InputScanIAProps {}
```

## Use

```js
<InputScanIA>InputScanIA</Input/InputScanIA>
```
