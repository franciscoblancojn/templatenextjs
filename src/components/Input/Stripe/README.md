# Stripe

## Dependencies

[Stripe](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/Stripe)

```js
import { Stripe } from '@/components/Input/Stripe';
```

## Import

```js
import { Stripe, StripeStyles } from '@/components/Input/Stripe';
```

## Props

```tsx
interface StripeProps {}
```

## Use

```js
<Stripe>Stripe</Input/Stripe>
```
