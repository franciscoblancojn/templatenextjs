import { StripeClassProps } from '@/components/Input/Stripe/Base';

export const mooveri: StripeClassProps = {
    classNameContent: `
        flex
        flex-nowrap
        flex-column
        flex-gap-row-21
    `,
    classNameContentCardElement: `
    
    `,
    classNameCardElement: `
        
    `,
    classNameContentBtn: `
    
    `,
    ButtonProps: {
        styleTemplate: 'mooveriAddPayment',
    },
};
