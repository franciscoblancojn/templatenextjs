import { Story, Meta } from "@storybook/react";

import { StripeProps, Stripe } from "./index";

export default {
    title: "Input/Stripe",
    component: Stripe,
} as Meta;

const StripeIndex: Story<StripeProps> = (args) => (
    <Stripe {...args}>Test Children</Stripe>
);

export const Index = StripeIndex.bind({});
Index.args = {
};
