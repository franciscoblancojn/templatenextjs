import { Story, Meta } from "@storybook/react";

import { InputAddonsProps, InputAddons } from "./index";

export default {
    title: "Input/InputAddons",
    component: InputAddons,
} as Meta;

const Template: Story<InputAddonsProps> = (args) => (
    <InputAddons {...args}>Test Children</InputAddons>
);

export const Index = Template.bind({});
Index.args = {
    label: "Label Test",
    onChangeValidate(e) {
        if(e == "error"){
            throw {
                message:"Error"
            }
        }
    },
};
