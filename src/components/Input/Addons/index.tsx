import {
    InputAddonsBase,
    InputAddonsBaseProps,
} from '@/components/Input/Addons/Base';
import * as styles from '@/components/Input/Addons/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InputAddonsStyle = { ...styles } as const;
export type InputAddonsStyles = keyof typeof InputAddonsStyle;

export interface InputAddonsProps extends InputAddonsBaseProps {
    styleTemplate?: InputAddonsStyles | ThemesType;
}

export const InputAddons = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputAddonsProps) => {
    return (
        <>
            <InputAddonsBase
                {...props}
                {...(InputAddonsStyle[styleTemplate as InputAddonsStyles] ??
                    InputAddonsStyle._default)}
            />
        </>
    );
};
export default InputAddons;
