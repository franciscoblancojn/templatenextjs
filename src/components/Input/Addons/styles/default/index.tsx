import { InputAddonsClassProps } from '@/components/Input/Addons/Base';

export const _default: InputAddonsClassProps = {
    classNameInput: `
        p-h-10 p-v-5 
        font-20 font-montserrat
        width-p-100 
        border-0 
        border-radius-0
        box-shadow box-shadow-inset box-shadow-s-1
        outline-none 
        box-shadow-black 
    `,
    classNameLabel: `
        pos-r 
        d-block 
        font-20 font-montserrat
    `,
    classNameError: `
        font-10 font-montserrat
        text-right
        p-v-3
        color-error
    `,
    //for Select
    classNameOptions: `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        
        border-style-solid border-1 border-black
        font-15
        z-index-1
        overflow-auto
    `,
    classNameOption: `
        p-h-15
        pointer
        list-none
        bg-gray-hover
        color-white-hover
    
    `,
    classNameOptionDisabled: `
        bg-gray
        color-white
    `,
};
