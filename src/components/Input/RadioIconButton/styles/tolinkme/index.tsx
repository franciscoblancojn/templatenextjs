import { RadioIconButtonClassProps } from '@/components/Input/RadioIconButton/Base';
import { IconButtonValues } from '@/interfaces/Button';

export const tolinkme: RadioIconButtonClassProps = {
    ContentWidthProps: {
        size: 500,
        className: `
            p-h-15
            m-h-auto
        `,
    },
    styleTemplateRadius: 'tolinkme',
    classNameBtn: `
        width-p-100
        text-center
        border-radius-10
        bg-greyish
        color-white
        text-capitalize
        font-nunito
        font-w-700

        font-17
        p-5

        p-h-27
        pos-r
    `,
    classNameBtnActive: ` 
        bg-gradient-darkAqua-brightPink
    `,
    classNameBtnInactive: `
        bg-greyish
    `,
    classNameBtnTypes: IconButtonValues,
    ImageProps: {
        src: 'rs/facebook.png',
        styleTemplate: 'tolinkme',
        className: `
            width-17
            pos-a
            top-0
            bottom-0
            m-auto
        `,
    },
};
