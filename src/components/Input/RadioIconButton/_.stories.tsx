import { Story, Meta } from "@storybook/react";

import { RadioIconButtonProps, RadioIconButton } from "./index";

export default {
    title: "Input/RadioIconButton",
    component: RadioIconButton,
} as Meta;

const RadioIconButtonIndex: Story<RadioIconButtonProps> = (args) => (
    <RadioIconButton {...args}>Test Children</RadioIconButton>
);

export const Index = RadioIconButtonIndex.bind({});
Index.args = {};
