import { useMemo } from 'react';

import * as styles from '@/components/Input/RadioIconButton/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    RadioIconButtonBaseProps,
    RadioIconButtonBase,
} from '@/components/Input/RadioIconButton/Base';

export const RadioIconButtonStyle = { ...styles } as const;

export type RadioIconButtonStyles = keyof typeof RadioIconButtonStyle;

export interface RadioIconButtonProps extends RadioIconButtonBaseProps {
    styleTemplate?: RadioIconButtonStyles | ThemesType;
}

export const RadioIconButton = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: RadioIconButtonProps) => {
    const Style = useMemo(
        () =>
            RadioIconButtonStyle[styleTemplate as RadioIconButtonStyles] ??
            RadioIconButtonStyle._default,
        [styleTemplate]
    );

    return <RadioIconButtonBase {...Style} {...props} />;
};
export default RadioIconButton;
