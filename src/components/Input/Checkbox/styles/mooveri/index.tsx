import { InputCheckboxClassProps } from '@/components/Input/Checkbox/Base';
import CheckSearch from '@/svg/checkSearch';

export const mooveri: InputCheckboxClassProps = {
    classNameLabel: `
        font-12 
        font-nunito
        font-w-400
        color-warmGreyThree
        pointer 
        color-warm-grey-three 
        flex 
        flex-align-center
        flex-nowrap
    `,
    classNameText: `
    
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-13
    height-13
    p-0 
    border-2 
    border-style-solid 
    border-warmGreyThree
    border-radius-3 
    m-r-9 
    bg-white
`,
    classNameCheckboxActive: `
    bg-warmGreyThree
    color-white
`,
    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};

export const mooveriBackoffice: InputCheckboxClassProps = {
    classNameLabel: `
        font-16 
        font-nunito
        font-w-400
        color-white
        pointer 
        flex 
        flex-align-center
        flex-nowrap
    `,
    classNameText: `
    
`,
    classNameCheckbox: `
        flex 
        flex-justify-center 
        flex-align-center 
        width-13
        height-13
        p-0 
        border-2 
        border-style-solid 
        border-warmGreyThree
        border-radius-3 
        m-r-9 
        bg-white
    `,
    classNameCheckboxActive: `
        bg-greenish-cyan
        color-white
        border-greenish-cyan
    `,
    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};
