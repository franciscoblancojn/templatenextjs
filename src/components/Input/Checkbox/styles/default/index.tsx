import {
    BaseStyle,
    InputCheckboxClassProps,
} from '@/components/Input/Checkbox/Base';

export const _default: InputCheckboxClassProps = {
    classNameLabel: `
        ${BaseStyle.classNameLabel}
    `,
    classNameText: `
        ${BaseStyle.classNameText}
    `,
    classNameCheckbox: `
        ${BaseStyle.classNameCheckbox}
        border-black
    `,
    classNameCheckboxActive: `
        ${BaseStyle.classNameCheckboxActive}
        bg-black
    `,
    classNameCheckboxInactive: `
        ${BaseStyle.classNameCheckboxInactive}
    `,
};
