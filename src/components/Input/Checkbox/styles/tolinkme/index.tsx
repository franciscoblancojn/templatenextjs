import { InputCheckboxClassProps } from '@/components/Input/Checkbox/Base';
import CheckSearch from '@/svg/checkSearch';
import Crown from '@/svg/crown';

export const tolinkme: InputCheckboxClassProps = {
    classNameLabel: `
        pos-r
        bg-white
        border-radius-26
        p-v-6 p-h-15
        d-inline-block
        font-15 
        font-nunito
        color-black
        pointer
    `,
    classNameText: `
    `,
    classNameCheckbox: `
        pos-a
        inset-0
        border-style-solid
        border-3
        border-transparent
        border-radius-26
    `,
    classNameCheckboxActive: `
        border-brightPink
    `,
    classNameCheckboxInactive: `
    `,
    icon: <></>,
};

export const tolinkme2: InputCheckboxClassProps = {
    classNameLabel: `
        flex
        flex-align-center
        flex-gap-column-20
        pointer
    `,
    classNameContentCheckbox: `
        pos-r
        border-radius-12
        p-3
        pointer
        width-50
        flex
        transition-5
        bg-FrenchBlue-hover
    `,
    classNameContentCheckboxActive: `
        bg-darkAqua
        flex-justify-right
    `,
    classNameContentCheckboxInactive: `
        flex-justify-left
        bg-warmGrey
    `,
    classNameText: `
        font-14 font-w-900 font-nunito
        color-greyishBrown
    `,
    classNameCheckbox: `
        width-18
        height-18
        border-radius-100
        bg-white
        flex
        flex-align-center
        flex-justify-center
    `,
    classNameCheckboxActive: `
        
    `,
    classNameCheckboxInactive: `
        
    `,
    icon: <></>,
};

export const tolinkmeCrown: InputCheckboxClassProps = {
    ...tolinkme2,
    classNameContentCheckboxActive: `
        bg-gold
        flex-justify-right
    `,
    classNameContentCheckboxInactive: `
        flex-justify-left
        bg-warmGrey
    `,
    classNameText: `
        font-14 font-w-900 font-nunito
        color-greyishBrown
    `,
    classNameCheckboxActive: `
        
    `,
    classNameCheckboxInactive: `
        
    `,
    icon: <Crown size={14} />,
};
export const tolinkme3: InputCheckboxClassProps = {
    classNameLabel: `
        pos-r
        bg-white
        border-radius-15
        p-v-16 p-h-15
        font-15 
        font-nunito
        color-black
        pointer
        width-p-100
        flex 
        flex-align-center
        flex-justify-center
    `,
    classNameText: `
    `,
    classNameCheckbox: `
        pos-a
        inset-0
        border-style-solid
        border-3
        border-transparent
        border-radius-15
        
    `,
    classNameCheckboxActive: `
        border-brightPink
        
    `,

    classNameCheckboxInactive: `
    `,
    icon: <></>,
};
export const tolinkme4: InputCheckboxClassProps = {
    classNameLabel: `
        font-12 
        font-nunito
        font-w-400
        color-warmGreyThree
        pointer 
        color-warm-grey-three 
        flex 
        flex-align-center
        flex-nowrap
        flex-justify-center
        width-p-100
        bg-white
        p-v-16
        p-h-15
        border-radius-15
        
    `,
    classNameText: `
    
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    flex-nowrap
    width-15
    height-15
    p-0 
    border-2 
    border-style-solid 
    border-white
    border-radius-3 
    m-r-9 
    bg-transparent
`,
    classNameCheckboxActive: `
    bg-warmGreyThree
    color-white
        flex 
        flex-justify-center 
        flex-align-center 
        width-13
        height-13
        p-0 
        border-radius-3 
        m-r-9 
        bg-darkAqua
    `,

    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};
export const tolinkme5: InputCheckboxClassProps = {
    classNameLabel: `
        font-12 
        font-nunito
        font-w-400
        color-warmGreyThree
        pointer 
        color-warm-grey-three 
        flex 
        flex-align-center
        flex-nowrap
    `,
    classNameText: `
    
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-15
    height-15
    p-0 
    border-2 
    border-style-solid 
    border-white
    border-radius-3 
    m-r-9 
    bg-transparent
`,
    classNameCheckboxActive: `
    bg-warmGreyThree
    color-white
`,
    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};
export const tolinkme6: InputCheckboxClassProps = {
    classNameLabel: `
        font-14
        font-nunito
        font-w-400
        pointer 
        color-black
        flex 
        flex-nowrap
        m-v-20
    `,
    classNameText: `
    
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-15
    height-15
    p-0 
    border-2 
    border-style-solid 
    border-white
    border-radius-3 
    m-r-9 
    bg-transparent
`,
    classNameCheckboxActive: `
    bg-warmGreyThree
    color-white
`,
    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};
export const tolinkme7: InputCheckboxClassProps = {
    classNameLabel: `
        font-12
        font-nunito
        flex-align-center
        font-w-400
        pointer 
        color-black
        flex 
        flex-nowrap
        m-v-20
    `,
    classNameText: `
    color-black
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-15
    height-15
    p-0 
    border-2 
    border-style-solid 
    border-black
    border-radius-3 
    m-r-9 
    bg-transparent
`,
    classNameCheckboxActive: `
    bg-transparent
    color-black
`,
    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};
export const tolinkme8: InputCheckboxClassProps = {
    classNameLabel: `
        pos-r
        bg-grayAcd
        border-radius-26
        p-v-6 p-h-15
        d-inline-block
        font-15 
        font-nunito
        color-black
        pointer
    `,
    classNameText: `
    `,
    classNameCheckbox: `
        pos-a
        inset-0
        border-style-solid
        border-3
        border-transparent
        border-radius-26
    `,
    classNameCheckboxActive: `
        border-brightPink
    `,
    classNameCheckboxInactive: `
    `,
    icon: <></>,
};
export const tolinkme9: InputCheckboxClassProps = {
    classNameLabel: `
        font-12
        font-nunito
        font-w-400
        pointer 
        color-black
        flex 
        flex-nowrap
        m-t-10
        m-b-5
        flex-align-center 
    `,
    classNameText: `
    color-black
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-15
    height-15
    p-0 
    border-2 
    border-style-solid 
    border-black
    border-radius-3 
    m-r-9 
    bg-transparent
`,
    classNameCheckboxActive: `
    bg-transparent
    color-black
`,
    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};
export const tolinkme10: InputCheckboxClassProps = {
    classNameLabel: `
        font-14 
        font-nunito
        font-w-400
        color-white
        pointer 
        flex 
        flex-nowrap
    `,
    classNameText: `
    
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-15
    height-15
    p-0 
    border-2 
    border-style-solid 
    border-white
    border-radius-3 
    m-r-9 
    bg-transparent
`,
    classNameCheckboxActive: `
    bg-warmGreyThree
    color-white
`,
    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};
export const tolinkme11: InputCheckboxClassProps = {
    classNameLabel: `
    font-14 
    font-nunito
    font-w-400
    color-white
    pointer 
    flex 
    flex-nowrap
    aspect-ratio-20
    flex
    flex-justify-between
    flex-align-center
    width-p-100
    `,
    classNameText: `
    width-p-100
    flex
    flex-justify-between
    flex-align-center
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-14
    height-14
    p-0 
    border-radius-5
    m-r-9 
    bg-darkAqua
`,
    classNameCheckboxActive: `
    bg-transparent
    color-brightPink
    border-2 
    border-style-solid 
    border-brightPink
`,
    classNameCheckboxInactive: `
`,
    icon: <CheckSearch size={8} />,
};
export const tolinkme12: InputCheckboxClassProps = {
    classNameLabel: `
        font-12 
        font-nunito
        font-w-400
        color-white
        pointer 
        flex 
        flex-nowrap
    `,
    classNameText: `
    
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-15
    height-15
    p-0 
    border-2 
    border-style-solid 
    border-white
    border-radius-3 
    m-r-9 
    bg-transparent
`,
    classNameCheckboxActive: `
    bg-warmGreyThree
    color-white
`,
    classNameCheckboxInactive: `
    

`,
    icon: <CheckSearch size={8} />,
};

export const tolinkme13: InputCheckboxClassProps = {
    classNameLabel: `
        pos-r
        bg-white
        width-p-100
        border-radius-15
        p-v-16 p-h-8
        d-inline-block
        font-15 
        font-nunito
        color-black
        pointer
    `,
    classNameText: `
    `,
    classNameCheckbox: `
        pos-a
        inset-0
        border-style-solid
        border-3
        border-transparent
        border-radius-15
    `,
    classNameCheckboxActive: `
        border-brightPink
    `,
    classNameCheckboxInactive: `
    `,
    icon: <></>,
};
export const tolinkme14: InputCheckboxClassProps = {
    classNameLabel: `
    flex
    flex-align-center
    pointer
    width-p-50
    m-b-15
    flex-nowrap
    flex-gap-column-25
    font-14
    font-nunito
    color-reddishGrey
    p-b-11
    p-t-12
    `,
    classNameText: `
    font-14
        font-nunito
        width-p-100
        font-w-900
`,
    classNameCheckbox: `
    flex 
    flex-justify-center 
    flex-align-center 
    width-15
    height-15
    p-0 
    border-2 
    border-style-solid 
    border-black
    border-radius-3 
    m-r-9 
    bg-transparent
`,
    classNameCheckboxActive: `
    border-brightPink
        border-style-solid
        border-3
        font-14
        font-w-900
        font-nunito
        color-brightPink
        aspect-ratio-1-1
`,
    classNameCheckboxInactive: `
    border-warmGreyThree
    border-style-solid
    border-3
    font-14
    font-nunito
    p-5
    aspect-ratio-1-1

`,
    icon: <CheckSearch size={8} />,
};
