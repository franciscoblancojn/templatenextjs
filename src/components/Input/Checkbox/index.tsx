import {
    InputCheckboxBase,
    InputCheckboxBaseProps,
} from '@/components/Input/Checkbox/Base';
import * as styles from '@/components/Input/Checkbox/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InputCheckboxStyle = { ...styles } as const;
export type InputCheckboxStyles = keyof typeof InputCheckboxStyle;

export interface InputCheckboxProps extends InputCheckboxBaseProps {
    styleTemplate?: InputCheckboxStyles | ThemesType;
    checked?: boolean;
}

export const InputCheckbox = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputCheckboxProps) => {
    const config = {
        ...(InputCheckboxStyle[styleTemplate as InputCheckboxStyles] ??
            InputCheckboxStyle._default),
        ...props,
    };
    return <InputCheckboxBase {...config} />;
};
export default InputCheckbox;
