import { InputCheckbox, InputCheckboxProps } from '@/components/Input/Checkbox';
import { useLang } from '@/lang/translate';

export const CheckBoxPacking = ({ ...props }: InputCheckboxProps) => {
    const _t = useLang();
    return (
        <InputCheckbox
            {...props}
            label={
                <>
                    {_t('Do you need a hand for packing?')}
                    <span className="font-em-8 m-0 m-l-10 text-right">
                        ({_t('Optional')})
                    </span>
                </>
            }
        />
    );
};
export default InputCheckbox;
