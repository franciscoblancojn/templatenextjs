import { useMemo } from 'react';

import * as styles from '@/components/Input/CardStripe/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    CardStripeBaseProps,
    CardStripeBase,
} from '@/components/Input/CardStripe/Base';

export const CardStripeStyle = { ...styles } as const;

export type CardStripeStyles = keyof typeof CardStripeStyle;

export interface CardStripeProps extends CardStripeBaseProps {
    styleTemplate?: CardStripeStyles | ThemesType;
}

export const CardStripe = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: CardStripeProps) => {
    const Style = useMemo(
        () =>
            CardStripeStyle[styleTemplate as CardStripeStyles] ??
            CardStripeStyle._default,
        [styleTemplate]
    );

    return <CardStripeBase {...Style} {...props} />;
};
export default CardStripe;
