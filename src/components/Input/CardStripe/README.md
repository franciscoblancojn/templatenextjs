# CardStripe

## Dependencies

[CardStripe](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/CardStripe)

```js
import { CardStripe } from '@/components/Input/CardStripe';
```

## Import

```js
import { CardStripe, CardStripeStyles } from '@/components/Input/CardStripe';
```

## Props

```tsx
interface CardStripeProps {}
```

## Use

```js
<CardStripe>CardStripe</Input/CardStripe>
```
