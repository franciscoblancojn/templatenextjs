import { loadStripe } from '@stripe/stripe-js';
import {
    CardElement,
    Elements,
    useElements,
    useStripe,
} from '@stripe/react-stripe-js';
import log from '@/functions/log';
import { Button, ButtonProps } from '@/components/Button';
import { useNotification } from '@/hook/useNotification';
import { useLang } from '@/lang/translate';
import { onSaveCardType } from '@/interfaces/Card';
import { useState } from 'react';
import { useUser } from '@/hook/useUser';
import { useRouter } from 'next/router';
import url from '@/data/routes';

export interface CardStripeClassProps {
    classNameContent?: string;
    classNameContentCardElement?: string;
    classNameCardElement?: string;
    classNameContentBtn?: string;
    ButtonProps?: ButtonProps;
}

export interface CardStripeBaseProps {
    onSaveCard?: onSaveCardType;
    textBtn?: string;
    urlRedirect?: string;
}

export interface CardStripeProps
    extends CardStripeClassProps,
        CardStripeBaseProps {}

export const CardStripeBaseCardElement = ({
    classNameContent = '',
    classNameContentCardElement = '',
    classNameCardElement = '',
    classNameContentBtn = '',
    ButtonProps = {},
    textBtn = 'Save Card',

    ...props
}: CardStripeProps) => {
    const _t = useLang();
    const route = useRouter();
    const { user } = useUser();
    const { pop } = useNotification();
    const [loader, setLoader] = useState<boolean>(false);
    const stripe: any = useStripe();
    const elements: any = useElements();
    const addPayment = async () => {
        setLoader(true);
        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: 'card',
            card: elements.getElement(CardElement),
        });
        if (error == undefined) {
            log('paymentMethod addPayment', paymentMethod, 'greem');
            const result = await props?.onSaveCard?.(
                {
                    id: paymentMethod.id,
                    brand: paymentMethod.card.brand,
                    last4: paymentMethod.card.last4,
                    exp_month: paymentMethod.card.exp_month,
                    exp_year: paymentMethod.card.exp_year,
                    postal_code:
                        paymentMethod.billing_details.address.postal_code,
                },
                user
            );
            log('result addPayment', result, 'green');
            if (result?.status == 'error') {
                pop({
                    type: 'error',
                    message: _t(result?.message ?? ''),
                });
                setLoader(false);
                return;
            }
            pop({
                type: 'ok',
                message: _t(result?.message ?? ''),
            });
            setLoader(false);
            route.push(props?.urlRedirect ?? url.index);
        } else {
            log('error addPayment', error, 'red');
            pop({
                type: 'error',
                message: _t(error.message),
            });
            setLoader(false);
        }
    };
    return (
        <div className={classNameContent}>
            <div className={classNameContentCardElement}>
                <CardElement className={classNameCardElement} />
            </div>
            <div className={classNameContentBtn}>
                <Button {...ButtonProps} onClick={addPayment} loader={loader}>
                    {_t(textBtn)}
                </Button>
            </div>
        </div>
    );
};

export const CardStripeBase = (props: CardStripeProps) => {
    if (process?.env?.['NEXT_PUBLIC_STORYBOK'] == 'TRUE') {
        return <></>;
    }
    try {
        const stripePromise = loadStripe(
            process?.env?.['NEXT_PUBLIC_STRIPE_KEY'] ?? ''
        );
        return (
            <>
                <Elements stripe={stripePromise}>
                    <CardStripeBaseCardElement {...props} />
                </Elements>
            </>
        );
    } catch (error) {
        return <> STRIPE_KEY empty </>;
    }
};
export default CardStripeBase;
