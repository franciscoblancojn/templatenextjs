import { Story, Meta } from "@storybook/react";

import { InputEditProps, InputEdit } from "./index";
import log from "@/functions/log";

export default {
    title: "Input/InputEdit",
    component: InputEdit,
} as Meta;

const Template: Story<InputEditProps> = (args) => (
    <InputEdit {...args}>Test Children</InputEdit>
);

export const Index = Template.bind({});
Index.args = {
    label:"label",
    onSave: async (data: string) => {
        log("onSave",data)
        return {
            status: "ok",
            message: "Save ok",
        };
    },
};

export const Error_ = Template.bind({});
Error_.args = {
    label:"label",
    onSave: async (data: string) => {
        log("onSave",data)
        throw {
            message: "error",
        };
        return {
            status: "ok",
            message: "Save ok",
        };
    },
};
