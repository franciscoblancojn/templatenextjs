import { useEffect, useMemo, useState } from 'react';

import { SubmitResult } from '@/components/Form/Base';
import { InputText, InputTextStyles } from '@/components/Input/Text';

import { useNotification } from '@/hook/useNotification';

import { useLang } from '@/lang/translate';

import { Theme, ThemesType } from '@/config/theme';
import { InputTextBaseProps } from '@/components/Input/Text/Base';
import InputTel, { InputTelStyles } from '@/components/Input/Tel';
import { parseTel } from '@/functions/parseTel';
import { InputTelValue } from '../Tel/Base';
import { PhoneProps } from '@/interfaces/Phone';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import Google, { GoogleStyles } from '../Google';
import { GoogleAddres } from '@/interfaces/Google/addres';

export type StatusTypeEdit = 'edit' | 'cancel-save';

export interface InputEditClassProps {
    classNameContent?: string;
    classNameContentInput?: string;
    classNameContentEdit?: string;
    classNameLoader?: string;
    classNameCancel?: string;
    classNameSave?: string;
    iconCancel?: any;
    iconSave?: any;
    iconEdit?: any;
    styleTemplateInputEdit?: InputTextStyles | ThemesType;
    styleTemplateInputSaveCancel?: InputTextStyles | ThemesType;
    styleTemplateInputTelEdit?: InputTelStyles | ThemesType;
    styleTemplateInputTelSaveCancel?: InputTelStyles | ThemesType;
    styleTemplateInputGoogleEdit?: GoogleStyles | ThemesType;
    styleTemplateInputGoogleSaveCancel?: GoogleStyles | ThemesType;
}
export interface InputEditBaseProps extends Omit<InputTextBaseProps, 'type'> {
    defaultValue?: string;
    onSave?: (value: string) => Promise<SubmitResult> | SubmitResult;
    setStatus?: (status: StatusTypeEdit) => void;
    activeEdit?: boolean;
    loader?: boolean;
    type?:
        | 'text'
        | 'search'
        | 'tel'
        | 'url'
        | 'password'
        | 'number'
        | 'textarea'
        | 'google';
}

export interface InputEditProps
    extends InputEditBaseProps,
        InputEditClassProps {}

export const InputEditBase = ({
    classNameContent = '',
    classNameContentInput = '',
    classNameContentEdit = '',
    classNameLoader = '',
    classNameCancel = '',
    classNameSave = '',
    defaultValue = '',
    iconEdit = <></>,
    iconCancel = <></>,
    iconSave = <></>,
    styleTemplateInputEdit = Theme?.styleTemplate ?? '_default',
    styleTemplateInputSaveCancel = Theme?.styleTemplate ?? '_default',
    styleTemplateInputTelEdit = Theme?.styleTemplate ?? '_default',
    styleTemplateInputTelSaveCancel = Theme?.styleTemplate ?? '_default',
    styleTemplateInputGoogleEdit = Theme?.styleTemplate ?? '_default',
    styleTemplateInputGoogleSaveCancel = Theme?.styleTemplate ?? '_default',
    activeEdit = true,
    loader = false,
    type = 'text',
    ...props
}: InputEditProps) => {
    const route = useRouter();
    const { pop } = useNotification();
    const _t = useLang();
    const [status, setStatus] = useState<StatusTypeEdit>('edit');
    const [value, setValue] = useState(defaultValue);
    const [valueEdit, setValueEdit] = useState(defaultValue);

    const onSave = async () => {
        try {
            const result = await props?.onSave?.(valueEdit);

            pop({
                type: 'ok',
                message: _t(result?.message ?? ''),
            });

            if (result?.status == 'ok') {
                setValue(valueEdit);
                setStatus('edit');
            }
        } catch (error: any) {
            pop({
                type: 'error',
                message: _t(error?.message ?? error ?? ''),
            });
            if (error?.code == 401) {
                route.push(url.logout);
            }
        }
    };
    const onCancel = () => {
        setValueEdit(value);
        setStatus('edit');
    };

    const CEdit = useMemo(() => {
        if (loader) {
            return <div className={classNameLoader} />;
        }
        if (status == 'edit') {
            return (
                <>
                    <div
                        className={classNameContentEdit}
                        onClick={() => {
                            setStatus('cancel-save');
                        }}
                    >
                        {iconEdit}
                        {_t('Edit')}
                    </div>
                </>
            );
        }
        if (status == 'cancel-save') {
            return (
                <>
                    <div className={classNameCancel} onClick={onCancel}>
                        {iconCancel}
                        {_t('Cancel')}
                    </div>
                    <div className={classNameSave} onClick={onSave}>
                        {iconSave}
                        {_t('Save')}
                    </div>
                </>
            );
        }

        return <></>;
    }, [status, value, valueEdit, loader]);

    useEffect(() => {
        props?.setStatus?.(status);
    }, [status]);

    const tel: PhoneProps = useMemo(() => {
        return parseTel(valueEdit);
    }, [valueEdit]);

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentInput}>
                    {type == 'tel' ? (
                        <>
                            <InputTel
                                {...props}
                                value={tel}
                                onChange={(tel: InputTelValue) => {
                                    setValueEdit(`${tel.code}-${tel.tel}`);
                                }}
                                disabled={status != 'cancel-save'}
                                styleTemplate={
                                    status == 'edit'
                                        ? styleTemplateInputTelEdit
                                        : styleTemplateInputTelSaveCancel
                                }
                            />
                        </>
                    ) : (
                        <>
                            {type == 'google' ? (
                                <>
                                    <Google
                                        {...props}
                                        onChange={(addres: GoogleAddres) => {
                                            setValueEdit(
                                                addres?.formatedAddress ?? ''
                                            );
                                        }}
                                        disabled={status != 'cancel-save'}
                                        styleTemplate={
                                            status == 'edit'
                                                ? styleTemplateInputGoogleEdit
                                                : styleTemplateInputGoogleSaveCancel
                                        }
                                    />
                                </>
                            ) : (
                                <>
                                    <InputText
                                        {...props}
                                        value={valueEdit}
                                        onChange={setValueEdit}
                                        disabled={status != 'cancel-save'}
                                        styleTemplate={
                                            status == 'edit'
                                                ? styleTemplateInputEdit
                                                : styleTemplateInputSaveCancel
                                        }
                                    />
                                </>
                            )}
                        </>
                    )}
                </div>
                <div className={classNameContentEdit}>
                    {activeEdit ? CEdit : ''}
                </div>
            </div>
        </>
    );
};
export default InputEditBase;
