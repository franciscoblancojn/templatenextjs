# Edit

## Dependencies

[InputText](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/InputText)

```js
import { InputText } from '@/components/InputText';
```

## Import

```js
import { Edit, EditStyles } from '@/components/Edit';
```

## Props

```tsx
interface EditProps {}
```

## Use

```js
<Edit>Edit</Edit>
```
