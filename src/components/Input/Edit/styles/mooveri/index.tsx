import { InputEditClassProps } from '@/components/Input/Edit/Base';
import Cancel from '@/svg/cancel';
import Edit from '@/svg/edit';
import SaveCheck from '@/svg/SaveCheck';

export const mooveri: InputEditClassProps = {
    classNameContent: `
        flex
        flex-align-center
        flex-gap-10 flex-sm-gap-25
        flex-gap-row-25
        flex-justify-between
        flex-nowrap 
    `,
    classNameContentInput: `
        flex-6
        flex
        flex-align-center
    `,
    classNameContentEdit: `
        flex-6
        flex
        flex-align-center
        flex-gap-column-5 flex-sm-gap-column-15
        flex-nowrap
        flex-justify-right
        pointer
        editField 
        font-11
        font-sm-14
        font-w-900
        color-sea 
        color-seaDark-hover 
    `,
    classNameLoader: `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
        text-center
        m-h-auto
    `,
    classNameCancel: `
        flex flex-gap-column-5
        color-peachyPink
        color-grayHexa-hover
        font-circular
        font-11 font-sm-14
        font-w-900
        pointer
        cancelEditField  
        flex-nowrap
    `,
    classNameSave: `
        flex flex-gap-column-5
        color-sea
        color-seaDark-hover
        font-nunito
        font-10 font-sm-14
        font-w-900
        pointer
        flex-nowrap
    `,
    styleTemplateInputEdit: 'mooveriEdit',
    styleTemplateInputSaveCancel: 'mooveriSaveCancel',
    styleTemplateInputTelEdit: 'mooveriEdit',
    styleTemplateInputTelSaveCancel: 'mooveriSaveCancel',
    styleTemplateInputGoogleEdit: 'mooveriManage',
    styleTemplateInputGoogleSaveCancel: 'mooveriManage',
    iconCancel: <Cancel size={8} />,
    iconSave: <SaveCheck size={8} />,
};

export const mooveri2: InputEditClassProps = {
    classNameContent: `
        flex
        flex-align-center
        flex-gap-10 flex-sm-gap-25
        flex-gap-row-25
        flex-justify-between
        flex-nowrap 
    `,
    classNameContentInput: `
        flex-11
        flex
        flex-align-center
    `,

    classNameContentEdit: `
        flex-1
        flex
        flex-align-center
        flex-gap-column-5 flex-sm-gap-column-15
        flex-nowrap
        flex-justify-right
        pointer
        editField 
        font-11
        font-sm-14
        font-w-900
        color-sea 
        color-seaDark-hover 
    `,
    classNameCancel: `
        flex flex-gap-column-5
        color-peachyPink
        color-grayHexa-hover
        font-nunito
        font-11 font-sm-14
        font-w-900
        pointer
        cancelEditField  
        flex-nowrap
    `,
    classNameSave: `
        flex flex-gap-column-5
        color-sea
        color-seaDark-hover
        font-nunito
        font-10 font-sm-14
        font-w-900
        pointer
        flex-nowrap
      
    `,
    styleTemplateInputEdit: 'mooveriProfileEdit',
    styleTemplateInputSaveCancel: 'mooveriSaveCancel',
    styleTemplateInputTelSaveCancel: 'mooveriSaveCancel',
    iconCancel: <Cancel size={8} />,
    iconSave: <SaveCheck size={8} />,
    iconEdit: <Edit size={16} />,
};
export const mooveri3: InputEditClassProps = {
    ...mooveri2,
    styleTemplateInputEdit: 'mooveriEdit',
};
