import { InputEditClassProps } from '@/components/Input/Edit/Base';

export const tolinkme: InputEditClassProps = {
    classNameContent: `
        flex
        flex-align-center
        flex-gap-10 flex-sm-gap-25
        flex-gap-row-25
        flex-justify-between
        flex-nowrap flex-sm-wrap
    `,
    classNameContentInput: `
        flex-6 flex-sm-10 flex-lg-4 
        flex
        flex-align-center
    `,
    classNameContentEdit: `
        flex-4 flex-sm-6 flex-lg-3
        flex
        flex-align-center
        flex-gap-column-5 flex-sm-gap-column-15
        flex-nowrap
        flex-justify-right
    `,
    classNameCancel: `
        color-pinkishGreyThree
        font-nunito
        font-10 font-sm-14
        font-w-900
        pointer
    `,
    classNameSave: `
        color-brightSkyBlue
        font-nunito
        font-10 font-sm-14
        font-w-900
        pointer
    `,
};
