import { Google, GoogleProps } from '@/components/Input/Google';

import Location from '@/svg/location';

export const InputGoogleMovingTo = ({ ...props }: GoogleProps) => {
    return (
        <Google {...props} label="Moving To" icon={<Location size={15} />} />
    );
};
export default InputGoogleMovingTo;
