import { Google, GoogleProps } from '@/components/Input/Google';

export const InputGoogleConfirm = ({ ...props }: GoogleProps) => {
    return <Google {...props} styleTemplate="mooveri" />;
};
export default InputGoogleConfirm;
