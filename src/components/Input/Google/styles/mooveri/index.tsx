import { GoogleClassProps } from '@/components/Input/Google/Base';

export const mooveri: GoogleClassProps = {
    classNameAutocomplete: `
        
    `,
    styleTemplateInput: 'mooveri',
};

export const mooveriManage: GoogleClassProps = {
    styleTemplateInput: 'mooveriManage',
};

export const mooveriGoogle: GoogleClassProps = {
    classNameAutocomplete: `
        
    `,
    styleTemplateInput: 'mooveri2InputIconReverse',
};

export const mooveri3: GoogleClassProps = {
    styleTemplateInput: 'mooveri2InputIconReverse',
};
