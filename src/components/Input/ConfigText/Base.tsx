import { useData } from 'fenextjs-hook/cjs/useData';
import {
    ConfigText,
    ConfigTextFontFamily,
    ConfigTextFontWeight,
} from '@/interfaces/ConfigText';
import { useLang } from '@/lang/translate';
import { InputRange, InputRangeStyles } from '../Range';
import Theme, { ThemesType } from '@/config/theme';
import { Text, TextStyles } from '@/components/Text';
import { useMemo } from 'react';
import { InputColor, InputColorStyles } from '../Color';
import { InputSelect } from '../Select';
import { InputTextStyles } from '../Text';
import { getCorrectColor } from '@/functions/getCorrectColor';
import { getText } from '@/functions/getText';

export interface ConfigTextClassProps {
    classNameContent?: string;
    classNameContentConfig?: string;

    classNameContentConfigInput?: string;
    styleTemplateTitleInput?: TextStyles | ThemesType;
    styleTemplateInputRange?: InputRangeStyles | ThemesType;
    styleTemplateInputColor?: InputColorStyles | ThemesType;
    styleTemplateInputSelect?: InputTextStyles | ThemesType;

    classNameContentExample?: string;
    classNameTextExample?: string;
}

export interface ConfigTextBaseProps {
    defaultValue?: ConfigText;
    onChange?: (data: ConfigText) => void;
    textExample?: any;
}

export interface ConfigTextProps
    extends ConfigTextClassProps,
        ConfigTextBaseProps {}

export const ConfigTextBase = ({
    classNameContent = '',
    classNameContentConfig = '',

    classNameContentConfigInput = '',
    styleTemplateTitleInput = Theme?.styleTemplate ?? '_default',
    styleTemplateInputRange = Theme?.styleTemplate ?? '_default',
    styleTemplateInputColor = Theme?.styleTemplate ?? '_default',
    styleTemplateInputSelect = Theme?.styleTemplate ?? '_default',

    classNameContentExample = '',
    classNameTextExample = '',

    defaultValue = {
        fontFamily: 'nunito',
        fontSize: 17,
        fontWeight: 'regular',
        color: 'black',
    },
    onChange,
    textExample = 'Text Example',
}: ConfigTextProps) => {
    const _t = useLang();
    const {
        data,
        onChangeData,
        dataMemo: example,
    } = useData<ConfigText, any>(defaultValue, {
        onChangeDataAfter: onChange,
        onMemo: (data: ConfigText) =>
            getText(data, textExample, classNameTextExample),
    });
    const bgTextExample = useMemo(
        () => getCorrectColor(data.color),
        [data.color]
    );

    return (
        <>
            <div className={classNameContent}>
                {textExample ? (
                    <div
                        className={classNameContentExample}
                        style={{ background: bgTextExample }}
                    >
                        {example}
                    </div>
                ) : (
                    <></>
                )}
                <div className={classNameContentConfig}>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Font Family')}
                            {': '}
                            <span
                                className={`text-capitalize font-${data?.fontFamily}`}
                            >
                                {data?.fontFamily}
                            </span>
                        </Text>
                        <InputSelect
                            defaultValue={{
                                value: data?.fontFamily ?? '',
                                text: data?.fontFamily ?? '',
                            }}
                            onChange={(d) => {
                                onChangeData('fontFamily')(d?.value ?? '');
                            }}
                            options={ConfigTextFontFamily.map((family) => ({
                                value: family,
                                text: family,
                                classNameOption: `font-${family}`,
                            }))}
                            styleTemplate={styleTemplateInputSelect}
                        />
                    </div>

                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Font Size')}
                        </Text>
                        <InputRange
                            min={10}
                            max={40}
                            defaultValue={data.fontSize}
                            onChange={onChangeData('fontSize')}
                            styleTemplate={styleTemplateInputRange}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Font Weight')}
                        </Text>
                        <InputRange
                            min={0}
                            max={ConfigTextFontWeight.length - 1}
                            defaultValue={ConfigTextFontWeight?.indexOf(
                                data.fontWeight ?? 'regular'
                            )}
                            onChange={(i: number) => {
                                onChangeData('fontWeight')(
                                    ConfigTextFontWeight[i]
                                );
                            }}
                            styleTemplate={styleTemplateInputRange}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Line Height')}
                        </Text>
                        <InputRange
                            min={0}
                            max={20}
                            defaultValue={data?.lineHeight ?? 10}
                            onChange={onChangeData('lineHeight')}
                            styleTemplate={styleTemplateInputRange}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Color')}
                        </Text>
                        <InputColor
                            defaultValue={data.color}
                            onChange={onChangeData('color')}
                            styleTemplate={styleTemplateInputColor}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default ConfigTextBase;
