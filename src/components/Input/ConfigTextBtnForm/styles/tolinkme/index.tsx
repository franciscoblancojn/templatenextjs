import { ConfigTextBtnFormClassProps } from '@/components/Input/ConfigTextBtnForm/Base';

export const tolinkme: ConfigTextBtnFormClassProps = {
    classNameContent: `
        p-h-15
    `,
    classNameContentConfig: `
        flex-12
    `,

    classNameContentConfigInput: `
        flex-12
        flex
        flex-column
        flex-gap-row-15
        p-v-15
        border-0
        border-b-1
        border-style-solid
        border-whiteTwo
        width-p-100
        flex-justify-between
    `,
    styleTemplateTitleInput: 'tolinkme13',
    styleTemplateInputColor: 'tolinkme2',

    classNameContentExample: `
        flex-12
        pos-sk
        top-0
    `,
    classNameTextExample: `
        text-center
    `,
};
