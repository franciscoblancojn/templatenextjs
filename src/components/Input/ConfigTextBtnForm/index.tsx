import { useMemo } from 'react';

import * as styles from '@/components/Input/ConfigTextBtnForm/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigTextBtnFormBaseProps,
    ConfigTextBtnFormBase,
} from '@/components/Input/ConfigTextBtnForm/Base';

export const ConfigTextBtnFormStyle = { ...styles } as const;

export type ConfigTextBtnFormStyles = keyof typeof ConfigTextBtnFormStyle;

export interface ConfigTextBtnFormProps extends ConfigTextBtnFormBaseProps {
    styleTemplate?: ConfigTextBtnFormStyles | ThemesType;
}

export const ConfigTextBtnForm = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigTextBtnFormProps) => {
    const Style = useMemo(
        () =>
            ConfigTextBtnFormStyle[styleTemplate as ConfigTextBtnFormStyles] ??
            ConfigTextBtnFormStyle._default,
        [styleTemplate]
    );

    return <ConfigTextBtnFormBase {...Style} {...props} />;
};
export default ConfigTextBtnForm;
