import { Story, Meta } from "@storybook/react";

import { ConfigTextBtnFormProps, ConfigTextBtnForm } from "./index";

export default {
    title: "Input/ConfigTextBtnForm",
    component: ConfigTextBtnForm,
} as Meta;

const ConfigTextBtnFormIndex: Story<ConfigTextBtnFormProps> = (args) => (
    <ConfigTextBtnForm {...args}>Test Children</ConfigTextBtnForm>
);

export const Index = ConfigTextBtnFormIndex.bind({});
Index.args = {};
