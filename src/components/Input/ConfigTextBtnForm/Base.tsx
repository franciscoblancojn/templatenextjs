import { useData } from 'fenextjs-hook/cjs/useData';
import { ConfigText } from '@/interfaces/ConfigText';
import { useLang } from '@/lang/translate';
import Theme, { ThemesType } from '@/config/theme';
import { Text, TextStyles } from '@/components/Text';
import { useMemo } from 'react';
import { InputColor, InputColorStyles } from '../Color';
import { getCorrectColor } from '@/functions/getCorrectColor';
import { getText } from '@/functions/getText';

export interface ConfigTextBtnFormClassProps {
    classNameContent?: string;
    classNameContentConfig?: string;

    classNameContentConfigInput?: string;
    styleTemplateTitleInput?: TextStyles | ThemesType;
    styleTemplateInputColor?: InputColorStyles | ThemesType;

    classNameContentExample?: string;
    classNameTextExample?: string;
}

export interface ConfigTextBtnFormBaseProps {
    defaultValue?: ConfigText;
    onChange?: (data: ConfigText) => void;
    textExample?: any;
}

export interface ConfigTextBtnFormProps
    extends ConfigTextBtnFormClassProps,
        ConfigTextBtnFormBaseProps {}

export const ConfigTextBtnFormBase = ({
    classNameContent = '',
    classNameContentConfig = '',

    classNameContentConfigInput = '',
    styleTemplateTitleInput = Theme?.styleTemplate ?? '_default',
    styleTemplateInputColor = Theme?.styleTemplate ?? '_default',

    classNameContentExample = '',
    classNameTextExample = '',

    defaultValue = {
        fontFamily: 'nunito',
        fontSize: 17,
        fontWeight: 'regular',
        color: 'black',
    },
    onChange,
    textExample = 'Text Example',
}: ConfigTextBtnFormProps) => {
    const _t = useLang();
    const {
        data,
        onChangeData,
        dataMemo: example,
    } = useData<ConfigText, any>(defaultValue, {
        onChangeDataAfter: onChange,
        onMemo: (data: ConfigText) =>
            getText(data, textExample, classNameTextExample),
    });
    const bgTextExample = useMemo(
        () => getCorrectColor(data.color),
        [data.color]
    );

    return (
        <>
            <div className={classNameContent}>
                {textExample ? (
                    <div
                        className={classNameContentExample}
                        style={{ background: bgTextExample }}
                    >
                        {example}
                    </div>
                ) : (
                    <></>
                )}
                <div className={classNameContentConfig}>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Color')}
                        </Text>
                        <InputColor
                            defaultValue={data.color}
                            onChange={onChangeData('color')}
                            styleTemplate={styleTemplateInputColor}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default ConfigTextBtnFormBase;
