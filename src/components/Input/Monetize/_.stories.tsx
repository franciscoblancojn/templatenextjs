import { Story, Meta } from "@storybook/react";

import { InputMonetizeProps, InputMonetize } from "./index";

export default {
    title: "Input/InputMonetize",
    component: InputMonetize,
} as Meta;

const Template: Story<InputMonetizeProps> = (args) => (
    <InputMonetize {...args}>Test Children</InputMonetize>
);

export const Index = Template.bind({});
Index.args = {
    label: "Label Test",
};
