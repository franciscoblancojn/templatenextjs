import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';
import Theme, { ThemesType } from '@/config/theme';
import { useData } from 'fenextjs-hook/cjs/useData';
import {
    SizeButton as RadioSizeButton,
    SizeButtonType as RadioSizeButtonType,
    SizeButtonValuesType,
} from '@/interfaces/Button';
import { useLang } from '@/lang/translate';
import { InputRadio, InputRadioStyles } from '../Radio';

export interface RadioSizeButtonClassProps {
    ContentWidthProps?: ContentWidthProps;
    styleTemplateRadius?: InputRadioStyles | ThemesType;
    classNameBtn?: string;
    classNameBtnActive?: string;
    classNameBtnInactive?: string;
    classNameBtnTypes?: SizeButtonValuesType;
}

export interface RadioSizeButtonBaseProps {
    defaultValue?: RadioSizeButtonType;
    onChange?: (value: RadioSizeButtonType) => void;
}

export interface RadioSizeButtonProps
    extends RadioSizeButtonClassProps,
        RadioSizeButtonBaseProps {}

export const RadioSizeButtonBase = ({
    ContentWidthProps = {},
    styleTemplateRadius = Theme.styleTemplate ?? '_default',
    classNameBtn = '',
    classNameBtnActive = '',
    classNameBtnInactive = '',
    classNameBtnTypes = {
        slim: '',
        regular: '',
        strong: '',
    },
    defaultValue = 'regular',
    ...props
}: RadioSizeButtonProps) => {
    const _t = useLang();
    const { data, onChangeData } = useData<{ value: number }>(
        { value: RadioSizeButton.indexOf(defaultValue) },
        {
            onChangeDataAfter: ({ value }) => {
                props?.onChange?.(RadioSizeButton?.[value]);
            },
        }
    );
    return (
        <>
            <ContentWidth {...ContentWidthProps}>
                <InputRadio
                    onChange={onChangeData('value')}
                    selectedRadioDefault={data.value}
                    options={RadioSizeButton.map((value, i) => ({
                        label: (
                            <div
                                className={`${classNameBtn} ${
                                    data.value == i
                                        ? classNameBtnActive
                                        : classNameBtnInactive
                                } ${classNameBtnTypes?.[value] ?? ''}`}
                            >
                                {_t(value)}
                            </div>
                        ),
                    }))}
                    styleTemplate={styleTemplateRadius}
                />
            </ContentWidth>
        </>
    );
};
export default RadioSizeButtonBase;
