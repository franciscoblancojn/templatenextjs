import { useMemo } from 'react';

import * as styles from '@/components/Input/RadioSizeButton/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    RadioSizeButtonBaseProps,
    RadioSizeButtonBase,
} from '@/components/Input/RadioSizeButton/Base';

export const RadioSizeButtonStyle = { ...styles } as const;

export type RadioSizeButtonStyles = keyof typeof RadioSizeButtonStyle;

export interface RadioSizeButtonProps extends RadioSizeButtonBaseProps {
    styleTemplate?: RadioSizeButtonStyles | ThemesType;
}

export const RadioSizeButton = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: RadioSizeButtonProps) => {
    const Style = useMemo(
        () =>
            RadioSizeButtonStyle[styleTemplate as RadioSizeButtonStyles] ??
            RadioSizeButtonStyle._default,
        [styleTemplate]
    );

    return <RadioSizeButtonBase {...Style} {...props} />;
};
export default RadioSizeButton;
