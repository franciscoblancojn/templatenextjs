import { RadioSizeButtonClassProps } from '@/components/Input/RadioSizeButton/Base';
import { SizeButtonValues } from '@/interfaces/Button';

export const tolinkme: RadioSizeButtonClassProps = {
    ContentWidthProps: {
        size: 500,
        className: `
            p-h-15
            m-h-auto
        `,
    },
    styleTemplateRadius: 'tolinkme',
    classNameBtn: `
        width-p-100
        text-center
        border-radius-10
        bg-greyish
        color-white
        text-capitalize
        font-nunito
        font-w-700

        font-17
        p-5
    `,
    classNameBtnActive: ` 
        bg-gradient-darkAqua-brightPink
    `,
    classNameBtnInactive: `
        bg-greyish
    `,
    classNameBtnTypes: SizeButtonValues,
};
