import { Story, Meta } from "@storybook/react";

import { RadioSizeButtonProps, RadioSizeButton } from "./index";

export default {
    title: "Input/RadioSizeButton",
    component: RadioSizeButton,
} as Meta;

const RadioSizeButtonIndex: Story<RadioSizeButtonProps> = (args) => (
    <RadioSizeButton {...args}>Test Children</RadioSizeButton>
);

export const Index = RadioSizeButtonIndex.bind({});
Index.args = {};
