# Template

## Dependencies

[Template](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/Template)

```js
import { Template } from '@/components/Input/Template';
```

## Import

```js
import { Template, TemplateStyles } from '@/components/Input/Template';
```

## Props

```tsx
interface TemplateProps {}
```

## Use

```js
<Template>Template</Input/Template>
```
