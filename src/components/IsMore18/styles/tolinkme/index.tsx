import { IsMore18ClassProps } from '@/components/IsMore18/Base';

export const tolinkme: IsMore18ClassProps = {
    ContentWidthProps: {
        className: `
            m-h-auto
            text-center
            flex
            flex-column
            flex-align-center
            flex-nowrap
            row-gap-24
            color-white
        `,
        size: 350,
    },
    classNameTitle: `
        font-26
        font-nunito
        font-w-900
        color-white
    `,
    classNameText: `
        font-12
        font-nunito
        font-w-400
        color-white
    `,
    styleTemplateButton: 'tolinkme9',
    styleTemplateLink: 'tolinkme8',
};
