# IsMore18

## Dependencies

[IsMore18](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/IsMore18)

```js
import { IsMore18 } from '@/components/IsMore18';
```

## Import

```js
import { IsMore18, IsMore18Styles } from '@/components/IsMore18';
```

## Props

```tsx
interface IsMore18Props {}
```

## Use

```js
<IsMore18>IsMore18</IsMore18>
```
