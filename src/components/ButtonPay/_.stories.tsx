import { Story, Meta } from "@storybook/react";

import { ButtonPayProps, ButtonPay } from "./index";

export default {
    title: "ButtonPay/ButtonPay",
    component: ButtonPay,
} as Meta;

const ButtonPayIndex: Story<ButtonPayProps> = (args) => (
    <ButtonPay {...args}>Test Children</ButtonPay>
);

export const Index = ButtonPayIndex.bind({});
Index.args = {
};
