import { ButtonPayClassProps } from '@/components/ButtonPay/Base';

export const tolinkme: ButtonPayClassProps = {
    classNameContentButton: `
        bg-americanSilver
        flex
        flex-align-center
        flex-justify-center
        border-radius-10
        width-p-100
        height-p-100
        animation-iteration-count-1 
        animation-duration-10
       
    `,
    classNameButtonPay: `
    width-p-100
    flex
    flex-justify-center
    flex-align-center
    `,
    classNameMoney: `
        p-l-5
        font-14 
        font-nunito 
        font-w-900
        color-black
    `,
    classNameImg: `
        color-black
        flex
    `,
};
