import Padlock from '@/svg/padlock';
import Text from '../Text';
import { PropsWithChildren } from 'react';
import { useLang } from '@/lang/translate';

export interface ButtonPayClassProps {
    classNameContentButton?: any;
    classNameMoney?: string;
    classNameImg?: string;
    classNameButtonPay?: string;
}

export interface ButtonPayBaseProps extends PropsWithChildren {
    price?: any;
    subscribe?: string;
    period?: string;
    logoIcon?: string;
    classNameContentButton?: any;
}

export interface ButtonPayProps
    extends ButtonPayClassProps,
        ButtonPayBaseProps {}

export const ButtonPayBase = ({
    classNameContentButton = '',
    classNameMoney = '',
    classNameImg = '',
    subscribe = '',
    logoIcon = '',
    period = 'Month',
    price = 8.5,
    classNameButtonPay = '',
}: ButtonPayProps) => {
    const _t = useLang();
    return (
        <>
            <div className={`${classNameContentButton} `}>
                <div className={classNameButtonPay}>
                    <div className="flex m-r-8">
                        <img className="width-20 height-20" src={logoIcon} />
                    </div>
                    <div className={classNameImg}>
                        <Padlock size={15} />
                    </div>
                    <Text styleTemplate={'tolinkme25'}>{_t(subscribe)}</Text>
                    <div className={classNameMoney}>
                        <span className="m-r-1">$</span>
                        {price}
                    </div>
                    <Text styleTemplate={'tolinkme26'}>/{_t(period)}</Text>
                </div>
            </div>
        </>
    );
};
export default ButtonPayBase;
