import { PaymentsClassProps } from '@/components/List/Payments/Base';

export const mooveri: PaymentsClassProps = {
    styleTemplateNotItems: 'mooveri',
    styleTemplateItems: 'mooveri',
    sizeContentWidtCardNumbers: 284,
    sizeSpaceCardNumber: 20,
    styleTemplateButton: 'mooveriAddPayment',
};
