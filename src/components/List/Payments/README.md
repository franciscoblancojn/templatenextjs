# Payments

## Dependencies

[Payments](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/List/Payments)

```js
import { Payments } from '@/components/List/Payments';
```

## Import

```js
import { Payments, PaymentsStyles } from '@/components/List/Payments';
```

## Props

```tsx
interface PaymentsProps {}
```

## Use

```js
<Payments>Payments</Payments>
```
