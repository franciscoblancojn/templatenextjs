import Button, { ButtonStyles } from '@/components/Button';
import ContentWidth from '@/components/ContentWidth';
import Link, { LinkProps } from '@/components/Link';
import { NotItemStyles } from '@/components/NotItem';
import { NotItemManage } from '@/components/NotItem/Template/Manage';
import Text, { TextProps } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';
import url from '@/data/routes';
import { DataManageAddress } from '@/interfaces/ManageAddress';
import { useLang } from '@/lang/translate';
import { useMemo } from 'react';

export interface AddressesClassProps {
    styleTemplateNotItems?: NotItemStyles | ThemesType;
    sizeContentWidtCardNumbers?: number;
    classNameContentAddress?: string;
    TitleProps?: TextProps;
    classNameContentNameAddress?: string;
    NameProps?: TextProps;
    classNameContentLink?: string;
    LinkProps?: LinkProps;
    sizeSpaceCardNumber?: number;
    styleTemplateButton?: ButtonStyles | ThemesType;
}

export interface AddressesBaseProps {
    addresses?: DataManageAddress[];
}

export interface AddressesProps
    extends AddressesClassProps,
        AddressesBaseProps {}

export const AddressesBase = ({
    styleTemplateNotItems = Theme.styleTemplate ?? '_default',
    sizeContentWidtCardNumbers = -1,
    classNameContentAddress = '',
    TitleProps = {},
    classNameContentNameAddress = '',
    NameProps = {},
    classNameContentLink = '',
    LinkProps = {},
    styleTemplateButton = Theme.styleTemplate ?? '_default',

    addresses = [],
}: AddressesProps) => {
    const _t = useLang();

    const content = useMemo(() => {
        if (addresses.length == 0) {
            return (
                <>
                    <NotItemManage styleTemplate={styleTemplateNotItems} />
                </>
            );
        }
        return (
            <>
                <ContentWidth size={sizeContentWidtCardNumbers}>
                    {addresses.map((address, i) => (
                        <>
                            <div className={classNameContentAddress} key={i}>
                                <Text {...TitleProps}>{_t('Address')}</Text>
                                <div className={classNameContentNameAddress}>
                                    <Text {...NameProps}>
                                        {address.location}
                                    </Text>
                                    <div className={classNameContentLink}>
                                        <Link
                                            {...LinkProps}
                                            href={
                                                url.myAccount.addresses.single +
                                                address.id
                                            }
                                        >
                                            {_t('Edit')}
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </>
                    ))}
                    <Link
                        href={url.myAccount.addresses.add}
                        styleTemplate="noDecoration"
                    >
                        <Button styleTemplate={styleTemplateButton}>
                            {_t('Add Addresses')}
                        </Button>
                    </Link>
                </ContentWidth>
            </>
        );
    }, [addresses]);

    return <>{content}</>;
};
export default AddressesBase;
