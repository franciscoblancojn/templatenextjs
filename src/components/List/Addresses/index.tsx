import { useMemo } from 'react';

import * as styles from '@/components/List/Addresses/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    AddressesBaseProps,
    AddressesBase,
} from '@/components/List/Addresses/Base';

export const AddressesStyle = { ...styles } as const;

export type AddressesStyles = keyof typeof AddressesStyle;

export interface AddressesProps extends AddressesBaseProps {
    styleTemplate?: AddressesStyles | ThemesType;
}

export const Addresses = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: AddressesProps) => {
    const Style = useMemo(
        () =>
            AddressesStyle[styleTemplate as AddressesStyles] ??
            AddressesStyle._default,
        [styleTemplate]
    );

    return <AddressesBase {...Style} {...props} />;
};
export default Addresses;
