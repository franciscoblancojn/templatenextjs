import CSS from 'csstype';

import { PropsWithChildren } from 'react';

export interface ContentWidthClassProps {}

export interface ContentWidthBaseProps extends PropsWithChildren {
    size?: number;
    className?: string;
    style?: CSS.Properties;
}

export interface ContentWidthProps
    extends ContentWidthClassProps,
        ContentWidthBaseProps {}

export const ContentWidthBase = ({
    children,
    size = -1,
    className = '',
    style = {},
}: ContentWidthProps) => {
    return (
        <div
            className={className}
            style={
                size >= 0
                    ? { width: `min(100%,${size / 16}rem)`, ...style }
                    : style
            }
        >
            {children}
        </div>
    );
};
export default ContentWidthBase;
