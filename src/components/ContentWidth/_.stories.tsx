import { Story, Meta } from "@storybook/react";

import { ContentWidthProps, ContentWidth } from "./index";

export default {
    title: "Content/ContentWidth",
    component: ContentWidth,
} as Meta;

const Template: Story<ContentWidthProps> = (args) => (
    <ContentWidth {...args}>Test Children</ContentWidth>
);

export const Index = Template.bind({});
Index.args = {
    size:100,
    className:"tesClass"
};
