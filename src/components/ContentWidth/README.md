# ContentWidth

## Import

```js
import { ContentWidth } from '@/components/ContentWidth';
```

## Props

```ts
interface ContentWidthProps {
    size?: number;
    className?: string;
    children?: any;
}
```

## Use

```js
<ContentWidth size={500}>Test Children</ContentWidth>
```
