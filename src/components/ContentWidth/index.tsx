import { useMemo } from 'react';

import * as styles from '@/components/ContentWidth/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ContentWidthBaseProps,
    ContentWidthBase,
} from '@/components/ContentWidth/Base';

export const ContentWidthStyle = { ...styles } as const;

export type ContentWidthStyles = keyof typeof ContentWidthStyle;

export interface ContentWidthProps extends ContentWidthBaseProps {
    styleTemplate?: ContentWidthStyles | ThemesType;
}

export const ContentWidth = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ContentWidthProps) => {
    const Style = useMemo(
        () =>
            ContentWidthStyle[styleTemplate as ContentWidthStyles] ??
            ContentWidthStyle._default,
        [styleTemplate]
    );

    return <ContentWidthBase {...Style} {...props} />;
};
export default ContentWidth;
