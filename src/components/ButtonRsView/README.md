# ButtonRsView

## Dependencies

[ButtonRsView](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ButtonRsView)

```js
import { ButtonRsView } from '@/components/ButtonRsView';
```

## Import

```js
import { ButtonRsView, ButtonRsViewStyles } from '@/components/ButtonRsView';
```

## Props

```tsx
interface ButtonRsViewProps {}
```

## Use

```js
<ButtonRsView>ButtonRsView</ButtonRsView>
```
