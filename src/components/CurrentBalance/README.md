# CurrentBalance

## Dependencies

[CurrentBalance](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/CurrentBalance)

```js
import { CurrentBalance } from '@/components/CurrentBalance';
```

## Import

```js
import {
    CurrentBalance,
    CurrentBalanceStyles,
} from '@/components/CurrentBalance';
```

## Props

```tsx
interface CurrentBalanceProps {}
```

## Use

```js
<CurrentBalance>CurrentBalance</CurrentBalance>
```
