import { CurrentBalanceClassProps } from '@/components/CurrentBalance/Base';

export const tolinkme: CurrentBalanceClassProps = {
    styleTemplateTitle: 'tolinkme27',
    classNameContentTitle: `
        p-t-20
        p-h-40
        text-center
    `,
    classNameSubscriptionsIcon: `
        m-auto
        flex
        width-p-60
    `,
    classNameButtonBack: `
        font-16
        font-w-400
        color-teal
        text-decoration-underline
        bg-transparent
        border-0
        font-nunito
        cursor-pointer
    `,
    classNameButtonBackIcon: `
        p-r-5
    `,
    ContentCard: `
        width-p-100
    `,
    classNameContentBtn: `
    pos-f
    bottom-20
    z-index-1
    width-p-100
    p-h-17
    left-0
    right-0
    m-auto
    flex
    flex-justify-center
    `,
    classNameContenIcon: `
        m-auto
        width-p-50
    `,
    classNameContenText: `
        m-t-40
        m-h-auto
        width-p-80
        text-center
    `,
    classNameBtn: `
        pos-f
        bottom-20
        z-index-1
        width-p-100
        p-h-17
        left-0
        right-0
        m-auto
        flex
        flex-justify-center
    
    `,
    classNameUpcomingWIthdraws: `
        width-p-100
    `,
    classNameLinkUpcomingWIthdraws: `
        m-t-10
        
    `,
    classNameBtnWIthdraws: `
    text-center 
    m-auto
    `,
    styleTemplateButtonWIthdraws: 'tolinkme2_2',
    classNameContentSuscription: `
        width-p-100
    `,
};
