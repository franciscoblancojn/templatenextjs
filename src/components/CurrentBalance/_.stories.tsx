import { Story, Meta } from "@storybook/react";

import { CurrentBalanceProps, CurrentBalance } from "./index";

export default {
    title: "CurrentBalance/CurrentBalance",
    component: CurrentBalance,
} as Meta;

const CurrentBalanceIndex: Story<CurrentBalanceProps> = (args) => (
    <CurrentBalance {...args}>Test Children</CurrentBalance>
);

export const Index = CurrentBalanceIndex.bind({});
Index.args = {};
