import { POST_STRIPE_CARD } from '@/api/tolinkme/Payment';
import Theme, { ThemesType } from '@/config/theme';
import { useNotification } from '@/hook/useNotification';
import { useUser } from '@/hook/useUser';
import { DataFormStripe } from '@/interfaces/FormStripe';
import { useLang } from '@/lang/translate';
import UnicornSad from '@/lottin/UnicornSad';
import ArrowGoBack from '@/svg/arrowGoBack';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useState } from 'react';
import BalanceMoney from '../BalanceMoney';
import Button, { ButtonStyles } from '../Button';
import ButtonAmountSuscriptions from '../ButtonAmountSuscriptions';
import ButtonDetailsSubcriptions from '../ButtonDetailsSubcriptions';
import ContentWidth from '../ContentWidth';
import { SubmitResult } from '../Form/Base';
import FormStripe from '../Form/Stripe';
import SelectCard from '../SelectCard';
import Text, { TextStyles } from '../Text';
import UpcomingWithdraws from '../UpcomingWithdraws';

export interface CurrentBalanceClassProps {
    styleTemplateButtonWIthdraws?: ButtonStyles | ThemesType;
    title?: string;
    icon?: any;
    classNameContentTitle?: string;
    icoBack?: any;
    classNameButtonBack?: string;
    classNameButtonBackIcon?: string;
    ContentCard?: string;
    classNameContentBtn?: string;
    classNameContenIcon?: string;
    classNameContenText?: string;
    classNameContentSuscription?: string;
    classNameBtn?: string;
    classNameUpcomingWIthdraws?: string;
    classNameLinkUpcomingWIthdraws?: string;
    classNameBtnWIthdraws?: string;
    defaultValue?: DataFormStripe[];
    uuid?: any;
    classNameContentSubscriptions?: string;
    classNameSubscriptionsIcon?: string;
    styleTemplateTitle?: TextStyles | ThemesType;
}

export interface TolinkmeButtonsDataProps {
    url?: string;
    uuid?: string;
    logo?: string;
    title?: string;
    ButtonPriceDetails?: {
        basePrice: any;
        period: string;
    };
    SubscriptableTransactions?: SubscriptableTransactionsDataProps[];
}

export interface SubscriptableTransactionsDataProps {
    uuid: string;
    status: string;
    user?: {
        email?: string;
        uuid?: string;
    };
}

export interface TolinkmeButtonsProps {
    btns?: TolinkmeButtonsDataProps[];
    getCurrentBalance?: {
        total?: any;
    };
    currentMounthAmounthBalance?: any;
}

export interface CurrentBalanceBaseProps extends TolinkmeButtonsProps {
    currentBalanceMoney?: number;
    currentPendingMoney?: number;
    BalanceMoneyMounth?: number;
    TotalButtonMonetize?: number;
    AmountSuscription?: number;
}
export interface CurrentBalanceProps
    extends CurrentBalanceClassProps,
        CurrentBalanceBaseProps {}

export const CurrentBalanceBase = ({
    title = "You don't have any subscriptions yet",
    icon = <UnicornSad />,
    classNameContentTitle = '',
    icoBack = <ArrowGoBack size={20} />,
    classNameContentSubscriptions = '',
    classNameButtonBack = '',
    classNameContentBtn = '',
    classNameButtonBackIcon = '',
    ContentCard = '',
    currentBalanceMoney = 0,
    classNameContenIcon = '',
    classNameContenText = '',
    classNameUpcomingWIthdraws = '',
    classNameLinkUpcomingWIthdraws = '',
    classNameContentSuscription = '',
    classNameBtnWIthdraws = '',
    classNameBtn = '',
    defaultValue = [],
    classNameSubscriptionsIcon = '',
    styleTemplateButtonWIthdraws = Theme.styleTemplate ?? '_default',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    btns = [],
    getCurrentBalance,
    currentMounthAmounthBalance,
}: CurrentBalanceProps) => {
    const { user, load } = useUser();
    const { pop } = useNotification();
    const { data: listCards } = useData<DataFormStripe[]>(defaultValue);
    const [transactionRequested] = useState(false);
    const [loader, setLoader] = useState<boolean | undefined>(undefined);
    const [showMethodPay, setShowNewComponent] = useState(false);
    const [showFormCreateCard, setShowFormCreateCard] = useState(false);
    const [showUpcomingWithdraws, setShowUpcomingWithdraws] = useState(false);

    const handleShowUpcomingWithdraws = () => {
        setShowUpcomingWithdraws(true);
    };
    const handleGoBack = () => {
        setShowUpcomingWithdraws(false);
    };

    const handleClickRequestWithdraw = () => {
        setShowNewComponent(true);
    };
    const handleBackClick = () => {
        setShowNewComponent(false);
    };
    const _t = useLang();
    const [btnSeelct, setBtnSeelct] = useState<
        TolinkmeButtonsDataProps | undefined
    >(undefined);

    const submit = async (data: DataFormStripe) => {
        setLoader(true);
        const r: SubmitResult = await POST_STRIPE_CARD({
            user,
            data: {
                firstName: data.firstName,
            },
        });
        if (r) {
            pop({
                message: r?.message ?? '',
                styleTemplate: 'tolinkme',
                type: r.status,
            });
        }
        setLoader(false);
        return r;
    };

    if (!load) {
        return <></>;
    }
    return (
        <>
            {btns.length === 0 ? (
                <>
                    <BalanceMoney
                        getCurrentBalance={getCurrentBalance?.total ?? 0}
                        currentPendingMoney={0}
                    />
                    <div className={classNameContentSubscriptions}>
                        <Text
                            styleTemplate={styleTemplateTitle}
                            className={classNameContentTitle}
                        >
                            {_t(title)}
                        </Text>
                        <span className={classNameSubscriptionsIcon}>
                            {icon}
                        </span>
                    </div>
                </>
            ) : (
                <>
                    {!btnSeelct ? (
                        <>
                            {currentBalanceMoney > 0 &&
                                !transactionRequested &&
                                !showMethodPay && (
                                    <ContentWidth
                                        size={400}
                                        className={classNameContentBtn}
                                    >
                                        <Button
                                            onClick={handleClickRequestWithdraw}
                                        >
                                            {_t('Request Withdraw')}
                                        </Button>
                                    </ContentWidth>
                                )}
                            {showMethodPay ? (
                                <div className={ContentCard}>
                                    <button
                                        onClick={handleBackClick}
                                        className={classNameButtonBack}
                                    >
                                        <span
                                            className={classNameButtonBackIcon}
                                        >
                                            {icoBack}
                                        </span>
                                        {_t('Back')}
                                    </button>
                                    {showFormCreateCard ? (
                                        <>
                                            <FormStripe
                                                loader={loader}
                                                onSubmit={submit}
                                            />
                                        </>
                                    ) : (
                                        <>
                                            {listCards.length == 0 ? (
                                                <div className="">
                                                    <Text
                                                        className={
                                                            classNameContenText
                                                        }
                                                        styleTemplate="tolinkme27"
                                                    >
                                                        {_t(
                                                            `You don't have any withdraw Method yet`
                                                        )}
                                                    </Text>
                                                    <div
                                                        className={
                                                            classNameContenIcon
                                                        }
                                                    >
                                                        <UnicornSad />
                                                    </div>
                                                    <ContentWidth
                                                        className={classNameBtn}
                                                        size={400}
                                                    >
                                                        <Button
                                                            onClick={() => {
                                                                setShowFormCreateCard(
                                                                    true
                                                                );
                                                            }}
                                                        >
                                                            {_t(
                                                                'Add withdraw Method'
                                                            )}
                                                        </Button>
                                                    </ContentWidth>
                                                </div>
                                            ) : (
                                                <>
                                                    <SelectCard cardNumber="5120 6944 7061 6271" />
                                                    <ContentWidth
                                                        className={classNameBtn}
                                                        size={400}
                                                    >
                                                        <Button
                                                            onClick={() => {
                                                                setShowFormCreateCard(
                                                                    true
                                                                );
                                                            }}
                                                        >
                                                            {_t(
                                                                'Add withdraw Method'
                                                            )}
                                                        </Button>
                                                    </ContentWidth>
                                                </>
                                            )}
                                        </>
                                    )}
                                </div>
                            ) : (
                                <>
                                    {showUpcomingWithdraws ? (
                                        <div
                                            className={
                                                classNameContentSuscription
                                            }
                                        >
                                            <button
                                                onClick={handleGoBack}
                                                className={classNameButtonBack}
                                            >
                                                <span
                                                    className={
                                                        classNameButtonBackIcon
                                                    }
                                                >
                                                    {icoBack}
                                                </span>
                                                {_t('Back')}
                                            </button>
                                            <UpcomingWithdraws
                                                moneyWithdraw={100}
                                                cardNumber="4242"
                                            />
                                        </div>
                                    ) : (
                                        <>
                                            <BalanceMoney
                                                getCurrentBalance={
                                                    getCurrentBalance?.total ??
                                                    0
                                                }
                                                currentPendingMoney={0}
                                            />

                                            <div
                                                className={`${classNameUpcomingWIthdraws} UpcomingWIthdraws`}
                                            >
                                                <ButtonAmountSuscriptions
                                                    TotalButtonMonetize={
                                                        btns.length ?? 0
                                                    }
                                                    BalanceMoneyMounth={
                                                        currentMounthAmounthBalance
                                                    }
                                                    AmountSuscription={btns.reduce(
                                                        (a, b) =>
                                                            a +
                                                            (b
                                                                .SubscriptableTransactions
                                                                ?.length ?? 0),
                                                        0
                                                    )}
                                                />
                                                <div
                                                    className={
                                                        classNameLinkUpcomingWIthdraws
                                                    }
                                                >
                                                    <Button
                                                        classNameBtn={
                                                            classNameBtnWIthdraws
                                                        }
                                                        styleTemplate={
                                                            styleTemplateButtonWIthdraws
                                                        }
                                                        onClick={
                                                            handleShowUpcomingWithdraws
                                                        }
                                                    >
                                                        {_t(
                                                            'Upcoming Withdraws'
                                                        )}
                                                    </Button>
                                                </div>

                                                {btns.map((btn, i) => {
                                                    return (
                                                        <>
                                                            <ButtonDetailsSubcriptions
                                                                onSelect={
                                                                    setBtnSeelct
                                                                }
                                                                btn={btn}
                                                                key={i}
                                                            />
                                                        </>
                                                    );
                                                })}
                                            </div>
                                        </>
                                    )}
                                </>
                            )}
                        </>
                    ) : (
                        <>
                            <ButtonDetailsSubcriptions
                                onSelect={setBtnSeelct}
                                btn={btnSeelct}
                                defaultActive={true}
                            />
                        </>
                    )}
                </>
            )}
        </>
    );
};
export default CurrentBalanceBase;
