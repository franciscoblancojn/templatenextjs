import { Story, Meta } from "@storybook/react";

import { RSShareProps, RSShare } from "./index";

export default {
    title: "RS/RSShare",
    component: RSShare,
} as Meta;

const RSShareIndex: Story<RSShareProps> = (args) => (
    <RSShare {...args}>Test Children</RSShare>
);

export const Index = RSShareIndex.bind({});
Index.args = {
    url:"",
    title:""
};
