import { RSShareClassProps } from '@/components/RS/Share/Base';

export const tolinkme: RSShareClassProps = {
    classNameContent: `
        flex 
        flex-column
        flex-gap-row-15
    `,
    classNameLink: `
        flex
        width-35
    `,
    classNameImg: `
        
    `,

    useText: false,
    useRsText: false,
};
