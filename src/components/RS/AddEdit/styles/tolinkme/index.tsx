import { RSAddEditClassProps } from '@/components/RS/AddEdit/Base';

export const tolinkme: RSAddEditClassProps = {
    classNameContent: `
        p-b-1
        m-b-0 m-sm-b-50
    `,
    classNameContentInputAddEdit: `
        m-b-38
    `,
    styleTemplateInput: 'tolinkme',
    classNameContentTitle: `
        m-b-30
        text-uppercase
    `,
    styleTemplateTitle: 'tolinkme',
    typeStyleTitle: 'h11',
    classNameImgInputAddEdit: `
        height-35
        object-fit-contain
    `,
};
