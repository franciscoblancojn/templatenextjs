import { Story, Meta } from "@storybook/react";

import { RS_Social_Type } from "@/data/components/RS";
import { RSAddEditSocial, RSAddEditProps} from "./index";
import log from "@/functions/log";

export default {
    title: "RS/RSAddEdit",
    component: RSAddEditSocial,
} as Meta;

const TemplateSocial: Story<RSAddEditProps<RS_Social_Type>> = (args) => (
    <RSAddEditSocial {...args}>Test Children</RSAddEditSocial>
);

export const Social = TemplateSocial.bind({});
Social.args = {
    onSave: (id) => async (value) => {
        log("id-value",{id,value})
        return {
            status: "ok",
        };
    },
};

