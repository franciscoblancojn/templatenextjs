import { RSLinkClassProps } from '@/components/RS/Link/Base';

export const tolinkme: RSLinkClassProps = {
    classNameContent: `
         p-v-13
        bg-white
        flex
        flex-align-center
        width-p-100
        border-style-solid
        border-transparent
        border-2
    `,
    classNameContentNoRounding: `
        boder-radius-0
    `,
    classNameContentSemiRounded: `
        border-radius-15  
    `,
    classNameContentRounded: `
        border-radius-49  
    `,
    classNameContentImg: `
        flex-4
        flex
        flex-align-center
        p-h-15 p-sm-h-30
    `,
    classNameImg: `
        height-25
        object-fit-contain
    `,
    classNameContentText: `
        flex-4
        flex
        flex-justify-center
        text-capitalize
        text-white-space-nowrap
    `,
    styleTemplateText: 'tolinkme8',
    classNameBlocked: `
        filter-grayscale-10
    `,
    styleTemplateTextBlocked: 'tolinkme9',
};
