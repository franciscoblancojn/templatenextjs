import {
    RSLinkConfigBase,
    RSLinkConfigBaseProps,
} from '@/components/RS/LinkConfig/Base';

import * as styles from '@/components/RS/LinkConfig/styles';

import { Theme, ThemesType } from '@/config/theme';
import { useMemo } from 'react';

export const LinkConfigStyle = { ...styles } as const;

export type LinkConfigStyles = keyof typeof LinkConfigStyle;

export interface RSLinkConfigProps extends RSLinkConfigBaseProps {
    styleTemplate?: LinkConfigStyles | ThemesType;
}

export const RSLinkConfig = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',

    ...props
}: RSLinkConfigProps) => {
    const Styles = useMemo(
        () =>
            LinkConfigStyle[styleTemplate as LinkConfigStyles] ??
            LinkConfigStyle._default,
        [styleTemplate]
    );
    return (
        <>
            <RSLinkConfigBase {...props} {...Styles} />
        </>
    );
};

export default RSLinkConfig;
