import { RSLinkConfigClassProps } from '@/components/RS/LinkConfig/Base';

export const tolinkme: RSLinkConfigClassProps = {
    classNameContent: `
        width-p-100
        pos-r
        p-11
        p-h-17
        p-t-25
        p-l-35
        p-r-20
        bg-white
        border-radius-29
        flex
        flex-column
        width-p-100
        box-shadow
        box-shadow-y-3
        box-shadow-b-6
        box-shadow-c-black16
    `,
    styleContent: {},
    classNameActive: `
        flex
        flex-justify-between
        flex-nowrap
        flex-align-center
        flex-gap-column-10
    `,
    styleTemplateActive: 'tolinkme2',
    classNameContentTitleRs: `
        flex
        flex-align-center
        text-capitalize
        flex-gap-column-5
    `,
    classNameImgSelected: `
        height-27
    `,

    classNameContentSelectRS: `
        width-p-100
        m-b-15
        bg-whiteFour
        border-radius-10
        p-h-10
        p-v-5
        text-capitalize
        m-t-15
    `,
    classNameContentSelectRS2: `
        width-p-100
        m-b-15
        bg-whiteFour
        border-radius-10
        p-h-10
        p-v-5
        text-capitalize
    `,
    styleTemplateSelectRS: 'tolinkmeSimple',
    classNameImgSelect: `
        width-20
        m-r-5
        object-fit-contain
    `,
    classNameContentUrl: `
        width-p-100
        m-b-12
    `,
    styleTemplateUrl: 'tolinkmeSimple2',
    classNameContentFooter: `
        flex
        flex-justify-between
    `,

    classNameDelete: `
        pointer  
        color-error-hover
    `,
    sizeDelete: 18,
    classNameMove: `
        pos-a
        top-0
        left-0
        bottom-0
        m-auto
        flex
        flex-align-center
        cursor-move
        p-h-12
        move
    `,
    sizeMove: 10,
    styleTemplateText: 'tolinkme3',

    classNameContentCustomTextImg: `
        flex
        flex-nowrap
        flex-gap-column-5
        flex-align-center
        width-p-100
        m-b-15
    `,
    styleTemplateCustomText: 'tolinkmeSimple2Bold',
    styleTemplateCustomImg: 'tolinkmeSmall',

    styleTemplateCrow: 'tolinkmeCrown',
    styleTemplateFile: 'tolinkme',
};
