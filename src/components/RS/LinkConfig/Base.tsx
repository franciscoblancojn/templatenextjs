import CSS from 'csstype';
import { InputTextStyles, InputText } from '@/components/Input/Text';
import {
    InputCheckbox,
    InputCheckboxStyles,
} from '@/components/Input/Checkbox';

import { Trash } from '@/svg/trash';
import { Move } from '@/svg/move';

import { Theme, ThemesType } from '@/config/theme';
import { TypeRS, RS_All } from '@/data/components/RS';
import { useLang } from '@/lang/translate';
import { useEffect, useMemo } from 'react';
import Image from '@/components/Image';
import { useData } from 'fenextjs-hook/cjs/useData';
import Text, { TextStyles } from '@/components/Text';
import { InputAvatar, InputAvatarStyles } from '@/components/Input/Avatar';
import SelectPopup from '@/components/Input/SelectPopup';
import useLocalStorage from 'uselocalstoragenextjs';
import { useNotification } from '@/hook/useNotification';
import PopupMonetize from '@/components/Input/PopupMonetize';
import {
    InputPopupFile,
    InputPopupFileStyles,
} from '@/components/Input/InputPopupFile';
import { DataFormMonetize } from '@/interfaces/FormMonetize';
import PopupAddons from '@/components/Input/PopupAddons';
import { Addons } from '@/interfaces/Addons';

export interface RSLinkConfigClassProps {
    classNameContent?: string;
    styleContent?: CSS.Properties;

    classNameContentTitleRs?: string;
    classNameImgSelected?: string;
    styleTemplateText?: TextStyles | ThemesType;
    classNameContentSelectRS?: string;
    classNameContentSelectRS2?: string;
    styleTemplateSelectRS?: InputTextStyles | ThemesType;
    classNameImgSelect?: string;

    classNameContentUrl?: string;
    styleTemplateUrl?: InputTextStyles | ThemesType;
    styleTemplateFile?: InputPopupFileStyles | ThemesType;

    classNameContentCustomTextImg?: string;
    styleTemplateCustomText?: InputTextStyles | ThemesType;
    styleTemplateCustomImg?: InputAvatarStyles | ThemesType;

    classNameMove?: string;
    sizeMove?: number;

    classNameContentFooter?: string;
    classNameDelete?: string;
    sizeDelete?: number;

    classNameActive?: string;
    styleTemplateActive?: InputCheckboxStyles | ThemesType;

    styleTemplateCrow?: InputCheckboxStyles | ThemesType;
}

export interface RSLinkConfigDataProps {
    uuid?: string;
    rs?: TypeRS | undefined;
    url?: string;
    active?: boolean;
    i?: number;
    order?: number;
    delete?: boolean;
    customImg?: string;
    customTitle?: string;
    isPrincipal?: boolean;
    monetize?: boolean;
    Select?: any;
    monetizeData?: DataFormMonetize;
    useMonetize?: boolean;
    isAddon?: boolean;
    addons?: Addons;
    ButtonPriceDetails?: {
        basePrice?: number;
        buttonUuid?: string;
        createdAt?: string;
        description?: string;
        period?: any;
        priceStripeId: string;
        recurrence?: string;
        type?: string;
        updatedAt?: string;
        uuid?: string;
    };
}

export interface RSLinkConfigBaseProps extends RSLinkConfigDataProps {
    onDelete?: (data: RSLinkConfigDataProps) => void;
    onChange?: (data: RSLinkConfigDataProps) => void;
    move?: boolean;
    nItemsPrincipal?: number;
}

export interface RSLinkConfigProps
    extends RSLinkConfigBaseProps,
        RSLinkConfigClassProps {}

export const RSLinkConfigBase = ({
    classNameContent = '',
    styleContent = {},

    classNameContentTitleRs = '',
    classNameImgSelected = '',
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    classNameContentSelectRS = '',
    classNameContentSelectRS2 = '',
    styleTemplateSelectRS = Theme?.styleTemplate ?? '_default',
    classNameImgSelect = '',

    classNameContentUrl = '',
    styleTemplateUrl = Theme?.styleTemplate ?? '_default',
    styleTemplateFile = Theme?.styleTemplate ?? '_default',

    classNameContentCustomTextImg = '',
    styleTemplateCustomText = Theme?.styleTemplate ?? '_default',
    styleTemplateCustomImg = Theme?.styleTemplate ?? '_default',

    classNameMove = '',
    sizeMove = 20,

    classNameContentFooter = '',
    classNameDelete = '',
    sizeDelete = 20,

    classNameActive = '',
    styleTemplateActive = Theme?.styleTemplate ?? '_default',

    styleTemplateCrow = Theme?.styleTemplate ?? '_default',

    rs = undefined,
    active = true,
    url = '',
    order,
    uuid,
    customImg = '',
    customTitle = '',
    isPrincipal = false,
    delete: isDelete = false,
    Select = {
        monetize: true,
    },
    onDelete = (data: RSLinkConfigBaseProps) => data,
    onChange = (data: RSLinkConfigBaseProps) => data,
    move = true,
    nItemsPrincipal = -1,
    monetizeData,
    useMonetize = false,
    addons,
}: RSLinkConfigProps) => {
    const _t = useLang();
    const { pop } = useNotification();

    const { value } = useLocalStorage({
        name: 'nItemsPrincipal',
        defaultValue: nItemsPrincipal,
        parse: parseInt,
    });
    const { data, onChangeData, isChange } = useData<
        RSLinkConfigBaseProps,
        Addons
    >({
        rs,
        url,
        active,
        order,
        uuid,
        customImg,
        customTitle,
        isPrincipal,
        monetizeData,
        addons,
    });

    useEffect(() => {
        if (isChange) {
            onChange(data);
        }
    }, [data]);

    const ComponentSelect = useMemo(() => {
        const rsSelect = RS_All.find((e) => e.id == rs);
        return (
            <>
                <SelectPopup
                    placeholder={_t('Select Social Network')}
                    defaultValue={{
                        text: rsSelect?.name ?? rs ?? '',
                        value: rsSelect?.id ?? rs ?? '',
                        ...(rsSelect
                            ? {
                                  html: (
                                      <>
                                          {rsSelect?.img ? (
                                              <Image
                                                  src={'rs/' + rsSelect.img}
                                                  className={classNameImgSelect}
                                              />
                                          ) : (
                                              <></>
                                          )}
                                          {rsSelect?.name ?? rs ?? ''}
                                      </>
                                  ),
                              }
                            : {}),
                    }}
                    options={RS_All.map((r) => ({
                        text: r.name,
                        value: r.id,
                        html: (
                            <>
                                <Image
                                    src={'rs/' + r.img}
                                    className={classNameImgSelect}
                                />
                                {r.name}
                            </>
                        ),
                        title: r?.title,
                    }))}
                    onChange={(e) => {
                        onChangeData('rs')(e.value);
                    }}
                />
            </>
        );
    }, [rs, RS_All, styleTemplateSelectRS]);

    const ComponentSelectMonetize = useMemo(() => {
        if (!data.rs) {
            return <></>;
        }
        if (!useMonetize) {
            return <></>;
        }
        return (
            <>
                <div className={classNameContentSelectRS2}>
                    <PopupMonetize
                        defaultValue={data?.monetizeData}
                        onChange={onChangeData('monetizeData')}
                    />
                </div>
            </>
        );
    }, [styleTemplateSelectRS, data, useMonetize]);

    const ComponentSelectCustomAddons = useMemo(() => {
        return (
            <>
                <div
                    className={`
                    width-p-100
                    m-b-15
                    m-t-15
                    bg-whiteFour
                    border-radius-10
                    p-h-10
                    p-v-5
                    text-capitalize
                `}
                >
                    <PopupAddons
                        defaultValue={data?.addons}
                        onChange={onChangeData('addons')}
                    />
                </div>
            </>
        );
    }, [data]);

    return (
        <>
            {isDelete ? (
                <></>
            ) : (
                <>
                    <div className={classNameContent} style={styleContent}>
                        {move && (
                            <div className={classNameMove}>
                                <Move size={sizeMove} />
                            </div>
                        )}
                        <div className={classNameActive}>
                            {data.rs ? (
                                <>
                                    {['custom', 'file'].includes(data.rs) ? (
                                        <>
                                            <div
                                                className={
                                                    classNameContentCustomTextImg
                                                }
                                            >
                                                <InputAvatar
                                                    defaultValue={{
                                                        fileData:
                                                            data.customImg,
                                                        text: '',
                                                    }}
                                                    onSubmit={(d) => {
                                                        onChangeData(
                                                            'customImg'
                                                        )(d?.fileData);
                                                    }}
                                                    styleTemplate={
                                                        styleTemplateCustomImg
                                                    }
                                                />
                                                <InputText
                                                    defaultValue={
                                                        data.customTitle
                                                    }
                                                    styleTemplate={
                                                        styleTemplateCustomText
                                                    }
                                                    onChange={onChangeData(
                                                        'customTitle'
                                                    )}
                                                    placeholder={_t(
                                                        'Custom Title'
                                                    )}
                                                />
                                            </div>
                                        </>
                                    ) : (
                                        <>
                                            <div
                                                className={
                                                    classNameContentTitleRs
                                                }
                                            >
                                                <Image
                                                    src={
                                                        'rs/' +
                                                        (
                                                            data.rs ?? ''
                                                        ).toLocaleLowerCase() +
                                                        '.png'
                                                    }
                                                    className={
                                                        classNameImgSelected
                                                    }
                                                />
                                                <Text
                                                    styleTemplate={
                                                        styleTemplateText
                                                    }
                                                >
                                                    {data.rs}
                                                </Text>
                                            </div>
                                        </>
                                    )}
                                </>
                            ) : (
                                <>
                                    <Text styleTemplate={styleTemplateText}>
                                        {_t('Choose a Button')}
                                    </Text>
                                </>
                            )}

                            {data.rs ? (
                                <InputCheckbox
                                    defaultValue={active}
                                    styleTemplate={styleTemplateActive}
                                    onChange={onChangeData('active')}
                                />
                            ) : (
                                <></>
                            )}
                        </div>

                        {data?.rs == 'addon' ? (
                            <></>
                        ) : (
                            <>
                                <div className={classNameContentSelectRS}>
                                    {ComponentSelect}
                                </div>
                            </>
                        )}

                        {data?.rs == 'addon' ? (
                            <>{ComponentSelectCustomAddons}</>
                        ) : (
                            <></>
                        )}
                        {data?.rs == 'addon' ? (
                            <></>
                        ) : (
                            <>
                                {Select.monetize ? (
                                    <>{ComponentSelectMonetize}</>
                                ) : (
                                    <></>
                                )}
                            </>
                        )}

                        <div className={classNameContentUrl}>
                            {data.rs ? (
                                <>
                                    {data.rs == 'file' ? (
                                        <>
                                            <div
                                                className={
                                                    classNameContentSelectRS2
                                                }
                                            >
                                                <InputPopupFile
                                                    styleTemplate={
                                                        styleTemplateFile
                                                    }
                                                    onChange={(e) => {
                                                        onChangeData('url')(
                                                            e.fileData
                                                        );
                                                    }}
                                                    defaultValue={{
                                                        fileData: url,
                                                    }}
                                                />
                                            </div>
                                        </>
                                    ) : (
                                        <>
                                            {data.rs == 'addon' ? (
                                                <></>
                                            ) : (
                                                <>
                                                    <InputText
                                                        defaultValue={url}
                                                        styleTemplate={
                                                            styleTemplateUrl
                                                        }
                                                        onBlur={onChangeData(
                                                            'url'
                                                        )}
                                                        placeholder={_t(
                                                            'URL - https://example.com/'
                                                        )}
                                                    />
                                                </>
                                            )}
                                        </>
                                    )}
                                </>
                            ) : (
                                <></>
                            )}
                        </div>
                        <div className={classNameContentFooter}>
                            <span>
                                {data.rs ? (
                                    <>
                                        {data.rs == 'addon' ? (
                                            <></>
                                        ) : (
                                            <>
                                                <InputCheckbox
                                                    defaultValue={
                                                        data.isPrincipal
                                                    }
                                                    styleTemplate={
                                                        styleTemplateCrow
                                                    }
                                                    onChange={onChangeData(
                                                        'isPrincipal'
                                                    )}
                                                    label={_t('Boton Primario')}
                                                    onValidateCheck={() => {
                                                        if ((value ?? 0) >= 6) {
                                                            throw pop({
                                                                message: _t(
                                                                    'Solo se puedente tener 6 botones principales'
                                                                ),
                                                                type: 'error',
                                                            });
                                                        }
                                                        if (
                                                            [
                                                                'custom',
                                                                'file',
                                                            ].includes(
                                                                data?.rs ?? ''
                                                            )
                                                        ) {
                                                            throw pop({
                                                                message: _t(
                                                                    'No puedes hacer Principal un Botton Custom'
                                                                ),
                                                                type: 'error',
                                                            });
                                                        }
                                                    }}
                                                />
                                            </>
                                        )}
                                    </>
                                ) : (
                                    <></>
                                )}
                            </span>
                            <span
                                className={classNameDelete}
                                onClick={() => {
                                    onDelete(data);
                                }}
                            >
                                <Trash size={sizeDelete} />
                            </span>
                        </div>
                    </div>
                </>
            )}
        </>
    );
};
export default RSLinkConfigBase;
