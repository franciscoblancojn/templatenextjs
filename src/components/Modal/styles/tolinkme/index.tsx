import Image from '@/components/Image';
import { ModalClassProps } from '@/components/Modal/Base';
import { Close } from '@/svg/close';

export const tolinkme: ModalClassProps = {
    classNameContent: `
        pos-f
        inset-0
        width-p-100
        height-p-100
        bg-black-144
        oveflow-auto
        flex
        flex-align-center
        flex-justify-center
        p-sm-15
        font-nunito
        transition-5
    `,
    classNameContentActive: `
        z-index-10
    `,
    classNameContentInactive: `
        opacity-0
        z-index--1
    `,
    stylesContentModal: {},
    classNameContentModal: `
        bg-white
        p-v-5 p-h-15
        border-radius-15
        pos-r
        width-p-min-100
        width-sm-p-min-50
        transition-4
    `,
    classNameContentModalActive: `
        
    `,
    classNameContentModalInactive: `
        transform
        transform-translate-X-vw-100
    `,
    classNameHeader: `
        p-v-15
        border-0
        border-b-1
        border-style-solid
    `,
    classNameBody: `
        p-v-25  
    `,
    classNameFooter: `
        p-v-15
        border-0
        border-t-1
        border-style-solid
    `,
    classNameClose: `
        pos-a
        top-10
        right-15
        cursor-pointer
    `,
    iconClose: <Close size={10} />,
};

export const tolinkmeFull: ModalClassProps = {
    classNameContent: `
        Modal-tolinkmeFull
        pos-f
        inset-0
        width-p-100
        height-p-100
        overflow-auto
    `,
    classNameContentActive: `
        z-index-10
    `,
    classNameContentInactive: `
        opacity-0
        z-index--1
    `,
    stylesContentModal: {},
    classNameContentModal: `
        width-p-100
    `,
    classNameContentModalActive: `
        
    `,
    classNameContentModalInactive: `
    `,
    classNameHeader: `
        p-v-15
        border-0
        border-b-1
        border-style-solid
    `,
    classNameBody: `
        p-v-25  
    `,
    classNameFooter: `
        p-v-15
        border-0
        border-t-1
        border-style-solid
    `,
    classNameClose: `
        pos-a
        bottom-20
        right-0
        left-0
        m-auto
        cursor-pointer
        color-white
        bg-blackTwo
        width-35
        height-35
        border-radius-100
        flex
        flex-align-center
        flex-justify-center
    `,
    iconClose: (
        <>
            <Close size={15} />
            <Image
                src="me_logo.svg"
                className={`
                    pos-a
                    top-p-100
                    width-25
                `}
            />
        </>
    ),
    delayHidden: 0,
    delayShow: 0,
};

export const tolinkme2: ModalClassProps = {
    classNameContent: `
        pos-f
        inset-0
        width-p-100
        height-p-100
        bg-black-144
        oveflow-auto
        flex
        flex-align-center
        flex-justify-center
        p-sm-15
        font-nunito
        transition-2
      
    `,
    classNameContentActive: `
        z-index-10
        p-h-20
    `,
    classNameContentInactive: `
        opacity-0
        z-index--1
    `,
    stylesContentModal: {},
    classNameContentModal: `
        bg-white
        p-v-5 p-h-15
        border-radius-15
        pos-r
        width-p-min-100
        width-sm-p-min-50
        transform
        
    `,
    classNameContentModalActive: `
        transform-translate-X-p-0
    `,
    classNameContentModalInactive: `
        transform-translate-X-vw-100   
    `,
    classNameHeader: `
        p-v-15
        border-0
        border-b-1
        border-style-solid
    `,
    classNameBody: `
        p-v-25  
    `,
    classNameFooter: `
        p-v-15
        border-0
        border-t-1
        border-style-solid
    `,
    classNameClose: `
        pos-a
        top-10
        right-15
        cursor-pointer
        color-teal-hover
    `,
    iconClose: <Close size={12} />,
};
