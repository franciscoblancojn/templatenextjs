import { useMemo } from 'react';

import * as styles from '@/components/Modal/styles';

import { Theme, ThemesType } from '@/config/theme';

import { ModalBaseProps, ModalBase } from '@/components/Modal/Base';

export const ModalStyle = { ...styles } as const;

export type ModalStyles = keyof typeof ModalStyle;

export interface ModalProps extends ModalBaseProps {
    styleTemplate?: ModalStyles | ThemesType;
}

export const Modal = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ModalProps) => {
    const Style = useMemo(
        () => ModalStyle[styleTemplate as ModalStyles] ?? ModalStyle._default,
        [styleTemplate]
    );

    return <ModalBase {...Style} {...props} />;
};
export default Modal;
