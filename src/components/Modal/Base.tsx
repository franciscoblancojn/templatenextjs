import CSS from 'csstype';

import { useState, PropsWithChildren, useEffect } from 'react';

import { Close } from '@/svg/close';

export interface ModalClassProps {
    classNameContent?: string;
    classNameContentActive?: string;
    classNameContentInactive?: string;

    stylesContentModal?: CSS.Properties;
    classNameContentModal?: string;
    classNameContentModalActive?: string;
    classNameContentModalInactive?: string;

    classNameHeader?: string;
    classNameBody?: string;
    classNameFooter?: string;

    classNameClose?: string;
    iconClose?: any;

    delayShow?: number;
    delayHidden?: number;
}

export interface ModalBaseProps extends PropsWithChildren {
    header?: any;
    footer?: any;
    onActive?: boolean;
    onClose?: () => void;
    showClose?: boolean;
}

export interface ModalProps extends ModalClassProps, ModalBaseProps {}

export const ModalBase = ({
    classNameContent = '',
    classNameContentActive = '',
    classNameContentInactive = '',
    stylesContentModal = {},
    classNameContentModal = '',
    classNameContentModalActive = '',
    classNameContentModalInactive = '',
    classNameHeader = '',
    classNameBody = '',
    classNameFooter = '',
    classNameClose = '',
    iconClose = <Close />,
    header = null,
    children,
    footer = null,
    delayShow = 500,
    delayHidden = 500,
    onActive = false,
    onClose = () => {},
    showClose = true,
}: ModalProps) => {
    const [contentActive, setContentActive] = useState<boolean>(false);
    const [active, setActive] = useState<boolean>(false);

    const onShow = async () => {
        setContentActive(true);
        await new Promise((r) => setTimeout(r, delayShow));
        setActive(true);
    };
    const onHidden = async () => {
        setContentActive(false);
        await new Promise((r) => setTimeout(r, delayHidden));
        setActive(false);
        onClose();
    };

    useEffect(() => {
        onActive ? onShow() : onHidden();
    }, [onActive]);

    return (
        <>
            <div
                className={`contentModal-- ${
                    active
                        ? 'active '
                        : `
                            transform
                            transform-translate-vw-100

                        `
                } ${classNameContent} ${
                    contentActive
                        ? classNameContentActive
                        : classNameContentInactive
                }`}
                onClick={(e) => {
                    const ele: any = e.target;
                    if (ele.classList.value.indexOf('contentModal--') > -1) {
                        onHidden();
                    }
                }}
            >
                <div
                    style={stylesContentModal}
                    className={`${classNameContentModal} ${
                        active
                            ? classNameContentModalActive
                            : classNameContentModalInactive
                    }`}
                >
                    {header && <div className={classNameHeader}>{header}</div>}
                    <div className={classNameBody}>{children}</div>
                    {footer && <div className={classNameFooter}>{footer}</div>}

                    {showClose ? (
                        <div className={classNameClose} onClick={onHidden}>
                            {iconClose}
                        </div>
                    ) : (
                        <></>
                    )}
                </div>
            </div>
        </>
    );
};
export default ModalBase;
