import { Story, Meta } from "@storybook/react";

import { ModalProps, Modal } from "./index";

export default {
    title: "Modal/Modal",
    component: Modal,
} as Meta;

const ModalIndex: Story<ModalProps> = (args) => (
    <Modal {...args}/>
);

export const Index = ModalIndex.bind({});
Index.args = {
    header:"Header",
    children:"Body",
    footer:"Footer",
    onActive:true
};
