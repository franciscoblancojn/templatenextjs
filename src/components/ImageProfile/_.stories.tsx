import { Story, Meta } from "@storybook/react";

import { ImageProfileProps, ImageProfile } from "./index";

export default {
    title: "ImageProfile/ImageProfile",
    component: ImageProfile,
} as Meta;

const ImageProfileIndex: Story<ImageProfileProps> = (args) => (
    <ImageProfile {...args}>Test Children</ImageProfile>
);

export const Index = ImageProfileIndex.bind({});
Index.args = {};
