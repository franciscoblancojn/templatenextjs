import { useMemo } from 'react';

import * as styles from '@/components/ImageProfile/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ImageProfileBaseProps,
    ImageProfileBase,
} from '@/components/ImageProfile/Base';

export const ImageProfileStyle = { ...styles } as const;

export type ImageProfileStyles = keyof typeof ImageProfileStyle;

export interface ImageProfileProps extends ImageProfileBaseProps {
    styleTemplate?: ImageProfileStyles | ThemesType;
}

export const ImageProfile = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ImageProfileProps) => {
    const Style = useMemo(
        () =>
            ImageProfileStyle[styleTemplate as ImageProfileStyles] ??
            ImageProfileStyle._default,
        [styleTemplate]
    );

    return <ImageProfileBase {...Style} {...props} />;
};
export default ImageProfile;
