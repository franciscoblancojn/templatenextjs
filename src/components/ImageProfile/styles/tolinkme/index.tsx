import { ImageProfileClassProps } from '@/components/ImageProfile/Base';

export const tolinkme: ImageProfileClassProps = {
    LogoContentWidthProps: {
        className: `
            width-md-p-max-50
            width-p-max-27
            m-h-auto
            flex 
            flex-justify-center
        `,
        size: 180,
    },
    ContentWidthProps: {
        size: 120,
        className: `
            pos-r
            m-h-auto
            m-b-15
            bg-whiteThree
            overflow-hidden
            color-white
        `,
    },
    classNameContentImgBorderPreview: `
        pos-a
        inset-0
        width-p-100
        height-p-100
    `,
    classNameContentImgPreview: `
        width-p-100
        p-p-b-100
        pos-r
    `,
    classNameContentImgAPreview: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        flex
        flex-align-center
        flex-justify-center  
    `,
    classNameImgPreview: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        object-fit-cover
    `,
    sizeIconNotImgPreview: 80,
};
