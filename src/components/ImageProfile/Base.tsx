import { ConfigImgProfile } from '@/interfaces/ConfigImgProfile';
import { useMemo } from 'react';
import { ContentWidth, ContentWidthProps } from '../ContentWidth';
import { BorderRadiusButtonValues } from '@/interfaces/Button';
import { UserAccount } from '@/svg/userAccount';
import Img from '../Img';
import Space from '../Space';

export interface ImageProfileClassProps {
    LogoContentWidthProps?: ContentWidthProps;
    ContentWidthProps?: ContentWidthProps;
    classNameContentImgPreview?: string;
    classNameContentImgBorderPreview?: string;
    classNameContentImgAPreview?: string;
    classNameImgPreview?: string;
    sizeIconNotImgPreview?: number;
}

export interface ImageProfileBaseProps {
    img?: string;
    data?: ConfigImgProfile;
}

export interface ImageProfileProps
    extends ImageProfileClassProps,
        ImageProfileBaseProps {}

export const ImageProfileBase = ({
    LogoContentWidthProps = {},
    ContentWidthProps = {},
    classNameContentImgPreview = '',
    classNameContentImgAPreview = '',
    classNameContentImgBorderPreview = '',
    classNameImgPreview = '',
    sizeIconNotImgPreview = 20,

    img = '',
    data,
}: ImageProfileProps) => {
    const dataMemo = useMemo(() => {
        const bg = {
            color: data?.bg?.color,
            gradient: `linear-gradient(${data?.bg?.gradient?.deg}deg, ${data?.bg?.gradient?.color1}, ${data?.bg?.gradient?.color2})`,
            img: '',
            video: '',
        };

        return {
            imgBorderColor: {
                background: bg[data?.bg?.type ?? 'color'],
                opacity: (data?.bg?.opacity ?? 100) / 100,
            },
            borderRadius:
                BorderRadiusButtonValues[data?.borderType ?? 'rounded'],
            sizeBorder: `${data?.borderSize}px`,
        };
    }, [data]);
    return (
        <>
            {data?.useLogoTolinkme ? (
                <>
                    <ContentWidth {...LogoContentWidthProps}>
                        <Img src="logo.png" />
                    </ContentWidth>
                    <Space size={15} />
                </>
            ) : (
                <></>
            )}

            <ContentWidth
                {...ContentWidthProps}
                className={`${ContentWidthProps.className} ${dataMemo?.borderRadius}`}
                style={{
                    padding: dataMemo?.sizeBorder,
                }}
            >
                <div
                    className={classNameContentImgBorderPreview}
                    style={dataMemo?.imgBorderColor}
                ></div>
                <div className={`${classNameContentImgPreview} `}>
                    <div className={classNameContentImgAPreview}>
                        {img == '' ? (
                            <>
                                <UserAccount size={sizeIconNotImgPreview} />
                            </>
                        ) : (
                            <>
                                <img
                                    src={img}
                                    className={`${classNameImgPreview} ${dataMemo?.borderRadius}`}
                                />
                            </>
                        )}
                    </div>
                </div>
            </ContentWidth>
        </>
    );
};
export default ImageProfileBase;
