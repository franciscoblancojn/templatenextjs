import { Story, Meta } from "@storybook/react";

import { TabProps, Tab } from "./index";

export default {
    title: "Tab/Tab",
    component: Tab,
} as Meta; 

const TabIndex: Story<TabProps> = (args) => <Tab {...args}>Test Children</Tab>;

export const Index = TabIndex.bind({});
Index.args = {
    items: [
        {
            tab: "tab1",
            content: "Tab Content 1",
        },
        {
            tab: "tab2",
            content: "Tab Content 2",
        },
        {
            tab: "tab3",
            content: "Tab Content 3",
        },
        {
            tab: "tab1",
            content: "Tab Content 1",
        },
        {
            tab: "tab2",
            content: "Tab Content 2",
        },
        {
            tab: "tab3",
            content: "Tab Content 3",
        },
        {
            tab: "tab1",
            content: "Tab Content 1",
        },
        {
            tab: "tab2",
            content: "Tab Content 2",
        },
        {
            tab: "tab3",
            content: "Tab Content 3",
        },
        {
            tab: "tab1",
            content: "Tab Content 1",
        },
        {
            tab: "tab2",
            content: "Tab Content 2",
        },
        {
            tab: "tab3",
            content: "Tab Content 3",
        },
    ],
};
