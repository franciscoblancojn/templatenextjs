# Tab

## Import

```js
import { Tab, TabStyles } from '@/components/Tab';
```

## Props

```tsx
export interface TabContentProps {
    tab: any;
    content: any;
}

export interface TabProps {
    styleTemplate?: TabStyles;
    items?: TabContentProps[];
    defaultSelect?: number;
    extraTab?: any;
}
```

## Use

```js
<Tab
    items={[
        {
            tab: 'tab1',
            content: 'Tab Content 1',
        },
        {
            tab: 'tab2',
            content: 'Tab Content 2',
        },
        {
            tab: 'tab3',
            content: 'Tab Content 3',
        },
        {
            tab: 'tab1',
            content: 'Tab Content 1',
        },
        {
            tab: 'tab2',
            content: 'Tab Content 2',
        },
        {
            tab: 'tab3',
            content: 'Tab Content 3',
        },
        {
            tab: 'tab1',
            content: 'Tab Content 1',
        },
        {
            tab: 'tab2',
            content: 'Tab Content 2',
        },
        {
            tab: 'tab3',
            content: 'Tab Content 3',
        },
        {
            tab: 'tab1',
            content: 'Tab Content 1',
        },
        {
            tab: 'tab2',
            content: 'Tab Content 2',
        },
        {
            tab: 'tab3',
            content: 'Tab Content 3',
        },
    ]}
/>
```
