import { useMemo } from 'react';

import * as styles from '@/components/Tab/styles';

import { Theme, ThemesType } from '@/config/theme';

import { TabBaseProps, TabBase } from '@/components/Tab/Base';

export const TabStyle = { ...styles } as const;

export type TabStyles = keyof typeof TabStyle;

export interface TabProps extends TabBaseProps {
    styleTemplate?: TabStyles | ThemesType;
}

export const Tab = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: TabProps) => {
    const Style = useMemo(
        () => TabStyle[styleTemplate as TabStyles] ?? TabStyle._default,
        [styleTemplate]
    );

    return <TabBase {...Style} {...props} />;
};
export default Tab;
