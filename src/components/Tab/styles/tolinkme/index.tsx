import { TabClassProps } from '@/components/Tab/Base';

export const tolinkme: TabClassProps = {
    classNameContent: `
        
    `,
    classNameContentTab: `
        bg-whiteTwo
        border-radius-10
        p-h-15
        flex
        flex-nowrap
        flex-gap-column-20
        overflow-auto
        overflow-no-bar
        pos-r
    `,
    classNameTab: `
        font-13
        font-nunito
        font-w-900
        p-v-18
        p-h-4
        pointer
        transition-5
        text-white-space-nowrap
        pos-r
        color-brightPink-hover
    `,
    classNameTabActive: `
        color-brightPink
    `,
    classNameTabNotActive: `
        color-greyishBrownFour
    `,
    classNameTabBar: `
        pos-a
        left-0
        bottom-0
        width-p-100
        height-4
        border-radius-100
        bg-brightPink
        transition-5
        transform
    `,
    classNameTabBarActive: `
    `,
    classNameTabBarNotActive: `
        transform-scale-X-0
    `,
    classNameTabContent: `
        bg-white
        p-v-30
        p-h-15
        font-nunito
    `,
};
export const tolinkme2: TabClassProps = {
    classNameContent: `
        
    `,
    classNameContentTab: `
        bg-whiteTwo
        p-h-15
        flex
        flex-nowrap
        flex-gap-column-100
        overflow-auto
        overflow-no-bar
        pos-r
    `,
    classNameTab: `
        font-30
        font-nunito
        font-w-900
        p-v-18
        p-h-4
        pointer
        transition-5
        text-white-space-nowrap
        pos-r
    `,
    classNameTabActive: `
        color-brightPink
    `,
    classNameTabNotActive: `
        color-greyishBrownFour
    `,
    classNameTabBar: `
        pos-a
        left-0
        bottom-0
        width-p-100
        height-4
        border-radius-100
        bg-brightPink
        transition-5
        transform
    `,
    classNameTabBarActive: `
    `,
    classNameTabBarNotActive: `
        transform-scale-X-0
    `,
    classNameTabContent: `
        bg-white
        p-v-30
        p-h-15
        font-nunito
    `,
};

export const tolinkme3: TabClassProps = {
    classNameContent: `
        
    `,
    classNameContentTab: `
        bg-whiteTwo
        p-h-15
        flex
        flex-nowrap
        flex-gap-column-100
        overflow-auto
        overflow-no-bar
        pos-r
    `,
    classNameTab: `
        font-30
        font-nunito
        font-w-900
        p-v-18
        p-h-4
        pointer
        transition-5
        text-white-space-nowrap
        pos-r
    `,
    classNameTabActive: `
        color-brightPink
    `,
    classNameTabNotActive: `
        color-greyishBrownFour
    `,
    classNameTabBar: `
        pos-a
        left-0
        bottom-0
        width-p-100
        height-4
        border-radius-100
        bg-brightPink
        transition-5
        transform
    `,
    classNameTabBarActive: `
    `,
    classNameTabBarNotActive: `
        transform-scale-X-0
    `,
    classNameTabContent: `
        bg-white
        p-v-30
        p-h-15
        font-nunito
    `,
};
