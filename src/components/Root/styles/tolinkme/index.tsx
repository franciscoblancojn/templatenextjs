import { RootClassProps } from '@/components/Root/Base';

import { createGlobalStyle } from 'styled-components';

export const tolinkme: RootClassProps = {
    styledComponent: createGlobalStyle`
        ::-webkit-scrollbar {
            width: 7px;
            background: transparent;
        }
        ::-webkit-scrollbar-track {
            background-color: var(--whiteThree);
        }
        ::-webkit-scrollbar-thumb {
            background-color: var(--darkAqua);
            border-radius:10px;
        }
        html {
            font-size: 16px;
            overflow: auto;
            @media (min-width: 575px) {
                font-size: 16px;
            }
            @media (min-width: 768px) {
                font-size: 16px;
            }
            @media (min-width: 992px) {
                font-size: 16px;
            }
            @media (min-width: 1024px) {
                font-size: 16px;
            }
            @media (min-width: 1440px) {
                font-size: 16px;
            }
            @media (min-width: 1680px) {
                font-size: 18px;
            }
            @media (min-width: 1920px) {
                font-size: 19px;
            }
        }
        `,
};
