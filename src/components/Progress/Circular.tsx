import { PropsWithChildren } from 'react';

const BaseStyle = {
    classNameProgressCircular: `
        progress-circular 
        width-p-100 
        pos-r 
        flex flex-justify-center flex-align-center 
        text-center 
        font-montserrat 
        box-shadow
        box-shadow-inset
        box-shadow-s-10
        border-radius-p-100
    `,
    classNameProcentaje: `
        progress-circular-porcentaje 
        pos-a
        top-0 
        left-0 
        right-0 
        bottom-0 
        m-auto 
        width-p-100 
        flex 
        flex-justify-center 
        flex-align-content-center
    `,
    classNameCap: `
        cap
        pos-a 
        top-0 
        width-p-50 
        height-p-100 
        overflow-hidden
    `,
    classNameCapCircle: `
        capCircle 
        border-10
        border-style-solid 
        border-t-transparent 
        border-r-transparent
        border-radius-p-100 
        pos-a 
        top-0
        width-p-100 
        height-p-100 
    `,
    classNameCircleTop: `
        circleTop 
        pos-a 
        top-0 
        left-0
        right-0 
        m-auto
        d-block
        border-radius-100
        width-10
        height-10
        bg-black
    `,
};

const ProgressCircularStyleColor: {
    [key: string]: typeof BaseStyle;
} = {};
const ProgressCircularStyleColors = [
    'blue',
    'indigo',
    'purple',
    'pink',
    'red',
    'orange',
    'yellow',
    'green',
    'teal',
    'cyan',
    'black',
    'gray',
    'dark',
    'bunker',
];
ProgressCircularStyleColors.forEach((color) => {
    ProgressCircularStyleColor[color] = {
        classNameProgressCircular: `
            ${BaseStyle.classNameProgressCircular}
            box-shadow-c-${color} 
        `,
        classNameProcentaje: `
            ${BaseStyle.classNameProcentaje}
        `,
        classNameCap: `
            ${BaseStyle.classNameCap}
        `,
        classNameCapCircle: `
            ${BaseStyle.classNameCapCircle}
            border-${color}  
            filter-brightness-5
        `,
        classNameCircleTop: `
            ${BaseStyle.classNameCircleTop}
            filter-brightness-5
        `,
    };
});
export const ProgressCircularStyle: {
    [key: string]: typeof BaseStyle;
} = {
    default: {
        classNameProgressCircular: `
            ${BaseStyle.classNameProgressCircular}
            box-shadow-c-gray
        `,
        classNameProcentaje: `
            ${BaseStyle.classNameProcentaje}
        `,
        classNameCap: `
            ${BaseStyle.classNameCap}
        `,
        classNameCapCircle: `
            ${BaseStyle.classNameCapCircle}
            border-bunker 
        `,
        classNameCircleTop: `
            ${BaseStyle.classNameCircleTop}
            box-shadow-c-bunker
        `,
    },
    ...ProgressCircularStyleColor,
    black: {
        classNameProgressCircular: `
            ${BaseStyle.classNameProgressCircular}
            box-shadow-c-gray
        `,
        classNameProcentaje: `
            ${BaseStyle.classNameProcentaje}
        `,
        classNameCap: `
            ${BaseStyle.classNameCap}
        `,
        classNameCapCircle: `
            ${BaseStyle.classNameCapCircle}
            border-black 
            filter-brightness-5
        `,
        classNameCircleTop: `
            ${BaseStyle.classNameCircleTop}
            box-shadow-c-black
            filter-brightness-5
        `,
    },
    dark: {
        classNameProgressCircular: `
            ${BaseStyle.classNameProgressCircular}
            box-shadow-c-gray
        `,
        classNameProcentaje: `
            ${BaseStyle.classNameProcentaje}
        `,
        classNameCap: `
            ${BaseStyle.classNameCap}
        `,
        classNameCapCircle: `
            ${BaseStyle.classNameCapCircle}
            border-dark 
            filter-brightness-5
        `,
        classNameCircleTop: `
            ${BaseStyle.classNameCircleTop}
            box-shadow-c-dark
            filter-brightness-5
        `,
    },
    bunker: {
        classNameProgressCircular: `
            ${BaseStyle.classNameProgressCircular}
            box-shadow-c-gray
        `,
        classNameProcentaje: `
            ${BaseStyle.classNameProcentaje}
        `,
        classNameCap: `
            ${BaseStyle.classNameCap}
        `,
        classNameCapCircle: `
            ${BaseStyle.classNameCapCircle}
            border-bunker 
            filter-brightness-5
        `,
        classNameCircleTop: `
            ${BaseStyle.classNameCircleTop}
            box-shadow-c-bunker
            filter-brightness-5
        `,
    },
};

export const ProgressCircularStyles = Object.keys(ProgressCircularStyle);

export interface ProgressCircularProps {
    styleTemplate?: string;
    p: number;
    posPor?: 'before' | 'after';
}

export const ProgressCircular = ({
    p = 50,
    children,
    posPor = 'before',
    styleTemplate = 'default',
    ...props
}: PropsWithChildren<ProgressCircularProps>) => {
    p = Math.min(Math.max(p, 0), 100);

    const config = {
        ...(ProgressCircularStyle[styleTemplate] ??
            ProgressCircularStyle.default),
        ...props,
    };

    return (
        <>
            <div
                className={config.classNameProgressCircular}
                style={
                    { '--p': p, paddingBottom: '100%' } as React.CSSProperties
                }
            >
                <span className={`${config.classNameProcentaje} ${posPor}`}>
                    {children}
                </span>
                <div className={`${config.classNameCap} cap1 right-0`}>
                    <div
                        className={`${config.classNameCapCircle} right-0`}
                        style={{
                            width: '200%',
                        }}
                    />
                </div>
                <div className={`${config.classNameCap} cap2 left-0`}>
                    <div
                        className={`${config.classNameCapCircle} left-0`}
                        style={
                            {
                                width: '200%',
                                '--min': 50,
                                '--max': 100,
                            } as React.CSSProperties
                        }
                    />
                </div>
                <span
                    className={config.classNameCircleTop}
                    style={{ background: 'var(--box-c,#000)' }}
                ></span>
                <div
                    className="pos-a top-0 left-0 width-p-100 height-p-100"
                    style={{
                        transform: `rotateZ( calc( 1deg * calc(3.6 * var(--p,0)) ) )`,
                    }}
                >
                    <span
                        className={config.classNameCircleTop}
                        style={{ background: 'var(--box-c,#000)' }}
                    ></span>
                </div>
            </div>
        </>
    );
};
export default ProgressCircular;
