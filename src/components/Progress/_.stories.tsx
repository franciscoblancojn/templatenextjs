import { Story, Meta } from "@storybook/react";

import { ProgressLinear, ProgressLinearProps } from "./Linear";
import { ProgressCircular, ProgressCircularProps } from "./Circular";

export default {
    title: "Progress/ProgressLinear",
    component: ProgressLinear,
} as Meta;

const Template: Story<ProgressLinearProps> = (args) => (
    <ProgressLinear {...args}>Test Children</ProgressLinear>
);

export const Linear = Template.bind({});
Linear.args = {
    p: 20,
};

const TemplateCircular: Story<ProgressCircularProps> = (args) => (
    <ProgressCircular {...args}>Test Children</ProgressCircular>
);

export const Circular = TemplateCircular.bind({});
Circular.args = {
    p: 30,
};
