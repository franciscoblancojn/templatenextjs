import { Story, Meta } from "@storybook/react";

import { FilterDateProps, FilterDate } from "./index";

export default {
    title: "Filter/FilterDate",
    component: FilterDate,
} as Meta;

const FilterDateIndex: Story<FilterDateProps> = (args) => (
    <FilterDate {...args}>Test Children</FilterDate>
);

export const Index = FilterDateIndex.bind({});
Index.args = {};
