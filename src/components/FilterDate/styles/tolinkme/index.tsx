import { FilterDateClassProps } from '@/components/FilterDate/Base';
import styled from 'styled-components';

export const tolinkme: FilterDateClassProps = {
    Content: styled.div`
        position: relative;
        .btnSelect {
            min-width: 13rem;
            border: 0;
            border-radius: 1rem;

            background: var(--whiteTwo);

            padding: 1rem 1.5rem;

            font-size: 1.2rem;
            font-weight: 600;
            font-family: circular;

            color: var(--warmGrey);

            display: inline-flex;
            align-items: center;
            cursor: pointer;
            .contentSvg {
                margin-right: 5px;
                display: inline-flex;
                align-items: center;
            }
        }
        .Selector {
            display: flex;
            gap: 2rem;
            position: absolute;
            top: 100%;
            left: 0;
            z-index: 1;

            margin-top: 1rem;
            min-width: 14rem;
            max-width: 70rem;
            border: 0;
            border-radius: 1.1rem;

            background: var(--whiteTwo);
            box-shadow: 0 0 3px #00000075;
            padding: 1rem 1.5rem;

            min-height: 6rem;

            font-size: 1rem;
            font-weight: 600;
            font-family: circular;

            color: var(--warmGrey);
            .close {
                display: none;
            }
            .contentItems {
                display: flex;
                flex-direction: column;
                padding-top: 20px;
                .item {
                    display: flex;
                    align-items: center;
                    margin-bottom: 1rem;
                    white-space: nowrap;
                    cursor: pointer;
                    font-size: 1.2rem;
                    .select {
                        width: 1.1rem;
                        height: 1.1rem;
                        border: solid 2px var(--warmGrey);
                        border-radius: 100%;
                        margin-right: 1rem;
                        &.active {
                            background: var(--darkAqua);
                        }
                    }
                }
            }
            .contentCalendares {
                width: 100%;
                display: flex;
                gap: 1rem;
                position: relative;
                .noClick {
                    position: absolute;
                    width: 100%;
                    height: 100%;
                    /* display: none; */
                }
                .react-calendar {
                    width: 100%;
                    min-width: 500px;
                    border: 0;
                    background-color: var(--whiteTwo);
                    * {
                        font-family: circular;
                    }

                    .react-calendar__navigation {
                        display: flex;
                        gap: 1rem;
                        margin-bottom: 1rem;
                    }
                    .react-calendar__navigation button {
                        color: var(--blackTwo);
                        border-radius: 1rem;
                        border: 1px solid;
                    }
                    .react-calendar__navigation button:disabled {
                        color: var(--blackTwo);
                        background-color: var(--white);
                        opacity: 0.5;
                        cursor: not-allowed;
                    }
                    .react-calendar__navigation__arrow {
                        aspect-ratio: 1/1;
                        padding: 0;
                        width: 1.25rem;
                    }
                    .react-calendar__month-view__weekdays {
                        justify-content: space-evenly;
                        display: grid !important;
                        grid-template-columns: repeat(7, 1fr);
                    }
                    .react-calendar__month-view__weekdays__weekday {
                        padding: 3px;
                        max-width: 2rem;
                        font-size: 0.7rem;
                    }
                    .react-calendar__month-view__days {
                        justify-content: space-evenly;
                        display: grid !important;
                        grid-template-columns: repeat(7, 1fr);
                        button {
                            position: relative;
                            border-radius: 100%;
                            padding: 0;
                            margin: 3px;
                            max-width: 1.5rem;
                            color: var(--blackTwo);
                            border: 0;
                            /* background-color: var(--white); */
                            &.react-calendar__tile:disabled {
                                color: var(--blackTwo);
                                background-color: var(--white);
                                opacity: 0.5;
                                cursor: not-allowed;
                            }
                            &.react-calendar__month-view__days__day--weekend {
                                color: var(--darkAqua);
                            }
                            &.react-calendar__tile--active {
                                background-color: var(--graySea);
                            }
                            &.react-calendar__tile--hasActive,
                            &.react-calendar__tile--rangeStart,
                            &.react-calendar__tile--rangeEnd {
                                background-color: var(--darkAqua);
                                color: var(--whiteTwo);
                            }
                            &::before {
                                content: '';
                                display: block;
                                padding-bottom: 100%;
                            }

                            abbr {
                                position: absolute;
                                inset: 0;
                                width: 100%;
                                height: 100%;
                                display: flex;
                                align-items: center;
                                justify-content: center;
                                text-align: center;
                                font-size: 0.7rem;
                            }
                        }
                    }
                }
            }
        }
        @media (max-width: 767px) {
            .Selector {
                position: fixed;
                inset: 0;
                width: 100%;
                height: 100%;
                z-index: 999;
                margin: 0;
                max-width: 100%;
                border-radius: 0;

                flex-direction: column;
                overflow: auto;
                padding: 1rem;
                gap: 0.5rem;
                .close {
                    display: block;
                    text-align: right;
                }
                .contentItems {
                    padding: 0;
                    display: grid;
                    grid-template-columns: repeat(2, 1fr);
                    column-gap: 1rem;
                    .item {
                        font-size: 0.9rem;
                    }
                }
                .react-calendar.react-calendar {
                    min-width: 0;
                }
                .react-calendar__viewContainer {
                    flex-direction: column;
                    gap: 1rem;
                }
                .react-calendar__month-view {
                    width: 100%;
                }
                .react-calendar__navigation.react-calendar__navigation.react-calendar__navigation {
                    display: grid;
                    grid-template-columns: repeat(4, 1fr);
                    grid-template-rows: repeat(2, 1fr);
                    justify-content: center;
                    gap: 0.5rem;
                }
                .react-calendar__navigation__label {
                    grid-area: 1 / 1 / 2 / 5;
                }
                .react-calendar__navigation__arrow {
                    margin: auto;
                }
            }
        }
    `,
    classNameContent: `
        
    `,
    styleTemplateSelect: 'tolinkme3',
    classNameContentStartEnd: `
        flex
        flex-nowrap
        flex-gap-column-10
    `,
    styleTemplateStartEnd: 'tolinkme3',
};
