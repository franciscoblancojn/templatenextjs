import { DateTypeValue } from '@/data/components/Filters';

import { InputTextStyles } from '@/components/Input/Text';
import { Date as DateOptions } from '@/data/components/Filters';
import { ThemesType } from '@/config/theme';
import styled, { StyledComponent } from 'styled-components';
import { Date as SVGDate } from '@/svg/date';
import Close from '@/svg/close';
import { useLang } from '@/lang/translate';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { Calendar } from 'react-calendar';
import { useRouter } from 'next/router';

export interface FilterDateClassProps {
    Content?: StyledComponent<'div', { p: '1' }>;
    classNameContent?: string;
    styleTemplateSelect?: InputTextStyles | ThemesType;
    classNameContentStartEnd?: string;
    styleTemplateStartEnd?: InputTextStyles | ThemesType;
}

export interface FilterDateBaseProps {
    defaultValue?: DateTypeValue | [Date, Date];
}

export interface FilterDateProps
    extends FilterDateClassProps,
        FilterDateBaseProps {}

export const FilterDateBase = ({ Content = styled.div`` }: FilterDateProps) => {
    const router = useRouter();

    const _t = useLang();
    const toDay = new Date();

    const [range /*setRange*/] = useState<[Date, Date]>([toDay, toDay]);
    const [showSelector, setShowSelector] = useState(false);
    const [date, setDate] = useState<{ id: string; text: string }>({
        id: 'today',
        text: 'Hoy',
    });

    const toggleSelector = () => {
        setShowSelector((pre) => !pre);
    };

    const itemsSelect = useMemo(
        () =>
            DateOptions.map((o, i) => (
                <div
                    className="item"
                    onClick={() => {
                        setDate(o);
                    }}
                    key={i}
                >
                    <div
                        className={`select ${date.id == o.id ? 'active' : ''}`}
                    ></div>
                    {_t(o.text)}
                </div>
            )),
        [date]
    );

    const getRange = useCallback((): [Date, Date] => {
        const start = new Date(toDay);
        const end = new Date(toDay);
        start.setHours(0);
        start.setMinutes(0);
        start.setSeconds(0);
        start.setMilliseconds(0);
        end.setHours(23);
        end.setMinutes(59);
        end.setSeconds(59);
        end.setMilliseconds(0);

        if (date.id == 'today') {
            return [start, end];
        }
        if (date.id == '7-day') {
            start.setDate(start.getDate() - 7);
            return [start, end];
        }

        if (date.id == '30-day') {
            start.setDate(start.getDate() - 30);
            return [start, end];
        }

        if (date.id == 'month') {
            start.setDate(1);
            return [start, end];
        }
        if (date.id == 'lastMonth') {
            start.setDate(1);
            start.setMonth(-1);

            end.setDate(0);
            return [start, end];
        }
        if (date.id == 'all') {
            return [new Date(0), end];
        }

        return range;
    }, [range, date]);

    const onSetStartEnd = () => {
        const rangeSelect = getRange();
        router.push(
            {
                pathname: router.pathname,
                query: {
                    start: rangeSelect[0].getTime().toString(),
                    end: rangeSelect[1].getTime().toString(),
                },
            },
            undefined,
            { scroll: false }
        );
    };

    useEffect(() => {
        onSetStartEnd();
    }, [range, date]);

    return (
        <Content>
            <div className="btnSelect" onClick={toggleSelector}>
                <span className="contentSvg">
                    <SVGDate size={20} />
                </span>
                {_t(date.text)}
            </div>

            {showSelector ? (
                <div className="Selector" onMouseLeave={toggleSelector}>
                    <div className="close">
                        <span onClick={toggleSelector}>
                            <Close size={25} />
                        </span>
                    </div>
                    <div className="contentItems">{itemsSelect}</div>
                    <div className="contentCalendares">
                        <Calendar
                            value={getRange()}
                            defaultValue={getRange()}
                            returnValue="range"
                            maxDate={new Date()}
                            showDoubleView={true}
                            selectRange={date.id == 'data_piker'}
                            onChange={(e: any) => {
                                if (e) {
                                    if (Array.isArray(e)) {
                                        const start = new Date(e?.[0] ?? 0)
                                            .getTime()
                                            .toString();
                                        const end = new Date(e?.[1] ?? 0)
                                            .getTime()
                                            .toString();
                                        router.push(
                                            {
                                                pathname: router.pathname,
                                                query: {
                                                    start: start,
                                                    end: end,
                                                },
                                            },
                                            undefined,
                                            { scroll: false }
                                        );
                                    }
                                }
                            }}
                        />

                        {!(date.id == 'data_piker') ? (
                            <div className="noClick" />
                        ) : (
                            <></>
                        )}
                    </div>
                </div>
            ) : (
                <></>
            )}
        </Content>
    );
};
export default FilterDateBase;
