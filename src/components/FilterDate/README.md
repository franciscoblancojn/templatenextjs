# DragDrop

## Dependencies

[Template](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/data/components/Filters)

```js
import { DateTypeValue } from '@/data/components/Filters';
```

## Import

```js
import { FilterDateStyles, FilterDate } from '@/components/FilterDate';
```

## Props

```ts
export interface FilterDateProps {
    styleTemplate?: FilterDateStyles;
    defaultValue?: DateTypeValue | [Date, Date];
    onChange?: (data: DateTypeValue | [Date, Date]) => void;
}
```

## Use

```js
<DragDrop
    defaultValue={'all'}
    onChange={(data: DateTypeValue | [Date, Date]) => {}}
/>
```
