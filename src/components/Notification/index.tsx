import { useMemo, PropsWithChildren } from 'react';

import * as styles from '@/components/Notification/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    NotificationBaseProps,
    NotificationBase,
} from '@/components/Notification/Base';

export const NotificationStyle = { ...styles } as const;

export type NotificationStyles = keyof typeof NotificationStyle;

export interface NotificationProps extends NotificationBaseProps {
    styleTemplate?: NotificationStyles | ThemesType;
}

export const Notification = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PropsWithChildren<NotificationProps>) => {
    const Style = useMemo(
        () =>
            NotificationStyle[styleTemplate as NotificationStyles] ??
            NotificationStyle._default,
        [styleTemplate]
    );

    return <NotificationBase {...Style} {...props} />;
};
export default Notification;
