import { Story, Meta } from "@storybook/react";

import { NotificationProps, Notification } from "./index";

export default {
    title: "Notification/Notification",
    component: Notification,
} as Meta;

const NotificationIndex: Story<NotificationProps> = (args) => (
    <Notification {...args} />
);

export const Index = NotificationIndex.bind({});
Index.args = {
    children: "Content",
    type: "normal",
};

export const Error_ = NotificationIndex.bind({});
Error_.args = {
    children: "Content",
    type: "error",
};

export const Ok = NotificationIndex.bind({});
Ok.args = {
    children: "Content",
    type: "ok",
};

export const Warning = NotificationIndex.bind({});
Warning.args = {
    children: "Content",
    type: "warning",
};
