import { useMemo } from 'react';

import * as styles from '@/components/ConfigInfoStyle/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigInfoStyleBaseProps,
    ConfigInfoStyleBase,
} from '@/components/ConfigInfoStyle/Base';

export const ConfigInfoStyleStyle = { ...styles } as const;

export type ConfigInfoStyleStyles = keyof typeof ConfigInfoStyleStyle;

export interface ConfigInfoStyleProps extends ConfigInfoStyleBaseProps {
    styleTemplate?: ConfigInfoStyleStyles | ThemesType;
}

export const ConfigInfoStyle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigInfoStyleProps) => {
    const Style = useMemo(
        () =>
            ConfigInfoStyleStyle[styleTemplate as ConfigInfoStyleStyles] ??
            ConfigInfoStyleStyle._default,
        [styleTemplate]
    );

    return <ConfigInfoStyleBase {...Style} {...props} />;
};
export default ConfigInfoStyle;
