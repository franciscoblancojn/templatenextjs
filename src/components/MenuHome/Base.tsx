import Text from '../Text';
import { PropsWithChildren } from 'react';
import { useLang } from '@/lang/translate';
import Link from '../Link';
import url from '@/data/routes';

export interface MenuHomeClassProps {
    classNameContentButton?: any;
    classNameMoney?: string;
    classNameImg?: string;
    classNameMenuHome?: string;
}

export interface MenuHomeBaseProps extends PropsWithChildren {}

export interface MenuHomeProps extends MenuHomeClassProps, MenuHomeBaseProps {}

export const MenuHomeBase = ({}: MenuHomeProps) => {
    const _t = useLang();
    return (
        <>
            <Link styleTemplate="tolinkme8" href={url.index}>
                <Text className="color-white color-brightPink-hover font-14 font-w-400">
                    {_t('Home')}
                </Text>
            </Link>
            <Link styleTemplate="tolinkme8" href={url.index}>
                <Text className="color-white color-brightPink-hover font-14 font-w-400">
                    {_t('Gallery')}
                </Text>
            </Link>
            <Link styleTemplate="tolinkme8" href={url.index}>
                <Text className="color-white color-brightPink-hover font-14 font-w-400">
                    {_t('Blog')}
                </Text>
            </Link>
        </>
    );
};
export default MenuHomeBase;
