import { useMemo } from 'react';

import * as styles from '@/components/MenuHome/styles';

import { Theme, ThemesType } from '@/config/theme';

import { MenuHomeBaseProps, MenuHomeBase } from '@/components/MenuHome/Base';

export const MenuHomeStyle = { ...styles } as const;

export type MenuHomeStyles = keyof typeof MenuHomeStyle;

export interface MenuHomeProps extends MenuHomeBaseProps {
    styleTemplate?: MenuHomeStyles | ThemesType;
}

export const MenuHome = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: MenuHomeProps) => {
    const Style = useMemo(
        () =>
            MenuHomeStyle[styleTemplate as MenuHomeStyles] ??
            MenuHomeStyle._default,
        [styleTemplate]
    );

    return <MenuHomeBase {...Style} {...props} />;
};
export default MenuHome;
