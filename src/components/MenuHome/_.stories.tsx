import { Story, Meta } from "@storybook/react";

import { MenuHomeProps, MenuHome } from "./index";

export default {
    title: "MenuHome/MenuHome",
    component: MenuHome,
} as Meta;

const MenuHomeIndex: Story<MenuHomeProps> = (args) => (
    <MenuHome {...args}>Test Children</MenuHome>
);

export const Index = MenuHomeIndex.bind({});
Index.args = {
};
