import { Story, Meta } from "@storybook/react";

import { BoxLinkProps, BoxLink } from "./index";
import { BoxLinkPersonal } from "./Template/Personal";
import { BoxLinkPayment } from "./Template/Payment";
import { BoxLinkManage } from "./Template/Manage";

export default {
    title: "BoxLink/BoxLink",
    component: BoxLink,
} as Meta;

const BoxLinkIndex: Story<BoxLinkProps> = (args) => (
    <BoxLink {...args}>Test Children</BoxLink>
);

export const Index = BoxLinkIndex.bind({});
Index.args = {
    icon:"icon",
    text:"link"
};

const BoxLinkPersonalIndex: Story<BoxLinkProps> = (args) => (
    <BoxLinkPersonal {...args}>Test Children</BoxLinkPersonal>
);

export const Personal = BoxLinkPersonalIndex.bind({});
Personal.args = {
};


const BoxLinkPaymentIndex: Story<BoxLinkProps> = (args) => (
    <BoxLinkPayment {...args}>Test Children</BoxLinkPayment>
);

export const Payment = BoxLinkPaymentIndex.bind({});
Payment.args = {
};


const BoxLinkManageIndex: Story<BoxLinkProps> = (args) => (
    <BoxLinkManage {...args}>Test Children</BoxLinkManage>
);

export const Manage = BoxLinkManageIndex.bind({});
Manage.args = {
};
