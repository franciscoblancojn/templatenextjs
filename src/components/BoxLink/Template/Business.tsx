import { BoxLink, BoxLinkProps } from '@/components/BoxLink';
import url from '@/data/routes';
import { Business } from '@/svg/business';

export const BoxLinkBusiness = ({ ...props }: BoxLinkProps) => {
    return (
        <BoxLink
            {...props}
            text="Business Information"
            icon={<Business size={22} />}
            link={url.myAccount.business.index}
        />
    );
};
export default BoxLink;
