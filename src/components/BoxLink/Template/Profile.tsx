import { BoxLink, BoxLinkProps } from '@/components/BoxLink';
import url from '@/data/routes';
import { UserAccount3 } from '@/svg/userAccount';

export const BoxLinkProfile = ({ ...props }: BoxLinkProps) => {
    return (
        <BoxLink
            {...props}
            text="Profile"
            icon={<UserAccount3 size={22} />}
            link={url.profile}
        />
    );
};
export default BoxLink;
