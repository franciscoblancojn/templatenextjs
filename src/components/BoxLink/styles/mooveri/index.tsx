import { BoxLinkClassProps } from '@/components/BoxLink/Base';

export const mooveri: BoxLinkClassProps = {
    classNameContentLink: `
        hover-scale-11
        d-inline-block
        
        width-p-100

        pos-r
    `,
    styleTemplateIconText: 'mooveri',
    styleTemplateBox: 'mooveri',
    styleTemplateLink: 'mooveri4',
    classNameContentTop: `
        pos-a
        top-15
        right-15
        text-left
    `,
};
