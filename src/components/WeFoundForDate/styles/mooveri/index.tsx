import { WeFoundForDateClassProps } from '@/components/WeFoundForDate/Base';

export const mooveri: WeFoundForDateClassProps = {
    classNameContent: `
        flex
        flex-align-center 
        flex-justify-right
    `,
    styleTemplateText: 'mooveri',
    styleTemplateResults: 'mooveri10',
    classNameTextDate: `
        flex flex-align-center
    `,
    classNameImgDate: `
        m-h-3
    `,
    classNameResultText: `
        m-h-3
    `,
    sizeImg: 10,
    styleTemplateTextDate: 'mooveri11',
};
