import { Story, Meta } from "@storybook/react";

import { WeFoundForDateProps, WeFoundForDate } from "./index";

export default {
    title: "WeFoundForDate/WeFoundForDate",
    component: WeFoundForDate,
} as Meta;

const WeFoundForDateIndex: Story<WeFoundForDateProps> = (args) => (
    <WeFoundForDate {...args}>Test Children</WeFoundForDate>
);

export const Index = WeFoundForDateIndex.bind({});
Index.args = {
    results:30,
    date:new Date()
};
