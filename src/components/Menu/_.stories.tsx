import { Story, Meta } from "@storybook/react";

import {DATA} from "@/data/components/Header";

import { MenuProps, Menu } from "./index";
export default {
    title: "Menu/Menu",
    component: Menu,
} as Meta;

const Template: Story<MenuProps> = (args) => (
    <Menu {...args}>Test Children</Menu>
);

export const Index = Template.bind({});
Index.args = {
    items: DATA.nav,
};