import { MenuClassProps, BaseStyle } from '@/components/Menu/Base';

const responsive = 'lg';

export const _default: MenuClassProps = {
    classNameContent: `
        flex flex-justify-center
        ${BaseStyle.classNameContent}
    `,
    classNameButton: `
        d-${responsive}-none
        ${BaseStyle.classNameButton}
    `,
    classNameUl: `
        flex flex-justify-center
        pos-${responsive}-r pos-a
        top-p-100
        height-${responsive}-p-max-100  height-vh-max-0
        overflow-hidden
        ${BaseStyle.classNameUl}
    `,
    classNameContentUlActive: `
        height-vh-max-100
        width-vw-100
        flex-column flex-nowrap flex-align-center
        bg-white
        overflow-auto
        ${BaseStyle.classNameUlActive}
    `,
    classNameLi: `
        flex
        font-16 font-montserrat
        ${BaseStyle.classNameLi}
    `,
    classNameLiActive: `
        p-v-25
        ${BaseStyle.classNameLiActive}
    `,
    classNameA: `
        p-v-5 p-h-10
        color-black color-blue-hover
        ${BaseStyle.classNameA}
    `,
};
