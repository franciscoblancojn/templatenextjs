import { MenuClassProps, BaseStyle } from '@/components/Menu/Base';

export const tolinkme: MenuClassProps = {
    classNameContent: `
        width-p-100
        flex flex-justify-right
        ${BaseStyle.classNameContent}
    `,
    classNameButton: `
        color-white
        bg-transparent
        border-0
        p-0
    `,
    classNameContentUl: `
        pos-f
        left-0
        width-p-100
        overflow-auto
        z-index-9
        p-h-35 p-v-50
        m-0
        bg-black
        transition-5
        transform
    `,
    styleContentUl: {
        maxWidth: 'min(100%,575px)',
        height: 'calc(100vh - var(--sizeHeader,0px))',
        top: 'var(--sizeHeader,0px)',
    },
    classNameContentUlActive: `
        transform-translate-X-0
    `,
    classNameContentUlInactive: `
        transform-translate-X-p--100
    `,
    classNameUl: `
        flex flex-justify-left
        flex-column flex-nowrap flex-align-start
        
        p-0 
        m-0
    `,
    classNameLi: `
        flex
        font-30 font-nunito font-w-900
        color-white
        color-brightPink-hover
        m-b-30
    `,
    classNameLiActive: `
    `,
    classNameLiInactive: ``,
    classNameLiStrong: `
    
    `,
    classNameA: `
        color-currentColor
    `,
};

export const tolinkme2: MenuClassProps = {
    classNameContent: `
        width-p-100
        flex flex-justify-right
        ${BaseStyle.classNameContent}
    `,
    classNameButton: `
        color-white
        bg-transparent
        border-0
    `,
    classNameContentUl: `
        pos-f
        right-0
        bottom-0
        width-p-100
        overflow-auto
        z-index-9
        box-shadow box-shadow-black box-shadow-b-1
        border-radius-0
        border-t-l-radius-10
        border-t-r-radius-10
        p-h-30 p-v-27
        m-0
        bg-white
        transition-5
        transform
    `,
    styleContentUl: {
        maxWidth: 'min(100%,20rem)',
        maxHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    classNameContentUlActive: `
        transform-translate-Y-0
    `,
    classNameContentUlInactive: `
        transform-translate-Y-p-100
    `,
    classNameUl: `
        flex flex-justify-left
        flex-column flex-nowrap flex-align-start
        
        p-0 
        m-0
    `,
    classNameLi: `
        flex
        font-14 font-nunito font-w-700
        color-blackTwo
        color-brightPink-hover
        p-v-15
    `,
    classNameLiActive: `
    `,
    classNameLiInactive: ``,
    classNameLiStrong: `
        font-24 font-w-900
    `,
    classNameA: `
        color-currentColor
    `,
};

export const tolinkme3: MenuClassProps = {
    classNameContent: `
        width-p-100
        flex flex-justify-right
        ${BaseStyle.classNameContent}
    `,
    classNameButton: `
        color-white
        bg-transparent
        border-0
        p-0
    `,
    classNameContentUl: `
        pos-f
        left-0
        width-p-100
        overflow-auto
        z-index-9
        p-h-35 p-v-50
        m-0
        bg-black
        transition-5
        transform
    `,
    styleContentUl: {
        maxWidth: 'min(100%,575px)',
        height: 'calc(100vh - var(--sizeHeader,0px))',
        top: 'var(--sizeHeader,0px)',
    },
    classNameContentUlActive: `
        transform-translate-X-0
    `,
    classNameContentUlInactive: `
        transform-translate-X-p--100
    `,
    classNameUl: `
        flex flex-justify-left
        flex-column flex-nowrap flex-align-start
        
        p-0 
        m-0
    `,
    classNameLi: `
        flex
        font-30 font-nunito font-w-900
        color-white
        color-brightPink-hover
        m-b-30
    `,
    classNameLiActive: `
    `,
    classNameLiInactive: ``,
    classNameLiStrong: `
    
    `,
    classNameA: `
        color-currentColor
    `,
};

export const tolinkme4: MenuClassProps = {
    classNameContent: `
    width-p-100
    flex flex-justify-right
    ${BaseStyle.classNameContent}
`,
    classNameButton: `
    color-white
    bg-transparent
    border-0
`,
    classNameContentUl: `
    pos-f
    left-0
    top-80
    width-p-100
    overflow-auto
    z-index-9
    box-shadow box-shadow-black box-shadow-b-2
    border-radius-0
    border-t-r-radius-10
    border-b-r-radius-10
    p-h-30 p-v-27
    m-0
    bg-black
    transition-5
    transform
`,
    styleContentUl: {
        maxWidth: 'min(100%,375px)',
        height: 'calc(100vh - var(--sizeHeader,0px))',
        top: '120px',
    },
    classNameContentUlActive: `
        transform-translate-X-0
    `,
    classNameContentUlInactive: `
        transform-translate-X-p--100
    `,
    classNameUl: `
    flex flex-justify-left
    flex-column flex-nowrap flex-align-start
    
    p-0 
    m-0
`,
    classNameLi: `
    flex
    font-18 font-nunito font-w-900
    color-white
    color-brightPink-hover
    p-v-15
`,
    classNameLiActive: `
`,
    classNameLiInactive: ``,
    classNameLiStrong: `
    font-24 font-w-900
`,
    classNameA: `
    color-currentColor
`,
};
