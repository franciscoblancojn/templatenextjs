import { MenuBase, MenuBaseProps } from '@/components/Menu/Base';

import * as styles from '@/components/Menu/styles';

import { Theme, ThemesType } from '@/config/theme';

export const MenuStyle = { ...styles } as const;

export type MenuStyles = keyof typeof MenuStyle;

export interface MenuProps extends MenuBaseProps {
    styleTemplate?: MenuStyles | ThemesType;
}

export const Menu = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: MenuProps) => {
    const config = {
        ...(MenuStyle[styleTemplate as MenuStyles] ?? MenuStyle._default),
        ...props,
    };

    return (
        <>
            <MenuBase {...config} />
        </>
    );
};
export default Menu;
