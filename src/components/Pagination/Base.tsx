import { InputNumber } from '@/components/Input/Number';
import Theme, { ThemesType } from '@/config/theme';

import {
    PaginationUp,
    PaginationPre,
    PaginationNext,
    PaginationDown,
} from '@/svg/pagination';
import { useEffect, useMemo, useState } from 'react';
import { InputTextStyles } from '../Input/Text';

export interface PaginationClassProps {
    classNameContent?: string;
    classNameUp?: string;
    classNamePre?: string;
    classNameCurrent?: string;
    classNameNext?: string;
    classNameDown?: string;
    icons?: {
        up?: any;
        pre?: any;
        next?: any;
        down?: any;
    };
    styleTemplateInput?: InputTextStyles | ThemesType;
}

export interface PaginationBaseProps {
    defaultPage?: number;
    nItems: number;
    nItemsPage?: number;
    disabled?: boolean;
    onChangePage?: (page: number) => void;
}

export interface PaginationProps
    extends PaginationClassProps,
        PaginationBaseProps {}

export const PaginationBase = ({
    classNameContent = '',
    classNameUp = '',
    classNamePre = '',
    classNameCurrent = '',
    classNameNext = '',
    classNameDown = '',

    icons = {
        up: <PaginationUp />,
        pre: <PaginationPre />,
        next: <PaginationNext />,
        down: <PaginationDown />,
    },

    styleTemplateInput = Theme.styleTemplate ?? '_default',

    defaultPage = 0,

    nItems,
    nItemsPage = 10,

    disabled = false,
    ...props
}: PaginationProps) => {
    const [page, setPage] = useState(defaultPage);

    const maxPage = useMemo(
        () => Math.ceil(nItems / nItemsPage) - 1,
        [nItems, nItemsPage]
    );

    const addPage = (add: number) => () => {
        if (disabled) {
            return;
        }
        let Value = page + add;
        Value = Math.max(0, Value);
        Value = Math.min(maxPage, Value);
        setPage(Value);
    };

    useEffect(() => {
        props?.onChangePage?.(page);
    }, [page]);

    return (
        <div className={classNameContent}>
            <div
                className={classNameUp}
                onClick={() => {
                    if (!disabled) {
                        setPage(0);
                    }
                }}
            >
                {icons.up}
            </div>
            <div className={classNamePre} onClick={addPage(-1)}>
                {icons.pre}
            </div>
            <div className={classNameCurrent}>
                <InputNumber
                    onChange={(e) => {
                        setPage(e == '' ? 0 : e);
                    }}
                    defaultValue={page}
                    val={page}
                    min={0}
                    disabled={disabled}
                    styleTemplate={styleTemplateInput}
                />
                -{maxPage}
            </div>
            <div className={classNameNext} onClick={addPage(1)}>
                {icons.next}
            </div>
            <div
                className={classNameDown}
                onClick={() => {
                    if (!disabled) {
                        setPage(maxPage);
                    }
                }}
            >
                {icons.down}
            </div>
        </div>
    );
};
export default PaginationBase;
