import { useMemo } from 'react';

import * as styles from '@/components/Pagination/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    PaginationBaseProps,
    PaginationBase,
} from '@/components/Pagination/Base';

export const PaginationStyle = { ...styles } as const;

export type PaginationStyles = keyof typeof PaginationStyle;

export interface PaginationProps extends PaginationBaseProps {
    styleTemplate?: PaginationStyles | ThemesType;
}

export const Pagination = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PaginationProps) => {
    const Style = useMemo(
        () =>
            PaginationStyle[styleTemplate as PaginationStyles] ??
            PaginationStyle._default,
        [styleTemplate]
    );

    return <PaginationBase {...Style} {...props} />;
};
export default Pagination;
