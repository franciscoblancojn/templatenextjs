import { PaginationClassProps } from '@/components/Pagination/Base';

export const _default: PaginationClassProps = {
    classNameContent: `
        flex
        flex-align-center
        flex-gap-column-10
        flex-nowrap
    `,
    classNameUp: `
        cursor-pointer
    `,
    classNamePre: `
        cursor-pointer
        
    `,
    classNameCurrent: `
        flex
        flex-align-center
        flex-gap-column-10
        flex-nowrap
    `,
    classNameNext: `
        cursor-pointer
        
    `,
    classNameDown: `
        cursor-pointer
        
    `,
};
