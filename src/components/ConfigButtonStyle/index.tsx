import { useMemo } from 'react';

import * as styles from '@/components/ConfigButtonStyle/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigButtonStyleBaseProps,
    ConfigButtonStyleBase,
} from '@/components/ConfigButtonStyle/Base';

export const ConfigButtonStyleStyle = { ...styles } as const;

export type ConfigButtonStyleStyles = keyof typeof ConfigButtonStyleStyle;

export interface ConfigButtonStyleProps extends ConfigButtonStyleBaseProps {
    styleTemplate?: ConfigButtonStyleStyles | ThemesType;
}

export const ConfigButtonStyle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigButtonStyleProps) => {
    const Style = useMemo(
        () =>
            ConfigButtonStyleStyle[styleTemplate as ConfigButtonStyleStyles] ??
            ConfigButtonStyleStyle._default,
        [styleTemplate]
    );

    return <ConfigButtonStyleBase {...Style} {...props} />;
};
export default ConfigButtonStyle;
