import { ConfigButtonStyleClassProps } from '@/components/ConfigButtonStyle/Base';

export const tolinkme: ConfigButtonStyleClassProps = {
    styleTemplatePopup: 'tolinkme2',
    styleTemplateTitlePopup: 'tolinkme21',

    styleTemplateCollapse: 'tolinkme',

    styleTemplateRadioSizeButton: 'tolinkme',
    styleTemplateRadioBorderButton: 'tolinkme',
    styleTemplateConfigBoxShadow: 'tolinkme',
    styleTemplateInputSelectBackground: 'tolinkme',
    styleTemplateConfigText: 'tolinkme',
    classNameContentExample: `
        flex
        flex-column
        row-gap-12
        flex-justify-center
        flex-align-center
        color-
        flex-nowrap
        
        font-nunito
        font-20
        font-w-900
        color-warmGrey
        
        pos-sk
        top-20
        bg-white-
        p-b-15
        z-index-5

        border-0
        border-b-1
        border-style-solid
        border-whiteTwo
        
        box-shadow
        box-shadow-c-white
        box-shadow-y--15-

        bg-white
    `,
    classNameReload: `
        cursor-pointer
    `,
};
