import { Story, Meta } from "@storybook/react";

import { ConfigButtonStyleProps, ConfigButtonStyle } from "./index";

export default {
    title: "ConfigButtonStyle/ConfigButtonStyle",
    component: ConfigButtonStyle,
} as Meta;

const ConfigButtonStyleIndex: Story<ConfigButtonStyleProps> = (args) => (
    <ConfigButtonStyle {...args}>Test Children</ConfigButtonStyle>
);

export const Index = ConfigButtonStyleIndex.bind({});
Index.args = {};
