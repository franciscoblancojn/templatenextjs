import { ProfileAndSubscriptionsClassProps } from '@/components/ProfileAndSubscriptions/Base';

export const tolinkme: ProfileAndSubscriptionsClassProps = {
    classNameImg: `
        p-t-10
        p-b-10
        width-p-100
    `,
    classNameAvatar: `
        height-100
        width-100
        border-radius-50
        object-fit-cover
    `,
    classNameContent: `
        bg-white
        p-t-20
        flex
        text-center
        flex-justify-center

`,
    classNameContentText: `
        p-b-10
        p-t-10
`,
    classNameContentAvatar: `
        flex-justify-center flex
        m-t-10
`,
    classNameContentWidthBtn: `
        m-auto
    `,
    classNameContentWidthAvatar: `
        m-auto
        text-center
        p-t-10
   
    `,
    styleTemplateText: 'tolinkme44',
    styleTemplateText2: 'tolinkme43',
};
