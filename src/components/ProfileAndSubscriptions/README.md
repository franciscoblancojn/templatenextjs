# ProfileAndSubscriptions

## Dependencies

[ProfileAndSubscriptions](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ProfileAndSubscriptions)

```js
import { ProfileAndSubscriptions } from '@/components/ProfileAndSubscriptions';
```

## Import

```js
import {
    ProfileAndSubscriptions,
    ProfileAndSubscriptionsStyles,
} from '@/components/ProfileAndSubscriptions';
```

## Props

```tsx
interface ProfileAndSubscriptionsProps {}
```

## Use

```js
<ProfileAndSubscriptions>ProfileAndSubscriptions</ProfileAndSubscriptions>
```
