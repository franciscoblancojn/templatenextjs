import { Story, Meta } from "@storybook/react";

import { ProfileAndSubscriptionsProps, ProfileAndSubscriptions } from "./index";

export default {
    title: "ProfileAndSubscriptions/ProfileAndSubscriptions",
    component: ProfileAndSubscriptions,
} as Meta;

const ProfileAndSubscriptionsIndex: Story<ProfileAndSubscriptionsProps> = (args) => (
    <ProfileAndSubscriptions {...args}>Test Children</ProfileAndSubscriptions>
);

export const Index = ProfileAndSubscriptionsIndex.bind({});
Index.args = {};
