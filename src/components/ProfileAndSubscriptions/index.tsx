import { useMemo } from 'react';

import * as styles from '@/components/ProfileAndSubscriptions/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ProfileAndSubscriptionsBaseProps,
    ProfileAndSubscriptionsBase,
} from '@/components/ProfileAndSubscriptions/Base';

export const ProfileAndSubscriptionsStyle = { ...styles } as const;

export type ProfileAndSubscriptionsStyles =
    keyof typeof ProfileAndSubscriptionsStyle;

export interface ProfileAndSubscriptionsProps
    extends ProfileAndSubscriptionsBaseProps {
    styleTemplate?: ProfileAndSubscriptionsStyles | ThemesType;
}

export const ProfileAndSubscriptions = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ProfileAndSubscriptionsProps) => {
    const Style = useMemo(
        () =>
            ProfileAndSubscriptionsStyle[
                styleTemplate as ProfileAndSubscriptionsStyles
            ] ?? ProfileAndSubscriptionsStyle._default,
        [styleTemplate]
    );

    return <ProfileAndSubscriptionsBase {...Style} {...props} />;
};
export default ProfileAndSubscriptions;
