import { Story, Meta } from "@storybook/react";

import { LoaderPageProps, LoaderPage } from "./index";

export default {
    title: "Loader/LoaderPage",
    component: LoaderPage,
} as Meta;

const Template: Story<LoaderPageProps> = (args) => (
    <LoaderPage {...args}>Test Children</LoaderPage>
);

export const Index = Template.bind({});
Index.args = {};
