import ContentLoader from 'react-content-loader';

export interface LoaderTableProps {}

export const LoaderTable = (props: LoaderTableProps) => {
    return (
        <ContentLoader
            viewBox="0 0 1000 550"
            backgroundColor="#eee"
            foregroundColor="#bbb"
            {...props}
        >
            <rect x="0" y={0} rx="3" ry="3" width="1000" height="2" />
            {new Array(5).fill(1).map((e, i) => {
                const y = i * 100 + 40;
                return (
                    <>
                        <rect
                            x="20"
                            y={y}
                            rx="3"
                            ry="3"
                            width="200"
                            height="16"
                        />
                        <rect
                            x="240"
                            y={y}
                            rx="3"
                            ry="3"
                            width="300"
                            height="16"
                        />
                        <rect
                            x="560"
                            y={y}
                            rx="3"
                            ry="3"
                            width="300"
                            height="16"
                        />
                        <circle cx="900" cy={y + 9} r="9" />
                        <circle cx="940" cy={y + 9} r="9" />

                        <rect
                            x="0"
                            y={y + 50}
                            rx="3"
                            ry="3"
                            width="1000"
                            height="2"
                        />
                    </>
                );
            })}
        </ContentLoader>
    );
};

export default LoaderTable;
