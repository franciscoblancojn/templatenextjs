import ContentLoader from 'react-content-loader';

export interface LoaderLineProps {
    width?: number;
    height?: number;
}

export const LoaderLine = ({ width = 200, height = 10 }: LoaderLineProps) => {
    return (
        <ContentLoader
            viewBox={`0 0 ${width} ${height}`}
            backgroundColor="#eee"
            foregroundColor="#bbb"
        >
            <rect x="0" y="0" rx="3" ry="3" width={width} height={height} />
        </ContentLoader>
    );
};

export interface LoaderLineMultipleProps extends LoaderLineProps {
    lines?: number;
}

export const LoaderLineMultiple = ({
    lines = 3,
    ...props
}: LoaderLineMultipleProps) => {
    return (
        <div className="width-p-100 flex flex-column flex-nowrap row-gap-16">
            {new Array(lines).fill(1).map((e, i) => {
                return <LoaderLine {...props} key={e + i} />;
            })}
        </div>
    );
};

export default LoaderLine;
