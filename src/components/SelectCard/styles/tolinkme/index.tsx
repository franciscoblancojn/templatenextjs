import { SelectCardClassProps } from '@/components/SelectCard/Base';

export const tolinkme: SelectCardClassProps = {
    classNameContentCard: `
    m-t-20
    bg-white
    color-black
    box-shadow 
    box-shadow-x-0 
    box-shadow-y-3 
    box-shadow-b-6 
    box-shadow-s-0 
    box-shadow-black-16 
    m-0
    p-h-12
    p-v-5
    border-radius-15
    width-p-100
    flex
    flex-nowrap
    flex-align-center
    flex-justify-between
    cursor-pointer
    `,
    classNameCol2: `
        flex
        flex-align-center
    `,
    classNameIconCard: `
        flex
        m-r-10
    `,
};
