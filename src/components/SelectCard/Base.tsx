import MasterCard from '@/svg/masterCard';
import React, { useState } from 'react';
import Button from '../Button';
import InputCheckbox from '../Input/Checkbox';
import Text from '../Text';

export interface SelectCardClassProps {
    classNameContentCard?: string;
    classNameIconCard?: string;
    classNameCol2?: string;
    iconCard?: any;
}

export interface SelectCardBaseProps {
    cardNumber: string;
    onRequestButtonClick?: () => void;
    onCheckboxChange?: () => void;
}

export interface SelectCardProps
    extends SelectCardClassProps,
        SelectCardBaseProps {}

export const SelectCardBase = ({
    iconCard = <MasterCard size={35} />,
    classNameContentCard = '',
    classNameCol2 = '',
    classNameIconCard = '',
    onCheckboxChange,
    cardNumber,
    onRequestButtonClick,
}: SelectCardProps) => {
    const [checkboxChecked, setCheckboxChecked] = useState(false);

    const handleCheckboxChange = () => {
        setCheckboxChecked(!checkboxChecked);
        onCheckboxChange?.();
    };

    const formattedCardNumber = `xxxx xxxx xxxx ${cardNumber.slice(-4)}`;

    return (
        <div className={classNameContentCard} onClick={handleCheckboxChange}>
            <div className="col1">
                <InputCheckbox
                    defaultValue={checkboxChecked}
                    onChange={handleCheckboxChange}
                    label={
                        <>
                            <div className={classNameCol2}>
                                <span className={classNameIconCard}>
                                    {iconCard}
                                </span>
                                <Text styleTemplate="tolinkme4">
                                    {formattedCardNumber}
                                </Text>
                            </div>
                        </>
                    }
                    styleTemplate="tolinkme7"
                />
            </div>

            <div className="col3">
                <Button
                    styleTemplate="tolinkme2_2"
                    onClick={onRequestButtonClick}
                >
                    Request
                </Button>
            </div>
        </div>
    );
};

export default SelectCardBase;
