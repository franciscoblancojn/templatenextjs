import { useMemo } from 'react';

import * as styles from '@/components/SelectCard/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    SelectCardBaseProps,
    SelectCardBase,
} from '@/components/SelectCard/Base';

export const SelectCardStyle = { ...styles } as const;

export type SelectCardStyles = keyof typeof SelectCardStyle;

export interface SelectCardProps extends SelectCardBaseProps {
    styleSelectCard?: SelectCardStyles | ThemesType;
}

export const SelectCard = ({
    styleSelectCard = Theme?.styleTemplate ?? '_default',
    ...props
}: SelectCardProps) => {
    const Style = useMemo(
        () =>
            SelectCardStyle[styleSelectCard as SelectCardStyles] ??
            SelectCardStyle._default,
        [styleSelectCard]
    );

    return <SelectCardBase {...Style} {...props} />;
};
export default SelectCard;
