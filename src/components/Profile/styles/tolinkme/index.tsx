import { ProfileClassProps } from '@/components/Profile/Base';

export const tolinkme: ProfileClassProps = {
    styleTemplateNotification: 'tolinkmeFullStatic',

    classNameContent: `
        p-b-50
        color-black
    `,

    classNameContentBtn: `
        pos-f
        bottom-0
        z-index-1
        width-p-100
        p-h-15
        p-v-20
        left-0
        right-0
        bg-white
    `,

    classNameContentBtnPreview: `
        pos-f
        bottom-20
        z-index-1
        width-p-100
        p-h-17
        left-0
        right-0
        m-auto
        flex
        flex-justify-center
    `,
    classNameBtnButton: `
    color-brightPink-hover
    `,
    classNameContentBtnConatiner: `
        container
        flex
        flex-nowrap
        flex-gap-column-15
        flex-justify-between
    `,

    styleTemplateBtn: 'tolinkme',
    styleTemplateBtnPublish: 'tolinkme4',

    classNameLinks: {
        classNameContent: `

        `,
        classNameContentButton: `
            flex
            flex-nowrap
            flex-gap-column-11
            m-b-29
        `,
        classNameContentItems: `
            flex
            flex-md-gap-15
            flex-gap-row-15 flex-md-gap-row-15
            flex-gap-column-15
        `,
        classNameContentItem: `
            width-p-max-100
            flex-12 flex-md-6 flex-xl-4
        `,
        classNameContentNoItems: `
            m-auto
            text-center
            
        `,
        styleContentNoItems: {
            width: 'min(100%,13rem)',
        },
        styleTemplateTextNoItems: 'tolinkme12',
        classNameContentImgNoItems: `
            m-auto
            m-b-11
        `,
        styleContentImgNoItems: {
            width: 'min(100%,9rem)',
        },
        styleTemplateBtnL: 'tolinkme5',
    },

    /*
    classNamePublic: {
        classNameContent: `
            flex
            flex-justify-center
            text-center
            p-v-35
        `,
        styleTemplateText: 'tolinkme12',
        styleTemplateBtnPublic: 'tolinkme4',
    },
  */

    classNameAnalitycs: {
        classNameContent: `
            flex
            flex-md-gap-20
            flex-justify-between
            flex-gap-row-20
            width-p-100
        `,
        styleTemplateIconNumberText: [
            'tolinkme2',
            'tolinkme',
            'tolinkme',
            'tolinkme',
        ],

        classNameContentAnatilycsInfo: `
            flex-12 flex-md-6 flex flex-4  
            width-p-100
        `,
        classNameContentClickedButtons: `
            flex-12 flex-md-6
        `,
        classNameContentCountryVisitors: `
            flex-12 flex-md-6
        `,
        classNameContentCountryClicks: `
            flex-12 flex-md-6
        `,
        styleTemplateContentAnatilycsTitleNumber: 'tolinkme14',

        classNameContentAnatilycsInfoColm: `
            flex flex-12 flex-justify-between flex-gap-6 flex-gap-row-6
        `,
        classNameContentAnatilycsInfoColmNs: `
            flex-6 
        `,

        classNameContentClickedButtonsTitle: `
        m-b-20
        `,
        //styleTemplateContentAnatilycsSelect: 'tolinkme',
    },

    classNameContentModal: `
        pos-f
        inset-0
        width-p-100
        height-p-100
        overflow-auto
    `,
    styleTemplatePreviewModal: 'tolinkmeFull',

    classNameMonetize: {
        classNameContent: `
        flex
            flex-md-gap-15
            flex-gap-row-15 
            flex-gap-column-15
            m-auto
            flex-justify-center
        width-p-100
            
        `,
        stepLine: `
            border-t-1
            border-0 
            border-style-solid
            border-pinkishGreyThree
            font-14
            color-warmGreyFive
            width-p-90
            m-v-0
            m-h-10
            d-inline-block
        `,
        contentNumber: `
            font-22
            font-w-900
            
        `,
        activeStep: `
           color-brightPink
           font-30
        `,
        inactiveStep: `
        color-pinkishGreyThree
        `,
        classNameContentSteps: `
            tex-center
            margin-b-20
            flex
            flex-justify-space-between
            flex-align-center
            width-p-100
            flex-nowrap
        `,
        classNameContentTitle: `
        width-p-70
        `,
        classNameContentButton: `
        width-p-30
        text-right
        `,
        classNameContentTitleText: `
            font-18
            font-w-900

        `,
        classNameButtonBack: `
            font-15
            font-w-400
            color-teal
            text-decoration-underline
            bg-transparent
            border-0 
            font-nunito
            cursor-pointer
        `,
        classNameButtonBackIcon: `
            p-r-10
        `,
        classNameshowStepsButton: `
            width-p-20
            text-left
        `,
        classNameshowStepsTitle: `
            width-p-80
            text-left
            
        `,
        classNameTextStatus: `
            font-14
            font-w-900
            color-sonicSilver
            p-r-4
        `,
    },
};
