import CSS from 'csstype';
import UnicornSad from '@/lottin/UnicornSad';
import { RSLinkConfig, RSLinkConfigProps } from '@/components/RS/LinkConfig';
import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';
import { Text, TextStyles } from '@/components/Text';
import { Button, ButtonStyles } from '@/components/Button';
import { DragDrop } from '@/components/DragDrop';

import { useLang } from '@/lang/translate';
import Theme, { ThemesType } from '@/config/theme';
import { useEffect, useMemo, useState } from 'react';
import useLocalStorage from 'uselocalstoragenextjs';
import { Addons } from '@/interfaces/Addons';
export interface ContentLinksBaseProps {
    links?: RSLinkConfigDataProps[];
    addons?: Addons | Addons[];
    onChange?: (data: RSLinkConfigDataProps[]) => void;
    useMonetize?: boolean;
}

export interface ContentLinksClassProps {
    classNameContent?: string;
    classNameContentButton?: string;
    classNameContentItems?: string;
    classNameContentItem?: string;
    classNameContentNoItems?: string;
    styleContentNoItems?: CSS.Properties;
    classNameContentImgNoItems?: string;
    styleContentImgNoItems?: CSS.Properties;
    styleTemplateTextNoItems?: TextStyles | ThemesType;
    styleTemplateBtnL?: ButtonStyles | ThemesType;
    onChangeAddons?: (data: Addons[]) => void;
}

export interface ContentLinksProps
    extends ContentLinksClassProps,
        ContentLinksBaseProps {}

export const ContentLinks = ({
    classNameContent = '',
    classNameContentButton = '',
    classNameContentItems = '',
    classNameContentItem = '',
    classNameContentNoItems = '',
    styleContentNoItems = {},
    classNameContentImgNoItems = '',
    styleContentImgNoItems = {},
    styleTemplateTextNoItems = Theme.styleTemplate ?? '_default',
    styleTemplateBtnL = Theme.styleTemplate ?? '_default',
    useMonetize = false,
    links = [],
    onChange = (data: RSLinkConfigDataProps[]) => data,
}: ContentLinksProps) => {
    const _t = useLang();
    const [add, setAdd] = useState(false);

    const [linksRS, setLinksRS] = useState<RSLinkConfigDataProps[]>(links);

    const nItemsPrincipal = useMemo(
        () => linksRS.reduce((p, c) => p + (c?.isPrincipal ? 1 : 0), 0),
        [linksRS]
    );
    const { setLocalStorage } = useLocalStorage({
        name: 'nItemsPrincipal',
        defaultValue: nItemsPrincipal,
        parse: parseInt,
    });

    useEffect(() => {
        setLocalStorage(nItemsPrincipal);
    }, [nItemsPrincipal]);

    const addNew = (d: Partial<RSLinkConfigDataProps>) => {
        setLinksRS((pre) => [d, ...pre]);
        setAdd(true);
        setTimeout(() => {
            setAdd(false);
        }, 50);
    };
    const addNewLink = () => {
        addNew({});
    };
    const addNewAddon = () => {
        addNew({
            rs: 'addon',
        });
    };

    const onChangeRS = (i: number, data: RSLinkConfigDataProps) => {
        setLinksRS((pre) => [...pre.map((l, li) => (li == i ? data : l))]);
    };
    const onDeleteRS = (i: number) => {
        setLinksRS((pre) => {
            return [
                ...pre.map((l, li) => (li == i ? { ...l, delete: true } : l)),
            ];
        });
        setAdd(true);
        setTimeout(() => {
            setAdd(false);
        }, 50);
    };

    useEffect(() => {
        onChange(linksRS);
    }, [linksRS]);

    const linksRSMap = useMemo(() => {
        return (
            linksRS
                //.filter((l) => l.delete!==true)
                .map((data, id) => ({
                    id,
                    data: {
                        i: id,
                        useMonetize,
                        ...data,
                        onDelete: () => {
                            onDeleteRS(id);
                        },
                        onChange: (d: any) => {
                            onChangeRS(id, d);
                        },
                    },
                }))
        );
    }, [linksRS, useMonetize]);

    const ComponentlinksRSIsExist = useMemo(() => {
        if (add) {
            return <></>;
        }
        return (
            <>
                <DragDrop<RSLinkConfigProps>
                    id={`RSLinkConfigProps-${linksRSMap.length}`}
                    classNameContent={classNameContentItems}
                    classNameItem={classNameContentItem}
                    component={RSLinkConfig}
                    onChangeState={(s: RSLinkConfigProps[]) => {
                        setLinksRS((pre) =>
                            s.map((e, j) => {
                                const f = pre.find((p, i) => i == e.i);
                                return {
                                    ...f,
                                    order: j,
                                };
                            })
                        );
                    }}
                    items={linksRSMap}
                />
            </>
        );
    }, [linksRSMap, add]);

    const ComponentlinksRS = useMemo(
        () => (
            <>
                {linksRSMap.filter((e) => !e.data.delete).length == 0 ? (
                    <div
                        className={classNameContentNoItems}
                        style={styleContentNoItems}
                    >
                        <div
                            className={classNameContentImgNoItems}
                            style={styleContentImgNoItems}
                        >
                            {/* <Image src="face3.png" /> */}
                            <UnicornSad />
                        </div>
                        <Text styleTemplate={styleTemplateTextNoItems}>
                            {_t("You don't have any links to display")}
                        </Text>
                    </div>
                ) : (
                    <>{ComponentlinksRSIsExist}</>
                )}
            </>
        ),
        [linksRSMap, ComponentlinksRSIsExist, _t]
    );

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentButton}>
                    <Button
                        classNameBtn="color-brightPink-hover"
                        onClick={addNewLink}
                        styleTemplate={styleTemplateBtnL}
                    >
                        {_t('Add New Link')}
                    </Button>
                    <Button
                        classNameBtn="color-brightPink-hover"
                        styleTemplate={styleTemplateBtnL}
                        onClick={addNewAddon}
                    >
                        {_t('😎Addons')}
                    </Button>
                </div>

                {ComponentlinksRS}
            </div>
        </>
    );
};
export default ContentLinks;
