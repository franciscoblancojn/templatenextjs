import { useNotification } from '@/hook/useNotification';
import { MessagesData } from '@/interfaces/message';
import { useLang } from '@/lang/translate';
import UnicornSad from '@/lottin/UnicornSad';
import ArrowGoBack from '@/svg/arrowGoBack';
import { useRouter } from 'next/router';
import { useState } from 'react';
import ContentWidth from '../ContentWidth';
import { SubmitResult } from '../Form/Base';
import Text from '../Text';
import { PUT_MESSAGES_SEEN } from '@/api/tolinkme/messages';
import Trash from '@/svg/trash';
import { TabBase } from '../Tab/Base';

export interface ContentMessagesBaseProps {}

export interface ContentMessagesClassProps {
    messagesData?: MessagesData[];
}

export interface ContentMessagesProps
    extends ContentMessagesClassProps,
        ContentMessagesBaseProps {}

export const ContentMessages = ({ messagesData }: ContentMessagesProps) => {
    const _t = useLang();
    const route = useRouter();
    const [showIndividualMessage, setShowIndividualMessage] = useState(false);
    const { pop } = useNotification();
    const [selectedMessage, setSelectedMessage] = useState<MessagesData | null>(
        null
    );

    const seen_message = async (notificationUUID: any, status = 'CHECK') => {
        let r: SubmitResult | undefined = undefined;

        r = await PUT_MESSAGES_SEEN({
            data: {
                status: status,
            },
            uuid_message: notificationUUID,
            status: status,
        });

        if (r) {
            pop({
                message: r?.message ?? '',
                styleTemplate: 'tolinkme',
                type: r.status,
            });
            if (r.status == 'ok') {
                return;
            }
            if (r.status == 'error') {
                return;
            }
        }
    };

    const [deletedMessages, setDeletedMessages] = useState<string[]>([]);

    const delete_message = async (
        notificationUUID: any,
        status = 'DELETED'
    ) => {
        let r: SubmitResult | undefined = undefined;

        r = await PUT_MESSAGES_SEEN({
            data: {
                status: status,
            },
            uuid_message: notificationUUID,
            status: status,
        });

        if (r && r.status === 'ok') {
            setDeletedMessages((prevDeletedMessages) => [
                ...prevDeletedMessages,
                notificationUUID,
            ]);
        }

        if (r) {
            pop({
                message: 'Delete Message',
                styleTemplate: 'tolinkme',
                type: r.status,
            });
            if (r.status == 'ok') {
                return;
            }
            if (r.status == 'error') {
                return;
            }
        }
    };

    if (!route.isReady) {
        return <></>;
    }

    const handleMessageClick = (message: any) => {
        setSelectedMessage(message);
        setShowIndividualMessage(true);
    };

    const handleBackButtonClick = () => {
        setShowIndividualMessage(false);
    };
    return (
        <>
            {messagesData?.length === 0 ? (
                <>
                    <ContentWidth className="m-auto m-v-40" size={250}>
                        <Text
                            className="text-center"
                            styleTemplate="tolinkme27"
                        >
                            {_t('You have no messages available')}
                        </Text>
                        <UnicornSad />
                    </ContentWidth>
                </>
            ) : (
                <>
                    <div className="flex">
                        {!showIndividualMessage ? (
                            <>
                                <TabBase
                                    classNameContentTab={`
                                       flex
                                       flex-nowrap
                                       flex-gap-column-20
                                       overflow-auto
                                       overflow-no-bar
                                       pos-r
                                   `}
                                    classNameTab={`
                                       font-13
                                       font-nunito
                                       font-w-900
                                       p-v-18
                                       p-h-4
                                       pointer
                                       transition-5
                                       text-white-space-nowrap
                                       pos-r
                                       color-brightPink-hover
                                   `}
                                    classNameTabActive={`
                                       color-brightPink
                                   `}
                                    classNameTabNotActive={`
                                       color-greyishBrownFour
                                   `}
                                    classNameTabBar={`
                                       pos-a
                                       left-0
                                       bottom-0
                                       width-p-100
                                       height-4
                                       border-radius-100
                                       bg-brightPink
                                       transition-5
                                       transform
                                   `}
                                    classNameTabBarActive={`
                                   `}
                                    classNameTabBarNotActive={`
                                       transform-scale-X-0
                                   `}
                                    classNameTabContent={`
                                       bg-white
                                       p-v-30
                                       p-h-15
                                       font-nunito
                                   `}
                                    classNameContent="width-p-100"
                                    defaultSelect={0}
                                    items={[
                                        {
                                            tab: _t('Check'),
                                            content: (
                                                <>
                                                    {messagesData
                                                        ?.filter(
                                                            (message: any) =>
                                                                message?.status ===
                                                                'CHECK'
                                                        )
                                                        .map((message: any) => {
                                                            if (
                                                                deletedMessages.includes(
                                                                    message.uuid
                                                                )
                                                            ) {
                                                                return null;
                                                            }

                                                            return (
                                                                <>
                                                                    {/* <div className="flex-7 flex-md-6 m-b-10">
                                                                    <Text styleTemplate="tolinkme22">From</Text>
                                                                </div>
                                                                <div className="flex-5 flex-md-6 m-b-10">
                                                                    <Text styleTemplate="tolinkme22">Messages</Text>
                                                                </div> */}

                                                                    <div
                                                                        key={
                                                                            message.uuid
                                                                        }
                                                                        className={`${
                                                                            message?.status ===
                                                                            'UNCHECK'
                                                                                ? 'bg-whiteFive'
                                                                                : ''
                                                                        } width-p-100  bg-whiteTwo-hover cursor-pointer m-v-2 flex flex-justify-between flex-align-center  p-h-10`}
                                                                    >
                                                                        <div
                                                                            className="flex flex-align-center  width-p-100 MESSAGE_ITEM_____"
                                                                            onClick={(
                                                                                e
                                                                            ) => {
                                                                                let clickedDelete =
                                                                                    false;

                                                                                let ele =
                                                                                    e.target as HTMLElement;
                                                                                while (
                                                                                    !ele.classList.value.includes(
                                                                                        'MESSAGE_ITEM_____'
                                                                                    )
                                                                                ) {
                                                                                    if (
                                                                                        ele.classList.value.includes(
                                                                                            'delete____'
                                                                                        )
                                                                                    ) {
                                                                                        clickedDelete =
                                                                                            true;
                                                                                        break;
                                                                                    }
                                                                                    ele =
                                                                                        ele.parentElement as HTMLElement;
                                                                                }

                                                                                if (
                                                                                    !clickedDelete
                                                                                ) {
                                                                                    seen_message(
                                                                                        message?.uuid
                                                                                    );
                                                                                    handleMessageClick(
                                                                                        message
                                                                                    );
                                                                                }
                                                                            }}
                                                                        >
                                                                            <div className="m-v-20 flex flex-7 flex-md-6 ">
                                                                                <Text
                                                                                    className="overflow-hidden  width-p-100"
                                                                                    styleTemplate="tolinkme36"
                                                                                >
                                                                                    {message
                                                                                        ?.Messages?.[0]
                                                                                        ?.email ??
                                                                                        ''}
                                                                                </Text>
                                                                                <Text styleTemplate="tolinkme36">
                                                                                    {message
                                                                                        ?.Messages?.[0]
                                                                                        ?.phone ??
                                                                                        ''}
                                                                                </Text>
                                                                            </div>
                                                                            <div className="flex flex-justify-between flex-5 flex-md-6 ">
                                                                                <Text
                                                                                    style={{
                                                                                        maxWidth:
                                                                                            '80px',
                                                                                        overflow:
                                                                                            'hidden',
                                                                                        textOverflow:
                                                                                            'ellipsis',
                                                                                        whiteSpace:
                                                                                            'nowrap',
                                                                                        display:
                                                                                            'block',
                                                                                    }}
                                                                                    className="flex flex-align-center"
                                                                                    styleTemplate="tolinkme36"
                                                                                >
                                                                                    {
                                                                                        message
                                                                                            ?.Messages?.[0]
                                                                                            ?.text
                                                                                    }
                                                                                </Text>
                                                                                <div
                                                                                    onClick={() => {
                                                                                        delete_message(
                                                                                            message?.uuid
                                                                                        );
                                                                                    }}
                                                                                    className="p-r-20 z-index-99 color-red-hover delete____"
                                                                                >
                                                                                    <Trash
                                                                                        size={
                                                                                            20
                                                                                        }
                                                                                    />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </>
                                                            );
                                                        })}
                                                </>
                                            ),
                                        },
                                        {
                                            tab: _t('Uncheck'),
                                            content: (
                                                <>
                                                    {messagesData
                                                        ?.filter(
                                                            (message: any) =>
                                                                message?.status ===
                                                                'UNCHECK'
                                                        )
                                                        .map((message: any) => {
                                                            if (
                                                                deletedMessages.includes(
                                                                    message.uuid
                                                                )
                                                            ) {
                                                                return null;
                                                            }

                                                            return (
                                                                <>
                                                                    {/* <div className="flex-7 flex-md-6 m-b-10">
                                                                    <Text styleTemplate="tolinkme22">From</Text>
                                                                </div>
                                                                <div className="flex-5 flex-md-6 m-b-10">
                                                                    <Text styleTemplate="tolinkme22">Messages</Text>
                                                                </div> */}

                                                                    <div
                                                                        key={
                                                                            message.uuid
                                                                        }
                                                                        className={`${
                                                                            message?.status ===
                                                                            'UNCHECK'
                                                                                ? 'bg-whiteFive'
                                                                                : ''
                                                                        } width-p-100  bg-whiteTwo-hover cursor-pointer m-v-2 flex flex-justify-between flex-align-center  p-h-10`}
                                                                    >
                                                                        <div
                                                                            className="flex flex-align-center  width-p-100 MESSAGE_ITEM_____"
                                                                            onClick={(
                                                                                e
                                                                            ) => {
                                                                                let clickedDelete =
                                                                                    false;

                                                                                let ele =
                                                                                    e.target as HTMLElement;
                                                                                while (
                                                                                    !ele.classList.value.includes(
                                                                                        'MESSAGE_ITEM_____'
                                                                                    )
                                                                                ) {
                                                                                    if (
                                                                                        ele.classList.value.includes(
                                                                                            'delete____'
                                                                                        )
                                                                                    ) {
                                                                                        clickedDelete =
                                                                                            true;
                                                                                        break;
                                                                                    }
                                                                                    ele =
                                                                                        ele.parentElement as HTMLElement;
                                                                                }

                                                                                if (
                                                                                    !clickedDelete
                                                                                ) {
                                                                                    seen_message(
                                                                                        message?.uuid
                                                                                    );
                                                                                    handleMessageClick(
                                                                                        message
                                                                                    );
                                                                                }
                                                                            }}
                                                                        >
                                                                            <div className="m-v-20 flex flex-7 flex-md-6 ">
                                                                                <Text
                                                                                    className="overflow-hidden  width-p-100"
                                                                                    styleTemplate="tolinkme36"
                                                                                >
                                                                                    {message
                                                                                        ?.Messages?.[0]
                                                                                        ?.email ??
                                                                                        ''}
                                                                                </Text>
                                                                                <Text styleTemplate="tolinkme36">
                                                                                    {message
                                                                                        ?.Messages?.[0]
                                                                                        ?.phone ??
                                                                                        ''}
                                                                                </Text>
                                                                            </div>
                                                                            <div className="flex flex-justify-between flex-5 flex-md-6 ">
                                                                                <Text
                                                                                    style={{
                                                                                        maxWidth:
                                                                                            '80px',
                                                                                        overflow:
                                                                                            'hidden',
                                                                                        textOverflow:
                                                                                            'ellipsis',
                                                                                        whiteSpace:
                                                                                            'nowrap',
                                                                                        display:
                                                                                            'block',
                                                                                    }}
                                                                                    className="flex flex-align-center"
                                                                                    styleTemplate="tolinkme36"
                                                                                >
                                                                                    {
                                                                                        message
                                                                                            ?.Messages?.[0]
                                                                                            ?.text
                                                                                    }
                                                                                </Text>
                                                                                <div
                                                                                    onClick={() => {
                                                                                        delete_message(
                                                                                            message?.uuid
                                                                                        );
                                                                                    }}
                                                                                    className="p-r-20 z-index-99 color-red-hover delete____"
                                                                                >
                                                                                    <Trash
                                                                                        size={
                                                                                            20
                                                                                        }
                                                                                    />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </>
                                                            );
                                                        })}
                                                </>
                                            ),
                                        },
                                    ]}
                                />
                            </>
                        ) : (
                            <>
                                <div>
                                    <button
                                        onClick={handleBackButtonClick}
                                        className="
                                                font-16
                                                font-w-400
                                                color-teal
                                                text-decoration-underline
                                                bg-transparent
                                                border-0
                                                font-nunito
                                                cursor-pointer"
                                    >
                                        <span className="p-r-5">
                                            <ArrowGoBack size={20} />
                                        </span>
                                        {_t('Back')}
                                    </button>

                                    {selectedMessage && (
                                        <>
                                            <div className="">
                                                {selectedMessage?.Messages?.[0]
                                                    ?.name == '' ? (
                                                    <></>
                                                ) : (
                                                    <>
                                                        <Text
                                                            className="m-t-20"
                                                            styleTemplate="tolinkme39"
                                                        >
                                                            {_t('From')}
                                                            {':'}
                                                        </Text>
                                                    </>
                                                )}
                                                {selectedMessage?.Messages?.[0]
                                                    ?.email == '' || null ? (
                                                    <></>
                                                ) : (
                                                    <>
                                                        <Text styleTemplate="tolinkme36">
                                                            Email :{' '}
                                                            {selectedMessage
                                                                ?.Messages?.[0]
                                                                ?.email ?? ''}
                                                        </Text>
                                                    </>
                                                )}

                                                {selectedMessage?.Messages?.[0]
                                                    ?.phone == '' || null ? (
                                                    <></>
                                                ) : (
                                                    <>
                                                        <Text styleTemplate="tolinkme36">
                                                            Phone:{' '}
                                                            {selectedMessage
                                                                ?.Messages?.[0]
                                                                ?.phone ?? ''}
                                                        </Text>
                                                    </>
                                                )}

                                                {selectedMessage?.Messages?.[0]
                                                    ?.text == '' || null ? (
                                                    <></>
                                                ) : (
                                                    <>
                                                        <Text
                                                            className="m-t-20"
                                                            styleTemplate="tolinkme39"
                                                        >
                                                            {_t('Message')}
                                                            {':'}
                                                        </Text>
                                                        <Text styleTemplate="tolinkme36">
                                                            Message:{' '}
                                                            {selectedMessage
                                                                ?.Messages?.[0]
                                                                ?.text ?? ''}
                                                        </Text>
                                                    </>
                                                )}
                                            </div>
                                        </>
                                    )}
                                </div>
                            </>
                        )}
                    </div>
                </>
            )}
        </>
    );
};
export default ContentMessages;
