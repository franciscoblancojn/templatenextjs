import Monetize from '../Monetize';
import Steps from '../Steps';
import React, { useState } from 'react';
import UnicornWealthy from '@/lottin/UnicornWealthy';
import FormMonetizerForm from '../Form/MonetizerForm';
import UnicornHappy from '@/lottin/UnicornHappy';
import FormMonetizeCategory from '../Form/MonetizeCategory';
import FormVerifyIdentity from '../Form/VerifyIdentity';
import InformationSent from '../InformationSent';
import ContentWidth from '../ContentWidth';
import MonetizeSelfieForm from '../Form/MonetizeSelfie';
import Take from '@/svg/Take';
import { useLang } from '@/lang/translate';
import ArrowGoBack from '@/svg/arrowGoBack';
import { useData } from 'fenextjs-hook/cjs/useData';
import CurrentBalance from '../CurrentBalance';
import {
    POST_MONETIZE,
    PUT_MONETIZE_USER_DOCUMENTATION,
    PUT_MONETIZE_USER_PERSONAL_DATA,
    PUT_MONETIZE_USER_VALIDATION,
} from '@/api/tolinkme/monetize';
import { useUser } from '@/hook/useUser';
import { useNotification } from '@/hook/useNotification';
import { SubmitResult } from '../Form/Base';
import { MonetizeData } from '@/interfaces/MonetizeData';
import { MonetizeDataProps } from '@/interfaces/MonetizeDataProps';

export interface ContentMonetizeDataProps extends MonetizeDataProps {}

export interface ContentMonetizeBaseProps extends MonetizeData {}

export interface ContentMonetizeClassProps {
    classNameContent?: string;
    classNameMonetize?: ContentMonetizeClassProps;
    stepLine?: string;
    contentNumber?: string;
    activeStep?: string;
    inactiveStep?: string;
    classNameContentSteps?: string;
    classNameContentTitle?: string;
    classNameContentButton?: string;
    classNameContentTitleText?: string;
    classNameButtonBack?: string;
    classNameshowStepsButton?: string;
    classNameshowStepsTitle?: string;
    classNameTextStatus?: string;
    classNameButtonBackIcon?: string;
}

export interface ContentMonetizeProps
    extends ContentMonetizeClassProps,
        ContentMonetizeBaseProps {}

export const ContentMonetize = ({
    classNameContent = '',
    icon = <UnicornWealthy />,
    icon2 = <UnicornHappy />,
    icoBack = <ArrowGoBack size={20} />,
    iconCamera = <Take size={30} />,
    title = 'Monetize your Buttons',
    title2 = 'Take a Selfie',
    text = 'Start our onboarding Process, on',
    easy = ' 4 easy steps',
    text2 = 'Make sure you looks like your id',
    textBtn = 'Start Now',
    textcheckbox1 = 'Click here to confirm that you are at least',
    textcheckbox2 = ' and the age of majority in your place of residence.',
    textcheckbox18 = ' 18 years old',
    stepLine = '',
    contentNumber = '',
    activeStep = '',
    inactiveStep = '',
    classNameContentSteps = '',
    classNameContentTitle = '',
    classNameContentButton = '',
    classNameContentTitleText = '',
    classNameButtonBack = '',
    classNameshowStepsButton = '',
    classNameshowStepsTitle = '',
    classNameTextStatus = '',
    classNameButtonBackIcon = '',

    defaultValue = {
        isCreate: false,
        step1: {
            confirm18: false,
        },
        step2: {},
        step3: {},
        step4: {
            confirm: false,
            privacyPolicy: false,
        },
    },
    statusSteps = {
        step1: 'Without_Sending',
        step2: 'Without_Sending',
        step3: 'Without_Sending',
    },
    balance = {
        currentBalanceMoney: 0,
        currentPendingMoney: 0,
        BalanceMoneyMounth: 0,
        TotalButtonMonetize: 0,
        AmountSuscription: 0,
        btns: [],
    },
}: ContentMonetizeProps) => {
    const { user, load } = useUser();
    const [loader, setLoader] = useState<boolean | undefined>(undefined);
    const { pop } = useNotification();
    const { data, onChangeData } =
        useData<ContentMonetizeDataProps>(defaultValue);

    const [showSteps, setShowSteps] = useState(true);
    const [step, setStep] = useState(
        Object.values(statusSteps).every((e) => e == 'Without_Sending') ? 0 : 5
    );
    const next = () => {
        setStep((prev) => prev + 1);
    };
    const stepNumbers = [];
    for (let i = 1; i <= 4; i++) {
        stepNumbers.push(i);
    }

    let backButtonTitle = '';
    switch (step) {
        case 1:
            backButtonTitle = 'Cancel';
            break;
        case 2:
            backButtonTitle = 'Back';
            break;
        case 3:
            backButtonTitle = 'Back';
            break;
        case 4:
            backButtonTitle = 'Back';
            break;
        default:
            backButtonTitle = '';
    }

    const _t = useLang();
    const submit = async () => {
        setLoader(true);
        let r: SubmitResult | undefined = undefined;
        if (data.isCreate) {
            //Update PUT
            if (step === 1) {
                r = await PUT_MONETIZE_USER_PERSONAL_DATA({
                    step1: data.step1,
                    user,
                });
            }
            if (step === 2) {
                r = await PUT_MONETIZE_USER_DOCUMENTATION({
                    step2: data.step2,
                    user,
                });
            }
            if (step === 3) {
                r = await PUT_MONETIZE_USER_VALIDATION({
                    step3: data.step3,
                    user,
                });
            }
        } else {
            //Create POST
            r = await POST_MONETIZE({
                user,
                data,
            });
        }
        if (r) {
            pop({
                message: r?.message ?? '',
                styleTemplate: 'tolinkme',
                type: r.status,
            });
            if (r.status == 'ok') {
                window.location.reload();
                return;
            }
            if (r.status == 'error') {
                return;
            }
        }
        setStep(5);
        setLoader(undefined);
    };
    if (!load) {
        return <></>;
    }

    return (
        <ContentWidth size={350} className={classNameContent}>
            {Object.values(statusSteps).every((e) => e == 'Verified') ? (
                <>
                    <CurrentBalance btns={balance.btns} {...balance} />
                </>
            ) : (
                <>
                    {!showSteps && step > 0 && step < 5 && (
                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                width: '100%',
                            }}
                        >
                            <div className={classNameshowStepsButton}>
                                {step >= 1 && step <= 4 && (
                                    <button
                                        className={classNameButtonBack}
                                        onClick={() => setStep(5)}
                                    >
                                        <span
                                            className={classNameButtonBackIcon}
                                        >
                                            {icoBack}
                                        </span>
                                        {_t(backButtonTitle)}
                                    </button>
                                )}
                            </div>
                            <div className={classNameshowStepsTitle}>
                                {step === 1 && (
                                    <h3 className={classNameContentTitleText}>
                                        <span className={classNameTextStatus}>
                                            {_t('Appeal')}
                                        </span>
                                        {_t('- Personal Details')}
                                    </h3>
                                )}
                                {step === 2 && (
                                    <h3 className={classNameContentTitleText}>
                                        <span className={classNameTextStatus}>
                                            {_t('Appeal')}
                                        </span>
                                        {_t('- Verify your Identity')}
                                    </h3>
                                )}
                                {step === 3 && (
                                    <h3 className={classNameContentTitleText}>
                                        <span className={classNameTextStatus}>
                                            {_t('Appeal')}
                                        </span>
                                        {_t('- Send Us a Selfie')}
                                    </h3>
                                )}
                                {step === 4 && (
                                    <h3
                                        className={classNameContentTitleText}
                                    ></h3>
                                )}
                            </div>
                        </div>
                    )}
                    {showSteps && step > 0 && step < 5 && (
                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                width: '100%',
                            }}
                        >
                            <div className={classNameContentTitle}>
                                {step === 1 && (
                                    <h3 className={classNameContentTitleText}>
                                        {_t('Personal Details')}
                                    </h3>
                                )}
                                {step === 2 && (
                                    <h3 className={classNameContentTitleText}>
                                        {_t('Verify your Identity')}
                                    </h3>
                                )}
                                {step === 3 && (
                                    <h3 className={classNameContentTitleText}>
                                        {_t('Send Us a Selfie')}
                                    </h3>
                                )}
                                {step === 4 && (
                                    <h3
                                        className={classNameContentTitleText}
                                    ></h3>
                                )}
                            </div>
                            <div className={classNameContentButton}>
                                {step >= 1 && step <= 4 && (
                                    <button
                                        className={classNameButtonBack}
                                        onClick={() => setStep(step - 1)}
                                    >
                                        <span
                                            className={classNameButtonBackIcon}
                                        >
                                            {icoBack}
                                        </span>
                                        {_t(backButtonTitle)}
                                    </button>
                                )}
                            </div>
                        </div>
                    )}

                    <div className={classNameContentSteps}>
                        {showSteps &&
                            step > 0 &&
                            step < 5 &&
                            stepNumbers.map((number, index) => (
                                <React.Fragment key={number}>
                                    {index > 0 && (
                                        <div
                                            className={`${stepLine} ${
                                                step >= index + 1
                                                    ? activeStep
                                                    : ''
                                            }`}
                                        />
                                    )}
                                    <div
                                        className={`${contentNumber} ${
                                            step >= index + 1
                                                ? activeStep
                                                : inactiveStep
                                        }`}
                                    >
                                        {number}
                                    </div>
                                </React.Fragment>
                            ))}
                    </div>

                    <Steps
                        currentStep={step}
                        steps={[
                            {
                                id: '1',
                                content: (
                                    <Monetize
                                        onClick={next}
                                        textBtn2={textBtn}
                                        icon={icon}
                                        title={title}
                                        text={text}
                                        easy={easy}
                                    />
                                ),
                            },
                            {
                                id: '2',
                                content: (
                                    <FormMonetizerForm
                                        onClick={!showSteps ? submit : next}
                                        textBtnNext={
                                            !showSteps ? 'Submit' : 'Next'
                                        }
                                        text18={textcheckbox18}
                                        text1={textcheckbox1}
                                        text2={textcheckbox2}
                                        styleTemplate={'tolinkme2'}
                                        defaultValue={data.step1}
                                        onChange={onChangeData('step1')}
                                        extraLoader={loader}
                                    />
                                ),
                            },
                            {
                                id: '3',
                                content: (
                                    <FormVerifyIdentity
                                        onClick={!showSteps ? submit : next}
                                        defaultValue={data.step2}
                                        onChange={onChangeData('step2')}
                                        textBtn={!showSteps ? 'Submit' : 'Next'}
                                        extraLoader={loader}
                                    />
                                ),
                            },
                            {
                                id: '4',
                                content: (
                                    <MonetizeSelfieForm
                                        title={title2}
                                        icon={icon2}
                                        BtnNext={!showSteps ? 'Submit' : 'Next'}
                                        iconCamera={iconCamera}
                                        text={text2}
                                        onClick={!showSteps ? submit : next}
                                        defaultValue={data.step3}
                                        onChange={onChangeData('step3')}
                                        extraLoader={loader}
                                    />
                                ),
                            },
                            {
                                id: '5',
                                content: (
                                    <FormMonetizeCategory
                                        onClick={submit}
                                        defaultValue={data.step4}
                                        onChange={onChangeData('step4')}
                                        extraLoader={loader}
                                    />
                                ),
                            },
                            {
                                id: '6',
                                content: (
                                    <InformationSent
                                        setStep={setStep}
                                        setShowSteps={setShowSteps}
                                        statusSteps={statusSteps}
                                    />
                                ),
                            },
                        ]}
                    />
                </>
            )}

            {}
        </ContentWidth>
    );
};
export default ContentMonetize;
