//import { useLang } from '@/lang/translate';

import Theme, { ThemesType } from '@/config/theme';
import { AnalitycsData } from '@/interfaces/AnalityscsData';
import { useLang } from '@/lang/translate';
import Clicks from '@/svg/clicks';
import Estadisticas from '@/svg/estadisticas';
import Stack from '@/svg/stack';
//import { useQuery } from 'fenextjs-hook/cjs/useQuery';
import { useEffect, useMemo, useState } from 'react';
import Button from '../Button';
import CountryCount from '../CountryCount';
import IconNumberText, { IconNumberTextStyles } from '../IconNumberText';
import ImgRsCount from '../ImgRsCount';
import { LoaderLineMultiple } from '../Loader/LoaderLine';
import Space from '../Space';
import Text, { TextStyles } from '../Text';
import * as ProfileApi from '@/api/tolinkme/profile';
import log from '@/functions/log';
import FilterDate, { FilterDateStyles } from '../FilterDate';
import { useRouter } from 'next/router';
//import { useQuery } from 'fenextjs-hook/cjs/useQuery';

export interface ContentAnalitycsBaseProps {
    profile_uuid: string;
    user_uuid: string;
}

export interface ContentAnalitycsClassProps {
    classNameContent?: string;
    classNameContentAnatilycsInfo?: string;
    classNameContentClickedButtonsTitle?: string;
    classNameTemplateContentAnatilycsTotalClicks?: string;
    classNameTemplateContentCountry?: string;
    classNameContentClickedButtons?: string;
    classNameContentCountryVisitors?: string;
    classNameContentCountryClicks?: string;
    classNameContentAnatilycsInfoColm?: string;
    classNameContentAnatilycsInfoColmNs?: string;
    styleTemplateContentAnatilycsTitleNumber?: TextStyles | ThemesType;
    styleTemplateContentAnatilycsSelect?: FilterDateStyles | ThemesType;
    styleTemplateIconNumberText?: IconNumberTextStyles[] | ThemesType[];
}

export interface ContentAnalitycsProps
    extends ContentAnalitycsClassProps,
        ContentAnalitycsBaseProps {}

export const ContentAnalitycs = ({
    classNameContentAnatilycsInfoColm = '',
    classNameContentAnatilycsInfo = '',
    classNameContent = '',
    classNameContentAnatilycsInfoColmNs = '',
    classNameContentClickedButtonsTitle = '',
    classNameContentClickedButtons = '',
    classNameContentCountryVisitors = '',

    styleTemplateContentAnatilycsTitleNumber = Theme.styleTemplate ??
        '_default',
    styleTemplateContentAnatilycsSelect = Theme.styleTemplate ?? '_default',

    styleTemplateIconNumberText = [],
    profile_uuid,
    user_uuid,
}: //...props
ContentAnalitycsProps) => {
    const _t = useLang();
    const route = useRouter();
    const [loader, setLoader] = useState(true);
    const [DATA, setAnalitycs] = useState<AnalitycsData>({});
    const onLoadAnalitycs = async () => {
        if (!route.isReady) {
            return;
        }
        setLoader(true);
        try {
            const result = await ProfileApi.GETANALITYCS({
                profile_id: profile_uuid ?? '',
                user_id: user_uuid ?? '',
                start: `${route.query.start ?? 0}`,
                end: `${route.query.end ?? new Date().getTime()}`,
            });
            setAnalitycs(result);
        } catch (error) {
            log('onChangeDate', error);
        }
        setLoader(false);
    };

    useEffect(() => {
        onLoadAnalitycs();
    }, [route?.isReady && route?.query]);

    const [limitCountryVisitors, setLimitCountryVisitors] = useState(5);

    const CountryVisitors = useMemo(() => {
        if (loader) {
            return (
                <>
                    <LoaderLineMultiple />
                </>
            );
        }
        return (
            <>
                {DATA?.countryVisitors
                    ?.sort((ca, cb) => cb.count - ca.count)
                    .filter(
                        (country, i) =>
                            country?.country != 'undefined' &&
                            (i < limitCountryVisitors ||
                                limitCountryVisitors == -1)
                    )
                    .map((country, i) => (
                        <CountryCount key={i} {...country} />
                    ))}

                {limitCountryVisitors != -1 ? (
                    <>
                        <div className="flex flex-justify-center">
                            <Button
                                styleTemplate="tolinkme2_2"
                                onClick={() => {
                                    setLimitCountryVisitors(-1);
                                }}
                            >
                                {_t('Ver Mas')}
                            </Button>
                        </div>
                    </>
                ) : (
                    <></>
                )}
            </>
        );
    }, [DATA, limitCountryVisitors, loader]);

    const [limitCountryClicks, setLimitCountryClicks] = useState(5);

    const CountryClicks = useMemo(() => {
        if (loader) {
            return (
                <>
                    <LoaderLineMultiple />
                </>
            );
        }
        return (
            <>
                {DATA?.countryClicks
                    ?.sort((ca, cb) => cb.count - ca.count)
                    .filter(
                        (country, i) =>
                            country?.country != 'undefined' &&
                            (i < limitCountryClicks || limitCountryClicks == -1)
                    )
                    .map((country, i) => (
                        <CountryCount key={i} {...country} />
                    ))}
                {limitCountryClicks != -1 ? (
                    <>
                        <div className="flex flex-justify-center">
                            <Button
                                styleTemplate="tolinkme2_2"
                                onClick={() => {
                                    setLimitCountryClicks(-1);
                                }}
                            >
                                {_t('Ver Mas')}
                            </Button>
                        </div>
                    </>
                ) : (
                    <></>
                )}
            </>
        );
    }, [DATA, limitCountryClicks, loader]);

    const [limitBtnClicks, setLimitBtnClicks] = useState(5);

    const BtnClicks = useMemo(() => {
        {
            if (loader) {
                return (
                    <>
                        <LoaderLineMultiple />
                    </>
                );
            }
        }
        return (
            <>
                {DATA?.listClicksButtons
                    ?.sort((a, b) => b.count - a.count)
                    .filter(
                        (rs, i) => i < limitBtnClicks || limitBtnClicks == -1
                    )
                    .map((rs, i) => (
                        <ImgRsCount key={i} {...rs} />
                    ))}
                {limitBtnClicks != -1 ? (
                    <>
                        <div className="flex flex-justify-center">
                            <Button
                                styleTemplate="tolinkme2_2"
                                onClick={() => {
                                    setLimitBtnClicks(-1);
                                }}
                            >
                                {_t('Ver Mas')}
                            </Button>
                        </div>
                    </>
                ) : (
                    <></>
                )}
            </>
        );
    }, [DATA, limitBtnClicks, loader]);

    const ComponentNumbers = useMemo(() => {
        if (loader) {
            return (
                <>
                    <LoaderLineMultiple />
                </>
            );
        }

        return (
            <>
                <div
                    className={
                        classNameContentAnatilycsInfoColmNs +
                        'flex flex-justify-left'
                    }
                >
                    <IconNumberText
                        number={DATA?.views ?? 0}
                        text="Visitors"
                        icon={<Estadisticas size={28} />}
                        styleTemplate={
                            styleTemplateIconNumberText?.[0] ?? 'tolinkme'
                        }
                    />
                </div>
                <div className={classNameContentAnatilycsInfoColmNs}></div>
                <div className={classNameContentAnatilycsInfoColmNs}>
                    <IconNumberText
                        number={DATA?.totalClicks ?? 0}
                        text="Total of clicks on buttons"
                        icon={<Clicks size={28} />}
                        styleTemplate={
                            styleTemplateIconNumberText?.[1] ?? 'tolinkme'
                        }
                    />
                </div>
                <div className={classNameContentAnatilycsInfoColmNs}>
                    <IconNumberText
                        number={DATA?.totalButtons ?? 0}
                        text="Total amount of Buttons"
                        icon={<Stack size={28} />}
                        styleTemplate={
                            styleTemplateIconNumberText?.[2] ?? 'tolinkme'
                        }
                    />
                </div>
                <div className={classNameContentAnatilycsInfoColmNs}>
                    <IconNumberText
                        number={DATA?.porcentClicks ?? 0}
                        text="Avg clicks on buttons per visitor"
                        icon={<Clicks size={28} />}
                        caraterNumber="%"
                        styleTemplate={
                            styleTemplateIconNumberText?.[3] ?? 'tolinkme'
                        }
                    />
                </div>
            </>
        );
    }, [loader, DATA, DATA, DATA, DATA]);

    if (!route.isReady) {
        return <></>;
    }
    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentAnatilycsInfo}>
                    <div className={classNameContentAnatilycsInfoColm}>
                        <div>
                            <FilterDate
                                styleTemplate={
                                    styleTemplateContentAnatilycsSelect
                                }
                            />
                        </div>
                        <Space size={25} />
                    </div>

                    <div className={classNameContentAnatilycsInfoColm}>
                        {ComponentNumbers}
                    </div>
                </div>

                <div className={classNameContentClickedButtons}>
                    <Text
                        className={classNameContentClickedButtonsTitle}
                        styleTemplate={styleTemplateContentAnatilycsTitleNumber}
                    >
                        {_t('Most Clicked Buttons')}
                    </Text>

                    {BtnClicks}
                </div>
                <div className={classNameContentCountryVisitors}>
                    <Text
                        className={classNameContentClickedButtonsTitle}
                        styleTemplate={styleTemplateContentAnatilycsTitleNumber}
                    >
                        {_t('Country Visitors')}
                    </Text>
                    {CountryVisitors}
                </div>
                <div className={classNameContentCountryVisitors}>
                    <Text
                        className={classNameContentClickedButtonsTitle}
                        styleTemplate={styleTemplateContentAnatilycsTitleNumber}
                    >
                        {_t('Country Clicks')}
                    </Text>
                    {CountryClicks}
                </div>
            </div>
        </>
    );
};
export default ContentAnalitycs;
