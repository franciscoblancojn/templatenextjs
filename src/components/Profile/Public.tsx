import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ImageProfile } from '@/components/ImageProfile';
import ContentWidth from '../ContentWidth';
import Space from '../Space';
import ButtonRsView from '../ButtonRsView';
import Settings from '@/svg/Settings';
import { RS_All } from '@/data/components/RS';
import { getBg } from '@/functions/getBg';
import { getBtn } from '@/functions/getBtn';
import { getText } from '@/functions/getText';
import { useUser } from '@/hook/useUser';
import url from '@/data/routes';
import Layer from '../Layer';
import { BorderRadiusButtonValues } from '@/interfaces/Button';
import { MessageUserSend } from '@/interfaces/message';
import { useLang } from '@/lang/translate';
import { PublicData } from '@/interfaces/PublicData';
import Toltip from '../Toltip';
import InputText from '../Input/Text';
import Button from '../Button';
import { POST_MESSAGES } from '@/api/tolinkme/messages';
import { SubmitResult } from '../Form/Base';
import { useNotification } from '@/hook/useNotification';
import { useData } from 'fenextjs-hook/cjs/useData';
import { CSSProperties } from 'styled-components';
import Text from '../Text';
import ReCAPTCHA from 'react-google-recaptcha';
import Layer2 from '../Layer2';

export interface ProfilePublicPublicProps extends PublicData, MessageUserSend {}

export interface ProfilePublicClassProps {
    onChange?: (data: MessageUserSend) => void;
    defaultValue?: MessageUserSend;
}

export interface ProfilePublicProps
    extends ProfilePublicClassProps,
        ProfilePublicPublicProps {}

export const ProfilePublic = ({
    style,
    onClickBtn,
    onChange = () => {},
    defaultValue = {
        email: '',
        message: '',
        name: '',
        phone: '',
    },
    uuid,
    ...props
}: ProfilePublicProps) => {
    console.log(props, 'props2');
    const _t = useLang();
    const classBg = 'pos-f inset-0 width-p-100 height-p-100';
    const [loader, setLoader] = useState<boolean | undefined>(undefined);
    const { pop } = useNotification();

    const { data, onChangeData, setData } =
        useData<MessageUserSend>(defaultValue);
    const [errors, setErrors] = useState({
        name: '',
        email: '',
        phone: '',
        message: '',
    });
    useEffect(() => {
        onChange(data);
    }, [data]);

    const [resetKey, setResetKey] = useState(0);

    type ValidationErrors = {
        name?: string;
        email?: string;
        phone?: string;
        message?: string;
    };
    console.log(props.links, 'props');

    const submit = useCallback(async () => {
        // Validaciones
        const validationErrors: ValidationErrors = {};

        // Validar nombre si está activo
        if (props.links?.some((btn) => btn.active && btn.addons?.form?.name)) {
            if (!data.name) {
                validationErrors.name = 'Nombre es requerido';
            }
        }

        // Validar correo electrónico si está activo
        if (props.links?.some((btn) => btn.active && btn.addons?.form?.email)) {
            if (!data.email) {
                validationErrors.email = 'Correo electrónico es requerido';
            }
        }

        // Validar teléfono si está activo
        if (props.links?.some((btn) => btn.active && btn.addons?.form?.phone)) {
            if (!data.phone) {
                validationErrors.phone = 'Teléfono es requerido';
            }
        }

        // Validar mensaje si está activo
        if (
            props.links?.some((btn) => btn.active && btn.addons?.form?.message)
        ) {
            if (!data.message) {
                validationErrors.message = 'Mensaje es requerido';
            }
        }

        // Verificar si hay errores
        if (Object.keys(validationErrors).length > 0) {
            setErrors(
                validationErrors as {
                    name: string;
                    email: string;
                    phone: string;
                    message: string;
                }
            );
            return;
        }
        console.log(validationErrors, 'validationErrors');

        setLoader(true);
        try {
            let r: SubmitResult | undefined = undefined;
            r = await POST_MESSAGES({
                data: {
                    message_details: {
                        email: data?.email,
                        message: data?.message,
                        name: data?.name,
                        phone: data?.phone,
                    },
                    user_details: {
                        core_uuid: uuid,
                    },
                },
                user: {
                    name: '',
                },
            });

            if (r) {
                pop({
                    message: r?.message ?? '',
                    styleTemplate: 'tolinkme',
                    type: r.status,
                });
                if (r.status === 'ok') {
                    setData({
                        email: '',
                        message: '',
                        name: '',
                        phone: '',
                    } as MessageUserSend);
                    setResetKey((prevKey) => prevKey + 1);
                    setErrors({
                        name: '',
                        email: '',
                        phone: '',
                        message: '',
                    });
                    return;
                }
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoader(false);
        }
    }, [data, uuid, resetKey, setErrors, props.links]);

    const links = useMemo(
        () =>
            (props?.links ?? [])
                .filter((l) => !l.delete)
                .sort((a, b) => (a?.order ?? 0) - (b?.order ?? 0)),
        [props.links]
    );

    const Bg = useMemo(() => {
        const bgDesktop = getBg(style.bg?.bg ?? {});
        const bgLayerDesktop = style.bg?.useLayer ? (
            getBg(style.bg?.bgLayer ?? {})
        ) : (
            <></>
        );

        const bgMovil = getBg(style.bg?.bgMovil ?? {});
        const bgLayerMovil = style.bg?.useLayerMovil ? (
            getBg(style.bg?.bgMovilLayer ?? {})
        ) : (
            <></>
        );

        const bgComponent =
            window.innerWidth > 991
                ? bgDesktop
                : style.bg?.bgMovil
                ? bgMovil
                : bgDesktop;
        const layerComponent =
            window.innerWidth > 991 ? (
                style.bg?.bgLayer ? (
                    bgLayerDesktop
                ) : (
                    <></>
                )
            ) : style.bg?.useLayerMovil ? (
                bgLayerMovil
            ) : (
                <></>
            );

        return (
            <>
                <div className={classBg}>
                    {bgComponent}
                    {layerComponent}
                </div>
            </>
        );
    }, [style.bg, window.innerWidth, _t]);

    const ImageP = useMemo(() => {
        return (
            <ImageProfile
                data={style.img}
                img={style.avatar}
                styleTemplate={'tolinkme'}
            />
        );
    }, [style.img]);

    const ButtonsPrincipales = useMemo(() => {
        return links
            ?.filter((btn) => btn.active && (btn.isPrincipal ?? false))
            .map((btn, i) => {
                return (
                    <a
                        key={i}
                        href={btn.url}
                        target={'_blank'}
                        className="d-inline-block"
                        onClick={() => {
                            onClickBtn?.(btn);
                        }}
                        rel="noreferrer"
                    >
                        <ButtonRsView
                            config={style.btnPrincipal}
                            Icon={
                                RS_All?.find((r) => r.id == btn.rs)?.svg ??
                                Settings
                            }
                        />
                    </a>
                );
            });
    }, [links, style.btnPrincipal, _t]);

    const Info = useMemo(() => {
        const titleComponent = getText(
            style.info?.name ?? {},
            style.username_title ?? style.name
        );
        const descriptionComponent = getText(
            style.info?.description ?? {},
            style.description
        );
        const webComponent = getText(style.info?.web ?? {}, style.web);

        return (
            <>
                <div className="text-center text-capitalize line-h-10">
                    {titleComponent}
                </div>
                <Space size={12} />
                <div className="flex flex-justify-center flex-gap-column-10 flex-gap-row-10">
                    {ButtonsPrincipales}
                </div>
                <Space size={12} />
                {descriptionComponent}
                <Space size={12} />
                <a
                    href={style.web}
                    target="_blank"
                    className="d-block text-decoration-underline text-word-break-all"
                    style={{
                        color: style.info?.web?.color,
                    }}
                    rel="noreferrer"
                >
                    {webComponent}
                </a>
            </>
        );
    }, [style.info, ButtonsPrincipales, _t]);

    const { user, load } = useUser();

    const getUrlLoginPayment = useCallback(
        (uuid: string) => {
            if (!load) {
                return '#';
            }
            const urlPay = `${url.pay}/${uuid}`;

            return urlPay;
        },
        [user, load, _t]
    );

    const reca = '6Lf-wx8pAAAAACIlmU5V02oNIdARFUqOpW83X40_';

    const Buttons = useMemo(() => {
        return links
            ?.filter((btn) => btn.active && !(btn.isPrincipal ?? false))
            .map((btn, i) => {
                if (btn?.rs === 'addon') {
                    return (
                        <React.Fragment key={i}>
                            {btn?.addons?.fromActive === true && (
                                <>
                                    {btn?.addons?.form?.title_placeholder ==
                                    '' ? (
                                        <></>
                                    ) : (
                                        <>
                                            <Text
                                                style={{
                                                    textAlign: `${
                                                        style?.formCustomInput
                                                            ?.title1
                                                            ?.textAlignTitleForm ??
                                                        'center'
                                                    }`,

                                                    fontFamily: `${style?.formCustomInput?.title1?.fontFamily}`,
                                                    lineHeight: `${style?.formCustomInput?.title1?.lineHeight}px`,
                                                    fontSize: `${style?.formCustomInput?.title1?.fontSize}px`,
                                                    color: `${style?.formCustomInput?.title1?.color}`,
                                                    fontWeight: `${
                                                        style?.formCustomInput
                                                            ?.title1
                                                            ?.fontWeight ===
                                                        'black'
                                                            ? '900'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title1
                                                                  ?.fontWeight ===
                                                              'extra-bold'
                                                            ? '800'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title1
                                                                  ?.fontWeight ===
                                                              'bold'
                                                            ? '700'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title1
                                                                  ?.fontWeight ===
                                                              'semi-bold'
                                                            ? '600'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title1
                                                                  ?.fontWeight ===
                                                              'medium'
                                                            ? '500'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title1
                                                                  ?.fontWeight ===
                                                              'regular'
                                                            ? '400'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title1
                                                                  ?.fontWeight ===
                                                              'light'
                                                            ? '300'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title1
                                                                  ?.fontWeight ===
                                                              'extra-light'
                                                            ? '200'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title1
                                                                  ?.fontWeight ===
                                                              'thin'
                                                            ? '100'
                                                            : ''
                                                    }`,
                                                    textDecoration: 'underline',
                                                }}
                                            >
                                                {
                                                    btn?.addons?.form
                                                        ?.title_placeholder
                                                }
                                            </Text>
                                        </>
                                    )}

                                    {btn?.addons?.form
                                        ?.description_placeholder == '' ? (
                                        <></>
                                    ) : (
                                        <>
                                            <Text
                                                style={{
                                                    textAlign: `${
                                                        style?.formCustomInput
                                                            ?.title2
                                                            ?.textAlignDescriptionForm ??
                                                        'center'
                                                    }`,
                                                    fontFamily: `${style?.formCustomInput?.title2?.fontFamily}`,
                                                    lineHeight: `${style?.formCustomInput?.title2?.lineHeight}px`,
                                                    fontSize: `${style?.formCustomInput?.title2?.fontSize}px`,
                                                    color: `${style?.formCustomInput?.title2?.color}`,
                                                    fontWeight: `${
                                                        style?.formCustomInput
                                                            ?.title2
                                                            ?.fontWeight ===
                                                        'black'
                                                            ? '900'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title2
                                                                  ?.fontWeight ===
                                                              'extra-bold'
                                                            ? '800'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title2
                                                                  ?.fontWeight ===
                                                              'bold'
                                                            ? '700'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title2
                                                                  ?.fontWeight ===
                                                              'semi-bold'
                                                            ? '600'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title2
                                                                  ?.fontWeight ===
                                                              'medium'
                                                            ? '500'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title2
                                                                  ?.fontWeight ===
                                                              'regular'
                                                            ? '400'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title2
                                                                  ?.fontWeight ===
                                                              'light'
                                                            ? '300'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title2
                                                                  ?.fontWeight ===
                                                              'extra-light'
                                                            ? '200'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.title2
                                                                  ?.fontWeight ===
                                                              'thin'
                                                            ? '100'
                                                            : ''
                                                    }`,
                                                    textDecoration: 'underline',
                                                }}
                                            >
                                                {
                                                    btn?.addons?.form
                                                        ?.description_placeholder
                                                }
                                            </Text>
                                        </>
                                    )}
                                    {btn?.addons?.form?.name && (
                                        <>
                                            <div
                                                className={`m-b-15 content-place-input`}
                                            >
                                                <InputText
                                                    key={resetKey}
                                                    styleTemplate="tolinkmeAddons"
                                                    style={{
                                                        border: `${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border
                                                                ?.type ===
                                                            'solid'
                                                                ? 'solid'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'dashed'
                                                                ? 'dashed'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'dotted'
                                                                ? 'dotted'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'double'
                                                                ? 'double'
                                                                : ''
                                                        } ${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border?.size
                                                        }px ${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border?.color
                                                        }`,
                                                        borderRadius: `${
                                                            style
                                                                ?.formCustomInput
                                                                ?.borderRadius ===
                                                            'rounded'
                                                                ? '100px'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.borderRadius ===
                                                                  'semi-rounded'
                                                                ? '10px'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.borderRadius ===
                                                                  'no-rounding'
                                                                ? '0px'
                                                                : ''
                                                        }`,
                                                        boxShadow: `
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.type ===
                                                                'normal'
                                                                    ? ''
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.boxShadow
                                                                          ?.type ===
                                                                      'inset'
                                                                    ? 'inset'
                                                                    : ''
                                                            } 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.x
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.y
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.blur
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.size
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.color
                                                            }
                                                        `,
                                                    }}
                                                    styleInput={
                                                        {
                                                            ['--placeholder-colors']: `${style?.formCustomInput?.text?.color}`,
                                                            fontFamily: `${style?.formCustomInput?.text?.fontFamily}`,
                                                            lineHeight: `${style?.formCustomInput?.text?.lineHeight}px`,
                                                            fontSize: `${style?.formCustomInput?.text?.fontSize}px`,
                                                            color: `${style?.formCustomInput?.text?.color}`,
                                                            fontWeight: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.text
                                                                    ?.fontWeight ===
                                                                'black'
                                                                    ? '900'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'extra-bold'
                                                                    ? '800'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'bold'
                                                                    ? '700'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'semi-bold'
                                                                    ? '600'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'medium'
                                                                    ? '500'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'regular'
                                                                    ? '400'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'light'
                                                                    ? '300'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'extra-light'
                                                                    ? '200'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'thin'
                                                                    ? '100'
                                                                    : ''
                                                            }`,
                                                            borderRadius: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.borderRadius ===
                                                                'rounded'
                                                                    ? '100px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.borderRadius ===
                                                                      'semi-rounded'
                                                                    ? '10px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.borderRadius ===
                                                                      'no-rounding'
                                                                    ? '0px'
                                                                    : ''
                                                            }`,
                                                            background: `color-mix(in srgb, ${style?.formCustomInput?.background?.color} ${style?.formCustomInput?.background?.opacity}%, white 0%)`,
                                                            padding: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.size ===
                                                                'slim'
                                                                    ? '5px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.size ===
                                                                      'regular'
                                                                    ? '10px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.size ===
                                                                      'strong'
                                                                    ? '15px'
                                                                    : ''
                                                            }`,
                                                        } as CSSProperties
                                                    }
                                                    placeholder={
                                                        btn?.addons?.form
                                                            ?.name_placeholder
                                                            ? btn?.addons?.form
                                                                  ?.name_placeholder
                                                            : _t('Name')
                                                    }
                                                    defaultValue={
                                                        data?.name ?? ''
                                                    }
                                                    onChange={onChangeData(
                                                        'name'
                                                    )}
                                                />
                                                {errors.name && (
                                                    <span className="font-nunito m-t-5 font-12 flex flex-justify-right color-red font-w-700">
                                                        {errors.name}
                                                    </span>
                                                )}
                                            </div>
                                        </>
                                    )}
                                    {btn?.addons?.form?.email && (
                                        <>
                                            <div className="m-b-15 content-place-input">
                                                <InputText
                                                    key={resetKey}
                                                    styleTemplate="tolinkmeAddons"
                                                    // defaultValue={}
                                                    style={{
                                                        border: `${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border
                                                                ?.type ===
                                                            'solid'
                                                                ? 'solid'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'dashed'
                                                                ? 'dashed'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'dotted'
                                                                ? 'dotted'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'double'
                                                                ? 'double'
                                                                : ''
                                                        } ${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border?.size
                                                        }px ${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border?.color
                                                        }`,
                                                        borderRadius: `${
                                                            style
                                                                ?.formCustomInput
                                                                ?.borderRadius ===
                                                            'rounded'
                                                                ? '100px'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.borderRadius ===
                                                                  'semi-rounded'
                                                                ? '10px'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.borderRadius ===
                                                                  'no-rounding'
                                                                ? '0px'
                                                                : ''
                                                        }`,
                                                        boxShadow: `
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.type ===
                                                                'normal'
                                                                    ? ''
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.boxShadow
                                                                          ?.type ===
                                                                      'inset'
                                                                    ? 'inset'
                                                                    : ''
                                                            } 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.x
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.y
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.blur
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.size
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.color
                                                            }
                                                        `,
                                                    }}
                                                    styleInput={
                                                        {
                                                            ['--placeholder-colors']: `${style?.formCustomInput?.text?.color}`,
                                                            fontFamily: `${style?.formCustomInput?.text?.fontFamily}`,
                                                            lineHeight: `${style?.formCustomInput?.text?.lineHeight}px`,
                                                            fontSize: `${style?.formCustomInput?.text?.fontSize}px`,
                                                            color: `${style?.formCustomInput?.text?.color}`,
                                                            fontWeight: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.text
                                                                    ?.fontWeight ===
                                                                'black'
                                                                    ? '900'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'extra-bold'
                                                                    ? '800'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'bold'
                                                                    ? '700'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'semi-bold'
                                                                    ? '600'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'medium'
                                                                    ? '500'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'regular'
                                                                    ? '400'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'light'
                                                                    ? '300'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'extra-light'
                                                                    ? '200'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'thin'
                                                                    ? '100'
                                                                    : ''
                                                            }`,
                                                            borderRadius: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.borderRadius ===
                                                                'rounded'
                                                                    ? '100px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.borderRadius ===
                                                                      'semi-rounded'
                                                                    ? '10px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.borderRadius ===
                                                                      'no-rounding'
                                                                    ? '0px'
                                                                    : ''
                                                            }`,
                                                            background: `color-mix(in srgb, ${style?.formCustomInput?.background?.color} ${style?.formCustomInput?.background?.opacity}%, white 0%)`,
                                                            padding: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.size ===
                                                                'slim'
                                                                    ? '5px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.size ===
                                                                      'regular'
                                                                    ? '10px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.size ===
                                                                      'strong'
                                                                    ? '15px'
                                                                    : ''
                                                            }`,
                                                        } as CSSProperties
                                                    }
                                                    placeholder={
                                                        btn?.addons?.form
                                                            ?.email_placeholder
                                                            ? btn?.addons?.form
                                                                  ?.email_placeholder
                                                            : _t('Email')
                                                    }
                                                    defaultValue={data?.email}
                                                    onChange={onChangeData(
                                                        'email'
                                                    )}
                                                />
                                                {errors.email && (
                                                    <span className="font-nunito m-t-5 font-12 flex flex-justify-right color-red font-w-700">
                                                        {errors.email}
                                                    </span>
                                                )}
                                            </div>
                                        </>
                                    )}
                                    {btn?.addons?.form?.phone && (
                                        <>
                                            <div className="m-b-15 content-place-input">
                                                <InputText
                                                    key={resetKey}
                                                    type="number"
                                                    styleTemplate="tolinkmeAddons"
                                                    style={{
                                                        border: `${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border
                                                                ?.type ===
                                                            'solid'
                                                                ? 'solid'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'dashed'
                                                                ? 'dashed'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'dotted'
                                                                ? 'dotted'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'double'
                                                                ? 'double'
                                                                : ''
                                                        } ${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border?.size
                                                        }px ${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border?.color
                                                        }`,
                                                        borderRadius: `${
                                                            style
                                                                ?.formCustomInput
                                                                ?.borderRadius ===
                                                            'rounded'
                                                                ? '100px'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.borderRadius ===
                                                                  'semi-rounded'
                                                                ? '10px'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.borderRadius ===
                                                                  'no-rounding'
                                                                ? '0px'
                                                                : ''
                                                        }`,
                                                        boxShadow: `
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.type ===
                                                                'normal'
                                                                    ? ''
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.boxShadow
                                                                          ?.type ===
                                                                      'inset'
                                                                    ? 'inset'
                                                                    : ''
                                                            } 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.x
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.y
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.blur
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.size
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.color
                                                            }
                                                        `,
                                                    }}
                                                    styleInput={
                                                        {
                                                            ['--placeholder-colors']: `${style?.formCustomInput?.text?.color}`,
                                                            fontFamily: `${style?.formCustomInput?.text?.fontFamily}`,
                                                            lineHeight: `${style?.formCustomInput?.text?.lineHeight}px`,
                                                            fontSize: `${style?.formCustomInput?.text?.fontSize}px`,
                                                            color: `${style?.formCustomInput?.text?.color}`,
                                                            fontWeight: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.text
                                                                    ?.fontWeight ===
                                                                'black'
                                                                    ? '900'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'extra-bold'
                                                                    ? '800'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'bold'
                                                                    ? '700'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'semi-bold'
                                                                    ? '600'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'medium'
                                                                    ? '500'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'regular'
                                                                    ? '400'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'light'
                                                                    ? '300'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'extra-light'
                                                                    ? '200'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'thin'
                                                                    ? '100'
                                                                    : ''
                                                            }`,
                                                            borderRadius: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.borderRadius ===
                                                                'rounded'
                                                                    ? '100px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.borderRadius ===
                                                                      'semi-rounded'
                                                                    ? '10px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.borderRadius ===
                                                                      'no-rounding'
                                                                    ? '0px'
                                                                    : ''
                                                            }`,
                                                            background: `color-mix(in srgb, ${style?.formCustomInput?.background?.color} ${style?.formCustomInput?.background?.opacity}%, white 0%)`,
                                                            padding: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.size ===
                                                                'slim'
                                                                    ? '5px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.size ===
                                                                      'regular'
                                                                    ? '10px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.size ===
                                                                      'strong'
                                                                    ? '15px'
                                                                    : ''
                                                            }`,
                                                        } as CSSProperties
                                                    }
                                                    placeholder={
                                                        btn?.addons?.form
                                                            ?.phone_placeholder
                                                            ? btn?.addons?.form
                                                                  ?.phone_placeholder
                                                            : _t('Phone')
                                                    }
                                                    defaultValue={
                                                        data?.phone ?? ''
                                                    }
                                                    onChange={onChangeData(
                                                        'phone'
                                                    )}
                                                />
                                                {errors.phone && (
                                                    <span className="font-nunito m-t-5 font-12 flex flex-justify-right color-red font-w-700">
                                                        {errors.phone}
                                                    </span>
                                                )}
                                            </div>
                                        </>
                                    )}
                                    {btn?.addons?.form?.message && (
                                        <>
                                            <div className="m-b-15 content-place-input">
                                                <InputText
                                                    key={resetKey}
                                                    styleTemplate="tolinkmeAddons"
                                                    style={{
                                                        border: `${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border
                                                                ?.type ===
                                                            'solid'
                                                                ? 'solid'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'dashed'
                                                                ? 'dashed'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'dotted'
                                                                ? 'dotted'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.border
                                                                      ?.type ===
                                                                  'double'
                                                                ? 'double'
                                                                : ''
                                                        } ${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border?.size
                                                        }px ${
                                                            style
                                                                ?.formCustomInput
                                                                ?.border?.color
                                                        }`,
                                                        borderRadius: `${
                                                            style
                                                                ?.formCustomInput
                                                                ?.borderRadius ===
                                                            'rounded'
                                                                ? '100px'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.borderRadius ===
                                                                  'semi-rounded'
                                                                ? '10px'
                                                                : style
                                                                      ?.formCustomInput
                                                                      ?.borderRadius ===
                                                                  'no-rounding'
                                                                ? '0px'
                                                                : ''
                                                        }`,
                                                        boxShadow: `
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.type ===
                                                                'normal'
                                                                    ? ''
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.boxShadow
                                                                          ?.type ===
                                                                      'inset'
                                                                    ? 'inset'
                                                                    : ''
                                                            } 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.x
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.y
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.blur
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.size
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.color
                                                            }
                                                        `,
                                                    }}
                                                    contentFlexAddon="flex"
                                                    styleInput={
                                                        {
                                                            ['--placeholder-colors']: `${style?.formCustomInput?.text?.color}`,

                                                            fontFamily: `${style?.formCustomInput?.text?.fontFamily}`,
                                                            fontSize: `${style?.formCustomInput?.text?.fontSize}px`,
                                                            color: `${style?.formCustomInput?.text?.color}`,
                                                            borderRadius: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.borderRadius ===
                                                                'rounded'
                                                                    ? '100px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.borderRadius ===
                                                                      'semi-rounded'
                                                                    ? '10px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.borderRadius ===
                                                                      'no-rounding'
                                                                    ? '0px'
                                                                    : ''
                                                            }`,
                                                            background: `color-mix(in srgb, ${style?.formCustomInput?.background?.color} ${style?.formCustomInput?.background?.opacity}%, white 0%)`,
                                                            fontWeight: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.text
                                                                    ?.fontWeight ===
                                                                'black'
                                                                    ? '900'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'extra-bold'
                                                                    ? '800'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'bold'
                                                                    ? '700'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'semi-bold'
                                                                    ? '600'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'medium'
                                                                    ? '500'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'regular'
                                                                    ? '400'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'light'
                                                                    ? '300'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'extra-light'
                                                                    ? '200'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.text
                                                                          ?.fontWeight ===
                                                                      'thin'
                                                                    ? '100'
                                                                    : ''
                                                            }`,
                                                            padding: `${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.size ===
                                                                'slim'
                                                                    ? '5px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.size ===
                                                                      'regular'
                                                                    ? '10px'
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.size ===
                                                                      'strong'
                                                                    ? '15px'
                                                                    : ''
                                                            }`,
                                                        } as CSSProperties
                                                    }
                                                    type="textarea"
                                                    defaultValue={
                                                        data?.message ?? ''
                                                    }
                                                    onChange={onChangeData(
                                                        'message'
                                                    )}
                                                    placeholder={
                                                        btn?.addons?.form
                                                            ?.message_placeholder
                                                            ? btn?.addons?.form
                                                                  ?.message_placeholder
                                                            : _t('Mensaje')
                                                    }
                                                />
                                                {errors.message && (
                                                    <span className="font-nunito m-t-5 font-12 flex flex-justify-right color-red font-w-700">
                                                        {errors.message}
                                                    </span>
                                                )}
                                            </div>
                                        </>
                                    )}
                                    {(btn?.addons?.form?.name ||
                                        btn?.addons?.form?.email ||
                                        btn?.addons?.form?.phone ||
                                        btn?.addons?.form?.message) && (
                                        <>
                                            <Button
                                                loader={loader}
                                                onClick={submit}
                                                styleTemplate={
                                                    'tolinkmeButtonForm'
                                                }
                                                styleButton={{
                                                    background: `color-mix(in srgb, ${style?.formCustomInput?.backgroundBtn?.color} ${style?.formCustomInput?.background?.opacity}%, white 0%)`,
                                                    borderRadius: `${
                                                        style?.formCustomInput
                                                            ?.borderRadius ===
                                                        'rounded'
                                                            ? '100px'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.borderRadius ===
                                                              'semi-rounded'
                                                            ? '10px'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.borderRadius ===
                                                              'no-rounding'
                                                            ? '0px'
                                                            : ''
                                                    }`,
                                                    border: `${
                                                        style?.formCustomInput
                                                            ?.border?.type ===
                                                        'solid'
                                                            ? 'solid'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.border
                                                                  ?.type ===
                                                              'dashed'
                                                            ? 'dashed'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.border
                                                                  ?.type ===
                                                              'dotted'
                                                            ? 'dotted'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.border
                                                                  ?.type ===
                                                              'double'
                                                            ? 'double'
                                                            : ''
                                                    } ${
                                                        style?.formCustomInput
                                                            ?.border?.size
                                                    }px ${
                                                        style?.formCustomInput
                                                            ?.border?.color
                                                    }`,
                                                    fontFamily: `${style?.formCustomInput?.text?.fontFamily}`,
                                                    fontSize: `${style?.formCustomInput?.text?.fontSize}px`,
                                                    color: `${style?.formCustomInput?.textBtn?.color}`,
                                                    fontWeight: `${
                                                        style?.formCustomInput
                                                            ?.text
                                                            ?.fontWeight ===
                                                        'black'
                                                            ? '900'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.text
                                                                  ?.fontWeight ===
                                                              'extra-bold'
                                                            ? '800'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.text
                                                                  ?.fontWeight ===
                                                              'bold'
                                                            ? '700'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.text
                                                                  ?.fontWeight ===
                                                              'semi-bold'
                                                            ? '600'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.text
                                                                  ?.fontWeight ===
                                                              'medium'
                                                            ? '500'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.text
                                                                  ?.fontWeight ===
                                                              'regular'
                                                            ? '400'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.text
                                                                  ?.fontWeight ===
                                                              'light'
                                                            ? '300'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.text
                                                                  ?.fontWeight ===
                                                              'extra-light'
                                                            ? '200'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.text
                                                                  ?.fontWeight ===
                                                              'thin'
                                                            ? '100'
                                                            : ''
                                                    }`,
                                                    padding: `${
                                                        style?.formCustomInput
                                                            ?.size === 'slim'
                                                            ? '5px'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.size ===
                                                              'regular'
                                                            ? '10px'
                                                            : style
                                                                  ?.formCustomInput
                                                                  ?.size ===
                                                              'strong'
                                                            ? '15px'
                                                            : ''
                                                    }`,
                                                    boxShadow: `
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.type ===
                                                                'normal'
                                                                    ? ''
                                                                    : style
                                                                          ?.formCustomInput
                                                                          ?.boxShadow
                                                                          ?.type ===
                                                                      'inset'
                                                                    ? 'inset'
                                                                    : ''
                                                            } 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.x
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.y
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.blur
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.size
                                                            }px 
                                                            ${
                                                                style
                                                                    ?.formCustomInput
                                                                    ?.boxShadow
                                                                    ?.color
                                                            }
                                                        `,

                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                }}
                                            >
                                                {!loader && (
                                                    <>
                                                        {btn?.addons?.form
                                                            ?.button_placeholder ===
                                                            undefined ||
                                                        btn?.addons?.form
                                                            ?.button_placeholder ===
                                                            ''
                                                            ? _t('Enviar')
                                                            : btn.addons.form
                                                                  .button_placeholder}
                                                    </>
                                                )}
                                            </Button>

                                            <Space size={15} />
                                            <ReCAPTCHA
                                                size="invisible"
                                                sitekey={reca ?? ''}
                                            />
                                        </>
                                    )}
                                </>
                            )}
                        </React.Fragment>
                    );
                } else {
                    return (
                        <Toltip
                            key={i}
                            tooltipMessage={
                                btn?.monetizeData?.description ?? ''
                            }
                            classNameContentToltip={`${
                                btn?.monetizeData?.description ??
                                btn?.monetizeData
                                    ? ''
                                    : 'hiddenToltip'
                            }`}
                        >
                            <a
                                href={
                                    btn.monetizeData
                                        ? getUrlLoginPayment(btn.uuid ?? '')
                                        : btn.ButtonPriceDetails?.period ===
                                              'JUST_ONE' &&
                                          btn.ButtonPriceDetails?.type ===
                                              'GIFT'
                                        ? `/pay/${btn.ButtonPriceDetails?.buttonUuid}`
                                        : btn.url
                                }
                                target={'_blank'}
                                className={`
                            
                                    pos-r
                                    overflow-hidden
                                    d-block m-b-15
                                    fa-${style.btnAnimation?.['on-page-load']}
                                     ${
                                         BorderRadiusButtonValues[
                                             style?.btn?.borderRadius ??
                                                 'semi-rounded'
                                         ]
                                     }
                        `}
                                onClick={() => {
                                    onClickBtn?.(btn);
                                }}
                                rel="noreferrer"
                                style={{
                                    animationIterationCount: 1,

                                    boxShadow: `
                                ${
                                    style?.btn?.boxShadow?.type === 'normal'
                                        ? ''
                                        : style?.formCustomInput?.boxShadow
                                              ?.type === 'inset'
                                        ? 'inset'
                                        : ''
                                } 
                                ${style?.btn?.boxShadow?.x}px 
                                ${style?.btn?.boxShadow?.y}px 
                                ${style?.btn?.boxShadow?.blur}px 
                                ${style?.btn?.boxShadow?.size}px 
                                ${style?.btn?.boxShadow?.color}
                            `,
                                }}
                            >
                                {btn.monetizeData ? (
                                    <div
                                        className={`width-p-100 pos-a top-0 left-0 z-index-10 d-block 
                                                    animation-iteration-count-1 animation-duration-10
                                                    bg-argent-2 height-p-100
                                                    fa-${style.btnAnimation?.infinite}
                                                    fa-${style.btnAnimation?.hover}-hover-
                                
                                `}
                                    >
                                        <Layer
                                            title={`${btn.rs ?? ''}`}
                                            price={
                                                btn?.monetizeData?.amount
                                                    ? parseFloat(
                                                          btn.monetizeData
                                                              .amount
                                                      )
                                                    : undefined
                                            }
                                            subscribe={`${
                                                btn.monetizeData?.recurrent ==
                                                'Flat_Rate'
                                                    ? _t('Regalo')
                                                    : btn.monetizeData
                                                          ?.recurrent
                                            }`}
                                            period={`${
                                                btn?.monetizeData?.period
                                                    ? btn?.monetizeData?.period
                                                    : 'Just once'
                                            }`}
                                        />
                                    </div>
                                ) : (
                                    <>
                                        {btn.ButtonPriceDetails?.period ===
                                            'JUST_ONE' &&
                                            btn.ButtonPriceDetails?.type ===
                                                'GIFT' && (
                                                <div
                                                    className={`width-p-100 pos-a top-0 left-0 z-index-10 d-block 
                                                        animation-iteration-count-1 animation-duration-10
                                                        bg-argent-2 height-p-100
                                                        fa-${style.btnAnimation?.infinite}
                                                        fa-${style.btnAnimation?.hover}-hover-
        `}
                                                >
                                                    <Layer2
                                                        title={`${
                                                            btn.rs ?? ''
                                                        }`}
                                                        price={
                                                            btn
                                                                ?.ButtonPriceDetails
                                                                ?.basePrice
                                                        }
                                                        subscribe={`${
                                                            btn
                                                                .ButtonPriceDetails
                                                                ?.recurrence ==
                                                            'FLAT_RATE'
                                                                ? _t('Regalo')
                                                                : btn
                                                                      .ButtonPriceDetails
                                                                      ?.recurrence
                                                        }`}
                                                        period={`${
                                                            btn
                                                                ?.ButtonPriceDetails
                                                                ?.period == ''
                                                                ? 'Just once'
                                                                : btn
                                                                      ?.ButtonPriceDetails
                                                                      ?.period
                                                        }`}
                                                    />
                                                </div>
                                            )}
                                    </>
                                )}

                                <div
                                    className={`fa-${style.btnAnimation?.hover}-hover `}
                                >
                                    {getBtn(btn, {
                                        ...(style?.btn ?? {}),
                                        className: `
                                fa-${style.btnAnimation?.infinite}
                                fa-${style.btnAnimation?.hover}-hover-
                                
                            `,
                                    })}
                                </div>
                            </a>
                        </Toltip>
                    );
                }
            });
    }, [
        links,
        style.btn,
        style.btnAnimation,
        user,
        load,
        _t,
        loader,
        style.formCustomInput,
        data,
    ]);

    return (
        <>
            <div
                className={`
                
                pos-f
                inset-0
                width-p-100
                height-p-100
                overflow-auto
                bg-gradient-darkAqua-brightPink
                p-v-25
                p-h-15
            `}
            >
                {Bg}
                <div className="pos-r">
                    <ContentWidth
                        size={500}
                        className="m-h-auto pos-r text-center"
                    >
                        {ImageP}
                        {Info}
                    </ContentWidth>
                    {Buttons.length !== 0 && (
                        <>
                            <Space size={20} />
                        </>
                    )}

                    <ContentWidth size={500} className="m-h-auto pos-r ">
                        {Buttons}
                    </ContentWidth>
                    <Space size={80} />
                </div>
            </div>
        </>
    );
};
export default ProfilePublic;
