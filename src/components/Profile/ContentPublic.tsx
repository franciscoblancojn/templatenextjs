import Button, { ButtonStyles } from '@/components/Button';
import ContentWidth from '@/components/ContentWidth';
import Image from '@/components/Image';
import Space from '@/components/Space';
import Text, { TextStyles } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

export interface ContentPublicBaseProps {
    onPublish?: () => void;
    loader?: boolean;
}

export interface ContentPublicClassProps {
    classNameContent?: string;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateBtnPublic?: ButtonStyles | ThemesType;
    stepLine?: string;
    contentNumber?: string;
    activeStep?: string;
    inactiveStep?: string;
    stepss?: string;
    hiddenStep?: string;
    classNameContentSteps?: string;
    classNameContentTitle?: string;
    classNameContentButton?: string;
    classNameContentTitleText?: string;
    classNameButtonBack?: string;
    classNameshowStepsButton?: string;
    classNameshowStepsTitle?: string;
    classNameTextStatus?: string;
    classNameButtonBackIcon?: string;
}

export interface ContentPublicProps
    extends ContentPublicClassProps,
        ContentPublicBaseProps {}

export const ContentPublic = ({
    classNameContent = '',

    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateBtnPublic = Theme.styleTemplate ?? '_default',

    loader = false,
    onPublish = () => {},
}: ContentPublicProps) => {
    const _t = useLang();
    return (
        <>
            <div className={classNameContent}>
                <ContentWidth size={212}>
                    <Text styleTemplate={styleTemplateText}>
                        {_t('Before Publish your site, take a final look')}
                    </Text>
                    <Space size={21} />
                    <Image src="face4.png" />
                    <Space size={18} />
                    <ContentWidth
                        size={200}
                        className={`
                        
                            pos-f
                            bottom-20
                            z-index-2
                            width-p-100
                            p-h-17
                            left-0
                            right-0
                            m-auto
                            flex
                            flex-justify-center
    
                    `}
                    >
                        <Button
                            onClick={onPublish}
                            loader={loader}
                            styleTemplate={styleTemplateBtnPublic}
                        >
                            {_t('Publish')}
                        </Button>
                    </ContentWidth>
                </ContentWidth>
            </div>
        </>
    );
};
export default ContentPublic;
