import { InfoProfileEdit } from '@/components/InfoProfileEdit';
import { InfoProfileEditBaseProps } from '@/components/InfoProfileEdit/Base';

export type ContentStylesBaseProps = InfoProfileEditBaseProps;

export interface ContentStylesClassProps {
    classNameContent?: string;
}

export interface ContentStylesProps
    extends ContentStylesClassProps,
        ContentStylesBaseProps {}

export const ContentStyles = ({
    classNameContent = '',
    ...props
}: ContentStylesProps) => {
    return (
        <>
            <div className={classNameContent}>
                <InfoProfileEdit {...props} />
            </div>
        </>
    );
};
export default ContentStyles;
