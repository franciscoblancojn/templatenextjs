import { ProfileBase, ProfileBaseProps } from '@/components/Profile/Base';
import * as styles from '@/components/Profile/styles';

import { Theme, ThemesType } from '@/config/theme';
import { useMemo } from 'react';

export const ProfileStyle = { ...styles } as const;

export type ProfileStyles = keyof typeof ProfileStyle;

export interface ProfileProps extends ProfileBaseProps {
    styleTemplate?: ProfileStyles | ThemesType;
}

export const Profile = ({
    styleTemplate = Theme.styleTemplate ?? '_default',

    ...props
}: ProfileProps) => {
    const Style = useMemo(
        () =>
            ProfileStyle[styleTemplate as ProfileStyles] ??
            ProfileStyle._default,
        [styleTemplate]
    );
    return (
        <>
            <ProfileBase {...props} {...Style} />
        </>
    );
};
export default Profile;
