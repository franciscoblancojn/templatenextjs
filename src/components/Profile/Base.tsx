import ContentLinks, {
    ContentLinksClassProps,
} from '@/components/Profile/ContentLinks';

import ContentStyles, {
    ContentStylesClassProps,
} from '@/components/Profile/ContentStyles';
import { ContentPublicClassProps } from '@/components/Profile/ContentPublic';
import { SubmitResult } from '@/components/Form/Base';

import Link from '../Link';
import url from '@/data/routes';
import { ProfileData } from '@/interfaces/ProfileData';
import { useRouter } from 'next/router';
import { useNotification } from '@/hook/useNotification';
import { useLang } from '@/lang/translate';
import { useUser } from '@/hook/useUser';
import { useMemo, useState } from 'react';
import Notification, { NotificationStyles } from '../Notification';
import Theme, { ThemesType } from '@/config/theme';
import Modal, { ModalStyles } from '../Modal';
import ProfilePublic from './Public';
import ContentWidth from '../ContentWidth';
import Button, { ButtonStyles } from '../Button';
import Close from '@/svg/close';
import Space from '../Space';
import ShareProfile from '../ShareProfile';
import Eye from '@/svg/eye';
import ContentMonetize from './ContentMonetize';
import Tab from '../Tab';
import ContentAnalitycs, {
    ContentAnalitycsClassProps,
} from './ContentAnalitycs';
import ContentMessages from './ContentMessages';
import { Addons } from '@/interfaces/Addons';
import { MessagesData, MessageUserData } from '@/interfaces/message';

export interface ProfileDataProps extends ProfileData {}

export interface ProfileBaseProps extends ProfileDataProps {
    onSubmit?: (data: ProfileDataProps) => Promise<SubmitResult>;
    name: string;
}

export interface ProfileClassProps {
    classNameContent?: string;
    classNameContentBtn?: string;
    classNameContentBtnConatiner?: string;
    classNameContentBtnPreview?: string;
    classNameBtnButton?: string;
    styleTemplateBtn?: ButtonStyles | ThemesType;
    styleTemplateBtnPublish?: ButtonStyles | ThemesType;
    classNameLinks?: ContentLinksClassProps;
    classNameStyles?: ContentStylesClassProps;
    classNameAnalitycs?: ContentAnalitycsClassProps;
    //classNamePublic?: ContentPublicClassProps;
    classNameMonetize?: ContentPublicClassProps;
    classNameContentModal?: string;
    styleTemplatePreviewModal?: ModalStyles | ThemesType;

    styleTemplateNotification?: NotificationStyles | ThemesType;
    messages?: MessageUserData;
}

export interface ProfileProps extends ProfileClassProps, ProfileBaseProps {}

export const ProfileBase = ({
    classNameContent = '',
    classNameContentBtn = '',
    classNameContentBtnConatiner = '',
    classNameContentBtnPreview = '',
    classNameBtnButton = '',
    styleTemplateBtn = Theme.styleTemplate ?? '_default',
    styleTemplateBtnPublish = Theme.styleTemplate ?? '_default',
    classNameLinks = {},
    classNameStyles = {},
    classNameAnalitycs = {},
    classNameMonetize = {},
    //classNamePublic = {},
    classNameContentModal = '',
    styleTemplatePreviewModal = Theme.styleTemplate ?? '_default',
    styleTemplateNotification = Theme.styleTemplate ?? '_default',
    uuid,
    linksDefault,
    styleDefault,
    name,
    isAprobed,
    monetize,
    addons,
    messages,
    ...props
}: ProfileProps) => {
    const responseData = messages;
    const mapMessageData = (message: any): MessagesData => ({
        status: message?.status,
        uuid: message?.uuid,
        updatedAt: message?.updatedAt,
        createdAt: message?.createdAt,
        Messages: [
            {
                uuid: message?.Messages?.[0]?.uuid,
                text: message?.Messages?.[0]?.text,
                roomCaseUuid: message?.Messages?.[0]?.roomCaseUuid,
                email: message?.Messages?.[0]?.email,
                phone: message?.Messages?.[0]?.phone,
                name: message?.Messages?.[0]?.name,
                createdAt: message?.Messages?.[0]?.createdAt,
                updatedAt: message?.Messages?.[0]?.updatedAt,
            },
        ],
    });

    const mappedMessages: MessagesData[] = useMemo(() => {
        return (responseData?.Messages || []).map(mapMessageData);
    }, [responseData]);

    const [messagesData] = useState<MessageUserData>({
        Messages: mappedMessages,
        count: responseData?.count,
        totalCount: responseData?.totalCount,
    });

    const router = useRouter();
    const { pop } = useNotification();
    const _t = useLang();
    const { user, updateUser } = useUser();
    const [loader, setLoader] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [links, setLinks] = useState(linksDefault);
    const [styles, setStyles] = useState(styleDefault);
    const [addonsForm, setAddonsForm] = useState<{ addons?: Addons[] }>({
        addons,
    });

    const addonsProp = useMemo(() => addonsForm?.addons || {}, [addonsForm]);

    const onSubmit = async () => {
        setLoader?.(true);
        try {
            const result = await props?.onSubmit?.({
                uuid,
                linksDefault: links,
                styleDefault: styles,
                isAprobed,
                addons: addonsProp,
            });

            pop({
                type: result?.status,
                message: _t(result?.message ?? ''),
            });

            if (result?.status == 'ok') {
                const w: any = window;
                if (w?.dataLayer?.push) {
                    w.dataLayer?.push?.({
                        event: `form-profile-published`,
                    });
                }

                updateUser({
                    ...user,
                    name: styles.name,
                    img: styles.avatar,
                });

                window.open('/' + styles.name, '_blank');

                if (result.status == 'ok') {
                    router.reload();
                }
            }
        } catch (error: any) {
            pop({
                type: 'error',
                message: _t(error?.message ?? error ?? ''),
            });
        }

        setLoader?.(false);
    };

    return (
        <>
            {!isAprobed ? (
                <Notification
                    type="warning"
                    styleTemplate={styleTemplateNotification}
                >
                    {_t('No has verficido tu cuenta,')}{' '}
                    <Link
                        href={url.confirmPhone.index}
                        styleTemplate="noStyleYesUnderlineDarkAqua"
                    >
                        {_t('has click aqui para verificar')}
                    </Link>
                </Notification>
            ) : (
                <></>
            )}

            <Modal
                styleTemplate={styleTemplatePreviewModal}
                onActive={showModal}
                onClose={() => {
                    setShowModal(false);
                }}
                showClose={false}
            >
                <div className={classNameContentModal}>
                    {
                        <ProfilePublic
                            addons={[addonsProp]}
                            links={links}
                            style={styles}
                        />
                    }

                    <div className={classNameContentBtn}>
                        <div className={classNameContentBtnConatiner}>
                            <ContentWidth size={200} className={''}>
                                <Button
                                    onClick={() => {
                                        setShowModal(false);
                                    }}
                                    classNameBtn={'color-brightPink-hover'}
                                    styleTemplate={styleTemplateBtn}
                                >
                                    <Close size={15} />
                                    {_t('Close')}
                                </Button>
                            </ContentWidth>
                            <ContentWidth size={200} className={''}>
                                <Button
                                    onClick={onSubmit}
                                    classNameBtn={'color-brightPink-hover'}
                                    styleTemplate={styleTemplateBtnPublish}
                                    loader={loader}
                                >
                                    {_t('Publish')}
                                </Button>
                            </ContentWidth>
                        </div>
                    </div>
                    <Space size={100} />
                </div>
            </Modal>

            <div className={`ContentTab ${classNameContent}`}>
                <ShareProfile name={name} />
                <ContentWidth
                    size={400}
                    className={`BtnPublish ${classNameContentBtnPreview}`}
                >
                    <Button
                        onClick={() => {
                            setShowModal(true);
                        }}
                        styleTemplate={styleTemplateBtn}
                        classNameBtn={classNameBtnButton}
                    >
                        <Eye size={23} />
                        {_t('Preview / Publish')}
                    </Button>
                </ContentWidth>

                <Tab
                    defaultSelect={0}
                    items={[
                        {
                            tab: _t('Links'),
                            content: (
                                <ContentLinks
                                    links={links}
                                    onChange={setLinks}
                                    useMonetize={
                                        monetize?.statusSteps?.step1 ==
                                            'Verified' &&
                                        monetize.statusSteps.step2 ==
                                            'Verified' &&
                                        monetize.statusSteps.step3 == 'Verified'
                                    }
                                    {...classNameLinks}
                                    onChangeAddons={(addons) => {
                                        setAddonsForm({
                                            addons,
                                        });
                                    }}
                                    addons={addonsProp}
                                />
                            ),
                        },
                        {
                            tab: _t('Style'),
                            content: (
                                <ContentStyles
                                    onChange={setStyles}
                                    defaultValue={styles}
                                    {...classNameStyles}
                                    links={links}
                                />
                            ),
                        },

                        {
                            tab: _t('Analitycs'),
                            content: (
                                <ContentAnalitycs
                                    {...classNameAnalitycs}
                                    profile_uuid={props.profile_uuid ?? ''}
                                    user_uuid={props?.user_uuid ?? ''}
                                />
                            ),
                        },
                        {
                            tab: _t('Monetize'),
                            content: (
                                <div className="TabMonetize">
                                    <ContentMonetize
                                        {...classNameMonetize}
                                        {...monetize}
                                    />
                                </div>
                            ),
                        },
                        {
                            tab: (
                                <>
                                    {messagesData?.totalCount == 0 ? (
                                        <></>
                                    ) : (
                                        <>
                                            <div>
                                                {' '}
                                                <span
                                                    className="
                                bg-white
                                p-v-2
                                p-h-8
                                border-radius-99
                                box-shadow 
                                box-shadow-x-0 
                                box-shadow-y-3 
                                box-shadow-b-2 
                                box-shadow-s-0 
                                box-shadow-black16
                            "
                                                >
                                                    {messagesData?.count}
                                                </span>{' '}
                                                {messagesData.count == 1
                                                    ? _t(`My message`)
                                                    : _t(`My messages`)}{' '}
                                            </div>
                                        </>
                                    )}
                                </>
                            ),
                            content: (
                                <div className="">
                                    <ContentMessages
                                        messagesData={
                                            messagesData?.totalCount == 0
                                                ? []
                                                : messagesData?.Messages
                                        }
                                    />
                                </div>
                            ),
                        },
                    ]}
                />
            </div>
        </>
    );
};
export default ProfileBase;
