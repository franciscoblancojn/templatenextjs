import { Story, Meta } from "@storybook/react";

import { ProfileProps, Profile } from "./index";

export default {
    title: "Page/Profile",
    component: Profile,
} as Meta;

const Template: Story<ProfileProps> = (args) => (
    <Profile {...args}>Test Children</Profile>
);

export const Index = Template.bind({});
Index.args = {
    linksDefault:[
        {
            rs:"bongacams",
            active:true,
            url:"test"
        },
        {
            rs:"camsoda",
            active:true,
            url:"test"
        },
        {
            rs:"facebook",
            active:true,
            url:"test"
        },
        {
            rs:"whatsapp",
            active:true,
            url:"test"
        },
        {
            rs:"onlyfans",
            active:true,
            url:"test"
        },
        {
            rs:"telegram",
            active:true,
            url:"test"
        },
        {
            rs:"tiktok",
            active:true,
            url:"test"
        },
    ],
    styleDefault:{
        name:"test",
        description:"test",
        web:"test",
        avatar:"https://www.aerocivil.gov.co/Style%20Library/CEA/img/01.jpg",
        bgDesktop:"https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg",
        bgMovil:"https://www.aerocivil.gov.co/Style%20Library/CEA/img/03.jpg",
        fontColor:"#0f0"
    },
   /*
   
    analitycs:{
        views:135850,
        totalClicks:83014, 
        totalButtons:4,
        porcentClicks:61,
        listClicksButtons: [
            {
                id:"bongacams",
                count:66796
            },
            {
                id:"onlyfans",
                count:13647
            },
            {
                id:"tiktok",
                count:1476
            },
            {
                id:"instagram",
                count:1095
            },

        ],
        countryVisitors:[
            {
                country:"Colombia",
                count:1212
            },
            {
                country:"Mexico",
                count:12
            },
            {
                country:"United States",
                count:122
            },
            {
                country:"Argentina",
                count:122
            },
            {
                country:"Ecuador",
                count:34
            },
        ],
        countryClicks:[
            {
                country:"Colombia",
                count:122
            },
            {
                country:"Mexico",
                count:123
            },
            {
                country:"United States",
                count:122
            },
            {
                country:"Argentina",
                count:1223
            },
            {
                country:"Ecuador",
                count:312
            },
        ],

   
    },
   */

};
