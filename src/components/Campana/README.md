# Balance

## Dependencies

[ContentWidth](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ContentWidth)

```js
import { ContentWidth } from '@/components/ContentWidth';
```

## Import

```js
import { Balance, BalanceStyles } from '@/components/Balance';
```

## Props

```tsx
interface BalanceProps {}
```

## Use

```js
<Balance>Balance</Balance>
```
