import Bell from '@/svg/Bell';
import { PropsWithChildren } from 'react';
import Popup, { PopupStyles } from '../Popup';
import Text from '../Text';
import { useRouter } from 'next/router';
import { ImgProfile, ImgProfileProps } from '@/components/User/ImgProfile';
import Button from '../Button';
import Check from '@/svg/check';
import Theme, { ThemesType } from '@/config/theme';
import { useNotificationSocket } from '@/hook/useNotificationSocket';
import Trash from '@/svg/trash';
import { useLang } from '@/lang/translate';

export interface CampanaClassProps {
    classNameIcon?: string;
    ImgProfileProps?: ImgProfileProps;
    classNameContentImgProfile?: string;
    classNameContentMsg?: string;
    classNameContentNotification?: string;
    classNameContentNotificationVoid?: string;
    classNameContentDate?: string;
    classNameContentBell?: string;
    styleTemplatePopup?: PopupStyles | ThemesType;
    skip?: number;
}

export interface CampanaBaseProps extends PropsWithChildren {
    text?: string;
}

export interface CampanaProps extends CampanaClassProps, CampanaBaseProps {}

export const CampanaBase = ({
    classNameContentImgProfile = '',
    classNameContentMsg = '',
    classNameContentNotification = '',
    classNameContentNotificationVoid = '',
    classNameContentDate = '',
    classNameContentBell = '',
    styleTemplatePopup = Theme.styleTemplate ?? '_default',
    ImgProfileProps = {},
    skip = 5,
}: CampanaProps) => {
    const _t = useLang();
    const router = useRouter();

    const { msgs, handleScroll } = useNotificationSocket(skip);

    if (!router.isReady) {
        return <></>;
    }
    const formatDate = (dateString: any) => {
        const date = new Date(dateString);
        return date.toLocaleDateString(undefined);
    };
    const updatedMsgs = msgs.map((msg) => {
        if (msg.EVENT === 'MONETIZE_AUTHROTIZATION_REQUEST') {
            return {
                ...msg,
                EVENT: 'Haz solicitado monetización',
            };
        } else if (msg.EVENT === 'MONETIZE_AUTHROTIZATION_APROBE') {
            return {
                ...msg,
                EVENT: 'Monetizacion Aprobada',
            };
        } else if (msg.EVENT === 'NEW_SUBSCRIPTOR_CUSTOMER') {
            return {
                ...msg,
                EVENT: 'Nuevo Cliente Suscrito',
            };
        } else {
            return msg;
        }
    });
    const hasMessages = updatedMsgs.length > 0;

    return (
        <>
            <Popup
                styleTemplate={styleTemplatePopup}
                btn={
                    <>
                        <div className={classNameContentBell}>
                            <Bell size={22} />
                        </div>
                    </>
                }
                btnSwActive={false}
            >
                <div className="notification-container" onScroll={handleScroll}>
                    {hasMessages ? (
                        <>
                            {updatedMsgs.map((msg, i) => (
                                <div
                                    className={classNameContentNotification}
                                    key={i}
                                >
                                    <div className={classNameContentImgProfile}>
                                        <ImgProfile {...ImgProfileProps} />
                                        <div className={classNameContentMsg}>
                                            <Text
                                                className="text-capitalize"
                                                styleTemplate="tolinkme20"
                                            >
                                                {msg.user?.username}
                                            </Text>
                                            <Text styleTemplate="tolinkme13">
                                                {msg.EVENT}
                                            </Text>
                                        </div>
                                    </div>
                                    <div className={classNameContentDate}>
                                        <Text
                                            className=""
                                            styleTemplate="tolinkme10"
                                        >
                                            {formatDate(msg.createdAt)}
                                        </Text>
                                        <div className="flex flex-justify-right">
                                            <Button
                                                classNameBtn="flex flex-align-center m-r-10"
                                                styleTemplate="tolinkmeSave"
                                            >
                                                <span className="flex p-r-3 color-black color-green-hover">
                                                    <Check size={12} />
                                                </span>
                                            </Button>
                                            <Button
                                                classNameBtn="flex flex-align-center "
                                                styleTemplate="tolinkmeDelete_2"
                                            >
                                                <span className="flex p-r-3 color-black color-error-hover">
                                                    <Trash size={12} />
                                                </span>
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </>
                    ) : (
                        <>
                            <div className={classNameContentNotificationVoid}>
                                <Text styleTemplate="tolinkme27">
                                    {_t('No notifications available')}
                                </Text>
                            </div>
                        </>
                    )}
                </div>
            </Popup>
        </>
    );
};

export default CampanaBase;
