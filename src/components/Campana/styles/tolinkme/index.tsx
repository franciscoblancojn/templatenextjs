import { CampanaClassProps } from '@/components/Campana/Base';

export const tolinkme: CampanaClassProps = {
    classNameContentImgProfile: `
       flex
       gap-10
   `,
    classNameContentMsg: `
       
   `,
    classNameContentNotification: `
        flex
        flex-justify-between
        width-p-100
        left-0 
        p-15
        bg-white
        bg-grayAcd-hover
        border-style-solid
        border-0
        border-b-1
        border-whiteTwo
   `,
    classNameContentDate: `
        
        text-right 
    `,
    classNameContentBell: `
        color color-white
        color-brightPink-hover 
        flex 
    `,
    styleTemplatePopup: 'tolinkme4',
    classNameContentNotificationVoid: `
        m-auto
        flex
        flex-justify-center
        
    `,
};
