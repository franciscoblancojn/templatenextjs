import { Story, Meta } from "@storybook/react";

import { CampanaProps, Campana } from "./index";

export default {
    title: "Campana/Campana",
    component: Campana,
} as Meta;

const CampanaIndex: Story<CampanaProps> = (args) => (
    <Campana {...args}>Test Children</Campana>
);

export const Index = CampanaIndex.bind({});
Index.args = {
    text: "Current Campana",
};
