import { useMemo } from 'react';

import * as styles from '@/components/Campana/styles';

import { Theme, ThemesType } from '@/config/theme';

import { CampanaBaseProps, CampanaBase } from '@/components/Campana/Base';

export const CampanaStyle = { ...styles } as const;

export type CampanaStyles = keyof typeof CampanaStyle;

export interface CampanaProps extends CampanaBaseProps {
    styleTemplate?: CampanaStyles | ThemesType;
}

export const Campana = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: CampanaProps) => {
    const Style = useMemo(
        () =>
            CampanaStyle[styleTemplate as CampanaStyles] ??
            CampanaStyle._default,
        [styleTemplate]
    );

    return <CampanaBase {...Style} {...props} />;
};
export default Campana;
