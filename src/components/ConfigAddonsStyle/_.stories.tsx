import { Story, Meta } from "@storybook/react";

import { ConfigAddonsStyleProps, ConfigAddonsStyle } from "./index";

export default {
    title: "ConfigAddonsStyle/ConfigAddonsStyle",
    component: ConfigAddonsStyle,
} as Meta;

const ConfigAddonsStyleIndex: Story<ConfigAddonsStyleProps> = (args) => (
    <ConfigAddonsStyle {...args}>Test Children</ConfigAddonsStyle>
);

export const Index = ConfigAddonsStyleIndex.bind({});
Index.args = {};
