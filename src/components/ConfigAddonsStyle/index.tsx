import { useMemo } from 'react';

import * as styles from '@/components/ConfigAddonsStyle/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigAddonsStyleBaseProps,
    ConfigAddonsStyleBase,
} from '@/components/ConfigAddonsStyle/Base';

export const ConfigAddonsStyleStyle = { ...styles } as const;

export type ConfigAddonsStyleStyles = keyof typeof ConfigAddonsStyleStyle;

export interface ConfigAddonsStyleProps extends ConfigAddonsStyleBaseProps {
    styleTemplate?: ConfigAddonsStyleStyles | ThemesType;
}

export const ConfigAddonsStyle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigAddonsStyleProps) => {
    const Style = useMemo(
        () =>
            ConfigAddonsStyleStyle[styleTemplate as ConfigAddonsStyleStyles] ??
            ConfigAddonsStyleStyle._default,
        [styleTemplate]
    );

    return <ConfigAddonsStyleBase {...Style} {...props} />;
};
export default ConfigAddonsStyle;
