import { ConfigAddonsStyleClassProps } from '@/components/ConfigAddonsStyle/Base';

export const tolinkme: ConfigAddonsStyleClassProps = {
    styleTemplatePopup: 'tolinkme2',
    styleTemplateTitlePopup: 'tolinkme21',

    styleTemplateCollapse: 'tolinkme',

    styleTemplateRadioSizeButton: 'tolinkme',
    styleTemplateRadioBorderButton: 'tolinkme',
    styleTemplateConfigBoxShadow: 'tolinkme',
    styleTemplateInputSelectBackground: 'tolinkme',
    styleTemplateConfigText: 'tolinkme',

    classNameReload: `
        cursor-pointer
    `,
};
