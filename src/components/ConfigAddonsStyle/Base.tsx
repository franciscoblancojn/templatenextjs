import { TextStyles } from '@/components/Text';
import { Theme, ThemesType } from '@/config/theme';
import { useLang } from '@/lang/translate';
import { useData } from 'fenextjs-hook/cjs/useData';
import { Popup, PopupStyles } from '../Popup';
import { Collapse, CollapseStyles } from '../Collapse';
import Color from '@/svg/Color';
import Font from '@/svg/Font';
import Border from '@/svg/Border';
import Size from '@/svg/Size';
import {
    RadioSizeButton,
    RadioSizeButtonStyles,
} from '../Input/RadioSizeButton';
import {
    RadioBorderRadiusButton,
    RadioBorderRadiusButtonStyles,
} from '../Input/RadioBorderRadiusButton';
import {
    InputSelectBackground,
    InputSelectBackgroundStyles,
} from '../Input/SelectBackground';
import { SelectBackgroundBtnForm } from '../Input/SelectBackgroundBtnForm';
import { ConfigText, ConfigTextStyles } from '../Input/ConfigText';
import {
    FormCustomFieldConfig,
    FormCustomFieldConfigType,
} from '@/interfaces/FormCustomField';
import { ConfigBorder, ConfigBorderStyles } from '../Input/ConfigBorder';
import ConfigBoxShadow, {
    ConfigBoxShadowStyles,
} from '../Input/ConfigBoxShadow';
import Clone from '@/svg/Clone';
import { useMemo, useState } from 'react';
import LoaderPage from '../Loader/LoaderPage';
import Reload from '@/svg/Reload';
import ConfigTextForm from '../Input/ConfigTextForm';
import ConfigTextBtnForm from '../Input/ConfigTextBtnForm';

export interface ConfigAddonsStyleClassProps {
    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;
    styleTemplateCollapse?: CollapseStyles | ThemesType;
    styleTemplateRadioSizeButton?: RadioSizeButtonStyles | ThemesType;
    styleTemplateRadioBorderButton?: RadioBorderRadiusButtonStyles | ThemesType;
    styleTemplateConfigBorder?: ConfigBorderStyles | ThemesType;
    styleTemplateConfigBoxShadow?: ConfigBoxShadowStyles | ThemesType;
    styleTemplateInputSelectBackground?:
        | InputSelectBackgroundStyles
        | ThemesType;
    styleTemplateConfigText?: ConfigTextStyles | ThemesType;
    classNameReload?: string;
}

export interface ConfigAddonsStyleBaseProps {
    defaultValue?: FormCustomFieldConfig;
    onChange?: (data: FormCustomFieldConfig) => void;
}

export interface ConfigAddonsStyleProps
    extends ConfigAddonsStyleClassProps,
        ConfigAddonsStyleBaseProps {}

export const ConfigAddonsStyleBase = ({
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',
    styleTemplateCollapse = Theme?.styleTemplate ?? '_default',
    styleTemplateRadioSizeButton = Theme?.styleTemplate ?? '_default',
    styleTemplateRadioBorderButton = Theme?.styleTemplate ?? '_default',
    styleTemplateInputSelectBackground = Theme?.styleTemplate ?? '_default',
    styleTemplateConfigText = Theme?.styleTemplate ?? '_default',
    styleTemplateConfigBorder = Theme?.styleTemplate ?? '_default',
    styleTemplateConfigBoxShadow = Theme?.styleTemplate ?? '_default',
    classNameReload = '',
    defaultValue = {},
    onChange,
}: ConfigAddonsStyleProps) => {
    const _t = useLang();

    const [loader, setLoader] = useState(false);
    const [isActive, setIsActive] = useState<FormCustomFieldConfigType | ''>(
        ''
    );

    const defaultValue_ = useMemo(() => defaultValue, []);
    const { data, onChangeData, onRestart } = useData<FormCustomFieldConfig>(
        defaultValue_,
        {
            onChangeDataAfter: (data: FormCustomFieldConfig) => {
                onChange?.(data);
            },
        }
    );

    const content = useMemo(() => {
        if (loader) {
            return <LoaderPage />;
        }

        return (
            <>
                <Collapse
                    header={
                        <>
                            <Size size={15} />
                            {_t('Field size')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'type'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'type' : '');
                    }}
                >
                    <RadioSizeButton
                        onChange={onChangeData('size')}
                        styleTemplate={styleTemplateRadioSizeButton}
                        defaultValue={data.size}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Clone size={15} />
                            {_t('Leftover from the field')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'box-shadow'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'box-shadow' : '');
                    }}
                >
                    <ConfigBoxShadow
                        defaultValue={data.boxShadow}
                        onChange={onChangeData('boxShadow')}
                        styleTemplate={styleTemplateConfigBoxShadow}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Border size={15} />
                            {_t('Field border')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'border'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'border' : '');
                    }}
                >
                    <RadioBorderRadiusButton
                        onChange={onChangeData('borderRadius')}
                        styleTemplate={styleTemplateRadioBorderButton}
                        defaultValue={data.borderRadius}
                    />
                    <ConfigBorder
                        defaultValue={data.border}
                        onChange={onChangeData('border')}
                        styleTemplate={styleTemplateConfigBorder}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Color size={15} />
                            {_t('Field background color')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'bg'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'bg' : '');
                    }}
                >
                    <InputSelectBackground
                        title={null}
                        onChange={onChangeData('background')}
                        styleTemplate={styleTemplateInputSelectBackground}
                        useType={{
                            color: true,
                            gradient: true,
                            img: false,
                            video: false,
                        }}
                        defaultValue={data.background}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Color size={15} />
                            {_t('Submit button background color')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'backgroundBtn'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'backgroundBtn' : '');
                    }}
                >
                    <SelectBackgroundBtnForm
                        title={null}
                        onChange={onChangeData('backgroundBtn')}
                        styleTemplate={styleTemplateInputSelectBackground}
                        useType={{
                            color: true,
                            gradient: true,
                            img: false,
                            video: false,
                        }}
                        defaultValue={data.backgroundBtn}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Submit button text color')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'textBtn'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'textBtn' : '');
                    }}
                >
                    <ConfigTextBtnForm
                        onChange={onChangeData('textBtn')}
                        styleTemplate={styleTemplateConfigText}
                        defaultValue={data.textBtn}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Field Text')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'text'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'text' : '');
                    }}
                >
                    <ConfigText
                        onChange={onChangeData('text')}
                        styleTemplate={styleTemplateConfigText}
                        defaultValue={data.text}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Form title')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'title1'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'title1' : '');
                    }}
                >
                    <ConfigTextForm
                        onChange={onChangeData('title1')}
                        styleTemplate={styleTemplateConfigText}
                        defaultValue={data.title1}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Form Description')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'title2'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'title2' : '');
                    }}
                >
                    <ConfigTextForm
                        onChange={onChangeData('title2')}
                        styleTemplate={styleTemplateConfigText}
                        defaultValue={data.title2}
                    />
                </Collapse>
            </>
        );
    }, [data, isActive, loader, _t]);

    const onRestartData = async () => {
        setLoader(true);
        onRestart();
        await new Promise((r) => setTimeout(r, 500));
        setLoader(false);
    };
    return (
        <>
            <Popup
                btn={_t('Addon Form Style')}
                styleTemplate={styleTemplatePopup}
                contentBackExtra={
                    <>
                        <div
                            className={classNameReload}
                            onClick={onRestartData}
                        >
                            <Reload size={20} />
                        </div>
                    </>
                }
            >
                {content}
            </Popup>
        </>
    );
};
export default ConfigAddonsStyleBase;
