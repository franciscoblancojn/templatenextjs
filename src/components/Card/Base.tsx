import Theme, { ThemesType } from '@/config/theme';
import Image from '@/components/Image';
import Text, { TextStyles } from '@/components/Text';
import Title, { TitleProps } from '@/components/Title';
import Button, { ButtonStyles } from '../Button';
import Layer, { LayerStyles } from '../Layer';
import Space from '../Space';
import Tiktok from '@/svg/tiktok';

export interface CardClassProps {
    classNameContent?: string;
    styleTemplateImage?: ThemesType;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateText2?: TextStyles | ThemesType;
    styleTemplateBtn?: ButtonStyles | ThemesType;
    styleTemplateLayer?: LayerStyles | ThemesType;
    TitleProps?: TitleProps;
}

export interface CardBaseProps {
    img: string;
    img2?: any;
    name: string;
    title: string;
    text: string;
    btn?: string;
    price?: number;
    textSub?: string;
    textTime?: string;
}

export interface CardProps extends CardClassProps, CardBaseProps {}

export const CardBase = ({
    classNameContent = '',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateText2 = Theme.styleTemplate ?? '_default',
    styleTemplateImage = Theme.styleTemplate ?? '_default',
    styleTemplateBtn = Theme.styleTemplate ?? '_default',
    styleTemplateLayer = Theme.styleTemplate ?? '_default',
    TitleProps = {},
    name,
    title,
    text,
    img,
    price = 8.5,
    textSub = 'Subscribe',
    textTime = 'Month',
    img2 = <Tiktok size={20} />,
    btn = 'Tiktok',
}: CardProps) => {
    return (
        <>
            <div className={classNameContent}>
                <Image src={img} styleTemplate={styleTemplateImage} />
                <Text styleTemplate={styleTemplateText2}>{name}</Text>
                <Title {...TitleProps}>{title}</Title>
                <Text styleTemplate={styleTemplateText}>{text}</Text>
                <Space size={50} />
                <div>
                    <Button styleTemplate={styleTemplateBtn} icon={img2}>
                        {btn}
                        <Layer
                            price={price}
                            subscribe={textSub}
                            period={textTime}
                            styleTemplate={styleTemplateLayer}
                        />
                    </Button>
                    <div></div>
                </div>
            </div>
        </>
    );
};
export default CardBase;
