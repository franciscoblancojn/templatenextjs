import { CardClassProps } from '@/components/Card/Base';

export const _default: CardClassProps = {
    classNameContent: `
    bg-gradient-darkAqua-brightPink
    p-l-10
    p-t-5
    p-b-10
    `,

    styleTemplateText: `_default5`,
    styleTemplateText2: '_default6',
    styleTemplateImage: '_default',
    TitleProps: {
        className: `p-t-10
        p-b-10`,
        styleTemplate: '_default',
        typeStyle: 'h6',
    },
};
