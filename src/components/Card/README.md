# Card

## Dependencies

[Text](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Text)
[Title](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Title)
[Image](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Image)

```js
import { Text } from '@/components/Text';
import { Title } from '@/components/Title';
import { Image } from '@/components/Image';
```

## Import

```js
import { Card, CardStyles } from '@/components/Card';
```

## Props

```tsx
interface CardProps {
    img: string;
    name: string;
    title: string;
    text: string;
}
```

## Use

```js
<Card
    img="Frame_8.png"
    name="Reporting"
    title="Stay on top of things with always up-to-date reporting features."
    text="We talked about reporting in the section above but we needed three items here, so mentioning it one more time for posterity."
    styleTemplate="_default"
/>
```
