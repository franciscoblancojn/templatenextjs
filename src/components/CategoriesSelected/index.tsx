import { useMemo } from 'react';

import * as styles from '@/components/CategoriesSelected/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    CategoriesSelectedBaseProps,
    CategoriesSelectedBase,
} from '@/components/CategoriesSelected/Base';

export const CategoriesSelectedStyle = { ...styles } as const;

export type CategoriesSelectedStyles = keyof typeof CategoriesSelectedStyle;

export interface CategoriesSelectedProps extends CategoriesSelectedBaseProps {
    styleTemplate?: CategoriesSelectedStyles | ThemesType;
}

export const CategoriesSelected = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: CategoriesSelectedProps) => {
    const Style = useMemo(
        () =>
            CategoriesSelectedStyle[
                styleTemplate as CategoriesSelectedStyles
            ] ?? CategoriesSelectedStyle._default,
        [styleTemplate]
    );

    return <CategoriesSelectedBase {...Style} {...props} />;
};
export default CategoriesSelected;
