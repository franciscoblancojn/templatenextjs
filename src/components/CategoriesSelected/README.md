# CategoriesSelected

## Dependencies

[CategoriesSelected](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Categories)

```js
import { CategoriesId } from '@/data/components/Categories';
```

## Import

```js
import {
    CategoriesSelected,
    CategoriesSelectedStyles,
} from '@/components/CategoriesSelected';
```

## Props

```tsx
interface CategoriesSelectedProps {
    styleTemplate?: CategoriesSelectedStyles;
    defaultCategories?: CategoriesId[];
    onChange?: (data: CategoriesId[]) => void;
    disabled?: boolean;
}
```

## Use

```js
<CategoriesSelected
    defaultCategories={[]}
    onChange={(data: CategoriesId[]) => {}}
    disabled={false}
/>
```
