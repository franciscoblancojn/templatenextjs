import Title from '@/components/Title';
import { all_categories, CategoriesId } from '@/data/components/Categories';
import { useLang } from '@/lang/translate';
import { useEffect, useState } from 'react';
import Select from '@/components/Input/Select';
import { Close } from '@/svg/close';
import { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import ContentWidth from '../ContentWidth';

export interface CategoriesSelectedClassProps {
    classNameContent?: string;
    classNameContentTop?: string;
    classNameTitle?: string;
    classNameContentCategory?: string;
    classNameCategory?: string;
    classNameCategoryDelete?: string;
    classNameAdd?: string;
    styleTemplateSelect?: InputTextStyles | ThemesType;
}

export interface CategoriesSelectedBaseProps {
    value?: CategoriesId[];
    defaultCategories?: CategoriesId[];
    onChange?: (data: CategoriesId[]) => void;
    disabled?: boolean;
}

export interface CategoriesSelectedProps
    extends CategoriesSelectedClassProps,
        CategoriesSelectedBaseProps {}

export const CategoriesSelectedBase = ({
    classNameContent = '',
    classNameContentTop = '',
    classNameTitle = '',
    classNameContentCategory = '',
    classNameCategory = '',
    classNameCategoryDelete = '',
    classNameAdd = '',
    styleTemplateSelect = Theme.styleTemplate ?? '_default',

    value,
    defaultCategories = [],
    disabled = false,
    ...props
}: CategoriesSelectedProps) => {
    const _t = useLang();
    const [categories, setCategories] = useState<CategoriesId[]>(
        value ?? defaultCategories
    );

    const addCategory = (newCategory: CategoriesId) => {
        setCategories((pre) => [...pre, newCategory]);
    };
    const deleteCategory = (deleteCategory: CategoriesId) => {
        setCategories((pre) => pre.filter((c) => c != deleteCategory));
    };
    useEffect(() => {
        props?.onChange?.(categories);
    }, [categories]);
    useEffect(() => {
        if (value) {
            if (JSON.stringify(value) != JSON.stringify(categories)) {
                setCategories(value);
            }
        }
    }, [value]);

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentTop}>
                    <Title className={classNameTitle}>{_t('Categories')}</Title>
                    <ContentWidth size={100}>
                        <div className={classNameAdd}>
                            <Select
                                styleTemplate={styleTemplateSelect}
                                placeholder={_t('+ Add More')}
                                options={all_categories
                                    .filter((c) => !categories.includes(c.id))
                                    .map((c) => ({
                                        text: c.name,
                                        value: c.id,
                                    }))}
                                onChange={(o) => {
                                    if (o) {
                                        addCategory(o.value as CategoriesId);
                                    }
                                }}
                                value=""
                                disabled={disabled}
                            />
                        </div>
                    </ContentWidth>
                </div>
                <div className={classNameContentCategory}>
                    {all_categories.map((c) => {
                        if (!categories.includes(c.id)) {
                            return <></>;
                        }
                        return (
                            <div key={c.id} className={classNameCategory}>
                                {c.name}
                                {!disabled ? (
                                    <span
                                        className={classNameCategoryDelete}
                                        onClick={() => {
                                            deleteCategory(c.id);
                                        }}
                                    >
                                        <Close size={9} />
                                    </span>
                                ) : (
                                    <></>
                                )}
                            </div>
                        );
                    })}
                </div>
            </div>
        </>
    );
};
export default CategoriesSelectedBase;
