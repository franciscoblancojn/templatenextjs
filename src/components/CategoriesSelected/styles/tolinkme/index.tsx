import { CategoriesSelectedClassProps } from '@/components/CategoriesSelected/Base';

export const tolinkme: CategoriesSelectedClassProps = {
    classNameContent: `
        width-p-100
    `,
    classNameContentTop: `
        width-p-100
        m-b-25
        flex
        flex-justify-between
        flex-align-center
    `,
    classNameTitle: `
        font-14 font-w-900 font-nunito
        color-greyishBrown 
        pos-r
    `,
    classNameAdd: `
        cursor-pointer
    `,
    styleTemplateSelect: 'tolinkme7',
    classNameContentCategory: `
        flex
        flex-gap-column-7
        flex-gap-row-7
    `,
    classNameCategory: `
        p-h-10
        p-v-5
        bg-whiteThree
        border-radius-26
        font-13
        font-nunito
        color-black
        flex
        flex-align-center
        flex-gap-column-7
    `,
    classNameCategoryDelete: `
        cursor-pointer
        flex
        flex-align-center
        color-warmGrey
    `,
};
