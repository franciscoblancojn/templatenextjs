import { PropsWithChildren, useEffect, useState } from 'react';
import CSS from 'csstype';
import GoBack, { GoBackProps } from '../GoBack';

export interface PopupClassProps {
    classNameContent?: string;
    classNameContentActive?: string;
    classNameContentInative?: string;
    classNameButton?: string;
    classNamePopupOutSider?: string;
    classNamePopupOutSiderActive?: string;
    classNamePopupOutSiderInactive?: string;
    classNamePopup?: string;
    stylePopup?: CSS.Properties;
    classNamePopupActive?: string;
    classNamePopupInactive?: string;
    classNamePopupWithBack?: string;
    classNamePopupContentBack?: string;

    back?: boolean;
    GoBackProps?: GoBackProps;
    iconBtn?: any;
}

export interface PopupBaseProps {
    className?: string;
    btn?: any;
    btnClose?: any;
    btnSwActive?: boolean;
    setBtnSwActive?: (active: boolean) => void;
    onClose?: () => void;
    goBackProps?: GoBackProps;
    contentBackExtra?: any;
}

export interface PopupProps extends PopupClassProps, PopupBaseProps {}

export const PopupBase = ({
    classNameContent = '',
    classNameContentActive = '',
    classNameContentInative = '',

    classNameButton = '',
    classNamePopupOutSider = '',
    classNamePopupOutSiderActive = '',
    classNamePopupOutSiderInactive = '',
    classNamePopup = '',
    stylePopup = {},
    classNamePopupActive = '',
    classNamePopupInactive = '',

    classNamePopupWithBack = '',
    classNamePopupContentBack = '',

    back = false,
    GoBackProps = {},
    iconBtn = <></>,

    children,
    className = '',
    btn,
    btnClose = null,
    btnSwActive = undefined,
    goBackProps = {},
    contentBackExtra = <></>,
    ...props
}: PropsWithChildren<PopupProps>) => {
    const dW = window.onpopstate;
    const [active, setActive] = useState(false);

    const toggleActive = () => {
        if (active) {
            props?.onClose?.();
        }
        setActive(!active);
    };

    useEffect(() => {
        if (active) {
            history.pushState(null, document.title, location.href);
            window.onpopstate = () => {
                setActive(false);
            };
        } else {
            window.onpopstate = dW;
        }

        if (active != btnSwActive) {
            props?.setBtnSwActive?.(active);
        }
    }, [active]);
    useEffect(() => {
        if (btnSwActive != undefined) {
            setActive(btnSwActive);
        }
    }, [btnSwActive]);

    return (
        <div
            className={`${classNameContent} ${className} ${
                active ? classNameContentActive : classNameContentInative
            } `}
        >
            <div className={classNameButton} onClick={toggleActive}>
                {active ? btnClose ?? btn : btn}
                {iconBtn}
            </div>
            <div
                className={`${classNamePopupOutSider} ${
                    active
                        ? classNamePopupOutSiderActive
                        : classNamePopupOutSiderInactive
                }`}
                onClick={toggleActive}
            ></div>
            <div
                className={`
                    ${classNamePopup}
                    ${
                        active ? classNamePopupActive : classNamePopupInactive
                    }    
                    ${back ? classNamePopupWithBack : ''}
                `}
                style={stylePopup}
            >
                <div className={classNamePopupContentBack}>
                    {back ? (
                        <>
                            <GoBack
                                onClick={toggleActive}
                                {...GoBackProps}
                                {...goBackProps}
                                className={`${GoBackProps?.className ?? ''} ${
                                    goBackProps?.className ?? ''
                                }`}
                            />
                        </>
                    ) : (
                        <></>
                    )}
                    {contentBackExtra}
                </div>
                {children}
            </div>
        </div>
    );
};
export default PopupBase;
