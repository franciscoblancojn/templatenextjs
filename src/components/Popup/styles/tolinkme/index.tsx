import { PopupClassProps } from '@/components/Popup/Base';
import { PaginationNext } from '@/svg/pagination';

export const tolinkme: PopupClassProps = {
    classNameContent: `
        tolinkme
    `,
    classNameContentInative: `
        overflow-hidden  
    `,
    classNameButton: `
        
    `,
    classNamePopup: `
        pos-f
        right-0
        bottom-0
        overflow-auto
        z-index-9
        box-shadow box-shadow-black-50 box-shadow-b-5
        border-radius-0
        border-t-l-radius-10
        border-t-r-radius-10-
        p-h-10 p-v-15
        m-0
        bg-white
        transition-5
        transform
    `,
    stylePopup: {
        maxWidth: 'min(100%,20rem)',
        maxHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    classNamePopupActive: `
        transform-translate-Y-0  
    `,
    classNamePopupInactive: `
        transform-translate-Y-p-100
    `,
};

export const tolinkme2: PopupClassProps = {
    classNameContent: `
        overflow-hidden  
        tolinkme2
    `,
    classNameContentInative: `
    `,
    classNameButton: `
        p-v-20
        p-h-5
        cursor-pointer
        font-nunito
        font-w-900
        color-Charcoal
        border-0
        border-b-1
        border-style-solid
        border-whiteTwo
        flex
        flex-align-center
        flex-justify-between
    `,
    iconBtn: (
        <>
            <PaginationNext size={10} />
        </>
    ),
    classNamePopup: `
        pos-f
        right-0
        z-index-9
        overflow-auto
        overflow-hidden-x
        box-shadow box-shadow-black-50 box-shadow-b-5
        p-h-15
        p-sm-h-20 p-v-15
        m-0
        bg-white
        transition-5
        transform
        height-vh-min-100
        height-lg-vh-min-0
        top-0
        top-lg-inherit
        bottom-lg-0
    `,
    stylePopup: {
        width: 'min(100vw,max(50vw,50rem))',
        height: 'calc(100vh - var(--sizeHeader,0px))',
    },
    classNamePopupActive: `
        transform-translate-X-0  
    `,
    classNamePopupInactive: `
        transform-translate-X-p-100
        transform-scale-X-0
        
    `,
    classNamePopupWithBack: `
    
    `,
    classNamePopupContentBack: `
        pos-sk
        top-0
        bg-white
        z-index-7
        box-shadow
        box-shadow-c-white
        box-shadow-y--15
        flex
        flex-justify-between
    `,

    back: true,
    GoBackProps: {
        styleTemplate: 'tolinkme2',
        className: `
            flex
            m-b-20
        `,
    },

    classNamePopupOutSider: `
        pos-f
        inset-0
        width-p-100
        heigth-p-100
        bg-black
        opacity-2
        z-index-1
    `,
    classNamePopupOutSiderInactive: `
        d-none
    `,
};
export const tolinkmeAddons: PopupClassProps = {
    classNameContent: `
        overflow-hidden  
        tolinkme2
    `,
    classNameContentInative: `
    `,
    classNameButton: `
        p-v-20
        p-h-5
        cursor-pointer
        font-nunito
        font-w-900
        border-0
        border-style-solid
        border-whiteTwo
        flex
        flex-align-center
        flex-justify-between
    `,
    iconBtn: (
        <>
            <PaginationNext size={10} />
        </>
    ),
    classNamePopup: `
        pos-f
        right-0
        z-index-9
        overflow-auto
        overflow-hidden-x
        box-shadow box-shadow-black-50 box-shadow-b-5
        p-h-15
        p-sm-h-20 p-v-15
        m-0
        bg-white
        transition-5
        transform
        height-vh-min-100
        height-lg-vh-min-0
        top-0
        top-lg-inherit
        bottom-lg-0
    `,
    stylePopup: {
        width: 'min(100vw,max(50vw,50rem))',
        height: 'calc(100vh - var(--sizeHeader,0px))',
    },
    classNamePopupActive: `
        transform-translate-X-0  
    `,
    classNamePopupInactive: `
        transform-translate-X-p-100
        transform-scale-X-0
        
    `,
    classNamePopupWithBack: `
    
    `,
    classNamePopupContentBack: `
        pos-sk
        top-0
        bg-white
        z-index-7
        box-shadow
        box-shadow-c-white
        box-shadow-y--15
        flex
        flex-justify-between
    `,

    back: true,
    GoBackProps: {
        styleTemplate: 'tolinkme2',
        className: `
            flex
            m-b-20
        `,
    },

    classNamePopupOutSider: `
        pos-f
        inset-0
        width-p-100
        heigth-p-100
        bg-black
        opacity-2
        z-index-1
    `,
    classNamePopupOutSiderInactive: `
        d-none
    `,
};

export const tolinkme3: PopupClassProps = {
    ...tolinkme2,
    classNameContent: `
        overflow-hidden  
        tolinkme3
    `,
    classNameButton: `
        p-v-
        p-h-5
        cursor-pointer
        font-nunito
        font-w-900
        color-Charcoal
        flex
        flex-align-center
        flex-justify-between
    `,
};

export const tolinkme4: PopupClassProps = {
    ...tolinkme3,
    classNamePopup: `
        pos-f
        right-0
        z-index-9
        overflow-auto
        overflow-hidden-x
        box-shadow box-shadow-black-50 box-shadow-b-5
         p-v-15
        m-0
        bg-white
        transition-5
        transform
        height-vh-min-100
        height-lg-vh-min-0
        top-0
        top-lg-inherit
        bottom-lg-0
    `,
    classNamePopupContentBack: `
       m-l-15 
    `,
    iconBtn: <></>,
};
