import { PopupClassProps } from '@/components/Popup/Base';

export const mooveri: PopupClassProps = {
    classNameContent: `
        
    `,
    classNameButton: `
        
    `,
    classNamePopup: `
        pos-f
        left-0
        bottom-0
        flex
        flex-align-center
        text-center
        z-index-9
        box-shadow box-shadow-black-50 box-shadow-b-2
        border-radius-0
        border-t-l-radius-20
        border-t-r-radius-20
        p-25
        bg-white
        transform
        transition-5
        m-auto
        width-p-100
    `,
    stylePopup: {
        height: 'min(calc(100vh - var(--sizeHeader,0px)),10rem)',
    },
    classNamePopupActive: `
        transform-translate-Y-0  
    `,
    classNamePopupInactive: `
        transform-translate-Y-vh-100
        
    `,
};
