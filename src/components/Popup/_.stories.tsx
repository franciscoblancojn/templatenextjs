import { Story, Meta } from "@storybook/react";

import { PopupProps, Popup } from "./index";

export default {
    title: "Popup/Popup",
    component: Popup,
} as Meta;

const PopupIndex: Story<PopupProps> = (args) => (
    <Popup {...args}>Test Children</Popup>
);

export const Index = PopupIndex.bind({});
Index.args = {
    btn:<button>Button</button>,
};
