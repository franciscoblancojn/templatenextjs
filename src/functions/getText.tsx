import { ConfigText, ConfigTextFontWeight } from '@/interfaces/ConfigText';
import CSS from 'csstype';

export const getText: (
    data: ConfigText,
    text?: string,
    className?: string,
    style?: CSS.Properties
) => any = (data: ConfigText, text = '', className = '', style = {}) => {
    return (
        <div
            style={{
                fontFamily: `${data.fontFamily ?? 'nunito'}`,
                fontSize: `${(data.fontSize ?? 16) / 16}rem`,
                fontWeight: `${
                    (ConfigTextFontWeight.indexOf(
                        data?.fontWeight ?? 'regular'
                    ) +
                        1) *
                    100
                }`,
                color: data.color,
                lineHeight: (data?.lineHeight ?? 10) / 10,
                ...style,
            }}
            className={className}
        >
            {text}
        </div>
    );
};
