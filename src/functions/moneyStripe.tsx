export const moneyStripe = (price: number) => {
    let letter = '';
    if (price > 999_999) {
        price /= 1_000_000;
        letter = 'M';
    }

    price /= 100;

    const options = {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
    };
    const numberFormat = new Intl.NumberFormat('en-US', options);
    return numberFormat.format(price) + letter;
};

export default moneyStripe;
