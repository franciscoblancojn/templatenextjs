import { UserStatusType } from '@/interfaces/User';

export const parseStatusUser = (status: UserStatusType) => {
    const sw: {
        [id in UserStatusType]: string;
    } = {
        verify: 'Verified',
        'no-verify': 'Not Verified',
        delete: 'Deleted',
    };
    return sw[status];
};
