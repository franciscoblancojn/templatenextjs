export const toCapitalize = (s: string) => {
    return s[0].toLocaleUpperCase() + s.slice(1, s.length);
};
