import { TypeDate } from '@/interfaces/Date';

export const parseDateYYYYMMDD = (date: Date) => {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
};

export interface parseTextToDateProps {
    text: string;
    type: TypeDate;
}

export const parseTextToDate = ({ text, type }: parseTextToDateProps) => {
    const date = new Date();

    if (type == 'date') {
        const a = text.split('-');
        date.setFullYear(parseInt(a?.[0]));
        date.setMonth(parseInt(a?.[1]) - 1);
        date.setDate(parseInt(a?.[2]));
        return date;
    }
    if (type == 'month') {
        const a = text.split('-');

        date.setFullYear(parseInt(a?.[0]));
        date.setMonth(parseInt(a?.[1]) - 1);
        date.setDate(1);
        return date;
    }
    if (type == 'week') {
        const a = text.split('-W');
        date.setFullYear(parseInt(a?.[0]));
        date.setMonth(0);
        date.setDate(parseInt(a?.[1]) * 7);
        return date;
    }
    if (type == 'time') {
        const a = text.split(':');
        date.setHours(parseInt(a?.[0]));
        date.setMinutes(parseInt(a?.[1]));
        return date;
    }
    return date;
};

export interface parseDateToTextProps {
    date?: Date;
    type: TypeDate | 'YYYY-MM-DD';
}

export const parseDateToText = ({
    date = new Date(),
    type,
}: parseDateToTextProps) => {
    if (type == 'YYYY-MM-DD') {
        return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
    }
    if (type == 'date') {
        return date.toDateString();
    }
    if (type == 'month') {
        return `${date.getFullYear()}-${date.getMonth() + 1}`;
    }
    if (type == 'week') {
        const tf: any = new Date(date);
        const ti: any = new Date(tf.getFullYear(), 0, 0);
        const diff = tf - ti;
        const oneDay = 1000 * 60 * 60 * 24;
        const d = Math.floor(diff / oneDay);
        const w = d / 7;
        return `${date.getFullYear()}-W${w}`;
    }
    if (type == 'time') {
        return `${date.getHours()}:${date.getMinutes()}`;
    }
    return '';
};
