import CSS from 'csstype';

import {
    BackgroundDataObjectTypeProps,
    BackgroundValueType,
} from '@/interfaces/Background';

export const getBg: (
    data: BackgroundDataObjectTypeProps,
    classBg?: string,
    style?: CSS.Properties
) => any = (
    data: BackgroundDataObjectTypeProps,
    classBg = 'pos-f inset-0 width-p-100 height-p-100',
    style = {}
) => {
    const bg: BackgroundValueType = {
        color: (
            <div
                className={classBg}
                style={{
                    background: data?.color,
                    opacity: (data?.opacity ?? 100) / 100,
                    ...style,
                }}
            />
        ),
        gradient: (
            <div
                className={classBg}
                style={{
                    opacity: (data?.opacity ?? 100) / 100,
                    background: `linear-gradient(${data?.gradient?.deg}deg, ${data?.gradient?.color1}, ${data?.gradient?.color2})`,
                    ...style,
                }}
            />
        ),

        img: (
            <>
                {data?.img?.fileData ? (
                    <>
                        <img
                            className={classBg}
                            src={data?.img?.fileData}
                            alt={data?.img?.text}
                            style={{
                                opacity: (data?.opacity ?? 100) / 100,
                                ...style,
                            }}
                        />
                    </>
                ) : (
                    <></>
                )}
            </>
        ),
        video: (
            <>
                {data?.video?.fileData ? (
                    <>
                        <video
                            src={data?.video?.fileData}
                            autoPlay={true}
                            controls={false}
                            loop={true}
                            muted={true}
                            className={`${classBg} object-fit-cover`}
                            style={{
                                opacity: (data?.opacity ?? 100) / 100,
                                ...style,
                            }}
                        />
                    </>
                ) : (
                    <></>
                )}
            </>
        ),
    };
    return bg[data?.type ?? 'color'];
};
