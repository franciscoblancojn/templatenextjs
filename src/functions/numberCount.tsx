export const numberCount = (n: number) => {
    return n.toLocaleString('en-US', {
        maximumFractionDigits: 2,
    });
};
export const numberCountString = (v: string) => {
    const n = `${v}`.replace(/[^\d.]/g, '');
    return parseFloat(n).toLocaleString('en-US', {
        maximumFractionDigits: 2,
    });
};
