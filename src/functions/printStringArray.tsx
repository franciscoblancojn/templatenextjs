export const printStringArray = (
    item: string | string[],
    parse?: (items: string[]) => any
) => {
    const items = [item].flat(2);

    if (parse) {
        return parse(items);
    }

    return (
        <>
            {items.map((e, i) => {
                return <div key={i}>{e}</div>;
            })}
        </>
    );
};
