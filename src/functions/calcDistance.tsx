export const calcDistance = async (origins: any, destinations: any) => {
    const service = new window.google.maps.DistanceMatrixService();
    return service.getDistanceMatrix({
        origins: origins,
        destinations: destinations,
        travelMode: window.google.maps.TravelMode.DRIVING,
        avoidHighways: false,
        avoidTolls: false,
        unitSystem: window.google.maps.UnitSystem.METRIC,
    });
};
