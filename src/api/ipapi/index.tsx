import { request } from '@/api/request';
import { URL } from '@/api/ipapi/_';
import log from '@/functions/log';

export interface IpInfo {
    ip: string;
    network: string;
    version: string;
    city: string;
    region: string;
    region_code: string;
    country: string;
    country_name: string;
    country_code: string;
    country_code_iso3: string;
    country_capital: string;
    country_tld: string;
    continent_code: string;
    in_eu: boolean;
    postal: string;
    latitude: number;
    longitude: number;
    timezone: string;
    utc_offset: string;
    country_calling_code: string;
    currency: string;
    currency_name: string;
    languages: string;
    country_area: number;
    country_population: number;
    asn: string;
    org: string;
}

export type GETINTERFACE = () => Promise<IpInfo | null>;

export const GET: GETINTERFACE = async () => {
    const result = await request({
        method: 'get',
        url: `${URL}`,
        headers: {
            'Content-Type': 'application/json',
        },
    });
    log('get result ipapi', result, 'aqua');
    if (result.type == 'ok') {
        return result.result;
    }
    return null;
};
