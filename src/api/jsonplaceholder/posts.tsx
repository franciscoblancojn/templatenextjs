import { request } from '@/api/request';

export const URL = 'https://jsonplaceholder.typicode.com/posts';

export interface GETProps {
    id?: string;
}

export const GET = async ({ id = '' }: GETProps) => {
    return await request({
        url: `${URL}/${id}`,
    });
};

export interface POSTProps {
    data: {
        [key: string]: any;
    };
}
export const POST = async ({ data }: POSTProps) => {
    return await request({
        method: 'post',
        url: `${URL}`,
        data,
    });
};

export interface PUTProps {
    id: string;
    data: {
        [key: string]: any;
    };
}
export const PUT = async ({ id, data }: PUTProps) => {
    return await request({
        method: 'put',
        url: `${URL}/${id}`,
        data,
    });
};

export interface DELETEProps {
    id: string;
}
export const DELETE = async ({ id }: DELETEProps) => {
    return await request({
        method: 'delete',
        url: `${URL}/${id}`,
    });
};
