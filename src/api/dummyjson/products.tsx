import { request } from '@/api/request';

export const URL = 'https://dummyjson.com/products';

export interface GETProps {
    limit?: number;
    skip?: number;
}

export const GET = async ({ limit = 10, skip = 0 }: GETProps) => {
    return await request({
        url: `${URL}?limit=${limit}&skip=${skip}`,
    });
};
