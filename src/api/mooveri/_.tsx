export const URLPRO = 'https://xytrl7uy.mooveri.com/api/v1';

export const URLDEV = 'https://stageback.mooveri.com/api/v1';
// export const URLDEV = 'https://7de7-181-62-52-68.ngrok.io/api/v1';

export const URL = URLPRO;

export const URLINFERENCIA = 'https://inference.mooveri.com/api/v1';

export const URLSOCKET = 'wss://inference.mooveri.com';
