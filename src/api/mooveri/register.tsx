import {
    onSubmintRegister,
    // onValidateUsername,
} from '@/components/Form/Register/Base';
import log from '@/functions/log';
import { request } from '@/api/request';
import { URL } from '@/api/mooveri/_';

import { DataRegister } from '@/interfaces/Register';

export const Register: onSubmintRegister = async (data: DataRegister) => {
    log('DataRegister', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'register ok mooveri',
            data: {
                id: '1',
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
                name: 'test',
                img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg',
            },
        };
    }
    const provider = 'email';
    const url = data.type == 'company' ? '/user_company' : '/client';
    const responde = await request({
        method: 'post',
        url: `${URL}${url}`,
        data: {
            email: data.email,
            first_name: data.firstName,
            last_name: data.lastName,
            phone: data.phone,
            password: data.password,
            provider,
        },
    });
    log('responde register', responde, 'aqua');
    if (responde.type == 'error') {
        throw {
            message: responde.error,
        };
    }

    return {
        status: 'ok',
        message: 'register ok',
        data: {
            id: responde.result?.data?.uuid,
            token: responde.result?.data?.token,
            name: responde.result?.data?.first_name,
            img: responde.result?.data?.imagen,
            email: responde.result?.data?.email,
            role: responde.result?.data?.type,
        },
    };
};
