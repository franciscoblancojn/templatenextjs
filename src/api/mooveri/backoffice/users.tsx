import { URL } from '@/api/mooveri/_';
import { request } from '@/api/request';
import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';
import { UserLoginProps } from '@/hook/useUser';
import { CompanyProps } from '@/interfaces/Company';
import { TransactionProps } from '@/interfaces/Transaction';

import { DataUserProps, UserProps } from '@/interfaces/User';
import { parseTransactionMooveri } from './transactions';

export const parseUserMooveri: (data: any) => UserProps = (data: any) => {
    try {
        const transactions: TransactionProps[] = (data?.transaction ?? [])?.map(
            parseTransactionMooveri
        );

        const companyObject: {
            [id: string]: CompanyProps;
        } = {};

        transactions.forEach((t: TransactionProps) => {
            if (t?.company && !companyObject[t?.company?.id ?? '']) {
                companyObject[t?.company?.id ?? ''] = t?.company;
            }
        });

        const companies: CompanyProps[] = Object.values(companyObject) ?? [];

        const address = data.client_address?.[0];

        const r: UserProps = {
            id: data.uuid,
            name: data.first_name,
            email: data.email,
            phone: data.phone,
            status: data?.status_confirm ? 'verify' : 'no-verify',
            dateCreate: new Date(data.created_at),

            first_name: data.first_name ?? '',
            imagen: data.imagen ?? '/image/_default/placeholder.png',
            last_name: data.last_name ?? '',
            status_confirm: data?.status_confirm ?? false,
            type_login: data.type_login ?? '',
            created_at: new Date(data.created_at),

            transactions,
            companies,

            address: address
                ? {
                      name_location: address.name_location,
                  }
                : undefined,
        };
        return r;
    } catch (error) {
        log('error parseUserMooveri', error, 'red');
        throw 'Error parseUserMooveri';
    }
};

export type onLoadUserProps = (
    user?: UserLoginProps
) => Promise<UserProps[]> | UserProps[];

export const onLoadUser: onLoadUserProps = async (user?: UserLoginProps) => {
    log('Data onSaveInput', {
        user,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return [
            {
                id: '1',
                name: 'Name',
                email: 'email@gmail.com',
                phone: '+57111111',
                status: 'verify',
                dateCreate: new Date('1-1-1'),
            },
            {
                id: '1',
                name: 'Name',
                email: 'email@gmail.com',
                phone: '+57111111',
                status: 'verify',
                dateCreate: new Date('1-1-1'),
            },
            {
                id: '1',
                name: 'Name',
                email: 'email@gmail.com',
                phone: '+57111111',
                status: 'verify',
                dateCreate: new Date('1-1-1'),
            },
        ];
    }
    const responde = await request({
        method: 'get',
        url: `${URL}/client`,
        headers: {
            authorization: user?.token,
        },
    });
    log('responde get client', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }

    const users = responde.result?.data;

    return users.map(parseUserMooveri);
};

export type onLoadUserByIdProps = (
    user?: UserLoginProps,
    uuid?: string
) => Promise<UserProps> | UserProps;

export const onLoadUserById: onLoadUserByIdProps = async (
    user?: UserLoginProps,
    uuid?: string
) => {
    log('Data onSaveInput', {
        user,
        URL,
        uuid,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            id: '1',
            name: 'Name',
            email: 'email@gmail.com',
            phone: '+57111111',
            status: 'verify',
            dateCreate: new Date('1-1-1'),
        };
    }
    const responde = await request({
        method: 'get',
        url: `${URL}/client?uuid=${uuid}`,
        headers: {
            authorization: user?.token,
        },
    });
    log('responde get client', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }

    const users = responde.result?.data;

    return users.map(parseUserMooveri)[0];
};

export type onUpdateUserProps = (
    user?: UserLoginProps,
    data?: DataUserProps
) => Promise<SubmitResult> | SubmitResult;

export const onUpdateUser: onUpdateUserProps = async (
    user?: UserLoginProps,
    data?: DataUserProps
) => {
    log('Data onSaveInput', {
        user,
        URL,
        data,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'User Update Ok',
        };
    }
    const responde = await request({
        method: 'put',
        url: `${URL}/client?uuid=${data?.id}`,
        headers: {
            authorization: user?.token,
        },
        data: {
            // first_name?: string;
            // imagen?: string;
            // last_name?: string;
            // status_confirm?: boolean;
            // type_login?: string;
            // created_at?: Date;
        },
    });
    log('responde get client', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }

    return {
        status: 'ok',
        message: 'User Update Ok',
    };
};
