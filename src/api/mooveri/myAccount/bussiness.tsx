// import { request } from "@/api/request";
import {
    DataTypeFormBussiness,
    onSubmintBussinessInformation,
} from '@/components/Form/BussinessInformation/Base';
import log from '@/functions/log';
import { UserLoginProps } from '@/hook/useUser';
import { DataBussinessInformation } from '@/interfaces/BussinessInformation';
import { URL } from '@/api/mooveri/_';
import { request } from '@/api/request';

export type onLoadBussinesProps = (
    user?: UserLoginProps
) =>
    | Promise<DataBussinessInformation<DataTypeFormBussiness>>
    | DataBussinessInformation<DataTypeFormBussiness>;

export const onLoadBussines: onLoadBussinesProps = async (
    user?: UserLoginProps
) => {
    log('Data onSaveInput', {
        user,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg',
            company: 'Company Mock',
            contactName: 'Contact Name Mock',
            legalName: 'Legal Name Mock',
            tel: {
                code: '+57',
                tel: '11111',
            },
            alternativeTel: {
                code: '+57',
                tel: '11111',
            },
            yearFounded: new Date(),
        };
    }
    const respond = await request({
        method: 'get',
        url: `${URL}/company?user_company_uuid=${user?.id}`,
        headers: {
            authorization: user?.token,
        },
    });
    log('respond onLoadBussines', respond, 'aqua');
    return {
        img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg',
        company: 'Company Mock',
        contactName: 'Contact Name Mock',
        legalName: 'Legal Name Mock',
        tel: {
            code: '+57',
            tel: '11111',
        },
        alternativeTel: {
            code: '+57',
            tel: '11111',
        },
        yearFounded: new Date(),
    };
};

export const onSaveBussines: onSubmintBussinessInformation =
    (user?: UserLoginProps) =>
    async (data: DataBussinessInformation<DataTypeFormBussiness>) => {
        log('Data onSaveInput', {
            data,
            user,
        });
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            return {
                status: 'ok',
                message: 'Save Bussines Ok',
            };
        }

        if (!data.uuid) {
            //TODO:pendiente
            const respond = await request({
                method: 'post',
                url: `${URL}/company`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    name: data?.contactName,
                    legal_name: data?.legalName,
                    phone_1: data?.tel,
                    phone_2: data?.alternativeTel,
                    imagen: data?.img,
                    company_details: {
                        create: {
                            contact_name: data?.contactName,
                            year_founded: data?.yearFounded,
                        },
                    },
                    company_precios: {
                        create: [
                            {
                                type: 'OVERTIME',
                                value: '0',
                            },
                            {
                                type: 'STANDAR',
                                value: '0',
                            },
                            {
                                type: 'WEEKEND_RATE',
                                value: '0',
                            },
                        ],
                    },
                    company_horarios: {
                        create: [
                            {
                                day: 'Monday',
                                active: false,
                                company_schedules_hours: {
                                    create: [
                                        {
                                            start: '00:00',
                                            end: '00:00',
                                        },
                                    ],
                                },
                            },
                            {
                                day: 'Tuesday',
                                active: false,
                                company_schedules_hours: {
                                    create: [
                                        {
                                            start: '00:00',
                                            end: '00:00',
                                        },
                                    ],
                                },
                            },
                            {
                                day: 'Wednesday',
                                active: false,
                                company_schedules_hours: {
                                    create: [
                                        {
                                            start: '00:00',
                                            end: '00:00',
                                        },
                                    ],
                                },
                            },
                            {
                                day: 'Thursday',
                                active: false,
                                company_schedules_hours: {
                                    create: [
                                        {
                                            start: '00:00',
                                            end: '00:00',
                                        },
                                    ],
                                },
                            },
                            {
                                day: 'Friday',
                                active: false,
                                company_schedules_hours: {
                                    create: [
                                        {
                                            start: '00:00',
                                            end: '00:00',
                                        },
                                    ],
                                },
                            },
                            {
                                day: 'Saturday',
                                active: false,
                                company_schedules_hours: {
                                    create: [
                                        {
                                            start: '00:00',
                                            end: '00:00',
                                        },
                                    ],
                                },
                            },
                            {
                                day: 'Sunday',
                                active: false,
                                company_schedules_hours: {
                                    create: [
                                        {
                                            start: '00:00',
                                            end: '00:00',
                                        },
                                    ],
                                },
                            },
                        ],
                    },
                },
            });
            log('respond onSaveBussines', respond, 'aqua', {
                json: true,
            });
        }
        throw {
            message: 'testing',
        };
        return {
            status: 'ok',
            message: 'Save Bussines Ok',
        };
    };
