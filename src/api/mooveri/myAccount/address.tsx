import { request } from '@/api/request';
import { onSubmintManageAddress } from '@/components/Form/ManageAddreess/Base';
import log from '@/functions/log';
import { UserLoginProps } from '@/hook/useUser';
import { DataManageAddress } from '@/interfaces/ManageAddress';
import { URL } from '@/api/mooveri/_';
import { onUpdateAddressType } from '@/components/Form/ManageAddreessEdit/Base';

export type onLoadAddressProps = (
    user?: UserLoginProps
) => Promise<DataManageAddress[]> | DataManageAddress[];

export const onLoadAddress: onLoadAddressProps = async (
    user?: UserLoginProps
) => {
    log('Data onSaveInput', {
        user,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return [
            {
                id: '1',
                address: 'Test Address',
                apt: 'Test Apt',
                city: 'Test City',
                location: 'Test Address',
                zipcode: 'Test ZipCode',
            },
            {
                id: '2',
                address: 'Test Address',
                apt: 'Test Apt',
                city: 'Test City',
                location: 'Test Address',
                zipcode: 'Test ZipCode',
            },
            {
                id: '3',
                address: 'Test Address',
                apt: 'Test Apt',
                city: 'Test City',
                location: 'Test Address',
                zipcode: 'Test ZipCode',
            },
        ];
    }
    const respond = await request({
        method: 'get',
        url: `${URL}/client/address?clientUuid=${user?.id}`,
        headers: {
            authorization: user?.token,
        },
    });
    log('respond onLoadAddress', respond, 'aqua');
    if (respond.type == 'error') {
        throw {
            message: '',
        };
    }

    return (respond.result?.data ?? []).map((a: any) => {
        const aR: DataManageAddress = {
            id: a.uuid,
            address: a.address,
            city: a.city,
            apt: a.apt,
            location: a.name_location,
            zipcode: a.zip_code,
        };
        return aR;
    });
};

export const onSaveAddress: onSubmintManageAddress =
    (user?: UserLoginProps) => async (data: DataManageAddress) => {
        log('Data onSaveInput', {
            data,
        });
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            return {
                status: 'ok',
                message: 'Save Address Ok',
            };
        }
        const respond = await request({
            method: 'post',
            url: `${URL}/client/address`,
            headers: {
                authorization: user?.token,
            },
            data: {
                address: data.address,
                city: data.city,
                apt: data.apt,
                zip_code: data.zipcode,
                name_location: data.location,
                clientUuid: user?.id,
            },
        });
        log('respond onSaveAddress', respond, 'aqua');
        if (respond.type == 'error') {
            return {
                status: 'error',
                message:
                    respond?.error?.response?.data?.error ??
                    respond?.error?.response?.data?.message ??
                    '',
            };
        }
        return {
            status: 'ok',
            message: 'Save Address Ok',
        };
    };

export const onUpdateAddress: onUpdateAddressType =
    (user?: UserLoginProps) => async (data: DataManageAddress) => {
        log('Data onSaveInputAddress', {
            data,
        });
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            return {
                status: 'ok',
                message: 'Update Address Ok',
            };
        }
        const respond = await request({
            method: 'put',
            url: `${URL}/client/address/${data.id}`,
            headers: {
                authorization: user?.token,
            },
            data: {
                address: data.address,
                city: data.city,
                apt: data.apt,
                zip_code: data.zipcode,
                name_location: data.location,
                clientUuid: user?.id,
            },
        });
        log('respond onSaveInputAddress', respond, 'aqua');
        if (respond.type == 'error') {
            return {
                status: 'error',
                message:
                    respond?.error?.response?.data?.error ??
                    respond?.error?.response?.data?.message ??
                    '',
            };
        }
        return {
            status: 'ok',
            message: 'Update Address Ok',
        };
    };

export type onLoadAddressByIdProps = (data: {
    user?: UserLoginProps;
    id?: string;
}) => Promise<DataManageAddress> | DataManageAddress;

export const onLoadAddressById: onLoadAddressByIdProps = async ({
    id,
    user,
}: {
    user?: UserLoginProps;
    id?: string;
}) => {
    log('Data onSaveInput', {
        user,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            id: '3',
            address: 'Test Address',
            apt: 'Test Apt',
            city: 'Test City',
            location: 'Test Address',
            zipcode: 'Test ZipCode',
        };
    }
    const respond = await request({
        method: 'get',
        url: `${URL}/client/address?uuid=${id}`,
        headers: {
            authorization: user?.token,
        },
    });
    log('respond onLoadAddress', respond, 'aqua');
    if (respond.type == 'error') {
        throw {
            message: '',
        };
    }

    return (respond.result?.data ?? []).map((a: any) => {
        const aR: DataManageAddress = {
            id: a.uuid,
            address: a.address,
            city: a.city,
            apt: a.apt,
            location: a.name_location,
            zipcode: a.zip_code,
        };
        return aR;
    })[0];
};
