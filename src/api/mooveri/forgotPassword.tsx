import { onSubmintForgotPassword } from '@/components/Form/ForgotPassword/Base';
import log from '@/functions/log';

import { DataForgotPassword } from '@/interfaces/ForgotPassword';

import { request } from '@/api/request';

import { URL } from '@/api/mooveri/_';

export const ForgotPassword: onSubmintForgotPassword = async (
    data: DataForgotPassword
) => {
    log('DataForgotPassword', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'ForgotPassword ok',
        };
    }

    const respond = await request({
        method: 'post',
        url: `${URL}/auth/recovery_password`,
        data: {
            email: data.email,
            type: data.type,
        },
    });
    log('respond login', respond, 'aqua');
    if (respond.type == 'error') {
        throw {
            message: 'Upps! No se pudo Enviar el Correo',
        };
    }
    return {
        status: 'ok',
        message: 'Email Send',
    };
};
