import { InputTelValue } from '@/components/Input/Tel/Base';
import { UserLoginProps } from '@/hook/useUser';
import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';

export type ConfirmPhoneProps = (data: {
    tel: InputTelValue;
    user: UserLoginProps;
}) => Promise<SubmitResult> | SubmitResult;

export const ConfirmPhone: ConfirmPhoneProps = async (data: {
    tel: InputTelValue;
    user: UserLoginProps;
}) => {
    log('ConfirmPhone Data', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'SMS Send',
        };
    }
    return {
        status: 'ok',
        message: 'SMS Send',
    };
};

export type ConfirmPhoneCodeProps = (data: {
    phone: string;
    code: string;
}) => Promise<SubmitResult> | SubmitResult;

export const ConfirmPhoneCode: ConfirmPhoneCodeProps = async (data: {
    phone: string;
    code: string;
}) => {
    log('ConfirmPhoneCode Data', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Code Ok',
        };
    }
    return {
        status: 'ok',
        message: 'Code Ok',
    };
};
