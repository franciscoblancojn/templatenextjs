import { onSubmintChangePassword } from '@/components/Form/ChangePassword/Base';
import { LoadPageFunctionRespond } from '@/components/Render';
import log from '@/functions/log';

import { DataChangePassword } from '@/interfaces/ChangePassword';
import { request } from '@/api/request';

import { URL } from '@/api/mooveri/_';

export const ChangePassword: onSubmintChangePassword = async (
    data: DataChangePassword
) => {
    log('DataChangePassword', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Change Password ok',
        };
    }
    const url = data.type == 'client' ? '/client/pass' : '/user_company/pass';

    const respond = await request({
        method: 'put',
        url: `${URL}${url}`,
        headers: {
            authorization: data.token,
        },
        data: {
            password: data.password,
        },
    });
    log('respond ChangePassword', respond, 'aqua');
    if (respond.type == 'error') {
        throw {
            message:
                respond?.error?.response?.data?.message?.meta?.cause ??
                respond?.error?.response?.data?.message ??
                'No se pudo cambiar la contraseña',
        };
    }

    return {
        status: 'ok',
        message: respond.result?.data,
    };
};

export type ChangePasswordValidateTokenProps = (data: {
    token: string;
}) => Promise<LoadPageFunctionRespond> | LoadPageFunctionRespond;

export const ChangePasswordValidateToken: ChangePasswordValidateTokenProps =
    async ({ token }: { token: string }) => {
        log('ChangePasswordValidateToken', token);
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: LoadPageFunctionRespond = 'ok';
            return r;
        }

        const r: LoadPageFunctionRespond = 'ok';
        return r;
    };
