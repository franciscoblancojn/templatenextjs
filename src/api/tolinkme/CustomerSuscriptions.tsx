import { SubmitResult } from '@/components/Form/Base';
import { UserLoginProps } from '@/hook/useUser';
import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';
import { parseIntPriceToNumber } from '@/functions/moneyPay';
import {
    ButtonsDataProps,
    ButtonsPurchaseDataProps,
    TolikmeButtonCustomerDataProps,
} from '@/components/Pages/MySubscriptions/index/content';
import log from '@/functions/log';

//Get subscriptions
export type GET_CUSTOMER_SUBCRIPTION_INTERFACE = (data: {
    user: UserLoginProps;
}) => Promise<SubmitResult<TolikmeButtonCustomerDataProps> | 401>;

export const GET_CUSTOMER_SUBCRIPTION: GET_CUSTOMER_SUBCRIPTION_INTERFACE =
    async ({ user }) => {
        const result = await request({
            url: `${URL}/subscription/user/customer/${user?.id}`,
            method: 'get',
            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
        });
        if (result.type == 'ok') {
            const r: SubmitResult<TolikmeButtonCustomerDataProps> = {
                status: 'ok',
                data: {
                    btns:
                        result?.result?.data?.getSubscriptionsByCustomerUser?.map(
                            (btn: any) => {
                                const r: ButtonsDataProps = {
                                    TransactionSubscriptionButtons: {
                                        Button: {
                                            buttons_model_profile: {
                                                model_profile: {
                                                    profile_img:
                                                        btn
                                                            ?.TransactionSubscriptionButtons
                                                            ?.Button
                                                            ?.buttons_model_profile
                                                            ?.model_profile
                                                            ?.profile_img ?? '',
                                                    user_ModelProfile: {
                                                        user: {
                                                            username:
                                                                btn
                                                                    ?.TransactionSubscriptionButtons
                                                                    ?.Button
                                                                    ?.buttons_model_profile
                                                                    ?.model_profile
                                                                    ?.user_ModelProfile
                                                                    ?.user
                                                                    ?.username ??
                                                                '',
                                                        },
                                                    },
                                                },
                                            },
                                            ButtonPriceDetails: {
                                                basePrice: `${parseIntPriceToNumber(
                                                    btn
                                                        ?.TransactionSubscriptionButtons
                                                        ?.Button
                                                        ?.ButtonPriceDetails
                                                        ?.basePrice ?? 0
                                                )}`,
                                                period:
                                                    btn
                                                        ?.TransactionSubscriptionButtons
                                                        ?.Button
                                                        ?.ButtonPriceDetails
                                                        ?.period ?? '',
                                            },
                                            logo:
                                                btn
                                                    ?.TransactionSubscriptionButtons
                                                    ?.Button?.logo ?? '',
                                            title:
                                                btn
                                                    ?.TransactionSubscriptionButtons
                                                    ?.Button?.title ?? '',
                                            url:
                                                btn
                                                    ?.TransactionSubscriptionButtons
                                                    ?.Button.url ?? '',
                                            uuid: btn
                                                .TransactionSubscriptionButtons
                                                ?.Button?.uuid,
                                        },
                                        Status: btn
                                            .TransactionSubscriptionButtons
                                            .Status,
                                        uuid: btn.TransactionSubscriptionButtons
                                            .uuid,
                                    },
                                };
                                return r;
                            }
                        ) ?? [],
                },
            };
            return r;
        }
        return 401;
    };

//Get Compras
export type GET_CUSTOMER_SHOPPING_INTERFACE = (data: {
    user: UserLoginProps;
}) => Promise<SubmitResult<TolikmeButtonCustomerDataProps> | 401>;

export const GET_CUSTOMER_SHOPPING: GET_CUSTOMER_SHOPPING_INTERFACE = async ({
    user,
}) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));

        const r: SubmitResult = {
            status: 'ok',
            message: 'My Account Delete',
            data: {
                uniquerPurchase: [
                    {
                        Button: {
                            logo: 'https://firebasestorage.googleapis.com/v0/b/linkme-f37f5.appspot.com/o/localhost%2Ffiles%2FPodcast-tools%201.png?alt=media&token=6e861e3f-f809-434a-a465-07fda8816b6f',
                            buttons_model_profile: {
                                model_profile: {
                                    profile_img:
                                        'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F110227607_1165145000509177_4569972509399988220_n.jpg?alt=media&token=2874d280-aa9b-4baf-aa02-b30a247ac8df',
                                    user_ModelProfile: {
                                        user: {
                                            username: 'Juan',
                                        },
                                    },
                                },
                            },
                            ButtonPriceDetails: {
                                basePrice: '20',
                                period: 'Mes',
                            },
                            title: 'Custom',
                        },
                    },
                    {
                        Button: {
                            logo: 'https://firebasestorage.googleapis.com/v0/b/linkme-f37f5.appspot.com/o/localhost%2Ffiles%2FPodcast-tools%201.png?alt=media&token=6e861e3f-f809-434a-a465-07fda8816b6f',
                            buttons_model_profile: {
                                model_profile: {
                                    profile_img:
                                        'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F110227607_1165145000509177_4569972509399988220_n.jpg?alt=media&token=2874d280-aa9b-4baf-aa02-b30a247ac8df',
                                    user_ModelProfile: {
                                        user: {
                                            username: 'Juan',
                                        },
                                    },
                                },
                            },
                            ButtonPriceDetails: {
                                basePrice: '20',
                                period: 'Mes',
                            },
                            title: 'Custom',
                        },
                    },
                    {
                        Button: {
                            logo: 'https://firebasestorage.googleapis.com/v0/b/linkme-f37f5.appspot.com/o/localhost%2Ffiles%2FPodcast-tools%201.png?alt=media&token=6e861e3f-f809-434a-a465-07fda8816b6f',
                            buttons_model_profile: {
                                model_profile: {
                                    profile_img:
                                        'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F110227607_1165145000509177_4569972509399988220_n.jpg?alt=media&token=2874d280-aa9b-4baf-aa02-b30a247ac8df',
                                    user_ModelProfile: {
                                        user: {
                                            username: 'Juan',
                                        },
                                    },
                                },
                            },
                            ButtonPriceDetails: {
                                basePrice: '20',
                                period: 'Mes',
                            },
                            title: 'Custom',
                        },
                    },
                ],
            },
        };
        return r;
    }
    const result = await request({
        url: `${URL}/subscription/customer/purchase/${user?.id}`,
        method: 'get',
        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
    });
    if (result.type == 'ok') {
        const r: SubmitResult<TolikmeButtonCustomerDataProps> = {
            status: 'ok',
            data: {
                uniquerPurchase:
                    result?.result?.data?.uniquerPurchase?.map(
                        (btnPurchase: any) => {
                            const r: ButtonsPurchaseDataProps = {
                                Button: {
                                    uuid: btnPurchase?.Button?.uuid,
                                    buttons_model_profile: {
                                        model_profile: {
                                            profile_img:
                                                btnPurchase?.Button
                                                    ?.buttons_model_profile
                                                    ?.model_profile
                                                    ?.profile_img ?? '',
                                            user_ModelProfile: {
                                                user: {
                                                    username:
                                                        btnPurchase?.Button
                                                            ?.buttons_model_profile
                                                            ?.model_profile
                                                            ?.user_ModelProfile
                                                            ?.user?.username ??
                                                        '',
                                                },
                                            },
                                        },
                                    },
                                    ButtonPriceDetails: {
                                        basePrice: `${parseIntPriceToNumber(
                                            btnPurchase?.Button
                                                ?.ButtonPriceDetails
                                                ?.basePrice ?? 0
                                        )}`,
                                        period:
                                            btnPurchase?.Button
                                                ?.ButtonPriceDetails?.period ??
                                            '',
                                    },
                                    logo: btnPurchase?.Button?.logo ?? '',
                                    title: btnPurchase?.Button?.title ?? '',
                                    url: btnPurchase?.Button.url ?? '',
                                },
                            };
                            return r;
                        }
                    ) ?? [],
            },
        };
        return r;
    }
    return 401;
};

export type DELETE_SUBCRIPTION_ID_INTERFACE = (data: {
    user: UserLoginProps | undefined;
    uuid: string;
}) => Promise<SubmitResult<ButtonsDataProps>>;

export const DELETE_SUBCRIPTION_ID: DELETE_SUBCRIPTION_ID_INTERFACE = async ({
    uuid,
    user,
}) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: ButtonsDataProps = {};
        return {
            status: 'ok',
            message: '',
            data: r,
        };
    }
    const result = await request({
        url: `${URL}/subscription/${uuid}`,
        method: 'delete',
        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
        data: {},
    });
    if (result.type == 'ok') {
        const r: ButtonsDataProps = {};
        log('', r);

        return {
            status: 'ok',
            message: 'Delete Subscription',
        };
    }
    return {
        status: 'error',
        message: 'Error delete',
    };
};
