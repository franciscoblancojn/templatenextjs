import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';

import { onSubmintChangePassword } from '@/components/Form/ChangePassword/Base';
import { LoadPageFunctionRespond } from '@/components/Render';
import log from '@/functions/log';

import { DataChangePassword } from '@/interfaces/ChangePassword';

export const ChangePassword: onSubmintChangePassword = async (
    data: DataChangePassword
) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Change Password ok',
        };
    }
    const result = await request({
        method: 'post',
        url: `${URL}/auth/recovery-password/${encodeURIComponent(
            data.token ?? ''
        )}`,
        data: {
            password: data.password,
        },
    });

    log('Send change password', result, 'yellow');

    if (result.type == 'error') {
        throw {
            message: result.error.response.data.data,
        };
    }

    return {
        status: 'ok',
        message: 'Password Update',
    };
};

export type ChangePasswordValidateTokenProps = (data: {
    token: string;
}) => Promise<LoadPageFunctionRespond> | LoadPageFunctionRespond;

export const ChangePasswordValidateToken: ChangePasswordValidateTokenProps =
    async ({ token }: { token: string }) => {
        log('ChangePasswordValidateToken', token);
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: LoadPageFunctionRespond = 'ok';
            return r;
        }
        const result = await request({
            url: `${URL}/change-password/validate-token`,
        });
        log('result ChangePasswordValidateToken', result);
        const r: LoadPageFunctionRespond = 'ok';
        return r;
    };
