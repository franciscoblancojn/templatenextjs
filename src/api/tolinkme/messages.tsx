import { request } from '@/api/request';
import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';
import { URL } from '@/api/tolinkme/_';
import { UserLoginProps } from '@/hook/useUser';
import {
    MessagesData,
    MessageUser as MessageUserDataProps,
    MessageUserData,
} from '@/interfaces/message';

export type POST_MESSAGESINTERFACE = (data: {
    data: MessageUserDataProps;
    user: UserLoginProps | undefined;
}) => Promise<SubmitResult>;

export const POST_MESSAGES: POST_MESSAGESINTERFACE = async ({ data, user }) => {
    log('POST_MESSAGES profile', data, 'aqua');
    if (!user) {
        const r: SubmitResult = {
            status: 'error',
            message: 'User is not found',
        };
        return r;
    }
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
            message: 'Profile Publish',
        };
        return r;
    }

    const result = await request({
        method: 'post',
        url: `${URL}/service/messages`,
        headers: {
            'Content-Type': 'application/json',
        },
        data: {
            message_details: {
                text: data?.message_details?.message ?? null,
                email: data?.message_details?.email ?? null,
                phone: data?.message_details?.phone ?? null,
                name: data?.message_details?.name ?? null,
            },
            user_details: {
                core_uuid: data?.user_details?.core_uuid,
            },
        },
    });

    if (result.type == 'ok') {
        const r: SubmitResult = {
            status: 'ok',
            message: 'Send Message',
        };
        return r;
    }
    if (result.type == 'error') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Error Send Message',
        };
        return r;
    }
    const r: SubmitResult = {
        status: 'ok',
        message: 'Send Message',
    };
    return r;
};

export type GET_MESSAGES_INTERFACE = (data: {
    user: UserLoginProps;
}) => Promise<SubmitResult<MessageUserData> | 401>;

export const GET_MESSAGES: GET_MESSAGES_INTERFACE = async ({ user }) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));

        const r: SubmitResult = {
            status: 'ok',
            message: '',
            data: {},
        };
        return r.data;
    }
    const result = await request({
        url: `${URL}/service/messages/user/${user?.id}`,
        method: 'get',
        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
    });
    if (result.type == 'ok') {
        const r: SubmitResult<MessageUserData> = {
            status: 'ok',
            data: {
                Messages:
                    Object.values(result?.result?.data ?? {})
                        ?.map((message: any) => {
                            const r: MessagesData = {
                                uuid: message?.uuid,
                                createdAt: message?.createdAt,
                                updatedAt: message?.updatedAt,
                                status: message?.status,
                                Messages: [
                                    {
                                        createdAt:
                                            message?.Messages?.[0]?.createdAt,
                                        updatedAt:
                                            message?.Messages?.[0]?.updatedAt,
                                        email: message?.Messages?.[0]?.email,
                                        name: message?.Messages?.[0]?.name,
                                        phone: message?.Messages?.[0]?.phone,
                                        text: message?.Messages?.[0]?.text,
                                        roomCaseUuid:
                                            message?.Messages?.[0]
                                                ?.roomCaseUuid,
                                        uuid: message?.Messages?.[0]?.uuid,
                                    },
                                ],
                            };
                            return r;
                        })
                        .filter(
                            (mes: any) =>
                                mes.uuid !== undefined && mes.uuid !== null
                        ) ?? [],
                count: result?.result?.data.count ?? 0,
                totalCount: result?.result?.data.totalCount,
            },
        };
        return r.data;
    }
    return 401;
};

export type PUT_MESSAGES_SEEN_INTERFACE = (data: {
    uuid_message: string;
    status?: string;
    data: MessagesData;
}) => Promise<SubmitResult>;

export const PUT_MESSAGES_SEEN: PUT_MESSAGES_SEEN_INTERFACE = async ({
    uuid_message,
    data,
    status,
}) => {
    log('PUT_MESSAGES_SEEN profile', data, 'aqua');

    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
            message: 'Message seen',
        };
        return r;
    }

    const result = await request({
        method: 'put',
        url: `${URL}/service/messages/${uuid_message}`,
        headers: {
            'Content-Type': 'application/json',
        },
        data: {
            status,
        },
    });

    if (result.type == 'ok') {
        const r: SubmitResult = {
            status: 'ok',
            message: 'Message seen',
        };
        return r;
    }
    if (result.type == 'error') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Error Message seen',
        };
        return r;
    }
    const r: SubmitResult = {
        status: 'ok',
        message: 'Message seen',
    };
    return r;
};
