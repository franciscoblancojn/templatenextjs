import { SubmitResult } from '@/components/Form/Base';

import log from '@/functions/log';
import { DataFormMonetize } from '@/interfaces/FormMonetize';

import { UserLoginProps } from '@/hook/useUser';
import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';
import { TolinkmePayProps } from '@/components/Pages/Pay/content';
import { parseIntPriceToNumber } from '@/functions/moneyPay';
import {
    SubscriptableTransactionsDataProps,
    TolinkmeButtonsDataProps,
    TolinkmeButtonsProps,
} from '@/components/CurrentBalance/Base';

export type POST_BUTTON_MONETIZEINTERFACE = (data: {
    data: DataFormMonetize;
    btn_uuid: string;
    user: UserLoginProps | undefined;
}) => Promise<SubmitResult>;

export const POST_BUTTON_MONETIZE: POST_BUTTON_MONETIZEINTERFACE = async ({
    data,
    btn_uuid,
    user,
}) => {
    log('put data profile', data, 'aqua');
    if (!user) {
        const r: SubmitResult = {
            status: 'error',
            message: 'User is not found',
        };
        return r;
    }
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
            message: 'Profile Publish',
        };
        return r;
    }

    const result = await request({
        method: 'post',
        url: `${URL}/buttons/monetize/${btn_uuid}`,
        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
        data: {
            recurrence: data.recurrent?.toUpperCase(),
            period: data.period?.toUpperCase(),
            basePrice: parseFloat(`${data.amount ?? 0}`),
            type: data.type?.toUpperCase(),
            description: data?.description,
        },
    });
    if (result.type == 'ok') {
        const r: SubmitResult = {
            status: 'ok',
            message: 'Monetized button created',
        };
        return r;
    }
    const r: SubmitResult = {
        status: 'ok',
        message: 'Monetized button created',
    };
    return r;
};

//
export type GET_BUTTONS_MONETIZE_INTERFACE = (data: {
    btn_uuid: string;
}) => Promise<SubmitResult<TolinkmePayProps> | 401>;

export const GET_BUTTONS_MONETIZE: GET_BUTTONS_MONETIZE_INTERFACE = async ({
    btn_uuid,
}) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult<TolinkmePayProps> = {
            status: 'ok',
        };
        return r;
    }

    const result = await request({
        url: `${URL}/buttons/${btn_uuid}`,
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
        },
    });
    if (result.type == 'ok') {
        const data = result?.result?.data;
        //const ButtonPriceDetails = data?.ButtonPriceDetails;

        const r: SubmitResult<TolinkmePayProps> = {
            status: 'ok',
            data: {
                amount: `${parseIntPriceToNumber(
                    data?.ButtonPriceDetails?.basePrice
                )}`,
                recurrence: data?.ButtonPriceDetails.recurrence,
                period: data?.ButtonPriceDetails?.period,
                description: data?.ButtonPriceDetails?.description,
                userImg:
                    result?.result?.data.buttons_model_profile?.model_profile
                        ?.profile_img,
                logoIcon: result?.result?.data.logo,
                username:
                    result?.result?.data.buttons_model_profile?.model_profile
                        ?.user_ModelProfile?.user.username,
                style: {
                    description: '',
                    name: '',
                    web: '',
                    btnBorderColor:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.btnBorderColor,
                    btnBgColor:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.btnBgColor,
                    btnColor:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.btnColor,
                    buttonHeight:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.buttonHeight ?? 'regular',
                    buttonRound:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.buttonRound ?? 'semi-rounded',
                    showIconButton:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.showIconButton ?? true,
                    fontColor:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.text_color ?? '',
                    bgDesktop:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.banner ?? '',
                    capaColor:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.capaColor ?? '',
                    bgMovil:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.banner_mobile ?? '',
                    tolinkmeLogo:
                        result?.result?.data.buttons_model_profile
                            ?.model_profile?.tolinkmeLogo ?? false,
                    btn: {
                        size:
                            result?.result?.data.buttons_model_profile
                                ?.model_profile?.buttonHeight ?? 'regular',

                        borderRadius:
                            result?.result?.data.buttons_model_profile
                                ?.model_profile?.buttonRound ?? 'semi-rounded',

                        background: {
                            type:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBgType ?? 'color',
                            color:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBgColor ?? '#ffffff',
                            gradient: {
                                color1:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile?.btnBgGradientColor1 ??
                                    '#ffffff',
                                color2:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile?.btnBgGradientColor2 ??
                                    '#ffffff',
                                deg:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile?.btnBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile?.btnBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile?.btnBgVideo ?? '',
                                text: '',
                            },
                            opacity:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBgOpacity ?? 100,
                        },
                        border: {
                            size:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBorderSize ?? 0,
                            color:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBorderColor ??
                                '#ffffff',
                            type:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBorderType ?? 'solid',
                        },
                        text: {
                            fontFamily:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnTextFontFamily ??
                                'nunito',
                            fontWeight:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnTextFontWeight ??
                                'black',
                            fontSize:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnTextFontSize ?? 17,
                            lineHeight:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnTextLineHeight ?? 10,
                            color:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnColor ?? '#8d8d8d',
                        },
                        icon:
                            result?.result?.data.buttons_model_profile
                                ?.model_profile?.showIconButton ?? true
                                ? 'con'
                                : 'sin',
                        iconConfig: {
                            size: Math.min(
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnIconConfigSize ?? 27,
                                40
                            ),
                            padding:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnIconConfigPadding ?? 0,

                            borderRadius:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnIconConfigRound ??
                                'no-rounding',
                            border: {
                                size:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile
                                        ?.btnIconConfigBorderSize ?? 0,
                                color:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile
                                        ?.btnIconConfigBorderColor ?? '#ffffff',
                                type:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile
                                        ?.btnIconConfigBorderType ?? 'solid',
                            },

                            background: {
                                type:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile?.btnIconConfigBgType ??
                                    'gradient',
                                color:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile?.btnIconConfigBgColor ??
                                    '#ffffff',
                                gradient: {
                                    color1:
                                        result?.result?.data
                                            .buttons_model_profile
                                            ?.model_profile
                                            ?.btnIconConfigBgGradientColor1 ??
                                        '#04506c',
                                    color2:
                                        result?.result?.data
                                            .buttons_model_profile
                                            ?.model_profile
                                            ?.btnIconConfigBgGradientColor2 ??
                                        '#9d016e',
                                    deg:
                                        result?.result?.data
                                            .buttons_model_profile
                                            ?.model_profile
                                            ?.btnIconConfigBgGradientDeg ?? 130,
                                },
                                img: {
                                    fileData:
                                        result?.result?.data
                                            .buttons_model_profile
                                            ?.model_profile
                                            ?.btnIconConfigBgImg ?? '',
                                    text: '',
                                },
                                video: {
                                    fileData:
                                        result?.result?.data
                                            .buttons_model_profile
                                            ?.model_profile
                                            ?.btnIconConfigBgVideo ?? '',
                                    text: '',
                                },
                                opacity:
                                    result?.result?.data.buttons_model_profile
                                        ?.model_profile
                                        ?.btnIconConfigBgOpacity ?? 0,
                            },
                        },

                        boxShadow: {
                            type:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBoxShadowType ??
                                'normal',
                            blur:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBoxShadowBlur ?? 0,
                            size:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBoxShadowSize ?? 0,
                            x:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBoxShadowX ?? 0,
                            y:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBoxShadowY ?? 0,
                            color:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnBoxShadowColor ??
                                '#0c0c0c',
                        },
                    },
                    btnPrincipal: {
                        size: Math.max(
                            result?.result?.data.buttons_model_profile
                                ?.model_profile?.btnPrincipalSize ?? 10,
                            15
                        ),
                        padding:
                            result?.result?.data.buttons_model_profile
                                ?.model_profile?.btnPrincipalPadding ?? 10,
                        color:
                            result?.result?.data.buttons_model_profile
                                ?.model_profile?.btnPrincipalColor ?? '#ffffff',

                        borderRadius:
                            result?.result?.data.buttons_model_profile
                                ?.model_profile?.btnPrincipalRound ?? 'rounded',
                        border: {
                            size:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnPrincipalBorderSize ??
                                0,
                            color:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnPrincipalBorderColor ??
                                '#ffffff',
                            type:
                                result?.result?.data.buttons_model_profile
                                    ?.model_profile?.btnPrincipalBorderType ??
                                'solid',
                        },
                    },
                },
                btn_uuid,
            },
        };

        return r;
    }
    return 401;
};

//
export type GET_BUTTONS_SUBCRIPTION_USER_ID_INTERFACE = (data: {
    user: UserLoginProps;
}) => Promise<SubmitResult<TolinkmeButtonsProps> | 401>;

export const GET_BUTTONS_SUBCRIPTION_USER_ID: GET_BUTTONS_SUBCRIPTION_USER_ID_INTERFACE =
    async ({ user }) => {
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: SubmitResult<TolinkmeButtonsProps> = {
                status: 'ok',
            };
            return r;
        }
        const result = await request({
            url: `${URL}/subscription/user/${user?.id}`,
            method: 'get',
            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
        });
        if (result.type == 'ok') {
            const r: SubmitResult<TolinkmeButtonsProps> = {
                status: 'ok',
                data: {
                    btns:
                        result?.result?.data?.getSubscriptionsByModelUser?.map(
                            (btn: any) => {
                                const r: TolinkmeButtonsDataProps = {
                                    url: btn?.url ?? '',
                                    uuid: btn?.uuid ?? '',
                                    title: btn?.title ?? '',
                                    logo: btn?.logo ?? '',
                                    ButtonPriceDetails: {
                                        basePrice: `${parseIntPriceToNumber(
                                            btn?.ButtonPriceDetails
                                                ?.basePrice ?? 0
                                        )}`,
                                        period:
                                            btn?.ButtonPriceDetails?.period ??
                                            '',
                                    },
                                    SubscriptableTransactions:
                                        btn?.TransactionBuyButtons?.map(
                                            (suscriptable: any) => {
                                                const r: SubscriptableTransactionsDataProps =
                                                    {
                                                        status:
                                                            suscriptable?.Status ??
                                                            '',
                                                        uuid:
                                                            suscriptable?.uuid ??
                                                            '',
                                                        user: {
                                                            email:
                                                                suscriptable
                                                                    ?.User
                                                                    ?.email ??
                                                                '',
                                                            uuid:
                                                                suscriptable
                                                                    ?.User
                                                                    ?.uuid ??
                                                                '',
                                                        },
                                                    };
                                                return r;
                                            }
                                        ) ?? [],
                                };
                                return r;
                            }
                        ) ?? [],
                    getCurrentBalance: {
                        total: `${parseIntPriceToNumber(
                            result?.result?.data?.getCurrentBalance?.total ?? 0
                        )}`,
                    },
                    currentMounthAmounthBalance: `${parseIntPriceToNumber(
                        result?.result?.data?.currentMounthAmounthBalance ?? 0
                    )}`,
                },
            };

            return r;
        }
        return 401;
    };
