import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';

import { InputTelValue } from '@/components/Input/Tel/Base';
import { UserLoginProps } from '@/hook/useUser';
import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';

export type ConfirmPhoneProps = (data: {
    tel: InputTelValue;
    user: UserLoginProps;
}) => Promise<SubmitResult> | SubmitResult;

export const ConfirmPhone: ConfirmPhoneProps = async (data: {
    tel: InputTelValue;
    user: UserLoginProps;
}) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'SMS Send',
        };
    }
    const result = await request({
        method: 'post',
        url: `${URL}/auth/generate-otp`,
        data: {
            user_uuid: data.user.id,
            phone: `${data.tel.code}-${data.tel.tel}`,
        },
    });

    if (result.type == 'error') {
        throw {
            message: result.error.response.data.data,
        };
    }

    return {
        status: 'ok',
        message: 'SMS Send',
    };
};

export type ConfirmPhoneCodeProps = (data: {
    phone: string;
    code: string;
    user: UserLoginProps;
}) => Promise<SubmitResult> | SubmitResult;

export const ConfirmPhoneCode: ConfirmPhoneCodeProps = async (data: {
    phone: string;
    code: string;
    user: UserLoginProps;
}) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Code Ok',
        };
    }
    const result = await request({
        method: 'post',
        url: `${URL}/auth/verify-otp`,
        data: {
            phone: data.phone,
            code: data.code,
            user_uuid: data.user.id,
        },
    });
    log('Send code phone', result, 'yellow');

    if (result.type == 'error') {
        const n = result.error.response.data.data;
        throw {
            message: typeof n == 'string' ? n : JSON.stringify(n),
        };
    }
    return {
        status: 'ok',
        message: 'Code Ok',
    };
};
