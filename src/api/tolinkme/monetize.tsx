import { SubmitResult } from '@/components/Form/Base';

import log from '@/functions/log';

import { URL } from '@/api/tolinkme/_';
import { UserLoginProps } from '@/hook/useUser';
import { MonetizeDataProps as ContentMonetizeDataProps } from '@/interfaces/MonetizeDataProps';
import { request } from '@/api/request';
import { InformationSubmittedStatusType } from '@/interfaces/InformationSubmittedStatusType';
import { ProfileData } from '@/interfaces/ProfileData';

export type GET_MONETIZEINTERFACE = (data: {
    user: UserLoginProps | undefined;
}) => Promise<ProfileData['monetize'] | 401>;

export const GET_MONETIZE: GET_MONETIZEINTERFACE = async ({ user }) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: ProfileData['monetize'] = {
            defaultValue: {
                isCreate: true,
                step1: {
                    fullName: 'test',
                    lastName: 'tets2',
                    country: {
                        text: 'Andorra',
                    },
                    state: {
                        text: 'La Massana',
                    },
                    Street: '6 Carrer Major',
                    city: {
                        text: 'La Massana',
                    },
                    confirm18: true,
                    zipPostalCode: '1213442',
                },
                step2: {
                    FrontIdentification: {
                        fileData:
                            'https://www.kasandbox.org/programming-images/avatars/leaf-blue.png',
                    },
                    LaterIdentification: {
                        fileData:
                            'https://www.kasandbox.org/programming-images/avatars/leaf-blue.png',
                    },
                },
                step3: {
                    avatar: 'https://www.kasandbox.org/programming-images/avatars/leaf-blue.png',
                },
                step4: {
                    categories: [
                        {
                            id: 'model',
                            name: '👚Model',
                            active: true,
                        },
                    ],
                    confirm: true,
                    privacyPolicy: true,
                },
            },
            statusSteps: {
                step1: 'Verified',
                step2: 'Verified',
                step3: 'Verified',
            },
            balance: {
                currentBalanceMoney: 20,
                currentPendingMoney: 20,
                BalanceMoneyMounth: 220,
                TotalButtonMonetize: 20,
                AmountSuscription: 20,
            },
        };
        return r;
    }

    try {
        const result = await request({
            url: `${URL}/monetize/fullfillment_status/${user?.id}`,
            method: 'get',
            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
        });
        if (result.type == 'ok') {
            const User_Personal_data_status =
                result?.result?.data?.User_Personal_data?.STATUS ?? 'PENDING';
            const User_Personal_data =
                result?.result?.data?.User_Personal_data ?? null;

            const UserDocumentationDNIBack =
                result?.result?.data?.UserDocumentation?.DNIBackSTATUS ??
                'PENDING';
            const UserDocumentationDNIFront =
                result?.result?.data?.UserDocumentation?.DNIFrontSTATUS ??
                'PENDING';
            const UserDocumentation =
                result?.result?.data?.UserDocumentation ?? null;

            const UserValidation = result?.result?.data?.UserValidation ?? null;
            const UserValidationStatus =
                result?.result?.data?.UserValidation?.STATUS ?? 'PENDING';

            let step1Status: InformationSubmittedStatusType = 'Without_Sending';
            let step2Status: InformationSubmittedStatusType = 'Without_Sending';
            let step3Status: InformationSubmittedStatusType = 'Without_Sending';

            if (User_Personal_data_status == 'APROBED') {
                step1Status = 'Verified';
            }
            if (User_Personal_data_status == 'PENDING') {
                step1Status = 'In_Review';
            }

            if (User_Personal_data == null) {
                step1Status = 'Without_Sending';
            }

            if (
                UserDocumentationDNIBack == 'APROBED' &&
                UserDocumentationDNIFront == 'APROBED'
            ) {
                step2Status = 'Verified';
            }

            if (
                UserDocumentationDNIBack == 'PENDING' &&
                UserDocumentationDNIFront == 'PENDING'
            ) {
                step2Status = 'In_Review';
            }

            if (UserDocumentation == null && UserDocumentation == null) {
                step2Status = 'Without_Sending';
            }

            if (UserValidation === null) {
                step3Status = 'Without_Sending';
            } else if (UserValidationStatus === 'APROBED') {
                step3Status = 'Verified';
            } else if (UserValidationStatus === 'PENDING') {
                step3Status = 'In_Review';
            }

            const r: ProfileData['monetize'] = {
                defaultValue: {
                    isCreate: result?.result?.data?.User_Personal_data != null,
                    step1: {
                        fullName:
                            result?.result?.data?.User_Personal_data
                                ?.fullName ?? '',
                        lastName:
                            result?.result?.data?.User_Personal_data
                                ?.lastName ?? '',
                        country: {
                            text:
                                result?.result?.data?.User_Personal_data
                                    ?.country ?? '',
                        },
                        state: {
                            text:
                                result?.result?.data?.User_Personal_data
                                    ?.state ?? '',
                        },
                        Street:
                            result?.result?.data?.User_Personal_data?.Street ??
                            '',
                        city: {
                            text:
                                result?.result?.data?.User_Personal_data
                                    ?.city ?? '',
                        },
                        confirm18:
                            result?.result?.data?.User_Personal_data
                                ?.confirm18 ?? false,
                        zipPostalCode:
                            result?.result?.data?.User_Personal_data
                                ?.zipPostalCode ?? '',
                    },
                    step2: {
                        FrontIdentification: {
                            fileData:
                                result?.result?.data?.UserDocumentation
                                    ?.FrontIdentification ?? '',
                        },
                        LaterIdentification: {
                            fileData:
                                result?.result?.data?.UserDocumentation
                                    ?.LaterIdentification ?? '',
                        },
                    },
                    step3: {
                        avatar: {
                            fileData:
                                result?.result?.data?.UserValidation?.avatar ??
                                '',
                        },
                    },
                    step4: {
                        categories: [
                            /*
                            {
                                id: 'model',
                                name: '👚Model',
                                active: true,
                            },
                            */
                        ],
                    },
                },
                statusSteps: {
                    step1: step1Status,
                    step2: step2Status,
                    step3: step3Status,
                },
                balance: {
                    currentBalanceMoney: 200,
                    currentPendingMoney: 220,
                },
            };
            return r;
        }
        return 401;
    } catch (error) {
        console.log(error);
        return 401;
    }
};

export type POST_MONETIZEINTERFACE = (data: {
    data: ContentMonetizeDataProps;
    user: UserLoginProps | undefined;
}) => Promise<SubmitResult>;

export const POST_MONETIZE: POST_MONETIZEINTERFACE = async ({ data, user }) => {
    log('POST_MONETIZE profile', data, 'aqua');
    if (!user) {
        const r: SubmitResult = {
            status: 'error',
            message: 'User is not found',
        };
        return r;
    }
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
            message: 'Profile Publish',
        };
        return r;
    }

    const result = await request({
        method: 'post',
        url: `${URL}/monetize/fullfillment/${user?.id}`,

        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
        data: {
            personal_data: {
                fullFirstName: data.step1.fullName,
                fullLastName: data.step1.lastName,
                country: data.step1.country.text,
                state: data.step1.state.text,
                address: data.step1.Street,
                city: data.step1.city.text,
                postalCode: data.step1.zipPostalCode,
                adult: data.step1.confirm18,
            },
            user_documentation: {
                DNIFront: data.step2.FrontIdentification?.fileData,
                DNIBack: data.step2.LaterIdentification?.fileData,
            },
            user_validation: {
                Selfie: data.step3.avatar.fileData,
            },
            categories: {
                uuids: data.step4.categories.id,
            },
        },
    });

    if (result.type == 'ok') {
        const r: SubmitResult = {
            status: 'ok',
            message: 'Send Fullfilment',
        };
        return r;
    }
    if (result.type == 'error') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Error Fullfilment',
        };
        return r;
    }
    const r: SubmitResult = {
        status: 'ok',
        message: 'Send Fullfilment',
    };
    return r;
};

export interface PUT_MONETIZE_USER_PERSONAL_DATA_INTERFACEPROS {
    step1: ContentMonetizeDataProps['step1'];
    user: UserLoginProps | undefined;
}
export type PUT_MONETIZE_USER_PERSONAL_DATA_INTERFACE = (
    data: PUT_MONETIZE_USER_PERSONAL_DATA_INTERFACEPROS
) => Promise<SubmitResult>;
export const PUT_MONETIZE_USER_PERSONAL_DATA: PUT_MONETIZE_USER_PERSONAL_DATA_INTERFACE =
    async ({ step1, user }: PUT_MONETIZE_USER_PERSONAL_DATA_INTERFACEPROS) => {
        if (!user) {
            const r: SubmitResult = {
                status: 'error',
                message: 'User is not found',
            };
            return r;
        }
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: SubmitResult = {
                status: 'ok',
                message: 'Detalles personales actualizados',
            };
            return r;
        }
        const result = await request({
            method: 'put',
            url: `${URL}/monetize/fullfillment_user_personal_data/${user?.id}`,
            data: {
                fullFirstName: step1.fullName,
                fullLastName: step1.lastName,
                country: step1.country.text,
                state: step1.state.text,
                address: step1.Street,
                city: step1.city.text,
                postalCode: step1.zipPostalCode,
                adult: step1.confirm18,
            },
            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
        });
        log('put result profile', result, 'aqua');
        const r: SubmitResult = {
            status: 'ok',
            message: 'Publish Ok',
        };
        return r;
    };

export interface PUT_MONETIZE_USER_DOCUMENTATION_INTERFACEPROS {
    step2: ContentMonetizeDataProps['step2'];
    user: UserLoginProps | undefined;
}
export type PUT_MONETIZE_USER_DOCUMENTATION_INTERFACE = (
    data: PUT_MONETIZE_USER_DOCUMENTATION_INTERFACEPROS
) => Promise<SubmitResult>;

export const PUT_MONETIZE_USER_DOCUMENTATION: PUT_MONETIZE_USER_DOCUMENTATION_INTERFACE =
    async ({ step2, user }: PUT_MONETIZE_USER_DOCUMENTATION_INTERFACEPROS) => {
        if (!user) {
            const r: SubmitResult = {
                status: 'error',
                message: 'User is not found',
            };
            return r;
        }
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: SubmitResult = {
                status: 'ok',
                message: 'Profile Publish',
            };
            return r;
        }

        const result = await request({
            method: 'put',
            url: `${URL}/monetize/fullfillment_user_documentation/${user?.id}`,
            data: {
                DNIFront: step2.FrontIdentification?.fileData,
                DNIBack: step2.LaterIdentification?.fileData,
            },
            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
        });
        log('put result profile', result, 'aqua');

        const r: SubmitResult = {
            status: 'ok',
            message: 'Publish Ok',
        };
        return r;
    };

export interface PUT_MONETIZE_USER_VALIDATION_INTERFACEPROS {
    step3: ContentMonetizeDataProps['step3'];
    user: UserLoginProps | undefined;
}
export type PUT_MONETIZE_USER_VALIDATION_INTERFACE = (
    data: PUT_MONETIZE_USER_VALIDATION_INTERFACEPROS
) => Promise<SubmitResult>;
export const PUT_MONETIZE_USER_VALIDATION: PUT_MONETIZE_USER_VALIDATION_INTERFACE =
    async ({ step3, user }: PUT_MONETIZE_USER_VALIDATION_INTERFACEPROS) => {
        if (!user) {
            const r: SubmitResult = {
                status: 'error',
                message: 'User is not found',
            };
            return r;
        }
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: SubmitResult = {
                status: 'ok',
                message: 'Profile Publish',
            };
            return r;
        }
        const result = await request({
            method: 'put',
            url: `${URL}/monetize/fullfillment_user_validation/${user?.id}`,
            data: {
                Selfie: step3.avatar,
            },
            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
        });
        log('put result profile', result, 'aqua');
        const r: SubmitResult = {
            status: 'ok',
            message: 'Publish Ok',
        };
        return r;
    };
