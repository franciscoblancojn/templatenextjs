import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';

import { onSubmintNameCategory } from '@/components/Form/NameCategory/Base';
import log from '@/functions/log';

import { DataNameCategory } from '@/interfaces/NameCategory';

export const NameCategory: onSubmintNameCategory = async (
    data: DataNameCategory
) => {
    log('data NameCategory', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Save Name and Categories',
        };
    }
    const result = await request({
        url: `${URL}/login`,
    });
    log('result NameCategory', result);
    return {
        status: 'ok',
        message: 'Save Name and Categories',
    };
};
