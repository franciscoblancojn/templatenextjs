//export const URLPRO = 'https://6bc0-190-165-100-93.ngrok-free.app/api/v1';

//export const URLDEV = 'https://6bc0-190-165-100-93.ngrok-free.app/api/v1';

export const URLPRO = 'https://backends.tolnk.me/api/v1';

export const URLDEV = 'https://backends.tolnk.me/api/v1';

//export const URLPRO = 'https://stagingback.tolinkme.com/api/v1';

//export const URLDEV = 'https://stagingback.tolinkme.com/api/v1';

export const URL = process.env['NEXT_PUBLIC_MODE'] == 'DEV' ? URLDEV : URLPRO;
