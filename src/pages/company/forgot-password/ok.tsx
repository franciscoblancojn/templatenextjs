import { PageSendEmail } from '@/components/Pages/SendEmail';

const SendEmail = () => {
    return (
        <PageSendEmail
            title="Succesfully Sent"
            text="Please check your email to recover your password"
            company={true}
        />
    );
};

export default SendEmail;
