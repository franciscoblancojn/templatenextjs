import { PageForgotPassword } from '@/components/Pages/ForgotPassword';

export const ForgotPassword = () => {
    return <PageForgotPassword company={true} />;
};
export default ForgotPassword;
