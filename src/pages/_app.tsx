import Router from 'next/router';
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

import 'fenextjs-component/styles/index.css';
import '@/styles/style.css';

import { maintenance } from '@/config/maintenance';
import { PageMaintenance } from '@/components/Pages/Maintenance';
import { Root } from '@/components/Root';

import { GlobalNotification } from '@/hook/useNotification';

function MyApp({ Component, pageProps }: any) {
    if (maintenance) {
        return <PageMaintenance />;
    }

    return (
        <div
            className="flex flex-column flex-nowrap"
            style={{ minHeight: 'calc(100vh - var(--sizeHeader,0px))' }}
        >
            <Component {...pageProps} />
            <GlobalNotification />
            <Root />
        </div>
    );
}

export default MyApp;
