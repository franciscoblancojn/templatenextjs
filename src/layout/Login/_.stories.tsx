import { Story, Meta } from "@storybook/react";

import { LayoutLoginProps, LayoutLogin } from "./index";

export default {
    title: "Layout/Login",
    component: LayoutLogin,
} as Meta;

const Template: Story<LayoutLoginProps> = (args) => (
    <LayoutLogin {...args}>Test Children</LayoutLogin>
);

export const Index = Template.bind({});
Index.args = {};
