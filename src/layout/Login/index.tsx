import { PropsWithChildren, useMemo } from 'react';

import ContentWidth from '@/components/ContentWidth';
import Img from '@/components/Img';
import { Link } from '@/components/Link';
import { LinkTerm } from '@/components/Links';
import { GoBack } from '@/components/GoBack';
import { SelectLang } from '@/components/SelectLang';

import * as styles from '@/layout/Login/styles';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';
import url from '@/data/routes';

export const LayoutLoginStyle = { ...styles } as const;

export type LayoutLoginStyles = keyof typeof LayoutLoginStyle;

export interface LayoutLoginProps {
    styleTemplate?: LayoutLoginStyles | ThemesType;
    skip?: boolean;
    fondo?: any;
    showLogo?: boolean;
    showTerm?: boolean;
    showLangs?: boolean;
}

export const LayoutLogin = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    skip = false,
    children,
    fondo = '',
    showLogo = true,
    showTerm = false,
    showLangs = true,
}: PropsWithChildren<LayoutLoginProps>) => {
    const _t = useLang();
    const Style = useMemo(
        () =>
            LayoutLoginStyle[styleTemplate as LayoutLoginStyles] ??
            LayoutLoginStyle._default,
        [styleTemplate]
    );
    return (
        <div className={Style.classNameContent} style={Style.styleContent}>
            <div
                className={`capa ${Style.classNameCapa}`}
                style={Style.styleCapa}
            ></div>
            {fondo}
            <div className={`top pos-r ${Style.classNameContentTop}`}>
                <div className={Style.classNameContentGoBack}>
                    <Link styleTemplate="tolinkme8" href={url.index}>
                        <GoBack />
                    </Link>
                </div>

                <ContentWidth
                    size={Style.sizeContentLogo}
                    className={Style.classNameContentLogo}
                >
                    <Link styleTemplate="tolinkme8" href={url.index}>
                        {showLogo && <Img src="logo.png" />}
                    </Link>
                </ContentWidth>
                <div className={Style.classNameContentSkip}>
                    {skip && (
                        <Link href={url.index} styleTemplate="tolinkme3">
                            {_t('Omitir')}
                        </Link>
                    )}
                    {showLangs ? (
                        <SelectLang
                            styleTemplate={Style.styleTemplateSelectLang}
                        />
                    ) : (
                        <></>
                    )}
                </div>
            </div>
            <div className={`content ${Style.classNameContentChildren}`}>
                {children}
            </div>
            {showTerm ? (
                <div className={`${Style.classNameContentLinkTerm}`}>
                    <LinkTerm />
                </div>
            ) : (
                <></>
            )}
        </div>
    );
};
export default LayoutLogin;
