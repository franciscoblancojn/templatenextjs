import { useMemo } from 'react';

import {
    LayoutBackofficeBase as LayoutBackofficeBase_,
    LayoutBackofficeBaseProps,
} from '@/layout/Backoffice/Base';

import * as styles from '@/layout/Backoffice/styles';

import { Theme, ThemesType } from '@/config/theme';

export const LayoutBackofficeBaseStyle = { ...styles } as const;

export type LayoutBackofficeBaseStyles = keyof typeof LayoutBackofficeBaseStyle;

export interface LayoutProps extends LayoutBackofficeBaseProps {
    styleTemplate?: LayoutBackofficeBaseStyles | ThemesType;
}

export const Layout = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: LayoutProps) => {
    const Style = useMemo(
        () =>
            LayoutBackofficeBaseStyle[
                styleTemplate as LayoutBackofficeBaseStyles
            ] ?? LayoutBackofficeBaseStyle._default,
        [styleTemplate]
    );
    return <LayoutBackofficeBase_ {...props} {...Style} />;
};

export const LayoutBackofficeBase = Layout;

export default Layout;
