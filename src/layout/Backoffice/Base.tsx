import CSS from 'csstype';
import { PropsWithChildren } from 'react';

import Header, { HeaderStyles } from '@/components/Header';

import { Theme, ThemesType } from '@/config/theme';

export interface LayoutClassProps {
    classNameContent?: string;
    styleContent?: CSS.Properties;

    styleTemplateHeader?: HeaderStyles | ThemesType;
}

export interface LayoutBackofficeBaseProps extends PropsWithChildren {
    className?: string;
}

export interface LayoutProps
    extends LayoutBackofficeBaseProps,
        LayoutClassProps {}

export const LayoutBackofficeBase = ({
    classNameContent = '',
    styleContent = {},

    styleTemplateHeader = Theme?.styleTemplate ?? '_default',

    className = '',
    children,
}: LayoutProps) => {
    return (
        <div
            className={`${className} ${classNameContent}`}
            style={styleContent}
        >
            <Header styleTemplate={styleTemplateHeader} />
            {children}
        </div>
    );
};
export default LayoutBackofficeBase;
