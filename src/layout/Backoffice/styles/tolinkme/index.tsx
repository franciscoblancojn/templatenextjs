import { LayoutClassProps } from '@/layout/Backoffice/Base';

export const tolinkme: LayoutClassProps = {
    classNameContent: `
        bg-white
        container
        p-sm-h-15
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    styleTemplateHeader: 'tolinkme',
};
