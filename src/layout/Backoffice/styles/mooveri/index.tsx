import { LayoutClassProps } from '@/layout/Backoffice/Base';

export const mooveri: LayoutClassProps = {
    classNameContent: `
        bg-white
        container
        p-sm-h-15
        p-t-15 p-md-t-33
        p-b-15 p-md-b-33
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    styleTemplateHeader: 'mooveri',
};
export const mooveriBackoffice: LayoutClassProps = {
    classNameContent: `
        bg-white
        p-sm-h-15
        p-t-15 p-md-t-33
        p-b-15 p-md-b-33
        transition-5
        bg-independence
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
        paddingLeft: 'calc(var(--wMenu,0px) + 0.9375rem)',
    },
    styleTemplateHeader: 'mooveriBackoffice',
};
