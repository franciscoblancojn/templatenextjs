import { LayoutClassProps } from '@/layout/Base/Base';

export const mooveri: LayoutClassProps = {
    classNameContent: `
        bg-white
        container
        p-sm-h-15
        p-t-15 p-md-t-33
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    styleTemplateHeader: 'mooveri',
};
