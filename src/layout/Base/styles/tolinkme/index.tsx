import { LayoutClassProps } from '@/layout/Base/Base';

export const tolinkme: LayoutClassProps = {
    classNameContent: `
        bg-white
        container
        p-sm-h-15
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    styleTemplateHeader: 'tolinkme',
};

export const tolinkme2: LayoutClassProps = {
    classNameContent: `
        bg-white
        container
        p-sm-h-15
    `,
    styleContent: {},
    styleTemplateHeader: 'tolinkm3',
};
