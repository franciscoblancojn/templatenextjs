import { Story, Meta } from "@storybook/react";

import { LayoutHomeProps, LayoutHome } from "./index";

export default {
    title: "Layout/Home",
    component: LayoutHome,
} as Meta;

const Template: Story<LayoutHomeProps> = (args) => (
    <LayoutHome {...args}>Test Children</LayoutHome>
);

export const Index = Template.bind({});
Index.args = {};
