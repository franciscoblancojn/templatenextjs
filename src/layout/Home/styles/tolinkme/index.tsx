import { LayoutLoginClassProps } from '@/layout/Login/styles/default';

export const tolinkme: LayoutLoginClassProps = {
    classNameContent: `
        height-vh-min-100 
        flex flex-column 
        p-t-27 p-b-100 
        p-sm-h-15
        bg-gradient-darkAqua-brightPink 
        pos-r
        animation
        animation-moveBg
        animation-duration-100
        animation-direction-alternate
    `,
    classNameContentTop: `
        flex
        flex-align-center
        flex-justify-between
        flex-nowrap
        p-h-17
    `,
    classNameContentGoBack: `
        width-p-31
        width-md-p-max-25
    `,
    classNameContentSkip: `
        width-p-31
        width-md-p-max-25
        flex 
        flex-justify-right
        flex-align-center
        pos-r
        z-index-3
    `,
    classNameContentLogo: `
        width-md-p-max-50
        width-p-max-27
        m-h-auto
        flex 
        flex-justify-center
    `,
    sizeContentLogo: 180,
    classNameContentChildren: `
        p-v-20 m-v-auto
        p-h-17
        pos-r
    `,
    styleContent: {},
    classNameCapa: `
        pos-a
        inset-0
        bg-red
    `,
    styleCapa: {
        background: 'url(/image/tolinkme/fondoLogin.png)',
        opacity: 0.04,
    },
    classNameContentLinkTerm: `
        pos-r
    `,
    styleTemplateSelectLang: 'tolinkme',
};
