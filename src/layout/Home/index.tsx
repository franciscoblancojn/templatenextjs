import { PropsWithChildren, useEffect } from 'react';
import { useRouter } from 'next/router';
import * as styles from '@/layout/Home/styles';
import { ThemesType } from '@/config/theme';
import Header from '@/components/Header';
import { useUser } from '@/hook/useUser';

export const LayoutHomeStyle = { ...styles } as const;
export type LayoutHomeStyles = keyof typeof LayoutHomeStyle;

export interface LayoutHomeProps {
    styleTemplate?: LayoutHomeStyles | ThemesType;
    goBack?: boolean;
    skip?: boolean;
    fondo?: any;
    showLogo?: boolean;
    showTerm?: boolean;
    showLangs?: boolean;
}

export const LayoutHome = ({
    // styleTemplate = Theme?.styleTemplate ?? '_default',
    children,
}: PropsWithChildren<LayoutHomeProps>) => {
    const { user } = useUser();
    {
        /*
        const Style = useMemo(
        () =>
            LayoutHomeStyle[styleTemplate as LayoutHomeStyles] ??
            LayoutHomeStyle._default,
        [styleTemplate]
    );
       */
    }

    const router = useRouter();

    useEffect(() => {
        if (router.pathname === '/es') {
            router.push('/');
        }
    }, []);

    return (
        <>
            {user ? (
                <>
                    <Header styleTemplate="tolinkm3" />
                </>
            ) : (
                <>
                    <Header styleTemplate="tolinkm4" />
                </>
            )}

            {children}
        </>
    );
};

export default LayoutHome;
