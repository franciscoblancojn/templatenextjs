import { Story, Meta } from "@storybook/react";

import { TwoColumnsProps, TwoColumns } from "./index";

export default {
    title: "Layout/TwoColumns",
    component: TwoColumns,
} as Meta;

const TwoColumnsIndex: Story<TwoColumnsProps> = (args) => (
    <TwoColumns {...args}>Test Children</TwoColumns>
);

export const Index = TwoColumnsIndex.bind({});
Index.args = {};
