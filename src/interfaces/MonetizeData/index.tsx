import { CurrentBalanceBaseProps } from '@/components/CurrentBalance/Base';
import { InformationSentBaseProps } from '@/components/InformationSent/Base';
import { MonetizeDataProps } from '@/interfaces/MonetizeDataProps';

export interface MonetizeData {
    icon?: any;
    icon2?: any;
    icoBack?: any;
    iconCamera?: any;
    title?: string;
    title2?: string;
    text?: string;
    easy?: string;
    text2?: string;
    textBtn?: string;
    textcheckbox1?: string;
    textcheckbox2?: string;
    textcheckbox18?: string;
    defaultValue?: MonetizeDataProps;
    statusSteps?: InformationSentBaseProps['statusSteps'];
    balance?: CurrentBalanceBaseProps;
}
