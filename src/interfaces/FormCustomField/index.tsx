import { AnimationsListType } from '../Animations';
import { BackgroundDataObjectTypeProps } from '../Background';
import { BorderConfig } from '../Border';
import { BoxShadowConfig } from '../BoxShadow';
import { ConfigText } from '../ConfigText';

export const SizeFormCustomFieldConst = ['slim', 'regular', 'strong'] as const;

export const SizeFormCustomField = [...SizeFormCustomFieldConst];

export type SizeFormCustomFieldType = (typeof SizeFormCustomFieldConst)[number];

export type SizeFormCustomFieldValuesType = {
    [id in SizeFormCustomFieldType]?: string;
};

export const SizeFormCustomFieldValues: SizeFormCustomFieldValuesType = {
    slim: `
        p-5
    `,
    regular: `
        p-10
    `,
    strong: `
        p-15
    `,
};
export const SizeFormCustomFieldContentImgValues: SizeFormCustomFieldValuesType =
    {
        slim: `
        p-5
    `,
        regular: `
        p-10
    `,
        strong: `
        p-10
    `,
    };
export const SizeFormCustomFieldImgValues: SizeFormCustomFieldValuesType = {
    slim: `
        height-20
        width-20
        object-fit-contain
        object-pocition-center
    `,
    regular: `
        height-25
        width-25
        object-fit-contain
        object-pocition-center
    `,
    strong: `
        height-30
        width-30
        object-fit-contain
        object-pocition-center
    `,
};
export const SizeFormCustomFieldImgValue = `
    height-p-100
    aspect-ratio-1-1
    object-fit-contain
`;

export const BorderRadiusFormCustomFieldConst = [
    'no-rounding',
    'semi-rounded',
    'rounded',
] as const;

export const BorderRadiusFormCustomField = [
    ...BorderRadiusFormCustomFieldConst,
];

export type BorderRadiusFormCustomFieldType =
    (typeof BorderRadiusFormCustomFieldConst)[number];

export type BorderRadiusFormCustomFieldValuesType = {
    [id in BorderRadiusFormCustomFieldType]?: string;
};

export const BorderRadiusFormCustomFieldValues: BorderRadiusFormCustomFieldValuesType =
    {
        'no-rounding': `
        border-radius-0
    `,
        'semi-rounded': `
        border-radius-10
    `,
        rounded: `
        border-radius-100
    `,
    };

export const IconFormCustomFieldConst = ['sin', 'con'] as const;

export const IconFormCustomField = [...IconFormCustomFieldConst];

export type IconFormCustomFieldType = (typeof IconFormCustomFieldConst)[number];

export type IconFormCustomFieldValuesType = {
    [id in IconFormCustomFieldType]?: string;
};

export const IconFormCustomFieldValues: IconFormCustomFieldValuesType = {
    sin: `
        d-none
    `,
    con: `
        left-15   
    `,
};

export interface FormCustomFieldIconConfig {
    borderRadius?: BorderRadiusFormCustomFieldType;
    background?: BackgroundDataObjectTypeProps;
    border?: BorderConfig;
    padding?: number;
    size?: number;
}

export type FormCustomFieldPrincipalConfigType =
    | 'size'
    | 'padding'
    | 'border'
    | 'icon'
    | 'bg';

export interface FormCustomFieldPrincipalConfig
    extends FormCustomFieldIconConfig {
    color?: string;
}

export type FormCustomFieldConfigType =
    | 'type'
    | 'textBtn'
    | 'box-shadow'
    | 'border'
    | 'bg'
    | 'backgroundBtn'
    | 'text'
    | 'title1'
    | 'titleForm'
    | 'descriptionForm'
    | 'TextBtnForm'
    | 'title2'
    | 'icon';

export interface FormCustomFieldConfig {
    backgroundBtn?: BackgroundDataObjectTypeProps;
    textBtn?: ConfigText;
    size?: SizeFormCustomFieldType;
    borderRadius?: BorderRadiusFormCustomFieldType;
    background?: BackgroundDataObjectTypeProps;
    text?: ConfigText;
    icon?: IconFormCustomFieldType;
    border?: BorderConfig;
    boxShadow?: BoxShadowConfig;
    iconConfig?: FormCustomFieldIconConfig;
    className?: string;
    title1?: ConfigText;
    title2?: ConfigText;
    titleForm?: string;
    descriptionForm?: string;
    TextBtnForm?: string;
}

export const FormCustomFieldAnimationsConfigConst = [
    'on-page-load',
    'infinite',
    'hover',
] as const;
export const FormCustomFieldAnimationsConfig = [
    ...FormCustomFieldAnimationsConfigConst,
];
export type FormCustomFieldAnimationsConfigType =
    (typeof FormCustomFieldAnimationsConfigConst)[number];

export type FormCustomFieldAnimationsConfig = {
    [id in FormCustomFieldAnimationsConfigType]?: AnimationsListType;
};
