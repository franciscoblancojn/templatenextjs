import { CardProps } from '@/interfaces/Card';

export interface PaymentProps {
    id?: string;
    card: CardProps;
}
