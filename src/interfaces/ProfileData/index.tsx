import { InfoProfileEditDataProps } from '@/components/InfoProfileEdit/Base';
import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';
import { MonetizeData } from '@/interfaces/MonetizeData';
import { Addons } from '../Addons';

export interface ProfileData {
    uuid?: string;
    user_uuid?: string;
    profile_uuid?: string;
    addons?: Addons[] | any;
    linksDefault: RSLinkConfigDataProps[];
    styleDefault: InfoProfileEditDataProps;
    isAprobed?: boolean;
    monetize?: MonetizeData;
}
