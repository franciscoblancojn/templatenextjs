export type DataRegisterPaymentInputs =
    | 'type'
    | 'userName'
    | 'email'
    | 'name'
    | 'firstName'
    | 'lastName'
    | 'phone'
    | 'password'
    | 'repeatPassword';

export type DataRegisterPayment<V = string> = {
    [id in DataRegisterPaymentInputs]?: V;
};
