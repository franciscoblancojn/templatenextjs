export type DataFormConfirmStep1Inputs =
    | 'avatar'
    | 'companyName'
    | 'legalName'
    | 'yearFounded'
    | 'facebook'
    | 'instagram';

export type DataFormConfirmStep1Props<V = string> = {
    [id in DataFormConfirmStep1Inputs]?: V;
};
