export type DataVerifyIdentityInputs =
    | 'FrontIdentification'
    | 'LaterIdentification';

export type DataVerifyIdentity<V = any> = {
    [id in DataVerifyIdentityInputs]?: V;
};
