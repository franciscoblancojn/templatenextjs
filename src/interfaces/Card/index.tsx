import { SubmitResult } from '@/components/Form/Base';
import { UserLoginProps } from '@/hook/useUser';

export type CardTypes = 'visa' | 'mastercard' | 'amex';

export interface CardProps {
    type: CardTypes;
    number: number | string;
}

export interface CardData {
    id: string;
    brand: string;
    last4: string;
    exp_month: number;
    exp_year: number;
    postal_code: string;
}

export type onSaveCardType = (
    data: CardData,
    user?: UserLoginProps
) => Promise<SubmitResult> | SubmitResult;

export type onDeleteCardType = (data: {
    user?: UserLoginProps;
    id?: string;
}) => Promise<SubmitResult> | SubmitResult;
