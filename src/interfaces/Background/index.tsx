import { InputFileDataProps } from '@/components/Input/File/Base';
import { InputGradientDataProps } from '@/components/Input/Gradient/Base';

export const BackgroundTypesConst = [
    'color',
    'gradient',
    'img',
    'video',
] as const;

export const BackgroundTypes = [...BackgroundTypesConst];

export type BackgroundType = (typeof BackgroundTypesConst)[number];

export type BackgroundValueType = {
    [id in BackgroundType]?: any;
};

export interface BackgroundDataObjectTypeProps {
    type?: BackgroundType;
    color?: string;
    gradient?: InputGradientDataProps;
    img?: InputFileDataProps;
    video?: InputFileDataProps;
    opacity?: number;
}
