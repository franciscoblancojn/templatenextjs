export type DataManageAddressInputs =
    | 'id'
    | 'address'
    | 'city'
    | 'apt'
    | 'zipcode'
    | 'location';

export type DataManageAddress<V = string> = {
    [id in DataManageAddressInputs]?: V;
};
