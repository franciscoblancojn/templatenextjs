import { BackgroundDataObjectTypeProps } from '../Background';

export type ConfigBackgroundType = 'bg' | 'bgMovil';

export interface ConfigBackground {
    bg?: BackgroundDataObjectTypeProps;
    useLayer?: boolean;
    bgLayer?: BackgroundDataObjectTypeProps;

    bgMovil?: BackgroundDataObjectTypeProps;
    useLayerMovil?: boolean;
    bgMovilLayer?: BackgroundDataObjectTypeProps;
}
