export type DataFormPaymentInputs =
    | 'email'
    | 'nameOnTheCard'
    | 'zipPostalCode'
    | 'Street'
    | 'country'
    | 'state'
    | 'stripe_before'
    | 'confirm18'
    | 'stripe_after'
    | 'city';

export type DataFormPayment<V = any> = {
    [id in DataFormPaymentInputs]?: V;
};
