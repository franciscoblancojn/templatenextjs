export const BoxShadowTypesConst = ['inset', 'normal'] as const;

export const BoxShadowTypes = [...BoxShadowTypesConst];

export type BoxShadowTypesType = (typeof BoxShadowTypesConst)[number];

export const BoxShadowTypesName: {
    [id in BoxShadowTypesType]: string;
} = {
    inset: 'Interno',
    normal: 'Externo',
};

export interface BoxShadowConfig {
    x?: number;
    y?: number;
    size?: number;
    blur?: number;
    color?: string;
    type?: BoxShadowTypesType;
}
