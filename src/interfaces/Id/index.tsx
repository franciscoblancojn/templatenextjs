import { RenderLoaderProps } from '@/components/Render';
import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';
import * as Ipapi from '@/api/ipapi';
import { PublicData } from '@/interfaces/PublicData';

export interface TolinkmeProps___
    extends Omit<PublicData, 'onClickBtn'>,
        idData {}

export interface idData extends Omit<PublicData, 'onClickBtn'> {
    ipInfo?: Ipapi.IpInfo | null;
    onClickBtn?: (
        ipInfo?: Ipapi.IpInfo | null,
        profile?: TolinkmeProps___
    ) => (button?: RSLinkConfigDataProps) => void;
    renderData?: RenderLoaderProps;
    isAprobed?: boolean;
}
