import { TypeDay } from '../Date';
import { TransactionProps } from '../Transaction';
import { UserProps } from '../User';

export interface CompanyDetailsProps {
    uuid?: string;
    created_at?: string;
    update_at?: string;
    status?: string;
    contact_name?: string;
    year_founded?: string;
    facebook_page_link?: string;
    instagram_page_link?: string;
    linkedin_link?: string;
    website_link?: string;
    trucks_have?: number;
    workers_have?: number;
    location_coordinate_lat?: number;
    location_coordinate_lng?: number;
    company_uuid?: string;
}
export interface CompanyVerificationProps {
    uuid?: string;
    created_at?: string;
    update_at?: string;
    local_license_link?: string;
    dot_license_link?: string;
    cdl_link?: string;
    local_license_status?: string;
    dot_license_status?: string;
    cdl_status?: string;
    company_uuid?: string;
}

export interface CompanyPricesProps {
    overtime_uuid?: string;
    overtime_value?: string | number;
    standar_uuid?: string;
    standar_value?: string | number;
    weekend_rate_uuid?: string;
    weekend_rate_value?: string | number;
}

export interface CompanySchedulesHours {
    uuid?: string;
    start?: string;
    end?: string;
    delete?: boolean;
}
export interface CompanySchedulesHoursDate
    extends Omit<CompanySchedulesHours, 'start' | 'end'> {
    start?: Date;
    end?: Date;
}

export interface CompanyHorariosProps {
    uuid?: string;
    day: TypeDay;
    active?: boolean;
    schedules?: CompanySchedulesHours[];
}

export interface CompanyProps {
    id: string;
    name: string;
    address: string;
    moovings: number;
    dateCreate: Date;

    legal_name?: string;
    phone_1?: string;
    phone_2?: string;
    city?: string;
    description?: string;
    state?: string;
    zip_code?: string;
    ssn?: string;
    ein?: string;
    imagen?: string;
    imagen_banner?: string;
    profile_status?: string;

    company_details?: CompanyDetailsProps;

    company_verification?: CompanyVerificationProps;

    company_prices?: CompanyPricesProps;

    company_horarios?: CompanyHorariosProps[];

    user_company?: UserProps;

    customers?: UserProps[];

    transactions?: TransactionProps[];
}

export interface CompanyImportProps extends CompanyProps {
    first_name?: string;
    last_name?: string;
    phone?: string;
    email?: string;
    password?: string;
    company_name?: string;
    company_legal_name?: string;
    company_phone_1?: string;
    company_phone_2?: string;
    company_city?: string;
    company_description?: string;
    company_state?: string;
    company_zip_code?: string;
    company_ssn?: string;
    company_ein?: string;
    company_imagen?: string;
    company_imagen_banner?: string;

    company_contact_name?: string;
    company_year_founded?: string;
    company_facebook_page_link?: string;
    company_instagram_page_link?: string;
    company_linkedin_link?: string;
    company_website_link?: string;
    company_trucks_have?: string;
    company_workers_have?: string;

    company_value_overtime?: string;
    company_value_standar?: string;
    company_value_weekend_rate?: string;

    company_horario_monday?: string | string[];
    company_horario_tuesday?: string | string[];
    company_horario_wednesday?: string | string[];
    company_horario_thursday?: string | string[];
    company_horario_friday?: string | string[];
    company_horario_saturday?: string | string[];
    company_horario_sunday?: string | string[];
}
