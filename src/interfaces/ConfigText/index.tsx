export const ConfigTextFontFamilyConst = [
    'nunito',
    'circular',
    'roboto',
    'open-sans',
    'lato',
    'oswald',
    'source-sans-pro',
] as const;

export const ConfigTextFontFamily = [...ConfigTextFontFamilyConst];

export type ConfigTextFontFamilyType =
    (typeof ConfigTextFontFamilyConst)[number];

export const ConfigTextFontWeightConst = [
    'thin',
    'extra-light',
    'light',
    'regular',
    'medium',
    'semi-bold',
    'bold',
    'extra-bold',
    'black',
] as const;

export const ConfigTextFontWeight = [...ConfigTextFontWeightConst];

export type ConfigTextFontWeightType =
    (typeof ConfigTextFontWeightConst)[number];

export const ConfigTextAlignConst = ['center', 'left', 'right'] as const;

export const ConfigTextAlignFamily = [...ConfigTextAlignConst];

export type ConfigTextAlignType = (typeof ConfigTextAlignConst)[number];

export interface ConfigText {
    fontFamily?: ConfigTextFontFamilyType;
    fontWeight?: ConfigTextFontWeightType;
    fontSize?: number;
    color?: string;
    lineHeight?: number;
    textAlignTitleForm?: ConfigTextAlignType;
    textAlignDescriptionForm?: ConfigTextAlignType;
}
