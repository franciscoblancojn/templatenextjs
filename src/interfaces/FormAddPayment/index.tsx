export type DataFormAddPaymentInputs = 'stripe_before' | 'stripe_after';

export type DataFormAddPayment<V = any> = {
    [id in DataFormAddPaymentInputs]?: V;
};
