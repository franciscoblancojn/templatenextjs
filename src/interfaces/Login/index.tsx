export type DataLoginInputs = 'email' | 'password' | 'type';

export type DataLogin<V = string> = {
    [id in DataLoginInputs]?: V;
};
