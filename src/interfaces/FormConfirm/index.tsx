import { DataFormConfirmStep1Props } from '@/interfaces/FormConfirmStep1';
import { DataFormConfirmStep2Props } from '@/interfaces/FormConfirmStep2';
import { DataFormConfirmStep3Props } from '@/interfaces/FormConfirmStep3';

export interface DataFormConfirm<T = string>
    extends DataFormConfirmStep1Props<T>,
        DataFormConfirmStep2Props<T>,
        DataFormConfirmStep3Props<T> {}
