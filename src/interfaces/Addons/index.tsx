export interface AddonsForm {
    uuid?: string;
    status?: boolean;
    addon_uuid?: string;
    name?: boolean;
    email?: boolean;
    phone?: boolean;
    message?: boolean;
    title_placeholder?: string;
    description_placeholder?: string;
    name_placeholder?: string;
    phone_placeholder?: string;
    email_placeholder?: string;
    message_placeholder?: string;
    button_placeholder?: string;
}

export interface Addons {
    uuid?: string;
    fromActive?: boolean;
    type?: 'FORM';
    form?: AddonsForm;
}
