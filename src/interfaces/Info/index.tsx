import { ConfigText } from '../ConfigText';

export type InfoConfigType = 'name' | 'description' | 'web';

export interface InfoConfig {
    name?: ConfigText;
    description?: ConfigText;
    web?: ConfigText;
}
