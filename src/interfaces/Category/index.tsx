export interface DataCategory {
    id: string;
    name: string;
    active?: boolean;
}
