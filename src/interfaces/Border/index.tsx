export const BorderTypesConst = [
    'solid',
    'dashed',
    'dotted',
    'double',
] as const;

export const BorderTypes = [...BorderTypesConst];

export type BorderTypesType = (typeof BorderTypesConst)[number];

export interface BorderConfig {
    size?: number;
    color?: string;
    type?: BorderTypesType;
}
