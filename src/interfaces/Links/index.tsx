export type DataLinksInputs = 'ForgotPassword' | 'Register';

export type DataLinks<V = string> = {
    [id in DataLinksInputs]?: V;
};
