import { CategoriesId } from '@/data/components/Categories';

export interface DataMyAccount {
    profile_uuid?: string;
    name: string;
    email: string;
    emailVerify: boolean;
    tel: {
        code: string;
        tel: string;
    };
    telVerify: boolean;
    password: string;
    repeatPassword?: string;
    categories: CategoriesId[];
}
