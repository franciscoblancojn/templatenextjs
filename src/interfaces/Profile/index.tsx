export type DataProfileInputs = 'companyName' | 'legalName' | 'description';

export type DataProfile<V = string> = {
    [id in DataProfileInputs]?: V;
};
