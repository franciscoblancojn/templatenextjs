export interface PhoneProps {
    code: string;
    tel: string;
}
