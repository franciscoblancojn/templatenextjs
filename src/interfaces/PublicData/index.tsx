import { InfoProfileEditDataProps } from '@/components/InfoProfileEdit/Base';

import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';
import { Addons } from '@/interfaces/Addons';

export interface PublicData {
    uuid?: string;
    name?: string;
    links: RSLinkConfigDataProps[];
    addons?: Addons[];
    style: InfoProfileEditDataProps;
    onClickBtn?: (button?: RSLinkConfigDataProps) => void;
    userLinksBuy?: string[];
}
