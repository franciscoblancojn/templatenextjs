export type DataMonetizeCategoryInputs =
    | 'categories'
    | 'confirm'
    | 'privacyPolicy'
    | 'type';

export type DataMonetizeCategory<V = any> = {
    [id in DataMonetizeCategoryInputs]?: V;
};
