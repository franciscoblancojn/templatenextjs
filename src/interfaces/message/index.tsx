export interface MessageUser {
    message_details?: {
        name?: string;
        email?: string;
        phone?: string;
        message?: string;
    };
    user_details?: {
        core_uuid?: string;
    };
}

export interface MessageUserSend {
    name?: string;
    email?: string;
    phone?: string;
    message?: string;
    core_uuid?: string;
}
export interface MessagesData {
    status?: string;
    uuid?: string;
    createdAt?: string;
    updatedAt?: string;
    Messages?: [
        {
            uuid?: string;
            text?: string;
            roomCaseUuid?: string;
            email?: string;
            phone?: string;
            name?: string;
            createdAt?: string;
            updatedAt?: string;
        }
    ];
}

export interface MessageUserData {
    Messages?: MessagesData[];
    count?: number;
    totalCount?: number;
}
