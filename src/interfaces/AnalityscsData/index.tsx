import { CountryCountProps } from '@/components/CountryCount';
import { ImgRsCountBaseProps } from '@/components/ImgRsCount/Base';
import { DateTypeValue } from '@/data/components/Filters';

export interface AnalitycsData {
    loader?: boolean;
    views?: number;
    totalClicks?: number;
    porcentClicks?: number;
    totalButtons?: number;
    listClicksButtons?: ImgRsCountBaseProps[];
    countryVisitors?: CountryCountProps[];
    countryClicks?: CountryCountProps[];

    onChangeDate?: (data: DateTypeValue | [Date, Date]) => void;
}
