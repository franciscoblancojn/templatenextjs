export type DataConfirmEmailInputs = 'email';

export type DataConfirmEmail<V = string> = {
    [id in DataConfirmEmailInputs]?: V;
};
