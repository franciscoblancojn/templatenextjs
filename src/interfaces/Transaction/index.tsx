import { CompanyProps } from '../Company';
import { UserProps } from '../User';

export interface TransactionProps {
    id: string;
    user: {
        id: string;
        name: string;
    };
    address: string;
    price: number;
    dateCreate: Date;

    chat_room_id?: string;
    client?: UserProps;
    client_uuid?: string;
    company_uuid?: string;
    created_at?: string;
    payment_intent_id?: string;
    status?: string;
    total_amount?: number;
    type?: string;
    type_payment?: string;
    type_user_transaction?: string;
    update_at?: string;
    uuid?: string;

    company?: CompanyProps;
}
