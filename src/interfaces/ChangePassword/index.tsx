export type DataChangePasswordInputs =
    | 'password'
    | 'repeatPassword'
    | 'token'
    | 'type';

export type DataChangePassword<V = string> = {
    [id in DataChangePasswordInputs]?: V;
};
