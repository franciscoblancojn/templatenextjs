export type DataFormMonetizeInputs =
    | 'recurrent'
    | 'period'
    | 'type'
    | 'optional'
    | 'description'
    | 'web'
    | 'name'
    | 'amount';

export type DataFormMonetize<V = string> = {
    [id in DataFormMonetizeInputs]?: V;
};
