# Datos pendientes por agregar en Btn Tolinkme

```ts
    isPrincipal : boolean (default false)
```

# Datos pendientes por agregar en styles Tolinkme

```ts
    btnPrincipalSize : number (default 10)
    btnPrincipalPadding : number (default 10)
    btnPrincipalColor : string (default "#ffffff")
    btnPrincipalRound : string (default "rounded")

    btnPrincipalBorderSize : number (default 0)
    btnPrincipalBorderColor : string (default "#ffffff")
    btnPrincipalBorderType : string (default "solid")
    
    btnPrincipalBgType : string (default "gradient")
    btnPrincipalBgColor : string (default "#ffffff")
    btnPrincipalBgGradientColor1 : string (default "#04506c")
    btnPrincipalBgGradientColor2 : string (default "#9d016e")
    btnPrincipalBgGradientDeg : number (default 130)
    btnPrincipalBgImg : string (default "")
    btnPrincipalBgVideo : string (default "")
    btnPrincipalBgOpacity : number (default 100)
```