import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'Fenextjs',
  webDir: 'out',
  bundledWebRuntime: false
};

export default config;
