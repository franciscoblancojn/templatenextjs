export const ErrorNumberWorkers = {
    required: 'Number of Workers is required',
    invalid: 'Number of Workers is invalid',
};
export default ErrorNumberWorkers;
