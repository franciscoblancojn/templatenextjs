export const ErrorCode = {
    required: 'Code is Required',
    invalid: 'Code invalid',
};
export default ErrorCode;
