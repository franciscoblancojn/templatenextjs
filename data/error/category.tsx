export const ErrorCategory = {
    required: 'Category is Required',
    invalid: 'Category invalid',
    min: 'Category is short',
    max: 'Category is long',
};
export default ErrorCategory;
