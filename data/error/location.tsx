export const ErrorLocation = {
    required: 'Location is Required',
    invalid: 'Location invalid',
};
export default ErrorLocation;
