export const ErrorMessage = {
    required: 'Message is Required',
    invalid: 'Message invalid',
};
export default ErrorMessage;
