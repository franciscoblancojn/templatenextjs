export const ErrorNumberTrucks = {
    required: 'Number of Trucks is required',
    invalid: 'Number of Trucks is invalid',
};
export default ErrorNumberTrucks;
