export const ErrorYearFounded = {
    required: 'Year Founded is Required',
    invalid: 'Year Founded invalid',
};
export default ErrorYearFounded;
