export const ErrorName = {
    required: 'Name is Required',
    invalid: 'Name invalid',
    min: 'Name is short',
    max: 'Name is long',
};
export const ErrorMiddleName = {
    required: 'Middle Name is Required',
    invalid: 'Middle Name invalid',
    min: 'Middle Name is short',
    max: 'Middle Name is long',
};
export const ErrorFirstName = {
    required: 'First Name is Required',
    invalid: 'First Name invalid',
    min: 'First Name is short',
    max: 'First Name is long',
};
export const ErrorLastName = {
    required: 'Last Name is Required',
    invalid: 'Last Name invalid',
    min: 'Last Name is short',
    max: 'Last Name is long',
};
export default ErrorName;
