export const ErrorEmail = {
    required: 'Email is Required',
    invalid: 'Email invalid',
};
export default ErrorEmail;
