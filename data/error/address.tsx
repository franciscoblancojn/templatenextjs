export const ErrorAddress = {
    required: 'Address is Required',
    invalid: 'Address invalid',
};
export default ErrorAddress;
