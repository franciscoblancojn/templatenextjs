export const ErrorFrontIdentification = {
    required: 'FrontIdentification is Required',
    invalid: 'FrontIdentification invalid',
};
export default ErrorFrontIdentification;
