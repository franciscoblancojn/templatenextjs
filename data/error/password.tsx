export const ErrorPassword = {
    required: 'Password is Required',
    invalid: 'Password Invalid',
    min: 'Password is short',
    max: 'Password is long',
    noRepeat: 'Password is diferent',
};
export default ErrorPassword;
