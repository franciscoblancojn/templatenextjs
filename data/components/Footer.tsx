export const DATA = {
    logo: 'logo.png',
    developers: [
        {
            link: 'https://franciscoblanco.vercel.app/',
            text: 'Francisco Blanco',
        },
    ],
};
