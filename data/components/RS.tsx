import Amazon from '@/svg/Amazon';
import Bancolombia from '@/svg/Bancolombia';
import Behance from '@/svg/Behance';
import Bongacams from '@/svg/Bongacams';
import Cams from '@/svg/Cams';
import Fansoda from '@/svg/Fansoda';
import Camsoda from '@/svg/Camsoda';
import Cashapp from '@/svg/Cashapp';
import Chaturbate from '@/svg/Chaturbate';
import Daviplata from '@/svg/Daviplata';
import Dribbble from '@/svg/Dribbble';
import Ebay from '@/svg/Ebay';
import Email from '@/svg/Email';
import Etsty from '@/svg/Etsty';
import FacebookF from '@/svg/FacebookF';
import Fantime from '@/svg/Fantime';
import Instagram from '@/svg/instagram';
import Linkedin from '@/svg/Linkedin';
import Manyvids from '@/svg/Manyvids';
import MercadoLibre from '@/svg/MercadoLibre';
import Nequi from '@/svg/Nequi';
import OnlyFans from '@/svg/OnlyFans';
import Patreon from '@/svg/Patreon';
import Paypal from '@/svg/Paypal';
import Pinterest from '@/svg/Pinterest';
import Pix from '@/svg/Pix';
import Reddit from '@/svg/Reddit';
import Snapchat from '@/svg/Snapchat';
import SoundCloud from '@/svg/SoundCloud';
import Spotify from '@/svg/Spotify';
import Steam from '@/svg/Steam';
import Stremate from '@/svg/Stremate';
import Stripachat from '@/svg/Stripachat';
import Stripe from '@/svg/Stripe';
import Telegram from '@/svg/Telegram';
import Tiktok from '@/svg/tiktok';
import Twitch from '@/svg/Twitch';
import Twitter from '@/svg/Twitter';
import Wechat from '@/svg/Wechat';
import Whatsapp from '@/svg/Whatsapp';
import Youtube from '@/svg/Youtube';
import Zelle from '@/svg/Zelle';
import Xfollow from '@/svg/Xfollow';

export interface RSItem {
    id: any;
    img: string;
    name: string;
    title?: string;
    placeholder?: string;
    href?: string;
    blocked?: boolean;
    svg?: any;
}

export const RS_SocialConst = [
    {
        id: 'tiktok',
        img: 'tik-tok.png',
        placeholder: '/Username',
        name: 'Tik Tok',
        svg: Tiktok,
    },
    {
        id: 'instagram',
        img: 'instagram.png',
        placeholder: '/Username',
        name: 'Instagram',
        svg: Instagram,
    },
    {
        id: 'facebook',
        img: 'facebook.png',
        placeholder: '/Username',
        name: 'Facebook',
        svg: FacebookF,
    },
    {
        id: 'youtube',
        img: 'youtube.png',
        placeholder: '/Username',
        name: 'Youtube',
        svg: Youtube,
    },
    {
        id: 'twitter',
        img: 'twitter.png',
        placeholder: '/Username',
        name: 'Twitter',
        svg: Twitter,
    },
    {
        id: 'snapchat',
        img: 'snapchat.png',
        placeholder: '/Username',
        name: 'Snapchat',
        svg: Snapchat,
    },
    {
        id: 'pinterest',
        img: 'pinterest.png',
        placeholder: '/Username',
        name: 'Pinterest',
        svg: Pinterest,
    },
    {
        id: 'linkedin',
        img: 'linkedin.png',
        placeholder: '/Username',
        name: 'Linkedin',
        svg: Linkedin,
    },
    {
        id: 'email',
        img: 'email.png',
        placeholder: 'mailto:email@email.com',
        name: 'Email',
        svg: Email,
    },
] as const;

export const RS_Social: RSItem[] = [...RS_SocialConst];

export type RS_Social_Type = (typeof RS_SocialConst)[number]['id'];

export const RS_DesingConst = [
    {
        id: 'behance',
        img: 'behance.png',
        placeholder: '/Username',
        name: 'Behance',
        svg: Behance,
    },
    {
        id: 'dribbble',
        img: 'dribbble.png',
        placeholder: '/Username',
        name: 'Dribbble',
        svg: Dribbble,
    },
] as const;

export const RS_Desing: RSItem[] = [...RS_DesingConst];

export type RS_Desing_Type = (typeof RS_DesingConst)[number]['id'];

export const RS_PaymentsConst = [
    {
        id: 'paypal',
        img: 'paypal.png',
        placeholder: '/',
        name: 'Paypal',
        svg: Paypal,
    },
    {
        id: 'stripe',
        img: 'stripe.png',
        placeholder: '/',
        name: 'Stripe',
        svg: Stripe,
    },
    {
        id: 'cashapp',
        img: 'cashapp.png',
        placeholder: '/',
        name: 'Cashapp',
        svg: Cashapp,
    },
    {
        id: 'zelle',
        img: 'zelle.png',
        placeholder: '/',
        name: 'Zelle',
        svg: Zelle,
    },
    {
        id: 'pix',
        img: 'pix.png',
        placeholder: '/',
        name: 'Pix',
        svg: Pix,
    },
    {
        id: 'nequi',
        img: 'nequi.png',
        placeholder: '/',
        name: 'Nequi',
        svg: Nequi,
    },
    {
        id: 'bancolombia',
        img: 'bancolombia.png',
        placeholder: '/',
        name: 'Bancolombia',
        svg: Bancolombia,
    },
    {
        id: 'daviplata',
        img: 'daviplata.png',
        placeholder: '/',
        name: 'Daviplata',
        svg: Daviplata,
    },
] as const;

export const RS_Payments: RSItem[] = [...RS_PaymentsConst];

export type RS_Payments_Type = (typeof RS_PaymentsConst)[number]['id'];

export const RS_MusicConst = [
    {
        id: 'spotify',
        img: 'spotify.png',
        placeholder: '/Username',
        name: 'Spotify',
        svg: Spotify,
    },
    {
        id: 'soundcloud',
        img: 'soundcloud.png',
        placeholder: '/Username',
        name: 'SoundCloud',
        svg: SoundCloud,
    },
] as const;

export const RS_Music: RSItem[] = [...RS_MusicConst];

export type RS_Music_Type = (typeof RS_MusicConst)[number]['id'];

export const RS_StreamingConst = [
    {
        id: 'twitch',
        img: 'twitch.png',
        placeholder: '/',
        name: 'Twitch',
        svg: Twitch,
    },
    {
        id: 'steam',
        img: 'steam.png',
        placeholder: '/',
        name: 'Steam',
        svg: Steam,
    },
] as const;

export const RS_Streaming: RSItem[] = [...RS_StreamingConst];

export type RS_Streaming_Type = (typeof RS_StreamingConst)[number]['id'];

export const RS_EcommerceConst = [
    {
        id: 'amazon',
        img: 'amazon.png',
        placeholder: '/',
        name: 'Amazon',
        svg: Amazon,
    },
    {
        id: 'ebay',
        img: 'ebay.png',
        placeholder: '/',
        name: 'Ebay',
        svg: Ebay,
    },
    {
        id: 'etsty',
        img: 'etsty.png',
        placeholder: '/',
        name: 'Etsty',
        svg: Etsty,
    },
    {
        id: 'mercadolibre',
        img: 'mercadolibre.png',
        placeholder: '/',
        name: 'Mercado Libre',
        svg: MercadoLibre,
    },
] as const;

export const RS_Ecommerce: RSItem[] = [...RS_EcommerceConst];

export type RS_Ecommerce_Type = (typeof RS_EcommerceConst)[number]['id'];

export const RS_AdultoConst = [
    {
        id: 'chaturbate',
        img: 'chaturbate.png',
        placeholder: '/',
        name: 'Chaturbate',
        svg: Chaturbate,
    },
    {
        id: 'bongacams',
        img: 'bongacams.png',
        placeholder: '/',
        name: 'Bongacams',
        svg: Bongacams,
    },
    {
        id: 'camsoda',
        img: 'camsoda.png',
        placeholder: '/',
        name: 'Camsoda',
        svg: Camsoda,
    },
    {
        id: 'stripachat',
        img: 'stripachat.png',
        placeholder: '/Username',
        name: 'Stripchat',
        svg: Stripachat,
    },
    {
        id: 'onlyfans',
        img: 'onlyfans.png',
        placeholder: '/Username',
        name: 'OnlyFans',
        svg: OnlyFans,
    },
    {
        id: 'fantime',
        img: 'fantime.png',
        placeholder: '/',
        name: 'Fantime',
        svg: Fantime,
    },
    {
        id: 'stremate',
        img: 'stremate.png',
        placeholder: '/',
        name: 'Stremate',
        svg: Stremate,
    },
    {
        id: 'manyvids',
        img: 'manyvids.png',
        placeholder: '/',
        name: 'Manyvids',
        svg: Manyvids,
    },
    {
        id: 'cams',
        img: 'cams.png',
        placeholder: '/',
        name: 'Cams',
        svg: Cams,
    },
    {
        id: 'fansoda',
        img: 'fansoda.png',
        placeholder: '/',
        name: 'Fansoda',
        svg: Fansoda,
    },
    {
        id: 'xfollow',
        img: 'xfollow.png',
        placeholder: '/',
        name: 'Xfollow',
        svg: Xfollow,
    },
] as const;

export const RS_Adulto: RSItem[] = [...RS_AdultoConst];

export type RS_Adulto_Type = (typeof RS_AdultoConst)[number]['id'];

export const RS_MessengerConst = [
    {
        id: 'whatsapp',
        img: 'whatsapp.png',
        placeholder: '+ Number',
        name: 'Whatsapp',
        svg: Whatsapp,
    },
    {
        id: 'telegram',
        img: 'telegram.png',
        placeholder: '/Username',
        name: 'Telegram',
        svg: Telegram,
    },
    {
        id: 'wechat',
        img: 'wechat.png',
        placeholder: '/',
        name: 'Wechat',
        svg: Wechat,
    },
] as const;

export const RS_Messenger: RSItem[] = [...RS_MessengerConst];

export type RS_Messenger_Type = (typeof RS_MessengerConst)[number]['id'];

export const RS_OthersConst = [
    {
        id: 'patreon',
        img: 'patreon.png',
        placeholder: '/Username',
        name: 'Patreon',
        svg: Patreon,
    },
    {
        id: 'reddit',
        img: 'reddit.png',
        placeholder: '/',
        name: 'Reddit',
        svg: Reddit,
    },
    {
        id: 'file',
        img: 'file.png',
        placeholder: 'FILE',
        name: 'Custom File',
    },
    {
        id: 'custom',
        img: 'custom.png',
        placeholder: '/',
        name: 'Custom Button',
    },
    {
        id: 'addon',
        img: 'file.png',
        placeholder: 'ADDON',
        name: 'Custom Addon',
    },
] as const;
export const RS_Others: RSItem[] = [...RS_OthersConst];

export type RS_Others_Type = (typeof RS_OthersConst)[number]['id'];

export const RS_AllConst = [
    {
        id: 'title_rs_social',
        img: 'social',
        name: 'Redes sociales',
        title: 'Redes sociales',
    },
    ...RS_SocialConst,

    {
        id: 'title_rs_design',
        img: 'design',
        name: 'Diseño',
        title: 'Diseño',
    },
    ...RS_DesingConst,

    {
        id: 'title_rs_payments',
        img: 'payments',
        name: 'Pagos',
        title: 'Pagos',
    },
    ...RS_PaymentsConst,

    {
        id: 'title_rs_Music',
        img: 'music',
        name: 'Musica',
        title: 'Musica',
    },
    ...RS_MusicConst,

    {
        id: 'title_rs_Streaming',
        img: 'streaming',
        name: 'Streaming',
        title: 'Streaming',
    },
    ...RS_StreamingConst,

    {
        id: 'title_rs_Ecommerce',
        img: 'Ecommerce',
        name: 'Ecommerce',
        title: 'Ecommerce',
    },
    ...RS_EcommerceConst,

    {
        id: 'title_rs_Adulto',
        img: 'Adulto',
        name: 'Adulto',
        title: 'Adulto',
    },
    ...RS_AdultoConst,
    {
        id: 'title_rs_Messenger',
        img: 'messenger',
        name: 'Messenger',
        title: 'Messenger',
    },
    ...RS_MessengerConst,

    {
        id: 'title_rs_custom',
        img: 'Others',
        name: 'Others',
        title: 'Others',
    },
    ...RS_OthersConst,
] as const;

export const RS_All: RSItem[] = [...RS_AllConst];
export type TypeRS =
    | RS_Social_Type
    | RS_Desing_Type
    | RS_Payments_Type
    | RS_Music_Type
    | RS_Streaming_Type
    | RS_Ecommerce_Type
    | RS_Adulto_Type
    | RS_Messenger_Type
    | RS_Others_Type;

export const RS_Share = [
    'whatsapp',
    'facebook',
    'twitter',
    // 'google',
    'telegram',
    'pinterest',
    'linkedin',
    'email',
] as const;

export type TypeRS_Share = (typeof RS_Share)[number];
