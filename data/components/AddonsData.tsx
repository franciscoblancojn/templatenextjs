export const AddonsMessages = [
    {
        label: 'Message',
    },
];

export const AddonsContact = [
    {
        label: 'Name',
    },
    {
        label: 'Email',
    },
    {
        label: 'Phone',
    },
];
