import { DataCategory } from '@/interfaces/Category';

export const all_categories = [
    {
        id: 'business',
        name: '💼Business',
    },
    {
        id: 'music',
        name: '🎵Music',
    },
    {
        id: 'signer',
        name: '‍🎤Signer',
    },
    {
        id: 'fitness',
        name: '🏋️Fitness',
    },
    {
        id: 'tech',
        name: '👨‍💻Tech',
    },
    {
        id: 'designer',
        name: '✏️Designer',
    },
    {
        id: 'beauty',
        name: '💄Beauty',
    },
    {
        id: 'fashion',
        name: '👒Fashion',
    },
    {
        id: 'food',
        name: '🍔Food',
    },
    {
        id: 'digital-creator',
        name: '🤳Digital Creator',
    },
    {
        id: 'adult-content',
        name: '👠Adult Content',
    },
    {
        id: 'webcam',
        name: '📷Webcam',
    },
    {
        id: 'model',
        name: '👚Model',
    },
] as const;

export const categories: DataCategory[] = [...all_categories];

export type CategoriesId = (typeof all_categories)[number]['id'];

export interface CategoryType {
    id: (typeof all_categories)[number]['id'];
    name: (typeof all_categories)[number]['name'];
}
