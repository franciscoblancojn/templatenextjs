export const DateConst = [
    {
        id: 'today',
        text: 'Hoy',
    },
    {
        id: '7-day',
        text: 'Últimos 7 días',
    },
    {
        id: '30-day',
        text: 'Últimos 30 días',
    },
    {
        id: 'month',
        text: 'Este Mes',
    },
    {
        id: 'lastMonth',
        text: 'El mes Pasado',
    },
    {
        id: 'all',
        text: 'Todos',
    },
    {
        id: 'data_piker',
        text: 'Personalizado',
    },
] as const;
export const Date = [...DateConst];

export type DateType = (typeof DateConst)[number];
export type DateTypeValue = (typeof DateConst)[number]['id'];
