import { url } from '@/data/routes';

export const DATA = {
    logo: 'logo.png',
    nav: [
        {
            link: '/',
            text: 'Home',
        },
        {
            link: '/doc',
            text: 'Documentacion',
        },
        {
            link: '/doc/icons',
            text: 'Icons',
        },
    ],
    nav2: [
        {
            link: url.myAccount.index,
            text: 'My Account',
        },
        {
            link: url.profile,
            text: 'Profile',
        },
        {
            link: url.mySubscriptions,
            text: 'My Subscriptions',
        },
        {
            link: '/logout',
            text: 'Log Out',
        },
    ],
    nav3: [
        {
            link: url.myAccount.index,
            text: 'Home',
        },
        {
            link: url.profile,
            text: 'Gallery',
        },
        {
            link: url.mySubscriptions,
            text: 'Blog',
        },
    ],
};
